---
type   : post
title  : "Setup LAMP stack with Manjaro OpenRC"
date   : 2015-10-16T12:49:15+07:00
slug   : lamp-stack-manjaro-openrc
categories: [backend]
tags      : [lamp, openrc]
keywords  : [lamp stack, manjaro, artix, openrc]
author : epsi
opengraph:
  image: assets/posts/case/2023/01-usman/11-bebek-gemes.jpg

toc    : "toc-2021-03-lamp-stack"

excerpt:
  This is a serious guidance for those setting LAMP
  in a not so very common environment.
  While most blog using systemd, MySQL and apt-get.
  This guidance show OpenRC, MariaDB and Pacman.

---

Preface
----------

Hello all.<br/>
I hope everyone that is reading this, is having a really good day.

I decide to share my ordinary log, <br/>
about setting up LAMP stack which is very common.

With special customization

* OpenRC instead of systemd

* MariaDB instead of MySQL

* Manjaro <code>pacman</code> instead of debian-based <code>apt-get</code>.

<br/>
Have Fun

[![LAMP OpenRC][image-ss-openrc]][photo-ss-openrc]

It is 2022, I'm using Artix instead of Manjaro OpenRC.

Reading
----------

Before you read this, you might want to check the holy arch wiki.

* [OpenRC][link-archwiki-openrc]

* [Pacman][link-archwiki-pacman]

* [Apache HTTP Server][link-archwiki-apache]

* [MySQL][link-archwiki-mysql]

* [PHPMyAdmin][link-archwiki-phpmyadmin]

Install
----------

#### [terminal]

{{< highlight bash >}}
$ sudo pacman -S apache mariadb php php-apache phpmyadmin apache-openrc mysql-openrc

$ sudo apachectl start

$ sudo rc-update add httpd default
 * service httpd added to runlevel default

$ sudo rc-update add mysql default
 * service mysql added to runlevel default
{{< / highlight >}} 

#### [browser: test]

> http://localhost/

Basic Apache Configuration
--------------------

#### [editor: /etc/httpd/conf/httpd.conf]

{{< highlight apacheconf >}}
# epsi!
LoadModule mpm_prefork_module modules/mod_mpm_prefork.so
LoadModule php5_module modules/libphp5.so

# comment out
#LoadModule mpm_event_module modules/mod_mpm_event.so

# uncomment
LoadModule rewrite_module modules/mod_rewrite.so

# epsi!
ServerName localhost

<IfModule mime_module>
    # ...    
    
 # epsi !
 AddType application/x-httpd-php .php
 AddType application/x-httpd-php-source .phps
 
</IfModule>

# epsi!
Include conf/extra/php5_module.conf
{{< / highlight >}}

In PHP 8

{{< highlight apacheconf >}}
LoadModule php_module modules/libphp.so
{{< / highlight >}}

#### [editor: /usr/lib/tmpfiles.d/]

This is not command line.
Edit this in your favorite text editor.

{{< highlight apacheconf >}}
d /run/httpd 0755 http http -
{{< / highlight >}}

#### [terminal]

{{< highlight bash >}}
$ sudo rc-service httpd restart
{{< / highlight >}}

Localhost Test
--------------------

#### [terminal]

{{< highlight bash >}}
$ sudo sh -c 'echo "<html><body>miauw</body><html>" > /srv/http/hello.html'
$ sudo sh -c 'echo "<html><body>miauw</body><html>" > /srv/http/hello.php'
$ sudo sh -c 'echo "<?php phpinfo(); ?>" > /srv/http/phpinfo.php'
{{< / highlight >}}

#### [browser: test]

> http://localhost/hello.html
> http://localhost/hello.php
> http://localhost/phpinfo.php

mariadb
--------------------

{{< highlight bash >}}
$ sudo /usr/bin/mysqld_safe --datadir='/var/lib/mysql'
{{< / highlight >}}

#### [editor: /etc/mysql/my.cnf]

{{< highlight ini >}}
# The MariaDB server
port = 3306
socket = /run/mysqld/mysqld.sock

# epsi !
user        = mysql
basedir     = /usr
datadir     = /var/lib/mysql
pid-file    = /run/mysql/mysql.pid
{{< / highlight >}}

{{< highlight bash >}}
$ sudo rc-service mysql restart
 * Checking mysqld configuration for mysql ...                    [ ok ]
 * Starting mysql ...
 * /run/mysql: creating directory
 * /run/mysql: correcting owner 
 
$ cd /media/Works/Backup.Temp/
$ mysql -u root < sf_book2.sql
$ mysql -u root < joomla30.sql
{{< / highlight >}}

In 2022 my config is simple

{{< highlight ini >}}
# The MariaDB server
port = 3306
socket = /run/mysqld/mysqld.sock

# epsi !
user        = mysql
{{< / highlight >}}

But the service has different name.

{{< highlight bash >}}
❯ sudo rc-service mariadb restart
[sudo] password for epsi: 
mariadb           | * Stopping mariadb ...                                   [ ok ]
mariadb           | * Starting mariadb ...
mariadb           |220202 20:14:04 mysqld_safe Logging to syslog.
mariadb           |220202 20:14:04 mysqld_safe Starting mariadbd daemon with databases from /var/lib/mysql     
{{< / highlight >}}
 
phpmyadmin
--------------------

#### [reading]

> <https://wiki.archlinux.org/index.php/PhpMyAdmin>

#### [terminal]

{{< highlight bash >}}
$ sudo touch ls /etc/httpd/conf/extra/phpmyadmin.conf
{{< / highlight >}}

#### [editor: /etc/httpd/conf/extra/phpmyadmin.conf]

{{< highlight apacheconf >}}
Alias /phpmyadmin "/usr/share/webapps/phpMyAdmin"
<Directory "/usr/share/webapps/phpMyAdmin">
    DirectoryIndex index.php
    AllowOverride All
    Options FollowSymlinks
    Require all granted
</Directory>
{{< / highlight >}}

#### [editor: /etc/httpd/conf/httpd.conf]

{{< highlight apacheconf >}}
# epsi!
# phpMyAdmin configuration
Include conf/extra/phpmyadmin.conf
{{< / highlight >}}

#### [editor: /etc/php/php.ini]

{{< highlight ini >}}
# set
date.timezone = "Asia/Jakarta"

# uncomment
extension=mysqli.so
# extension=mcrypt.so

# add directory
open_basedir = /srv/http/:/home/:/tmp/:/usr/share/pear/:/usr/share/webapps/:/etc/webapps/
{{< / highlight >}}

#### [terminal]

{{< highlight bash >}}
$ cat /etc/webapps/phpmyadmin/config.inc.php | less

$ sudo rc-service httpd restart
{{< / highlight >}}

#### [browser: test]

> http://localhost/phpmyadmin

#### [editor: /usr/share/webapps/phpMyAdmin/config.inc.php]

{{< highlight php >}}
 CREATE USER 'newuser'@'localhost' IDENTIFIED BY 'password';
 GRANT ALL PRIVILEGES ON *.* TO 'newuser'@'localhost';
 FLUSH PRIVILEGES;
{{< / highlight >}}

Local Host
--------------------

#### [editor: /etc/httpd/conf/httpd.conf]

{{< highlight apacheconf >}}
<Directory "/srv/http">
    # change
    AllowOverride All
</Directory>
{{< / highlight >}}

#### [terminal]

{{< highlight bash >}}
$ cd /srv/http
$ sudo ln -s /media/Works/Development/www/symfony2/book2/ book2
$ sudo ln -s /media/Works/Development/www/drupal/ drupal
$ sudo ln -s /media/Works/Development/www/sites/ sites

$ sudo rc-service httpd restart
{{< / highlight >}}

Virtual Host
--------------------

#### [terminal]

{{< highlight bash >}}
$ sudo mkdir /etc/httpd/conf/vhosts
$ sudo touch /etc/httpd/conf/vhosts/localhost.conf
$ sudo touch /etc/httpd/conf/vhosts/book2.conf
{{< / highlight >}}

#### [editor: /etc/hosts]

{{< highlight bash >}}
#<ip-address>   <hostname.domain.org>   <hostname>
127.0.0.1       localhost
127.0.1.1       axioo
127.0.0.1       book2
127.0.0.1       localhost.localdomain   localhost
::1             localhost.localdomain   localhost
{{< / highlight >}}

#### [editor: /etc/httpd/conf/vhosts/localhost.conf]

{{< highlight apacheconf >}}
<VirtualHost *:80>
    ServerName localhost
    DocumentRoot /srv/http
</VirtualHost>
{{< / highlight >}}

*

#### [editor: /etc/httpd/conf/vhosts/book2.conf]

{{< highlight apacheconf >}}
<VirtualHost *:80>

    ServerName book2
    DocumentRoot /media/Works/Development/www/symfony2/book2/web
    DirectoryIndex app.php
    ErrorLog /var/log/httpd/book2.log
    CustomLog /var/log/httpd/book2.log common

    <Directory "/media/Works/Development/www/symfony2/book2/web">
        AllowOverride All
     Require all granted
    </Directory>
    
</VirtualHost>
{{< / highlight >}}

*

#### [editor: /etc/httpd/conf/httpd.conf]

{{< highlight apacheconf >}}
# epsi!
# Enabled Vhosts:
Include conf/vhosts/localhost.conf
Include conf/vhosts/book2.conf
{{< / highlight >}}

#### [editor: /etc/php/php.ini]

{{< highlight ini >}}
# uncomment
extension=mysql.so
extension=pdo_mysql.so

# add folder
open_basedir = /srv/http/:/home/:/tmp/:/usr/share/pear/:/usr/share/webapps/:/etc/webapps/:/media/Works/Development/www/
{{< / highlight >}}

#### [terminal]

{{< highlight bash >}}
$ sudo rc-service httpd restart
$ httpd -S
{{< / highlight >}}

[![LAMP Browser][image-ss-browser]][photo-ss-browser]

Finalization
--------------------

Sleep

Have a nice dream.

[//]: <> ( -- -- -- links below -- -- -- )

[image-ss-openrc]:  {{< baseurl >}}assets/posts/backend/2015/10/lamp-manjaro-openrc-terminal.png
[photo-ss-openrc]:  https://photos.google.com/album/AF1QipOYi1tE3AwxfL0DQCn7eAhQHekVu1xxo2-lHjta/photo/AF1QipMNA4kSsPrBTD28YziByRo-SxTnvtjY1S5eYUkU
[image-ss-browser]: {{< baseurl >}}assets/posts/backend/2015/10/lamp-manjaro-openrc-browser.png
[photo-ss-browser]: https://photos.google.com/album/AF1QipOYi1tE3AwxfL0DQCn7eAhQHekVu1xxo2-lHjta/photo/AF1QipNbd-SbyNbBdGCTq9ft47Iz-UbX8O0faBbLyyT_

[link-archwiki-openrc]: https://wiki.archlinux.org/index.php/OpenRC
[link-archwiki-apache]: https://wiki.archlinux.org/index.php?title=Apache_HTTP_Server&redirect=no
[link-archwiki-mysql]: https://wiki.archlinux.org/index.php/MySQL
[link-archwiki-phpmyadmin]: https://wiki.archlinux.org/index.php/PhpMyAdmin
[link-archwiki-pacman]: https://wiki.archlinux.org/index.php/pacman
