---
type   : post
title  : "Docker - Setup"
date   : 2021-03-21T12:49:15+07:00
slug   : docker-setup
categories: [backend]
tags      : [lamp, openrc]
keywords  : [docker, damp stack, artix, openrc]
author : epsi
opengraph:
  image: assets/posts/case/2023/01-usman/11-bebek-gemes.jpg

toc    : "toc-2021-03-lamp-stack"

excerpt:
  Setting-up basic docker service with Artix OpenRC.

---

### Preface

> Goal: Setting-up basic docker service with Artix OpenRC.

The last time I wrote about LAMP stack is 2015.
And another LAMP article in 2017.
I also write docker article in 2017.
I have been craving to learn to use DAMP stack,
an application using docker, since those days.

Unfortunately life happened,
I had been busy leveraging my knowledge.
I simply have no time to explore.
People around me, not willing to give a clue.
Five years has gone away,
and I keep myself dumb about DAMP.

Suddenly I setup LAMP stack, and my friend told me,
to use DAMP stack, instead of LAMP stack.
And it turned out surprisingly easy.

I know I'm, running out of time.
I do not suppose to write,
in very tight working hours.
Writing these article take hours our man hour.
But I need to keep a not for my future self.
So I can find out the setup quickly in my own blog.
So here it is, my notes as usual.

#### Why Write?

> Yeah why?

Why do I write? Why should I update?
Because I fail so many times.
Because I'm not a genious.
That I have to keep a note on any successfull attempt.

_I don't mind with shortermism._
_But I keep writing anyway_

-- -- --

### Package Manager and Service

> Diversity Consideration

Most of the tutorial related to docker and LAMP stack,
is available with most widely used system,
with my my rough guess is about these two:

1. Package Manager: Mostly used is Debian based APT and Redhat Family.

2. Init: Widely adopted is using systemd.

I decide not to make,
just yet another APT+systemd tutorial.
But rather by giving a chance for diversity,
using Pacman+OpenRC.

#### Package Manager

> Disclaimer

This material utilize pacman.
Your package manager might need different packages.
Your linux distribution might use different package manager.

#### Service

> Disclaimer

This material utilize OpenRC.
Init in your system might utilize different command.
The systemd is the most widely used init system in modern Linux distributions.

-- -- --

### Install Docker

I'm using Artix linux.
It is basically, an Arch based linux distribution,
that using different ini other than `systemd`.
So we should begin with `pacman`.

Since I use OpenRC,
I'm going to need additional package,
related with init system.

{{< highlight bash >}}
❯ sudo pacman -S docker docker-openrc
{{< / highlight >}}

The output is as below:

{{< highlight bash >}}
❯ sudo pacman -S docker docker-openrc
resolving dependencies...
looking for conflicting packages...

Packages (5) bridge-utils-1.7.1-1
             containerd-1.6.19-1  runc-1.1.4-1
             docker-1:23.0.1-1
             docker-openrc-20210505-1

Total Installed Size:  228.61 MiB

:: Proceed with installation? [Y/n] y
(5/5) checking keys in keyring                    
(5/5) checking package integrity                  
(5/5) loading package files                       
(5/5) checking for file conflicts                 
(5/5) checking available disk space               
{{< / highlight >}}

![Docker Setup: Pacman: Docker][01-pacman-s-top]

Just be aware of the last message.
You might need to manually configure the service.

{{< highlight bash >}}
:: Processing package changes...
(1/5) installing bridge-utils                     
(2/5) installing runc                             
Optional dependencies for runc
    criu: checkpoint support
(3/5) installing containerd                       
(4/5) installing docker                           
Optional dependencies for docker
    btrfs-progs: btrfs backend support [installed]
    pigz: parallel gzip compressor support
    docker-scan: vulnerability scanner
    docker-buildx: extended build capabilities
(5/5) installing docker-openrc                    
:: Running post-transaction hooks...
(1/2) Creating system user accounts...
(2/2) Displaying openrc service help ...
	==> Add a service to runlevel:
	rc-update add <service> <runlevel>
	==> Start/stop/restart a service:
	rc-service <service> <start/stop/restart>
{{< / highlight >}}

![Docker Setup: Pacman: Docker OpenRC][02-pacman-s-bottom]

#### Archwiki

> Holy ...

Just open your holy archwiki for more infomation

* [/wiki.archlinux.org/title/docker][archwiki]

[archwiki]: https://wiki.archlinux.org/title/docker

-- -- --

### User and Group Management

> Privileges

The thing is, we should setup privileges properly.
After install, you sytem would have these additional entry:

{{< highlight bash >}}
❯ cat /etc/group | grep docker
docker:x:960:

❯ sudo cat /etc/gshadow | grep docker
docker:!*::
{{< / highlight >}}

![Docker Setup: Privileges: group][03-group]

Adding user group is easy:

{{< highlight bash >}}
❯ sudo gpasswd -a epsi docker
Adding user epsi to group docker

❯ sudo cat /etc/gshadow | grep docker
docker:!*::epsi
{{< / highlight >}}

Now the both `group` and `gshadow` are affected:

![Docker Setup: Privileges: gshadow][05-gpasswd]

You can just leave the sock with `root:docker` ownership as below:

![Docker Setup: Privileges: Sock][08-docker-sock]

{{< highlight bash >}}
❯ ls -l /var/run/docker.sock
srw-rw---- 1 root docker 0 Mar 21 12:31 /var/run/docker.sock
❯ stat /var/run/docker.sock
  File: /var/run/docker.sock
  Size: 0         	Blocks: 0          IO Block: 4096   socket
Device: 0,20	Inode: 2736        Links: 1
Access: (0660/srw-rw----)  Uid: (    0/    root)   Gid: (  960/  docker)
Access: 2023-03-21 12:37:39.706365883 +0700
Modify: 2023-03-21 12:31:09.943018819 +0700
Change: 2023-03-21 12:31:09.943018819 +0700
 Birth: 2023-03-21 12:31:09.943018819 +0700
{{< / highlight >}}

You might need to logout to make the user privilege called correctly.

-- -- --

### Running Service

> Init in your system might utilize different command.

_sThe systemd is the most widely used init system in modern Linux distributions_.

Now we can light up the service.
Using `OpenRC`, the command is as below:

{{< highlight bash >}}
❯ sudo rc-update add docker default
 * service docker added to runlevel default
{{< / highlight >}}

![Docker Setup: Update][06-rc-update]

You can check the status.
If the service has not been start,
you might need to start manually.

{{< highlight bash >}}
❯ sudo rc-service docker status
 * status: stopped
❯ sudo rc-service docker start
docker            | * Starting docker ...       [ ok ]
❯ sudo rc-service docker status
 * status: started
{{< / highlight >}}

![Docker Setup: Service][07-rc-service]

If you don't want docker to be run automatically,
tou can safely remove from default level.

{{< highlight bash >}}
❯ sudo rc-update delete docker
 * service docker removed from runlevel default
{{< / highlight >}}

-- -- --

### Running

Check if docker run properly.

{{< highlight bash >}}
❯ sudo docker version
{{< / highlight >}}

{{< highlight bash >}}
Client:
 Version:           23.0.1
 API version:       1.42
 Go version:        go1.20
 Git commit:        a5ee5b1dfc
 Built:             Sun Feb 12 07:07:35 2023
 OS/Arch:           linux/amd64
 Context:           default

Server:
 Engine:
  Version:          23.0.1
  API version:      1.42 (minimum version 1.12)
  Go version:       go1.20
  Git commit:       bc3805a0a0
  Built:            Sun Feb 12 07:07:35 2023
  OS/Arch:          linux/amd64
  Experimental:     false
 containerd:
  Version:          v1.6.19
  GitCommit:        1e1ea6e986c6c86565bc33d52e34b81b3e2bc71f
 runc:
  Version:          1.1.4
  GitCommit:        
 docker-init:
  Version:          0.19.0
  GitCommit:        de40ad0
{{< / highlight >}}

![Docker Setup: Docker Version][11-dc-version]

{{< highlight bash >}}
❯ docker info
{{< / highlight >}}

{{< highlight bash >}}
Client:
 Context:    default
 Debug Mode: false
 Plugins:
  compose: Docker Compose (Docker Inc.)
    Version:  2.16.0
    Path:     /usr/lib/docker/cli-plugins/docker-compose

Server:
 Containers: 0
  Running: 0
  Paused: 0
  Stopped: 0
 Images: 2
 Server Version: 23.0.1
 Storage Driver: overlay2
  Backing Filesystem: extfs
  Supports d_type: true
  Using metacopy: true
  Native Overlay Diff: false
  userxattr: false
 Logging Driver: json-file
 Cgroup Driver: cgroupfs
 Cgroup Version: 1
 Plugins:
  Volume: local
  Network: bridge host ipvlan macvlan null overlay
  Log: awslogs fluentd gcplogs gelf journald json-file local logentries splunk syslog
 Swarm: inactive
 Runtimes: runc io.containerd.runc.v2
 Default Runtime: runc
 Init Binary: docker-init
 containerd version: 1e1ea6e986c6c86565bc33d52e34b81b3e2bc71f
 runc version: 
 init version: de40ad0
 Security Options:
  seccomp
   Profile: builtin
 Kernel Version: 6.2.6-artix1-1
 Operating System: Artix Linux
 OSType: linux
 Architecture: x86_64
 CPUs: 4
 Total Memory: 7.511GiB
 Name: andalan
 ID: 283982ab-a821-4447-8893-382baa5c4465
 Docker Root Dir: /var/lib/docker
 Debug Mode: false
 Registry: https://index.docker.io/v1/
 Experimental: false
 Insecure Registries:
  127.0.0.0/8
 Live Restore Enabled: false
{{< / highlight >}}

![Docker Setup: Docker Info][12-docker-info]

-- -- --

### What is Next 🤔?

We need to see how we can use docker in real life with `docker-compose`.

Consider continue reading [ [Docker - Compose][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}backend/2021/03/23/docker-compose/

[01-pacman-s-top]:      {{< baseurl >}}assets/posts/backend/2021/03/01-pacman-s-top.png
[02-pacman-s-bottom]:   {{< baseurl >}}assets/posts/backend/2021/03/02-pacman-s-bottom.png
[03-group]:             {{< baseurl >}}assets/posts/backend/2021/03/03-group.png
[04-gpasswd]:           {{< baseurl >}}assets/posts/backend/2021/03/04-gpasswd.png
[05-gpasswd]:           {{< baseurl >}}assets/posts/backend/2021/03/05-gpasswd.png
[06-rc-update]:         {{< baseurl >}}assets/posts/backend/2021/03/06-rc-update.png
[07-rc-service]:        {{< baseurl >}}assets/posts/backend/2021/03/07-rc-service.png
[08-docker-sock]:       {{< baseurl >}}assets/posts/backend/2021/03/08-docker-sock.png
[11-dc-version]:        {{< baseurl >}}assets/posts/backend/2021/03/11-dc-version.png
[12-docker-info]:       {{< baseurl >}}assets/posts/backend/2021/03/12-docker-info.png

