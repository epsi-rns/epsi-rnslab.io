---
type   : post
title  : "Docker - Compose"
date   : 2021-03-23T12:49:15+07:00
slug   : docker-compose
categories: [backend]
tags      : [lamp, openrc]
keywords  : [docker, damp stack, artix, openrc]
author : epsi
opengraph:
  image: assets/posts/backend/2021/03/21-dc-summary.png

toc    : "toc-2021-03-lamp-stack"

excerpt:
  Setting-up docker-compose to run phpmyadmin.

---

### Preface

> Goal: Setting-up docker-compose to run phpmyadmin.

We need to see how we can use docker in real life with `docker-compose` to setup a phpmyadmin site in localhost.

-- -- --

### Summary

As a summary, the command we need to use is as simple as these below:

{{< highlight bash >}}
❯ docker compose pull
[+] Running 2/2
 ⠿ phpmyadmin Pulled                               2.9s
 ⠿ db Pulled                                       2.9s
❯ docker compose up -d
[+] Running 2/2
 ⠿ Container db   Started                          0.4s
 ⠿ Container pma  Started                          0.8s
❯ docker ps -a --format 'table {{.Names}}\t{{.Status}}'
NAMES     STATUS
pma       Up 8 seconds
db        Up 8 seconds
❯ docker compose stop
[+] Running 2/2
 ⠿ Container pma  Stopped                          1.2s
 ⠿ Container db   Stopped                          1.4s
❯ docker ps -a --format 'table {{.Names}}\t{{.Status}}'
NAMES     STATUS
pma       Exited (0) 4 seconds ago
db        Exited (0) 2 seconds ago
❯ docker compose rm -f
Going to remove pma, db
[+] Running 2/0
 ⠿ Container db   Removed                          0.0s
 ⠿ Container pma  Removed                          0.0s
❯ docker ps -a --format 'table {{.Names}}\t{{.Status}}'
NAMES     STATUS
❯ docker compose down
[+] Running 1/1
 ⠿ Network dockerngadimin_default  Removed       0.1s
❯ docker-compose build --pull
{{< / highlight >}}

![Docker Compose: CLI Summary][21-dc-summary]

The output while the docker running is as below:

![Docker Compose: PHPMyAdmin Screenshot][25-dc-phpmyadmin]

-- -- --

### Installation

We need to install one more package.

{{< highlight bash >}}
❯ sudo pacman -S docker-compose
{{< / highlight >}}

{{< highlight bash >}}
resolving dependencies...
looking for conflicting packages...

Packages (1) docker-compose-2.16.0-1

Total Installed Size:  49.87 MiB

:: Proceed with installation? [Y/n] y
(1/1) checking keys in keyring                    
(1/1) checking package integrity                  
(1/1) loading package files                       
(1/1) checking for file conflicts                 
(1/1) checking available disk space               
:: Processing package changes...
(1/1) installing docker-compose   
{{< / highlight >}}

![Docker Setup: Pacman: Docker Compose][10-pacman-s-compose]

-- -- --

### Configuration

For this example,
I copy-paste the configuration from site below:

* [Docker-compose for MySQL with phpMyAdmin][tecmint]

[tecmint]: https://tecadmin.net/docker-compose-for-mysql-with-phpmyadmin/

Consider make a new directory for our new project.
PHPMyAdmin in Docker.

{{< highlight bash >}}
❯ mkdir dockerngadimin
❯ cd dockerngadimin
{{< / highlight >}}

and make new file named `docker-compose.yml`.
The YAML configuration would be as below:

{{< highlight yml >}}
version: '3'
 
services:
  db:
    image: mysql:5.7
    container_name: db
    environment:
      MYSQL_ROOT_PASSWORD: blewah
      MYSQL_DATABASE: app_db
      MYSQL_USER: db_user
      MYSQL_PASSWORD: db_user_pass
    ports:
      - "6033:3306"
    volumes:
      - dbdata:/var/lib/mysql
  phpmyadmin:
    image: phpmyadmin/phpmyadmin
    container_name: pma
    links:
      - db
    environment:
      PMA_HOST: db
      PMA_PORT: 3306
      PMA_ARBITRARY: 1
    restart: always
    ports:
      - 8081:80
volumes:
  dbdata:
{{< / highlight >}}

![Docker Compose: docker-compose.yml][22-dc-vim-yml]

In that directory you can start with,
pulling the docker image into your linux system.

{{< highlight bash >}}
❯ docker-compose pull
[+] Running 31/2
 ⠿ db Pulled                                    39.1s
 ⠿ phpmyadmin Pulled                            27.9s
{{< / highlight >}}

![Docker Compose: First Pull][23-dc-first-pull]

The first pull will download the docker image.
This might take a few seconds long.

-- -- --

❯ sudo pacman -S docker-compose
resolving dependencies...
looking for conflicting packages...

Packages (1) docker-compose-2.16.0-1

Total Installed Size:  49.87 MiB

:: Proceed with installation? [Y/n] y
(1/1) checking keys in keyring                    
(1/1) checking package integrity                  
(1/1) loading package files                       
(1/1) checking for file conflicts                 
(1/1) checking available disk space               
:: Processing package changes...
(1/1) installing docker-compose  

-- -- --

### Case: PHPMyAdmin

Consider have case, such as having phpmyadmin installed as docker.
This way, we do not pollute the main system.
We can break down the step one by one.

#### Pull

We can start by pulling the docker.

{{< highlight bash >}}
❯ docker compose pull
[+] Running 2/2
 ⠿ phpmyadmin Pulled                             3.0s
 ⠿ db Pulled                                     3.0s
{{< / highlight >}}

![Docker Compose: Pull][23-dc-pull]

#### Up as Daemon

Then running as service

{{< highlight bash >}}
❯ docker compose up -d
[+] Running 2/2
 ⠿ Container db   Started                        0.4s
 ⠿ Container pma  Started                        0.8s
{{< / highlight >}}

![Docker Compose: Up as Daemon][24-dc-up-d]

#### Preview in Browser

Now we can open the PHPMyAdmin in any browser.

![Docker Compose: PHPMyAdmin Screenshot][26-dc-phpmyadmin]

{{< highlight bash >}}
❯ docker compose stop
[+] Running 2/2
 ⠿ Container pma  Stopped                        1.2s
 ⠿ Container db   Stopped                        1.4s
{{< / highlight >}}

#### Stop

You don't need to stop the process.
But if you wish, you can just stop the daemon,
for local directory.

![Docker Compose: Stop][27-dc-stop]

{{< highlight bash >}}
❯ docker compose rm -f
Going to remove pma, db
[+] Running 2/0
 ⠿ Container db   Removed                        0.0s
 ⠿ Container pma  Removed                        0.0s
{{< / highlight >}}

![Docker Compose: Remove][28-dc-rm-f]

After stopping,
the docker process would show nothing.

{{< highlight bash >}}
❯ docker ps -a --format 'table {{.Names}}\t{{.Status}}'
NAMES     STATUS
{{< / highlight >}}

![Docker Compose: Process Table][29-d-ps-table]

#### Down

I'm not sure about this.

{{< highlight bash >}}
❯ docker compose down
[+] Running 1/1
 ⠿ Network dockerngadimin_default  Removed       0.1s
{{< / highlight >}}

![Docker Compose: Down][31-dc-down]

I just like to issue some command.

-- -- --

### What is Next 🤔?

You might be interested in see what happened to the docker image.

Consider continue reading [ [Docker - Image][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}backend/2021/03/25/docker-image/

[10-pacman-s-compose]:  {{< baseurl >}}assets/posts/backend/2021/03/10-pacman-s-compose.png

[21-dc-summary]:        {{< baseurl >}}assets/posts/backend/2021/03/21-dc-summary.png
[22-dc-vim-yml]:        {{< baseurl >}}assets/posts/backend/2021/03/22-dc-vim-yml.png
[23-dc-first-pull]:     {{< baseurl >}}assets/posts/backend/2021/03/23-dc-first-pull.png
[23-dc-pull]:           {{< baseurl >}}assets/posts/backend/2021/03/23-dc-pull.png
[24-dc-up-d]:           {{< baseurl >}}assets/posts/backend/2021/03/24-dc-up-d.png
[25-dc-phpmyadmin]:     {{< baseurl >}}assets/posts/backend/2021/03/25-dc-phpmyadmin.png
[26-dc-phpmyadmin]:     {{< baseurl >}}assets/posts/backend/2021/03/26-dc-phpmyadmin.png
[27-dc-stop]:           {{< baseurl >}}assets/posts/backend/2021/03/27-dc-stop.png
[28-dc-rm-f]:           {{< baseurl >}}assets/posts/backend/2021/03/28-dc-rm-f.png
[29-d-ps-table]:        {{< baseurl >}}assets/posts/backend/2021/03/29-d-ps-table.png
[31-dc-down]:           {{< baseurl >}}assets/posts/backend/2021/03/31-dc-down.png

