---
type   : post
title  : "Docker - Image and Process"
date   : 2021-03-25T12:49:15+07:00
slug   : docker-image
categories: [backend]
tags      : [lamp, openrc]
keywords  : [docker, damp stack, artix, openrc]
author : epsi
opengraph:
  image: assets/posts/case/2023/01-usman/11-bebek-kechil.jpg

toc    : "toc-2021-03-lamp-stack"

excerpt:
  Examine docker image for docker compose.

---

### Preface

> Goal: Examine docker image for docker compose.

We need to understand the difference between image and process.

-- -- --

### Docker Image

### Checking Image

Even when the process has been stopped.
The docker image is still exist in your system.

You can check using `docker image`.

{{< highlight bash >}}
❯ docker image ls
REPOSITORY              TAG       IMAGE ID       CREATED        SIZE
composetest-web         latest    537a9d280280   21 hours ago   215MB
mysql                   5.7       0018a8d83892   12 days ago    455MB
redis                   alpine    5130e5ba7d24   2 weeks ago    29.9MB
phpmyadmin/phpmyadmin   latest    f50666f18b90   5 weeks ago    517MB
{{< / highlight >}}

### Delete One Image

You can delete the image that you don't need.

{{< highlight bash >}}
❯ docker image rm 537a9d280280
Untagged: composetest-web:latest
Deleted: sha256:537a9d280280a459018c8148e41728c979b77509a9f516dea4c3a342ef9c3ece
{{< / highlight >}}

For some image, the output of removing image might be a little bit longer.

{{< highlight bash >}}
❯ docker image ls
REPOSITORY              TAG       IMAGE ID       CREATED       SIZE
mysql                   5.7       0018a8d83892   12 days ago   455MB
redis                   alpine    5130e5ba7d24   2 weeks ago   29.9MB
phpmyadmin/phpmyadmin   latest    f50666f18b90   5 weeks ago   517MB
❯ docker image rm 0018a8d83892
Untagged: mysql:5.7
Untagged: mysql@sha256:9202fc6bc8fa63615e6bfc0049fc660f712d164220c5c54d86519870c305ea48
Deleted: sha256:0018a8d838923d94318aa8dd3195510226b31540901a6f4c643aacec69f7ab62
Deleted: sha256:9b5d92d35c0adf139172789ce721b36094b0ae9d82f3a9177837ebb63ab64041
Deleted: sha256:a5dcf3c1f9639ef1e0cfc9327f728d2c4bf433c6788904bcdaabc9bf801b8d95
Deleted: sha256:83ece01e2f548250a37ebb1da07201331d67e06bb75391ae2e587677795fbee1
Deleted: sha256:333192b3cc1ca102332db942aa66067c455cc086b5252817b6d2085637720e76
Deleted: sha256:e9bf9bde782fb6a391e5187979c29c4691bc35c125b94b5c4f6d898028cbd5cf
Deleted: sha256:e173168faa0ae439d00aefb5164db3e21938ecd117e67777a201a838a38590de
Deleted: sha256:474640eb47375fa6f63b7b1f955d6fbfa3bb306cc7fd6c32d9f2e6ba3773b247
Deleted: sha256:e7636df6ffe8ab322bcc32131f9c40e8e2be4bb9a97586d23ce2af39ed237f76
Deleted: sha256:42886e6940007f3f2d3b9d4006508ffd1481726af1077acfb28257e51776ce97
Deleted: sha256:f18aa902264806cbc034b3fceca35740b0d33aa6f2212cfeac8d242d36425f23
Deleted: sha256:6b4c150e3167b36440e566aa26380f5326233654cb991ed437c813b9f40c81f7
{{< / highlight >}}

![Docker Image: Remove][33-d-i-rm]

### Delete All Images

Deleteing all image require just oneliner.
But it is better to see how we do it step by step.

{{< highlight bash >}}
❯ docker image ls
REPOSITORY              TAG       IMAGE ID       CREATED       SIZE
redis                   alpine    5130e5ba7d24   2 weeks ago   29.9MB
phpmyadmin/phpmyadmin   latest    f50666f18b90   5 weeks ago   517MB
{{< / highlight >}}

{{< highlight bash >}}
❯ docker images -a
REPOSITORY              TAG       IMAGE ID       CREATED       SIZE
redis                   alpine    5130e5ba7d24   2 weeks ago   29.9MB
phpmyadmin/phpmyadmin   latest    f50666f18b90   5 weeks ago   517MB
{{< / highlight >}}

{{< highlight bash >}}
❯ docker images -aq
5130e5ba7d24
f50666f18b90
{{< / highlight >}}

{{< highlight bash >}}
❯ docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
{{< / highlight >}}

{{< highlight bash >}}
❯ docker ps -aq
{{< / highlight >}}

{{< highlight bash >}}
❯ docker rmi -f $(docker images -aq)

[...long output...]
{{< / highlight >}}

{{< highlight bash >}}
❯ docker image ls
REPOSITORY   TAG       IMAGE ID   CREATED   SIZE
{{< / highlight >}}

![Docker Image: Remove All][34-d-i-rm-all]

The complete long output ias as below:

{{< highlight bash >}}
❯ docker rmi -f $(docker images -aq)
Untagged: redis:alpine
Untagged: redis@sha256:eda49a8747271c820ee64ae71b360154af91ab54e91dc382b6f4fa3ff9621b05
Deleted: sha256:5130e5ba7d244aa17bf8ad36f7a1ef08dff389090b42a25a3b6b6bcd22c4fb52
Deleted: sha256:42bcbd783d4639b1ba39e09f3d884016c035092d687b4c5f37fdb102346c9aea
Deleted: sha256:a79ae84c6cf7d817d478d73a99d158086b8d69e8dc704a73fa72da5a9b7b8810
Deleted: sha256:890fa9e354178f490a01777562516e38d3b4b516aa7d690353a7454ad4697680
Deleted: sha256:eaa19ca11530ff32146670b1d76ed56e9efba5e2ca62b1a52ab915f628371228
Deleted: sha256:329ac28098a660e2a85faea94f855bf543c729737af579b7b7ddf51ca757e4ec
Untagged: phpmyadmin/phpmyadmin:latest
Untagged: phpmyadmin/phpmyadmin@sha256:0d951ee3bec76c5d7083122f5db509ebfa6c209efc5e70f1d47af2e13a34f543
Deleted: sha256:f50666f18b90c95b8c7e3f2f93daa00666cc3a69204036881b79711332a628dd
Deleted: sha256:6093a43dad545c1cca199af423c5e21f804c85b0aed74ca6a16465071ec70c84
Deleted: sha256:856f01d6170c7187c517f7aa3e1eeefcba32bafd157cc4d9e7bb1957e7fc8087
Deleted: sha256:a3a5ecd3d3b3eda07e968ab297de28e26213797bc311411b7d0d1a5259f05ed1
Deleted: sha256:b31f7f6e341e4b687db40a6fdd410691b39ca7dd36ccfd72a75deabfb57e6d42
Deleted: sha256:a1a4ffacf41eaa427ea49fcf0d54828b949e75a63a9a35c3f9cf48cf358d1db4
Deleted: sha256:27c72d229c2ed4752111fcf6ff02db44c519dc3c584ccd3e6561a699d3eadf46
Deleted: sha256:2d15f6018ab0f65b3258a71c6711c463fd2f9766c973d1072de628874fa54774
Deleted: sha256:90027b1d1a269c12c87bf4be2de1b63a1410d45317033de1cbcd919eaf6558d6
Deleted: sha256:10958191be33d89418f39c8efd88d940381e9ad261381fe4bdee20521b17264d
Deleted: sha256:5f1f1a6749248aa7204470a421ff10eae69cd32bc823a4bab5008b3541535edf
Deleted: sha256:3c4183417a339f5f07e377bf25c8a526e9e8a40b8d8309797db5be020e9c6231
Deleted: sha256:e39d82c668fd1b729b122757bc9dee5a7d7fa7c1dbf357eb40d3ddf0e442070c
Deleted: sha256:c61d49ec3ffe439c30f49d9c503384b622cd707b3c485a152edc1d2f0a767a14
Deleted: sha256:dd5cc80bff5f1b7c5970b3e83bd3ff2d37c1664fa47445a67c9d6c29b80b9c3c
Deleted: sha256:b43ab0f805e45d47abff1115f5632e7d9f54eb0afa8ac4673ba9593633e6f473
Deleted: sha256:4feb79f80838285e4a63fc86b475ae43b369af78c827095981a903bcab16c4c2
Deleted: sha256:b94c14d92a9583071665f84d5050a63cc4a75c4b0a12580ee5862c206671fad3
Deleted: sha256:bd2fe8b74db65d82ea10db97368d35b92998d4ea0e7e7dc819481fe4a68f64cf
{{< / highlight >}}

![Docker Image: Remove All][35-d-i-rm-all]

-- -- --

### Compose Up in Detail

Consider see our configuration once again,
and see the pulling process in detail.

{{< highlight yml >}} 
services:
  db:
    image: mysql:5.7
    container_name: db
    ...
  phpmyadmin:
    image: phpmyadmin/phpmyadmin
    container_name: pma
    ...
volumes:
  dbdata:
{{< / highlight >}}

![Docker Compose: Configuration][41-dc-bat-yml]

This can be done with `compose up`,
but without running it as daemon.
This will produce this long lines.

{{< highlight bash >}}
❯ docker compose up
[+] Running 3/2
 ⠿ Network dockerngadimin_default  Created                      0.0s
 ⠿ Container db                    Created                      0.0s
 ⠿ Container pma                   Created                      0.0s
Attaching to db, pma
db   | 2023-03-21 03:25:07+00:00 [Note] [Entrypoint]: Entrypoint script for MySQL Server 5.7.41-1.el7 started.
db   | 2023-03-21 03:25:07+00:00 [Note] [Entrypoint]: Switching to dedicated user 'mysql'
db   | 2023-03-21 03:25:07+00:00 [Note] [Entrypoint]: Entrypoint script for MySQL Server 5.7.41-1.el7 started.
pma  | AH00558: apache2: Could not reliably determine the server's fully qualified domain name, using 172.20.0.3. Set the 'ServerName' directive globally to suppress this message
db   | '/var/lib/mysql/mysql.sock' -> '/var/run/mysqld/mysqld.sock'
pma  | AH00558: apache2: Could not reliably determine the server's fully qualified domain name, using 172.20.0.3. Set the 'ServerName' directive globally to suppress this message
pma  | [Tue Mar 21 03:25:07.877599 2023] [mpm_prefork:notice] [pid 1] AH00163: Apache/2.4.54 (Debian) PHP/8.1.15 configured -- resuming normal operations
pma  | [Tue Mar 21 03:25:07.877691 2023] [core:notice] [pid 1] AH00094: Command line: 'apache2 -D FOREGROUND'
db   | 2023-03-21T03:25:08.009705Z 0 [Warning] TIMESTAMP with implicit DEFAULT value is deprecated. Please use --explicit_defaults_for_timestamp server option (see documentation for more details).
db   | 2023-03-21T03:25:08.011075Z 0 [Note] mysqld (mysqld 5.7.41) starting as process 1 ...
db   | 2023-03-21T03:25:08.015344Z 0 [Note] InnoDB: PUNCH HOLE support available
db   | 2023-03-21T03:25:08.015367Z 0 [Note] InnoDB: Mutexes and rw_locks use GCC atomic builtins
db   | 2023-03-21T03:25:08.015371Z 0 [Note] InnoDB: Uses event mutexes
db   | 2023-03-21T03:25:08.015375Z 0 [Note] InnoDB: GCC builtin __atomic_thread_fence() is used for memory barrier
db   | 2023-03-21T03:25:08.015378Z 0 [Note] InnoDB: Compressed tables use zlib 1.2.12
db   | 2023-03-21T03:25:08.015383Z 0 [Note] InnoDB: Using Linux native AIO
db   | 2023-03-21T03:25:08.016453Z 0 [Note] InnoDB: Number of pools: 1
db   | 2023-03-21T03:25:08.017596Z 0 [Note] InnoDB: Using CPU crc32 instructions
db   | 2023-03-21T03:25:08.020348Z 0 [Note] InnoDB: Initializing buffer pool, total size = 128M, instances = 1, chunk size = 128M
db   | 2023-03-21T03:25:08.029770Z 0 [Note] InnoDB: Completed initialization of buffer pool
db   | 2023-03-21T03:25:08.031994Z 0 [Note] InnoDB: If the mysqld execution user is authorized, page cleaner thread priority can be changed. See the man page of setpriority().
db   | 2023-03-21T03:25:08.056231Z 0 [Note] InnoDB: Highest supported file format is Barracuda.
db   | 2023-03-21T03:25:08.072076Z 0 [Note] InnoDB: Creating shared tablespace for temporary tables
db   | 2023-03-21T03:25:08.072175Z 0 [Note] InnoDB: Setting file './ibtmp1' size to 12 MB. Physically writing the file full; Please wait ...
db   | 2023-03-21T03:25:08.101193Z 0 [Note] InnoDB: File './ibtmp1' size is now 12 MB.
db   | 2023-03-21T03:25:08.102053Z 0 [Note] InnoDB: 96 redo rollback segment(s) found. 96 redo rollback segment(s) are active.
db   | 2023-03-21T03:25:08.102063Z 0 [Note] InnoDB: 32 non-redo rollback segment(s) are active.
db   | 2023-03-21T03:25:08.102482Z 0 [Note] InnoDB: Waiting for purge to start
db   | 2023-03-21T03:25:08.152846Z 0 [Note] InnoDB: 5.7.41 started; log sequence number 12184432
db   | 2023-03-21T03:25:08.153138Z 0 [Note] InnoDB: Loading buffer pool(s) from /var/lib/mysql/ib_buffer_pool
db   | 2023-03-21T03:25:08.153353Z 0 [Note] Plugin 'FEDERATED' is disabled.
db   | 2023-03-21T03:25:08.167260Z 0 [Note] InnoDB: Buffer pool(s) load completed at 230321  3:25:08
db   | 2023-03-21T03:25:08.167432Z 0 [Note] Found ca.pem, server-cert.pem and server-key.pem in data directory. Trying to enable SSL support using them.
db   | 2023-03-21T03:25:08.167458Z 0 [Note] Skipping generation of SSL certificates as certificate files are present in data directory.
db   | 2023-03-21T03:25:08.167463Z 0 [Warning] A deprecated TLS version TLSv1 is enabled. Please use TLSv1.2 or higher.
db   | 2023-03-21T03:25:08.167466Z 0 [Warning] A deprecated TLS version TLSv1.1 is enabled. Please use TLSv1.2 or higher.
db   | 2023-03-21T03:25:08.171487Z 0 [Warning] CA certificate ca.pem is self signed.
db   | 2023-03-21T03:25:08.171537Z 0 [Note] Skipping generation of RSA key pair as key files are present in data directory.
db   | 2023-03-21T03:25:08.172057Z 0 [Note] Server hostname (bind-address): '*'; port: 3306
db   | 2023-03-21T03:25:08.172362Z 0 [Note] IPv6 is available.
db   | 2023-03-21T03:25:08.172378Z 0 [Note]   - '::' resolves to '::';
db   | 2023-03-21T03:25:08.172398Z 0 [Note] Server socket created on IP: '::'.
db   | 2023-03-21T03:25:08.173953Z 0 [Warning] Insecure configuration for --pid-file: Location '/var/run/mysqld' in the path is accessible to all OS users. Consider choosing a different directory.
db   | 2023-03-21T03:25:08.209210Z 0 [Note] Event Scheduler: Loaded 0 events
db   | 2023-03-21T03:25:08.209483Z 0 [Note] mysqld: ready for connections.
db   | Version: '5.7.41'  socket: '/var/run/mysqld/mysqld.sock'  port: 3306  MySQL Community Server (GPL)
{{< / highlight >}}

I split the output into two:

![Docker Compose: Up: Top][43-dc-up-top]

![Docker Compose: Up: Bottom][44-dc-up-bottom]

-- -- --

### Process

We can stop the process by issuing this command.

{{< highlight bash >}}
^CGracefully stopping... (press Ctrl+C again to force)
Aborting on container exit...
[+] Running 2/2
 ⠿ Container pma  Stopped                                       1.3s
 ⠿ Container db   Stopped                                       1.8s
canceled
{{< / highlight >}}

![Docker Compose: Stop][45-dc-stop]

After the process stopped,
we can check that the image is still persist.

{{< highlight bash >}}
❯ docker ps -a --format 'table {{.Image}}\t{{.Names}}\t{{.Status}}'
IMAGE                   NAMES     STATUS
phpmyadmin/phpmyadmin   pma       Exited (0) 3 minutes ago
mysql:5.7               db        Exited (0) 3 minutes ago
{{< / highlight >}}

![Docker Compose: Process][46-docker-ps]

Or even shorter

{{< highlight bash >}}
❯ docker ps -a --format 'table {{.Names}}\t{{.Status}}'
NAMES     STATUS
pma       Exited (0) 2 minutes ago
db        Exited (0) 2 minutes ago
{{< / highlight >}}

![Docker Compose: Process][49-docker-ps]

-- -- --

### Prune

We can even prune the system or the image.

#### System Prune

{{< highlight bash >}}
❯ docker system prune
WARNING! This will remove:
  - all stopped containers
  - all networks not used by at least one container
  - all dangling images
  - all dangling build cache

Are you sure you want to continue? [y/N] y
Deleted Containers:
2763d8c19f8dfbd9cf9470311ab41313142423785eed3b464f9fada4b5513038
649718f6dd723ab227d7a69428e7a76e128b3265b160167e4acf2a0e61ebd9b6

Deleted Networks:
dockerngadimin_default

Total reclaimed space: 68B
{{< / highlight >}}

#### Image Prune

{{< highlight bash >}}
❯ docker image prune
WARNING! This will remove all dangling images.
Are you sure you want to continue? [y/N] y
Total reclaimed space: 0B
{{< / highlight >}}

I think that's all.

-- -- --

### What is Next 🤔?

How about setup LAMP in classic technique.
Using the native system.
We are going to start with setting MariaDB.
Then Apache+PHP. And finally test with PHPMyAdmin.

Consider continue reading [ [LAMP - MariaDB Setup][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}backend/2021/04/01/lamp-overview/

[33-d-i-rm]:            {{< baseurl >}}assets/posts/backend/2021/03/33-d-i-rm.png
[34-d-i-rm-all]:        {{< baseurl >}}assets/posts/backend/2021/03/34-d-i-rm-all.png
[35-d-i-rm-all]:        {{< baseurl >}}assets/posts/backend/2021/03/35-d-i-rm-all.png
[41-dc-bat-yml]:        {{< baseurl >}}assets/posts/backend/2021/03/41-dc-bat-yml.png
[43-dc-up-top]:         {{< baseurl >}}assets/posts/backend/2021/03/43-dc-up-top.png
[44-dc-up-bottom]:      {{< baseurl >}}assets/posts/backend/2021/03/44-dc-up-bottom.png
[45-dc-stop]:           {{< baseurl >}}assets/posts/backend/2021/03/45-dc-stop.png
[46-docker-ps]:         {{< baseurl >}}assets/posts/backend/2021/03/46-docker-ps.png
[47-dc-up-d]:           {{< baseurl >}}assets/posts/backend/2021/03/47-dc-up-d.png
[48-dc-stop]:           {{< baseurl >}}assets/posts/backend/2021/03/48-dc-stop.png
[49-docker-ps]:         {{< baseurl >}}assets/posts/backend/2021/03/49-docker-ps.png
