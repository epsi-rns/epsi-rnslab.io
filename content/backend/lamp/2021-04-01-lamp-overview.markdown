---
type   : post
title  : "LAMP - Overview"
date   : 2021-04-01T12:49:15+07:00
slug   : lamp-overview
categories: [backend]
tags      : [lamp, openrc]
keywords  : [docker, damp stack, artix, openrc]
author : epsi
opengraph:
  image: assets/posts/case/2023/01-usman/11-bebek-gemes.jpg

toc    : "toc-2021-03-lamp-stack"

excerpt:
  Setting-up LAMP stack with Artix OpenRC.
  Overview.

---

### Preface

> Goal: Setting-up LAMP stack with Artix OpenRC.
> Overview.

LAMP is an infrastructure,
in which you can build your super duper application, on top of it.
To setup your data center,
you need to place your physical or virtual machine. 
Then put OS on that machine, then add services on that system,
so that your machine can act as a server.

The question is, how to setup these services?
We have LAMP for daily usage in native system,
XAMPP for development in windows,
and even DAMP using docker.

How about setup LAMP in classic technique.
Using the native system.
We are going to start with setting MariaDB.
Then Apache+PHP. And finally test with PHPMyAdmin.

-- -- --

### Package Manager and Service

> Diversity Consideration

Most of the tutorial related to docker and LAMP stack,
is available with most widely used system,
with my my rough guess is about these two:

1. Package Manager: Mostly used is Debian based APT and Redhat Family.

2. Init: Widely adopted is using systemd.

I decide not to make,
just yet another APT+systemd tutorial.
But rather by giving a chance for diversity,
using Pacman+OpenRC.

#### Package Manager

> Disclaimer

This material utilize pacman.
Your package manager might need different packages.
Your linux distribution might use different package manager.

#### Service

> Disclaimer

This material utilize OpenRC.
Init in your system might utilize different command.
The systemd is the most widely used init system in modern Linux distributions.

-- -- --

### Complete Stack

I have already make an article about LAMP in 2015 and 2017.

* [Setup LAMP stack with Manjaro OpenRC][local-manjaro-openrc]

This time I want to update differently.
Instead of make the whole LAMP stack in just one article.
I'd rather separate one by one.

These days, we have options.
Such as you might use MySQL compatible,
but you might use flask instead of laravel.
So we installing PHP is not required.

You might even heard new terminology,
such as MEAN stack, JAM stack, SSG and so on.

But you might still need PHPMyAdmin to manage your database.
Thus you still the whole LAMP stack anyway.

So here I am deciding to rewrite,
my obsolete LAMP article into these article series.

#### Complete Install

You can install all packages by issuing this command:

{{< highlight bash >}}
❯ sudo pacman -S apache mariadb php php-apache phpmyadmin apache-openrc mariadb-openrc
{{< / highlight >}}

![LAMP OpenRC: Pacman: Install All][01-pacman-all]

However, this article series choose to install one by one.
Each installation separated in different topic.

-- -- --

### What is Next 🤔?

Enough with lecture.
Let's go directly into setup.

Consider continue reading [ [LAMP - MariaDB Setup][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}backend/2021/04/03/lamp-mariadb-setup/

[local-manjaro-openrc]: {{< baseurl >}}backend/2015/10/16/lamp-stack-manjaro-openrc/

[01-pacman-all]:        {{< baseurl >}}assets/posts/backend/2021/04/01-pacman-all.png
