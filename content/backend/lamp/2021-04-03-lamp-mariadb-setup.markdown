---
type   : post
title  : "LAMP - MariaDB Setup"
date   : 2021-04-03T12:49:15+07:00
slug   : lamp-mariadb-setup
categories: [backend]
tags      : [lamp, openrc]
keywords  : [docker, damp stack, artix, openrc]
author : epsi
opengraph:
  image: assets/posts/case/2023/01-usman/11-bebek-gemes.jpg

toc    : "toc-2021-03-lamp-stack"

excerpt:
  Setting-up LAMP stack with Artix OpenRC.
  Configure MariaDB.

---

### Preface

> Goal: Setting-up LAMP stack with Artix OpenRC.
> Configure MariaDB.

MariaDB is MySQL compatible.
So we use a lot of mysql reference here.
Such as data path, service name and such.

-- -- --

### Install MariaDB Packages

Just like any other stack we start with installing.
Instead of ready to use docker, this time we install in system.

* mariadb
* mariadb-openrc

{{< highlight bash >}}
❯ sudo pacman -S mariadb mariadb-openrc
resolving dependencies...
looking for conflicting packages...

Packages (2) mariadb-10.11.2-1
             mariadb-openrc-20210505-2

Total Installed Size:  207.89 MiB

:: Proceed with installation? [Y/n] y
(2/2) checking keys in keyring                    
(2/2) checking package integrity                  
(2/2) loading package files                       
(2/2) checking for file conflicts                 
(2/2) checking available disk space               
:: Processing package changes...
{{< / highlight >}}

![LAMP OpenRC: Pacman: Install MariaDB Packages][11-pacman-mariadb-01]

What we need to care about is,
whether the we should do additional task manually,
or just leave it.
Such as manual database setup.

{{< highlight bash >}}
(1/2) installing mariadb                          
:: You need to initialize the MariaDB data directory prior to starting
   the service. This can be done with mariadb-install-db command, e.g.:
   mariadb-install-db --user=mysql --basedir=/usr --datadir=/var/lib/mysql
Optional dependencies for mariadb
    cracklib: for cracklib plugin [installed]
    curl: for ha_s3 plugin [installed]
    galera: for MariaDB cluster with Galera WSREP
    judy: for Open Query GRAPH (OQGraph) computation
    engine
    perl-dbd-mariadb: for mariadb-hotcopy,
    mariadb-convert-table-format and
    mariadb-setpermission
    python-mysqlclient: for myrocks_hotbackup
    xz: lzma provider [installed]
{{< / highlight >}}

![LAMP OpenRC: Pacman: Install MariaDB Packages][11-pacman-mariadb-02]

And so init related stuff.
Depend on the policy,
some system require us to do stuff manually,
for flexibility reason.

{{< highlight bash >}}
(2/2) installing mariadb-openrc                   
:: Running post-transaction hooks...
(1/3) Creating system user accounts...
(2/3) Creating temporary files...
(3/3) Displaying openrc service help ...
	==> Add a service to runlevel:
	rc-update add <service> <runlevel>
	==> Start/stop/restart a service:
	rc-service <service> <start/stop/restart>
{{< / highlight >}}

![LAMP OpenRC: Pacman: Install MariaDB Packages][11-pacman-mariadb-03]

-- -- --

### Setup Database

This is my favorite.
We actually can setup the database,
with our own parameter.

{{< highlight bash >}}
❯ sudo mariadb-install-db \
  --user=mysql \
  --basedir=/usr \
  --datadir=/var/lib/mysql
{{< / highlight >}}

{{< highlight bash >}}
Installing MariaDB/MySQL system tables in '/var/lib/mysql' ...
OK

To start mariadbd at boot time you have to copy
support-files/mariadb.service to the right place for your system
{{< / highlight >}}

![LAMP OpenRC: Setup Database][13-install-01-short]

The complete response is here.
I guess you understand the reason why,
I don't dump them all in this article.

![LAMP OpenRC: Setup Database][13-install-01-compl]

If your database is already exist,
the command would response differently.

![LAMP OpenRC: Setup Database][13-install-02]

-- -- --

### Running Service

Init handling is a must have knowledge in linux system.
Init diversity is optional, depend on your taste,
and distribution flavour.
For OpenRC we have this two commands:

* rc-update,
* rc-service

We need to set that the mysql started automatically,
at the default runlevel.
We can reach this runlevel default after boot,
or depend on your init management.

{{< highlight bash >}}
❯ sudo rc-update add mysql default
 * service mysql added to runlevel default
{{< / highlight >}}

![LAMP OpenRC: Service: Running Service][12-rc-update-add]

Of course the mysql is still not running,
unless we have booted the system.
That is why we have to start manually.

{{< highlight bash >}}
❯ sudo rc-service mysql status
 ERROR! MariaDB is not running
{{< / highlight >}}

Now let's get it started.

 {{< highlight bash >}}
❯ sudo rc-service mysql start
Starting MariaDB.230321 17:40:51 mysqld_safe Logging to '/var/lib/mysql/andalan.err'.
230321 17:40:51 mysqld_safe Starting mariadbd daemon with databases from /var/lib/mysql
 SUCCESS! 
❯ sudo rc-service mysql status
 SUCCESS! MariaDB running (32473)
{{< / highlight >}}

![LAMP OpenRC: Service: Running Service][12-rc-service-start]

We are sooo.. up and running...

-- -- --

### Hard Reset

If you don't have any necessary data,
you can reset you database.

Sometimes it is necessary for new system.
Just to make sure that a process is repeatable.

{{< highlight bash >}}
❯ sudo rc-service mysql stop
Shutting down MariaDB. SUCCESS! 
❯ sudo rm -rf /var/lib/mysql/
{{< / highlight >}}

![LAMP OpenRC: Hard Reset: Hard Reset][13-reset]

If you start, you will have an error.
Of course, because we have no setup yet.

![LAMP OpenRC: Hard Reset: Hard Reset][13-start]

You will have to do setup procedure again.

{{< highlight bash >}}
❯ sudo mariadb-install-db \
    --user=mysql \
    --basedir=/usr \
    --datadir=/var/lib/mysql
{{< / highlight >}}

-- -- --

### Socks Issue

I was once encounter this sock issue.

{{< highlight bash >}}
❯ mariadb
ERROR 2002 (HY000): Can't connect to local server through socket '/run/mysqld/mysqld.sock' (2)
{{< / highlight >}}

![LAMP OpenRC: Socks Issue][14-mysqld-sock]

All I need to do is restart the service to get the sock.

![LAMP OpenRC: Socks Issue][14-mysql-restart]

No need to logout or even reboot.

-- -- --

### Version

> Test

_How I would I know that MariaDB works in my machine?_

If everything fine,
your MariaDB should show the versionn.

{{< highlight bash >}}
❯ mariadb --version
mariadb  Ver 15.1 Distrib 10.11.2-MariaDB, for Linux (x86_64) using readline 5.1
{{< / highlight >}}

![LAMP OpenRC: Test: MariaDB Version][14-version]

We are ready.

-- -- --

### What is Next 🤔?

Now it is time to examine SQL in MariaDB.

Consider continue reading [ [LAMP - MariaDB Prompt][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}backend/2021/04/05/lamp-mariadb-prompt/

[local-manjaro-openrc]: {{< baseurl >}}backend/2015/10/16/lamp-stack-manjaro-openrc/

[11-pacman-mariadb-01]: {{< baseurl >}}assets/posts/backend/2021/04/11-pacman-mariadb-01.png
[11-pacman-mariadb-02]: {{< baseurl >}}assets/posts/backend/2021/04/11-pacman-mariadb-02.png
[11-pacman-mariadb-03]: {{< baseurl >}}assets/posts/backend/2021/04/11-pacman-mariadb-03.png
[12-rc-service-start]:  {{< baseurl >}}assets/posts/backend/2021/04/12-rc-service-start.png
[12-rc-update-add]:     {{< baseurl >}}assets/posts/backend/2021/04/12-rc-update-add.png
[13-install-01-compl]:  {{< baseurl >}}assets/posts/backend/2021/04/13-install-01-complete.png
[13-install-01-short]:  {{< baseurl >}}assets/posts/backend/2021/04/13-install-01-short.png
[13-install-02]:        {{< baseurl >}}assets/posts/backend/2021/04/13-install-02.png
[13-reset]:             {{< baseurl >}}assets/posts/backend/2021/04/13-reset.png
[13-start]:             {{< baseurl >}}assets/posts/backend/2021/04/13-start.png
[14-mysqld-sock]:       {{< baseurl >}}assets/posts/backend/2021/04/14-mysqld-sock.png
[14-mysql-restart]:     {{< baseurl >}}assets/posts/backend/2021/04/14-mysql-restart.png
[14-version]:           {{< baseurl >}}assets/posts/backend/2021/04/14-version.png
