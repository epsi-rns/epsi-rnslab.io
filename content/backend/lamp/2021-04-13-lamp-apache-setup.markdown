---
type   : post
title  : "LAMP - Apache Setup"
date   : 2021-04-13T12:49:15+07:00
slug   : lamp-apache-setup
categories: [backend]
tags      : [lamp, openrc]
keywords  : [docker, damp stack, artix, openrc]
author : epsi
opengraph:
  image: assets/posts/case/2023/01-usman/11-bebek-gemes.jpg

toc    : "toc-2021-03-lamp-stack"

excerpt:
  Setting-up LAMP stack with Artix OpenRC.
  Configure the Apache + PHP tier.

---

### Preface

> Goal: Setting-up LAMP stack with Artix OpenRC.
> Configure the Apache + PHP tier.

-- -- --

### Install Apache Packages

Just like any other service in OpenRC,
we need both packages, and the init package.

* apache
* apache-openrc

{{< highlight bash >}}
❯ sudo pacman -S apache apache-openrc
warning: apache-2.4.56-1 is up to date -- reinstalling
resolving dependencies...
looking for conflicting packages...

Packages (2) apache-2.4.56-1
             apache-openrc-20210505-2

Total Installed Size:  6.47 MiB
Net Upgrade Size:      0.01 MiB

:: Proceed with installation? [Y/n] y
(2/2) checking keys in keyring                    
(2/2) checking package integrity                  
(2/2) loading package files                       
(2/2) checking for file conflicts                 
(2/2) checking available disk space               
{{< / highlight >}}

![LAMP OpenRC: Apache: Install Apache Packages][21-pacman-apache-01]

As usual, pay attention to the message below:

{{< highlight bash >}}
:: Processing package changes...
(1/2) reinstalling apache                         
(2/2) installing apache-openrc                    
:: Running post-transaction hooks...
(1/2) Creating temporary files...
(2/2) Displaying openrc service help ...
	==> Add a service to runlevel:
	rc-update add <service> <runlevel>
	==> Start/stop/restart a service:
	rc-service <service> <start/stop/restart>
{{< / highlight >}}

![LAMP OpenRC: Apache: Install Apache Packages][21-pacman-apache-02]

-- -- --

### Running Service

For OpenRC we have this two commands:

* rc-update,
* rc-service

{{< highlight bash >}}
❯ sudo rc-update add httpd default
 * service httpd added to runlevel default
{{< / highlight >}}

![LAMP OpenRC: Apache: Running Service][22-rc-update-add]

{{< highlight bash >}}
❯ sudo rc-service httpd status
 * status: stopped
❯ sudo rc-service httpd start
httpd             | * Starting httpd ...        [ ok ]
❯ sudo rc-service httpd status
 * status: crashed
{{< / highlight >}}

![LAMP OpenRC: Apache: Running Service][22-rc-service-start]

Woat? It crashed.

#### CURL

Strange, that it works, even though the init say it crashed.
And I still don't know why.

{{< highlight html >}}
❯ curl http://localhost
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<html>
 <head>
  <title>Index of /</title>
 </head>
 <body>
<h1>Index of /</h1>
  <table>
   <tr><th valign="top"><img src="/icons/blank.gif" alt="[ICO]"></th><th><a href="?C=N;O=D">Name</a></th><th><a href="?C=M;O=A">Last modified</a></th><th><a href="?C=S;O=A">Size</a></th><th><a href="?C=D;O=A">Description</a></th></tr>
   <tr><th colspan="5"><hr></th></tr>
   <tr><th colspan="5"><hr></th></tr>
</table>
</body></html>
~ 
{{< / highlight >}}

![LAMP OpenRC: Apache: CURL localhost][24-curl-localhost]

You can also pipe the output to vim as
`curl http://localhost | vim -R -`.

-- -- --

### Configure HTTPD

You might have a situation where you find this server's fully qualified domain name error.

{{< highlight bash >}}
❯ sudo rc-service httpd start
httpd             | * Caching service dependencies ...                                                  [ ok ]
httpd             | * Starting httpd ...
httpd             |AH00558: httpd: Could not reliably determine the server's fully qualified domain name, using 127.0.1.1. Set the 'ServerName' directive globally to suppress this message
httpd             |httpd (pid 5849) already running 
{{< / highlight >}}

We just need to add Servername in confgiuration.

{{< highlight bash >}}
# epsi!
ServerName localhost
{{< / highlight >}}

![LAMP OpenRC: Apache: Configure HTTPD][25-conf-httpd]

-- -- --

#### Browser

_How I would I know that Apache works in localhost?_

Consider have a look at
how the web server response goes in browser.

![LAMP OpenRC: Apache: Browser][24-qb-localhost]

This web server works well.

-- -- --

### What is Next 🤔?

We can contiune our tier to PHP.

Consider continue reading [ [LAMP - PHP Setup][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}backend/2021/04/15/lamp-php-setup/

[21-pacman-apache-01]:  {{< baseurl >}}assets/posts/backend/2021/04/21-pacman-apache-01.png
[21-pacman-apache-02]:  {{< baseurl >}}assets/posts/backend/2021/04/21-pacman-apache-02.png
[22-rc-service-start]:  {{< baseurl >}}assets/posts/backend/2021/04/22-rc-service-start.png
[22-rc-update-add]:     {{< baseurl >}}assets/posts/backend/2021/04/22-rc-update-add.png
[24-curl-localhost]:    {{< baseurl >}}assets/posts/backend/2021/04/24-curl-localhost.png
[24-qb-localhost]:      {{< baseurl >}}assets/posts/backend/2021/04/24-qb-localhost.png
[25-conf-httpd]:        {{< baseurl >}}assets/posts/backend/2021/04/25-conf-httpd.png