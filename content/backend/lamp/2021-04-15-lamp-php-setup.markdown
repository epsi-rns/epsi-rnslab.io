---
type   : post
title  : "LAMP - PHP Setup"
date   : 2021-04-15T12:49:15+07:00
slug   : lamp-php-setup
categories: [backend]
tags      : [lamp, openrc]
keywords  : [docker, damp stack, artix, openrc]
author : epsi
opengraph:
  image: assets/posts/case/2023/01-usman/11-bebek-kechil.jpg

toc    : "toc-2021-03-lamp-stack"

excerpt:
  Setting-up LAMP stack with Artix OpenRC.
  Configure the Apache + PHP tier.

---

### Preface

> Goal: Setting-up LAMP stack with Artix OpenRC.
> Configure the Apache + PHP tier.

-- -- --

### Install PHP Packages

We do not need any other service handler.
But we need to tie the apache with php.

* php
* php-apache

{{< highlight bash >}}
❯ sudo pacman -S php php-apache
resolving dependencies...
looking for conflicting packages...

Packages (2) php-8.2.4-1  php-apache-8.2.4-1

Total Installed Size:  41.00 MiB

:: Proceed with installation? [Y/n] y
{{< / highlight >}}

![LAMP OpenRC: PHP: Install PHP Packages][31-pacman-php-01]

{{< highlight bash >}}
(2/2) checking keys in keyring                    
(2/2) checking package integrity                  
(2/2) loading package files                       
(2/2) checking for file conflicts                 
(2/2) checking available disk space               
:: Processing package changes...
(1/2) installing php                              
(2/2) installing php-apache 
{{< / highlight >}}

![LAMP OpenRC: PHP: Install PHP Packages][31-pacman-php-02]

-- -- --

### Configure PHP Library

This is not a PHP configuration.
But rather talk about a HTTPD configuration.

The php library name has changed from time to time.
I can find the name of this php library here.

{{< highlight bash >}}
❯ ls /usr/lib/httpd/modules | grep php
libphp.so
{{< / highlight >}}

For my basic usage, I mostly use this configuration.

{{< highlight bash >}}
# epsi!

LoadModule mpm_prefork_module modules/mod_mpm_prefork.so
LoadModule php_module modules/libphp.so
LoadModule rewrite_module modules/mod_rewrite.so
{{< / highlight >}}

![LAMP OpenRC: PHP: Configure PHP Library][32-conf-httpd-01]

### Configure PHP Module

Since we need to take care different sites.
We need to manage which modules to be added to HTTPD.

{{< highlight bash >}}
❯ ls /etc/httpd/conf/extra/
httpd-autoindex.conf  httpd-languages.conf	     httpd-ssl.conf	 phpmyadmin.conf
httpd-dav.conf	      httpd-manual.conf		     httpd-userdir.conf  proxy-html.conf
httpd-default.conf    httpd-mpm.conf		     httpd-vhosts.conf
httpd-info.conf       httpd-multilang-errordoc.conf  php_module.conf
{{< / highlight >}}

![LAMP OpenRC: PHP: Configure PHP Module][32-conf-ls]

We can add this basic module here.

{{< highlight bash >}}
# epsi!
Include conf/extra/php_module.conf
{{< / highlight >}}

![LAMP OpenRC: PHP: ][32-conf-httpd-02]

You can have a look at the file, and compare.

![LAMP OpenRC: PHP: ][32-extra-php-module]

I think it is definitely easier than my last 2015 article.
Just one line, and the PHP just up and running.

-- -- --

### PHP Info

> Again, test!

_How I would I know that PHP works?_

I like the idea to use phpinfo() in my localhost.
But be aware that using this in production is bad bad idea,
as it leads to security holes,
when somebody know what's inside your machine.

Consider make a script in `www` directory.
This `www` directory might be different,
for different linux distribution

{{< highlight bash >}}
❯ sudo touch /srv/http/phpinfo.php
{{< / highlight >}}

![LAMP OpenRC: PHP: PHP Info][33-touch-phpinfo]

And add this line using any text editor.

{{< highlight php >}}
<?php phpinfo();
{{< / highlight >}}

![LAMP OpenRC: PHP: PHP Info][33-vim-phpinfo]

Don;t worry.
I won't push you to use Vim.
You can use any text ediotr.

#### Browser

Now you can open your [localhost](http://localhost).

![LAMP OpenRC: PHP: Browser][34-qb-localhost]

And click into [localhost/phpinfo.php](http://localhost/phpinfo.php).

![LAMP OpenRC: PHP: Browser][34-qb-phpinfo]

It works perfectly.
At least on my machine.

-- -- --

### What is Next 🤔?

Finally what we need.
A tools to manage our database.

Consider continue reading [ [LAMP - PHPMyAdmin][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}backend/2021/04/17/lamp-phpmyadmin/

[31-pacman-php-01]:     {{< baseurl >}}assets/posts/backend/2021/04/31-pacman-php-01.png
[31-pacman-php-02]:     {{< baseurl >}}assets/posts/backend/2021/04/31-pacman-php-02.png
[32-conf-httpd-01]:     {{< baseurl >}}assets/posts/backend/2021/04/32-conf-httpd-01.png
[32-conf-httpd-02]:     {{< baseurl >}}assets/posts/backend/2021/04/32-conf-httpd-02.png
[32-conf-ls]:           {{< baseurl >}}assets/posts/backend/2021/04/32-conf-ls.png
[32-extra-php-module]:  {{< baseurl >}}assets/posts/backend/2021/04/32-extra-php-module.png
[33-touch-phpinfo]:     {{< baseurl >}}assets/posts/backend/2021/04/33-touch-phpinfo.png
[33-vim-phpinfo]:       {{< baseurl >}}assets/posts/backend/2021/04/33-vim-phpinfo.png
[34-qb-localhost]:      {{< baseurl >}}assets/posts/backend/2021/04/34-qb-localhost.png
[34-qb-phpinfo]:        {{< baseurl >}}assets/posts/backend/2021/04/34-qb-phpinfo.png
