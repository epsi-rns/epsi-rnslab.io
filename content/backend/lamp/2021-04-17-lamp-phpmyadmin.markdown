---
type   : post
title  : "LAMP - PHPMyAdmin"
date   : 2021-04-17T12:49:15+07:00
slug   : lamp-phpmyadmin
categories: [backend]
tags      : [lamp, openrc]
keywords  : [docker, damp stack, artix, openrc]
author : epsi
opengraph:
  image: assets/posts/case/2023/01-usman/11-bebek-kechil.jpg

toc    : "toc-2021-03-lamp-stack"

excerpt:
  Setting-up LAMP stack with Artix OpenRC.
  Manage database with PHPMyAdmin.

---

### Preface

> Goal: Setting-up LAMP stack with Artix OpenRC.
> Manage database with PHPMyAdmin.

Finally what we need.
A tools to manage our database.

This can also be used to test,
whether the LAMP stack works well or not.

-- -- --

### Install PHPMyAdmin Package

We only need one package:
* phpmyadmin

{{< highlight bash >}}
❯ sudo pacman -S phpmyadmin
warning: phpmyadmin-5.2.1-1 is up to date -- reinstalling
resolving dependencies...
looking for conflicting packages...

Packages (1) phpmyadmin-5.2.1-1

Total Installed Size:  47.94 MiB
Net Upgrade Size:       0.00 MiB

:: Proceed with installation? [Y/n] y
{{< / highlight >}}

![LAMP OpenRC: PHPMyAdmin: Install PHPMyAdmin Package][41-pacman-myadmin-02]

-- -- --

### HTTPD Configuration

If we open [localhost/phpmyadmin](http://localhost/phpmyadmin).
This is what we got.

![LAMP OpenRC: PHPMyAdmin: HTTPD Configuration][44-qb-forbidden]

We need to setup this file.

{{< highlight bash >}}
❯ sudo touch /etc/httpd/conf/extra/phpmyadmin.conf

Alias /phpmyadmin "/usr/share/webapps/phpMyAdmin"
<Directory "/usr/share/webapps/phpMyAdmin">
    DirectoryIndex index.php
    AllowOverride All
    Options FollowSymlinks
    Require all granted
</Directory>
{{< / highlight >}}

![LAMP OpenRC: PHPMyAdmin: HTTPD Configuration][46-extra-phpmyadmin]

And add this line to httpd configuration.

{{< highlight bash >}}
# epsi!
# phpMyAdmin configuration
Include conf/extra/phpmyadmin.conf
{{< / highlight >}}

![LAMP OpenRC: PHPMyAdmin: HTTPD Configuration][45-conf-httpd]

And do not forget to restart the service

{{< highlight bash >}}
❯ sudo rc-service httpd restart
httpd             | * Stopping httpd ...                                                                [ ok ]
httpd             | * Starting httpd ...  
{{< / highlight >}}

![LAMP OpenRC: PHPMyAdmin: HTTPD Configuration][45-rc-service-restart]

-- -- --

### PHP Configuration

Again, if we open [localhost/phpmyadmin](http://localhost/phpmyadmin).
This is what we got.

![LAMP OpenRC: PHPMyAdmin: PHP Configuration][44-qb-mysqli]

All we need to do is to uncomment this line in PHP configuration.

{{< highlight bash >}}
extension=mysqli
{{< / highlight >}}

![LAMP OpenRC: PHPMyAdmin: PHP Configuration][45-ini-mysqli]

Then restart the service

-- -- --

### Browser

Now we can login into PHPMyAdmin.
![LAMP OpenRC: PHPMyAdmin: Browser][46-qb-login]

Our PHPMyAdmin should works well right now.

![LAMP OpenRC: PHPMyAdmin: Browser][46-qb-database]

-- -- --

### Tips

> Playground

If you have time.
You should also see the designer page in phpmyadmin for this `mysql` table.

-- -- --

### What is Next 🤔?

After the sytstem, we can use SQL in practical way.

Consider continue reading [ [Soccer - Overview][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}backend/2021/05/01/soccer-overview/

[41-pacman-myadmin-01]: {{< baseurl >}}assets/posts/backend/2021/04/41-pacman-phpmyadmin-01.png
[41-pacman-myadmin-02]: {{< baseurl >}}assets/posts/backend/2021/04/41-pacman-phpmyadmin-02.png
[44-qb-forbidden]:      {{< baseurl >}}assets/posts/backend/2021/04/44-qb-forbidden.png
[44-qb-mysqli]:         {{< baseurl >}}assets/posts/backend/2021/04/44-qb-mysqli.png
[45-conf-httpd]:        {{< baseurl >}}assets/posts/backend/2021/04/45-conf-httpd.png
[45-ini-mysqli]:        {{< baseurl >}}assets/posts/backend/2021/04/45-ini-mysqli.png
[45-rc-service-restart]:{{< baseurl >}}assets/posts/backend/2021/04/45-rc-service-restart.png
[46-extra-phpmyadmin]:  {{< baseurl >}}assets/posts/backend/2021/04/46-extra-phpmyadmin.png
[46-qb-database]:       {{< baseurl >}}assets/posts/backend/2021/04/46-qb-database.png
[46-qb-login]:          {{< baseurl >}}assets/posts/backend/2021/04/46-qb-login.png