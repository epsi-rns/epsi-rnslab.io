---
type   : post
title  : "Soccer - SQLAlchemy - Two"
date   : 2021-05-09T12:49:15+07:00
slug   : soccer-sqlalchemy-02
categories: [backend]
tags      : [sql]
keywords  : [SQLite, SQLAlchemy, query, python]
author : epsi
opengraph:
  image: assets/posts/case/2023/01-usman/11-bebek-kechil.jpg

toc    : "toc-2021-04-sql"

excerpt:
  Playing SQL with Soccer Case. SQLAlchemy ORM.

---

### Preface

> Goal: Playing SQL with Soccer Case. SQLAlchemy ORM.

Instead of regular statement,
we can use reflection with ORM.

-- -- --

### Simple Reflection

There is other way to connect, using table reflection.

{{< highlight python >}}
people = Table('People', metadata, autoload=True)
{{< / highlight >}}

* [github.com/.../sqlalchemy/soccer/04-soccer.py][py-04-soccer]

The complete preparation is as below:

{{< highlight python >}}
from sqlalchemy import (
  create_engine, MetaData, Table)
from tabulate import tabulate

str_conn = "sqlite+pysqlite:///./soccer.db"
engine   = create_engine(str_conn)

metadata = MetaData()
metadata.reflect(bind=engine)

people = Table('People', metadata, autoload=True)
{{< / highlight >}}

![SQAlchemy: Using reflection table][24-reflect-bind]

Now we can build statement using ORM.
For example, I want to find somebody with sepcific ID.

{{< highlight python >}}
with engine.connect() as conn:
  stmt = people.select().where(people.c.id == 7)
  result = conn.execute(stmt)

  row = result.fetchall()
  print(row)

  ks  = result.keys() 
  print(tabulate(row, headers=ks, 
    tablefmt='rounded_outline'))
{{< / highlight >}}

![SQAlchemy: Fecth result using ORM][24-reflect-result]

#### Result in CLI

The result is as below box.

{{< highlight bash >}}
❯ python 04-soccer.py
[(7, 'Vladimir Ivanov', 'vladimir.ivanov@example.com', 15, 'Male')]
╭──────┬─────────────────┬─────────────────────────────┬───────┬──────────╮
│   id │ name            │ email                       │   age │ gender   │
├──────┼─────────────────┼─────────────────────────────┼───────┼──────────┤
│    7 │ Vladimir Ivanov │ vladimir.ivanov@example.com │    15 │ Male     │
╰──────┴─────────────────┴─────────────────────────────┴───────┴──────────╯
{{< / highlight >}}

All the result can be seen here.

![SQAlchemy: Tabulate Result][24-tabulate-result]

-- -- --

### Reflection with Join

From this small soccer topic,
we can dig out a few more examples.

#### Source Code

The source code can be obtained here:

* [github.com/.../sqlalchemy/soccer/07a-soccer.py][py-07a-soccer]

#### Engine

As usual... prepare the engine.

{{< highlight python >}}
from sqlalchemy import create_engine
from sqlalchemy import MetaData, Table, select
from tabulate import tabulate

# create a SQLite engine
str_conn = "sqlite+pysqlite:///./soccer.db"
engine   = create_engine(str_conn)
{{< / highlight >}}

![SQAlchemy: Reflection: Engine][27-a-engine]

#### Metadata

This time we have three tables.

{{< highlight python >}}
# create a SQLAlchemy metadata object
metadata = MetaData()
metadata.reflect(bind=engine)

# reflect the existing tables
people = Table('People',
  metadata, autoload=True)
resps  = Table('Responsibilities',
  metadata, autoload=True)
staff  = Table('StaffResps',
  metadata, autoload=True)
{{< / highlight >}}

![SQAlchemy: Reflection: Metadata][27-a-reflect]

#### Statement

With table reflections above,
we can build our SQL statement.

{{< highlight python >}}
stmt = people.select().\
  join(staff, people.c.id == staff.c.person_id).\
  join(resps, resps.c.id == staff.c.resp_id).\
  where(resps.c.name == 'Doctor');
{{< / highlight >}}

![SQAlchemy: Reflection: Statement][27-a-statement]

#### Fetch

Now we can fetch as usual,
and feed the result into `tabulate` library.

{{< highlight python >}}
with engine.connect() as conn:
  result = conn.execute(stmt)

  ks = result.keys()
  ra = result.fetchall()

  print(tabulate(ra, headers=ks))
{{< / highlight >}}

![SQAlchemy: Reflection: Fetch][27-a-fetch]

#### Result in CLI

The result is as below box.

{{< highlight bash >}}
❯ python 07a-soccer.py
  id  name              email                       age  gender
----  ----------------  ------------------------  -----  --------
   4  dr. Komal Sharma  komal.sharma@example.com     26  Female
  27  dr. Johnson Bun   dr.johnson@example.com       50  Male
{{< / highlight >}}

![SQAlchemy: Tabulate Result][27-tabulate-result-a]

-- -- --

### Join Statement

We can write above statement differently.

#### Source Code

The source code can be obtained here:

* [github.com/.../sqlalchemy/soccer/07b-soccer.py][py-07b-soccer]

#### Engine

The engine is the same as above script.

#### Metadata

{{< highlight python >}}
metadata = MetaData()
metadata.reflect(bind=engine)

people = metadata.tables['People']
resps  = metadata.tables['Responsibilities']
staff  = metadata.tables['StaffResps']
{{< / highlight >}}

![SQAlchemy: Reflection: Metadata][27-b-reflect]

#### Statement

{{< highlight python >}}
join_stmt = people
join_stmt = join_stmt.join(staff,
  people.c.id == staff.c.person_id)
join_stmt = join_stmt.join(resps,
  resps.c.id == staff.c.resp_id)

stmt = select(people.c.email, resps.c.name)
stmt = stmt.select_from(join_stmt)
stmt = stmt.where(resps.c.name == 'Doctor');
{{< / highlight >}}

![SQAlchemy: Reflection: Statement][27-b-statement]

#### Fetch

The fetch code is the same as previous script.

#### Result in CLI

The result is as below box.

{{< highlight bash >}}
❯ python 07b-soccer.py
email                     name
------------------------  ------
komal.sharma@example.com  Doctor
dr.johnson@example.com    Doctor
{{< / highlight >}}

![SQAlchemy: Tabulate Result][27-tabulate-result-b]

-- -- --

### What is Next 🤔?

We need to explore more about SQL statement.

Consider continue reading [ [Soccer - SQL Alchemy - Part Three][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}backend/2021/05/11/soccer-sqlalchemy-03/

[24-reflect-bind]:      {{< baseurl >}}assets/posts/backend/2021/05/24-reflect-bind.png
[24-reflect-result]:    {{< baseurl >}}assets/posts/backend/2021/05/24-reflect-result.png
[24-tabulate-result]:   {{< baseurl >}}assets/posts/backend/2021/05/24-tabulate-result.png

[27-a-engine]:          {{< baseurl >}}assets/posts/backend/2021/05/27-a-engine.png
[27-a-fetch]:           {{< baseurl >}}assets/posts/backend/2021/05/27-a-fetch.png
[27-a-reflect]:         {{< baseurl >}}assets/posts/backend/2021/05/27-a-reflect.png
[27-a-statement]:       {{< baseurl >}}assets/posts/backend/2021/05/27-a-statement.png
[27-b-engine]:          {{< baseurl >}}assets/posts/backend/2021/05/27-b-engine.png
[27-b-fetch]:           {{< baseurl >}}assets/posts/backend/2021/05/27-b-fetch.png
[27-b-reflect]:         {{< baseurl >}}assets/posts/backend/2021/05/27-b-reflect.png
[27-b-statement]:       {{< baseurl >}}assets/posts/backend/2021/05/27-b-statement.png
[27-tabulate-result-a]: {{< baseurl >}}assets/posts/backend/2021/05/27-tabulate-result-a.png
[27-tabulate-result-b]: {{< baseurl >}}assets/posts/backend/2021/05/27-tabulate-result-b.png

[//]: <> ( -- -- -- links below -- -- -- )

[py-04-soccer]:         {{< codecase >}}/python/sqlalchemy/soccer/04-soccer.py

[py-07a-soccer]:        {{< codecase >}}/python/sqlalchemy/soccer/07a-soccer.py
[py-07b-soccer]:        {{< codecase >}}/python/sqlalchemy/soccer/07b-soccer.py


