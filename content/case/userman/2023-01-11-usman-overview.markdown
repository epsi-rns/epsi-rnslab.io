---
type   : post
title  : "User - Overview"
date   : 2023-01-11T09:17:35+07:00
slug   : user-man-overview
categories: [case]
tags      : [requirement, UML]
keywords  : [Vision Document, SRS, UML Diagram, Artefact]
author : epsi
opengraph:
  image: assets/posts/case/2023/01-usman/11-bebek-gemes.jpg

toc    : "toc-2023-01-case-user-man"

excerpt:
  A simple user management showcase,
  step by step with help of ChatGPT.
  Starting from Requirement.
  Read the review first.

---

### Preface

> Goal: Starting from Requirement.

The hype of ChatGPT lead me into my longing past.
First about using proper requirement document,
and also about creating my own site admin.

ChatGPT give us courage to start.
Knowing that we have friend to ask about stuff,
other than rubber duck.
But keep in mind,
the fact that ChatGPT is a good assistance,
does not make it a good teacher.

In order to utilize ChatGPT,
we need to ask the right question.
I start a project from requirement.
In about two hours,
I have complete all the requirement artefact,
along with basic diagram.
So here I'm starting a new project.

![ChatGPT: Use Case Diagram][11-23-tahun]

-- -- --

### Reflection from the Past

> Why not?

Sure I've been introduced to UML, about 23 years ago,
from a friend in college from psychology faculty who need pair discussion.
I also get a book about *managing requirement*,
about 15 years ago from post graduate student.
So yeah I know about vison document
along with SRS (Software requirement specification),
since long time ago.
But I never have any real project that require,
those artefact.

I have made a project along with site admin,
using symfony2 in last 2011.
It is about a decade ago. The project is complete,
but It has never been used anywhere.
During that time, the hardest part is the site admin.
So I know how important to make a guidance for beginner,
to make site admin, starting from user management,
along with login page.
So why don't I recreate my own project.

Unfortunately, I never have a job related with IT.
I've been drifting away from coding,
and get busy with something else,
I'm to scared to get back to learn this stuff,
because I feel like I'm responsible with family business.

Last december, I got a call, then I move to another job.
This job is related to device.
My engineering approach is to start with requirement document.
And this device also need a dashboard.
And in the same time, ChatGPT come to help as an assistance.
My manager also give me green signal.
A blurry one, but enough for me to take courage to start.

> So why not?

-- -- --

### The Riddler?

> Asking the right question.

I finally found something to talk to,
better than my duck rubber.

![Rubber Duck: Plekcing: Bebek Gemes][11-bebek-gemes]

> you can try it yourself

ChatGPT is a good companion.
For any project a discussion become productive,
if you can just, ask the right question.

For example for **site admin**,
here is my actual interview,
ordered chronologically.

{{< notes title="Questions?" >}}
* What is the proper requirement for user management in site admin?

* Can this above be written in UML fashioned?

* How does it looks like in database tabel and field?

* Again, please rewrite the database table and field, in UML fashioned.

* Would you mind write the UML down in SQL?

* I need a basic data example.
  Please give me a fixture of above table.

* Again, rewrite in SQL style.

* How about index, in SQL?

* Can you make a vision document for this case?

* How about SRS?

* Would you mind drawing an ERD of above database design?

* Is there any other artefact that I need beside UML, ERD, SQL, vision document, and SRS for this case?

* I wonder how a use case diagram looks like for this case.

* How would the actor looks like. in that diagram. Can you draw it for me?

* How about class diagram?

* You know what? You are great bro! Really.

* How about Sequence Diagram?

* I never write a proper test before.
  What do you mean by Test Plan?
  And give me example for this case.

* Any diagram for test?

* Do you mind explain about deployment for this case.

* I wonder how the visualization looks like in a deployment diagram.

* Do I require to consider any constraint for this requirement?

* Is there any detail for User authentication?
{{< / notes >}}

The next day, I found that ChatGPT can produce PlantUML code.
So we can generate the UML diagram online.
So I ask all over again.

{{< notes title="Questions?" >}}
* Can you make use case diagram in SVG?
* Would you mind writing down the above requirements for user management in a site admin in PlantUML format for use with draw.io?
* Would you mind writing this down into PlantUML format?
* Would you mind writing this down into PlantUML format?
* Please write this textual diagram below into PlantUML format.
* Would you mind writing this down into PlantUML format?
* Would you mind writing down the above Sequence Diagrams for user management in a site admin in PlantUML format for use with draw.io?
* Would you mind writing this sequence diagram below into PlantUML format?
* If you do not mind, please help me write down both test diagram above, the test case diagram and sequence diagram in PlantUML format for use with draw.io?
* One last thing about requirement artefact. Please help me write down deployment diagram in PlantUML format.
{{< / notes >}}

I keep this chronological order, for tracebility reason.

> That's all.

This article series would not be presented in chronological order.
But rather by topic systematically,
such as concept first, then diagram and detail later.

I preserve the implementation, such as coding,
in other ChatGPT discussion.

-- -- --

### Artefact Rearrangement

> Let's get organized

For convenience, The topic should be rearranged.
So we can write down the answer in systematic approach.

#### Requirement

{{< notes title="Questions?" >}}
* What is the proper requirement for user management in site admin?

* Can you make a vision document for this case?

* How about SRS?

* Can this above be written in UML fashioned?

* Would you mind writing this down into PlantUML format?

* You know what? You are great bro! Really.

{{< / notes >}}

#### Use Case Diagram

{{< notes title="Questions?" >}}
* Is there any other artefact that I need beside UML, ERD, SQL, vision document, and SRS for this case?

* Do I require to consider any constraint for this requirement?

* I wonder how a use case diagram looks like for this case.

* How would the actor looks like. in that diagram. Can you draw it for me?

* Can you make use case diagram in SVG?

* Would you mind writing down the above requirements for user management in a site admin in PlantUML format for use with draw.io?

{{< / notes >}}

#### Class Diagram

{{< notes title="Questions?" >}}

* Again, please rewrite the database table and field, in UML fashioned.

* Would you mind writing this down into PlantUML format?

* Would you mind drawing an ERD of above database design?

* Please write this textual diagram below into PlantUML format.

* How about class diagram?

* Would you mind writing this down into PlantUML format?

{{< / notes >}}

#### Sequence Diagram

{{< notes title="Questions?" >}}
* How about Sequence Diagram?

* Would you mind writing this sequence diagram below into PlantUML format?

* Would you mind writing down the above Sequence Diagrams for user management in a site admin in PlantUML format for use with draw.io?

{{< / notes >}}

#### Testing Artefact

{{< notes title="Questions?" >}}
* I never write a proper test before.
  What do you mean by Test Plan?
  And give me example for this case.

* Any diagram for test?

* If you do not mind, please help me write down both test diagram above, the test case diagram and sequence diagram in PlantUML format for use with draw.io?

{{< / notes >}}

#### Deployment artefact

{{< notes title="Questions?" >}}
* Do you mind explain about deployment for this case.

* I wonder how the visualization looks like in a deployment diagram.

* One last thing about requirement artefact. Please help me write down deployment diagram in PlantUML format.

{{< / notes >}}

#### Initial SQL

{{< notes title="Questions?" >}}
* How does it looks like in database table and field?

* I need a basic data example.
  Please give me a fixture of above table.

{{< / notes >}}

{{< notes title="Questions?" >}}
* Would you mind write the UML down in SQL?

* Again, rewrite in SQL style.

* How about index, in SQL?

* Is there any detail for User authentication?

{{< / notes >}}

-- -- --

### I Might be Wrong

Sure I've been using ChatGPT for a while.
And it turned out that ChatGPT can make serious mistakes.

The same applied for document.
As an assistance, do not assume that ChatGPT is right.
You have to test your implementation.

My approach is, to write down the answer as is.
And modify later in implementation part in this article series.

-- -- --

### Begin The Discussion

Let's get the ChatGPT started.

Consider continue reading [ [User - Requirement][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}case/2023/01/13/user-man-requirement/

[11-23-tahun]:          {{< baseurl >}}assets/posts/case/2023/01-usman/11-23-tahun.png

[11-bebek-gemes]:       {{< baseurl >}}assets/posts/case/2023/01-usman/11-bebek-gemes.jpg
