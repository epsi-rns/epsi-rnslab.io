---
type   : post
title  : "User - Use Case Diagram"
date   : 2023-01-15T09:17:35+07:00
slug   : user-man-use-case
ccategories: [case]
tags      : [requirement, UML]
keywords  : [Vision Document, SRS, UML Diagram, Artefact]
author : epsi
opengraph:
  image: assets/posts/case/2023/01-usman/svg/15-usman-use-case.png

toc    : "toc-2023-01-case-user-man"

excerpt:
  A simple user management showcase,
  step by step with help of ChatGPT.
  Starting from Requirement.
  Use case diagram.

---

### Preface

> Goal: Starting from Requirement.

We should move on to other artefact as well as below questions:

{{< notes title="Questions?" >}}
* Is there any other artefact that I need beside UML, ERD, SQL, vision document, and SRS for this case?

* Do I require to consider any constraint for this requirement?

* I wonder how a use case diagram looks like for this case.

* How would the actor looks like. in that diagram. Can you draw it for me?

* Can you make use case diagram in SVG?

* Would you mind writing down the above requirements for user management in a site admin in PlantUML format for use with draw.io?

{{< / notes >}}

Consider examine one, by one.

-- -- --

### Other Artefacts

Other related diagram has been asked to ChatGPT as below questions:

{{< notes title="Questions?" >}}
Is there any other artefact that I need beside UML, ERD, SQL, 
Vision Document, and SRS for this case?
{{< / notes >}}

{{< notes title="Response" color="blue" >}}
Yes, there are a few other artifacts that you might need for this case:

1. **Use Case Diagram**:
   A use case diagram is a visual representation of the interactions between users (actors) and the system. It can help you understand how the different actors will interact with the system and what the system needs to do to support those interactions.

2. **Class Diagram**:
   A class diagram is a visual representation of the classes, interfaces, and objects in the system. It can help you understand the relationships between different parts of the system and how they interact with each other.

3. **Sequence Diagram**:
   A sequence diagram is a visual representation of the interactions between objects in the system. It can help you understand the flow of data and control between different parts of the system.

4. **Test Plan**:
   A test plan is a document that outlines the testing approach, strategies, and methods for the system. It can help you ensure that the system is tested thoroughly and meets all of the requirements specified in the SRS.

5. **Deployment Diagram**:
    A deployment diagram is a visual representation of the hardware and software components in the system and how they are deployed. It can help you understand the infrastructure needed to support the system.

These artifacts can help you understand and document different aspects of the system, and ensure that it meets the needs of your users.
{{< / notes >}}

If seems to be there are few thing I miss.
We need more explanation,
I will start from class diagram in this article,
followed by use case diagram and sequence diagram next article.
Test and deployment would require special section in this article series.

-- -- --

### Constraint

> Other Consideration

{{< notes title="Questions?" >}}
Do I require to consider any constraint for this requirement?
{{< / notes >}}

{{< notes title="Response" color="blue" >}}
Yes, it's important to consider any constraints that may impact the implementation or use of the User Management System. Here are some examples of possible constraints:

* **Technology constraints**: The User Management System may need to be built using specific programming languages or frameworks due to existing infrastructure or other technical considerations.
* **Security constraints**: The User Management System may need to comply with specific security standards or regulations, such as HIPAA or GDPR.
* **Performance constraints**: The User Management System may need to be designed to handle a certain amount of traffic or user load to ensure that it can handle peak usage times without downtime or slowdowns.
* **Budget constraints**: The User Management System may need to be built within a specific budget, which could impact choices around technology, resources, and development timelines.
* **Timeline constraints**: The User Management System may need to be delivered within a specific timeframe, which could impact the level of complexity or customization that can be included in the initial release.
* **Accessibility constraints**: The User Management System may need to be accessible to users with disabilities, which could require specific design considerations and coding practices to be followed.

By considering these and other constraints, you can help ensure that the User Management System is designed and implemented in a way that meets the needs of stakeholders while also taking into account any limitations or requirements that may impact the project.
{{< / notes >}}

I know how boring it looks like reading above string.
Instead of textual, we can examine the diagram right away.

-- -- --

### Use Case Diagram

Starting from Use Case.

{{< notes title="Questions?" >}}
I wonder how a use case diagram looks like for this case.
{{< / notes >}}

{{< notes title="Response" color="blue" >}}
Sure, here is an example of a use case diagram for the user management system:
{{< / notes >}}

{{< highlight bash >}}
+----------------------------------------------------+
|                 User Management System             |
+----------------------------------------------------+
|                  <<use case diagram>>              |
+----------------------------------------------------+
| [Admin]                                            |
| - Manage Users                                     |
| - Manage Roles                                     |
| - Manage Permissions                               |
| - Assign Roles to Users                            |
| - Assign Permissions to Roles                      |
|                                                    |
| [User]                                             |
| - Login                                            |
| - View Profile                                     |
+----------------------------------------------------+
{{< / highlight >}}

{{< notes color="blue" >}}
### Explanation:

* The use case diagram shows the different actors (Admin and User) and their interactions with the system.
* The Admin actor can perform five different use cases: Manage Users, Manage Roles, Manage Permissions, Assign Roles to Users, and Assign Permissions to Roles.
* The User actor can perform two different use cases: Login and View Profile.
* Each use case is represented as an oval shape with the name of the use case inside.
* The actors are represented as stick figures, with their name above the figure.
* The use cases are connected to the actors with lines, indicating that the actors are involved in those use cases.
{{< / notes >}}

It is still unclear to me, so I ask for the second time.

{{< notes title="Questions?" >}}
How would the actor looks like. in that diagram. Can you draw it for me?
{{< / notes >}}

{{< notes title="Response" color="blue" >}}
Sure, here is an example of what the actors might look like in the use case diagram:
{{< / notes >}}

{{< highlight bash >}}
+----------------------------------------------------+
|                 User Management System             |
+----------------------------------------------------+
|                  <<use case diagram>>              |
+----------------------------------------------------+
| [Admin]                                            |
| - Manage Users                                     |
| - Manage Roles                                     |
| - Manage Permissions                               |
| - Assign Roles to Users                            |
| - Assign Permissions to Roles                      |
|                                                    |
| [User]                                             |
| - Login                                            |
| - View Profile                                     |
+----------------------------------------------------+

            +--------+                   +--------+
            | Admin  |                   |  User  |
            +--------+                   +--------+
{{< / highlight >}}

{{< notes color="blue" >}}
Explanation:

* The actors are represented as stick figures with their name written above them.
* The Admin actor is on the left, and the User actor is on the right.
* The use cases are connected to the appropriate actor using lines.
{{< / notes >}}

I finally get those actors.

#### Diagram as Image

> Rewrite Diagram from Text to Image

I don't why the actor in use case diagram has very big head.
But I draw them anyway.

![User Management: UML: Use Case Diagram][15-png-use-case]

You can obtain the schematic source of PlantUML diagram here:

* [UML in SVG: Use Case Diagram][15-svg-use-case]

Not sure. I don't know, what I'm doing.

-- -- --

#### Simple PlantUML

Consider examine generated PlantUML from ChatGPT.
Then compare with my SVG design above.

{{< notes title="Questions?" >}}
Can you make use case diagram in SVG?
{{< / notes >}}

{{< notes title="Response" color="blue" >}}
Unfortunately, as a language model, I don't have the capability to create images in SVG format. However, I can provide you with the use case diagram in a textual format that you can then use to create an SVG diagram using a tool like draw.io or Lucidchart:
{{< / notes >}}

{{< highlight cpp >}}
@startuml
actor User
actor Admin
usecase Register
usecase Login
usecase Logout
usecase View Profile
usecase Edit Profile
usecase Create User (Admin)
usecase Edit User (Admin)
usecase Delete User (Admin)

User --> Register
User --> Login
User --> Logout
User --> View Profile
User --> Edit Profile
Admin --> Create User (Admin)
Admin --> Edit User (Admin)
Admin --> Delete User (Admin)
@enduml
{{< / highlight >}}

{{< notes color="blue" >}}
This code is written in PlantUML format and can be used to generate the use case diagram in a variety of formats, including SVG, PNG, and PDF. You can copy and paste this code into a PlantUML editor or an online converter tool to generate an SVG diagram.
{{< / notes >}}

The code above won't work, because it use space without double quote. So I make a few tweak, and voila. The code works.

The generated diagram from [draw.io](draw.io) can be shown as below

![User Management: PlantUML: Simple Use Case Diagram][15-drawio-use-case-01]

You can obtain the schematic source of PlantUML diagram here:

* [PlantUML: Simple Use Case Diagram][15-puml-use-case-01]

-- -- --

#### Better PlantUML

Since it is far different from my SVG.
I ask again.

{{< notes title="Questions?" >}}
Would you mind writing down the above requirements for user management in a site admin in PlantUML format for use with draw.io?
{{< / notes >}}

{{< notes title="Response" color="blue" >}}
Sure! Here's the PlantUML code for the User Management System requirements:
{{< / notes >}}

{{< highlight cpp >}}
@startuml
title User Management System Requirements

actor User
actor Admin

rectangle "User Management System" {
  rectangle "User Management" {
    usecase "Register"
    usecase "Login"
    usecase "Logout"
    usecase "View Profile"
    usecase "Edit Profile"
  }
  
  rectangle "Admin Management" {
    usecase "Create User (Admin)"
    usecase "Edit User (Admin)"
    usecase "Delete User (Admin)"
  }

  User --> "User Management"
  Admin --> "User Management"
  Admin --> "Admin Management"
}

@enduml
{{< / highlight >}}

{{< notes color="blue" >}}
You can copy and paste this code into the text editor of draw.io and then use the "File" -> "Export As" menu option to export the diagram in SVG format.
{{< / notes >}}

The code above would work with a little trick.

The generated diagram from [draw.io](draw.io) can be shown as below

![User Management: PlantUML: Better Use Case Diagram][15-drawio-use-case-02]

You can obtain the source SVG image here:

* [PlantUML: Better Use Case Diagram][15-puml-use-case-02]

Since it is far different from my SVG.
I ask again.

By comparation, It is far better than my diagram.

-- -- --

### What is Next 🤔?

Remember these tree basic diagrams:
* Use Case Diagram
* Class Diagram
* Sequence Diagram

Do not just jump into coding,
it is pretty bad idea.
Consider draw, at least these three for any project.

Consider continue reading [ [User - Class Diagram][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}case/2023/01/17/user-man-class/

[15-png-use-case]:      {{< baseurl >}}assets/posts/case/2023/01-usman/svg/15-usman-use-case.png
[15-svg-use-case]:      {{< baseurl >}}assets/posts/case/2023/01-usman/svg/15-usman-use-case.svg

[15-drawio-use-case-01]:{{< baseurl >}}assets/posts/case/2023/01-usman/puml/15-use-case-01.drawio.png
[15-drawio-use-case-02]:{{< baseurl >}}assets/posts/case/2023/01-usman/puml/15-use-case-02.drawio.png

[15-puml-use-case-01]:  {{< baseurl >}}assets/posts/case/2023/01-usman/puml/15-use-case-01.puml
[15-puml-use-case-02]:  {{< baseurl >}}assets/posts/case/2023/01-usman/puml/15-use-case-02.puml
