---
type   : post
title  : "User - Class Diagram"
date   : 2023-01-17T09:17:35+07:00
slug   : user-man-class
ccategories: [case]
tags      : [requirement, UML]
keywords  : [Vision Document, SRS, UML Diagram, Artefact]
author : epsi
opengraph:
  image: assets/posts/case/2023/01-usman/svg/17-usman-class-specification.png

toc    : "toc-2023-01-case-user-man"

excerpt:
  A simple user management showcase,
  step by step with help of ChatGPT.
  Starting from Requirement.
  Class diagram.

---

### Preface

> Goal: Starting from Requirement.

Basic class diagram and also ERD,
has been asked to ChatGPT as below questions:

{{< notes title="Questions?" >}}
* Again, please rewrite the database table and field, in UML fashioned.

* Would you mind writing this down into PlantUML format?

* Would you mind drawing an ERD of above database design?

* Please write this textual diagram below into PlantUML format.

* How about class diagram?

* Would you mind writing this down into PlantUML format?

{{< / notes >}}

For quick and dirty solution, 
again, one question at a time, please.

-- -- --

### Database

{{< notes title="Questions?" >}}
Again, please rewrite the database table and field, in UML fashioned.
{{< / notes >}}

{{< notes title="Response" color="blue" >}}
Here is an example of how the database tables and fields for a user management system in a site admin could be represented using UML class diagrams:
{{< / notes >}}

{{< highlight bash >}}
+--------+                  +-------+                  +--------------+
|  User  |                  |  Role |                  |  Permission  |
+--------+                  +-------+                  +--------------+
|   id   |                  |   id  |                  |      id      |
|   ...  |                  |  name |                  |     name     |
+--------+                  +-------+                  +--------------+
     |                            |                            |
     +----------------+           +----------------+           |
                      |                            |           |
             +---------------------+         +------------------+
             | User_Role_Permission |        | Role_Permission  |
             +---------------------+         +------------------+
             |         user_id     |         |     role_id      |
             |         role_id     |         |  permission_id   |
             |     permission_id   |         +------------------+
             +---------------------+
{{< / highlight >}}

{{< notes color="blue" >}}
In this UML class diagram, we have three main entities: User, Role, and Permission. Each entity has one or more attributes (represented by the properties in each class box). For example, the User entity has an id attribute, as well as other unspecified attributes represented by ....

The relationships between entities are represented by the associations between the classes. For example, the User entity has a many-to-many relationship with both the Role and Permission entities, which are represented by the User_Role_Permission and Role_Permission association classes. These association classes are used to represent the many-to-many relationship and can have additional attributes or methods as needed.

Note that this is just one possible way to represent the database tables and fields using UML. The actual structure will depend on the specific requirements and design of the user management system.
{{< / notes >}}

#### Conceptual Diagram

> Rewrite Diagram from Text to Image

All I can do is guess.
And I guess I can interpret the ChatGPT response above to image below:

![User Management: UML: Class Diagram: Role_Permission][17-png-class-table-01]
![User Management: UML: Class Diagram: User_Role_Permission][17-png-class-table-02]

You can obtain both SVG sources here:

* [UML in SVG: Class Diagram: Role_Permission][17-svg-class-table-01]
* [UML in SVG: Class Diagram: User_Role_Permission][17-svg-class-table-02]

I know, I alter a few things.
* Agregation attribute.
* One-to-many instead of many-to-many.

I might be wrong though.

#### PlantUML

Again, ask using given textual UML.

{{< notes title="Questions?" >}}
Would you mind writing this down into PlantUML format?

```
  ...
  ...
```
{{< / notes >}}

{{< notes title="Response" color="blue" >}}
Sure! Here's the PlantUML code for the User, Role, Permission and User_Role_Permission classes and their relationships:
{{< / notes >}}

{{< highlight cpp >}}
@startuml
title User Management System ER Diagram

class User {
  +id: int
  ...
}

class Role {
  +id: int
  +name: string
}

class Permission {
  +id: int
  +name: string
}

class User_Role_Permission {
  +user_id: int
  +role_id: int
  +permission_id: int
}

User --> "*" User_Role_Permission
Role --> "*" User_Role_Permission
Permission --> "*" User_Role_Permission
@enduml
{{< / highlight >}}

{{< notes color="blue" >}}
You can copy and paste this code into a PlantUML editor or an online converter tool to generate the ER diagram.
{{< / notes >}}

The generated diagram from [draw.io](draw.io) can be shown as below

![User Management: PlantUML: Class Diagram: Main][17-drawio-class-02]

You can obtain the schematic source of PlantUML diagram here:

* [PlantUML: Class Diagram][17-puml-class-02]

>Not bad.

By comparation, my SVG and PlantUML seems similar.

-- -- --

### Entity Relationship Diagram

> Don not be confused with Engineering Requirement Document (ERD).

{{< notes title="Questions?" >}}
Would you mind drawing an ERD of above database design?
{{< / notes >}}

{{< notes title="Response" color="blue" >}}
Sure, here is an ERD (Entity-Relationship Diagram) for the user management system database design:
{{< / notes >}}

{{< highlight bash >}}
                                          +-----------+
                                          |   User    |
                                          +-----------+
                                          |PK: user_id|
                                          +-----------+
                                                |
                                                |
                          +---------------------+-------------------+
                          |                                           |
                    +-----------+                                +-----------+
                    |   Role    |                                | Permission|
                    +-----------+                                +-----------+
                    |PK: role_id|                                |PK: perm_id|
                    +-----------+                                +-----------+
                          |                                           |
                          |                                           |
      +-------------------+---------------------+       +------------+-------------+
      |                                         |       |                          |
+------------------+                +----------------+----------------+    +--------------+
|UserRole          |                | RolePermission |                |    |RolePermission|
+------------------+                +----------------+                |    +--------------+
|PK: user_role_id  |                |PK: role_perm_id|                |    |PK: rp_id     |
|FK: user_id       |                |FK: role_id     |                |    |FK: role_id   |
|FK: role_id       |                |FK: perm_id     |                |    |FK: perm_id   |
+------------------+                +----------------+                +-----------------+
{{< / highlight >}}

{{< notes color="blue" >}}
### Explanation:

* The diagram shows four entities: User, Role, Permission, and UserRole.
* The User entity represents the users of the system and has a primary key (PK) of user_id.
* The Role entity represents the roles that can be assigned to users and has a PK of role_id.
* The Permission entity represents the permissions that can be assigned to roles and has a PK of perm_id.
* The UserRole entity is a join table between the User and Role entities and has a PK of user_role_id. It also has foreign keys (FKs) referencing user_id and role_id.
* The RolePermission entity is a join table between the Role and Permission entities and has a PK of role_perm_id. It also has FKs referencing role_id and perm_id.
* There is also a self-referencing relationship between the RolePermission entity, allowing for the assignment of permissions to other permissions. This is represented by the RolePermission entity having a FK referencing its own PK (FK: rp_id).
{{< / notes >}}

#### Conceptual Diagram

ERD is not part of UML standard.
Before we go on.. I want to say...

> Oh my cat... I don't get it....

I simply do not understand the relationship between these boxes.
I can't interpret what I do not comprehend.

Allright. I might be wrong on this ERD.
But I draw this anyway for practice purpose.

![User Management: ERD: User_Role][17-png-erd-01]
![User Management: ERD: User_Role_Permission][17-png-erd-02]

You can obtain both SVG sources here:

* [ERD in SVG: User_Role][17-svg-erd-01]
* [ERD in SVG: User_Role_Permission][17-svg-erd-02]

Again, I alter a few things.

Again. I might be completely wrong.

#### PlantUML

> As usual, compare!

{{< notes title="Questions?" >}}
Please write this textual diagram below into PlantUML format.

```
  ...
  ...
```
{{< / notes >}}

{{< notes title="Response" color="blue" >}}
Here's the PlantUML code for the above diagram:
{{< / notes >}}

{{< highlight cpp >}}
@startuml
class User {
    +id: int
    ...
}

class Role {
    +id: int
    name: string
}

class Permission {
    +id: int
    name: string
}

User "1" --> "N" Role
Role "1" --> "N" Permission

class User_Role_Permission {
    +user_id: int
    +role_id: int
    +permission_id: int
}

User_Role_Permission "N" --> "1" User
User_Role_Permission "N" --> "1" Role
User_Role_Permission "N" --> "1" Permission

class Role_Permission {
    +role_id: int
    +permission_id: int
}

Role_Permission "N" --> "1" Role
Role_Permission "N" --> "1" Permission
@enduml
{{< / highlight >}}

{{< notes color="blue" >}}
Note that the diagram includes the class definitions for the User, Role, and Permission entities, as well as the relationship between them. The User_Role_Permission and Role_Permission classes represent the many-to-many relationships between users, roles, and permissions.
{{< / notes >}}

The generated diagram from [draw.io](draw.io) can be shown as below

![User Management: PlantUML: Entity Relationship Diagram: Main][17-drawio-entity-03]

You can obtain the schematic source of PlantUML diagram here:

* [PlantUML: Entity Relationship Diagram][17-puml-entity-03]

> Too bad.

By comparation, I can't even analyze,
whether it is similar or not.

-- -- --

### Class Diagram

{{< notes title="Questions?" >}}
How about class diagram?
{{< / notes >}}

{{< notes title="Response" color="blue" >}}
Here is an example of a class diagram for the user management system:
{{< / notes >}}

{{< highlight bash >}}
+----------------------------------------------------+
|                 User Management System             |
+----------------------------------------------------+
|                 <<class diagram>>                  |
+----------------------------------------------------+
|                        User                        |
+----------------------------------------------------+
| - id: int                                          |
| - username: string                                 |
| - password: string                                 |
| - email: string                                    |
| - created_at: datetime                             |
| - updated_at: datetime                             |
+----------------------------------------------------+
|                        Role                        |
+----------------------------------------------------+
| - id: int                                          |
| - name: string                                     |
| - description: string                              |
| - created_at: datetime                             |
| - updated_at: datetime                             |
+----------------------------------------------------+
|                   Permission                       |
+----------------------------------------------------+
| - id: int                                          |
| - name: string                                     |
| - description: string                              |
| - created_at: datetime                             |
| - updated_at: datetime                             |
+----------------------------------------------------+
|                  UserRole                          |
+----------------------------------------------------+
| - user_id: int                                     |
| - role_id: int                                     |
+----------------------------------------------------+
|            RolePermission                          |
+----------------------------------------------------+
| - role_id: int                                     |
| - permission_id: int                               |
+----------------------------------------------------+
{{< / highlight >}}

{{< notes color="blue" >}}
### Explanation:

* The class diagram shows the different classes in the system and their attributes.
* There are four classes: User, Role, Permission, UserRole, and RolePermission.
* Each class has its attributes listed below it.
* The User class has id, username, password, email, created_at, and updated_at attributes.
* The Role class has id, name, description, created_at, and updated_at attributes.
* The Permission class has id, name, description, created_at, and updated_at attributes.
* The UserRole class has user_id and role_id attributes, which represent a many-to-many relationship between users and roles.
* The RolePermission class has role_id and permission_id attributes, which represent a many-to-many relationship between roles and permissions.
{{< / notes >}}

#### Specification Diagram

Since the diagram above contain the type,
I must assume that, this is not a conceptual diagram,
but rather a specification diagram.

> I still don't get it....

But I draw my interpretation anyway.

![User Management: Class Specification Diagram][17-png-specification]

You can obtain the SVG source here:

* [User Management: Class Specification Diagram][17-svg-specification]

I must admit. I'm not sure.

I should test the schema in database,
so I can be sure, whether it is workable or not.

#### PlantUML

> As usual, compare!

{{< notes title="Questions?" >}}
Would you mind writing this down into PlantUML format?

```
  ...
  ...
```
{{< / notes >}}

{{< notes title="Response" color="blue" >}}
Sure! Here's the PlantUML code for the class diagram:
{{< / notes >}}

{{< highlight cpp >}}
@startuml
title User Management System
skinparam class {
    BackgroundColor<<class diagram>> Cornsilk
    ArrowColor Black
    BorderColor Black
}

class User {
    - id: int
    - username: string
    - password: string
    - email: string
    - created_at: datetime
    - updated_at: datetime
}

class Role {
    - id: int
    - name: string
    - description: string
    - created_at: datetime
    - updated_at: datetime
}

class Permission {
    - id: int
    - name: string
    - description: string
    - created_at: datetime
    - updated_at: datetime
}

class UserRole {
    - user_id: int
    - role_id: int
}

class RolePermission {
    - role_id: int
    - permission_id: int
}

User -- UserRole
Role -- UserRole
Role -- RolePermission
Permission -- RolePermission
@enduml
{{< / highlight >}}

{{< notes color="blue" >}}
I've also included some skin parameters to make the diagram look more like a class diagram. You can adjust these to your liking.
{{< / notes >}}

The generated diagram from [draw.io](draw.io) can be shown as below

![User Management: PlantUML: Class Diagram: Main][17-drawio-class-04]

You can obtain the schematic source of PlantUML diagram here:

* [PlantUML: Class Diagram][17-puml-class-04]

> Too good to be true.

By comparation, I think it is very similar with mine.
Now I got my confidence back.

-- -- --

### What is Next 🤔?

We still have a lot of diagram.
From sequence diagram, test diagram, and deployment.

Consider continue reading [ [User - Sequence Diagram][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}case/2023/01/19/user-man-sequence/

[17-png-class-table-01]:{{< baseurl >}}assets/posts/case/2023/01-usman/svg/17-usman-class-tables-01.png
[17-svg-class-table-01]:{{< baseurl >}}assets/posts/case/2023/01-usman/svg/17-usman-class-tables-01.svg
[17-png-class-table-02]:{{< baseurl >}}assets/posts/case/2023/01-usman/svg/17-usman-class-tables-02.png
[17-svg-class-table-02]:{{< baseurl >}}assets/posts/case/2023/01-usman/svg/17-usman-class-tables-02.svg

[17-png-erd-01]:        {{< baseurl >}}assets/posts/case/2023/01-usman/svg/17-usman-erd-01.png
[17-svg-erd-01]:        {{< baseurl >}}assets/posts/case/2023/01-usman/svg/17-usman-erd-01.svg
[17-png-erd-02]:        {{< baseurl >}}assets/posts/case/2023/01-usman/svg/17-usman-erd-02.png
[17-svg-erd-02]:        {{< baseurl >}}assets/posts/case/2023/01-usman/svg/17-usman-erd-02.svg

[17-png-specification]: {{< baseurl >}}assets/posts/case/2023/01-usman/svg/17-usman-class-specification.png
[17-svg-specification]: {{< baseurl >}}assets/posts/case/2023/01-usman/svg/17-usman-class-specification.svg

[//]: <> ( -- -- -- links below -- -- -- )

[17-drawio-class-02]:   {{< baseurl >}}assets/posts/case/2023/01-usman/puml/17-class-diagram-02.drawio.png
[17-drawio-entity-03]:  {{< baseurl >}}assets/posts/case/2023/01-usman/puml/17-er-diagram-03.drawio.png
[17-drawio-class-04]:   {{< baseurl >}}assets/posts/case/2023/01-usman/puml/17-class-diagram-04.drawio.png

[17-puml-class-02]:     {{< baseurl >}}assets/posts/case/2023/01-usman/puml/17-class-diagram-02.puml
[17-puml-entity-03]:    {{< baseurl >}}assets/posts/case/2023/01-usman/puml/17-er-diagram-03.puml
[17-puml-class-04]:     {{< baseurl >}}assets/posts/case/2023/01-usman/puml/17-class-diagram-04.puml

