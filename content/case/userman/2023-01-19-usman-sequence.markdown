---
type   : post
title  : "User - Sequence Diagram"
date   : 2023-01-19T09:17:35+07:00
slug   : user-man-sequence
ccategories: [case]
tags      : [requirement, UML]
keywords  : [Vision Document, SRS, UML Diagram, Artefact]
author : epsi
opengraph:
  image: assets/posts/case/2023/01-usman/puml/19-seq-01-login.drawio.png

toc    : "toc-2023-01-case-user-man"

excerpt:
  A simple user management showcase,
  step by step with help of ChatGPT.
  Starting from Requirement.
  Sequence diagram.

---

### Preface

> Goal: Starting from Requirement.

{{< notes title="Questions?" >}}
* How about Sequence Diagram?

* Would you mind writing this sequence diagram below into PlantUML format?

* Would you mind writing down the above Sequence Diagrams for user management in a site admin in PlantUML format for use with draw.io?

{{< / notes >}}

One question at a time.

-- -- --

### Sequence Diagram: Master

{{< notes title="Questions?" >}}
How about Sequence Diagram?
{{< / notes >}}

{{< notes title="Response" color="blue" >}}
Here's an example of a sequence diagram for the user management system:
{{< / notes >}}

{{< highlight bash >}}
+----------------------------------------------------+
|                 User Management System             |
+----------------------------------------------------+
|              <<sequence diagram>>                  |
+----------------------------------------------------+
|                                                    |
|     Admin                                          |
|        |                                           |
|        v                                           |
|   [ Manage Users ]                                 |
|        |                                           |
|        v                                           |
|  <<create>>UserController                          |
|        |                                           |
|        v                                           |
|  [ add_user(user_info) ]                           |
|        |                                           |
|        v                                           |
| <<create>>UserService                              |
|        |                                           |
|        v                                           |
| [ validate_user_info(user_info) ]                  |
|        |                                           |
|        v                                           |
|   [ Add User to Database ]                         |
|        |                                           |
|        v                                           |
|         Database                                   |
|                                                    |
+----------------------------------------------------+
{{< / highlight >}}

{{< notes color="blue" >}}
### Explanation:

* The sequence diagram shows the sequence of interactions between the actors and the system components.
* In this case, the Admin actor is interacting with the system to add a new user to the database.
* The sequence starts with the Admin invoking the "Manage Users" use case.
* The use case creates an instance of the UserController class, which is responsible for handling user-related operations.
* The UserController then calls the "add_user" method of the UserService class, which is responsible for validating user information and adding the user to the database.
* The UserService class validates the user information and then calls the "Add User to Database" operation.
* Finally, the database adds the user to the user table.

Note: This is just an example, and the specific details of the sequence diagram may vary based on the specific requirements and architecture of your system.
{{< / notes >}}

I really have no idea why the textual diagram look like above result. I guess I need the image representation.

#### PlantUML

> So I ask again.

{{< notes title="Questions?" >}}
Would you mind writing this down into PlantUML format?

```
  ...
  ...
```
{{< / notes >}}

{{< notes title="Response" color="blue" >}}
Sure, here's the PlantUML code for the sequence diagram:
{{< / notes >}}

{{< highlight cpp >}}
@startuml
title User Management System

actor Admin
Admin -> [Manage Users]: selects
[Manage Users] -> UserController: create
UserController -> [add_user(user_info)]: call
[add_user(user_info)] -> UserService: create
UserService -> [validate_user_info(user_info)]: call
[validate_user_info(user_info)] -> [Add User to Database]: call
[Add User to Database] --> Database

@enduml
{{< / highlight >}}

{{< notes color="blue" >}}
Sure, here's the PlantUML code for the sequence diagram:
{{< / notes >}}

The generated diagram from [draw.io](draw.io) can be shown as below

![User Management: PlantUML: Sequence Diagram][19-drawio-seq-master]

You can obtain the source SVG image here:

* [PlantUML: Sequence Diagram][19-puml-seq-master]

> Confused

This image doesn't look like any sequence diagram I ever draw. Is it beacuse I never have formal IT education? My lack of experience? I guess I have to dig a little deeper.

-- -- --

### Sequence Diagram: Detailed

> So I ask again.

I accidentaly ask a question that breakdown the master sequence diagram above into detail below.
For each response, the generated diagram from [draw.io](draw.io) is shown.

{{< notes title="Questions?" >}}
Would you mind writing down the above Sequence Diagrams for user management in a site admin in PlantUML format for use with draw.io?
{{< / notes >}}

{{< notes title="Response" color="blue" >}}
Sure, here is the PlantUML code for the Sequence Diagrams for User Management:
{{< / notes >}}

#### User Registration Sequence Diagram

{{< highlight cpp >}}
@startuml
title User Registration

actor User
participant "User Registration Form" as Form
participant "User Registration Controller" as Controller
participant "User Database" as DB

User -> Form : Open registration form
Form -> User : Display registration form
User -> Form : Submit registration form
Form -> Controller : Process registration form
Controller -> DB : Create user record
DB -> Controller : User created
Controller -> User : Redirect to login page
@enduml
{{< / highlight >}}

![User Management: PlantUML: Sequence Diagram: Registration][19-drawio-seq-01]

#### User Login Sequence Diagram

{{< highlight cpp >}}
@startuml
title User Login

actor User
participant "Login Form" as Form
participant "Login Controller" as Controller
participant "User Database" as DB

User -> Form : Open login form
Form -> User : Display login form
User -> Form : Submit login form
Form -> Controller : Process login form
Controller -> DB : Retrieve user record
DB -> Controller : User record retrieved
Controller -> Form : Redirect to home page
@enduml
{{< / highlight >}}

![User Management: PlantUML: Sequence Diagram: Login][19-drawio-seq-02]

#### User Logout Sequence Diagram

{{< highlight cpp >}}
@startuml
title User Logout

actor User
participant "Logout Controller" as Controller

User -> Controller : Click logout button
Controller -> Controller : Clear session data
Controller -> User : Redirect to login page
@enduml
{{< / highlight >}}

![User Management: PlantUML: Sequence Diagram: Logout][19-drawio-seq-03]

#### Update User Profile Sequence Diagram

{{< highlight cpp >}}
@startuml
title Update User Profile

actor User
participant "Profile Edit Form" as Form
participant "Profile Controller" as Controller
participant "User Database" as DB

User -> Controller : Open profile edit form
Controller -> Form : Populate form with user data
Form -> User : Display profile edit form
User -> Form : Submit profile edit form
Form -> Controller : Process profile edit form
Controller -> DB : Update user record
DB -> Controller : User record updated
Controller -> Form : Redirect to profile page
@enduml
{{< / highlight >}}

![User Management: PlantUML: Sequence Diagram: Proflle][19-drawio-seq-04]

#### Reset Password Sequence Diagram

{{< highlight cpp >}}
@startuml
title Reset Password

actor User
participant "Reset Password Form" as Form
participant "Reset Password Controller" as Controller
participant "User Database" as DB

User -> Form : Open reset password form
Form -> User : Display reset password form
User -> Form : Submit reset password form
Form -> Controller : Process reset password form
Controller -> DB : Retrieve user record
DB -> Controller : User record retrieved
Controller -> Form : Display password reset instructions
@enduml
{{< / highlight >}}

![User Management: PlantUML: Sequence Diagram: Password][19-drawio-seq-05]

#### User Activity Monitoring Sequence Diagram

{{< highlight cpp >}}
@startuml
title User Activity Monitoring

actor User
participant "Activity Log Controller" as Controller
participant "User Database" as DB

User -> Controller : Perform activity on site
Controller -> DB : Log user activity
@enduml
{{< / highlight >}}

![User Management: PlantUML: Sequence Diagram: Monitoring][19-drawio-seq-06]

You can obtain the PlantUML schematic sources here:

* [PlantUML: Sequence Diagram: Registration][19-puml-seq-01]
* [PlantUML: Sequence Diagram: Login][19-puml-seq-02]
* [PlantUML: Sequence Diagram: Logout][19-puml-seq-03]
* [PlantUML: Sequence Diagram: Profile][19-puml-seq-04]
* [PlantUML: Sequence Diagram: Password][19-puml-seq-05]
* [PlantUML: Sequence Diagram: Monitoring][19-puml-seq-06]

> Self explanatory.

Now I understand clearly.
The sequence diagram looks familiar.

-- -- --

### What is Next 🤔?

You should also consider these two in requirement:
* Testing
* Deploying

Consider continue reading [ [User - Testing Artefact][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}case/2023/01/21/user-man-test/

[19-drawio-seq-master]: {{< baseurl >}}assets/posts/case/2023/01-usman/puml/19-sequence-diagram-01-master.drawio.png
[19-drawio-seq-01]:     {{< baseurl >}}assets/posts/case/2023/01-usman/puml/19-seq-01-registration.drawio.png
[19-drawio-seq-02]:     {{< baseurl >}}assets/posts/case/2023/01-usman/puml/19-seq-02-login.drawio.png
[19-drawio-seq-03]:     {{< baseurl >}}assets/posts/case/2023/01-usman/puml/19-seq-03-logout.drawio.png
[19-drawio-seq-04]:     {{< baseurl >}}assets/posts/case/2023/01-usman/puml/19-seq-04-profile.drawio.png
[19-drawio-seq-05]:     {{< baseurl >}}assets/posts/case/2023/01-usman/puml/19-seq-05-password.drawio.png
[19-drawio-seq-06]:     {{< baseurl >}}assets/posts/case/2023/01-usman/puml/19-seq-06-monitoring.drawio.png

[19-puml-seq-master]:   {{< baseurl >}}assets/posts/case/2023/01-usman/puml/19-sequence-diagram-01-master.puml
[19-puml-seq-01]:       {{< baseurl >}}assets/posts/case/2023/01-usman/puml/19-seq-01-registration.puml
[19-puml-seq-02]:       {{< baseurl >}}assets/posts/case/2023/01-usman/puml/19-seq-02-login.puml
[19-puml-seq-03]:       {{< baseurl >}}assets/posts/case/2023/01-usman/puml/19-seq-03-logout.puml
[19-puml-seq-04]:       {{< baseurl >}}assets/posts/case/2023/01-usman/puml/19-seq-04-profile.puml
[19-puml-seq-05]:       {{< baseurl >}}assets/posts/case/2023/01-usman/puml/19-seq-05-password.puml
[19-puml-seq-06]:       {{< baseurl >}}assets/posts/case/2023/01-usman/puml/19-seq-06-monitoring.puml

