---
type   : post
title  : "User - Testing Artefact"
date   : 2023-01-21T09:17:35+07:00
slug   : user-man-test
ccategories: [case]
tags      : [requirement, UML]
keywords  : [Test, Deployment, UML Diagram, Artefact]
author : epsi
opengraph:
  image: assets/posts/case/2023/01-usman/puml/21-test-02-sequence-diagram.drawio.png

toc    : "toc-2023-01-case-user-man"

excerpt:
  A simple user management showcase,
  step by step with help of ChatGPT.
  Starting from Requirement.
  Testing Artefact.

---

### Preface

> Goal: Starting from Requirement.

{{< notes title="Questions?" >}}
* I never write a proper test before.
  What do you mean by Test Plan?
  And give me example for this case.

* Any diagram for test?

* If you do not mind, please help me write down both test diagram above, the test case diagram and sequence diagram in PlantUML format for use with draw.io?

{{< / notes >}}

-- -- --

### Test Plan

{{< notes title="Questions?" >}}
I never write a proper test before.

What do you mean by Test Plan?
And give me example for this case.
{{< / notes >}}

{{< notes title="Response" color="blue" >}}
A test plan is a document that outlines the testing strategy and approach for a particular system or software. It includes details about the scope of testing, the testing methodology, the test environment, the test schedule, the test cases, and the expected results.

Here's an example test plan for the user management system:

### Test Plan: User Management System
{{< / notes >}}

{{< notes color="blue" >}}
#### Introduction

The User Management System is a web application that allows an admin user to manage users. This test plan outlines the testing approach for the system.
{{< / notes >}}

{{< notes color="blue" >}}
#### Testing Objectives

The testing objectives for the User Management System are as follows:

* To ensure that the system meets the functional requirements specified in the SRS document.
* To ensure that the system is easy to use and provides a good user experience.
* To ensure that the system is reliable, secure, and performs well.
{{< / notes >}}

{{< notes color="blue" >}}
#### Scope

The scope of testing for the User Management System includes the following:

* Testing of all the functional requirements specified in the SRS document.
* Testing of the user interface and user experience.
* Testing of the security and performance of the system.
{{< / notes >}}

{{< notes color="blue" >}}
#### Testing Methodology

The testing methodology for the User Management System will include the following:

* Black box testing to test the functionality of the system.
* User acceptance testing to test the user interface and user experience.
* Security testing to test the security of the system.
* Performance testing to test the performance of the system under various loads.
{{< / notes >}}

{{< notes color="blue" >}}
#### Test Environment

The test environment for the User Management System will include the following:

* Operating System: Windows 10
* Web Browser: Google Chrome
* Database: MySQL
* Server: Apache Tomcat
{{< / notes >}}

{{< notes color="blue" >}}
#### Test Schedule

The test schedule for the User Management System is as follows:

* Test Planning: Week 1
* Test Case Development: Week 2
* Test Execution: Week 3 - Week 6
* Test Reporting: Week 7
{{< / notes >}}

{{< notes color="blue" >}}
#### Test Cases

The following are sample test cases for the User Management System:

##### Test Case 1: Add User

* Objective: To ensure that an admin user can add a new user to the system.
* Preconditions: The admin user is logged in to the system.
* Steps:
  * Click on "Manage Users" link.
  * Click on "Add User" button.
  * Enter the user information in the form.
  * Click on "Save" button.
* Expected Results:
  * The new user is added to the system.
  * The user is redirected to the "User List" page.
  * The new user is displayed in the user list.

##### Test Case 2: Delete User

* Objective: To ensure that an admin user can delete a user from the system.
* Preconditions: The admin user is logged in to the system.
* Steps:
  * Click on "Manage Users" link.
  * Select the user to be deleted.
  * Click on "Delete" button.
  * Confirm the deletion.
* Expected Results:
  * The user is deleted from the system.
  * The user is removed from the user list.
{{< / notes >}}

{{< notes color="blue" >}}
#### Conclusion

This test plan outlines the testing approach for the User Management System. By following this plan, we can ensure that the system meets the functional requirements, is easy to use, and is reliable, secure, and performs well.
{{< / notes >}}

-- -- --

### Test Diagram

{{< notes title="Questions?" >}}
Any diagram for test?
{{< / notes >}}

{{< notes title="Response" color="blue" >}}
Yes, there are a few diagrams that are commonly used in software testing. Here are two examples that can be used in conjunction with the test plan and test cases outlined for the User Management System:
{{< / notes >}}

{{< notes color="blue" >}}
1. Test Case Diagram

    A test case diagram is a UML diagram that shows the relationships between test cases, test suites, and the system under test. It can help visualize the test plan and ensure that all test cases are accounted for.

    Here's an example test case diagram for the User Management System:
{{< / notes >}}

{{< highlight bash >}}
+---------------+
| User Mgmt Sys |
+---------------+
         |
         |
    +----------+
    | TestSuite|
    +----------+
         |
         |
    +----------+
    | TestCase |
    +----------+
{{< / highlight >}}

{{< notes color="blue" >}}
2. Sequence Diagram

    A sequence diagram is a UML diagram that shows the interactions between objects or components in a system. It can be useful for visualizing the flow of data and actions during a particular test case.

    Here's an example sequence diagram for the "Add User" test case:
{{< / notes >}}

{{< highlight bash >}}
Admin User -> User Management System: Click "Manage Users" link
User Management System -> Admin User: Display "User List" page
Admin User -> User Management System: Click "Add User" button
User Management System -> Admin User: Display "Add User" form
Admin User -> User Management System: Enter user information
Admin User -> User Management System: Click "Save" button
User Management System -> Database: Save user information
User Management System -> Admin User: Redirect to "User List" page
User Management System -> Admin User: Display new user in user list
{{< / highlight >}}

{{< notes color="blue" >}}
These diagrams can help clarify the testing process and ensure that all necessary components are accounted for.
{{< / notes >}}

-- -- --

### PlantUML

> As usual, ask again!

Here are the answer along with the generated diagram from [draw.io](draw.io).


{{< notes title="Questions?" >}}
If you do not mind, please help me write down both test diagram above, the test case diagram and sequence diagram in PlantUML format for use with draw.io?
{{< / notes >}}

{{< notes title="Response" color="blue" >}}
Sure, here are the PlantUML formats for both the test case diagram and sequence diagram:
{{< / notes >}}

#### Test Case Diagram:

{{< highlight cpp >}}
@startuml
|User Management System|

left to right direction
skinparam packageStyle rectangle

rectangle "User Management" {
  (uc1) << (1) Login >>
  (uc2) << (2) Register >>
  (uc3) << (3) Update Profile >>
  (uc4) << (4) View Roles >>
  (uc5) << (5) Add Role >>
  (uc6) << (6) View Permissions >>
  (uc7) << (7) Add Permission >>
  (uc8) << (8) Add User Role >>
  (uc9) << (9) Remove User Role >>
  (uc10) << (10) Add Role Permission >>
  (uc11) << (11) Remove Role Permission >>
}

(1) <.. (2): include
(1) <.. (3): include
(4) <.. (5): include
(6) <.. (7): include
(8) <.. (9): include
(5) <.. (10): include
(6) <.. (11): include
@enduml
{{< / highlight >}}

![User Management: PlantUML: Test: Test Diagram][21-drawio-test-01]

#### Sequence Diagram

{{< highlight cpp >}}
@startuml
|User Management System|

actor Admin

participant UserController
participant UserService
participant Database

Admin -> [Manage Users]

[Manage Users] -> UserController: <<create>>
UserController -> UserService: <<create>>
UserController -> UserService: add_user(user_info)
UserService -> UserService: validate_user_info(user_info)
UserService -> Database: Add User to Database
@enduml
{{< / highlight >}}

![User Management: PlantUML: Test: Sequence Diagram][21-drawio-test-02]

#### PUML Source

You can obtain the schematic source of PlantUML diagram here:

* [PlantUML: Test: Test Diagram][21-puml-test-01]
* [PlantUML: Test: Sequence Diagram][21-puml-test-02]

-- -- --

### What is Next 🤔?

We are almost finished with artefact.

Consider continue reading [ [User - Deploying Artefact][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}case/2023/01/23/user-man-deploy/

[21-drawio-test-01]:    {{< baseurl >}}assets/posts/case/2023/01-usman/puml/21-test-01-test-case.drawio.png
[21-drawio-test-02]:    {{< baseurl >}}assets/posts/case/2023/01-usman/puml/21-test-02-sequence-diagram.drawio.png

[21-puml-test-01]:      {{< baseurl >}}assets/posts/case/2023/01-usman/puml/21-test-01-test-case.puml
[21-puml-test-02]:      {{< baseurl >}}assets/posts/case/2023/01-usman/puml/21-test-02-sequence-diagram.puml

