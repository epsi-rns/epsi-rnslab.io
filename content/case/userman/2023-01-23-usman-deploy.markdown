---
type   : post
title  : "User - Deployment Artefact"
date   : 2023-01-23T09:17:35+07:00
slug   : user-man-deploy
ccategories: [case]
tags      : [requirement, UML]
keywords  : [Test, Deployment, UML Diagram, Artefact]
author : epsi
opengraph:
  image: assets/posts/case/2023/01-usman/11-bebek-kechil.jpg

toc    : "toc-2023-01-case-user-man"

excerpt:
  A simple user management showcase,
  step by step with help of ChatGPT.
  Starting from Requirement.
  Deploying Artefact.

---

### Preface

> Goal: Starting from Requirement.

{{< notes title="Questions?" >}}
* Do you mind explain about deployment for this case.

* I wonder how the visualization looks like in a deployment diagram.

* One last thing about requirement artefact. Please help me write down deployment diagram in PlantUML format.

{{< / notes >}}

-- -- --

### Deployment

{{< notes title="Questions?" >}}
Do you mind explain about deployment for this case.
{{< / notes >}}

{{< notes title="Response" color="blue" >}}
Sure! Deployment refers to the process of installing, configuring, and running software applications on a server or network of servers. For the User Management System, deployment would involve taking the application code and any necessary configuration files, installing them on one or more servers, and configuring the application to run in a production environment.

Here are some key considerations for deploying the User Management System:

1. **Server Configuration**: The application should be deployed on a server that meets the minimum system requirements for the software stack it is built on (e.g. PHP, MySQL, etc.). The server should also be configured with appropriate security measures, such as firewalls and SSL certificates.

2. **Database Setup**: The User Management System relies on a MySQL database to store user data. The database should be set up on a separate server from the application server for security and scalability reasons.

3. **File System Permissions**: The application code and any associated files (e.g. configuration files, images, etc.) should be set up with appropriate file system permissions to ensure that only authorized users can access or modify them.

4. **Deployment Automation**: To ensure consistency and reduce the risk of errors during deployment, it's recommended to use deployment automation tools such as Ansible or Jenkins.

5. **Testing and Monitoring**: Once the application is deployed, it's important to test it thoroughly to ensure that it's functioning correctly and that there are no security vulnerabilities. Ongoing monitoring of the application and server infrastructure can help identify issues and ensure that the system is performing optimally.

These are just some high-level considerations for deploying the User Management System. The specific details of the deployment process will depend on factors such as the target environment, the size of the user base, and the desired level of performance and scalability.
{{< / notes >}}

Now I feel stupid.
The more I ask, the more I know that I don't know.

-- -- --

### Deployment Diagram

{{< notes title="Questions?" >}}
I wonder how the visualization looks like in a deployment diagram.
{{< / notes >}}

{{< notes title="Response" color="blue" >}}
Sure, here is an example deployment diagram for the User Management System:
{{< / notes >}}

{{< highlight bash >}}
         +------------------------+             +------------------------+
         |      Web Server        |             |      Database Server    |
         |    (Apache, PHP)       |             |        (MySQL)          |
         |                        |             |                        |
         +------------------------+             +------------------------+
                    |                                           |
                    |                                           |
                    |                                           |
                    |                                           |
                    |                                           |
         +------------------------+             +------------------------+
         |   User Management App  |             |      User Database     |
         |                        |             |                        |
         |                        |             |                        |
         |                        |<------------|                        |
         |                        |             |                        |
         |                        |             |                        |
         +------------------------+             +------------------------+
{{< / highlight >}}

{{< notes color="blue" >}}
In this diagram, there are two main components: the web server and the database server. The web server runs the User Management System application, which is composed of multiple modules, such as user authentication, user registration, and user profile management. The database server stores all user-related data, such as usernames, passwords, and profile information.

The deployment diagram shows how the different components of the system are distributed across servers. The arrows indicate the direction of data flow between components. For example, data flows from the User Management App to the User Database when a user submits their registration form.

Note that this is just one example of how the User Management System could be deployed. The actual deployment architecture would depend on factors such as the anticipated user traffic, the available server infrastructure, and the desired level of performance and reliability.
{{< / notes >}}


-- -- --

### PlantUML

{{< notes title="Questions?" >}}
One last thing about requirement artefact. Please help me write down deployment diagram in PlantUML format.
{{< / notes >}}

{{< notes title="Response" color="blue" >}}
One last thing about requirement artefact. Please help me write down deployment diagram in PlantUML format.
{{< / notes >}}

{{< highlight cpp >}}
@startuml
!define AWSPUML https://raw.githubusercontent.com/awslabs/aws-icons-for-plantuml/v20.10.23/dist
!includeurl AWSPUML/AWSCommon.puml
!includeurl AWSPUML/Compute/AmazonEC2.puml
!includeurl AWSPUML/Compute/AmazonECS.puml
!includeurl AWSPUML/Compute/AmazonLambda.puml
!includeurl AWSPUML/Database/AmazonRDS.puml
!includeurl AWSPUML/Database/AmazonDynamoDB.puml
!includeurl AWSPUML/ApplicationIntegration/AmazonSNS.puml
!includeurl AWSPUML/ApplicationIntegration/AmazonSQS.puml

node "Web Server" {
  artifact "Web App" as webapp
}

node "Application Server" {
  artifact "API Server" as apiserver
}

node "Database Server" {
  AmazonRDS(database)
}

webapp --> apiserver
apiserver --> database
@enduml
{{< / highlight >}}

{{< notes color="blue" >}}
This example includes AWS icons for EC2, ECS, Lambda, RDS, DynamoDB, SNS, and SQS. You can modify the contents of the node blocks to represent the actual servers and services in your deployment.
{{< / notes >}}

#### Other Sources

> Not working.

Unfortunately, the github shown above has been changed a lot. So the code above won't work.

Luckily we can obtain example for github above.

{{< highlight cpp >}}
@startuml

!define AWSPuml https://raw.githubusercontent.com/awslabs/aws-icons-for-plantuml/v15.0/dist
!include AWSPuml/AWSCommon.puml

node "Web Server" {
  artifact "Web App" as webapp
}

node "Application Server" {
  artifact "API Server" as apiserver
}

node "Database Server" {
  AmazonRDS(database)
}

webapp --> apiserver
apiserver --> database
@enduml
{{< / highlight >}}

The generated diagram from [draw.io](draw.io) can be shown as below

![User Management: PlantUML: Deployment Diagram: Main][23-drawio-deployment]

#### PUML Source

You can obtain the schematic source of PlantUML diagram here:

* [PlantUML: Deployment Diagram][23-puml-deployment]

I really don't know about what this diagram is talking about.
I guess I just have to save the diagram in this article and review later on.

-- -- --

### What is Next 🤔?

We are done with artefact.
Now we can think about prototype.

Consider continue reading [ [User - Initial SQL][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}case/2023/01/25/user-man-sql/

[23-drawio-deployment]: {{< baseurl >}}assets/posts/case/2023/01-usman/puml/23-deployment.drawio.png
[23-puml-deployment]:   {{< baseurl >}}assets/posts/case/2023/01-usman/puml/23-deployment.puml