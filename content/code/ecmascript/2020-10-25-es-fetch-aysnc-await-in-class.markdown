---
type   : post
title  : "ES - Fetch Async/Await in Class"
date   : 2020-10-25T13:13:35+07:00
slug   : es-fetch-async-await-in-class
categories: [code]
tags      : [cli, ecmascript]
keywords  : [fetch, json, asynchronous, request, OOP]
author : epsi
opengraph:
  image: assets/posts/code/2020/10/02-01-read-id.png

excerpt:
  A class example to fetch JSON using ES async/await.
---

### Preface

> Goal: A class example to fetch JSON using ES async/await.

We are going to run this `ecmascript` in nodejs in CLI mode.

#### Source Examples

You can obtain source examples here:

* [/cli-example][source-example]

#### What Standard 🤔?

Standard being used in this article.

1. Fat Arrow: ES 2015 (ES6)
2. Classes:   ES 2015 (ES6)
3. Const:     ES 2015 (ES6)
4. Template Strings: ES 2015 (ES6)
5. Async Function:   ES 2017 (ES8)
6. Fetch: WHATWG

`Fetch` is not an `ecmacscript` standard.
This is why in `NodeJS` you need an additional `node-fetch` package.

-- -- --

### JSON Provider

JSON is usually utilized to access API.
The API utilization increasing with JAM stack.
To start learning we need an online API example in JSON format.

#### Fake REST API

For tutorial purpose, We are going to use `jsonplaceholder`:
A fake online REST API for testing and prototyping

* <https://jsonplaceholder.typicode.com/guide/>

The first example copied from that site above is below code:

{{< highlight javascript >}}
fetch('https://jsonplaceholder.typicode.com/posts/1')
  .then((response) => response.json())
  .then((json) => console.log(json))
{{< / highlight >}}

#### Example Fetch API Usage

There are about six example from above site:

1. Getting a resource
2. Listing all resources
3. Creating a resource
4. Updating a resource with PUT
5. Updating a resource with PATCH
6. Deleting a resource

Since we are going to use JSON from `jsonplaceholder`,
we are also going to use these pattern.

-- -- --

### 1: Simple Promise

#### Preparing: Install

Since `fetch` is not available in `nodejs`,
we require to install `node-fetch` package in local folder.

{{< highlight bash >}}
❯ npm i -s -D node-fetch
+ node-fetch@2.6.1
updated 1 package and audited 99 packages in 1.978s
{{< / highlight >}}

![Node Fetch: NPM Install][img-isd-node-fetch]

#### Getting a Resource

Consider rewrite the provided example as ES6 below:

* [gitlab.com/.../fetch/01-01-read-id.js][src-01-01-read-id]

{{< highlight javascript >}}
// Getting a resource
const fetch   = require("node-fetch")
const baseURL = 'https://jsonplaceholder.typicode.com'
const id      = 1

fetch(`${baseURL}/posts/${id}`)
fetch(baseURL+'/posts/1')
  .then(response => response.json())
  .then(json => console.log(json))
{{< / highlight >}}

We are going to get the result,
already in JSON format as below:

{{< highlight javascript >}}
$ node 01-01-read-id.js
{
  userId: 1,
  id: 1,
  title: 'sunt aut facere repellat provident occaecati excepturi optio reprehenderit',
  body: 'quia et suscipit\n' +
    'suscipit recusandae consequuntur expedita et cum\n' +
    'reprehenderit molestiae ut ut quas totam\n' +
    'nostrum rerum est autem sunt rem eveniet architecto'
}
{{< / highlight >}}

![Simple Promise: Getting a Resource][img-01-01-read-id]

This used javascript `promise`.
It means it run in synchronous manner.

#### Fetch Options

Options can be attached to a fetch call.
For example this update below:

* [gitlab.com/.../fetch/01-04-update.js][src-01-04-update]

{{< highlight javascript >}}
// Updating a resource with PUT
const fetch   = require("node-fetch")
const headers = { 'Content-type': 'application/json; charset=UTF-8' }
const baseURL = 'https://jsonplaceholder.typicode.com'
const id      = 1

fetch(`${baseURL}/posts/${id}`, {
  method: 'PUT',
  body: JSON.stringify({
    id: 1,
    title: 'foo',
    body: 'bar',
    userId: 1,
  }),
  headers: headers
})
  .then((response) => response.json())
  .then((json) => console.log(json))
{{< / highlight >}}

This has these three options:

1. Method: might be `GET`, `POST`, `PUT`, `PATCH`, or `DELETE`.
2. Body: The data.
3. Headers: Using JSON.

There are more options that you can read:

* [MDN Web Docs: Using Fetch][mdn-using-fetch]

#### And The Rest …

I do not think I have to pour them all in a blog,
since it is already explained in original `jsonplaceholder` site anyway.
However you can have a look at all the codes in my repository.

1. [Promised: Getting a resource][src-01-01-read-id]
2. [Promised: Listing all resources][src-01-02-read-all]
3. [Promised: Creating a resource][src-01-03-create]
4. [Promised: Updating a resource with PUT][src-01-04-update]
5. [Promised: Updating a resource with PATCH][src-01-05-patch]
6. [Promised: Deleting a resource][src-01-06-delete]

-- -- --

### 2: Asynchronus Await

#### Reading Reference

There is a better explanation about `await` here:

* https://dev.to/johnpaulada/synchronous-fetch-with-asyncawait

I do not have to repeat myself.

#### Fetch Using Async/Await

Consider rewrite the provided the code above in asynchronous manner.
This time using knowledge from article above.

{{< highlight javascript >}}
// Getting a resource
const fetch   = require("node-fetch")
const baseURL = 'https://jsonplaceholder.typicode.com'
const id      = 1

const request = async () => {
  const response = await fetch(`${baseURL}/posts/${id}`)
  const json = await response.json()
  return json
}

request()
  .then(data => console.log(data))
  .catch(reason => console.log(reason.message))
{{< / highlight >}}

This code looks fine right?

#### Try … Catch

> What's the catch?

We cannot just put the code above in `try/catch` clause,
without proper understanding of `asynchronous` process.

Instead of returning value,
we can use function straight away,
and let the function handle the response.

* [gitlab.com/.../fetch/02-01-read-id.js][src-02-01-read-id]

{{< highlight javascript >}}
// Getting a resource
const fetch   = require("node-fetch")
const baseURL = 'https://jsonplaceholder.typicode.com'
const id      = 1
const show    = json => console.log(json)

const request = async () => {
  try {
    const response = await fetch(`${baseURL}/posts/${id}`)
    const json = await response.json()
    show(json)
  } catch(err) {
    console.log(err.message)
  }
}

request()
{{< / highlight >}}

Consider turn of the `wifi`, and experience the `error message`:

{{< highlight bash >}}
$ node 02-01-read-id.js
request to https://jsonplaceholder.typicode.com/posts/1 failed, reason: getaddrinfo EAI_AGAIN jsonplaceholder.typicode.com
{{< / highlight >}}

![Asynchronus Await: Getting a Resource][img-02-01-read-id]

#### Apply This Pattern

With the same pattern we can rewrite all the codes.
For example this update below:

* [gitlab.com/.../fetch/02-04-update.js][src-02-04-update]

{{< highlight javascript >}}
// Updating a resource with PUT
const fetch   = require("node-fetch")
const headers = { 'Content-type': 'application/json; charset=UTF-8' }
const baseURL = 'https://jsonplaceholder.typicode.com'
const id      = 1
const show    = json => console.log(json)

const options = {
  method: 'PUT',
  body: JSON.stringify({
    id: 1,
    title: 'foo',
    body: 'bar',
    userId: 1,
  }),
  headers: headers
}

const request = async () => {
  try {
    const response = await fetch(`${baseURL}/posts/${id}`, options)
    const json = await response.json()
    show(json)
  } catch(err) {
    console.log(err.message)
  }
}

request()
{{< / highlight >}}

#### And The Rest …

I do not think I have to pour them all in a blog,
since you can have a look at all the codes in my repository anyway.
Each script can be shown as below, along with the result.

1. [Asynchronus: Getting a resource][src-02-01-read-id]

2. [Asynchronus: Listing all resources][src-02-02-read-all]

3. [Asynchronus: Creating a resource][src-02-03-create]

{{< highlight javascript >}}
$ node 02-03-create.js
{ title: 'foo', body: 'bar', userId: 1, id: 101 }
{{< / highlight >}}

4. [Asynchronus: Updating a resource with PUT][src-02-04-update]

{{< highlight javascript >}}
$ node 02-04-update.js
{ id: 1, title: 'foo', body: 'bar', userId: 1 }
{{< / highlight >}}

5. [Asynchronus: Updating a resource with PATCH][src-02-05-patch]

{{< highlight javascript >}}
$ node 02-05-patch.js
{
  userId: 1,
  id: 1,
  title: 'foo',
  body: 'quia et suscipit\n' +
    'suscipit recusandae consequuntur expedita et cum\n' +
    'reprehenderit molestiae ut ut quas totam\n' +
    'nostrum rerum est autem sunt rem eveniet architecto'
}
{{< / highlight >}}

6. [Asynchronus: Deleting a resource][src-02-06-delete]

{{< highlight javascript >}}
$ node 02-06-delete.js
{{< / highlight >}}

![Asynchronus Await: All Result][img-02-02-all]

The last one is `delete`.
This will show nothing.

-- -- --

### 3: Class Wrapper

> So many file, so little differences.

The six source code above, have a lot in common.
We can manage all this code into one class.

#### Constructor

We can start with `constructor`.
And place common variable over there.

* [gitlab.com/.../fetch/03-01-class.js][src-03-01-class]

{{< highlight javascript >}}
const fetch = require("node-fetch")

class PlaceHolder {
  constructor() {
    this.baseURL = 'https://jsonplaceholder.typicode.com'
    this.headers = { 'Content-type': 'application/json; charset=UTF-8' }
  }

  …
}
{{< / highlight >}}

#### Private Method

This is the core of our class.
We can have `async function` in a class.

{{< highlight javascript >}}
  // Private Method (using # at tc39)
  async _request(url, options = null) {
    try {
      const response = options === null ?
        await fetch(url) : await fetch(url, options)
      const json = await response.json()
      return json
    } catch(err) {
      console.log(err.message)
    }
  }
{{< / highlight >}}

This way, we can call the `_request`,
or each _public method_ later, in a simple way.

{{< highlight javascript >}}
  read(id) {
    return this._request(`${this.baseURL}/posts/${id}`)
  }
{{< / highlight >}}

And then we call the method later with:

{{< highlight javascript >}}
placeholder.read(1)
  .then(json => console.log(json))
{{< / highlight >}}

Most of the time, I do not know what later implementation,
will be working on the data.
I'd better keep these methods in _returned_ fashioned,
and decide what to do with it later.

#### Public Method

We have six public method.
All five are here.

{{< highlight javascript >}}
  // Public Method
  readAll() {
    return this._request(`${this.baseURL}/posts`)
  }

  read(id) {
    return this._request(`${this.baseURL}/posts/${id}`)
  }

  create(body) {
    const options = {
      method: 'POST',
      body: JSON.stringify(body),
      headers: this.headers
    }
    return this._request(`${this.baseURL}/posts`, options)
  }

  update(id, body) {
    const options = {
      method: 'PUT',
      body: JSON.stringify(body),
      headers: this.headers
    }
    return this._request(`${this.baseURL}/posts/${id}`, options)
  }

  patch(id, body) {
    const options = {
      method: 'PATCH',
      body: JSON.stringify(body),
      headers: this.headers
    }
    return this._request(`${this.baseURL}/posts/${id}`, options)
  }
{{< / highlight >}}

#### Delete Method

> The choice is yours.

The last is the delete `method`.
Depends on your requirement, this can run a callback, or just do nothing.
This example show different pattern for `delete` function.
Thus, using its own method,
instead of using already made `this._request` method.

{{< highlight javascript >}}
  async delete(id) {
    try {
      const url = `${this.baseURL}/posts/${id}`
      const response = await fetch(url, { method: 'DELETE' })
      const json = await response.json()
    } catch(err) {
      console.log(err.message)
    }
  }
{{< / highlight >}}

The code management is depend on your requirement.
Mine might different with yours.

#### Example Usage

First we must make a new class instance.

* [gitlab.com/.../fetch/03-01-class.js][src-03-01-class]

{{< highlight javascript >}}
const placeholder = new PlaceHolder()
{{< / highlight >}}

Then we can call each different method,
that might have different `data`.

1. Usage: Getting a resource

{{< highlight javascript >}}
placeholder.readAll()
  .then(json => console.log(json))
{{< / highlight >}}

2. Usage: Listing all resources

{{< highlight javascript >}}
placeholder.read(1)
  .then(json => console.log(json))
{{< / highlight >}}

3. Usage: Creating a resource

{{< highlight javascript >}}
const body = { title: 'foo', body: 'bar',  userId: 1 }
placeholder.create(body)
  .then(json => console.log(json))
{{< / highlight >}}

4. Usage: Updating a resource with PUT

{{< highlight javascript >}}
const body = { id: 1, title: 'foo', body: 'bar',  userId: 1 }
placeholder.update(1, body)
  .then(json => console.log(json))
{{< / highlight >}}

5. Usage: Updating a resource with PATCH

{{< highlight javascript >}}
const body = { id: 1, title: 'foo' }
placeholder.patch(1, body)
  .then(json => console.log(json))
{{< / highlight >}}

6. Usage: Deleting a resource

{{< highlight javascript >}}
placeholder.delete(1)
{{< / highlight >}}

This last one won't return anything.

#### Alternative Usage

You can also write down in asynchronous fashioned:

{{< highlight javascript >}}
const request = async () => { return await placeholder.read(1) }
request().then(json => console.log(json))
{{< / highlight >}}

But I still do not know,
if there is any practical reason to do this.

-- -- --

### 4: Callback in Class

> Sometimes I failed 😁. 

I have managed other way to show all the class,
without using `.then(…)` over and over again.

#### Callback Method

> You can skip this entire useless part.

Turning above function into call back is not a hard thing to do.

* [gitlab.com/.../fetch/03-02-class.js][src-03-02-class]

{{< highlight javascript >}}
class PlaceHolder {
  constructor(callback=null) {
    this.baseURL = 'https://jsonplaceholder.typicode.com'
    this.headers = { 'Content-type': 'application/json; charset=UTF-8' }
    this.callback = callback
  }

  // Private Method (using # at tc39)
  _doCallback(json, response) {
    if (this.callback) this.callback(json, response)
  }
  
  async _request(url, options = null) {
    try {
      const response = options === null ?
        await fetch(url) : await fetch(url, options)
      const json = await response.json()
      this._doCallback(json, response)
    } catch(err) {
      console.log(err.message)
    }
  }

  …
}
{{< / highlight >}}

#### Example Usage

Now we can instantiate the class as below:

{{< highlight javascript >}}
const placeholder = new PlaceHolder( (json, response) => { 
  if (response.status == '200') {
    console.log(json)
  } else {
    console.log(`status [${response.status}]`)
  }
});

const body = { id: 1, title: 'foo' }
placeholder.patch(1, body)
{{< / highlight >}}

With the result as below:

{{< highlight javascript >}}
$ node 03-02-class.js
{
  userId: 1,
  id: 1,
  title: 'foo',
  body: 'quia et suscipit\n' +
    'suscipit recusandae consequuntur expedita et cum\n' +
    'reprehenderit molestiae ut ut quas totam\n' +
    'nostrum rerum est autem sunt rem eveniet architecto'
}
{{< / highlight >}}

#### Error Response

Consider test our callback.

{{< highlight javascript >}}
placeholder.read(1000)
{{< / highlight >}}

This will show

{{< highlight javascript >}}
$ node 03-02-class.js
status [404]
{{< / highlight >}}

#### What Failure?

> The method must return data asynchronously to be used later.

Yes, this code looks fine, at the first sight.
But this concept doesn't have any practical application in real project.
I still keep the code for personal reason,
but I simply **drop the whole idea**.

#### Finally

That is all with `fetch`.

-- -- --

### 5: Axios Alternative

There is however other library.
A very popular one, the `axios` library.

We can rewrite the above script as below:

* [gitlab.com/.../axios/01-01-read-id.js][src-ax-01-read-id]

{{< highlight javascript >}}
// Getting a resource
const axios   = require('axios').default;
const baseURL = 'https://jsonplaceholder.typicode.com'
const id      = 1
const url     = `${baseURL}/posts/${id}`

axios.get(url)
  .then( response => console.log(response.data) )
  .catch( error   => console.log(error) )
{{< / highlight >}}

Or if you wish for a more complete `options` example:

* [gitlab.com/.../axios/01-04-update.js][src-ax-04-update]

{{< highlight javascript >}}
// Updating a resource with PUT
const axios   = require('axios').default;
const headers = { 'Content-type': 'application/json; charset=UTF-8' }
const baseURL = 'https://jsonplaceholder.typicode.com'
const id      = 1
const url     = `${baseURL}/posts/${id}`

const data    = { id: 1, title: 'foo', body: 'bar',  userId: 1 }

axios.put(url, data)
  .then( response => console.log(response.data) )
  .catch( error   => console.log(error) )
{{< / highlight >}}

With about the same result.

#### And The Rest …

There are already so many `axios` tutorial.
So I do not think I have to pour them all in a blog either.

1. [Axios: Getting a resource][src-ax-01-read-id]
2. [Axios: Listing all resources][src-ax-02-read-all]
3. [Axios: Creating a resource][src-ax-03-create]
4. [Axios: Updating a resource with PUT][src-ax-04-update]
5. [Axios: Updating a resource with PATCH][src-ax-05-patch]
6. [Axios: Deleting a resource][src-ax-06-delete]

#### Which one?

> Native asynchronous fetch() or Axios.

I must say: __I do not know__.

-- -- --

### Conclusion

Fetching JSON is not as hard as thought.
It is become easier now since you have already had  a working example.

What do you think?

[//]: <> ( -- -- -- links below -- -- -- )

[mdn-using-fetch]:  https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch

[img-isd-node-fetch]:   {{< baseurl >}}assets/posts/code/2020/10/es-fetch-01-isd-node-fetch.png
[img-01-01-read-id]:    {{< baseurl >}}assets/posts/code/2020/10/es-fetch-01-01-read-id.png
[img-02-01-read-id]:    {{< baseurl >}}assets/posts/code/2020/10/es-fetch-02-01-read-id.png
[img-02-02-all]:        {{< baseurl >}}assets/posts/code/2020/10/es-fetch-02-02-all.png

[source-example]:       {{< tutor-ecmascript >}}/cli-example/

[src-01-01-read-id]:    {{< tutor-ecmascript >}}/cli-example/fetch/01-01-read-id.js
[src-01-02-read-all]:   {{< tutor-ecmascript >}}/cli-example/fetch/01-02-read-all.js
[src-01-03-create]:     {{< tutor-ecmascript >}}/cli-example/fetch/01-03-create.js
[src-01-04-update]:     {{< tutor-ecmascript >}}/cli-example/fetch/01-04-update.js
[src-01-05-patch]:      {{< tutor-ecmascript >}}/cli-example/fetch/01-05-patch.js
[src-01-06-delete]:     {{< tutor-ecmascript >}}/cli-example/fetch/01-06-delete.js

[src-02-01-read-id]:    {{< tutor-ecmascript >}}/cli-example/fetch/02-01-read-id.js
[src-02-02-read-all]:   {{< tutor-ecmascript >}}/cli-example/fetch/02-02-read-all.js
[src-02-03-create]:     {{< tutor-ecmascript >}}/cli-example/fetch/02-03-create.js
[src-02-04-update]:     {{< tutor-ecmascript >}}/cli-example/fetch/02-04-update.js
[src-02-05-patch]:      {{< tutor-ecmascript >}}/cli-example/fetch/02-05-patch.js
[src-02-06-delete]:     {{< tutor-ecmascript >}}/cli-example/fetch/02-06-delete.js

[src-03-01-class]:      {{< tutor-ecmascript >}}/cli-example/fetch/03-01-class.js
[src-03-02-class]:      {{< tutor-ecmascript >}}/cli-example/fetch/03-02-class.js

[src-ax-01-read-id]:    {{< tutor-ecmascript >}}/cli-example/axios/01-01-read-id.js
[src-ax-02-read-all]:   {{< tutor-ecmascript >}}/cli-example/axios/01-02-read-all.js
[src-ax-03-create]:     {{< tutor-ecmascript >}}/cli-example/axios/01-03-create.js
[src-ax-04-update]:     {{< tutor-ecmascript >}}/cli-example/axios/01-04-update.js
[src-ax-05-patch]:      {{< tutor-ecmascript >}}/cli-example/axios/01-05-patch.js
[src-ax-06-delete]:     {{< tutor-ecmascript >}}/cli-example/axios/01-06-delete.js
