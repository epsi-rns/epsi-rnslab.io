---
type   : post
title  : "Inkscape: Event Schedule Announcement"
date   : 2015-07-07T07:07:15+07:00
slug   : inkscape-event-schedule
categories: [design]
tags      : [inkscape, alumni]
keywords  : [vector graphic, flyer, woro-woro, event schedule]
author : epsi
opengraph:
  image: assets/site/images/topics/inkscape.png

toc    : "toc-2015-inkscape-alumni"

excerpt:
  Two sample inkscape vector templates, for event schedule announcement.
  That anybody can download and use it freely.

---

This event schedule announcement is my early works in inkscape,
No layers, no original background, no pattern detail, no filter effect. 
So simple, just shape, text and standard font.
So there is nothing much to talk about the design.

-- -- --

### Schedule of Golden Anniversary of Alumni

Please Click for higher resolution.

[![Schedule of Golden Anniversary of Alumni][image-sched-1]][hires-sched-1]

#### Document Properties

**Size**: 1200px width, 1000px height

#### SVG Source

* [epsi-rns.gitlab.io/.../50th-ftui-schedule.svg.gz][dotfiles-sched-1]

* [nurwijayadi.deviantart.com/art/...][deviant-sched-1]

-- -- --

### Schedule of Alumni Election Day

Please Click for higher resolution.

[![Schedule of Alumni Election Day][image-sched-2]][hires-sched-2]

#### Document Properties

**Size**: 960px width, 960px height

#### SVG Source

* [epsi-rns.gitlab.io/.../munas-ftui-schedule.svg.gz][dotfiles-sched-2]

* [nurwijayadi.deviantart.com/art/...][deviant-sched-2]

-- -- --

That's all for now.


[//]: <> ( -- -- -- links below -- -- -- )


[image-sched-1]:    {{< assets-design >}}/2015/07/50th-ftui-schedule.png
[hires-sched-1]:    https://photos.google.com/album/AF1QipOYi1tE3AwxfL0DQCn7eAhQHekVu1xxo2-lHjta/photo/AF1QipMI_kjPzmQ7lFbrojRgMGG0SLX7polCVgsuYY4c
[dotfiles-sched-1]: {{< assets-design >}}/2015/07/50th-ftui-schedule.svg.gz
[deviant-sched-1]:  http://nurwijayadi.deviantart.com/art/50th-FTUI-schedule-01-645783014

[image-sched-2]:    {{< assets-design >}}/2015/07/munas-ftui-schedule.png
[hires-sched-2]:    https://photos.google.com/album/AF1QipOYi1tE3AwxfL0DQCn7eAhQHekVu1xxo2-lHjta/photo/AF1QipPHAAetXRwy5DhF44TxvR-7y3D21KaR5A_F1DtC
[dotfiles-sched-2]: {{< assets-design >}}/2015/07/munas-ftui-schedule.svg.gz
[deviant-sched-2]:  http://nurwijayadi.deviantart.com/art/Munas-FTUI-schedule-02-645783181
