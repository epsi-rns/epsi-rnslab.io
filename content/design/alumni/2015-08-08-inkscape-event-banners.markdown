---
type   : post
title  : "Inkscape: Event Banner Collection"
date   : 2015-08-08T08:08:15+07:00
slug   : inkscape-event-banners
categories: [design]
tags      : [inkscape, alumni]
keywords  : [vector graphic, banner, print]
author : epsi
opengraph:
  image: assets/site/images/topics/inkscape.png

toc    : "toc-2015-inkscape-alumni"

excerpt:
  A collection of inkscape vector designs with printing press targeting.
  That anybody can download and use it freely.
  A collection of banners with varying size ratio
  for your event!
  
---

Need banners for your event ?
Download these, change the colours,
and send them to printing press !

I like to support my community.
Sharing become part of my desire.
These designs below are for your event!

Based on my original effort
for alumni reunion/election day.

Not a super awesome design, but sufficient.

-- -- --

### Design

I have already put each stuff in its own layer.
So you can change the color and pattern easily.

#### Typography

As usual, I always use free font. 

* Capture It <http://www.dafont.com/capture-it.font>

* Bird of Paradise <http://www.dafont.com/birds-of-paradise.font>

* Droid Sans <https://www.fontsquirrel.com/fonts/droid-sans>

#### Size Ratio/ Document Properties

* Wall of Fame: 4m x 3m <br>: 1200px width, 900px height

* Banner: 6m x 5m <br>: 1200px width, 1000px height

* Banner: 12m x 5m <br>: 1200px width, 500px height

* Spanduk: 5m x 2m <br>: 1500px width, 600px height

* Backdrop: 10m x 2m <br>: 1500px width, 300px height

* Highway Banner: 2m x 20m <br>: no SVG source

* Umbul-umbul: 90cm x 300cm <br>: 300px width, 1000px height

#### Caveat

Press Printing use CMYK instead of RGB.
If you care, you need to adjust the CMYK,
or you might get slightly different color, e.g. darker.


-- -- --

#### Wall of Fame: 4m x 3m

Please Click for higher resolution.

[![Desain Munas: Wall of Fame: 4m x 3m][image-wof]][hires-wof]

![Foto Munas: Wall of Fame: 4m x 3m][photo-wof]

#### SVG Source

* [epsi-rns.gitlab.io/.../munas-01-wall-of-fame-4x3.svg.gz][dotfiles-wof]

* [nurwijayadi.deviantart.com/art/...][deviant-wof]

-- -- --

### Banner: 6m x 5m

Please Click for higher resolution.

[![Desain Munas: Banner: 6m x 5m][image-banner-1]][hires-banner-1]

![Foto Munas: Banner: 6m x 5m][photo-banner-1]

#### SVG Source

* [epsi-rns.gitlab.io/.../munas-02-banner-6x5.svg.gz][dotfiles-banner-1]

* [nurwijayadi.deviantart.com/art/...][deviant-banner-1]

-- -- --

### Banner: 12m x 5m

Please Click for higher resolution.

[![Desain Munas: Banner: 12m x 5m][image-banner-2]][hires-banner-2]

![Foto Munas: Banner: 12m x 5m][photo-banner-2]

#### SVG Source

* [epsi-rns.gitlab.io/.../munas-03-banner-12x5.svg.gz][dotfiles-banner-2]

* [nurwijayadi.deviantart.com/art/...][deviant-banner-2]

-- -- --

### Spanduk: 5m x 2m

Please Click for higher resolution.

[![Desain Munas: Spanduk: 5m x 2m][image-spanduk]][hires-spanduk]

#### SVG Source

* [epsi-rns.gitlab.io/.../munas-04-spanduk-5x2.svg.gz][dotfiles-spanduk]

* [nurwijayadi.deviantart.com/art/...][deviant-spanduk]

-- -- --

### Backdrop: 10m x 2m

Please Click for higher resolution.

[![Desain Munas: Backdrop: 10m x 2m][image-backdrop]][hires-backdrop]

![Foto Munas: Backdrop: 10m x 2m][photo-backdrop]

#### SVG Source

* [epsi-rns.gitlab.io/.../munas-05-backdrop-10x2.svg.gz][dotfiles-backdrop]

* [nurwijayadi.deviantart.com/art/...][deviant-backdrop]

-- -- --

### Highway Banner: 2m x 20m

Please Click for higher resolution.

[![Desain Munas: Highway Banner: 2m x 20m][image-highway-1]][hires-highway-1]

* [nurwijayadi.deviantart.com/art/...][deviant-highway-1]

[![Desain Munas: Highway Banner: 2m x 20m][image-highway-2]][hires-highway-2]

* [nurwijayadi.deviantart.com/art/...][deviant-highway-2]

-- -- --

### Umbul-Umbul: 90cm x 300cm

Please Click for higher resolution.

[![Desain Munas: Umbul-Umbul: 90cm x 300cm][image-umbul2]][hires-umbul2]

![Foto Munas: Umbul-Umbul: 90cm x 300cm][photo-umbul2]

#### SVG Source

* [epsi-rns.gitlab.io/.../munas-07-umbul2-90x300.svg.gz][dotfiles-umbul2]

* [nurwijayadi.deviantart.com/art/...][deviant-umbul2]

-- -- --

That's all for now.

[//]: <> ( -- -- -- links below -- -- -- )

[image-wof]:        {{< assets-design >}}/2015/08/munas-01-wall-of-fame-4x3.png
[hires-wof]:        https://photos.google.com/album/AF1QipOYi1tE3AwxfL0DQCn7eAhQHekVu1xxo2-lHjta/photo/AF1QipN9117McniooDWtSjQRn4BDn9vuRu-jUTNqP04o
[photo-wof]:        {{< assets-design >}}/2015/08/munas-01-wall-of-fame-4x3-cetak.jpg
[dotfiles-wof]:     {{< assets-design >}}/2015/08/munas-01-wall-of-fame-4x3.svg.gz
[deviant-wof]:      http://nurwijayadi.deviantart.com/art/Munas-Wall-of-Fame-4x3-645784790

[image-banner-1]:   {{< assets-design >}}/2015/08/munas-02-banner-6x5.png
[hires-banner-1]:   https://photos.google.com/album/AF1QipOYi1tE3AwxfL0DQCn7eAhQHekVu1xxo2-lHjta/photo/AF1QipPW0WEovVwX5nFeQIC6jvoAPBrxXFt5qfC_1Vr2
[photo-banner-1]:   {{< assets-design >}}/2015/08/munas-02-banner-6x5-cetak.jpg
[dotfiles-banner-1]: {{< assets-design >}}/2015/08/munas-02-banner-6x5.svg.gz
[deviant-banner-1]:  http://nurwijayadi.deviantart.com/art/Munas-Banner-6x5-645785107

[image-banner-2]:   {{< assets-design >}}/2015/08/munas-03-banner-12x5.png
[hires-banner-2]:   https://photos.google.com/album/AF1QipOYi1tE3AwxfL0DQCn7eAhQHekVu1xxo2-lHjta/photo/AF1QipPQrixB-cBXR6QZuFi1IiGPjaJIdqWpbklbskMM
[photo-banner-2]:   {{< assets-design >}}/2015/08/munas-03-banner-12x5-cetak.jpg
[dotfiles-banner-2]: {{< assets-design >}}/2015/08/munas-03-banner-12x5.svg.gz
[deviant-banner-2]:  http://nurwijayadi.deviantart.com/art/Munas-Banner-12x5-645785262

[image-spanduk]:    {{< assets-design >}}/2015/08/munas-04-spanduk-5x2.png
[hires-spanduk]:    https://photos.google.com/album/AF1QipOYi1tE3AwxfL0DQCn7eAhQHekVu1xxo2-lHjta/photo/AF1QipPFh6eeXSwmV4iKxpYCTaJmzmhLXav4ViqJsF6E
[dotfiles-spanduk]: {{< assets-design >}}/2015/08/munas-04-spanduk-5x2.svg.gz
[deviant-spanduk]:  http://nurwijayadi.deviantart.com/art/Munas-Spanduk-5x2-645785418

[image-backdrop]:   {{< assets-design >}}/2015/08/munas-05-backdrop-10x2.png
[hires-backdrop]:   https://photos.google.com/album/AF1QipOYi1tE3AwxfL0DQCn7eAhQHekVu1xxo2-lHjta/photo/AF1QipMbCd8fGTowDDIa9W5PTLyGoCIV_xGNvtB0JrqH
[photo-backdrop]:   {{< assets-design >}}/2015/08/munas-05-backdrop-10x2-cetak.jpg
[dotfiles-backdrop]: {{< assets-design >}}/2015/08/munas-05-backdrop-10x2.svg.gz
[deviant-backdrop]:  http://nurwijayadi.deviantart.com/art/Munas-Backdrop-10x2-645785604

[image-highway-1]:  {{< assets-design >}}/2015/08/munas-06-spanduk-2x20-11b.png
[hires-highway-1]:  https://photos.google.com/album/AF1QipOYi1tE3AwxfL0DQCn7eAhQHekVu1xxo2-lHjta/photo/AF1QipMTG2HWi21gr_7KoC0IdPYaClyflUJqg9DVU7Ff
[deviant-highway-1]: http://nurwijayadi.deviantart.com/art/Munas-Spanduk-2x20-645785843

[image-highway-2]:  {{< assets-design >}}/2015/08/munas-06-spanduk-2x20-12c.png
[hires-highway-2]:  https://photos.google.com/album/AF1QipOYi1tE3AwxfL0DQCn7eAhQHekVu1xxo2-lHjta/photo/AF1QipMu4EmAgarmuRY8Uyf5Cm6OJhxFWkYh_Od0Jc-X
[deviant-highway-2]: http://nurwijayadi.deviantart.com/art/Munas-Spanduk-2x20-645785957

[image-umbul2]:     {{< assets-design >}}/2015/08/munas-07-umbul2-90x300.png
[hires-umbul2]:     https://photos.google.com/album/AF1QipOYi1tE3AwxfL0DQCn7eAhQHekVu1xxo2-lHjta/photo/AF1QipPIsTRPktvLw6pFiMx5zHkX6UDxQ1i9F6jnrLDo
[photo-umbul2]:     {{< assets-design >}}/2015/08/munas-07-umbul2-90x300-cetak.jpg
[dotfiles-umbul2]:  {{< assets-design >}}/2015/08/munas-07-umbul2-90x300.svg.gz
[deviant-umbul2]:   http://nurwijayadi.deviantart.com/art/Munas-Umbul-Umbul-90x300-645785733
