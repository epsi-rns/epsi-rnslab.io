---
type   : post
title  : "Inkscape: Event Invitation Stock Image"
date   : 2015-08-17T03:23:15+07:00
slug   : inkscape-invitation-stock
categories: [design]
tags      : [inkscape, alumni]
keywords  : [vector graphic, stock image, event invitation, woro-woro]
author : epsi
opengraph:
  image: assets/site/images/topics/inkscape.png

toc    : "toc-2015-inkscape-alumni"

excerpt:
  An elegant inkscape vector image stock. 
  That anybody can download and use it freely.
  Nice to have it as an event invitation.

---

My alumni friend, Esti Handayani,
has created a very nice event invitation,
for Alumni Gathering for University of Indonesia.

I have clone her design using inkscape,
so anyone can use and modify, this vector image stock freely. 

You can use this "Contoh Undangan Alumni" for 

* e-flyer, targeting whatsapp and facebook.

* or print it as hardcopy, using inkscape png export.

-- -- --

### Design

As usual, I always use free font. 

* Trajan Pro for campus life,

I have already put each stuff in its own layer. So you can change the color and pattern easily.

#### Document Properties

**Size**: 1200px width, 1200px height

-- -- --

### Preview

Please Click for higher resolution.

[![Sample Invitation: Yellow][image-yellow-invitation]][picasa-yellow-invitation]

[![Sample Invitation: Blue][image-blue-invitation]][picasa-blue-invitation]

-- -- --

### SVG Source

* [epsi-rns.gitlab.io/.../event-invitation-sample-alumni.svg.gz][dotfiles-invitation]

* [nurwijayadi.deviantart.com/art/...yellow...][deviant-invitation-yellow]

* [nurwijayadi.deviantart.com/art/...blue...][deviant-invitation-blue]


[//]: <> ( -- -- -- links below -- -- -- )

[image-yellow-invitation]:   {{< assets-design >}}/2015/08/event-invitation-sample-alumni-yellow.png
[picasa-yellow-invitation]:  https://photos.google.com/album/AF1QipOYi1tE3AwxfL0DQCn7eAhQHekVu1xxo2-lHjta/photo/AF1QipPVtkUmnKbthSVSHpHmwHCl13WeRSmf4U_TlK4x
[image-blue-invitation]:     {{< assets-design >}}/2015/08/event-invitation-sample-alumni-blue.png
[picasa-blue-invitation]:    https://photos.google.com/album/AF1QipOYi1tE3AwxfL0DQCn7eAhQHekVu1xxo2-lHjta/photo/AF1QipNUNbnjO20F9sxLmHrC2MDuQsYhK7L99wiuCHje

[dotfiles-invitation]:       {{< assets-design >}}/2015/08/event-invitation-sample-alumni.svg.gz
[deviant-invitation-yellow]: http://nurwijayadi.deviantart.com/art/Undangan-Alumni-645736320
[deviant-invitation-blue]:   http://nurwijayadi.deviantart.com/art/Undangan-Alumni-645736708
