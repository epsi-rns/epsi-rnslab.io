---
type   : post
title  : "Inkscape: Logo with Stamp Ornament"
date   : 2015-09-03T08:48:15+07:00
slug   : inkscape-stamp-logo
categories: [design]
tags      : [inkscape, logo]
keywords  : [vector graphic, stamp, logo]
author : epsi
opengraph:
  image: assets/site/images/topics/inkscape.png

toc    : "toc-2015-inkscape-alumni"

excerpt:
  An inkscape vector artwork. That anybody can download and use it freely.
  Nice to have it as avatar background,
  or put it as a stamp in your picture while travelling.

---

Logo with manual stamp ornament border would make an accent to your flyer.

I also add shadow with envelope path effects under the logo

-- -- --

### Pink Padz

**SVG Source**:

* [epsi-rns.gitlab.io/.../logo-stamp-pink-padz.svg.gz][dotfiles-pink-padz]

* [nurwijayadi.deviantart.com/art/...][deviant-pink-padz]

[![Pink Padz][image-ss-pink-padz]][photo-ss-pink-padz]

-- -- --

### ILUNI FTUI

**SVG Source**:

* [epsi-rns.gitlab.io/.../logo-stamp-iluni-ftui.svg.gz][dotfiles-iluni-ftui]

* [nurwijayadi.deviantart.com/art/...][deviant-iluni-ftui]

[![ILUNI Teknik][image-ss-iluni-ftui]][photo-ss-iluni-ftui]

### Document Properties

**Size**: 1200px width, 1200px height

-- -- --

### Stock Images Cover

I also make a cover for stock images.

![Stock Images 01][image-ss-stock-01]

* [www.deviantart.com/nurwijayadi/art/...][deviant-stock-01]

![Stock Images 02][image-ss-stock-02]

* [www.deviantart.com/nurwijayadi/art/...][deviant-stock-01]

-- -- --

Thank you for visiting.

[//]: <> ( -- -- -- links below -- -- -- )

[image-ss-pink-padz]:  {{< assets-design >}}/2015/09/logo-stamp-pink-padz.png
[photo-ss-pink-padz]:  https://photos.google.com/album/AF1QipOYi1tE3AwxfL0DQCn7eAhQHekVu1xxo2-lHjta/photo/AF1QipNRIaZe4nRF9LNCuVHZdL3Wo8IA9ZMAi2gpHd7Q
[image-ss-iluni-ftui]: {{< assets-design >}}/2015/09/logo-stamp-iluni-ftui.png
[photo-ss-iluni-ftui]: https://photos.google.com/album/AF1QipOYi1tE3AwxfL0DQCn7eAhQHekVu1xxo2-lHjta/photo/AF1QipNdW7RoYAoiXXrwg72lGE1lak-iNRQ3XfBEaQd2

[dotfiles-pink-padz]:  {{< assets-design >}}/2015/09/logo-stamp-pink-padz.svg.gz
[deviant-pink-padz]:   http://nurwijayadi.deviantart.com/art/Logo-Stamp-Ornament-Iluni-FTUI-645792391
[dotfiles-iluni-ftui]: {{< assets-design >}}/2015/09/logo-stamp-iluni-ftui.svg.gz
[deviant-iluni-ftui]:  http://nurwijayadi.deviantart.com/art/Logo-Stamp-Ornament-Padmanaba-in-Pink-645792516

[image-ss-stock-01]:  {{< assets-design >}}/2015/09/stock-perangko-01.png
[deviant-stock-01]:   https://www.deviantart.com/nurwijayadi/art/Cover-Stock-Images-645991377
[image-ss-stock-02]:  {{< assets-design >}}/2015/09/stock-perangko-02.png
[deviant-stock-02]:   https://www.deviantart.com/nurwijayadi/art/Cover-Stok-Images-645991166
