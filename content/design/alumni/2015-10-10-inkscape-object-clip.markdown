---
type   : post
title  : "Inkscape: Object Clip"
date   : 2015-10-10T10:10:15+07:00
slug   : inkscape-object-clip
categories: [design]
tags      : [inkscape, alumni]
keywords  : [vector graphic, object clip]
author : epsi
opengraph:
  image: assets/site/images/topics/inkscape.png

toc    : "toc-2015-inkscape-alumni"

excerpt:
  Combining photograph and test with object clip is fun.
  Even if your photography skill sucks,
  you can still make a good impression.

---

Combining photograph and test with object clip is easy.
Even when my photography skill terrible,
It still make a good impression.
 
As bonus, you can download the SVG template below and use it freely.

-- -- --

### Basic: Step By Step

#### Photo

Put photo on inkscape document.

![Clip Step One][image-step-photo]

#### Text

Put text with nice font, above that photo object.

![Clip Step Two][image-step-text]

#### Clip

Select both photo object and text, and set object clip from menu.
And Voila!

![Clip Step Three][image-step-clip]

-- -- --

### Real World Example

#### Typography

* Sablon Font

-- -- --

### ILUNI UI: Halal Bihalal

Please Click for higher resolution.

[![ILUNI UI: Halal Bihalal][image-iluni-ui]][hires-iluni-ui]

#### Document Properties

**Size**: 1000px width, 720px height

#### SVG Source

* [epsi-rns.gitlab.io/.../clip-iluni-01.svg.gz][dotfiles-iluni-ui]

* [nurwijayadi.deviantart.com/art/...][deviant-iluni-ui]

-- -- --

### ILUNI FTUI: Laporan Pertanggungjawaban

Please Click for higher resolution.

[![ILUNI FTUI: Laporan Pertanggungjawaban][image-iluni-ftui]][hires-iluni-ftui]

#### Document Properties

**Size**: 1000px width, 1100px height

#### SVG Source

* [epsi-rns.gitlab.io/.../clip-iluni-02.svg.gz][dotfiles-iluni-ftui]

* [nurwijayadi.deviantart.com/art/...][deviant-iluni-ftui]

-- -- --

### ILUNI FTUI: Diskusi Energi Terbarukan

Please Click for higher resolution.

[![Energi: Ucapan Terimakasih][image-energi-thanks]][hires-energi-thanks]

#### Document Properties

**Size**: 1200px width, 1200px height

#### SVG Source

* [epsi-rns.gitlab.io/.../energi-terbarukan-ucapan-terima-kasih.svg.gz][dotfiles-energi-thanks]

* [nurwijayadi.deviantart.com/art/...][deviant-energi-thanks]

-- -- --

That's all. Have Fun with Clipping Object.

[//]: <> ( -- -- -- links below -- -- -- )

[image-step-photo]:     {{< assets-design >}}/2015/10/clip-step-01.png
[image-step-text]:      {{< assets-design >}}/2015/10/clip-step-02.png
[image-step-clip]:      {{< assets-design >}}/2015/10/clip-step-03.png

[image-iluni-ui]:       {{< assets-design >}}/2015/10/clip-iluni-01.png
[hires-iluni-ui]:       https://photos.google.com/album/AF1QipOYi1tE3AwxfL0DQCn7eAhQHekVu1xxo2-lHjta/photo/AF1QipMd6rsf7mkN3PYCiIOQv2ndj_m8tQwSZIiv-b5g
[dotfiles-iluni-ui]:    {{< assets-design >}}/2015/10/clip-iluni-01.svg.gz
[deviant-iluni-ui]:     http://nurwijayadi.deviantart.com/art/Ucapan-Terimakasih-clip-01-645782174

[image-iluni-ftui]:         {{< assets-design >}}/2015/10/clip-iluni-02.png
[hires-iluni-ftui]:         https://photos.google.com/album/AF1QipOYi1tE3AwxfL0DQCn7eAhQHekVu1xxo2-lHjta/photo/AF1QipPzTaUga94dCYQFIpFxCfMyHIhY5OOwDl8aUU5S
[dotfiles-iluni-ftui]:      {{< assets-design >}}/2015/10/clip-iluni-02.svg.gz
[deviant-iluni-ftui]:       http://nurwijayadi.deviantart.com/art/Laporan-Pertanggungjawaban-Clip-02-645782381

[image-energi-thanks]:      {{< assets-design >}}/2015/09/energi-terbarukan-ucapan-terima-kasih.png
[hires-energi-thanks]:      https://photos.google.com/album/AF1QipOYi1tE3AwxfL0DQCn7eAhQHekVu1xxo2-lHjta/photo/AF1QipO1s9jWFfB5u8XjfMs6ODUarl8ppTJcmRiu79Sn
[dotfiles-energi-thanks]:   {{< assets-design >}}/2015/09/energi-terbarukan-ucapan-terima-kasih.svg.gz
[deviant-energi-thanks]:  http://nurwijayadi.deviantart.com/art/Ucapan-Terimakasih-clip-03-645782639
