---
type   : post
title  : "Inkscape: Progress Ring Animation"
date   : 2017-11-15T04:29:15+07:00
slug   : inkscape-progress-ring
categories: [design]
tags      : [inkscape, animation]
keywords  : [saporito]
author : epsi
opengraph:
  image: assets-design/2017/11/progress-bright-smooth.png
toc    : "toc-2015-inkscape-daily"

excerpt:
  Inkscape progress ring animation with ffmpeg.

---

I have been following tutorial from Nick Saporito.

* [Inkscape Tutorial: Vector Progress Rings][saporito]

And I have a few ideas for myself.
One of them is making animation based on this progress ring.

-- -- --

### Dark

#### Unified Graphic Material

> Go straight to the point.

The inkscape source file is here:

* [epsi-rns.gitlab.io/.../progress-ring-dark.svg][source-dark]

Or this

* [epsi-rns.gitlab.io/.../progress-ring-bright.svg][source-bright]

And the result is here:

![Progress Ring: Dark][image-dark]

### DeviantArt

I also put a link in DeviantArt, with downloadable SVGs.

* [Progress Ring Animation][deviant-ring]

#### Extracting Frame

I have already made some layers, one of the named `cut-frame`,
so you can select the object and export each frame easily.

![Progress Ring: Select Frame][select-dark]

You can see when you export PNG on selection,
it is already has target file to
`/home/epsi/Documents/progress-ring/progress-095.png`.

![Exports and Layers][layers-bright]

#### XML

> But how do I do that?

You can utilize XML editor in Inkscape to edit the `id`.

![XML Editor][xml-bright]

Or even easier to do it with text editor directly.
With text editor, you can `find-and-replace` the dpi right away.
Or you can change the target filename easily.
Especially for replacing multiple objects.

{{< highlight xml >}}
    <rect
       inkscape:export-filename="/home/epsi/Documents/progress-ring/progress-095.png"
       inkscape:export-ydpi="640"
       inkscape:export-xdpi="640"
       ...
       id="95%"
       ...
       />
{{< / highlight >}}

![Editing SVG in Text Editor][editor-bright]

#### Extracted Files

Select each object frame for all progress ring, then export,
and you will have yourself frames in your file manager.

![File Manager: Each Frames][caja-dark]

#### GIMP Animation

My first attempt is `GIMP animation`.
After exporting to GIF, I realize that I lost some colors.
The result is not smooth.

![Progress Ring: GIF Animation][image-gif]

#### FFMPEG

I need a smooth one.
The solution is `ffmpeg`.

> Go straight to the point.

The command is here:

{{< highlight bash >}}
$ ffmpeg \
    -framerate 4 \
    -pattern_type glob -i '*.png' \
    -c:v libx264 -profile:v baseline \
    -level 3.0 \
    -pix_fmt yuv420p \
    progress-dark.mp4
{{< / highlight >}}

And the result is here:

{{< embed-video width="250" height="250" src="assets/posts/design/2017/11/progress-dark.mp4" >}}

#### Caveat

I still don't know how to set different delay for each frame with `ffmpeg`.
Setting delay for each frame is pretty easy with `gimp animation`.
But of course, video is based on `framerate` rather than `delay`.

-- -- --

### Bright

#### Unified Graphic Material

> Go straight to the point.

The inkscape source file is here:

* [epsi-rns.gitlab.io/.../progress-ring-bright.svg][source-bright]

And the result is here:

![Progress Ring: Bright][image-bright]

#### Extracting Frame

The same steps as `dark` above.

![Progress Ring: Select Frame][select-bright]

#### Extracted Files

Select each progress, then export,
and you will have yourself frames in your file manager.

![File Manager: Each Frames][caja-bright]

#### FFMPEG

> Go straight to the point.

The command is here:

{{< highlight bash >}}
$ ffmpeg \
    -framerate 4 \
    -pattern_type glob -i '*.png' \
    -c:v libx264 -profile:v baseline \
    -level 3.0 \
    -pix_fmt yuv420p \
    progress-bright.mp4
{{< / highlight >}}

And the result is here:

{{< embed-video width="250" height="250" src="assets/posts/design/2017/11/progress-bright.mp4" >}}

-- -- --

### Smooth

#### Unified Graphic Material

All frames can be shown here.

![Progress Ring: Bright][image-smooth]

#### Arch Smoothing Process

> But How?

I put an overlay arc on top of the normal arc.

**Normal Arc**

The normal bright arc is shown as below figure.

![Normal Arc][045-bright]

**Overlay Arc**

A translucent overlay arc should be put on top of normal arc.

![Overlay Arc][045-add]

**Smooth Bright**

And the final result looks better.

![Smooth Arc][045-smooth]

#### The Overlay Arc

> Start with solid, end with translucency.

It utilize gradient as usual,
all RGBA with three stroke color arrangements.

![Overlay Arc: Gradient][045-gradient]

* top node: free, as it won't be shown anyway.

* middle node: the same as background,
  such as `#f5f5f5ff`.

* bottom node: the same as bottom gradient of the normal arc,
  such as `#ffeb3bff`.
  Except the alpha should be set to zero,
  so we have the final result as `#ffeb3b00`.
  
#### Fix Outline

In case you did not notice in this arc,
there is an annoying outline like looks.
It happened because there is bottom object with color.

![Overlay Arc: Annoying Outline][045-outline]

You can fix this by add very slight length to the arc.
By altering `sodipodi:start`, from `0` to `-0.01`.
You can do it directly by using XML editor.

![Overlay Arc: Change start XML Editor][045-xml]

Of course you have to change the top gradient node
to the same stroke color with background, such as `#f5f5f5ff`.

Now you have yourself a smooth arc.

![Overlay Arc: Fix Outline][045-outline-f]

#### FFMPEG

The result is here:

{{< embed-video width="250" height="250" src="assets/posts/design/2017/11/progress-bright-smooth.mp4" >}}

Not pretty useful. But I feel happy that I tried.
I will use it for my presentation someday in about 2020

-- -- --

### Kdenlive

You can utilize transparency for use with `kdenlive`,
to create watermark.

#### Timeline

It is pretty easy to add a bunch of images as frames in `kdenlive` timeline.

![Transparent Watermark in Video using Kdenlive][kdenlive-line]

The issue comes when you have to do repetitive task such as edit duration.

#### XML

If you think, that editing duration is cumbersome,
you can also edit `*.kdenlive` file artefact in text editor.
And then replace all occurence of the length property,
from five second to one second.
Or to be exact from `00:00:05.000` to `00:00:01.000`.

{{< highlight xml >}}
 <producer id="producer2" in="00:00:00.000" out="00:00:00.960">
  <property name="length">00:00:01.000</property>
  <property name="eof">pause</property>
  <property name="resource">/home/epsi/Documents/progress-ring/progress-005.png</property>
  ...
 </producer>
{{< / highlight >}}

![Editing Kdenlive in Text Editor][kdenlive-text]

#### FFMPEG

The render result from kdenlive is big.
I can reduce the size using `ffmpeg`.

{{< highlight bash >}}
$ ffmpeg -i test-bengkel-shadow.mp4 \
    -vf "scale=iw/4:ih/4" \
    -an \
    test-bengkel-fourth-shadow.mp4
{{< / highlight >}}

#### MP4 Result

{{< embed-video width="360" height="270" src="assets/posts/design/2017/11/bengkel-fourth-shadow.mp4" >}}

-- -- --

I think that's all for now.

Thank you for visiting.

[//]: <> ( -- -- -- links below -- -- -- )

[saporito]: https://www.youtube.com/watch?v=zu0HpnH1dXo

[image-gif]:    {{< assets-design >}}/2017/11/progress-ring-all.gif

[image-dark]:   {{< assets-design >}}/2017/11/progress-ring-dark.png
[select-dark]:  {{< assets-design >}}/2017/11/select-dark.png
[caja-dark]:    {{< assets-design >}}/2017/11/file-manager-dark.png
[source-dark]:  {{< assets-design >}}/2017/11/progress-ring-dark.svg

[image-bright]: {{< assets-design >}}/2017/11/progress-ring-bright.png
[select-bright]:{{< assets-design >}}/2017/11/select-bright.png
[layers-bright]:{{< assets-design >}}/2017/11/layers-bright.png
[xml-bright]:   {{< assets-design >}}/2017/11/xml-bright.png
[editor-bright]:{{< assets-design >}}/2017/11/editor-bright.png
[caja-bright]:  {{< assets-design >}}/2017/11/file-manager-bright.png
[source-bright]:{{< assets-design >}}/2017/11/progress-ring-bright.svg

[045-bright]:   {{< assets-design >}}/2017/11/progress-045-normal-s.png
[045-smooth]:   {{< assets-design >}}/2017/11/progress-045-smooth-s.png
[045-add]:      {{< assets-design >}}/2017/11/progress-045-additional-s.png
[045-gradient]: {{< assets-design >}}/2017/11/progress-045-gradient-s.png
[045-outline]:  {{< assets-design >}}/2017/11/progress-045-outline-crop.png
[045-outline-f]:{{< assets-design >}}/2017/11/progress-045-outline-fix.png
[045-xml]:      {{< assets-design >}}/2017/11/progress-045-xml.png

[image-smooth]: {{< assets-design >}}/2017/11/progress-bright-smooth.png

[deviant-ring]:     https://www.deviantart.com/nurwijayadi/art/Progress-Ring-Animation-713895798

[kdenlive-line]:{{< assets-design >}}/2017/11/kdenlive-timeline.png

[kdenlive-text]:{{< assets-design >}}/2017/11/editor-kdenlive.png
