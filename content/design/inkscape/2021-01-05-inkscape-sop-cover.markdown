---
type   : post
title  : "Inkscape SOP Docs - Cover"
date   : 2021-01-05T09:17:35+07:00
slug   : inkscape-template-sop-cover
categories: [design]
tags      : [inkscape]
keywords  : [SOP, ISO, Diagram, Paper, A4]
author : epsi
opengraph:
  image: assets/posts/design/2021/01/116-final-cat.png

toc    : "toc-2021-inkscape-sop"

excerpt:
  SOP related template using inkscape.
  Starting from cover.

---

### Preface

> SOP related template using inkscape.
> Starting from cover.

I found out for poeple who has the skill,
inkscape is actually more efficient tool,
to create a certain kind of document.
More control, looks sharper, and faster to be done.
If you can handle the complexity, inkscape is simpler.
The complexity can solve your problem in smple way.
The problem is people are scared to the learning steep of inkscape.

#### My Personal Journey

Inkscape can be used to create professional documents.
During the COVID period, I delivered hundreds of pages
of medical-related presentations using Inkscape.
The results were remarkably sharp.
Something I could not achieve with PowerPoint or Impress.

Over time, I began using Inkscape
to draw workflows following the BPML standard.
With Inkscape, I could control every shape and position precisely,
producing results that were unmatched.
I developed numerous SOP documents,
utilizing a standardized screen template,
consisting of workflow diagrams and, occasionally, descriptions.
Since these documents often required multiple pages,
I needed to create my own cover design.

At one point, I was tasked with preparing a competency matrix for my division.
This led me to design a custom A4 paper header.
I adapted my screen cover to fit the A4 format.
During this process, I realized that the matrix could also map ISO elements,
allowing me to better understand the entire process flow.
This realization inspired me to craft an ISO element matrix document,
complete with a well-designed header.
It became easier to build these forms from scratch.

Eventually, I pushed myself further by using Inkscape
to create formal internal SOP documents related to ISO.
This series will guide you through crafting SOP templates,
starting from the cover page, and expanding to include headers,
workflow diagrams, and other ISO 9001:2015-specific elements.

#### Preview

This is the result of the document.

![Inkscape - A4 Template - SOP - Example Pages][011-okular-four-pages]

Inkscape source image are available for each steps.

#### Why Inkscape over Word Processors?

Here are my considerations:

1. It is easier to align shapes in Inkscape,
   compared to MS Word or LibreOffice Writer
   Excel and Calc are simply not vector-image-friendly.

2. For diagram-intensive documents,
   I prefer Inkscape over LibreOffice Draw.
    It is easier to work with and faster to complete.

If a document contains mostly text with very few diagrams,
I would rather use a word processor.
For example, when writing minutes of meetings (MoM),
a good word processor is the better choice.
However, if I know the document will contain many diagrams,
Inkscape becomes my preferred application.

That said, using Inkscape for office documents comes with a trade-off.
**Only I can easily edit these files**.
I accept that risk because I enjoy working this way.
Besides, nobody really cares which tool I use,
as long as the document is delivered.

Inkscape is my choice for serious reports.

-- -- --

### 1: Cover for Screen Document

If you works with a lot of document,
you likely to focus on the document forget detail,
and forget about the cover.

I have a bunch of document, but I never have a nice cover.
So one day I decide to make a few original cover,
for general purpose document.
Most are not good, since I'm not an artist anyway.
But I have this one, that is good enough to be shared for you.

![Inkscape - Screen Template - Cover][101-cover-hexa-s]

My intention is, after reading this article,
you can make your own cover.
But it is okay if you want to use this cover directly.

#### SVG Image Source

> Multi Layered

I've prepared an example cover for you,
so there's no need to generate a new one.

* [cover-hexa-ginger.svg][cover-hexa-ginger-svg]

This document use multi layer,
you can experiment by turning off each layer visibility,
by clicking eye icon.

![Inkscape - Screen Cover - Complete Layer][103-layer-complete]

#### Page Ratio

My most screen document use this kind of ratio:

1. Official Presentation: 1600px * 900px.
2. Rectangle: 1000px x 1000px.
3. Following Content: Random Size

The rectangle is like a scratch pad,
where I begin to draw anything.
After sometimes, the size is usually in needed to be adjusted.
Stretched horizontally, or vertically to fit the diagram.
The final result will be embedded in official presentation.

Sometimes I need a quick informal explanation for my colleague,
and do not have time to bundle into proper presentation.
For example, this below is my early accounting proposal:

![Inkscape - Screen - Accounting Flow][012-bagan-alur]

This is the actual PDF that I have sent to my colleagues.
No need proper size, as long as my friend understand my intention.

#### Page Setup

Let's start with rectangle of 1500px x 1500px.

![Inkscape - Screen Cover - Page Setup][102-page-setup]

Then fill background with rectangle with size of 1500px x 1500px.

![Inkscape - Screen Cover - Simple Layer][104-layer-simple]

Then create guidance layer, to divide the page in three region (top, middle, bottom),
all with the same size of 500px height x 1500px width.
Use different color other than white, so we can easily spot the guidance.

![Inkscape - Screen Cover - Guidance Region][105-guidance-region]

-- -- --

### 1: Hexagonal Shape

You can use any shape to fit your needs.
I try hexagonal and it works so far.
Maybe I should try something else later.

#### Background

Let's make a rectangle widht 300px center in the middle 0f 500px rectangle.

#### Hexagonal Shape

Let's make a diamond shape with size of 500px height and 900px width.
I use Add Corners LPE to smooth the corner.

![Inkscape - Screen Cover - Diamond Based Shape][106-diamond-base]

Then duplicate to make a smaller one with size [w: 770px, h: 400px],
Let's continue with [w: 640px, h: 300px], downto [w: 510px, h: 200px], 
and finally [w: 380px, h: 100px].

With this five shape stacked on each other,
use 50% opacity for all shapes.
and set different color scale for each shapes.

![Inkscape - Screen Cover - Stacked Hexagonal][107-hexa-stacked]

You can try different shape as well.

#### Shape Rotation

Apply rotation to each shape differently.
All shape with -30°, exclude the biggest shape,
then apply -30°  again, and so on for each shape.
You can try different angle.

![Inkscape - Screen Cover - Hexa - Rotate with -30°][111-rotate-30]

Add effect for each shape,
such as drop shadow of cut glow.

![Inkscape - Screen Cover - Hexa - Color and Effect][112-rotate-color]

You may try other variation as well.

#### Photo Clip

Duplicate one shape, this will be used for photo clip.

![Inkscape - Screen Cover - CHexa - olor and Effect][113-duplicate]

Duplicate again, this will be used for hard shadow.
Let's apply drop shadow with blur 10px, x-offset 5px, y-offset 5px,
but using outer cutout only.

![Inkscape - Screen Cover - Hexa - Hard Shadow][114-hard-shadow]

Now we can use the original shape for photo clip.
Put the photo behind the shape. Select both, and set the clip.

![Inkscape - Screen Cover - Hexa - Set Clip][115-set-clip]

The cat will definitely stay.

![Inkscape - Screen Cover - Hexa - Final Cat][116-final-cat]

-- -- --

### 1: Information Text

Let's do it layer by layer.

#### Guidance

First, make a guidance region where our text would fit in the page.

![Inkscape - Screen Cover - Info - Guidance][121-guidance]

#### Text

Then add text or any line attribute, in that region.

![Inkscape - Screen Cover - Info - Text][122-info-text]

After finished, hide the guidance layer.

![Inkscape - Screen Cover - Info - Text Only][123-info-text-only]

Now the page shown only the necessary text.

-- -- --

### 1: Left Decoration

I stacked three shapes.
All based on rectangles,
but each bottom right corner position altered.
I set 50% opacity for all shapes.
I set different color scale for each shapes.
The result would be:

![Inkscape - Screen Cover - Left Decoration - Shape][131-left-decoration]

I use Add Corners LPE to smooth the altered bottom right corner.

![Inkscape - Screen Cover - Left Decoration - Add Corners LPE][132-add-corners-lpe]

The slight drop shadow is set,
using previously made from filter editor.

![Inkscape - Screen Cover - Left Decoration - Filter Editor][133-filter-editor]

-- -- --

### 1: Logo

Add last, I added logo on the right top region.
And also apply a drop shadow,
using available filter from filter editor.

![Inkscape - Screen Cover - Logo][134-logo-right-top]

Show all layer, but hide all guidance.
Now we finally get our complete cover for use with screen template

![Inkscape - Screen Cover - Complete][135-complete-cover]

-- -- --

### 2: Cover for A4 Paper

I setup an A$ paper,
then copy all the layer from previous SVG to this image.
This need a few size and position adjustment.

#### SVG Image Source

> Multi Layered

I've prepared an example cover for you,
so there's no need to generate a new one.

* [procedure-01-cover.svg][procedure-cover-svg]

#### Result

I also adjust the color, and the text to suit my need.
The result can be seen here.

![Inkscape - A4 Paper Cover - Complete][141-procedure-cover]

I never actually printed the result.
But it looks nice in PDF.

Again, it is just one idea of cover.
You can make your own original cover,
as long as you are willing to spend enough time.

-- -- --

### What is Next 🤔?

Having a document cover is nice,
but what we need is page layout example,
and also document content template.

We are going to deal with document content first,
strating from flowchart diagram.

Feel free to keep reading: [ [Inkscape SOP Docs - Diagram][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}design/2021/01/07/inkscape-template-sop-diagram/

[cover-hexa-ginger-svg]:{{< baseurl >}}assets/posts/design/2021/01/cover-hexa-ginger.svg
[procedure-cover-svg]:  {{< baseurl >}}assets/posts/design/2021/01/procedure-01-cover.svg

[//]: <> ( -- -- -- links below -- -- -- )

[011-okular-four-pages]:{{< baseurl >}}assets/posts/design/2021/01/011-okular-four-pages-crop.png
[012-bagan-alur]:       {{< baseurl >}}assets/posts/design/2021/01/012-bagan-alur.png
[101-cover-hexa-s]:     {{< baseurl >}}assets/posts/design/2021/01/101-cover-hexa-ginger-s.png
[102-page-setup]:       {{< baseurl >}}assets/posts/design/2021/01/102-page-setup.png
[103-layer-complete]:   {{< baseurl >}}assets/posts/design/2021/01/103-layer-complete.png
[104-layer-simple]:     {{< baseurl >}}assets/posts/design/2021/01/104-layer-simple.png
[105-guidance-region]:  {{< baseurl >}}assets/posts/design/2021/01/105-guidance-region.png
[106-diamond-base]:     {{< baseurl >}}assets/posts/design/2021/01/106-diamond-base.png
[107-hexa-stacked]:     {{< baseurl >}}assets/posts/design/2021/01/107-hexa-stacked.png

[111-rotate-30]:        {{< baseurl >}}assets/posts/design/2021/01/111-rotate-30.png
[112-rotate-color]:     {{< baseurl >}}assets/posts/design/2021/01/112-rotate-color.png
[113-duplicate]:        {{< baseurl >}}assets/posts/design/2021/01/113-duplicate.png
[114-hard-shadow]:      {{< baseurl >}}assets/posts/design/2021/01/114-hard-shadow.png
[115-set-clip]:         {{< baseurl >}}assets/posts/design/2021/01/115-set-clip.png
[116-final-cat]:        {{< baseurl >}}assets/posts/design/2021/01/116-final-cat.png

[121-guidance]:         {{< baseurl >}}assets/posts/design/2021/01/121-guidance.png
[122-info-text]:        {{< baseurl >}}assets/posts/design/2021/01/122-info-text.png
[123-info-text-only]:   {{< baseurl >}}assets/posts/design/2021/01/123-info-text-only.png

[131-left-decoration]:  {{< baseurl >}}assets/posts/design/2021/01/131-left-decoration.png
[132-add-corners-lpe]:  {{< baseurl >}}assets/posts/design/2021/01/132-add-corners-lpe.png

[133-filter-editor]:    {{< baseurl >}}assets/posts/design/2021/01/133-filter-editor.png
[134-logo-right-top]:   {{< baseurl >}}assets/posts/design/2021/01/134-logo-right-top.png
[135-complete-cover]:   {{< baseurl >}}assets/posts/design/2021/01/135-complete-cover.png

[141-procedure-cover]:  {{< baseurl >}}assets/posts/design/2021/01/141-procedure-01-cover.png
