---
type   : post
title  : "Inkscape SOP Docs - Diagram"
date   : 2021-01-07T09:17:35+07:00
slug   : inkscape-template-sop-diagram
categories: [design]
tags      : [inkscape]
keywords  : [SOP, ISO, Diagram, Paper, A4]
author : epsi
opengraph:
  image: assets/posts/design/2021/11/11-cert-testbed-03.png

toc    : "toc-2021-inkscape-sop"

excerpt:
  SOP related template using inkscape.
  Simple flowchart diagram.

---

### Preface

> SOP related template using inkscape.
> Simple flowchart diagram.

Visualising work instruction with flowchart diagram,
is bettter than long paragraphs.

We can start with common flowchart,
before discussing about standarized BPML.

I don't have many word to say,
let's get into our first example.

-- -- --

### Example 1: Accounting Flow

#### Preview

This is the result of the document.

![Inkscape - Flowchart Diagram - Accounting Flow][acc-flow-png]

#### SVG Image Source

> Multi Layered

I've prepared an example cover for you,
so there's no need to generate a new one.

* [acc-flow.svg][acc-flow-svg]

We will discuss layer by layer.

#### Page Setup for Screen

I usually use 250px witdh for single line vertical flowchart.
since this accounting have six horizontal sections,
then my width 1500.

So let's make the page dimension as 1500px width and 600px height.

![Inkscape - Flowchart Diagram - Page Setup for Screen][211-page-setup]

#### Layer

I use seven layers:

* Background
* Guidance
* Partition
* Title
* Artefact
* Connector
* Text

![Inkscape - Flowchart Diagram - Layer][212-layer]

#### Background

First time to do, I always set white background,
covering all page width and height.

![Inkscape - Flowchart Diagram - Background][213-background]

#### Guidance

Each six section has two guidance panels

* Header: 250px width and 50px width
* Content: 250px width and 550px width

![Inkscape - Flowchart Diagram - Guidance][214-guidance]

I give each panel, different gradient scale.
Most of the time, I hide this panel, because I like simple white.
But using this color as background, can create good accent to.
SO sometimes, I have this layer to be visible.

#### Partition

For aesthetic reason I put a line to separate each panels.
Each have thickness with only 2px.

![Inkscape - Flowchart Diagram - Partition][215-partition]

#### Title

Now we can put header text on top of each header panel.

![Inkscape - Flowchart Diagram - Title][216-title]

I align the text using panel.
So the text centered in the middle.
Horizontally and vertically.

#### Artefact

> The Shape

Now it is time to put the flowchart diagram.
Let's put each shape centered in each section's panel.

For example start has dimension as 120px * 40px.
The placement is 65px * 70 px.

![Inkscape - Flowchart Diagram - Artefact][217-artefact]

Then I add a slight drop shadow.

#### Connector

Also align the connector.
I usually align vertical arrow in the middle of the shape.

![Inkscape - Flowchart Diagram - Connector][218-connector]

#### Text

Then I add so many description.
Pour anything in my mind.

![Inkscape - Flowchart Diagram - Text][219-text]

#### Final

Unhiding guidance,
we can see the final result as below:

![Inkscape - Flowchart Diagram - Final][219-final]

I use this diagram a lot.
It is easy to use.

You can have the image source from above link.

-- -- --

### Example 2: Macro/Pivot Cases

This is a slightly complex diagram.
We can separate complex diagram by using reference.
And also differ the shape between process and documents.

#### Preview

This is the result of the document.

![Inkscape - Flowchart Diagram - Pivot Cases][pivot-cases-png]

I use this diagram for my article series.

* [Pivot - Python - Overview][pivot-python-overview]

#### SVG Image Source

> Multi Layered

I've prepared an example cover for you,
so there's no need to generate a new one.

* [pivot-cases.svg][pivot-cases-svg]

This diagram has 1600px width and 1100px height.

#### Layer

This flowchart diagram has the same layers as previous.
I don't think that I need to explain layer by layer.

![Inkscape - Macro/Pivot Cases - Layer][222-layer]

You can have the image source from above link.

-- -- --

### What is Next 🤔?

I haven't finished with flowchart diagram yet.
There is also BPML diagram, but I haven't got the material ready yet.
So let's skip it for while.

We go directly to page layout with matrix skill example.

Consider continuing with: [ [Inkscape SOP Docs - Matrix Skill][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

-- -- --

### Conclusion

Done. Finished.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}design/2021/01/11/inkscape-template-sop-matrix/

[pivot-python-overview]:https://epsi.bitbucket.io/automation/2022/03/01/pivot-model-01/

[acc-flow-png]:         {{< baseurl >}}assets/posts/design/2021/01/acc-flow.png
[acc-flow-svg]:         {{< baseurl >}}assets/posts/design/2021/01/acc-flow.svg
[pivot-cases-png]:      {{< baseurl >}}assets/posts/design/2021/01/pivot-cases.png
[pivot-cases-svg]:      {{< baseurl >}}assets/posts/design/2021/01/pivot-cases.svg

[211-page-setup]:       {{< baseurl >}}assets/posts/design/2021/01/211-page-setup.png
[212-layer]:            {{< baseurl >}}assets/posts/design/2021/01/212-layer.png
[213-background]:       {{< baseurl >}}assets/posts/design/2021/01/213-background.png
[214-guidance]:         {{< baseurl >}}assets/posts/design/2021/01/214-guidance.png
[215-partition]:        {{< baseurl >}}assets/posts/design/2021/01/215-partition.png
[216-title]:            {{< baseurl >}}assets/posts/design/2021/01/216-title.png
[217-artefact]:         {{< baseurl >}}assets/posts/design/2021/01/217-artefact.png
[218-connector]:        {{< baseurl >}}assets/posts/design/2021/01/218-connector.png
[219-text]:             {{< baseurl >}}assets/posts/design/2021/01/219-text.png
[219-final]:            {{< baseurl >}}assets/posts/design/2021/01/219-final.png
[222-layer]:            {{< baseurl >}}assets/posts/design/2021/01/222-layer.png
