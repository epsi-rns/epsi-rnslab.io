---
type   : post
title  : "Inkscape SOP Docs - Matrix Skill"
date   : 2021-01-11T09:17:35+07:00
slug   : inkscape-template-sop-matrix
categories: [design]
tags      : [inkscape]
keywords  : [SOP, ISO, Diagram, Paper, A4]
author : epsi
opengraph:
  image: assets/posts/design/2021/01/116-final-cat.png

toc    : "toc-2021-inkscape-sop"

excerpt:
  SOP related template using inkscape.
  Harvey matrix skill in A4 paper page.

---

### Preface

> SOP related template using inkscape.
> Harvey matrix skill in A4 paper page.

We are going to start from setting up the A4 paper page.
And setting up layout for Harvey Matrix later.

-- -- --

### 1: A4 Page Layout

The main layout consist of page layout and header layout.

#### Header and Body

The A4 dimension is 210 mm width and 297 mm height.
With margin 20 mm form the edge,
we can have 170 mm width and 257 mm height.

Now we have the size of header and body.
The size of the body depend on the header height.
Assuming the header have 40 mm height,
the body height will be 217 mm height.

![Inkscape - A4 Page Layout - Header and Body][cm-page-png]

### Header Layout

The header size depend on what field need to be availabe in the header.
Header are varies from document type to other document type.
But consist among one particular type.
For our current header, the size is 40 mm and 170 mm.

This has three part:

* Logo and Title: 20 mm height
* Document Information: 12 mm height
* Document Signed: 8 mm height

The main dimension header can be seen in this figure below.
If you need more detail,
you can see in the inkscape document directly.

![Inkscape - A4 Page Layout - Header Layout][cm-header-png]

You can design header with any kind of layout that you want.
Any size, any column placement.
After a while, you might need revision.
Sometimes change are inevitable.
Nothing last forever.

-- -- --

### Matrix Skill

And setting up layout for Harvey Matrix later.


#### Getting the page right.




-- -- --

### What is Next 🤔?



Feel free to keep reading: [ [Inkscape SOP Docs - Diagram][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}design/2021/01/07/inkscape-template-sop-diagram/

[cover-hexa-ginger-svg]:{{< baseurl >}}assets/posts/design/2021/01/cover-hexa-ginger.svg
[procedure-cover-svg]:  {{< baseurl >}}assets/posts/design/2021/01/procedure-01-cover.svg

[cm-header-png]:        {{< baseurl >}}assets/posts/design/2021/01/cm-header.png
[cm-page-png]:          {{< baseurl >}}assets/posts/design/2021/01/cm-page.png

[//]: <> ( -- -- -- links below -- -- -- )
