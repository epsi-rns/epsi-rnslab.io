---
type   : post
title  : "Inkscape Extension - Part One"
date   : 2021-11-01T09:17:35+07:00
slug   : inkscape-extension-01
categories: [design]
tags      : [inkscape, python]
keywords  : [extension]
author : epsi
opengraph:
  image: assets/posts/design/2021/11/11-cert-testbed-04.png

toc    : "toc-2021-inkscape-auto"

excerpt:
  Introduction to inkscape extension.

---

### Preface

> Introduction to inkscape extension.

Inkscape is a powerful design tool,
one of them is the inkscape extension.
Inkscape extension bring python programming to design.

I can automate repetitive task using python,
saving a lot of working hour.
Manual task is also error prone,
inkscape automation reduce human error.

However, there is somewhere where you should start with.

-- -- --

### Getting Started

#### Reading

I'm not sure where the landing page is,
but I find this link is useful.

* [wiki/Category:Extensions][category]

#### Official Example

The Offical Example is, of course the inkscape itself.

![Inkscape Extension - Menu][extension-menu]

Now, where is the extension directory?

There are two kind of directory

* System Directory

* User Directory

Note that Windows and Linux has different path.

#### System Directory

Here lies the directories where you can find good examples.

* Linux: `/usr/share/inkscape/extensions`

* Windows: `C:\\Program Files\Inkscape\share\inkscape\extensions`

![Inkscape Extension - System Directory][extension-system-dir]

I suggest that you start with colors.
They are short, thus easy to learn with.

#### User Directory

Now it is time for you to start your own extension.
So prepare your directory.

* Linux: `~/.config/inkscape/extensions`

* Windows: `C:\\Users\[username]\AppData\Roaming\Inkscape\extensions`

#### A Pair of Files

As you can see above, to make an extension you need two pair of file.

* The inx file: contain an XML file for menu setting.

* The py file: contain the python code

#### XML Parameters

You can read the details of the XML parameters here:

* [Extensions: INX widgets and parameters][params]

-- -- --

### Generate Extension

Consider begin with something easy, the generating hello world.

#### Artefacts

We need two files.

* `hello.inx`
* `hello.py`

Put these two files below on your user directoy.
In order to have the menu activated,
you should restart your inkscape.

![Hello World - Menu][hello-menu]

#### Hello World XML

* [gitlab.com/.../extensions/hello.inx][src-inx-hello]

{{< highlight xml >}}
<?xml version="1.0" encoding="UTF-8"?>
<inkscape-extension
  xmlns="http://www.inkscape.org/namespace/inkscape/extension">
  <name>Test Hello</name>
  <id>epsi.hello</id>
  <param name="greeting_text" type="string"
         gui-text="Greeting Text:">Hello World</param>
  <effect>
    <object-type>all</object-type>
    <effects-menu>
       <submenu name="Epsi"/>
    </effects-menu>
  </effect>
  <script>
    <command location="inx" interpreter="python">hello.py</command>
  </script>
</inkscape-extension>
{{< / highlight >}}

#### Hello World 

> Start with importing `inkex`

* [gitlab.com/.../extensions/hello.py][src-py--hello]

{{< highlight python >}}
#!/usr/bin/env python
import inkex
from inkex.elements import TextElement

class Hello(inkex.GenerateExtension):
    def add_arguments(self, pars):
        pars.add_argument("--greeting_text",
            type=str, default="Yellow Here",
            help="Cute Greeting Text")

    def generate(self):
      textElement = TextElement()
      textElement.text = str(self.options.greeting_text)
      return textElement

if __name__ == '__main__':
    Hello().run()
{{< / highlight >}}

#### What Does It Do?

This simply ask for a greet parameter,
then create a text element.

![Hello Extension - Preview][hello-preview]

-- -- --

### Effect Extension

Instead of generate something new,
inkscape extension can read any object,
and alter the properties.

Or you can also generate any shape,
on a selected layer.

#### Official Tutorial

Consider begin with something official form inkscape.

The official tutorial is here:

* [Generating objects from extensions][generate]

I change a little bit to suit my needs.

#### Artefacts

We need two files.

* `rectangle.inx`
* `rectangle.py`

![Rectangle - Menu][rectangle-menu]

#### Rectangle XML

* [gitlab.com/.../extensions/rectangle.inx][src-inx-rectangle]

{{< highlight xml >}}
<?xml version="1.0" encoding="UTF-8"?>
<inkscape-extension
  xmlns="http://www.inkscape.org/namespace/inkscape/extension">
  <name>Test Rectangle</name>
  <id>epsi.rectangle</id>
  <effect>
    <object-type>all</object-type>
    <effects-menu>
       <submenu name="Epsi"/>
    </effects-menu>
  </effect>
  <script>
    <command location="inx"
             interpreter="python">rectangle.py</command>
  </script>
</inkscape-extension>
{{< / highlight >}}

#### Rectangle Python

The structure of the extension is shown as below.

* [gitlab.com/.../extensions/rectangle.py][src-py--rectangle]

{{< highlight python >}}
#!/usr/bin/env python

import inkex, simplestyle
from lxml import etree

def draw_SVG_square(w,h, x,y, parent):
    ...

class Kotak(inkex.EffectExtension):
  def effect(self):
    parent = self.svg.get_current_layer()
    draw_SVG_square(100,100, 0,0, parent)

if __name__ == '__main__':
  Kotak().run()
{{< / highlight >}}

While the `draw_SVG_square` function is as below:

{{< highlight python >}}
def draw_SVG_square(w,h, x,y, parent):
    style = { 'stroke'        : 'none',
              'stroke-width'  : '1',
              'fill'          : '#000000'
            }

    attribs = {
        'style'     : str(inkex.Style(style)),
        'height'    : str(h),
        'width'     : str(w),
        'x'         : str(x),
        'y'         : str(y)
            }
    circ = etree.SubElement(
        parent, inkex.addNS('rect','svg'), attribs )
    return circ
{{< / highlight >}}

Notice the `inkex namespace` and `inkex style`.

#### What Does It Do?

This simply ask for a greet parameter,
then create a text element.

![Rectangle Extension - Result][rectangle-result]

#### XML Editor

As you can see above,
the `draw_SVG_square` functions looks like a CSS.

In fact it is an XML properties.

![Rectangle Shape - XML Editor][rectangle-xml]

We are going to deal a lot with XML stuff later.

-- -- --

### What is Next ?

Consider continue reading [ [Inkscape Automation - Part Two][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}design/2021/11/03/inkscape-extension-02/

[src-inx-hello]:        {{< anycode >}}/inkscape/extensions/hello.inx
[src-py--hello]:        {{< anycode >}}/inkscape/extensions/hello.py
[src-inx-rectangle]:    {{< anycode >}}/inkscape/extensions/rectangle.inx
[src-py--rectangle]:    {{< anycode >}}/inkscape/extensions/rectangle.py

[category]:     https://wiki.inkscape.org/wiki/Category:Extensions
[params]:       https://wiki.inkscape.org/wiki/Extensions:_INX_widgets_and_parameters
[generate]:     https://wiki.inkscape.org/wiki/Generating_objects_from_extensions

[extension-menu]:       {{< baseurl >}}assets/posts/design/2021/11/01-inkscape-extension-menu.png
[extension-system-dir]: {{< baseurl >}}assets/posts/design/2021/11/02-inkscape-extension-directory.png

[hello-menu]:       {{< baseurl >}}assets/posts/design/2021/11/03-test-hello-menu.png
[hello-preview]:    {{< baseurl >}}assets/posts/design/2021/11/03-hello-earth-preview.png
[rectangle-menu]:   {{< baseurl >}}assets/posts/design/2021/11/04-rectangle-menu.png
[rectangle-result]: {{< baseurl >}}assets/posts/design/2021/11/04-black-rectangle.png
[rectangle-xml]:    {{< baseurl >}}assets/posts/design/2021/11/04-xml-editor.png
