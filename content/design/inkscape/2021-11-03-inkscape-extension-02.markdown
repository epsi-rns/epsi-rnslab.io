---
type   : post
title  : "Inkscape Extension - Part Two"
date   : 2021-11-03T09:17:35+07:00
slug   : inkscape-extension-02
categories: [design]
tags      : [inkscape, python]
keywords  : [extension]
author : epsi
opengraph:
  image: assets/posts/design/2021/11/11-cert-testbed-01.png

toc    : "toc-2021-inkscape-auto"

excerpt:
  Query layer using inkscape extension.

---

### Preface

> Query layer using inkscape extension.

Here we are going to learn to access XML tree.

-- -- --

### The XML Properties

#### Example SVG

> The Test Bed

We need any working SVG file with layer.
You can choose any of these two test bed:

* [cert-testbed-04.svg][cert-testbed-04-svg]

![Certificate Test Bed 04][cert-testbed-04]

#### Layers

Consider open one of those,
and examine all layers.

![All Layers][cert-layers-alt]

#### Layer in XML

> Group Mode

Again, examine all layers in XML representation.

![All Layers in XML Editor][cert-layers-in-xml]

You can spot the layer properties in XML editor.
It has 'inkscape:groupmode` as `layer`.

This is the basic of our query.

#### Text Editor

You can also open the file using any text editor,
and do some find tools.

![All Layers in Text Editor][cert-layers-in-text]

-- -- --

### Query Layer Extension

#### Artefacts

We need two files.

* `query_layer.inx`
* `query_layer.py`

#### Query Layer XML

* [gitlab.com/.../extensions/query_layer.inx][src-inx-query]

{{< highlight xml >}}
<?xml version="1.0" encoding="UTF-8"?>
<inkscape-extension
  xmlns="http://www.inkscape.org/namespace/inkscape/extension">
  <name>Query Layer</name>
  <id>epsi.query_layer</id>
  <effect>
    <object-type>all</object-type>
    <effects-menu>
       <submenu name="Epsi"/>
    </effects-menu>
  </effect>
  <script>
    <command location="inx"
             interpreter="python">query_layer.py</command>
  </script>
</inkscape-extension>
{{< / highlight >}}

#### Query Layer Python

The structure of the extension is shown as below.

* [gitlab.com/.../extensions/query_layer.py][src-py--query]

{{< highlight python >}}
#!/usr/bin/env python
import inkex

class QueryLayer(inkex.EffectExtension):
  def effect(self):
    self.recurse(1, self.svg)

  ...

if __name__ == "__main__":
    QueryLayer().run()
{{< / highlight >}}

Notice the `self.svg` as the root node.

#### Recurse Method

Recurse and the supporting method is as below:

{{< highlight python >}}
class QueryLayer(inkex.EffectExtension):
  ...

  def recurse(self, level, nodes):
    for node in sorted(nodes, key =  self._sort):
      if isinstance(node, inkex.ShapeElement):
        if node.get('inkscape:groupmode') == 'layer':
          self.print_layer_name(level, node)
          self.recurse(level+1, node)

  def print_layer_name(self, level, node):
    label_name = node.get('inkscape:label')
    inkex.errormsg('-'*level + ' ' +label_name)
{{< / highlight >}}

Notice the `get` method from `node` to get any property,
such as `inkscape:groupmode`.

#### Sorting

Instead of filter, we can also add sort feature as below method:

{{< highlight python >}}
  def _sort(self, node):
    if node.get('inkscape:groupmode') == 'layer':
      return node.get('inkscape:label')
    else:
      return  ""
{{< / highlight >}}

_

#### What Does It Do?

This will throw the result using `inkex.errormsg`.

![Query Layer Result - Error Message][cert-error-msg-alt]

#### Caveat

The code above rely on this code

{{< highlight python >}}
if node.get('inkscape:groupmode') == 'layer':
{{< / highlight >}}

For complex layer, this approach does not always work.
I prefer to use `inkscape:label` instead.

{{< highlight python >}}
if node.get('inkscape:label') is not None:
{{< / highlight >}}

#### Final Code

Now our final code will be:

{{< highlight python >}}
#!/usr/bin/env python
import inkex

class QueryLayer(inkex.EffectExtension):
  def effect(self):
    self.recurse(1, self.svg)

  def recurse(self, level, nodes):
    for node in sorted(nodes, key =  self._sort):
      if isinstance(node, inkex.ShapeElement):
        if node.get('inkscape:label') is not None:
          self.print_layer_name(level, node)
          self.recurse(level+1, node)

  def print_layer_name(self, level, node):
    label_name = node.get('inkscape:label')
    inkex.errormsg('-'*level + ' ' +label_name)

  def _sort(self, node):
    if node.get('inkscape:label') is not None:
      return node.get('inkscape:label')
    else:
      return  ""

if __name__ == "__main__":
    QueryLayer().run()
{{< / highlight >}}

-- -- --

### What is Next ?

Consider continue reading [ [Inkscape Extension - Part Three][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}design/2021/11/05/inkscape-extension-03/

[src-inx-query]:        {{< anycode >}}/inkscape/extensions/query_layer.inx
[src-py--query]:        {{< anycode >}}/inkscape/extensions/query_layer.py

[cert-testbed-01]:      {{< baseurl >}}assets/posts/design/2021/11/11-cert-testbed-01.png
[cert-testbed-02]:      {{< baseurl >}}assets/posts/design/2021/11/11-cert-testbed-02.png
[cert-testbed-04]:      {{< baseurl >}}assets/posts/design/2021/11/11-cert-testbed-04.png
[cert-testbed-01-svg]:  {{< baseurl >}}assets/posts/design/2021/11/cert-testbed-01.svg
[cert-testbed-02-svg]:  {{< baseurl >}}assets/posts/design/2021/11/cert-testbed-02.svg
[cert-testbed-04-svg]:  {{< baseurl >}}assets/posts/design/2021/11/cert-testbed-04.svg
[cert-layers]:          {{< baseurl >}}assets/posts/design/2021/11/12-layers.png
[cert-layers-alt]:      {{< baseurl >}}assets/posts/design/2021/11/12-layers-alt.png
[cert-layers-in-xml]:   {{< baseurl >}}assets/posts/design/2021/11/12-layers-in-xml.png
[cert-layers-in-text]:  {{< baseurl >}}assets/posts/design/2021/11/12-text-editor.png
[cert-error-msg]:       {{< baseurl >}}assets/posts/design/2021/11/13-err-msg.png
[cert-error-msg-alt]:   {{< baseurl >}}assets/posts/design/2021/11/13-err-msg-alt.png
