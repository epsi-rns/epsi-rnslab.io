---
type   : post
title  : "Inkscape Extension - Part Three"
date   : 2021-11-05T09:17:35+07:00
slug   : inkscape-extension-03
categories: [design]
tags      : [inkscape, python]
keywords  : [extension]
author : epsi
opengraph:
  image: assets/posts/design/2021/11/11-cert-testbed-02.png

toc    : "toc-2021-inkscape-auto"

excerpt:
  Duplicate layer using inkscape extension.

---

### Preface

> Duplicate layer using inkscape extension.

Here we are going to learn to alter XML tree.

-- -- --

### Preparation

Put your replacable text in a layer named `Template`.
This template layer is belong to to level layer called `Container`.

![Template inside Container Layer][cert-layer-template]

Now, also consider to have a look at the XML properties:

![Layer Template in XML Editor][layer-template-xml]

Notice that there is a child the `text`,
and a grandchild the `tspan`.
We are going to alter the text inside the `tspan`.

-- -- --

### Duplicate Layer Extension

#### Artefacts

We need two files.

* `duplicate_layer.inx`
* `duplicate_layer.py`

#### Duplicate Layer XML

* [gitlab.com/.../extensions/duplicate_layer.inx][src-inx-duplicate]

{{< highlight xml >}}
<?xml version="1.0" encoding="UTF-8"?>
<inkscape-extension
  xmlns="http://www.inkscape.org/namespace/inkscape/extension">
  <name>Duplicate Layers</name>
  <id>epsi.duplicate</id>
  <effect>
    <object-type>all</object-type>
    <effects-menu>
       <submenu name="Epsi"/>
    </effects-menu>
  </effect>
  <script>
    <command location="inx"
             interpreter="python">duplicate_layer.py</command>
  </script>
</inkscape-extension>
{{< / highlight >}}

#### Duplicate Layer Python

Now we are ready to go to the coding part using python.

The structure of the extension is shown as below.

{{< highlight python >}}
#!/usr/bin/env python

import inkex
import sys, copy

class DuplicateLayer(inkex.EffectExtension):
  ...

if __name__ == '__main__':
  DuplicateLayer().run()

{{< / highlight >}}

Notice that we need to import `copy` for object duplication.

#### Effect Method

This is the main method in our inkscape extension.

{{< highlight python >}}
  def effect(self):
    all_layers   = self.get_layers()
    container    = self.find_layer(all_layers,'Container')
    source_layer = self.find_layer(all_layers, 'Template')

    self.duplicate_layer(
      container, source_layer,
      'Zeta Mataharani', 'Zeta Mataharani, MD')
{{< / highlight >}}

We are using __hardcoded__ layer name,
such as `Container` and `Template`.

I will explain the `duplicate_layer` method, later.
For now, we discuss the result first.

#### Result

*Disclaimer*: I'have been using the `Mataharani` name,
as example mockup for more than one decades.
even my home wifi SSID is `Mataharani`.

The name `Zeta Mataharani` will be used for layer name

![Certificate - ZM][cert-layer-template]

and the `Zeta Mataharani, MD` will be used for display text in `tspan`.

![Certificate Test Bed 04 - ZM MD][cert-testbed-04-zm]

Now, consider go back to see how the code works under the hood.
Consider begin with the `get_layers` method,
then `duplicate_layer` method, and the rest following.


-- -- --

### Finding Layers

Consider begin with the `get_layers` method,
then `duplicate_layer` method, and the rest following.

#### Get Layers

It is simply list comprehension.
Or to be exact, dict comprehension.

This look for any layer object.

{{< highlight python >}}
  def get_layers(self):
    return {g
        for g in self.svg.xpath('//svg:g')
        if g.get('inkscape:groupmode') == 'layer'
      }
{{< / highlight >}}

We will be using this as:

{{< highlight python >}}
    def effect(self):
    all_layers   = self.get_layers()
    container    = self.find_layer(all_layers,'Container')
    source_layer = self.find_layer(all_layers, 'Template')
{{< / highlight >}}

So how to find the layer?

#### First Occurence

This is just about finding first occurence of layer label.

{{< highlight python >}}
  def find_layer(self, layers, label_name):
    for layer in layers:
      name = layer.get('inkscape:label')
      if name == label_name:
        return layer

    return None
{{< / highlight >}}

I'm not sure about my code.
This above works, but I guess there is better way to do this.
After all this is not a javascript DOM library.

-- -- --

### Duplicate Layer

#### Deepcopy

The main course is just a deepcopy

{{< highlight python >}}
    new_layer = copy.deepcopy(source_layer)
    container.append(new_layer)
{{< / highlight >}}

Then append it to `Container` layer.

#### Alter Name and Visibility

But I begin to think about, changing the layer label and the visibility.
So I have this code

{{< highlight python >}}
  def duplicate_layer(self, container, source_layer, layer_label):
    new_layer = copy.deepcopy(source_layer)
    new_layer.label = layer_label
    new_layer.style = 'display:none'
    container.append(new_layer)
{{< / highlight >}}

#### Alter Text

And more feature, change the `tspan` text.
The `Template` layer might has one or a few `TextElement`,
but each `TextElement` only has one `tspan`.

{{< highlight python >}}
    for node in new_layer:
      if isinstance(node, inkex.TextElement):
        tspan = node[0]
        tspan.text = text_span
{{< / highlight >}}

#### Final Method

Finally we have this

{{< highlight python >}}
    def duplicate_layer(self, container, source_layer, layer_label, text_span):
    new_layer = copy.deepcopy(source_layer)
    new_layer.label = layer_label
    new_layer.style = 'display:none'
    container.append(new_layer)

    for node in new_layer:
      if isinstance(node, inkex.TextElement):
        tspan = node[0]
        tspan.text = text_span
{{< / highlight >}}

-- -- --

### Complete Code

For your comfort, this below is the complete code.

* [gitlab.com/.../extensions/duplicate_layer.py][src-py--duplicate]

{{< highlight python >}}
#!/usr/bin/env python

import inkex
import sys, copy

class DuplicateLayer(inkex.EffectExtension):
  def get_layers(self):
    return {g
        for g in self.svg.xpath('//svg:g')
        if g.get('inkscape:groupmode') == 'layer'
      }

  def find_layer(self, layers, label_name):
    for layer in layers:
      name = layer.get('inkscape:label')
      if name == label_name:
        return layer

    return None

  def duplicate_layer(self, container, source_layer, layer_label, text_span):
    new_layer = copy.deepcopy(source_layer)
    new_layer.label = layer_label
    new_layer.style = 'display:none'
    container.append(new_layer)

    for node in new_layer:
      if isinstance(node, inkex.TextElement):
        tspan = node[0]
        tspan.text = text_span

  def effect(self):
    all_layers   = self.get_layers()
    container    = self.find_layer(all_layers,'Container')
    source_layer = self.find_layer(all_layers, 'Template')

    self.duplicate_layer(
      container, source_layer,
      'Zeta Mataharani', 'Zeta Mataharani, MD')

if __name__ == '__main__':
  DuplicateLayer().run()
{{< / highlight >}}

Looks not pretty useful?
Wogh.. you should read the next automation part.
This short code above really save my working hours.

-- -- --

### What is Next ?

Consider continue reading [ [Inkscape Automation - Part One][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}design/2021/11/07/inkscape-automation-01/

[src-inx-duplicate]:    {{< anycode >}}/inkscape/extensions/duplicate_layer.inx
[src-py--duplicate]:    {{< anycode >}}/inkscape/extensions/duplicate_layer.py

[layer-template-xml]:   {{< baseurl >}}assets/posts/design/2021/11/21-xml-template-alt.png
[cert-layers]:          {{< baseurl >}}assets/posts/design/2021/11/12-layers.png
[cert-testbed-03-zm]:   {{< baseurl >}}assets/posts/design/2021/11/17-cert-testbed-03-zm.png
[cert-testbed-04-zm]:   {{< baseurl >}}assets/posts/design/2021/11/17-cert-testbed-04-zm.png
[cert-layer-zm]:        {{< baseurl >}}assets/posts/design/2021/11/17-layer-name.png
[cert-layer-name]:      {{< baseurl >}}assets/posts/design/2021/11/17-layer-name-name.png
[cert-layer-template]:  {{< baseurl >}}assets/posts/design/2021/11/17-layer-name-template.png
