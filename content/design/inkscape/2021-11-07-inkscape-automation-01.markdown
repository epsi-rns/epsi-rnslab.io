---
type   : post
title  : "Inkscape Automation - Part One"
date   : 2021-11-07T09:17:35+07:00
slug   : inkscape-automation-01
categories: [design]
tags      : [inkscape, python]
keywords  : [extension]
author : epsi
opengraph:
  image: assets/posts/design/2021/11/17-cert-testbed-04-zm.png

toc    : "toc-2021-inkscape-auto"

excerpt:
  Prepare an inkscape automation for mass product certificate.

---

### Preface

> Prepare an inkscape automation for mass product certificate.

We are going to make a mass production of certificate,
using python in inkscape.
This require a mailmerge like feature,
which has not exist yet in inkscape.
Actually mailmerge will be exist soon, but we are not there yet.

#### Step by step?

However, we can use alternate approach with these step below.

1. Prepare the design using inkscape.

2. Prepare prepopulated name data in python.

3. **Using inkscape extension**: duplicate layer.

4. **Using inkscape CLI**: export each layer above.

#### The Needs

But why do I even bother with automation,
while I can do it manually?

The reason is _human tend to err when in comes to tedious task_.

Well.. Not all people. But at least I know that,
I make so many mistake when I'm tired.
Sometimes I break, when I haven't got enough sleep.
Fatique is not a myth.

The second reason is _I love coding better than tedious task_.
It is not boring to discover something new.

-- -- --

### Prepare the Design

#### The SVG File

Consider our previous design.
It is still a simple design, with a little visual enhancement.

[cert-testbed-04.svg][cert-testbed-04-svg]

![Certificate Test Bed][cert-testbed-04]

We need to separate the duplicated layer from other.
So we need a root layer. Consider name the the layer `Container`.

![All Layers][cert-layer-container]

Put the replacable text in a special layer.
Consider name the layer `Template`.
And then hide the layer visibility using the eye toggle.

![Template Layer][cert-layer-hidden]

The looks of the certificate should looks as below:

![Certificate Template Layer Hidden][cert-testbed-04-hid]

Keep the `Template` layer hidden, and save.

### XML Properties

Consider have a look at the XML properties:

![Layer Template in XML Editor][layer-template-xml]

Notice that there is a child the `text`, and a grandchild the `tspan`.
And also there are three ids: layer, text, and tspan.
We are going to name the layer id and tspan id, using inkscape extension.

Now we are ready to go to the coding part using python.

-- -- --

### Prepopulated Data.

#### What Data?

We only need three data.

1. ID, maybe number or anything.
   For use in XML as explained above..

2. Name, such as full name.
   For layer name, and also for file naming later.

3. Name with Title.
   As replacable text, for use in certificate.

4. You can also add additional data.
   But for this example case, let's keep this case simple.

#### Trainees   

Certificate will be give for trainees.
So we have to setup a file.

Consider name the python file as `testbed.py`.

* [gitlab.com/.../trainees/testbed.py][src-py-testbed]

{{< highlight python >}}
#!/usr/bin/env python

people = {
  1: ["Other Data","Brian May","Brian May, PhD"],
  2: ["Other Data","Jim Morrison","Jim Morrison, BSc"],
  3: ["Other Data","Lisa Loeb","Lisa Loeb, BA"],
  4: ["Other Data","Tom Morello","Tom Morello, BA"],
  5: ["Other Data","Greg Graffin","Greg Graffin, PhD"]
}
{{< / highlight >}}

#### Testing in CLI

Before we get into a complex extension coding,
it is better to start with CLI.

Consider name the python file as `show-people.py`.

* [gitlab.com/.../trainees/show-people.py][src-py-show-people]

{{< highlight python >}}
#!/usr/bin/env python

from testbed import people

for key, person in people.items():
  number     = str(key).zfill(2)
  name_only  = person[1]
  name_title = person[2]

  # layer related
  layer_id   = 'person-id-' + number
  layer_name = number + ' - ' + name_only
  print(layer_id)
  print(layer_name)
  
  # display name
  text_id   = 'display-id-' + number
  print(text_id)
  print(name_title)

  # just another newline
  print()
{{< / highlight >}}

#### Result

With the result simply as below:

{{< highlight bash >}}
person-id-01
01 - Brian May
display-id-01
Brian May, PhD

person-id-02
02 - Jim Morrison
display-id-02
Jim Morrison, BSc

person-id-03
03 - Lisa Loeb
display-id-03
Lisa Loeb, BA

person-id-04
04 - Tom Morello
display-id-04
Tom Morello, BA

person-id-05
05 - Greg Graffin
display-id-05
Greg Graffin, PhD
{{< / highlight >}}

![ViM: show-people.py][vim-show-people]

### XML Properties

If you wonder, what I'm going to achieve.
It is shown here in the XML properties:

![Layer Template ID in XML Editor][layer-template-xml-id]

You can compare with the result of python code above as below:

{{< highlight bash >}}
person-id-01
01 - Brian May
display-id-01
Brian May, PhD
{{< / highlight >}}

-- -- --

### What is Next ?

Consider continue reading [ [Inkscape Automation - Part Two][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}design/2021/11/09/inkscape-automation-02/

[src-py-testbed]:       {{< anycode >}}/inkscape/extensions/trainees/testbed.py
[src-py-show-people]:   {{< anycode >}}/inkscape/extensions/trainees/show-people.py

[cert-testbed-03]:      {{< baseurl >}}assets/posts/design/2021/11/11-cert-testbed-03.png
[cert-testbed-03-hid]:  {{< baseurl >}}assets/posts/design/2021/11/11-cert-testbed-03-hidden.png
[cert-testbed-03-svg]:  {{< baseurl >}}assets/posts/design/2021/11/cert-testbed-03.svg

[cert-testbed-04]:      {{< baseurl >}}assets/posts/design/2021/11/11-cert-testbed-04.png
[cert-testbed-04-hid]:  {{< baseurl >}}assets/posts/design/2021/11/11-cert-testbed-04-hidden.png
[cert-testbed-04-svg]:  {{< baseurl >}}assets/posts/design/2021/11/cert-testbed-04.svg

[cert-layers]:          {{< baseurl >}}assets/posts/design/2021/11/12-layers.png
[cert-layer-hidden]:    {{< baseurl >}}assets/posts/design/2021/11/17-layer-hidden.png
[layer-template-xml]:   {{< baseurl >}}assets/posts/design/2021/11/21-xml-template.png
[layer-template-xml-id]:{{< baseurl >}}assets/posts/design/2021/11/17-layer-xml-id.png

[cert-layer-container]: {{< baseurl >}}assets/posts/design/2021/11/17-layer-name-container.png
[cert-layer-template]:  {{< baseurl >}}assets/posts/design/2021/11/17-layer-name-template.png

[vim-show-people]:      {{< baseurl >}}assets/posts/design/2021/11/17-py-show-people.png
