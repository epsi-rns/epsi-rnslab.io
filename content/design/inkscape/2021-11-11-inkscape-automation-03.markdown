---
type   : post
title  : "Inkscape Automation - Part Three"
date   : 2021-11-11T09:17:35+07:00
slug   : inkscape-automation-03
categories: [design]
tags      : [inkscape, python]
keywords  : [extension]
author : epsi
opengraph:
  image: assets/posts/design/2021/11/11-cert-testbed-03.png

toc    : "toc-2021-inkscape-auto"

excerpt:
  Export layers using etree, then inkscape CLI.

---

### Preface

> Export layers using etree, then inkscape CLI.

From extension we are going to use CLI.
Because I have no idea how to do it with extension.

The main idea is just,
turn on visibility of one layer at a time.
So we have different text display,
as each layer has different text.

-- -- --

### Export Layer Scripts

#### Objective

This script export each layer to

1. SVG
2. PDF
3. PNG

#### Artefacts

We need two files.

* `trainees_testbed.py`
* `export_layers.py`

#### Trainees Testbed

This should be exactly as previous prepopulated data.

* [gitlab.com/.../inkscape/trainees_testbed.py][src-py-testbed]

{{< highlight python >}}
#!/usr/bin/env python

people = {
  1: ["Other Data","Brian May","Brian May, PhD"],
  2: ["Other Data","Jim Morrison","Jim Morrison, BSc"],
  3: ["Other Data","Lisa Loeb","Lisa Loeb, BA"],
  4: ["Other Data","Tom Morello","Tom Morello, BA"],
  5: ["Other Data","Greg Graffin","Greg Graffin, PhD"]
}
{{< / highlight >}}

### Result

There is not much to shown, because most stuff done in backend.
Well, actually the result comes in files in a folder.

![CLI Result Folder][cli-result]

-- -- --

### ElementTree

The simpler part of this script is just print a message.

* [gitlab.com/.../inkscape/export_layers.py][src-py-export-layers]

{{< highlight python >}}
#! /usr/bin/python3
import xml.etree.ElementTree as ET
import os
from trainees_testbed import people

tree = ET.parse('cert-testbed.svg')
root = tree.getroot()

for key, person in people.items():
  number     = str(key).zfill(2)
  name_only  = person[1]
  name_title = person[2]

  element = root.find(".//*[@id='person-id-" + number + "']")
  name = element.find(".//*[@id='display-id-" + number + "']")
  print(element.get('id') + ' : ' + name.text)
{{< / highlight >}}

The result of the script is, simply as below,

{{< highlight bash >}}
person-id-01 : Brian May, PhD
person-id-02 : Jim Morrison, BSc
person-id-03 : Lisa Loeb, BA
person-id-04 : Tom Morello, BA
person-id-05 : Greg Graffin, PhD
{{< / highlight >}}

-- -- --

### Export Files

#### SVG Part

Then we can export each layer to SVG.

{{< highlight python >}}
  svg_filename = 'Certificate - ' + name_only + '.svg'
  element.set('style', 'display:inline')
  tree.write (svg_filename)
  element.set('style', 'display:none')
{{< / highlight >}}

This is the main idea,
to turn on visibility of one layer at a time for each loop item.
So we have different SVG for different layer visibility.

#### PDF Part

Really, this is just additional part.

{{< highlight python >}}
  pdf_filename = '"./pdf/Certificate - ' + name_only + '.pdf"'

  pdf_cmd = 'inkscape "' + svg_filename + '" ' +\
            "--export-area-page --batch-process " +\
            "--export-type=pdf --export-filename=" + pdf_filename
{{< / highlight >}}

### PNG Part

And so this below.
this is just also additional part.

{{< highlight python >}}
  png_filename = '"./Certificate - ' + name_only + '.png"'

  pdf_cmd = 'inkscape "' + svg_filename + '" ' +\
            "--export-area-page --batch-process " +\
            "--export-type=pdf --export-filename=" + pdf_filename

  png_cmd = 'inkscape "' + svg_filename + '" ' +\
            "--export-area-page --batch-process --export-dpi=96 " +\
            "--export-type=png --export-filename=" + png_filename
{{< / highlight >}}

#### Surpress PNG Warning

Since it is just a CLI, we can send to `/dev/null`

{{< highlight python >}}
  png_cmd = 'inkscape "' + svg_filename + '" ' +\
            "--export-area-page --batch-process --export-dpi=96 " +\
            "--export-type=png --export-filename=" + png_filename +\
            " 2> /dev/null"
{{< / highlight >}}

Or in windows

{{< highlight python >}}
  png_cmd = 'inkscape "' + svg_filename + '" ' +\
            "--export-area-page --batch-process --export-dpi=96 " +\
            "--export-type=png --export-filename=" + png_filename +\
            " 2> nul"
{{< / highlight >}}

-- -- --

### Real Life Certificate

I use this script to make mass production of hundreds certificates.

![Real Life Mass Production Result][cli-all-result]

This save working hours. And also fun to work with.

-- -- --

### What is Next ?

Done. Finished.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[src-py-export-layers]: {{< anycode >}}/inkscape/export_layers.py
[src-py-testbed]:       {{< anycode >}}/inkscape/trainees_testbed.py

[extension-fill-menu]:  {{< baseurl >}}assets/posts/design/2021/11/22-fill-menu.png
[layers-duplicated]:    {{< baseurl >}}assets/posts/design/2021/11/22-layers-duplicated.png
[extension-file]:       {{< baseurl >}}assets/posts/design/2021/11/22-extension-file.png
[layer-template-xml-id]:{{< baseurl >}}assets/posts/design/2021/11/21-xml-template-id.png
[cli-result]:           {{< baseurl >}}assets/posts/design/2021/11/25-result-alt.png
[cli-all-result]:       {{< baseurl >}}assets/posts/design/2021/11/25-all-result.png
