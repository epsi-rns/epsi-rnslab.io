---
type   : post
title  : "Inkscape: Impress Slides - Part One"
date   : 2020-09-21T04:29:15+07:00
slug   : inkscape-impress-slides-01
categories: [design]
tags      : [inkscape, LibreOffice]
keywords  : [impress, master slides, template, Rania Amina, simple]
author : epsi
opengraph:
  image: assets-design/2020/09-impress/isometric-preview.png

toc    : "toc-2020-inkscape-template"

excerpt:
  Making simple LibreOffice impress master slide using inkscape.

---

### Preface

> Making simple LibreOffice impress master slide using inkscape.

I name this template `auto`.
Because this template follow the design and colors of,
my automotive workshop.

#### Article Series

This is a six parts article series.

* Part One:   Auto, the simple template

* Part Two:   Candyclone, the modern classic template

* Part Three: Adding custom bullets decoration

* Part Four:  Adding content illustration

* Part Five:  Creating illustration diagram

* Part Six:   Adding genuine artwork

![Isometric Preview][isometric-preview]

-- -- --

### DIY: Do it Yourself

After suffering a hard time, and a hard lesson,
I have been doing all deskjob for my little company and workshop,
from accounting to design by myself.
In design area, I have design logos, business card, banner, stamp,
standar A4 paper with cover, standard invoice design, hardcopy receipt,
website, and all of them I did it with fun.
On thing that left is making a presentation template, and company profile.

#### A Dream Come True

A weeks ago, my friend [Rania Amina][raniaamina]
make an `Impress` template contest,
for the 10th anniversary of LibreOffice.
I don't have any interest to win any contest,
since I'm not really a designer.

My plan was just making own template for my company.
But since my friend ask people to contribute,
I planned to submit the template anyway.

#### Official Template

My first attempt is, of course research.

* <https://dohliam.github.io/LibreOffice-impress-templates/>

I have download all the templates in that repository,
and suprisingly most of the templates is just changing background.
Some of those wallpaper are old, and maybe the templates are old too.
Whoaa... Impress is a great software, and Impress deserve more than these.

#### Example Material

I put all example in this article in this repository

* [github.com/epsi-rns/berkas2][repository]

#### Example Real Life Presentations

> I can't wait to apply my own template to my own presentation.

For the impatient reader,
you can have a look at these four,
real life presentations using this template.

* [Linux Diversity][linux-diversity]

* [Desktop Customization][customization]

* [Concept CSS][concept-css]

* [Concept SSG][concept-ssg]

#### Widening Ecosystem

LibreOffice has its own Drawing software called LibreOffice Draw.
Here I represent different approach,
using Inkscape instead of LibreOffice Draw.

-- -- --

### The Master Slide View

If you are new to LibreOffice User Interface,
you need to know how to switch from normal slide to master slide.

### Using Standard Toolbar

With menu user interface it is very straightforward.

![Impress Menu UI: Switch to Master Slide][slide-menu-select]

You can also click the icon.

### Using Tabbed UI

With tabbed UI, is easier.
Here is the normal view.

![Impress Tabbed UI: Normal Slide View][slide-tabbed-normal]

And here below is the master slide view.

![Impress Tabbed UI: Master Slide View][slide-tabbed-master]

Note that I'm using my own template, that's why we have above view.
For empty template, you should face an empty blank master slide.

-- -- --

### Impress

#### Preview

I have made a few of original wallpaper using inkscape.
Since in Impress, it is just changing background.
I thought that I can make background by myself using inkscape.
Either using my material wallpaper that I have made,
or create from scratch.

My first template is just mimic the business card design,
as a master slide. And the result is a simple looks,
not so beautiful, but not terribly ugly either.

![Auto Template: Preview][auto-cover]

#### Source

I put the source in my repository:

* [github.com/.../impress-template-auto][repo-auto]

#### Master Slides

This template consist of three master slides,
that you can see in them all Impress.

![Auto Template: Impress: Master Slide][auto-master]

From top to bottom,
I name each page as:

* Default

* Single: for chapter title or break between chapter

* Image: Master Slide, made to show single image.

-- -- --

### Inkscape

The background in above slide was made using inkscape.
Each master slide, made in its own layers in inkscape.

![Auto Template: Inkscape: Layers][auto-layers]

The inkscape preview is here:

![Auto Template: Inkscape: Single Slide: Cut][auto-cut]

Which is the source of the image before `cut` process is here.

![Auto Template: Inkscape: Single Slide: Source][auto-source]

I always keep the source in separated layer,
so that I can alter the final image easily.
I keep this layer hidden, and show only the final cut.

#### Size

The Impress slide has ratio instead of fix size of pages, such as `16:9`.
The inkscape that I use should reflect this ratio too.
For example the inkscape document use the size of `1600px * 900px`.

#### Source

I also put the SVG source in my repository:

* [github.com/.../source-auto.svg][source-inkscape]

-- -- --

### Making Master Slide

You can start from empty master slides.

![Auto Template: Impress: Empty Master Slide][auto-impress-empty]

You can directly copy paste, from inkscape to impress.
Just be aware of the position.

* Such as the `X` position for full horizontal image,
  should be start from zero.

* And so is the `Y` position for full vertical image,
  should be start from zero.

![Auto Template: Impress: Copy Paste][auto-impress-paste]

#### Text Adjustment

Now make adjustment,
such as moving the image to the backside of the text.
Move the size and position of the text.
And also alter the color of the text to suit the background color.

![Auto Template: Impress: Adjustment][auto-impress-adjust]

#### Font

I like to use these two free google fonts:

* Source Sans Pro

* Source Sans Pro Semibold

Adjust the title as you need.

#### Slide

Consider your slide, in normal view,
to check whether your slide is ready.

![Auto Template: Impress: Normal Slide][auto-impress-normal]

-- -- --

### Vector Template

Since I use SVG from Inkscape,
I begin to question myself whether my method of copying is right.
Then I realize that there is two differents method.

#### Copy Raster

When I copy from Inkscape and paste to Impress,
the object will be saved as bitmap,
as shown in extracted source below:

![Auto Template: Extract: Copy Raster][auto-extract-raster]

#### Insert Vector

When I import Inkscape file to Impress using `insert - image` menu,
the object will be saved as both SVG and PNG,
as shown in extracted source below:

![Auto Template: Extract: Import Vector][auto-extract-vector]

That is the different.

-- -- --

### What is Next ?

Consider continue reading [ [Inkscape: Impress Slides - Part Two][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}design/2020/09/22/inkscape-impress-slides-02/

[repository]:   https://github.com/epsi-rns/berkas2/
[raniaamina]:   https://raniaamina.id/
[repo-auto]:    https://github.com/epsi-rns/berkas2/tree/master/impress-template-auto
[source-inkscape]:      {{< berkas2-blob >}}//impress-template-auto/source-auto.svg

[linux-diversity]:      https://epsi-rns.github.io/system/2020/10/11/slides-linux-diversity.html
[customization]:        https://epsi-rns.github.io/desktop/2020/10/11/slides-desktop-customization.html
[concept-css]:          https://epsi-rns.gitlab.io/frontend/2020/10/11/slides-concept-css/
[concept-ssg]:          https://epsi-rns.gitlab.io/ssg/2020/10/11/slides-concept-ssg/

[isometric-preview]:{{< assets-design >}}/2020/09-impress/isometric-preview.png

[auto-cover]:   {{< assets-design >}}/2020/09-impress/01-auto-linux-diversity-cover.png
[auto-master]:  {{< assets-design >}}/2020/09-impress/01-auto-impress-master-slides.png
[auto-layers]:  {{< assets-design >}}/2020/09-impress/01-auto-inkscape-layers.png
[auto-cut]:     {{< assets-design >}}/2020/09-impress/01-auto-inkscape-cut.png
[auto-source]:  {{< assets-design >}}/2020/09-impress/01-auto-inkscape-source.png

[auto-impress-empty]:   {{< assets-design >}}/2020/09-impress/01-auto-impress-empty.png
[auto-impress-paste]:   {{< assets-design >}}/2020/09-impress/01-auto-impress-paste.png
[auto-impress-adjust]:  {{< assets-design >}}/2020/09-impress/01-auto-impress-adjustment.png
[auto-impress-normal]:  {{< assets-design >}}/2020/09-impress/01-auto-impress-normal.png

[auto-extract-raster]:  {{< assets-design >}}/2020/09-impress/01-auto-extract-raster-png.png
[auto-extract-vector]:  {{< assets-design >}}/2020/09-impress/01-auto-extract-vector-svg.png

[slide-menu-select]:    {{< assets-design >}}/2020/09-impress/00-masterslide-menu-select.png
[slide-tabbed-normal]:  {{< assets-design >}}/2020/09-impress/00-masterslide-tabbed-normal.png
[slide-tabbed-master]:  {{< assets-design >}}/2020/09-impress/00-masterslide-tabbed-master.png

