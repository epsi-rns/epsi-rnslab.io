---
type   : post
title  : "Inkscape: Impress Slides - Part Two"
date   : 2020-09-22T04:29:15+07:00
slug   : inkscape-impress-slides-02
categories: [design]
tags      : [inkscape, LibreOffice]
keywords  : [impress, master slides, template, Hervy QA, classic modern]
author : epsi
opengraph:
  image: assets-design/2020/09-impress/02-candyclone-cover.png

toc    : "toc-2020-inkscape-template"

excerpt:
  Making real life LibreOffice impress master slide using inkscape.

---

### Preface

> Making real life LibreOffice impress master slide using inkscape.

I name this template `candyclone`.

#### Article Series

This is a six parts article series.

* Part One:   Auto, the simple template

* Part Two:   Candyclone, the modern classic template

* Part Three: Adding custom bullets decoration

* Part Four:  Adding content illustration

* Part Five:  Creating illustration diagram

* Part Six:   Adding genuine artwork

![Thumbs 4 Preview][thumbs-4-preview]

-- -- --

### Original Template

> Luckily I met this person.

You know bad habit of coders is the tendency to show off.
I like to post my recent works right away to forum,
and one person who replied is a great designer named [Hervy QA][hervy-qa].
He said my presentation is good,
and give a link to his last year works.

* <https://hervyqa.com/gnome-bluez-LibreOffice-impress-free-template/>

And wow, his works is amazing. You can have a look at this video:

* <http://www.youtube.com/watch?v=O3urHT5AHG8#action=share>

#### Border Line

I saw the borderline (left and right) of his master slide,
and realize that it is very similar with my blog design,
the difference is I use many colors from google material pallete.
I intent to clone the looks of his design, but using google colors.

I find out that the border line he use for master slide is,
native from LibreOffice, it is just boxes with thin width,
to make it looks like a line.
I decide to use inkscape to clone the looks,
because it is easier to create the shape that I want using inkscape.

#### The Clone Template

Since my clone template contain eye candy color from google pallete.
I name this template `candyclone`.

![Candyclone Template: Preview][clone-cover]

I tried to leverage myself from making,
low quality third grade presentation,
to be a second grade one.
I know I do not have premium quality yet.
But at least I share, just in case somebody need this template.

This is a classic modern template in a sense that,
the looks (and shape) is actually just simple classic looks.
But the shape modernized by using the looks of google material color.

#### 76 Master Slides

With this template,
you can switch to master slides with four background kind,
with each kind have 19 choices of colors

![Candyclone Template: 76 Master Slides][switch-master]

There are three ways to switch your master slide:

1. Using `Menu - Slide - Change Slide Master`

2. Using `Sidebar - Properties - Slide`

3. Using `Tabbed UI - Layout - Change Slide Master`

-- -- --

### Inkscape: Default Layers

#### Document Properties

I use the size of `1600px * 900px`.
And checkerboard background to differ from the white background.

#### Source Layer

I made the source layer first.
As usual, I start from my favorite material color, the `blue500`.

![Candyclone Template: Inkscape: Default Slide: Source][clone-source]

You can have you own width arrangement for border line such as

* Left Border: `40px`,

* Right Border: `30px`.

#### Cut Layer

Then I cut the border line.

![Candyclone Template: Inkscape: Default Slide: Cut][clone-cut]

I use google material color, consecutively

* Left Bottom: `blue900`,

* Left Middle: `blue700`,

* Left Top: `blue500`,

* Right Bottom: `blue500`,

* Right Middle: `blue300`,

* Right Top: `blue100`.

![Candyclone Template: Inkscape: Material Design Pallete][coloring-material]

#### Group

And group all the shape together.
Now you should have the grouped shape with the size of `1600px * 900px`.

#### Layers

Copy the `group object` above,  into its own layer,
for each 19 colors of google material pallete.

![Candyclone Template: Inkscape: Default Slide: Layers][clone-layers]

-- -- --

### Impress: Default Master Slides

> Use insert-image instead of copy-paste.

Just like previous article, repeat the steps, for each 19 colors.
Import Inkscape file to Impress using `insert - image` menu,
It means you should have 19 master slides.

![Candyclone Template: Impress: Default Slide: Master][clone-master-default]

Do not forget to rename each master slide to reflect the color.

-- -- --

### Inkscape: Other Layers

Repeat the steps for the rest of the three slides.

#### Grouping Layers

Group layer for the same slide kind such as

* Default: Has 19 layers, plus cut and source.

* Alternate: Has 19 layers.

* Single: Has 19 layers, plus cut and source.

* Image: Has 19 layers, plus cut and source. And maybe alternate source.

#### Alternate Slide/Layer

The same with default slide, but with background color.

![Candyclone Template: Inkscape: Alternate Slide: Cut][clone-alternate]

With a slight different material color:

* Left Bottom: `blue900`,

* Left Middle: `blue700`,

* Left Top: `blue400`,

* Right Bottom: `blue600`,

* Right Middle: `blue300`,

* Right Top: `blue100`.

#### Single Slide/Layer

The same step for slide for chapter or breaking between chapter.

![Candyclone Template: Inkscape: Single Slide: Source][clone-single-src]

The final cut for `single-blue`.

![Candyclone Template: Inkscape: Single Slide: Cut][clone-single-cut]

You can have any color that you want.
I'm using `blue900` for middle center shape.

#### Image Slide/Layer

The same step for slide for showing image.

![Candyclone Template: Inkscape: Image Slide: Source][clone-image-src]

The final cut for `image-blue`.

![Candyclone Template: Inkscape: Image Slide: Cut][clone-image-cut]

I'm using the same color arrangement as default slide/layer.

-- -- --

### Impress: Other Three Master Slides

For other three slides, copy from Inkscape, and paste to Impress.
Do them all for each 19 colors.
The final result should be 4x19 slide, it is 76 slides.

![Candyclone Template: Impress: Image Slide: Master][clone-master-image]

Again, do not forget to rename each master slide to reflect the color.

#### Adjustments

> For each 76 slides!

Again, adjustment for the text position, size, color and font.
I know it is tedious tasks.
Just do it, and it will be finished before you get so tired.

-- -- --

### Normal View

Switch to normal slide to test each master slide.

#### Default Slide

![Candyclone Template: Impress: Default Slide: Normal][normal-default]

#### Alternate Slide with Background

![Candyclone Template: Impress: Alternate Slide: Normal][normal-alternate]

#### Single Slide for Chapter Title

![Candyclone Template: Impress: Single Slide: Normal][normal-single]

#### Image Slide

![Candyclone Template: Impress: Image Slide: Normal][normal-image]

> It is easy right?

-- -- --

### Arranging Chapter

Using multiple color choices,
I can separate each chapter in my presentation.
The same chapter, with the same colors.
And different chapter with different color.

![Candyclone Template: Impress: Slide Sorter][chapter-color]

-- -- --

### What is Next ?

Consider continue reading [ [Inkscape: Impress Slides - Part Three][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}design/2020/09/23/inkscape-impress-slides-03/

[hervy-qa]:     https://hervyqa.com/

[thumbs-4-preview]: {{< assets-design >}}/2020/09-impress/thumbs-4.png
[coloring-material]:{{< assets-design >}}/2020/09-impress/coloring-candyclone.png

[switch-master]:{{< assets-design >}}/2020/09-impress/02-switch-master-slide.png

[clone-cover]:  {{< assets-design >}}/2020/09-impress/02-candyclone-cover.png
[clone-cut]:    {{< assets-design >}}/2020/09-impress/02-clone-inkscape-cut.png
[clone-source]: {{< assets-design >}}/2020/09-impress/02-clone-inkscape-source.png
[clone-layers]: {{< assets-design >}}/2020/09-impress/02-clone-inkscape-layers.png

[clone-master-default]: {{< assets-design >}}/2020/09-impress/02-clone-impress-master-default.png
[clone-master-image]:   {{< assets-design >}}/2020/09-impress/02-clone-impress-master-image.png

[clone-alternate]:  {{< assets-design >}}/2020/09-impress/02-clone-inkscape-alternate-cut.png
[clone-image-cut]:  {{< assets-design >}}/2020/09-impress/02-clone-inkscape-image-cut.png
[clone-image-src]:  {{< assets-design >}}/2020/09-impress/02-clone-inkscape-image-source.png
[clone-single-cut]: {{< assets-design >}}/2020/09-impress/02-clone-inkscape-single-cut.png
[clone-single-src]: {{< assets-design >}}/2020/09-impress/02-clone-inkscape-single-source.png

[normal-default]:   {{< assets-design >}}/2020/09-impress/02-clone-impress-normal-default.png
[normal-alternate]: {{< assets-design >}}/2020/09-impress/02-clone-impress-normal-alternate.png
[normal-single]:    {{< assets-design >}}/2020/09-impress/02-clone-impress-normal-single.png
[normal-image]:     {{< assets-design >}}/2020/09-impress/02-clone-impress-normal-image.png

[chapter-color]:    {{< assets-design >}}/2020/09-impress/02-clone-impress-slide-sorter.png
