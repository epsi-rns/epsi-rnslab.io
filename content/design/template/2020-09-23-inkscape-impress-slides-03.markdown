---
type   : post
title  : "Inkscape: Impress Slides - Part Three"
date   : 2020-09-23T04:29:15+07:00
slug   : inkscape-impress-slides-03
categories: [design]
tags      : [inkscape, LibreOffice]
keywords  : [impress, master slides, template, bullets]
author : epsi
opengraph:
  image: assets-design/2020/09-impress/03-points-four-items-big.png

toc    : "toc-2020-inkscape-template"

excerpt:
  Adding custom bullets decoration to impress slide using inkscape.

---

### Preface

> Adding custom bullets decoration to impress slide using inkscape.

I have made a some presentation in the past.
And all the content arranged by points of thought.

#### Article Series

This is a six parts article series.

* Part One:   Auto, the simple template

* Part Two:   Candyclone, the modern classic template

* Part Three: Adding custom bullets decoration

* Part Four:  Adding content illustration

* Part Five:  Creating illustration diagram

* Part Six:   Adding genuine artwork

-- -- --

### Default View

This one is an example of default view, in a simple fashioned view.

![Points: Simple View][points-simple]

I think, there must be a way that this looks can be improved.

### Progress Ring

Then I remember that I have made this article.

* [Inkscape: Progress Ring Animation][local-progress-ring]

With the preview image as below:

![Progress Ring: Unified Graphic Material][progress-bright]

#### Source

Source image is provided here below:

* [github.com/.../progress-ring-bright-number.svg][source-inkscape]

You can adjust the shadow yourself in inkscape, using filter editor.

### Two Items

With a little changes I can produce this easily.

![Points: Two Items][points-2-items]

Just beware of the size,
while copying directly from `Inkscape` to `Impress`.

### Three Items

Now I can make it generic and put it into template.

![Points: Three Items][points-3-items]

### Four Items: Big

Almost the same

![Points: Four Items: Big][points-4b-items]

### Four Items: Small

For four points of thought,
I could also use smaller bullet.

![Points: Four Items: Small][points-4s-items]

### Five Items

Five bullets is just six bullets minus one.
Just remove one bullet from below example.

![Points: Five Items][points-5-items]

### Six Items

No need to explain.

![Points: Six Items][points-6-items]

### Real Life Presentation

Here I present a slide sorter view of my SSG presentation.
The original presentation consist of 48 pages.
I remove almost all no bullets pages,
and leave the slide sorter with,
a bunch of pages with bullet of thoughts.

![SSG Presentation: Slide Sorter][slide-sorter-bullets]

### Custom Bullets

Of course you can make your own bullet to suits your own style.
This is just an example, how you can improve your presentation.

> After all it is all about creativity

-- -- --

### What is Next ?

Consider continue reading [ [Inkscape: Impress Slides - Part Four][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}design/2020/09/24/inkscape-impress-slides-04/

[local-progress-ring]:  {{< baseurl >}}design/2017/11/15/inkscape-progress-ring/

[source-inkscape]:      {{< berkas2-blob >}}//impress-template-candyclone/bullets/progress-ring-bright-number.svg

[progress-bright]:  {{< assets-design >}}/2020/09-impress/03-progress-ring-bright-number.png
[points-simple]:    {{< assets-design >}}/2020/09-impress/03-points-simple-view.png
[points-2-items]:   {{< assets-design >}}/2020/09-impress/03-points-two-items.png
[points-3-items]:   {{< assets-design >}}/2020/09-impress/03-points-three-items.png
[points-4s-items]:  {{< assets-design >}}/2020/09-impress/03-points-four-items.png
[points-4b-items]:  {{< assets-design >}}/2020/09-impress/03-points-four-items-big.png
[points-5-items]:   {{< assets-design >}}/2020/09-impress/03-points-five-items.png
[points-6-items]:   {{< assets-design >}}/2020/09-impress/03-points-six-items.png
[slide-sorter-bullets]: {{< assets-design >}}/2020/09-impress/03-clone-bullets-slide-sorter.png
