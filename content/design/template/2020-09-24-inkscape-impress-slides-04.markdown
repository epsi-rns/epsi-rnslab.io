---
type   : post
title  : "Inkscape: Impress Slides - Part Four"
date   : 2020-09-24T04:29:15+07:00
slug   : inkscape-impress-slides-04
categories: [design]
tags      : [inkscape, LibreOffice]
keywords  : [impress, master slides, template, original artwork]
author : epsi
opengraph:
  image: assets-design/2020/09-impress/04-12-candyclone-content-brake-pads.png

toc    : "toc-2020-inkscape-template"

excerpt:
  Adding content illustration to impress slide using inkscape.

---

### Preface

> Adding content illustration to impress slide using inkscape.

The hardest part of making presentation after master slides is the content illustration.
Here is my content illustration attempt that you can use for your daily presentation.
You can also share your content illustration to be used by other people.

My mission is clear.
This template should be usable enough to be used by Impress user.
But unfortunately to modify this, require Inkscape knowledge.
This is why we need pretty diagram, with infographics style.

All will be no good if you do not have good material to be presented.

#### Article Series

This is a six parts article series.

* Part One:   Auto, the simple template

* Part Two:   Candyclone, the modern classic template

* Part Three: Adding custom bullets decoration

* Part Four:  Adding content illustration

* Part Five:  Creating illustration diagram

* Part Six:   Adding genuine artwork

![Thumbs 3 Preview][thumbs-3-preview]

-- -- --

### Preparation

#### The Requirement

I had been in a contest of Impress template in Indonesia for a while,
and get the fifth nominee of the best six template.
One of the requirement is the content.
So after master slides and bullets, content is the things to do.

The contest itself is a cooperation,
between LibreOffice Indonesia, and Gimpscape.
Gimpscape is a local community in Indonesia,
mostly discussing design using libre graphics,
not limited by `Inkscape` and `The Gimp`.
This is my chance to use Inkscape,
and I choose to make diagram like shape,
as an illustration for my Impress content.

#### Source

Source image is provided here below:

* [github.com/.../template-candyclone-content.svg.zip][source-inkscape]

#### Layers

After a few moment, I found it easy to do it with inkscape.
In fact I cannot stop making illustration.
Finally I make fifteen illustration,
with two additional rejected illustration that,
I even refuse to use.

You can see all layers here:

![Content Illustration: Inkscape: All Layers][content-layers]

#### Base Layer

To simulate master slide, we need to prepare background layer.
I use three colors: red, green, and blue.
And one fake title is also required,
for positioning reason.

![Content Illustration: Inkscape: Base Layer][content-layer-base]

With source image preview as below:

![Content Illustration: Inkscape: Master Slide][inkscape-master-slide]

-- -- --

### The Process

I am going to explain step by step using one case.
The same process applied to all other case as well.

#### Inkscape: Main Artefacts

Generally I use source layer, before cut them into useful shape.
I use whatever color, and set the aesthetic part later.

![Content Illustration: Inkscape: Source Layer][inkscape-03-source]

Then cut it in separate layer.
And also set the material color.
Notice that I use more icon positionings than required.

![Content Illustration: Inkscape: Cut Layer][inkscape-03-cut]

It takes some iteration to find the right color.
Especially the shadow below,
until I found that I only need to darkened them.
For example, if I use `blue300`,
then I could use the shadow color with `blue400` or `blue500`.

#### Inkscape: Prepare Import File

To be able to insert from Inkscape with Impress menu,
we need to delete all other unused ayers.
So save the inkscape file as different SVG file artefact.

![Content Illustration: Inkscape: Import Source][inkscape-03-import]

Notice that there is only one layer, although multi layer is fine.
My intention to use only one layer is simplicity.

#### Impress: Insert Image

After preparing my `candy clone` template with all master slides,
I can make a new slide, and insert SVG image.

![Content Illustration: Impress: Insert Image][impress-insert-image]

Make sure that the position start zero point.

![Content Illustration: Impress: Position and Size][impress-position-size]

#### Impress: Break and Adjust

Break the Image, and remove unecessary icons.
Now you can have the slide preview as below:

![Content Illustration: Impress: Fresh Image][impress-03-fresh]

You do not need to break the image in Impress.
You can prepare all in Inkscape Instead.

Without breaking, you cannot adjust position of object in Impress.
But who wants to adjust position in Impress anyway,
while we can do it all in Inkscape.

#### Impress: Adding Text

With method above,
all we need to do in Impress is only adding text.

![Content Illustration: Impress: Final Image][impress-03-final]

#### Repeat The Process

Now you can do the same process,
for all other diagram illustration.
Just remember that the creative process in Inkscape part,
might be different for each diagram.
One maybe simpler, and the other one might be complex,
as shown in example below:

![Content Illustration: Inkscape: Example Complex Layers][impress-08-layers]

With color from `material design` palette,
it is easy to switch the color, to suit your needs.
The guidance lines in above figure shown blue material color,
because originally the shapes are blue,
before I turn them to the red counterparts.

#### Coloring Example

For easy color switching,
you might want to utilize material pallete color only.

![Candyclone Template: Inkscape: Material Design Pallete][coloring-material]

-- -- --

### Custom Icons

> I'm not a lawyer. I avoid mixing licenses.

Of course I can use any free icon such as FontAwesome or Google Icons.
But I decided to use my own custom made icon that I have made in 2017.

![Content Illustration: Inkscape: Custom Nano Icons][custom-nano-icons]

I call this nano icon.
My intention is to make something like atom bonding,
and I failed miserably.
Until I finally find it useful for this diagram in 2020.

-- -- --

### All Diagram Illustrations

Here I represent all the diagram illustrations

#### 01: Timeline

![Content Illustration: Impress: Preview 01][preview-content-01]

#### 02: Simple Flow

![Content Illustration: Impress: Preview 02][preview-content-02]

#### 03: Two Sided Cutter

![Content Illustration: Impress: Preview 03][preview-content-03]

#### 04: Middle Sprocket Chain

![Content Illustration: Impress: Preview 04][preview-content-04]

#### 05: Synergy

![Content Illustration: Impress: Preview 05][preview-content-05]

#### 06: Arrow Verse

![Content Illustration: Impress: Preview 06][preview-content-06]

#### 07: Wave Columns

![Content Illustration: Impress: Preview 07][preview-content-07]

#### 08: 3D Divider

![Content Illustration: Impress: Preview 08][preview-content-08]

#### 09: 3D Step by Step

![Content Illustration: Impress: Preview 09][preview-content-09]

#### 10: Bolt Fingers: Middle Center

![Content Illustration: Impress: Preview 10][preview-content-10]

#### 11: Bolt Fingers: Left

![Content Illustration: Impress: Preview 11][preview-content-11]

#### 12: Brake Pads

![Content Illustration: Impress: Preview 12][preview-content-12]

#### 13: Reservoir

![Content Illustration: Impress: Preview 13][preview-content-13]

#### 14: Grinder Disc

![Content Illustration: Impress: Preview 14][preview-content-14]

#### 15: Partial Disc

![Content Illustration: Impress: Preview 15][preview-content-15]

-- -- --

### What is Next ?

Consider continue reading [ [Inkscape: Impress Slides - Part Five][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}design/2020/09/25/inkscape-impress-slides-05/

[thumbs-3-preview]: {{< assets-design >}}/2020/09-impress/thumbs-3.png
[coloring-material]:{{< assets-design >}}/2020/09-impress/coloring-content.png

[source-inkscape]:  {{< assets-design >}}/2020/09-impress/template-candyclone-content.svg.zip
[content-layers]:   {{< assets-design >}}/2020/09-impress/04-content-inkscape-all-layers.png
[content-layer-base]:   {{< assets-design >}}/2020/09-impress/04-content-inkscape-layer-base.png
[inkscape-master-slide]:{{< assets-design >}}/2020/09-impress/04-content-inkscape-master-slide-source.png
[impress-insert-image]: {{< assets-design >}}/2020/09-impress/04-impress-insert-image.png
[impress-position-size]:{{< assets-design >}}/2020/09-impress/04-impress-position-size.png
[custom-nano-icons]:    {{< assets-design >}}/2020/09-impress/04-content-inkscape-custom-nano-icons.png

[inkscape-03-cut]:      {{< assets-design >}}/2020/09-impress/04-content-inkscape-03-cut.png
[inkscape-03-source]:   {{< assets-design >}}/2020/09-impress/04-content-inkscape-03-source.png
[inkscape-03-import]:   {{< assets-design >}}/2020/09-impress/04-content-inkscape-03-import-source.png
[impress-03-fresh]:     {{< assets-design >}}/2020/09-impress/04-content-impress-03-fresh-image.png
[impress-03-final]:     {{< assets-design >}}/2020/09-impress/04-content-impress-03-final-image.png
[impress-08-layers]:    {{< assets-design >}}/2020/09-impress/04-content-inkscape-08-layers.png

[preview-content-01]:   {{< assets-design >}}/2020/09-impress/04-01-candyclone-content-timeline.png
[preview-content-02]:   {{< assets-design >}}/2020/09-impress/04-02-candyclone-content-simple-flow.png
[preview-content-03]:   {{< assets-design >}}/2020/09-impress/04-03-candyclone-content-two-sided.png
[preview-content-04]:   {{< assets-design >}}/2020/09-impress/04-04-candyclone-content-synergy.png
[preview-content-05]:   {{< assets-design >}}/2020/09-impress/04-05-candyclone-content-sprocket.png
[preview-content-06]:   {{< assets-design >}}/2020/09-impress/04-06-candyclone-content-arrow-verse.png
[preview-content-07]:   {{< assets-design >}}/2020/09-impress/04-07-candyclone-content-wave-columns.png
[preview-content-08]:   {{< assets-design >}}/2020/09-impress/04-08-candyclone-content-3d-divider.png
[preview-content-09]:   {{< assets-design >}}/2020/09-impress/04-09-candyclone-content-3d-step-by-step.png
[preview-content-10]:   {{< assets-design >}}/2020/09-impress/04-10-candyclone-content-bolt-finger-middle-center.png
[preview-content-11]:   {{< assets-design >}}/2020/09-impress/04-11-candyclone-content-bolt-finger-left-mounting.png
[preview-content-12]:   {{< assets-design >}}/2020/09-impress/04-12-candyclone-content-brake-pads.png
[preview-content-13]:   {{< assets-design >}}/2020/09-impress/04-13-candyclone-content-reservoir.png
[preview-content-14]:   {{< assets-design >}}/2020/09-impress/04-14-candyclone-content-grinder-disc.png
[preview-content-15]:   {{< assets-design >}}/2020/09-impress/04-15-candyclone-content-partial-disc.png
