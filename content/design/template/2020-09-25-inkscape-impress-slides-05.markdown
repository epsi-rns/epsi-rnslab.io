---
type   : post
title  : "Inkscape: Impress Slides - Part Five"
date   : 2020-09-25T04:29:15+07:00
slug   : inkscape-impress-slides-05
categories: [design]
tags      : [inkscape, LibreOffice]
keywords  : [impress, master slides, template, original artwork]
author : epsi
opengraph:
  image: assets-design/2020/09-inkscape/drops-color-annotation.png

toc    : "toc-2020-inkscape-template"

excerpt:
  Creating illustration diagram using inkscape.

---

### Preface

> Creating illustration diagram using inkscape.

How exactly is the inkscape process,
to create this illustration?
Do you want to reproduce the process,
so you can make your authentic shape?

I believe you guys have design knowledge,
but if you should create from scratch,
I am here to give one example.
So you can reproduce the illustration,
and modify to suit your needs.

#### Article Series

This is a six parts article series.

* Part One:   Auto, the simple template

* Part Two:   Candyclone, the modern classic template

* Part Three: Adding custom bullets decoration

* Part Four:  Adding content illustration

* Part Five:  Creating illustration diagram

* Part Six:   Adding genuine artwork

![Thumbs 6 Preview][thumbs-6-preview]

-- -- --

### The Drop Pinned Shape

The shape the want to create is as below.
Pardon my taste of art. I'm not a designer.

![Drop Pinned: Shape][pinned-shape]

Most of the time, I group all object into one.

-- -- --

### Coloring

I'm using pallete from material design as an example,
so you can change to any color in that pallete, any time.
Here is a coloring example.

![Drop Pinned: Coloring][pinned-color]

The transparent color is optional.
Sometimes you need a gap, in box with precise size,
so you need a box with transparent color.
Most of the time, I do not need this transparent color.

-- -- --

### Size Planning

> I'm an engineer, I like to do things precisely.

It is a good practice to plan the size, for reusable shape.
I mean, if you are going to use,
this shape many times, then you need proper size.
Thus you can place, resize or transform, with correct precision.

I start with simple size such as 100px, 200px and so on.
This shape has 200px width x 300px height.
The circle in the inside start from 100px diameter.

![Drop Pinned: Size Plan][pinned-measure]

I know most designer like to draw things freely.
Here I present other perspective to do things.

-- -- --

#### Offsetting

The other combination is mostly just offsetting the size by 10px.
You can scale object with transform tools.

For example, select circle with diameter 100px.
Duplicate the shape first.
Then go to `transform` tool dock, select `scale` tab,
and change the unit to `px`.

Now select with 110px (offset 10px),
and check proportional scale.

Click `apply`, then you get circle with 110px diameter.
With the same center point.

![Drop Pinned: Offset 10px][pinned-offset-10]

-- -- --

### Nodes Bending

But how exactly do this shape created?
This six step in figure below should explain clearly.

![Drop Pinned: Nodes Bending][pinned-nodes]

#### 1: Create Circle Shape

You can see in the first picture,
that there is three node in circle.
You have to rotate it before using it.
Because we need to bend it to bottom instead to the left.

#### 2: Alter Node

We are going to change the node of the circle with 180px diameter.
Add node for both side, so now we have five nodes.
Then move the lower most node to about 50px below.

You can use any size other than 50px.

#### 3: Prettify the shape

Again add node for both side, so now we have seven nodes.
And move the third and fifth node,
with horizontal distance about 20px from center.

#### 4: Offset The Object

Again, you can offset the size of a shape,
using `scale tab` from `transform dock`.
For example from 180px to 190px.

Just remember that since the shape of the object is not a circle,
you should adjust the positioning, to 5px below the original shape,
to make sure that the 190px shape has the center with the 180px shape.

#### 5: Adjust Shape

Move the lower most node to about 25px below.
You are freely to use any distance size.

#### 6: Finished Shape

Repeat making the size for 200px circle.
Move the lower most node to so we finally have 300px height shape.

-- -- --

### Uniform Rotating

#### Setting Midpoint

There are a few different way to rotate object in Inkscape.
I like to utilize `snap` feature, and change midpoint to other circle.

![Drop Pinned: Setting Midpoint][pinned-midpoint]

#### Transform Tools

Now you can rotate the object using `rotate` tab in `transform dock`.

![Drop Pinned: Rotate with Transform Tools][pinned-rotate-72]

For example, with 5 number of shape,
we should set the angle of circular movement to 72 degree.

#### Circular Result

And here is the result in this figure below:

![Drop Pinned: Circular][pinned-circular]

I won't say this is the final result.
There are many ways to improve this shape,
such as adding icon inside a shape,
or rotate 15 degree for all object as a whole.

But at least your shape is ready by now.
It is a good time to go back to Impress.

-- -- --

### What is Next ?

Consider continue reading [ [Inkscape: Impress Slides - Part Six][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]: {{< baseurl >}}design/2020/09/26/inkscape-impress-slides-06/
[thumbs-6-preview]: {{< assets-design >}}/2020/09-impress/thumbs-6.png

[pinned-shape]:     {{< assets-design >}}/2020/09-inkscape/drops-shape.png
[pinned-color]:     {{< assets-design >}}/2020/09-inkscape/drops-color-annotation.png
[pinned-circular]:  {{< assets-design >}}/2020/09-inkscape/drop-pinned-circular-final.png
[pinned-measure]:   {{< assets-design >}}/2020/09-inkscape/drops-measure-boxed.png
[pinned-nodes]:     {{< assets-design >}}/2020/09-inkscape/drops-path-nodes-screenshot.png

[pinned-midpoint]:  {{< assets-design >}}/2020/09-inkscape/drop-pinned-circularsnap-midpoint.png
[pinned-rotate-72]: {{< assets-design >}}/2020/09-inkscape/drop-pinned-circularsnap-transform-72.png
[pinned-offset-10]: {{< assets-design >}}/2020/09-inkscape/drop-pinned-offsetting-transform-10.png
