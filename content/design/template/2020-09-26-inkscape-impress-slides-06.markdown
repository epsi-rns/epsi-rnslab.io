---
type   : post
title  : "Inkscape: Impress Slides - Part Six"
date   : 2020-09-26T04:29:15+07:00
slug   : inkscape-impress-slides-06
categories: [design]
tags      : [inkscape, LibreOffice]
keywords  : [impress, master slides, template, original artwork]
author : epsi
opengraph:
  image: assets-design/2020/09-impress/04-reference-ex-03.png

toc    : "toc-2020-inkscape-template"

excerpt:
  Adding genunine artwork to impress slide using inkscape.

---

### Preface

> Adding genunine artwork to impress slide using inkscape.

Original artwork also the hard part of making presentation.
This is a nice to have, but can improve your presentation a lot.
Here is my artwork attempt that you can use for your daily presentation.
You can also share your artwork to be used by other people.

Still, all will be no good if you do not have good material to be presented.

#### Article Series

This is a six parts article series.

* Part One:   Auto, the simple template

* Part Two:   Candyclone, the modern classic template

* Part Three: Adding custom bullets decoration

* Part Four:  Adding content illustration

* Part Five:  Creating illustration diagram

* Part Six:   Adding genuine artwork

![Thumbs 2 Preview][thumbs-2-preview]

-- -- --

### Good Example

For a more complete example, I recommend you, to visit Hervy's work.

* <https://hervyqa.com/gnome-bluez-LibreOffice-impress-free-template/>

-- -- --

### Cover Slide

There is no doubt, with my lack of creativity,
my cover page is just a copy-paste of the looks of Hervy's works.

The only difference is, I put my own original artwork.
Although most of this artwork based tutorial in youtube.
Just like cover page from a book, basically you can put any picture,
that does not have to be related to your presentation.
Or maybe this is just me, that have this bad behaviour.

#### Example: CSS Concept

![Slide Cover: CSS Concept][cover-css-concept]

Artwork Source: [Spectrum Rainbow][rainbow-spectrum]

#### Example: Linux Diversity

![Slide Cover: Linux Diversity][cover-diversity]

Artwork Source: [Isometric Wallpaper][github-isometric]

#### Example: Desktop Customization

![Slide Cover: Desktop Customization][cover-ricing]

Artwork Source: [Inkscape: Isometric Infographics][local-isometric]

-- -- --

### Reference Link and Simple Point

Sometimes you have a very simple slide with a very few text.
You can either keep your presentation clean from any decoration,
or give it an artwork, as we did with cover slide.

#### Example Simple Point 1

![Simple Point: Example 1][sim-ex-01]

#### Example Simple Point 2

![Simple Point: Example 2][sim-ex-02]

#### Example Reference 1

![Reference Link: Example 1][ref-ex-01]

#### Example Reference 2

![Reference Link: Example 2][ref-ex-02]

#### Example Reference 3

It is always better if the image is related to the link topic.

![Reference Link: Example 3][ref-ex-03]

-- -- --

### Common Slide

> No more artwork

There is, of course a reccuring slide,
that happened in every presentation.
I'd rather keep this simple without artwork.

#### Opening: About The Author

![Common Slide: About Me][common-about-me]

#### Opening: About The Material or Topic

![Common Slide: About Material][common-about-matl]

#### Closing : What's Next?

![Common Slide: What's Next?][common-whats-next]

#### Closing : Questions

![Common Slide: Questions][common-question]

#### Closing : Thank You

![Common Slide: Thank You][common-thank-you]

-- -- --

### Publishing Template

Do not forget to share and publish your template.
In a blog, or any repository (github/gitlab/bitbucket).

* [github.com/.../impress-template-candyclone/README.md][github-candyclone]

![Share and Publish Template][isometric-preview]

_A friend in need is a friend indeed._

-- -- --

I think that's all for now.

Thank you for visiting.

[//]: <> ( -- -- -- links below -- -- -- )

[local-isometric]:      {{< baseurl >}}design/2015/11/11/inkscape-isometric-infographics/
[github-isometric]:     https://github.com/epsi-rns/isometric-wallpaper
[github-candyclone]:    {{< berkas2-blob >}}//impress-template-candyclone/README.md
[rainbow-spectrum]:     https://akutidaktahu.netlify.app/assets/posts/desain/2017/11-ilusin/rainbow-spectrum.svg

[thumbs-2-preview]: {{< assets-design >}}/2020/09-impress/thumbs-2.png
[isometric-preview]:{{< assets-design >}}/2020/09-impress/isometric-preview.png

[cover-css-concept]:{{< assets-design >}}/2020/09-impress/05-cover-concept-css.png
[cover-ricing]:     {{< assets-design >}}/2020/09-impress/05-cover-customization.png
[cover-diversity]:  {{< assets-design >}}/2020/09-impress/05-cover-diversity.png

[sim-ex-01]:    {{< assets-design >}}/2020/09-impress/05-simple-ex-01.png
[sim-ex-02]:    {{< assets-design >}}/2020/09-impress/05-simple-ex-02.png
[ref-ex-01]:    {{< assets-design >}}/2020/09-impress/05-reference-ex-01.png
[ref-ex-02]:    {{< assets-design >}}/2020/09-impress/05-reference-ex-02.png
[ref-ex-03]:    {{< assets-design >}}/2020/09-impress/05-reference-ex-03.png

[common-about-me]:  {{< assets-design >}}/2020/09-impress/05-common-about-me.png
[common-about-matl]:{{< assets-design >}}/2020/09-impress/05-common-about-material.png
[common-whats-next]:{{< assets-design >}}/2020/09-impress/05-common-what-is-next.png
[common-question]:  {{< assets-design >}}/2020/09-impress/05-common-question.png
[common-thank-you]: {{< assets-design >}}/2020/09-impress/05-common-thank-you.png

