---
type   : post
title  : "Sozi Animation - Part One"
date   : 2020-09-27T04:29:15+07:00
slug   : sozi-animation-01
categories: [design]
tags      : [inkscape, Sozi]
keywords  : [animation, shape, presentation, border line]
author : epsi
opengraph:
  image: assets-design/2020/09-sozi/11-sozi-08-lt-frames.png

toc    : "toc-2020-inkscape-template"

excerpt:
  Making animated presentation entrance using Sozi.

---

### Preface

> Making animated presentation entrance using Sozi.

I name this entrance `candyline`
Because this design based on my `candyclone` Impress template.

#### Article Series

This is a three parts article series.

* Part One: Border Line

* Part Two: Title

* Part Three: Cubes Decoration

![Candyline: Lines Preview][Lines-preview]

-- -- --

### About Sozi

> Changing Shape Position Between Frames.

Sozi is a presentation tools utilizing shape from inkscape,
animating each shape position between frames.
The fact that Sozi is using Inkscape, make it an ideal tool,
because all you need is just inkscape to handle all the stuff,
and the Sozi itself to animate each shape.

#### Official Site

* [sozi.baierouge.fr](https://sozi.baierouge.fr/)

All you need is to download the Sozi release, and it runs well.

#### Sozi Export

Exporting to other media can be achieved with other tools,
such as `sozi-export`.

This `sozi-export` is works well in my openSUSE,
but I have difficulties to run it in my Gentoo,
since `sozi-export` rely on deprecated `libav`.

#### Method

There is at least three ways to set positioning in Sozi.

1. Move the position directly using mouse and keyboard.

2. Using frames

3. Edit JSON file directly (not recommended).

I'm more like a technical person,
as opposed as designer with taste of art.
So I choose to use frames as based of shape positioning in Sozi.

You may choose the first method as well.
There is no need to argue.

As the third method, using JSON,
sometimes I have a look at the source,
just to know how it works.
It works, but I prefer to use frames instead.

#### Example Material

I put all example in this article in this repository:

* [github.com/epsi-rns/berkas2][repository]

You can download them all freely.

-- -- --

### Inkscape: Prepare Based Shape

#### Document Properties

Just like Impress, Sozi utilize ratio for presentation,
such as `4:3` or else.
I use `4:3` ratio that is more common in video.
Of course you can use `16:9`,
but the result would be stretched in some social media.

Page size is irrelevant when it comes to Sozi.
But for our own reference,
it is going to be easier if we set the size first.
I'm using `400px` width * `300px` height.

You can use any size as along as it reflect the presentation ratio.

#### Layers

Just like any other project, I started with basic shape.
It consist of three layers.

* Background,

* Source (before cut), and

* Cut (the final image).

![Base: Three Layers][base-layers]

#### Source Before Cut

Our design is just a border line.
Both left border and right border are just,
a rectangle with `10px` width.

![Base: Source Before Cut][base-source]

I'm using blue color from google material pallete.
You can use different color as well.

#### Final Image After Cut

Cut the source image above,
and you will get the result as below:

![Base: Final Image After Cut][base-cut]

The color arrangement is set, consecutively as below

* Left Bottom: `blue100`,

* Left Middle: `blue300`,

* Left Top: `blue500`,

* Right Bottom: `blue500`,

* Right Middle: `blue700`,

* Right Top: `blue900`.

#### SVG Source Image

I also put the SVG source of this base image in my repository:

* [github.com/.../candyline-base-shape.svg][source-base]

-- -- --

### Inkscape: Lines Only

> A long preparation, please be patient!

Now create another inkscape file artefact,
based on the base inkscape above.
We use different inkscape files,
because we do not messed up animation with basic design.

#### SVG Source Image

I also put the SVG source of this animation image in my repository:

* [github.com/.../candyline-only.svg][source-inkscape]

This article discuss line only. That's why I give weird name as above.

#### Layers

Since Sozi works based on layers,
we must separate each shape in its own layers,
including the background.
Now the SVG files consist of seven layers.

* Background,

* Left Bottom, Left Middle, Left Top,

* Right Bottom, Right Middle, and Right Top.

![Lines Only: Seven Layers][lines-layers]

#### Frames Sublayers

Sozi also works based on frames.
Each frames can be placed anywhere,
but it is going to be easier to put them in a layer,
so you can hide all frames easily, by hiding layer in inkscape.

For a beginner, it is better to follow Sozi example,
to put frames as a sublayer.
So now we have seven frames sublayers as below

* Background: Frames,

* Left Bottom: Frames, Left Middle: Frames, Left Top: Frames,

* Right Bottom: Frames, Right Middle: Frames, and Right Top: Frames.

![Lines Only: Frames SubLayers][frames-sublayers]

#### Object ID: Background

Sozi match the positioning by frames.
But how exactly is this?
The positioning can be set by using object `ID`.
For example with a red rectangle frame below:

![Lines Only: Object ID: Background 01:01][object-id-bg-1-1]

You may name the object with anything you want.
I name this as `bg-frame-01-01`,
to show of scale ratio relative to pages.

#### Object ID: Lines

You can make as many frame as you want,
so you can choose frames easily in Sozi with your own prebuilt frames.
For example this left bottom shape, might require zooming effect in Sozi,
start with `1/6` of the size.
So I make a frame with the size six times of the original page.

![Lines Only: Object ID: Left Bottom 06:01][object-id-lb-6-1]

I make a few custom frame for this `Left Bottom` shape such as:

* LB Frame 02:01: `lb-frame-02-01`

* LB Frame 04:01: `lb-frame-04-01`

* LB Frame 06:01: `lb-frame-06-01`

* LB Frame 08:01: `lb-frame-08-01`

* LB Frame 10:01: `lb-frame-10-01`

* LB Frame 12:01: `lb-frame-12-01`

You only need to make these frameset once.
No need to make frameset for all shapes.
This means Right Bottom image can use the Left Bottom frame,
As long as you know the object `ID` to be set in Sozi.

Now that we are done with `Inkscape` document.
We can continue straight away to `Sozi` document.

-- -- --

### Sozi: Lines Only

#### Preview Without Frame

Before we begin, consider have a look at the preview.

{{< embed-video width="512" height="384" src="assets/posts/design/2020/09-sozi/candyline-only-without-frame-small.mp4" >}}

#### Command Line

You can open inkscape files in Sozi, directly in Terminal Shell.

{{< highlight bash >}}
❯ cd /media/Works/bin/Sozi-20.05.09-1589035558-linux-x64

❯ ./Sozi ~/Documents/step-01-lines/candyline-only.svg &!

{{< / highlight >}}

![Sozi: Opening Inksape Files][running-sozi]

It is easier this way.

#### Empty Document

Of course, we start from empty documents.

![Sozi: Empty Document][sozi-01-empty]

No frames. No Layers.

#### Positioning: Background

Three steps for these:

1. Add new frame, and give it a title `Start`.

2. Add background layer.

3. Set frame positioning: `Outline element ID` should be set to `bg-frame-01-01`.

![Sozi: Positioning: Background][sozi-02-bg]

Now we have proper presentation size, match the inkscape page size.

#### Positioning: One Line

Add one layer at a time, start from `Left Bottom`.
Then set the positioning in `Outline element ID`,
for example, set to `lb-frame-10-01` to scale it to `1/10` size.

![Sozi: Positioning: Left Bottom][sozi-03-lb]

#### Positioning: All Lines

Do it for the rest of the layers.

![Sozi: Positioning: All Lines][sozi-04-all]

Remember that each layer represent one line shape.
So we have total six line shapes, with size of `1/10` ratio size.

#### Movement: Prepare All Frames

Prepare to make each movement for each shapes, by making six frames.

* Left Bottom, Left Middle, Left Top,

* Right Bottom, Right Middle, and Right Top.

![Sozi: Movement: Prepare All Frames][sozi-05-frames]

You can use any title for those frames.

#### Movement: Frame: Left Bottom

Set new positioning in for the `Left Bottom` frame,
to suit the `1:1` size of the presentation slide.

* Left Bottom: `bg-frame-01-01`

![Sozi: Movement: Left Bottom Position][sozi-06-lb-f]

You can also set the positioning for the rest of the layers.

* Left Middle: `lm-frame-06-01`

* Left Top: `lt-frame-12-01`

* Right Bottom: `rb-frame-10-01`

* Right Middle: `rm-frame-08-01`

* Right Top: `rt-frame-08-01`

![Sozi: Movement: The Rest of Layer Position][sozi-06-canvas]

Note that you can use any frame positioning, to get different effects.
Be creative, try different frame, even with rotating position.

#### Movement: Left Middle

Set new positioning for the each frames.

* Left Middle: `bg-frame-01-01`

And the rest of the layers

* Left Top: `lt-frame-08-01`

* Right Bottom: `rb-frame-06-01`

* Right Middle: `rm-frame-12-01`

* Right Top: `rt-frame-06-01`

![Sozi: Movement: Left Middle Position][sozi-07-lm-f]

#### Movement: Left Top

And the rest is following.

* Left Top: `bg-frame-01-01`

And the rest of the layers

* Right Bottom: `rb-frame-04-01`

* Right Middle: `rm-frame-06-01`

* Right Top: `rt-frame-02-01`

You can see how it works by unhide all frames in inkscape.

![Sozi: Movement: Left Top Position with Frames][sozi-08-lt-f]

#### Movement: Right Bottom

For each progress, the lesser we have to set.

* Right Bottom: `bg-frame-01-01`

And the rest of the layers

* Right Middle: `rm-frame-08-01`

* Right Top: `rt-frame-08-01`

#### Movement: Right Middle

Until finally there are only these two.

* Right Middle: `bg-frame-01-01`

And the rest of the layers

* Right Top: `rt-frame-04-01`

#### Movement: Right Top

And we are done with this.

* Right Top: `bg-frame-01-01`

#### Preview With Frame

Now that we are done, consider have a look again at the video, but this time with frame shown, to see how it works.

{{< embed-video width="512" height="384" src="assets/posts/design/2020/09-sozi/candyline-only-with-frame-small.mp4" >}}

You can set the `frames` layers visibility using `inkscape`.

-- -- --

### Sozi Export

#### OGV Format

You can convert to video easily using `sozi-export`.

{{< highlight bash >}}
$ sozi-to-video candyline-only.sozi.html
{{< / highlight >}}

This will result to a video named `candyline-only.sozi.ogv`.

![Sozi Export][sozi-to-video]

#### MP4 Format

If you want to share social media, you need proper format.
You can utilize `ffmpeg`.

{{< highlight bash >}}
ffmpeg -i candyline-only.sozi.ogv \
  -vf "scale=iw/2:ih/2" \
  -c:v libx264 \
  -profile:v baseline \
  -level 3.0 \
  -pix_fmt yuv420p \
  candyline-only-small.mp4
{{< / highlight >}}

#### GIF Format

Github pages do not support video embedding.
But you can substiute with `gif` instead.

{{< highlight bash >}}
ffmpeg -i candyline-cubes.sozi.ogv \
       -vf "fps=10,scale=400:-1:flags=lanczos,split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse"\
       -loop 0 \
       candyline-only-small.gif
{{< / highlight >}}

-- -- --

### What is Next ?

Consider continue reading [ [Sozi Animation - Part Two][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}design/2020/09/28/sozi-animation-02/

[repository]:       https://github.com/epsi-rns/berkas2/
[source-base]:      https://github.com/epsi-rns/berkas2/blob/master/sozi-candyline/step-01-lines/candyline-base-shape.svg
[source-inkscape]:  https://github.com/epsi-rns/berkas2/blob/master/sozi-candyline/step-01-lines/candyline-only.svg

[base-layers]:  {{< assets-design >}}/2020/09-sozi/10-base-layers.png
[base-source]:  {{< assets-design >}}/2020/09-sozi/10-base-source.png
[base-cut]:     {{< assets-design >}}/2020/09-sozi/10-base-cut.png

[lines-preview]:    {{< assets-design >}}/2020/09-sozi/11-candyline-only-small.gif
[lines-layers]:     {{< assets-design >}}/2020/09-sozi/11-lines-only-layers.png
[frames-sublayers]: {{< assets-design >}}/2020/09-sozi/11-frames-sublayers.png
[object-id-bg-1-1]: {{< assets-design >}}/2020/09-sozi/11-object-id-bg-frame.png
[object-id-lb-6-1]: {{< assets-design >}}/2020/09-sozi/11-object-id-lb-frame.png

[running-sozi]:     {{< assets-design >}}/2020/09-sozi/11-running-sozi.png
[sozi-01-empty]:    {{< assets-design >}}/2020/09-sozi/11-sozi-01-empty.png
[sozi-02-bg]:       {{< assets-design >}}/2020/09-sozi/11-sozi-02-background.png
[sozi-03-lb]:       {{< assets-design >}}/2020/09-sozi/11-sozi-03-left-bottom.png
[sozi-04-all]:      {{< assets-design >}}/2020/09-sozi/11-sozi-04-all-lines.png
[sozi-05-frames]:   {{< assets-design >}}/2020/09-sozi/11-sozi-05-all-frames.png
[sozi-06-lb-f]:     {{< assets-design >}}/2020/09-sozi/11-sozi-06-lb-frames.png
[sozi-06-canvas]:   {{< assets-design >}}/2020/09-sozi/11-sozi-06-lb-frames-canvas.png
[sozi-07-lm-f]:     {{< assets-design >}}/2020/09-sozi/11-sozi-07-lm-frames.png
[sozi-08-lt-f]:     {{< assets-design >}}/2020/09-sozi/11-sozi-08-lt-frames.png
[sozi-to-video]:    {{< assets-design >}}/2020/09-sozi/11-sozi-to-video.png
