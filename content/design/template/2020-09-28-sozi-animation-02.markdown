---
type   : post
title  : "Sozi Animation - Part Two"
date   : 2020-09-28T04:29:15+07:00
slug   : sozi-animation-02
categories: [design]
tags      : [inkscape, Sozi]
keywords  : [animation, shape, presentation, title, decoration]
author : epsi
opengraph:
  image: assets-design/2020/09-sozi/12-candyline-title-preview.png

toc    : "toc-2020-inkscape-template"

excerpt:
  Making animated presentation entrance using Sozi.

---

### Preface

> Making animated presentation entrance using Sozi.

One sozi frames can be used for multiple animation at once.

#### Article Series

This is a three parts article series.

* Part One: Border Line

* Part Two: Title

* Part Three: Cubes Decoration

{{< embed-video width="512" height="384" src="assets/posts/design/2020/09-sozi/candyline-title.mp4" >}}

-- -- --

### Inkscape: Titles

#### SVG Source Image

I also put the SVG source of this image in my repository:

* [github.com/.../candyline-title.svg][source-title]

#### Layers

Add four more Layers:

* Main Title, Sub Title, Date, Decoration

Remember that we already have these layers below:

* Background,

* Left Bottom, Left Middle, Left Top,

* Right Bottom, Right Middle, and Right Top.

![Title: Four More Layers][title-layers]

The order of the layers will also matters in Sozi,
the top most layer will also be shown in top most layer in Sozi,
and also in the result of presentation.

#### Title: Entrance Frame Objects

In a nutshell the entrance frame objects is as below image:

![Inkscape: Title: Entrance Frame Objects][entrance-frames]

1. Main Title: `mt-frame-01-01`, `Y = -300px`

2. Sub Title: `st-frame-01-01`, `X = 400px`

3. Date: `dt-frame-01-01`, Rotate 45 degrees.

#### Title: Additional Frame Objects

And the movement arrangement set in additional frame objects:

![Inkscape: Title: Additional Frame Objects][additional-frames]

1. `mt-frame-x30`, `X = -30px`

2. `mt-frame-x70`, `X = -70px`

3. `mt-frame-x100`, `X = -100px`

-- -- --

### Sozi: Titles

#### The Frames

All the frames can be shown here.

![Sozi: Title: All Frames and Layers][title-frames]

All the Frames are:

* Start, Border One, Border Two,

* Title In (entrance),

* Title Move, Title Max, Title Back, Title Stop

* Still (enable audience to see the last position for a few moment).

#### Title: Entrance Frame Objects

Set the frame positioning in `Border Two` as below:

* Main Title: `mt-frame-01-01`

* Sub Title: `st-frame-01-01`

* Date: `dt-frame-01-01`

And set the frame positioning in `Title In` as below:

* Main Title: `bg-frame-01-01`

* Sub Title: `bg-frame-01-01`

* Date: `bg-frame-01-01`

And have a look at the result:

![Candyline: Title Entrance Preview][title-entrance]

#### Title: Additional Frame Objects

We have four more frames to go:

* Title Move (move to the right, to 30px or 70px)

* Title Max  (move to the right to maximum 100px)

* Title Back (move to the left, to 30px or 70px, or 0px)

* Title Stop (back to the original position 0px)

Set the frame positioning in `Title Move` as below:

* Main Title: `mt-frame-x30`

* Sub Title: `mt-frame-x70`

* Date: `mt-frame-x30`

Then adjust the frame positioning in `Title Max` as below:

* Main Title: `mt-frame-x100`

* Sub Title: `mt-frame-x100`

* Date: `mt-frame-x100`

Again, set the frame positioning in `Title Back` as below:

* Main Title: `bg-frame-01-01`

* Sub Title: `mt-frame-x30`

* Date: `mt-frame-x30`

And finally, set the frame positioning in `Title Stop` as below:

* Main Title: `bg-frame-01-01`

* Sub Title: `bg-frame-01-01`

* Date: `bg-frame-01-01`

Now we have our result as below:

![Candyline: Title Movement Preview][title-movement]

-- -- --

### Inkscape: Simple Decoration

This article show simple decoration animation.
We will enhance this decoration in the next article.

You can use any image that you want for decoration.

#### Decoration: Frame Objects

This only consist two frames object:

* Hidden Small: `dec-frame-04-04`, for original position.

* Displayed Small: `dec-frame-04-04-ce`, for entrance to center page.

![Inkscape: Decoration: Frame Objects][deco-frames]

-- -- --

### Sozi: Simple Decoration

We can utilize, already made frames.
No need to make any new frame.

#### Decoration: Frame Objects

We have three frames to be used:

* Title Move (still hiddden)

* Title Max  (entrance, still small)

* Title Back (resize to normal size)

Set the frame positioning in `Title Move` as below:

* Decoration: `dec-frame-04-04`

Then the frame positioning in `Title Max` as below:

* Decoration: `dec-frame-04-04-ce`

Again, set the frame positioning in `Title Back` as below:

* Decoration: `bg-frame-01-01`

Now we have our result as below:

![Candyline: Decoration Movement Preview][deco-movement]

-- -- --

### What is Next ?

Consider continue reading [ [Sozi Animation - Part Three][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}design/2020/09/29/sozi-animation-03/

[source-title]:     https://github.com/epsi-rns/berkas2/blob/master/sozi-candyline/step-02-titles/candyline-title.svg

[title-layers]:     {{< assets-design >}}/2020/09-sozi/12-inkscape-title-layers.png
[additional-frames]:{{< assets-design >}}/2020/09-sozi/12-inkscape-title-additional-frames.png
[entrance-frames]:  {{< assets-design >}}/2020/09-sozi/12-inkscape-title-entrance-frames.png

[title-entrance]:   {{< assets-design >}}/2020/09-sozi/12-candyline-title-entrance-small.gif
[title-movement]:   {{< assets-design >}}/2020/09-sozi/12-candyline-title-movement-small.gif
[deco-movement]:    {{< assets-design >}}/2020/09-sozi/12-candyline-title-decoration-small.gif

[title-frames]:     {{< assets-design >}}/2020/09-sozi/12-sozi-title-frames.png
[deco-frames]:      {{< assets-design >}}/2020/09-sozi/12-inkscape-decorations-frames.png
