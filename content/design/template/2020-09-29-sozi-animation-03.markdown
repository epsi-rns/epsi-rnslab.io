---
type   : post
title  : "Sozi Animation - Part Three"
date   : 2020-09-29T04:29:15+07:00
slug   : sozi-animation-03
categories: [design]
tags      : [inkscape, Sozi]
keywords  : [animation, shape, presentation, title, cubes decoration]
author : epsi
opengraph:
  image: assets-design/2020/09-sozi/13-sozi-entrance-frames.png

toc    : "toc-2020-inkscape-template"

excerpt:
  Making animated presentation entrance using Sozi.

---

### Preface

> Making animated presentation entrance using Sozi.

One sozi frames can be used for muliple animation at once.
We are going to explore.

#### Article Series

This is a three parts article series.

* Part One: Border Line

* Part Two: Title

* Part Three: Cubes Decoration

{{< embed-video width="512" height="384" src="assets/posts/design/2020/09-sozi/candyline-with-audio-small.mp4" >}}

-- -- --

### Inkscape: Cubes Animation

We are going to make a better version of previous cubes animation.
The movement idea is, highly inspired from,
the excellent works by `Rania Amina`,
that he made for the release of LibreOffice 7.0.

* [LibreOffice-7.0-Final.mp4][libreoffice-final]

#### SVG Source Image

I also put the SVG source of this image in my repository:

* [github.com/.../candyline-cubes.svg][source-cubes]

#### Layers

> Good things comes in small packages.

Small movement with detail looks better.
This is actually just a short vertical movement.
I manage each cube in each seven layers.

* Cube-01, Cube-02, Cube-03, Cube-04, Cube-05, Cube-06, Cube-07, Dummy.

Remember that we already have these layers below:

* Background,

* Left Bottom, Left Middle, Left Top,

* Right Bottom, Right Middle, and Right Top,

* Main Title, Sub Title, Date.

![Cubes: Seven More Layers][cubes-layers]

With assumption that the cubes count from left to right,
the order of the layer will become:

* Cube-04 (top most)

* Cube-05 and Cube-03,

* Cube-06 and Cube-02,

* Cube-07 and Cube-01.

What is the `dummy` layer anyway?
I decide not to have frames sublayer for each shape objects.
So I put all the required frames,
into that `dummy` layers for tidiness purpose.
In fact, you can put the frames in any layer.

#### Cubes: Entrance

We still have use previous setting, only consist two frames object:

* Hidden Small: `dec-frame-04-04`, for original position.

* Displayed Small: `dec-frame-04-04-ce`, for entrance to center page.

#### Cubes: Shakes

There is only two movement arrangement require:

1. `cubes-up-1`, `Y = 10px`

2. `cubes-down-1`, `Y = -10px`

![Cubes: Up and Down Frames][cubes-frames]

And that's all. That simple.

-- -- --

### Sozi: Cubes Animation

#### Cubes: Entrance

Set the frame positioning in `Start` as below:

* All Cubes [01..07]: `dec-frame-04-04`

![Sozi: Cubes: Entrance Frame Objects][entrance-frames]

The next thing to do, set in `Cubes One` as below:

* Cube-01: `bg-frame-01-01`

And then, set in `Cube Three` as below:

* Cube-05 and Cube-03: `bg-frame-01-01`

Again, set in `Cube Five` as below:

* Cube-06 and Cube-02: `bg-frame-01-01`

Finally, set in `Cube Stop` as below:

* Cube-07 and Cube-01: `bg-frame-01-01`

Also add `Still` frames, and set two seconds delay.

Now have a look at the result:

![Candyline: Cubes Entrance Preview][cubes-entrance]

#### Cubes: Shakes

We are going to use our last frame, the `Cube Stop`.
Set the frame positioning in `Cube Stop` as below:

* All Cubes [01..07]: `bg-frame-01-01`

![Sozi: Cubes: Shake Frame Objects][shake-frames]

The next thing to do, set in the first `Shake Up` as below:

* Cube-04: `cubes-down-1`

* Cube-06 and Cube-02: `cubes-up-1`

Note that you are free to set which cube is `up`,
and which cube is `down`, to get your very own desired effect.

Then, set in the first `Shake Down` as below:

* Cube-04: `bg-frame-01-01`

* Cube-05 and Cube-03: `cubes-down-1`

* Cube-06 and Cube-02: `bg-frame-01-01`

* Cube-07 and Cube-01: `cubes-up-1`

Again, set in the second `Shake Up` as below:

* Cube-04: `cubes-down-1`

* Cube-05 and Cube-03: `cubes-up-1`

* Cube-06 and Cube-02: `cubes-down-1`

* Cube-07 and Cube-01: `bg-frame-01-01`

And another around, set in the second `Shake Down` as below:

* Cube-04: `cubes-up-1`

* Cube-05 and Cube-03: `cubes-down-1`

* Cube-06 and Cube-02: `bg-frame-01-01`

* Cube-07 and Cube-01: `cubes-up-1`

Finally, set in `Shake Stop` as below:

* All Cubes [01..07]: `bg-frame-01-01`

Now have a look at the result:

![Candyline: Cubes Shakes Preview][cubes-shakes]

It is all depend on your creativity.
After all, this is about imagination.

-- -- --

### Put The Whole Thing Together

Finally we have a screenshot of all frames and layers together:

![Sozi: All Frame Objects][all-frames]

-- -- --

### Kdenlive

#### Adding Audio

You download free sound from the internet,
and combine with the Sozi result in Kdenlive.

![Kdenlive: Adding audio to Sozi Animation][kdenlive]

#### Final Result

Consider have a look again at the final result.

{{< embed-video width="512" height="384" src="assets/posts/design/2020/09-sozi/candyline-with-audio-small.mp4" >}}

-- -- --

### Conclusion

Sozi is a good tool for simple animation.

I think that's all for now.
Thank you for visiting.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}design/2020/09/29/sozi-animation-03/

[source-cubes]:     https://github.com/epsi-rns/berkas2/blob/master/sozi-candyline/step-03-cubes/candyline-cubes.svg
[libreoffice-final]:https://github.com/libreofficeid/LibreOffice-7-Video-Release/raw/master/LibreOffice-7.0-Final.mp4

[cubes-layers]:     {{< assets-design >}}/2020/09-sozi/13-inkscape-cubes-layers.png
[cubes-frames]:     {{< assets-design >}}/2020/09-sozi/13-inkscape-cubes-frames.png
[entrance-frames]:  {{< assets-design >}}/2020/09-sozi/13-sozi-entrance-frames.png
[shake-frames]:     {{< assets-design >}}/2020/09-sozi/13-sozi-shake-frames.png
[all-frames]:       {{< assets-design >}}/2020/09-sozi/13-sozi-all-frames.png
[kdenlive]:         {{< assets-design >}}/2020/09-sozi/14-kdenlive-add-audio.png

[cubes-entrance]:   {{< assets-design >}}/2020/09-sozi/13-candyline-cubes-entrance-small.gif
[cubes-shakes]:     {{< assets-design >}}/2020/09-sozi/13-candyline-cubes-shake-small.gif
