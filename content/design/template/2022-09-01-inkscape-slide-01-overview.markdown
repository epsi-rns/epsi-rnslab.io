---
type   : post
title  : "Inkscape: Slides - Overview"
date   : 2022-09-01T04:29:15+07:00
slug   : inkscape-slide-templates-01
categories: [design]
tags      : [inkscape]
keywords  : [master slides, template, candyclone, overview]
author : epsi
opengraph:
  image: assets/posts/design/2022/09/isometric/isometric-withgap-transparent.png

toc    : "toc-2020-inkscape-template"

excerpt:
  An overview candyclone, the inkscape master slide templates

---

### Preface

> An overview candyclone, the inkscape master slide templates

It has been a year since I last rewrote my template in Inkscape.
Each time I dove into it, more pages were added. However,
I haven't been entirely satisfied with how the template is presented in later versions.
As a result, I've been hesitant to share it publicly.
There's something that doesn't quite feel right,
because my multi-layered arrangement seems too complex for beginners.

Fortunately, Inkscape 1.2 was released in May this year,
introducing a multiple-page feature.
This feature is incredibly handy and can significantly streamline the workflow.
End-users can now easily choose a slide by selecting a specific page.
While it's not quite perfect yet,
it's sufficient for me to begin writing
about this template in a blog article.

It's not that I'm not proficient in English;
I can express my thoughts quite adequately.
It's just that I struggle to write in a way
that flows smoothly and is easy for people to understand.
I recognize that I need more practice.

#### Preview

*Warning:* Sound may contain explicit material.

{{< embed-video width="512" height="384" src="assets/posts/design/2022/09/candyclone-iso-amix.mp4" >}}

-- -- --

### Table of Content

This inkscape template is a rewrite version,
from my Impress work in 2020, to pure inkscape in 2022.
Instead of putting all inkscape stuff into Libreoffice Impress presentation,
I found that it is easier to work in pure inkscape.
I also add a few pages in 2022, and more in 2023.

#### Article Series

> Let's Get Organized.

This is a multiple parts article series.

Template crafting is a broad topic.
So I have to systemically separate the topic,
by the order of creation.

1. Crafting an Inkscape slide template background,
   with sub-layers.
   * Why: There should be an alternative template in which users have freedom,
     to modify and customize according to their specific needs.
   * What: Focus on creating a single customizable slide template.

2. Managing Inkscape master-slide templates,
   using the new multi-page feature.
   * Why: Layer structures could be standarized to facilitate
     seamless document exchange within a team.
   * What: Discussing how to handle multiple master-slide templates.

3. Organizing each color of Inkscape master-slide templates,
   using the new multi-page feature.
   * Why: Putting each slide on individual pages enhances workflow efficiency.
     Providing a clear view and allowing for easy selection.
   * What: Highlighting the use of multi-pages in master slides.

4. Versatile multi purpose Inkscape example-slide templates,
   arranged as multi-page presentations.
   * Why: Ready-to-use templates enable users
     to focus on content creation rather than design.
   * What: Emphasizing the versatility of example-slide templates.

5. Sample by sample, one at a time,
   with an emphasis on layer management.
   * Why: Layer as a powerful tool deserve a real world example.
   * What: Detail of previous slide along with its layer.

6. Beyond template, demonstrating our hard work.
   * Why: Showing off in social media as a marketing tools should be easy.
   * What: Thumbnails, 3D isometric, video with audio.

The first three are the main template.

![Inkscape: Thumbnail: Template: Teal][template-06-teal]

And the next two are the example content.

![Inkscape: Thumbnail: Example Content][content-06-flow-and]

And the last one, is post-slide processing.

Each topic might have one or more separated articles as well,
depend on the technical complexity in writing.

-- -- --

### Overview

> How about the template itself?

Enough with the tutorials;
it's time to consider the perspective of the end user.

#### Personal Work

> What take me so long? Since 2020 to 2022???

As a low-level employee with a workload that consumes much of my time,
I've faced numerous challenges in designing each page.
I've been thinking about making it easier for end users.
The most challenging part has been finding a few spare months
just to write the tutorial article.
Being just a humble worker,
my schedule has been quite packed,
especially during the COVID years.

Surely making this article, require full concentration.
Alone with just my own mini-pc, with relaxed me-time.
without anybody suspiciously staring at my big monitor,
repetitively asking me to do something else,
according to job description.

I made a conscious decision,
not to work on this template during office hours,
because I wanted complete ownership of the license.

#### Licensing

> Public Domain

This template, along with all the artwork,
is licensed under the Public Domain.
I understand how challenging it was to complete this template.
It would be a waste of time if nobody ever used it.

I created this template for my daily work,
but I also wanted to make it easier for people working with Inkscape.
That's why I've chosen to release it into the public domain.

![Inkscape Template Licensed: Public Domain][public-domain]

You're free to do whatever you want with it.
I'm not a lawyer, but I believe it's entirely permissible.
No attribution is necessary.
You can use, modify, sell, and so on,
for your specific needs, at your own risk.

#### SVG Source

* [Inkscape Template: Candyclone][inkscape-slides]

#### Impress

The Impress version is a classic modern template inspired by Hervy's work.

You can find the Impress version here:

* [Impress Template: Candyclone][impress-slides]

#### Credits: Hervy QA

> Kudos to his excellent works!

I drew inspiration from Hervy QA.
Without his examples,
I could have never created the `Candyclone` template.

This template is a complete rewrite based on the design by Hervy QA.
At first, I meticulously followed the original design page by page,
with Hervy QA's permission.
Then a add a bunch of my own.

You can find the original template here.

* [Gnome Bluez Libreoffice Impress Free Template][hervy-qa-bluez]

Don't forget to check out his videos too.

* [Demo GNOME Bluez Libre Office Impress Free Template][hervy-qa-video]

You can see the big difference.
But int he same time you can deny the influence of Hervy's works.

-- -- --

### Specification

#### License

This template along with all the artwork is licensed as Public Domain.

![Inkscape Template Licensed: Public Domain][public-domain]

#### Slides Ratio

> 16:9

#### Font

I use two fonts for this template:

1. Source San Pro

2. Source San Pro Semibold

#### Color

I use Google Material.
Google Material Pallete has 19 distinct color types.
With additional experimental gradient slide.
So it has a total of 20 distinct color types.

Note: I must admit the gradient type is not a good looking one.
But I put it here anyway, because I have nowhere else,
to store this experimental page slide.

#### Master Slide

> 5 * (19+1)

There are only four kinds of master slides:

* Default: 40px left, 30px right

* Default: 40px left, 40px right

* Chapter: For chapter title or chapter break

* Single: To show Figures, Chart or Infographics

* Background: Inverted version of default, using background

Each four kind of master slides implemented
with 19 google colors + 1 gradient slide.
Resulting total of 100 master slides.

-- -- --

### Preview

Without further explanation,
these slide examples in figures below may worth a thousand word.

#### Cover and Preface

![Inkscape: Isometric Preview: 01][candyclone-iso-01]

#### Flow and Milestones

![Inkscape: Isometric Preview: 02][candyclone-iso-02]

#### Miscellanous Creativity

![Inkscape: Isometric Preview: 03][candyclone-iso-03]

#### Bolt and Discs

![Inkscape: Isometric Preview: 04][candyclone-iso-04]

#### 3D Frames

![Inkscape: Isometric Preview: 05][candyclone-iso-05]

#### Point of Drops

![Inkscape: Isometric Preview: 06][candyclone-iso-06]

-- -- --

### Decoration Artwork Source

> Original Decoration

This inkscape slide template use my original artwork.
It is also available as below:

#### Bullet Numbering Sets

* [Inkscape: Progress Ring Animation][progress-ring]

#### Isometric Wallpapers

* [Inkscape: Isometric Wallpaper][isometric-wall]

#### Business Cards

* [Inkscape - Kartu Nama][kartu-nama]

#### Cubes

* [Inkscape: Isometric Infographics][isometric-cubes]

Good luck using it, now we can be ready for the detail in the next article.

-- -- --

### Learning Tips

> In most situations, having a friend can be invaluable.

To effectively use this template,
you'll need to become proficient in Inkscape.
Additionally, creating an impactful Inkscape slide,
requires mastering other Inkscape skills.
Keep in mind that when you need
to collaborate on documents within a team,
there are also some minor rules to learn.

Find a supportive community
where you can seek guidance
as you progress in your learning journey.
As the difficulty level increases,
having a place to ask questions becomes crucial.
Even when you don't have specific questions,
observing and learning from others can be immensely beneficial.
Investing in educating has its own cost,
but chance are your progress might empower the community.

Smart individuals often help each other
by sharing their knowledge or providing guidance.
Even on their worst days, they can still
point you in the right direction.
On the other hand, toxic individuals tend to be
domineering and unhelpful, hindering their own progress.
These two distinct types of people,
shape the tone of daily discussions,
so be prepared for both.

Regardless of your environment,
your growth mindset is the most important factor.
Within your environment,
your attitude plays a significant role in your progress.

-- -- --

### What Comes Next 🤔?

How do exactly, can I craft my own template, like these one?
The answer is: _start from simple_.

Consider continue reading [ [Inkscape: Slide Templates - Part Two][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}design/2022/09/03/inkscape-slide-templates-02/

[impress-slides]:   https://github.com/epsi-rns/berkas2/tree/master/impress-presentation
[inkscape-slides]:  https://github.com/epsi-rns/candyclone-inkscape/

[public-domain]:    {{< baseurl >}}assets/posts/design/2022/09/cc-pdm.png

[hervy-qa-bluez]:   https://hervyqa.id/gnome-bluez-libreoffice-impress-free-template/
[hervy-qa-video]:   http://www.youtube.com/watch?v=O3urHT5AHG8

[isometric-wall]:   https://github.com/epsi-rns/isometric-wallpaper
[kartu-nama]:       https://akutidaktahu.netlify.app/inkscape/2017/10/03/kartu-nama.html
[isometric-cubes]:  https://epsi-rns.gitlab.io/design/2015/11/11/inkscape-isometric-infographics/
[progress-ring]:    https://epsi-rns.gitlab.io/design/2017/11/15/inkscape-progress-ring/

[//]: <> ( -- -- -- links below -- -- -- )

[candyclone-iso-01]:    {{< baseurl >}}assets/posts/design/2022/09/isometric/candyclone-iso-01.png
[candyclone-iso-02]:    {{< baseurl >}}assets/posts/design/2022/09/isometric/candyclone-iso-02.png
[candyclone-iso-03]:    {{< baseurl >}}assets/posts/design/2022/09/isometric/candyclone-iso-03.png
[candyclone-iso-04]:    {{< baseurl >}}assets/posts/design/2022/09/isometric/candyclone-iso-04.png
[candyclone-iso-05]:    {{< baseurl >}}assets/posts/design/2022/09/isometric/candyclone-iso-05.png
[candyclone-iso-06]:    {{< baseurl >}}assets/posts/design/2022/09/isometric/candyclone-iso-06.png

[template-06-teal]:     {{< baseurl >}}assets/posts/design/2022/09/thumbs-template/06-teal.png
[content-06-flow-and]:  {{< baseurl >}}assets/posts/design/2022/09/thumbs-content/06-flow-and-milestones.png

