---
type   : post
title  : "Inkscape: Slides - Single"
date   : 2022-09-03T04:29:15+07:00
slug   : inkscape-slide-templates-02
categories: [design]
tags      : [inkscape]
keywords  : [master slides, template, candyclone, single]
author : epsi
opengraph:
  image: assets/posts/design/2022/09/isometric/candyclone-iso-01.png

toc    : "toc-2020-inkscape-template"

excerpt:
  Crafting an Inkscape slide template background,
  with sub-layers.

---

### Preface

> Crafting an Inkscape slide template background,
> with multiple sub-layers.

For every achievement,
one should start form simple thing.
Then good thing take some time.
Patience is your ally.

![Isometric Preview][candyclone-preview]

When creating something for teamwork,
always consider replication,
especially for the essential elements.
Think about how your work can be recreated by others
without needing access to your document's soft copy.
Otherwise, you risk unintentionally,
restricting your team's access with a read-only design.

#### Why

To enable users to modify and customize it,
according to their specific needs.

#### What

Focus on creating a single customizable slide template.

-- -- --

### Basic Page

> Getting Started with Inkscape Page

These steps apply to most of the Inkscape documents I create.

Many Inkscape users use drag and drop,
and often skip specifying placement or width,
among other details.

I use placement and width for tutorial purposes
so anyone can replicate the slide design with precision.
You don't need to adhere to any specific working style,
to create your own design.

#### Document Properties

> Page Setup

Although example provided, consider make a new document from scratch.

* I use the size of 1600px * 900px, so both display unit should be in px.
* The checkerboard background to differ from the white background.
* The scale should be 1 or other natural number, so you can arrange page in XML manually.

![Inkscape: Document Properties][001-doc-properties]

The scale is a crucial factor to consider
when manually arranging the page's positioning.
If you find that your previous document has a different scale,
you can easily adjust it by modifying the document properties,
and resizing the vector image.

This adjustment can be accomplished
using a straightforward technique.
However, I'll explain it in another article
to keep our focus intact.

#### Color Pallete

Choose Material Design pallete for this template.

![Inkscape: Initial Looks][002-material-design]

#### Initial Looks

Your screen should resemble the image below::

![Inkscape: Initial Looks][003-initial-looks]

#### Background Layer

We need a white background layer.
Start by making  a 1600px * 900px rectangle,
filled with solid white color.

* X: 0; Y: 0; W: 1600; H: 900

![Inkscape: Rectangle Size][005-rectangle-size]

Be cautious of rounded corners;
ensure that both Rx and Ry are set to zero.

![Inkscape: Reunded Corner][005-rounded-corner]

Feel free to name the rectangle, such as `Main BG` or simply `White`.
I intentionally use white background,
so I can print the slide in A4 paper,
without using ink at all.

![Inkscape: Background Layer][004-background-layer]

Your screen should now resemble the image above.

Don't forget to save your work;
we are now prepared to move on to the next step.

#### SVG Source

You can download the SVG source from the link below:

* [SVG Source: Example: Step One][example-step-01]

-- -- --

### Template Layer

#### Source Layer

Consider creating a new layer for our default template,
and within it, establish a a source sublayer.

Lock the background layer
so that we can freely work on the source layer.

#### Two Vertical Lines

Now, proceed to create a left line with a width of 40px.

* X: 0; Y: 0; W: 40; H: 900; Color: Blue700

Next, create a right line with a width of 30px.

* X: 1570; Y: 0; W: 30; H: 900; Color: Blue500

Your Inkscape window should resemble the image below:

![Inkscape: Two Vertical Lines][006-two-vert-lines]

Again, I intentionally use bright color,
so I can print the slide in A4 paper.
without having to worry about using too much ink.

For better organization, you can also name the objects as shown in the layer pane.

#### Four Additional Lines

To separate the two lines mentioned earlier,
we need to add four more lines as separators.

Now, create four additional lines with a height of 500px.
The widths should be 40px and 80px.
While the exact position doesn't matter,
I'll provide the details anyway
to make it easier for beginners.

* X: 100; Y: 0; W: 40; H: 500; Color: Blue900
* X: 200; Y: 400; W: 80; H: 500; Color: Blue300
* X: 1400; Y: 0; W: 80; H: 500; Color: Blue300
* X: 1500; Y: 400; W: 40; H: 500; Color: Blue300

![Inkscape: Four More Lines][007-four-more-lines]

The color choice is flexible;
I'm including it for the sake of aesthetics in this example.

#### Diagonal Lines

We need to rotate the lines 45 degrees counterclockwise or,
in numerical terms, -45 degrees to the right.

![Inkscape: Rotate 45 Degrees][008-rotate-45degrees]

Rotate all four lines mentioned earlier,
ensuring they are properly placed as follows:

* X: -100; Y: 0
* X: -100; Y: 500
* X: 1400; Y: 100
* X: 1400; Y: 600

This is not an overly complex rocket science,
so you can use alternative placements,
as needed to fit your design.

![Inkscape: Diagonal Lines][009-diagonal-lines]

Your Inkscape window should resemble the image above.

Your Inkscape window should resemble the image above.

#### SVG Source

You can download the SVG source from the link below:

* [SVG Source: Example: Step Two][example-step-02]

-- -- --

### Creating Shapes

Starting from a draft slide,
we can now proceed to create the master slide.

With the structural elements in place,
it's time to cut the lines.
Extract the two cut lines into six separate lines.
Lastly, group the six lines together.

#### Cutting the Lines

Duplicate the layer into new layer.
And name it `Source: Cut`.
Lock and hide the original layer,
to keep a backup of your original line structure.

Now, select the two lines that you wish to cut,
and choose `Path Difference`` from the menu.

![Inkscape: Path Difference Menu][011-cut-difference]

You should observe the following result:

![Inkscape: Cut Lines Result][012-cut-lines-result]

Note: The process isn't complete yet, so please continue with the next steps.

#### Extract and group.

Utilize 'Path Break Apart' to extract both lines.
Once again, duplicate the layer and name it `Blue`.
Lock and hide the original layer
to retain your initial line structure.

Select all six paths in the 'Blue' layer, 
and group them as a single entity.

Modify the colors using the Material palette,
starting from the bottom-left
and proceeding from left to right.

* Left Bottom: Blue900
* Left Middle: Blue700
* Left Top: Blue500
* Right Bottom: Blue500
* Right Middle: Blue300
* Right Top: Blue100

![Inkscape: Group in Blue][013-group-in-blue]

Feel free to add more colors like green and red.
To change colors, activate the layer with the specific color
and deactivate layers with unwanted colors.

Remember to save your work,
and we're now ready to proceed to the next step.

#### SVG Source

You can download the SVG source from the link below:

* [SVG Source: Example: Step Three][example-step-03]

-- -- --

### Managing Colors

Now that we have a collection of templates,
let's differentiate them by assigning,
a specific color to each line using Materialize colors.

For example, you can create a green slide template like this:

![Inkscape: Green Slide][014-group-in-green]

And another one with a red slide template like this:

![Inkscape: Red Slide][015-group-in-red]

Once you've customized the colors for your templates,
you can proceed to export the slides as PNG images.

![Inkscape: Export: Red Slide][016-cc-step-04-red]

With this step, we've completed the default template,
one of five available templates.

#### SVG Source

You can download the SVG source from the link below:

* [SVG Source: Example: Step Four][example-step-04]

-- -- --

### What Comes Next 🤔?

We're about to embark on a journey to explore other templates,
each boasting a palette of 19+1 colors.
They'll be meticulously organized with a similar sublayer structure, providing you with even more creative options.

Consider continue reading [ [Inkscape: Slide Templates - Part Three][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}design/2022/09/05/inkscape-slide-templates-03/

[example-step-01]:  https://github.com/epsi-rns/candyclone-inkscape/blob/main/steps/cc-step-01.svg
[example-step-02]:  https://github.com/epsi-rns/candyclone-inkscape/blob/main/steps/cc-step-02.svg
[example-step-03]:  https://github.com/epsi-rns/candyclone-inkscape/blob/main/steps/cc-step-03.svg
[example-step-04]:  https://github.com/epsi-rns/candyclone-inkscape/blob/main/steps/cc-step-04.svg

[//]: <> ( -- -- -- links below -- -- -- )

[candyclone-preview]:   {{< baseurl >}}assets/posts/design/2022/09/isometric/isometric-withgap-transparent.png

[001-doc-properties]:   {{< baseurl >}}assets/posts/design/2022/09/001-doc-properties.png
[002-material-design]:  {{< baseurl >}}assets/posts/design/2022/09/002-material-design.png
[003-initial-looks]:    {{< baseurl >}}assets/posts/design/2022/09/003-initial-looks.png
[004-background-layer]: {{< baseurl >}}assets/posts/design/2022/09/004-background-layer.png
[005-rectangle-size]:   {{< baseurl >}}assets/posts/design/2022/09/005-rectangle-size.png
[005-rounded-corner]:   {{< baseurl >}}assets/posts/design/2022/09/005-rounded-corner.png
[006-two-vert-lines]:   {{< baseurl >}}assets/posts/design/2022/09/006-two-vert-lines.png
[007-four-more-lines]:  {{< baseurl >}}assets/posts/design/2022/09/007-four-more-lines.png
[008-rotate-45degrees]: {{< baseurl >}}assets/posts/design/2022/09/008-rotate-45degrees.png
[009-diagonal-lines]:   {{< baseurl >}}assets/posts/design/2022/09/009-diagonal-lines.png
[011-cut-difference]:   {{< baseurl >}}assets/posts/design/2022/09/011-cut-difference.png

[012-cut-lines-result]: {{< baseurl >}}assets/posts/design/2022/09/012-cut-lines-result.png
[013-group-in-blue]:    {{< baseurl >}}assets/posts/design/2022/09/013-group-in-blue.png
[014-group-in-green]:   {{< baseurl >}}assets/posts/design/2022/09/014-group-in-green.png
[015-group-in-red]:     {{< baseurl >}}assets/posts/design/2022/09/015-group-in-red.png
[016-cc-step-04-red]:   {{< baseurl >}}assets/posts/design/2022/09/016-cc-step-04-red.png
