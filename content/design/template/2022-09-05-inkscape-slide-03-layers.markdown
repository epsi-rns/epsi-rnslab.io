---
type   : post
title  : "Inkscape: Slides - Layers"
date   : 2022-09-05T04:29:15+07:00
slug   : inkscape-slide-templates-03
categories: [design]
tags      : [inkscape]
keywords  : [master slides, template, candyclone, layers]
author : epsi
opengraph:
  image: assets/posts/design/2022/09/isometric/candyclone-iso-02.png

toc    : "toc-2020-inkscape-template"

excerpt:
  Managing Inkscape master-slide templates,
  using the new multi-page feature.

---

### Preface

> Managing Inkscape master-slide templates,
> using the new multi-page feature.

Starting from a simple draft slide,
we are poised to craft a master slide.
Our approach involves utilizing multiple layers,
with each slide residing in its own dedicated layer.

To streamline our workflow and enhance organization,
each slide will have its own designated page.

This allows us to begin by establishing page placements,
before diving into the creation of additional templates.

#### Why

Standardized layer structures to facilitate
seamless document exchange within a team.

#### What

Discussing how to handle multiple master-slide templates.

-- -- --

### Page Placement

#### The Geometry of Placement

> Understanding Page Geometry

To maintain organized and structured pages,
we'll name each page based on its XY position,
as follows:

* page-01.01: first x-column,  first y-row.
* page-01.02: first x-column,  second y-row.
* page-02.03: second x-column, third y-row.
* page-03.03: third x-column,  third y-row.

Considering that each slide has,
a uniform size of 1600px * 900px,
and accounting for the gap between pages,
the precise pixel placements are as follows:

* page-01.01: X=0, Y=0
* page-01.02: X=0, Y=1000
* page-02.03: X=1800, Y=2000
* page-03.03: X=3600, Y=2000

#### Accessing the XML Editor Pane

When you create a new Inkscape document,
it automatically provides you with a default front page.
However, if you access the XML editor pane,
you will notice that the `namedview` section,
displays something similar to the following:

![Inkscape: XML Pane: No Page][021-xml-nopage]

#### Adding a New Page

To add a new page in Inkscape,
you can use the page toolbar.
Simply click on the plus icon as shown here:

![Inkscape: Toolbar: Add Page][022-add-page]

After adding the new page,
the canvas will display as follows:

![Inkscape: Canvas: New Page][023-canvas-new-page]

In the XML editor pane,
under the `namedview`` section,
you will now see two child nodes:

![Inkscape: XML Pane: New Page][024-xml-new-page]

These nodes are named as `page1` and `page2``.
The detailed values of newly created `page2`
in pixels are as follows:

* X: 0
* Y: 1610

#### Moving Page

Let's proceed to move the newly created page,
below the front page, as illustrated below:

![Inkscape: Canvas: New Page Placement][025-canvas-new-page]

We'll need to rename the pages as follows:

* Page-01.01
* Page-01.02

For the `page-01.02`` node,
we'll adjust the placement in pixels to:

* X: 1000
* Y: 0

![Inkscape: XML Pane: New Page Placement][026-xml-new-page]

The page size in pixels will remain unchanged:

* W: 1600
* H: 900

-- -- --

### Arranging All Pages

Do it again,
repeat the same process,
for the remaining five pages.

Managing page placement becomes more straightforward,
when we use consistent page names,
that accurately reflect their respective placements.

#### Managing Page Names and Placement

You can use any namespace to manage the page names,
such as the example below:

* Page-01.01
* Page-01.02
* Page-01.03
* Page-01.04
* Page-01.05

For the `page-01.05` node,
set the positioning in pixels to:

* X: 4000
* Y: 0

![Inkscape: Canvas and XML Pane: All Pages Positioning][027-xml-five-page]

#### Adding Page Labels

To give each page its unique identity,
you can choose to add labels either through the toolbar or XML
whichever method suits you best.
These labels will be displayed in the PDF file.

For our first page, please name it as follows:

* `Default: 40:30`

![Inkscape: Toolbar: Title][028-toolbar-title]

For the remaining pages, assign labels as indicated below:

* `Default: 40:30`
* `Default: 40:40`
* `Single: Chapter`
* `Image: Title`
* `Alternate: Background`

![Inkscape: Canvas and XML Pane: All Pages Label][029-page-label]

Remember to save your work.
We have now completed the page placement,
and can shift our focus to working on the template itself,
along with the respective layer.

#### SVG Source

You can download SVG source from below link:

* [SVG Source: Example: Step Five][example-step-05]

-- -- --

### Managing Layers and Pages

Instead of starting a new slide from scratch,
we can efficiently copy our layer onto a new page.

Consider begin with a straightforward example.

#### The Original Layer

We'll start by making a slight modification to our first slide,
placing all elements as sub-layers,
under a primary layer named `Default: 40:30``.

![Inkscape: Base Layer][031-base-layer]

#### The Copy Layer

Duplicate the slide, erase all sublayer,
except the basic one,
and rename the layer to  `Default: 40: 40`.

Next, let's make a minor adjustment,
by resizing the right line to a width of 40 pixels.

Select the newly created `Default: 40:40` layer,
and change the `Y` position to 1000 pixels.
This will shift the layer to the right onto the second page,
as illustrated below:

![Inkscape: Duplicate and Move][032-duplicate-move]

#### Creating the Slide

Next, modify the width of the right line,
from 30 pixels to 40 pixels.
Adjust the X position by -10 pixels to 1560 pixels.
For clarity, rename the rectangle to `Right: 40``.
This step is crucial for creating this second master slide.

![Inkscape: Resize Line][033-40px-width]

Now, replicate the process from our previous article,
until we complete the master slide.

![Inkscape: Two Master Slides][034-two-master-slides]

We have now crafted two distinct master slides,
each residing in its dedicated layer and page.

This arrangement allows us to,
swiftly copy and paste a master slide,
into another Inkscape document by merely selecting the layer

With a little effort,
we've established an efficient workflow.

#### SVG Source

You can download SVG source from below link:

* [SVG Source: Example: Step Six][example-step-06]

-- -- --

### Creating Additional Master Slides

We have three more master slides to create.

#### Master Slides: Chapter Break

Consider practice by crafting another slide,
this time for chapter breaks.
As usual, we begin with vertical lines,
and you can find the detailed SVG source for reference.

![Inkscape: Vertlines][035-chapter-vertlines]

Next, we rotate these lines by 45 degrees,
to form diagonal shapes.

![Inkscape: Line][036-chapter-line]

Now, we proceed with cutting...
Cut.. cut... cut...

![Inkscape: Cut][037-chapter-cut]

And there we have it.
A brand new master slide!

![Inkscape: Chapter Break][038-cc-chapter-break]

It's done.

#### Master Slides: Image Title

Creating a slide for images with titles,
follows a similar process to the previous ones.
As usual, we begin with vertical lines,
but this time we have a handy helper.
You can find detailed SVG source information for reference.

![Inkscape: Vertlines][041-image-vertlines]

Next, we rotate these lines by 45 degrees to form diagonal shapes.

![Inkscape: Line][042-image-line]

Once more, we proceed with cutting...
Again.. Cut.. cut... cut...

![Inkscape: Cut][043-image-cut]

And once again,
we have ourselves a brand new master slide!

![Inkscape: Image Title][044-cc-image-title]

Another one completed.

#### Master Slides: Alternate Background

Creating an alternate background master slide is,
essentially the same as the original slide,
with the only difference being the color arrangement.

![Inkscape: Alternate Background][045-alt-bg-blue]

In this version, the background uses Blue500,
where other lines requiring different colors:

* Background: Blue500
* Left Bottom: Blue900
* Left Middle: Blue700
* Left Top: Blue400
* Right Bottom: Blue600
* Right Middle: Blue300
* Right Top: Blue100

I must admit, I rarely use this particular master-slide.

#### All Pages, All Layers

Our comprehensive work will look like this:

![Inkscape: All Pages, All Layers][046-all-pages-layers]

#### SVG Source

You can download the SVG source from the link provided below:

* [SVG Source: Example: Step Seven][example-step-07]

-- -- --

### Comprehensive Color Palette

> Organized as sub-layers under a single master layer.

Alternatively, you can explore my original creations here:

* [SVG Source: Original Layers][original-layers]

This SVG source comprises a rich palette of 19 material colors,
along with one experimental gradient.

To change the color scheme,
simply activate the layer with the desired color,
and in the same time deactivate layers with unwanted colors.

![Inkscape: Original Slides][047-original-slides]

We'll be utilizing this comprehensive color palette,
in the upcoming chapter.

-- -- --

### Save a Copy to PDF

One of the standout features in Inkscape 1.2
that I absolutely adore is
the ability to save pages directly to PDF.

![Inkscape: Menu: Save a Copy][048-candy-save-a-copy]

With the menu we can afford the result as below figure.

![Inkscape: PDF View of Basic Candyclone][049-candy-template]

This means I can streamline my entire workflow,
from creating the artwork to delivering the presentation,
using just Inkscape.
This eliminates the need for constant copy-pasting
between various applications,
and the best part is that,
it doesn't embed anything except photographic images.

What's even more impressive is,
the option to export multiple PDF or PNG images,
in a single batch.
This makes Inkscape an incredibly efficient tool,
for daily office tasks and workflows.

-- -- --

### What Comes Next 🤔?

Our next step involves learning how to,
manage each slide variant on its dedicated page.

Consider continue reading [ [Inkscape: Slide Templates - Part Four][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}design/2022/09/07/inkscape-slide-templates-04/

[example-step-05]:  https://github.com/epsi-rns/candyclone-inkscape/blob/main/steps/cc-step-05.svg
[example-step-06]:  https://github.com/epsi-rns/candyclone-inkscape/blob/main/steps/cc-step-06.svg
[example-step-07]:  https://github.com/epsi-rns/candyclone-inkscape/blob/main/steps/cc-step-07.svg
[original-layers]:  https://github.com/epsi-rns/candyclone-inkscape/blob/main/template-candyclone-multilayers.svg

[//]: <> ( -- -- -- links below -- -- -- )

[021-xml-nopage]:       {{< baseurl >}}assets/posts/design/2022/09/021-xml-nopage.png
[022-add-page]:         {{< baseurl >}}assets/posts/design/2022/09/022-add-page.png
[023-canvas-new-page]:  {{< baseurl >}}assets/posts/design/2022/09/023-canvas-new-page.png
[024-xml-new-page]:     {{< baseurl >}}assets/posts/design/2022/09/024-xml-new-page.png
[025-canvas-new-page]:  {{< baseurl >}}assets/posts/design/2022/09/025-canvas-new-page.png
[026-xml-new-page]:     {{< baseurl >}}assets/posts/design/2022/09/026-xml-new-page.png
[027-xml-five-page]:    {{< baseurl >}}assets/posts/design/2022/09/027-xml-five-page.png
[028-toolbar-title]:    {{< baseurl >}}assets/posts/design/2022/09/028-toolbar-title.png
[029-page-label]:       {{< baseurl >}}assets/posts/design/2022/09/029-page-label.png

[031-base-layer]:       {{< baseurl >}}assets/posts/design/2022/09/031-base-layer.png
[032-duplicate-move]:   {{< baseurl >}}assets/posts/design/2022/09/032-duplicate-move.png
[033-40px-width]:       {{< baseurl >}}assets/posts/design/2022/09/033-40px-width.png
[034-two-master-slides]:{{< baseurl >}}assets/posts/design/2022/09/034-two-master-slides.png

[035-chapter-vertlines]:{{< baseurl >}}assets/posts/design/2022/09/035-chapter-vertlines.png
[036-chapter-line]:     {{< baseurl >}}assets/posts/design/2022/09/036-chapter-line.png
[037-chapter-cut]:      {{< baseurl >}}assets/posts/design/2022/09/037-chapter-cut.png
[038-cc-chapter-break]: {{< baseurl >}}assets/posts/design/2022/09/038-cc-chapter-break.png

[041-image-vertlines]:  {{< baseurl >}}assets/posts/design/2022/09/041-image-vertlines.png
[042-image-line]:       {{< baseurl >}}assets/posts/design/2022/09/042-image-line.png
[043-image-cut]:        {{< baseurl >}}assets/posts/design/2022/09/043-image-cut.png
[044-cc-image-title]:   {{< baseurl >}}assets/posts/design/2022/09/044-cc-image-title.png
[045-alt-bg-blue]:      {{< baseurl >}}assets/posts/design/2022/09/045-alt-bg-blue.png

[046-all-pages-layers]: {{< baseurl >}}assets/posts/design/2022/09/046-all-pages-layers.png
[047-original-slides]:  {{< baseurl >}}assets/posts/design/2022/09/047-original-slides.png
[048-candy-save-a-copy]:{{< baseurl >}}assets/posts/design/2022/09/048-candy-save-a-copy.png
[049-candy-template]:   {{< baseurl >}}assets/posts/design/2022/09/049-candy-template.png