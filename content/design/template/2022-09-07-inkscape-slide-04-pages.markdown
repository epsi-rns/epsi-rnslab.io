---
type   : post
title  : "Inkscape: Slides - Pages"
date   : 2022-09-07T04:29:15+07:00
slug   : inkscape-slide-templates-04
categories: [design]
tags      : [inkscape]
keywords  : [master slides, template, candyclone, pages]
author : epsi
opengraph:
  image: assets/posts/design/2022/09/isometric/candyclone-iso-03.png

toc    : "toc-2020-inkscape-template"

excerpt:
  Organizing each color of Inkscape master-slide templates,
  using the new multi-page feature.

---

### Preface

> Organizing each color of Inkscape master-slide templates,
> using the new multi-page feature.

Creating color variants is a breeze with Google Material Colors.
Take, for example, this red variant inspired by our original blue slide:

![Inkscape: Thumbnail: Template: Red][template-07-red]

However, there's a key consideration:
we must meticulously organize each slide within its dedicated page.
This approach allows us to instantly visualize the result,
without toggling layer visibility.

Once more, we embark on organizing page placements,
and separating our previous layer,
into its distinct variant colors.

#### Why

Enhances workflow efficiency by providing a clear view
of each slide on individual pages, allowing for easy selection.

#### What

Highlighting the use of multi-pages in master slides.

-- -- --

### Page Placement

#### Organizing the Layout Geometry

Our approach to page naming involves,
assigning each page a unique XY position,
as illustrated in the table below:

| Red        | Green      | Blue       |
|------------|------------|------------|
| Page-01.01 | Page-02.01 | Page-03.01 |
| Page-01.02 | Page-02.02 | Page-03.02 |
| Page-01.03 | Page-02.03 | Page-03.03 |
| Page-01.04 | Page-02.04 | Page-03.04 |
| Page-01.05 | Page-02.05 | Page-03.05 |

This XY positioning allows us to precisely align each page placement.
For instance, considering a uniform slide size of 1600px * 900px:

* page-03.05: X=3600, Y=4000

With this meticulously organized arrangement,
we can readily observe the results as shown below:

![Inkscape: Page Layout: 3x5][051-page-layout]

#### Restructuring Layers

I aim to cultivate a unique experience,
by introducing creative diversity through distinct layers.
This approach enables me to craft layers,
that align more closely with my preferences.

I've also chosen to maintain consistency,
by naming the second and third column,
with the same titles as the first column.

| Red Column     | Green Column   | Blue Column    |
|----------------|----------------|----------------|
| Default 40: 30 | Default 40: 30 | Default 40: 30 |
| Default 40: 40 | Default 40: 40 | Default 40: 40 |
| Chapter: Break | Chapter: Break | Chapter: Break |
| Image: Title   | Image: Title   | Image: Title   |
| Alternate      | Alternate      | Alternate      |

![Inkscape: Restructure Layer][052-layer-structure]

#### Duplicate Layers

To create color variants,
we'll start by duplicating the first column `01-Red` layers,
and rename them as `02-Green`, `03-Blue`, and `00-Source`.

Next, we'll use the Transform tools,
to reposition each layer horizontally:

* `02-Green`:  Move 1800px to the right.
* `03-Blue`:   Move 1800px to the right, twice. 
* `01-Red`:    Stay in its original position.
* `00-Source`: Shift by -2700px.

Following this,
we'll remove any unnecessary sub-layers,
retaining only the color sub-layer and the background,
for each slide color.

The expected outcome should resemble the figure below:

![Inkscape: Duplicate Layer][053-layer-duplicate]

I always maintain the source,
ensuring the flexibility to make changes with ease.

#### Access the SVG Source

You can download the SVG source from the link provided below:

* [SVG Source: Example: Step Eight][example-step-08]

-- -- --

#### The Full Collection of Slides

In crafting unique page arrangements using XML, consider this scenario:
a layout featuring 5 rows of master slides and 20 color columns,
would result in an exceptionally long horizontal column.
To enhance clarity and aesthetics,
I opted for a different approach
where each column accommodates two distinct colors,
separated by a gap.

This adjustment necessitates manual XY positioning in XML,
disregarding our previous page naming conventions.
For instance, `page-02.07`` for the second color (grey),
which contains a layer named `Default: 40:30``,
can be assigned X = 0px and Y = 6000px.

As a result as depicted below,
the document now comprises 11 rows and 10 columns.

![Inkscape: Complete Slides][054-complete-slides]

Pretty cool right?

#### Layers and Pages: A Visual Connection

The intricate connection between layers and pages is,
illustrated in the image below:

![Inkscape: Layers and Pages][055-layer-pages]

> Patience is your companion.

While the concept may seem straightforward,
in the real world, it demands meticulous craftsmanship.
Certain endeavors may consume
a significant amount of time and effort,
but the results are well worth the investment.

#### Just an example

> Nothing particularly extraordinary

While the functionality of the page
and layer features is essential,
aesthetics play a crucial role.

I've attempted to add an aesthetic touch
to these master slide templates,
but I acknowledge my limited artistic sense.
I'm confident that you, as a creative community,
can elevate these master slides
beyond what I've achieved.

-- -- --

### A Brief Preview

> Quick look

I've included a quick-look version
to simplify your selection processs o you can pick directly,
allowing you to make choices without the need to open the vector source.

#### Blue

![Inkscape: Template Thumbnails: 01 - Blue][template-01-blue]

#### Grey

![Inkscape: Template Thumbnails: 02 - Grey][template-02-grey]

#### Brown

![Inkscape: Template Thumbnails: 03 - Brown][template-03-brown]

#### Blue Grey

![Inkscape: Template Thumbnails: 04 - Blue Grey][template-04-blue-grey]

#### Yellow

![Inkscape: Template Thumbnails: 05 - Yellow][template-05-yellow]

#### Teal

![Inkscape: Template Thumbnails: 06 - Teal][template-06-teal]

#### Red

![Inkscape: Template Thumbnails: 07 - Red][template-07-red]

#### Purple

![Inkscape: Template Thumbnails: 08 - Purple][template-08-purple]

#### Pink

![Inkscape: Template Thumbnails: 09 - Pink][template-09-pink]

#### Orange

![Inkscape: Template Thumbnails: 10 - Orange][template-10-orange]

#### Lime

![Inkscape: Template Thumbnails: 11 - Lime][template-11-lime]

#### Light Green

![Inkscape: Template Thumbnails: 12 - Light Green][template-12-light-green]

#### Light Blue

![Inkscape: Template Thumbnails: 13 - Light Blue][template-13-light-blue]

#### Indigo

![Inkscape: Template Thumbnails: 14 - Indigo][template-14-indigo]

#### Green

![Inkscape: Template Thumbnails: 15 - Green][template-15-green]

#### Deep Purple

![Inkscape: Template Thumbnails: 16 - Deep Purple][template-16-deep-purple]

#### Deep Orange

![Inkscape: Template Thumbnails: 17 - Dep Orange][template-17-deep-orange]

#### Cyan

![Inkscape: Template Thumbnails: 18 - Cyan][template-18-cyan]

#### Amber

![Inkscape: Template Thumbnails: 19 - Amber][template-19-amber]

#### Multiple Gradient

> Experimental

![Inkscape: Template Thumbnails: 20 - Multiple Gradient][template-20-multi-grad]

And with that, we've reached the end.
As a public domain resource,
you are free to utilize it without restrictions.

#### SVG Source

Feel free to download the complete master slides' SVG source,
from the link provided below:

* [SVG Source: Original Pages][original-pages]

Have a fantastic day! 👏👏👏

-- -- --

### What Comes Next 🤔?

What's the use of a master slide template,
without a corresponding ready-to-use content template?
I've already crafted content templates,
and made them available as public domain resources.
Take a look; you might find one,
that perfectly suits your presentation project.

Feel free to explore further by continuing to read [ [Inkscape: Slide Templates - Part Five][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}design/2022/09/09/inkscape-slide-templates-05/

[example-step-08]:  https://github.com/epsi-rns/candyclone-inkscape/blob/main/steps/cc-step-08.svg
[original-pages]:   https://github.com/epsi-rns/candyclone-inkscape/blob/main/template-candyclone-multipages.svg

[//]: <> ( -- -- -- links below -- -- -- )

[template-01-blue]:     {{< baseurl >}}assets/posts/design/2022/09/thumbs-template/01-blue.png
[template-02-grey]:     {{< baseurl >}}assets/posts/design/2022/09/thumbs-template/02-grey.png
[template-03-brown]:    {{< baseurl >}}assets/posts/design/2022/09/thumbs-template/03-brown.png
[template-04-blue-grey]:{{< baseurl >}}assets/posts/design/2022/09/thumbs-template/04-blue-grey.png
[template-05-yellow]:   {{< baseurl >}}assets/posts/design/2022/09/thumbs-template/05-yellow.png
[template-06-teal]:     {{< baseurl >}}assets/posts/design/2022/09/thumbs-template/06-teal.png
[template-07-red]:      {{< baseurl >}}assets/posts/design/2022/09/thumbs-template/07-red.png
[template-08-purple]:   {{< baseurl >}}assets/posts/design/2022/09/thumbs-template/08-purple.png
[template-09-pink]:     {{< baseurl >}}assets/posts/design/2022/09/thumbs-template/09-pink.png
[template-10-orange]:   {{< baseurl >}}assets/posts/design/2022/09/thumbs-template/10-orange.png
[template-11-lime]:     {{< baseurl >}}assets/posts/design/2022/09/thumbs-template/11-lime.png
[template-12-light-green]:  {{< baseurl >}}assets/posts/design/2022/09/thumbs-template/12-light-green.png
[template-13-light-blue]:   {{< baseurl >}}assets/posts/design/2022/09/thumbs-template/13-light-blue.png
[template-14-indigo]:   {{< baseurl >}}assets/posts/design/2022/09/thumbs-template/14-indigo.png
[template-15-green]:    {{< baseurl >}}assets/posts/design/2022/09/thumbs-template/15-green.png
[template-16-deep-purple]:  {{< baseurl >}}assets/posts/design/2022/09/thumbs-template/16-deep-purple.png
[template-17-deep-orange]:  {{< baseurl >}}assets/posts/design/2022/09/thumbs-template/17-deep-orange.png
[template-18-cyan]:     {{< baseurl >}}assets/posts/design/2022/09/thumbs-template/18-cyan.png
[template-19-amber]:    {{< baseurl >}}assets/posts/design/2022/09/thumbs-template/19-amber.png
[template-20-multi-grad]:   {{< baseurl >}}assets/posts/design/2022/09/thumbs-template/20-multi-gradient.png

[//]: <> ( -- -- -- links below -- -- -- )

[051-page-layout]:      {{< baseurl >}}assets/posts/design/2022/09/051-page-layout.png
[052-layer-structure]:  {{< baseurl >}}assets/posts/design/2022/09/052-layer-structure.png
[053-layer-duplicate]:  {{< baseurl >}}assets/posts/design/2022/09/053-layer-duplicate.png
[054-complete-slides]:  {{< baseurl >}}assets/posts/design/2022/09/054-complete-slides.png
[055-layer-pages]:      {{< baseurl >}}assets/posts/design/2022/09/055-layer-pages.png
