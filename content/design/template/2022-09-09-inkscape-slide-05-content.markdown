---
type   : post
title  : "Inkscape: Slides - Content"
date   : 2022-09-09T04:29:15+07:00
slug   : inkscape-slide-templates-05
categories: [design]
tags      : [inkscape]
keywords  : [master slides, template, candyclone, content]
author : epsi
opengraph:
  image: assets/posts/design/2022/09/isometric/candyclone-iso-04.png

toc    : "toc-2020-inkscape-template"

excerpt:
  Versatile multi purpose Inkscape example-slide templates,
  arranged as multi-page presentations.

---

### Preface

> Versatile multi purpose Inkscape example-slide templates,
> arranged as multi-page presentations.

What good is a master slide template,
without a readily usable content template?
I've already created a content template,
and made it available in the public domain.
Take a look; it might be suitable
for your documentation project.

![Inkscape: Thumbnail: Example Content][content-06-flow-and]

Alternatively, you can access the provided vector source image,
and creatively craft your own sample template content.

#### Why

Ready-to-use templates enable users
to focus on content creation rather than design.

#### What

Emphasizing the versatility of example-slide templates.

-- -- --

### A Big Document

> Presenting a document is a valuable life skill,
> closely intertwined with public speaking.

This vector source is a big document
featuring a staggering 89 distinct slides,
all arranged into 16 columns,
that would barely fit on a widescreen display.
Within the Inkscape user interface,
this expansive document appears as shown below:

![Inkscape: 16 Columns of big document][061-all-content-long]

When exported as a PDF,
the document presents itself in the following format:

![Evince: 16 Columns of big document][061-all-in-pdf]

While it's not feasible to delve into each individual slide,
I encourage you to zoom in slightly,
within the Inkscape user interface,
to glean a general understanding,
of the work's overarching concept.

#### Part One

![Inkscape: Thumbnail: First Slice of Big][062-slice-01-shorter]

#### Part Two

![Inkscape: Thumbnail: Second Slice of Big][062-slice-02-shorter]

#### Part Three

![Inkscape: Thumbnail: Third Slice of Big][062-slice-03-shorter]

By now you can guess the main idea of the work result.

#### Layers and Pages

The intricate connection between layers and pages is,
vividly depicted in the image below.
As I frequently employ this template,
I often make subtle adjustments to the layer structure,
to align with the specific slide project at hand.

![Inkscape: Layers and Pages][063-layers-pages]

Currently, I find myself without a specific topic,
to delve into regarding this aspect.
I simply do not have any idea what to explain here.

I suggest that you simply explore,
the provided SVG source link below,
and immerse yourself in working with the example content.

-- -- --

### Inkscape: Source of Decorative Artworks

> The Birth of Original Decorations

These artworks didn't materialize overnight;
they evolved over years of dedicated effort.
Then it struck me!
Why not share them in a place accessible to anyone in need?

#### Bullets

* [Inkscape: Progress Ring Animation][progress-ring]

#### Isometric Wallpapers

* [Isometric Wallpaper][isometric-wall]

#### Business Cards

* [Inkscape - Kartu Nama][kartu-nama]

#### Cubes

* [Inkscape: Isometric Infographics][isometric-cubes]

-- -- --

### A Sneak Peek

> A Quick Preview

I've prepared a quick look version for your convenience,
allowing you to make direct selections,
without the need to open the vector source.
Detailed information will follow in the next article.

#### Preface

You might think it's a bit mundane,
but I firmly believe that every slide should begin,
with a captivating cover.

In this section, I'll also include:

* About the author: Insights about the author
* About the slide: An introduction to the slide's purpose
* Template Specification
* Availability of the SVG source as public domain
* Links to related tutorials
* Credits to Hervy: Acknowledgments and credits
* Contact information for the author

![Inkscape: Thumbnail: Preface][content-01-preface]

#### Master Slides

> A Treasure Trove

I diligently maintain the master slides, including their titles,
with each color meticulously organized into sublayers.

This practice grants me the flexibility,
to effortlessly copy and paste any slide I require,
into my own presentation projects.

* Default: 40: 30
* Default: 40: 40
* Single Page: Chapter Break
* Image Page: Title
* Alternate Page: Background

Additionally, I've included examples of:

* Preface
* Reference

![Inkscape: Thumbnail: Master Slides][content-02-master]

#### Boxes, Charts and Figures

> A Timeless Collection

This section holds a special place in my Impress presentation,
serving as both the oldest segment
and the most recent addition to this Inkscape template.
The reason for its inclusion is simple:
most presentations include these elements,
so I decided to craft an Inkscape version.

* Boxes: While designing boxes might seem like
  a common and straightforward task,
  I don't underestimate the challenge of,
  arranging elements within such confined spaces.

* Charts: Every chart you encounter here is,
  a genuine Inkscape vector creation,
  a testament to the precision and detail of my work.
  The only exception is the LibreOffice chart,
  which is presented as a PNG image,
  instead of original vector image

* Figures: Within this category,
  you'll find a collection of pages,
  that I've meticulously crafted,
  each offering a unique visual perspective.

![Inkscape: Thumbnail: Box, Charts and Figures][content-03-box-chart]

#### Tables and Forms

> A Personal Mission

Tables and forms represent a personal journey,
born out of my time working in the accounting division with spreadsheets.

The spark for this venture ignited
when I came across a statistics book with an exceptionally appealing design.
I told myself that I should strive to recreate such aesthetics one day.
And so, I ventured into creating Excel and Calc tables,
that often needed to be shared into social media such as whatsapp,
as a quick report preview with bosses and clients alike.

Then, one fateful day, I found myself compelled tom
rewrite a spreadsheet table entirely in pure Inkscape.
That experience led me to include it here as a template.

![Inkscape: Thumbnail: Table and Form][content-04-table-and]

The guiding principle for crafting a bright template is quite simple:
embrace the brightest gradient scale possible.
For instance, opt for `Blue50` instead of `Blue700`,
or `Teal100` instead of `Teal500`.

#### Custom Numbering Bullets

> Conveying Clarity

The essence of crafting a compelling slide,
lies in conveying thoughts distinctly through individual points.
Whether it's a summary or intricate details,
the importance of aesthetically pleasing bullets,
cannot be overstated.

_Good looking bullet is a must._

![Inkscape: Thumbnail: Bullets][content-05-bullet]

We've got you covered with the SVG source provided!
Every bullet has been meticulously,
imported into Inkscape as SVG vectors,
allowing you to tailor both their appearance,
and content to your exact preferences.

#### Flow and Milestones

> Illustrating Progress

In the various roles I've undertaken,
there has always been a need,
to elucidate progress or processes.

This was particularly true during my time as a trainer,
where I had to succinctly convey the syllabus,
outlining what the training entailed,
all within the confines of a single page.

![Inkscape: Thumbnail: Flow and Milestones][content-06-flow-and]

We've made sure to provide you with the SVG source!
The artwork is readily available,
and entirely at your disposal,
for both use and customization.

#### Bolt and Discs

> Beyond Bullets

Sometimes, the aim is to break free,
from the monotony of bullet points on slides.
That's precisely where the concept of bolts and discs comes into play.
Why settle for mundane when you can infuse,
a touch of industrial artistry into your presentations?

This is my original creation,
devoid of any imitation of others' designs.

![Inkscape: Thumbnail: Bolt and Discs][content-07-bolt-and]

The satisfaction of crafting,
original industrial artwork cannot be overstated.
It's about capturing uniqueness,
rather than emulating designs created by others.

I'm happy with the result.
I'm content with the current outcome,
but I'm also aware that the creative journey never truly ends.
But strange I haven't found any idea,
to go further for other original shape.

I do not lost my Mojo.
Sometimes, it's merely a matter of shifting focus,
and allowing inspiration to flow naturally.
I just need to concentrate to something else.

#### 3D Frames

> A Creative Twist

Once again, we explore an alternative to traditional bullet points,
with our 3D frames.

3D looks design is always fascinating.
And it turned out that I can make many kind of shape,
offering a plethora of possibilities in the realm of shapes.

But not everything good enough and fitted into a template slide.
The reason is the better the 3D design,
the less room we have for textual content for the slide.

To strike a balance, I have crafted thin 3D lines,
that preserve ample space for your text.

![Inkscape: Thumbnail: 3D Frames][content-08-3d-frames]

#### Point of Drops

> Unconventional Bullets

In essence, these serve as an innovative take,
on traditional bullet points.
The concept allows us to use various shapes,
to convey points of thought.
We can creatively,
use any shape as a point of thought.

![Inkscape: Thumbnail: Point of Drops][content-09-point-of]

In reality, the journey diverged from my initial expectations.
I was too busy to make other shape.
So I'm stuck with this design.

As simple as that.

#### Closing

> A Parting Thought

My slides often conclude with a summary,
or fostering two-way communication with the audience,
or simply offering a word of gratitude.
I mean, _thank you_ word.

The emphasis here lies not in the shape, but wording.
Thus, I've left this page intentionally without artwork,
allowing you the freedom to craft your own message.

![Inkscape: Thumbnail: Closing][content-10-closing]

And with that, our journey comes to an end.
As a creation in the public domain,
you are free to use it as you see fit.

#### SVG Source

Feel free to download the SVG source files for the complete set of master slides using the link below:

* [SVG Source: Original Content][original-content]

Wishing you a fantastic day! 👏👏👏

-- -- --

### What Comes Next 🤔?

We're now ready to delve into the details.

Consider continuing your journey by reading [ [Inkscape: Slide Templates - Part Six][local-whats-next] ].

Thank you for being a part of this journey.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}design/2022/09/11/inkscape-slide-templates-06/


[original-content]: https://github.com/epsi-rns/candyclone-inkscape/blob/main/template-candyclone-content.svg


[isometric-wall]:   https://github.com/epsi-rns/isometric-wallpaper
[kartu-nama]:       https://akutidaktahu.netlify.app/inkscape/2017/10/03/kartu-nama.html
[isometric-cubes]:  https://epsi-rns.gitlab.io/design/2015/11/11/inkscape-isometric-infographics/
[progress-ring]:    https://epsi-rns.gitlab.io/design/2017/11/15/inkscape-progress-ring/

[//]: <> ( -- -- -- links below -- -- -- )

[content-01-preface]:   {{< baseurl >}}assets/posts/design/2022/09/thumbs-content/01-preface.png
[content-02-master]:    {{< baseurl >}}assets/posts/design/2022/09/thumbs-content/02-master-slides.png
[content-03-box-chart]: {{< baseurl >}}assets/posts/design/2022/09/thumbs-content/03-box-chart-figures.png
[content-04-table-and]: {{< baseurl >}}assets/posts/design/2022/09/thumbs-content/04-table-and-form.png
[content-05-bullet]:    {{< baseurl >}}assets/posts/design/2022/09/thumbs-content/05-bullet-numbering.png
[content-06-flow-and]:  {{< baseurl >}}assets/posts/design/2022/09/thumbs-content/06-flow-and-milestones.png
[content-07-bolt-and]:  {{< baseurl >}}assets/posts/design/2022/09/thumbs-content/07-bolt-and-disc.png
[content-08-3d-frames]: {{< baseurl >}}assets/posts/design/2022/09/thumbs-content/08-3d-frames.png
[content-09-point-of]:  {{< baseurl >}}assets/posts/design/2022/09/thumbs-content/09-point-of-drops.png
[content-10-closing]:   {{< baseurl >}}assets/posts/design/2022/09/thumbs-content/10-closing.png

[//]: <> ( -- -- -- links below -- -- -- )

[061-all-content-long]: {{< baseurl >}}assets/posts/design/2022/09/061-all-content-long.png
[061-all-in-pdf]:       {{< baseurl >}}assets/posts/design/2022/09/061-all-in-pdf.png

[062-slice-01-shorter]: {{< baseurl >}}assets/posts/design/2022/09/062-slice-01-shorter.png
[062-slice-02-shorter]: {{< baseurl >}}assets/posts/design/2022/09/062-slice-02-shorter.png
[062-slice-03-shorter]: {{< baseurl >}}assets/posts/design/2022/09/062-slice-03-shorter.png
[063-layers-pages]:     {{< baseurl >}}assets/posts/design/2022/09/063-layers-pages.png

