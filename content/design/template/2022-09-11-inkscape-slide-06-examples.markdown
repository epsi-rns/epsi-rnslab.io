---
type   : post
title  : "Inkscape: Slides - Example"
date   : 2022-09-11T04:29:15+07:00
slug   : inkscape-slide-templates-06
categories: [design]
tags      : [inkscape]
keywords  : [master slides, template, candyclone, example]
author : epsi
opengraph:
  image: assets/posts/design/2022/09/isometric/candyclone-iso-05.png

toc    : "toc-2020-inkscape-template"

excerpt:
  Sample by sample, one at a time,
  with an emphasis on layer management.

---

### Preface

> Sample by sample, one at a time, with an emphasis on layer management.

I believe it's essential to break down each section of the slide individually.
You don't need to go through all 86 pages;
instead, focus on a few samples to examine them closely.
Later, you can choose the variant that best suits your needs.
In essence, the other slides are just variations.

_Aren't you curious about how these slides were created?_

Moreover, this example isn't solely about,
creating visually appealing slides.
It also serves as a demonstration of,
effective layer and page management,
ultimately leading to an improved workflow.

#### The Aesthetics

> Industrial Styling

These example slides exhibit an industrial aesthetic.
However, this doesn't imply that every slide,
should adopt this particular style.
In fact, I aspire to have the creative freedom,
to explore alternative styles.

During my college years,
I acquired skills in technical drawing,
which heavily influenced the appearance of these example slides.
However, you're not obligated to adhere,
to my style with meticulously organized elements.

You can trust your instincts,
employ a simple drag-and-drop approach,
and perhaps achieve more captivating results through artistic expression,
rather than relying solely on shapes inspired by technical drawing.

#### The Purpose

> Preparedness

Beyond aesthetics, is there practicality?
Is this even useful?

This exemplary slide serves a multitude of purposes,
and can be employed for various occasions.
I've personally utilized these templates extensively in the past.
However, I've also come to realize that there are instances,
when I don't require any of these shapes at all.
Different projects demand distinct slides.
By having the foresight to prepare,
I can expedite my tasks more efficiently.

#### Workflow

Efficiently managing layers is a crucial aspect,
of our work process when using Inkscape.
Surprisingly, this topic is often overlooked by Inkscape users.
Working on substantial documents offers us an opportunity to illustrate
how organizing layers can significantly streamline our work.

The concept of workflow, on the other hand,
represents a different dimension altogether.
It isn't an inherent part of the artwork,
nor is it a component of the diagramming process.
When creating art or diagrams,
we often do so without much consideration for pages and layers.

In many Inkscape projects,
the primary focus is on the artwork itself,
and document organization might seem unnecessary.
I learned technical drawing in college,
where I could produce drawings manually with just paper and pencil.

However, the professional landscape has evolved.
We now need to share our documents efficiently,
often without requiring extensive training from our audience.
Utilizing standard templates,
consistent layer naming conventions,
standardized page placement,
and predefined layouts can save us,
from reinventing the wheel every time.

Standardization might seem like it limits creativity,
but it's important to note that you can always,
customize your templates during your free time.
In busy professional hours, where time is precious,
your priority should be efficiently completing your tasks.
This means delivering your presentation slides to your audience.

#### Main Layers

> Introducing a dummy layer, a placeholder for importing object

For a big document,
I often create several detailed diagrams independently. 
Then, I select only one of these handcrafted diagrams,
to be incorporated into the main slide.
This repetitive process require a special workflow.

Typically, I establish an import layer,
positioned at the top, above all other layers.
This layer serves as a space,
where I can seamlessly copy and paste elements,
from other Inkscape documents.
I can then adjust their positioning or scale,
without causing any disruption to the existing layers.

![Inkscape: Main Layers: All][101-main-layers]

#### SVG Source

Feel free to download the SVG source files,
for the complete set of master slides using the link below:

* [SVG Source: Original Content][original-content]

-- -- --

### Preface

> A Brief Introduction

The cover page holds paramount significance.
You can explore the significance of cover pages online until your fingers tire.

#### Cover Page

For official documents,
you may want to include a revision table,
logos, and other elements.
One of the most challenging aspects is creating the artwork.

![Inkscape: Slide Example: Cover][01.01-cover]

I maintain a consistent sublayer structure, including:

* Hilite
* Title
* Text
* Image

This cover page utilizes a `Title` layer,
along with an additional `Decoration` layer.

![Inkscape: Sublayers: Cover][111-layers-cover]

#### Contact

This slide serves as a sort of business card,
allowing you to introduce yourself swiftly.
As a professional with numerous accolades and merits,
simplifying your business card can be a challenging task.
I firmly believe that less is more.
Instead of crowding all your resume details into one slide,
focus on highlighting your most impactful strengths.

With ready-made artwork at your disposal,
you can craft your own introductory slide.

![Inkscape: Slide Example: Contact][01.08-contact]

Notice the `hilite` layer in the top left corner of the page.
I use the `hilite` layer to convey additional information for the slide
 such as `draft`` or `needs review``, emphasizing it with a yellow color.
 This helps ensure that I don't inadvertently send,
 an incomplete document to another division.

I also utilize the hilite layer to,
accentuate specific sections of a slide or convey the purpose,
such as the `why`, `how`, and `what` in the Golden Circle.

![Inkscape: Sublayers: Contacts][112-layers-contacts]

Other layers like `Text` and `Image` are,
a common name sublayer names that occur almost in every slide.

Much like assigning variable names in coding,
coming up with suitable layer names may require substantial thought.
To save time, I've established standardized layer names for myself,
preventing me from spending excessive time pondering ideal layer names.

-- -- --

### Master Slides

> A Valuable Resource

I've placed the template here for quick and easy access,
allowing me to copy and paste swiftly for my presentation projects.

#### Default Slide

You've likely encountered this particular slide numerous times.
This time, it comes equipped with,
a pre-designed title and text for your convenience.

![Inkscape: Slide Example: Default Page][02.03-default-page]

The layer structure remains consistent,
with the layers in the previous article,
with the only addition being the `Text`` layer.


![Inkscape: Sublayers: Default][123-layer-default]

I intentionally maintain a gap,
between the chapter break and the master slide ,
o distinguish it from regular example slides.
While the chapter page occupies the topmost position in the column as number 1,
the master slide page starts from 3, rather than 2.

-- -- --

### Master Examples

In addition to the master template,
I've also included examples of how to use the template,
with straightforward content.

#### Alternate Background

This serves as an illustration of the alternate template with a background,
complete with a `Lorem Ipsum`` paragraph.
This allows you to visualize how your ideas will appear,
when incorporated into this template.

![Inkscape: Slide Example: Alternate Background][03.03-alternate-back]

For authentic office templates,
logos are typically a requirement.
Therefore, I've established a layer
where you can seamlessly incorporate your official logo.

![Inkscape: Sublayers: Alternate Background][133-layer-alternate]

-- -- --

### Boxes, Charts and Figures

> A Timeless Collection

Now is an opportune moment to explore different examples of layer management. 
Starting with this template,
I've created separate layers to distinguish between the template and the content.

#### Boxes Design

This design was inspired by Hervy's work,
and it turns out that this style is quite common in slide templates.

![Inkscape: Slide Example: Boxes Design][04.03-team-members]

This page features the following sublayer structure:

* Text
* Boxes
* Common Page
  * Default Teal
  * Background

![Inkscape: Sublayers: Boxes][143-layer-boxes]

The way you organize the layers for each slide is entirely at your discretion.

#### Infographics

This project originated as my homework assignment back in 2019,
when I was studying tax management for my own company.
Surprisingly, Inkscape turned out to be
a powerful tool for illustrating tax calculations.

![Inkscape: Slide Example: Infographics][05.04-infographics]

You can even differentiate between the text layer and the title layer,
as they serve different purposes.
Sometimes, a layer is purely a vector diagram with no text at all,
such as this `Taxes` layer.
The complete sublayer structure for this example slide is as follows:

* Text
* Taxes
* Common Page
  * Default Cyan
  * Background

![Inkscape: Sublayers: Taxes][154-layer-taxes]

-- -- --

### Tables and Forms

> A Personal Endeavor

Why not use Excel/Calc instead?
Is this not the right tool for the job?

Most of the time, I rely on spreadsheets,
and simply copy-paste screenshots or export them as PNG images.
However, there are rare occasions,
when I need to incorporate an editable table into an SVG vector format.
In such instances, having a prepared template with a table is ,
ar better than scrambling to create one during busy times.

I've provided two types of designs:

1. The most common table design you can find on the internet.
2. My original form design, which I personally use for my accounting tasks.

#### Simple Statistics

This is the typical table design that,
you can easily find on the internet.
Creating table designs in vector format,
isn't as straightforward as using a spreadsheet.
We need to prepare specific sublayers,
for tables/forms, including text, borders, and boxes.

![Inkscape: Slide Example: Simple Statistics][06.03-simple-stat]

The specialized sublayer structure for tables can be illustrated as follows:

* Title
* Text
* Table: Text
* Table: Border
* Table: Box
* Image
* Default Blue
* Background White

![Inkscape: Sublayers: Table][163-layer-table]

Yo can check what object exist in any sublayer by examining the SVG source.

#### Fancy Form Journal

This is my original form design,
which I use for my accounting tasks.
This slide is relatively complex,
so I've included an additional table guidance layer,
to help align text using hidden boxes.

![Inkscape: Slide Example: Fancy Form Journal][06.05-fancy-form-jour]

The specialized sublayer structure for tables is as follows:

* Title
* Text
* Table: Text
* Table: Border
* Table: Box
* Table: Guidance
* Image
* Default Blue
* Background White

![Inkscape: Sublayers: Form][165-layer-form]

You can reveal the table guidance by unhiding the layer,
enabling you to align text based on the position of the boxes.

![Inkscape: View: Table/Form Guidance][165-view-guidance]

Remember to hide the guidance layer's visibility,
once you've finished aligning the text.

-- -- --

### Custom Numbering Bullets

> Conveying Clarity

Inkscape users often grapple with consistent layouting.
However, once you've mastered it,
layouting becomes a breeze.
I typically prepare a few lines or boxes,
with predefined placements as alignment references.
This approach allows me to meticulously align each sentence,
while channeling my thoughts onto the canvas.

With simplified layouting, I can concentrate on the content,
without becoming overly concerned about the technical aspects of placement.

#### Bullet Numbering Sets

> How can you create something like this?

I created this vibrant collection of progress rings,
quite some time before considering making presentation slides.

* [Inkscape: Progress Ring Animation][progress-ring]

Feel free to adjust the bullet colors to match your mood.

![Inkscape: Bullet Numbering Sets: Progress Ring][progress-ring-bright]

#### Four Bullets

Using larger bullets is necessary when you only have short sentences,
preventing your slide from appearing too empty.

![Inkscape: Slide Example: Four Bullets][07.03-four-bullets]

The sublayer structure for this slide is straightforward,
so we can skip the sublayer screenshot this time.

#### Six Red Bullets

As paragraphs accumulate,
space on the slide becomes limited,
necessitating the use of smaller bullets.

![Inkscape: Slide Example: Six Red Bullets][07.05-six-red-bullets]

The document comprises the following layers:

* Nano Logo
* Text
* Deco
* Guidance
* Common Page

Notice that I've placed the necessary guidance in its dedicated layer.

![Inkscape: Sublayers: Bullets][174-layer-bullets]

Once again, you can reveal the table guidance by unhiding the layer.
This allows you to align text according to the blue lines' positions,
both on the left and at the bottom.

The guidance serves as a helpful starting point for layouting.
However, after fixing the positioning,
you may still need to adjust the objects for better placement.
As illustrated below, the guidance isn't a strict rule to follow.

![Inkscape: View: Bullets Guidance][174-view-guidance]

For a comprehensive understanding,
you can open the document and inspect the objects on each layer.
Compare their placement with the guidance.

Even better, you can create your own guidance objects,
and experiment with your own text placement.

-- -- --

### Flow and Milestones

> Depicting Progress

I employ these slides below as a preliminary summary,
before commencing a training session.
This provides the audience with a preview of the material,
they will encounter in the class.

Progress slides are a typical element in example presentations
I admire the way they are presented; they inspire me.
I often wondered if I could create similar infographics,
until I ultimately crafted my own original progress slide,
to fulfill my specific requirements.

#### Timeline Six Milestones

This shape is just a fusion of rectangles and triangles,
enhanced with a gradient of material colors.

![Inkscape: Slide Example: Timeline Six Milestones][08.02-timeline-six]

Similar to the master slide,
it consists of the source vector shape and the cut version.
Now we have layers as follows:

* Text
* Cut
* Source
* Common Page

![Inkscape: Sublayers: Timeline][182-layer-timeline]

Because it's created using simple shapes,
I can easily customize the shape.
I can modify it to have five or four parts instead of a six-part timeline,
as you can see in other slides.

#### 3D Step-by-Step

This ladder is one of my favorites,
for illustrating the level of difficulty.
It allows me to map the big picture,
while highlighting what needs to be accomplished at each step.

![Inkscape: Slide Example: 3D Step by Step][08.05-3d-step-by-step]

I begin creating this slide by establishing a guidance layer.

#### 3D Divider

Initially, I designed this slide for side-by-side comparisons.
However, in practice, I use it to display a table of contents.
I have the flexibility to include a different number of points,
and leave some placements empty.
It's user-friendly, requires minimal layout adjustments,
and provides ample space for additional content.

![Inkscape: Slide Example: 3D Divider][09.02-3d-divider]

I also began creating this slide by establishing a guidance layer.
Then, I crafted the 3D lines. Now we have layers as follows:

* Text
* Icon
* Circle
* Iso Line
* Guidance
* Common Page

![Inkscape: Sublayers: Divider][192-layer-divider]

You can see how something as simple as a guiding layer,
can help in creating an impressive shape.

![Inkscape: View: Divider Guidance][192-view-divider]

-- -- --

### Bolt and Discs

> Expanding Beyond Bullets

The challenge with predefined shapes is that,
the number of ideas we can convey on a slide is,
restricted by the shape itself.
By using circular shapes, I can create predefined templates:
for three points, four points, five, six, or even seven,
offering more flexibility and creative freedom.

#### Bolt Fingers

I favor this shape for representing ideas.
I used to employ this shape extensively,
when I had nothing more than a summary of thoughts
 Nowadays, I rarely use it,
 because I'm preoccupied with drawing more detailed content.

Creating this circular shape is straightforward,
all you need to do is rotate each shape,
based on a temporary larger circle.

![Inkscape: Slide Example: Bolt Fingers][10.02-bolt-fingers]

I place an icon on each outer circle for decoration purposes.
Furthermore, I position all the icon in a hideable layer,
allowing me to easily customize the basic shape,
by hiding unnecessary decorations.

Now we have layers as below:

* Text
* Icon
* Shape
* Common Page

![Inkscape: Sublayers: Bolt][202-layer-bolt]

You can observe that I've crafted,
other variants of this circular shape in the layer above.

#### Three Bolts

This is another example of a shape based on rotating an outer shape.

![Inkscape: Slide Example: Three Bolts][10.04-three-bolts]

The layer arrangement remains identical to the previous slide,
with no changes at all.

#### Brake Pads

This is simply a variant with a higher level of technical complexity.

![Inkscape: Slide Example: Brake Pads][11.01-brake-pads]

The main layer contains the following sublayers:

* Text
* Shape: Small
* Annotation
* Icon
* Shape: Big
* Common Page

![Inkscape: Sublayers: Brake][216-layer-brake]

It may appear challenging, but it's quite straightforward.

-- -- --

### 3D Frames

> A Creative Approach

Crafting 3D visuals with a distinctive appearance,
demands the use of shape tools.
It involves adjusting the edge points of an original rectangle,
with consistent offsets.

It's important to note that these aren't 3D isometric shapes.
While isometric shapes typically require a 30-degree offset,
I've opted for a 45-degree offset to align with the master slide's cut.
This means that if you have an X:Y position of 200px:200px,
you can offset it to 220px:220px or perhaps 220px:180px,
depending on the direction.

I've created numerous variants,
but fundamentally, they all share the same principles.

#### Outer Box

Here's an example of the variant in action.

![Inkscape: Slide Example: Outer Box][13.02-3d-outer-box]

This is how I manipulate the edge points of a rectangle.
By offsetting the XY points, we can achieve this 3D effect.

![Inkscape: Shape Tools: Walk][235-tool-walk]

The layers for this slide are as follows:

* Text
* Shape
* Guidance
* Common Page

#### 3D Six Divider

![Inkscape: Slide Example: 3D Six Divider][13.05-3d-six-divider]

You can gain a deeper understanding of the provided example,
by examining the SVG source.
There are no special techniques involved,
just patience and precision.

-- -- --

### Point of Drops

> Unique Bullet Points

Not content with my own colorful bullets,
I drew inspiration from my collection of shapes,
to create alternative bullet points.

You can access the original image here:

* [Impress Slide: Point of Drops][point-of-drops]

![Inkscape: Basic Shapes: Point of Drops][drops-measure-boxed]

#### Three Horizontal Points

Now we can easily apply this shape to any slide.

![Inkscape: Slide Example: Three Horizontal Points][14.05-three-h-points]

The original layer names were created in the past,
before the page feature was introduced in Inkscape.
Back then, all slides were on a single page.
Since the layer arrangement at that time,
involved grouping everything under one main layer,
the only way to quickly distinguish objects was,
by carefully naming the layer to reflect its content.

For this historical reasons,
the layer arrangement is slightly different with distinct names:

* 3H Text
* 3H Drops
* Source: Alone
* Common Page

Now, for each slide variant,
we have these layer names as shown in the figure below:

![Inkscape: Sublayers: Horizontal Points][245-layer-3h-drops]

#### Three Vertical Points

![Inkscape: Slide Example: Three Vertical Points][15.01-three-v-points]

Also for historical reason,
I placed the title in the `Common Page` layer.

* 3V Text
* 3V Drops
* Source: Alone
* Common Page
  * Title
  * Default Blue
  * Background

![Inkscape: Sublayers: Vertical Points][251-layer-3v-drops]

Time has passed quickly,
and I inadvertently placed the title in the `Common Page` layer,
where it should not be.

I appreciate maintaining diversity,
so I believe it's best to leave it as is,
considering it as a variation in layer management.

#### Four Diagonal Points

This is just another example.

![Inkscape: Slide Example: Four Diagonal Points][15.04-four-d-points]

There's nothing new with this slide.

-- -- --

### Closing

> A Parting Reflection

Your closing statement can often,
be the measure of your entire presentation.
The choice of words is paramount,
surpassing mere visual aesthetics.

#### Chapter Quest

As is customary in public speaking,
two-way communication can be employed to engage the audience.

Based on my experience, there are several methods,
but they are highly specific and may not fit generic templates.
Therefore, I won't delve into these methods in this article.

![Inkscape: Slide Example: Chapter Quest][16.01-chapter-quest]

#### Conclusions

However, fancy visuals can still enhance the content.

![Inkscape: Slide Example: Conclusions][16.04-conclusions]

The layer structure appears standard:

* Text
* Nano Logo
* Image
* Guidance
* Common Page
  * Title
  * Default Red
  * Background

![Inkscape: Sublayers: Conclusions][264-layer-conclusion]

Without a doubt, guidance is,
essential for properly laying out the slide.

![Inkscape: View: Conclusion Guidance][264-view-conclusion]

-- -- --

### Complete Preview

By using just Inkscape as single application only,
I've streamlined my entire workflow,
from creating the artwork to delivering the presentation.

We have now covered the example slides.
How about taking another look at all the templates in thumbnail form?

![Inkscape: Thumbnails Preview: All Contents][preview-all-content]

In the real world, you would be pouring your thoughts into the slides,
whether using this template or your own custom template.

#### SVG Source

Feel free browse the candycone repository.

* [Candyclone Repository][original-repo]

-- -- --

### What Comes Next 🤔?

After the pouring content, we can still creatively showing off the slide.
From just thumbnails with shadow, isometric 3D,
or even sharing video in social media.

Consider continuing your journey by reading [ [Inkscape: Slide Templates - Part Seven][local-whats-next] ].

Thank you for being a part of this journey.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}design/2022/09/13/inkscape-slide-templates-07/


[original-repo]:    https://github.com/epsi-rns/candyclone-inkscape/
[original-content]: https://github.com/epsi-rns/candyclone-inkscape/blob/main/template-candyclone-content.svg

[progress-ring]:    https://epsi-rns.gitlab.io/design/2017/11/15/inkscape-progress-ring/
[point-of-drops]:   https://epsi-rns.gitlab.io/design/2020/09/25/inkscape-impress-slides-05/


[//]: <> ( -- -- -- links below -- -- -- )

[01.01-cover]:          {{< baseurl >}}assets/posts/design/2022/09/content/01.01-cover.png
[01.08-contact]:        {{< baseurl >}}assets/posts/design/2022/09/content/01.08-contact.png
[02.03-default-page]:   {{< baseurl >}}assets/posts/design/2022/09/content/02.03-default-page.png
[03.03-alternate-back]: {{< baseurl >}}assets/posts/design/2022/09/content/03.03-alternate-background.png
[04.03-team-members]:   {{< baseurl >}}assets/posts/design/2022/09/content/04.03-team-members.png
[05.04-infographics]:   {{< baseurl >}}assets/posts/design/2022/09/content/05.04-infographics.png
[06.03-simple-stat]:    {{< baseurl >}}assets/posts/design/2022/09/content/06.03-simple-statistics.png
[06.05-fancy-form-jour]:{{< baseurl >}}assets/posts/design/2022/09/content/06.05-fancy-form-journal.png
[07.03-four-bullets]:   {{< baseurl >}}assets/posts/design/2022/09/content/07.03-four-bullets.png
[07.05-six-red-bullets]:{{< baseurl >}}assets/posts/design/2022/09/content/07.05-six-red-bullets.png
[08.02-timeline-six]:   {{< baseurl >}}assets/posts/design/2022/09/content/08.02-timeline-six-milestones.png
[08.05-3d-step-by-step]:{{< baseurl >}}assets/posts/design/2022/09/content/08.05-3d-step-by-step.png
[09.02-3d-divider]:     {{< baseurl >}}assets/posts/design/2022/09/content/09.02-3d-divider.png
[10.02-bolt-fingers]:   {{< baseurl >}}assets/posts/design/2022/09/content/10.02-bolt-fingers-middle.png
[10.04-three-bolts]:    {{< baseurl >}}assets/posts/design/2022/09/content/10.04-three-bolts.png
[11.01-brake-pads]:     {{< baseurl >}}assets/posts/design/2022/09/content/11.01-brake-pads.png
[13.02-3d-outer-box]:   {{< baseurl >}}assets/posts/design/2022/09/content/13.02-3d-outer-box.png
[13.05-3d-six-divider]: {{< baseurl >}}assets/posts/design/2022/09/content/13.05-3d-six-divider.png
[14.05-three-h-points]: {{< baseurl >}}assets/posts/design/2022/09/content/14.05-three-horizontal-points.png
[15.01-three-v-points]: {{< baseurl >}}assets/posts/design/2022/09/content/15.01-three-vertical-points.png
[15.04-four-d-points]:  {{< baseurl >}}assets/posts/design/2022/09/content/15.04-four-diagonal-points.png
[16.01-chapter-quest]:  {{< baseurl >}}assets/posts/design/2022/09/content/16.01-chapter-question.png
[16.04-conclusions]:    {{< baseurl >}}assets/posts/design/2022/09/content/16.04-conclusions.png

[//]: <> ( -- -- -- links below -- -- -- )

[progress-ring-bright]: {{< baseurl >}}assets/posts/design/2022/09/progress-ring-bright-number.png
[basic-drops-asking]:   {{< baseurl >}}assets/posts/design/2022/09/basic-drops-asking.png
[drops-measure-boxed]:  {{< baseurl >}}assets/posts/design/2022/09/drops-measure-boxed.png

[//]: <> ( -- -- -- links below -- -- -- )

[preview-all-content]:  {{< baseurl >}}assets/posts/design/2022/09/thumbs-content/preview-all.png

[101-main-layers]:      {{< baseurl >}}assets/posts/design/2022/09/101-main-layers.png

[111-layers-cover]:     {{< baseurl >}}assets/posts/design/2022/09/111-layers-cover.png
[112-layers-contacts]:  {{< baseurl >}}assets/posts/design/2022/09/112-layers-contacts.png

[123-layer-default]:    {{< baseurl >}}assets/posts/design/2022/09/123-layer-default.png
[133-layer-alternate]:  {{< baseurl >}}assets/posts/design/2022/09/133-layer-alternate.png

[143-layer-boxes]:      {{< baseurl >}}assets/posts/design/2022/09/143-layer-boxes.png
[154-layer-taxes]:      {{< baseurl >}}assets/posts/design/2022/09/154-layer-taxes.png

[163-layer-table]:      {{< baseurl >}}assets/posts/design/2022/09/163-layer-table.png
[165-layer-form]:       {{< baseurl >}}assets/posts/design/2022/09/165-layer-form.png
[165-view-guidance]:    {{< baseurl >}}assets/posts/design/2022/09/165-view-guidance.png

[174-layer-bullets]:    {{< baseurl >}}assets/posts/design/2022/09/174-layer-bullets.png
[174-view-guidance]:    {{< baseurl >}}assets/posts/design/2022/09/174-view-guidance.png

[182-layer-timeline]:   {{< baseurl >}}assets/posts/design/2022/09/182-layer-timeline.png
[192-layer-divider]:    {{< baseurl >}}assets/posts/design/2022/09/192-layer-divider.png
[192-view-divider]:     {{< baseurl >}}assets/posts/design/2022/09/192-view-divider.png

[202-layer-bolt]:       {{< baseurl >}}assets/posts/design/2022/09/202-layer-bolt.png
[216-layer-brake]:      {{< baseurl >}}assets/posts/design/2022/09/216-layer-brake.png

[235-tool-walk]:        {{< baseurl >}}assets/posts/design/2022/09/235-tool-walk.png
[245-layer-3h-drops]:   {{< baseurl >}}assets/posts/design/2022/09/245-layer-3h-drops.png
[251-layer-3v-drops]:   {{< baseurl >}}assets/posts/design/2022/09/251-layer-3v-drops.png

[264-layer-conclusion]: {{< baseurl >}}assets/posts/design/2022/09/264-layer-conclusion.png
[264-view-conclusion]:  {{< baseurl >}}assets/posts/design/2022/09/264-view-conclusion.png
