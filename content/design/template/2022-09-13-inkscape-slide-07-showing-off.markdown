---
type   : post
title  : "Inkscape: Slides - Showing Off"
date   : 2022-09-13T04:29:15+07:00
slug   : inkscape-slide-templates-07
categories: [design]
tags      : [inkscape]
keywords  : [master slides, template, candyclone, thumbnails, 3D isometric, videos]
author : epsi
opengraph:
  image: assets/posts/design/2022/09/isometric/candyclone-iso-05.png

toc    : "toc-2020-inkscape-template"

excerpt:
  Presenting Design through Thumbnails, 3D Isometric Views, and Videos.

---

### Preface

> Presenting the design in thumbnails, 3D isometric and even video.

Certainly, a seasoned Inkscape expert understands precisely,
how to creatively showcase their designs on social media platforms.
However, not everyone has extensive experience with Inkscape.
For newcomers, the process of efficiently presenting their designs may appear daunting.
Whether it's about casually sharing their work with close friends,
or demonstrating their hard work to their superiors,
this guide aims to provide clarity and guidance,
on how to effectively utilize thumbnails,
3D isometric views, and videos for these purposes.

-- -- --

### Thumbnails

#### An example Case

Shadows can somehow add intrigue to thumbnails,
as demonstrated here:

![Inkscape: Thumbnail: Flow and Milestones][content-06-flow-and]

#### The Guidance

> Guidance behind the looks

As always, there's a method behind the aesthetics.
In this example, we're working with the following specifications:


* Columns: 5
* Rows: 3

While I have various thumbnail layouts at my disposal,
including 3x3, 5x1, 5x2, and more,
for our demonstration, we'll focus on the 5x3 format:

![Inkscape: Multi Thumbnail Guidance][301-flow-shadow]

Now, let's take a peek behind the scenes,
to delve deeper into the structure.
Here, we have a sublayer arrangement consisting of:

* Thumbs
* Shadow
* Guidance
* Background

![Inkscape: Sublayers: Thumbnails][302-layer-thumbs]

Mmmmhhh.. Curious about what's really happening here?
Let's explore further.

#### Document Properties

> Page Setup

While the provided example is useful,
it's worth noting that we can start
with a fresh document designed specifically,
for a single row in height.

Here are the recommended settings:

* Unit: px
* Width: 2000
* Height: 225
* Scale: 1

![Inkscape: Thumbnails: Document Properties][303-page-setup]

Now, you might be wondering why these particular dimensions were chosen.

#### The Placeholder Geometry

The original slide size measures 1600 px by 900 px.
To resize it for your thumbnail,
you selected to fifth of the original size,
resulting in dimensions of 320 px by 180 px.

However, to simplify things and avoid complex calculations,
we can rounded it up to a more manageable frame of 400 px by 200 px.

![Inkscape: Single Thumbnail Guidance][304-single-slide]

Now, when it comes to centering the inner box (the image placeholder),
you don't need to do the math manually.
Inkscape provides alignment tools for precisely this purpose.

![Inkscape: Alignment Tools][305-alignment]

These alignment tools make it effortless,
to align and position elements within your design,
ensuring everything looks clean and professional,
without the need for manual calculations.

#### The Gaps

Determining the right gap size is,
an essential part of achieving a balanced and visually pleasing design.
Here's how you've calculated and applied the gaps.

*Horizontal Gap*:

The width of the outer box (the guidance) is 400 px
and without any gap, 
he total width across five columns would be 2000 px,
which matches the width of the page.

*Vertical Gap*:

Initially, I preferred the page height,
to have round numbers like 200 px, 400 px, or 600 px.
However, to improve the visual aesthetics,
I decided to add additional gaps,
resulting in heights of 225 px, 450 px, and 675 px.

Here's how I calculated the padding and gaps:

* *For 1 row space* (225 px - 200 px = 25 px difference):
  * 10 px top padding
  * 15 px bottom padding

* *For 2 row space* (450 px - 400 px = 50 px difference):
  * 10 px top padding
  * 25 px gap
  * 15 px bottom padding

* *For 3 row space* (675 px - 600 px = 75 px difference):
  * 10 px top padding
  * 25 px gap
  * 25 px gap
  * 15 px bottom padding

With these calculations in mind,
you can confidently position the outer rectanglem
along with the inner box inside the designated spaces,
for a well-structured and visually appealing design.

#### Multiply rectangle

Now, I can multiply both rectangles by,
duplicating the object and then shifting them using the transform tools.

* Horizontal: 40px
* Vertical: 225px

![Inkscape: Transform Tools][306-transform]

#### The Placeholder Looks

Given that the slide's color may vary,
I opted for a gradient fill for the guidance box.
This choice ensures a clear distinction,
between the slide and the guidance,
and you can select any gradient for this purpose.
Aesthetic considerations are not essential here,
since we intend to hide the layer visibility anyway.

![Inkscape: Fill Stroke][307-fill-stroke]

Next, we can apply a drop shadow effect,
to the 320 px by 180 px inner box.

![Inkscape: Drop Shadow][308-drop-shadow]

You might wonder why we're placing the shadow,
on the placeholder boxes rather than directly on the slide itself. 
he reason is that I frequently update the slide's content.
The slide is dynamic, while the placeholders remain static.
Placing the shadow on the placeholders ensures consistent visual appeal,
regardless of the slide content changes.

#### Importing the Slide

I import the PNG version of the slide,
each originally sized at 1600 px by 900 px.
While you can resize the PNG during the export process,
for the sake of this example,
let's keep the images at their original dimensions.

Upon importing, you'll likely need to adjust the image's size,
to fit within the placeholder appropriately.
To maintain the correct aspect ratio,
simply lock the dimensions while resizing.

![Inkscape: Resize: Keep Ratio][309-import-slide]

Once you've resized the image,
proceed to position it within the placeholder area.

![Inkscape: Slide Placement][311-slide-placement]

> That's it!

Don't forget to hide the visibility of the guidance layer,
for a polished final result.

#### Result

Now, let's take a look at the result for a 3-row by 3-column layout.

![Inkscape: Showing Off: Thumbnails 3x3][312-thumbs-3x3]

#### SVG Source

If you'd like to access the SVG source files for these thumbnails,
please feel free to download them using the following link:

* [SVG Source: Content Thumbnails][content-thumbs-svg]

-- -- --

### 3D Isometric

Why stick with plain flat designs,
when you can create your own customizable 3D designs?
Incorporating 3D elements can add intrigue and depth to your images.

![Inkscape: Isometric Preview: 01][candyclone-iso-01]

Crafting a 3D isometric design is relatively straightforward,
but does *require a few essential steps*.
Fortunately, I've outlined clear instructions using actions and numbers,
making it easy for you to replicate the results with minimal effort.

#### SVG Source

For a comprehensive,
step-by-step guide to creating 3D isometric designs,
please feel free to download the SVG source files,
using the following link:

* [SVG Source: 3D Isometric: step-by step][example-iso-slides]

#### Document Properties

Before diving into your 3D isometric project,
consider creating a new document
and configuring the properties as follows:

* Unit: px
* Diplay Unit: px
* Width: 2000
* Height: 1500
* S: Doesn't matter

![Inkscape: 3D Isometric: Document Properties][321-doc-properties]

These settings will provide you with a suitable canvas for your 3D design endeavors.

#### Rectangle Source

Begin by creating two rectangles,
each with a slightly distinct color:

*Outer Rectangle*:

* Width: 400
* Height: 200
* Color: Blue100 with 80% Alpha

*Inner Rectangle*:

* Width: 320
* Height: 180
* Color: Blue200 with 80% Alpha

![Inkscape: 3D Isometric: Rectangle Source][322-source-rectangle]

Now, to ensure proper alignment,
center the inner rectangle using the alignment tool.
Once aligned, group both rectangles together,
and if desired, you can optionally assign the object group the name `Left Top`.

#### Plain Guidance

Creating the 3D object requires a temporary guidance,
and we'll start this guidance from a plain, flat object.

Begin by duplicating the image,
and then moving it using the align tool,
specifically the icon with the dot,
as shown in figure below:

![Inkscape: Align Tool][323-align-tool]

Alternatively, you can position
this 400 px by 200 px object as follows:

*Rect: Left - Top (Back)*

* X: 40
* Y: 140
* C: Blue100, Blue200 with 80% Alpha

*Rect: Right - Top (Back)*

* X: 440
* Y: 340
* C: Blue200, Blue300 with 80% Alpha

*Rect: Left - Bottom (Front)*

* X: 40
* Y: 140
* C: Blue300, Blue400 with 80% Alpha

*Rect: Right - Bottom (Front)*

* X: 440
* Y: 340
* C: Blue400, Blue500 with 80% Alpha

These adjustments will result in the image shown below:

![Inkscape: 3D Isometric: Plain Guidance][324-plain-guidance]

Optionally, you can rename the other layers,
to reflect the corresponding boxes' positions.

![Inkscape: 3D Isometric: Layer: Plain Guidance][325-plain-layer]

#### Isometric Guidance

> Skew and Rotate

To create a 3D isometric effect, we'll utilize the transform tool.
First, skew the object by 30 degrees and then rotate it by 30 degrees.

*Skew*

![Inkscape: Transfrom Tools: Skew 30° Degree][326-skew-30]

*Rotate*

![Inkscape: Transfrom Tools: Rotate 30° Degree][326-rotate-30]

*Transform: Left - Top (Back)*

* Skew: Horizontal -30°
* Rotate: -30°

*Transform: Right - Top (Back)*

* Skew: Horizontal 30°
* Rotate: 30°

*Transform: Left - Bottom (Front)*

* Skew: Horizontal -30°
* Rotate: -30°

*Transform: Right - Bottom (Front)*

* Skew: Horizontal 30°
* Rotate: 30°

The result of applying these transformations,
will look like the image below:

![Inkscape: 3D Isometric: Isometric Guidance][327-iso-guidance]

However, there's an issue to address:
the edges of the left object do not align perfectly,
with the edges of the right object.

#### Alignment

To address the misalignment issue,
we can use the alignment tool to ensure that,
the edges of the objects are perfectly aligned.

![Inkscape: Align Tool][328-align-edges]

By applying the alignment tool,
we achieve an aligned result as shown below:

![Inkscape: 3D Isometric: Aligned Isometric Guidance][329-iso-guidance]

#### Remove Helper

To simplify the design,
we can eliminate the outer rectangle.
Follow these steps:

* Ungroup the four isometric objects.
* Delete all four outer objects, leaving only the four inner objects.

The result will look like this:

![Inkscape: 3D Isometric: Isometric Objects][331-isometric-object]

For the sake of clarity, I've renamed the layer as shown below:

![Inkscape: 3D Isometric: Layer Rename][332-layer-rename]

#### Shadow

Now, we can apply a shadow effect,
to the isometric rectangle with the following settings:

* Blur Radius: 2px
* Horizontal Offset: 4px
* Vertical Offset: 4px

![Inkscape: 3D Isometric: Drop Shadow][333-drop-shadow]

The resulting image will look like this:

![Inkscape: 3D Isometric: Isometric with Shadow][334-iso-shadow]

#### Multiple The Isometric Objects

Now, we can replicate the result,
by carefully managing the placement,
moving 400 px both horizontally and vertically.
Follow these steps for each box:

Duplicate the original object,
move and change the color.

*Box 01*:
* Blue300, Blue100 with 80% Alpha
* Move: Horizontal (H): 0, Vertical (V): 0
* Shadow

*Box 02*:
* Red300, Red100 with 80% Alpha
* Move: Horizontal (H): 800, Vertical (V): 0
* Shadow

*Box 03*:
* Teal300, Teal100 with 80% Alpha
* Move: Horizontal (H): 400, Vertical (V): 400
* Shadow

*Box 04*:
* Amber300, Amber100 with 80% Alpha
* Move: Horizontal (H): 1200, Vertical (V): 400
* Shadow

*Box 05*:
* Green300, Green100 with 80% Alpha
* Move: Horizontal (H): 0, Vertical (V): 800
* Shadow

*Box 06*:
* Indigo300, Indigo100 with 80% Alpha
* Move: Horizontal (H): 800, Vertical (V): 800
* Shadow

The resulting composition will look like this:

![Inkscape: 3D Isometric: Isometric Replicated][335-iso-multiply]

For clarity, I've renamed the layer as shown:

![Inkscape: 3D Isometric: Layer Rename][336-layer-replicate]

By adding shadows and fine-tuning the details,
you can export images as shown below:

![Inkscape: 3D Isometric: Exported Image][337-cc-iso-slides]

Now, you're ready to incorporate,
these isometric designs into your actual presentation.

#### Slide Images

Just as before,
we'll use PNG images as the source for our slides.
Here are the steps to incorporate them into your isometric design:

1. Import the PNG slide
   ![Inkscape: 3D Isometric: Import Slide PNG Image][338-import-slide]

2. Select, resize, skew, rotate, and align the image to fit the isometric design.
3. Send the image backward and hide original inner blue box guidance.
   ![Inkscape: 3D Isometric: Single Isometric Slide][339-iso-slide-single]

4. For images, 90% alpha transparency is enough.
   Unlike plain rectangle, images contain color texture.

Repeat these steps for the remaining three slides,
and apply shadows to the layers for consistency.

![Inkscape: 3D Isometric: Four Isometric Slides][341-iso-slides]

For ease of management,
rearrange the layers so you can easily hide the inner rectangles.

![Inkscape: 3D Isometric: Layer Rename][342-layer-slides]

With this, your isometric slides are complete,
ready to be showcased on social media.

#### SVG Source

If you'd like to access the SVG source files,
for the complete set of master slides,
please feel free to download them using the following link:

* [SVG Source: 3D Isometric: Complete][isometric-withgap]

-- -- --

### Video

Creating videos is even easier.

#### Source Images

To begin, let's create six frames containing our previous isometric slides,
each filled with content templates. Name them as follows:

* tmpl-01.png
* tmpl-02.png
* tmpl-03.png
* tmpl-04.png
* tmpl-05.png
* tmpl-06.png

![Inkscape: Video: Content Template Slides][351-tmpl-slides]

These six frames will serve as the foundation for generating a 30-second video.
This duration is perfect for sharing,
on platforms like WhatsApp as a marketing tool,
for your company or your workshop.

#### Convert PNG Images to MP4

You can utilize `FFmpeg`` in the terminal shell,
to convert the images into a video. Here's the command:

{{< highlight bash >}}
$ ffmpeg \
  -framerate 1/5 \
  -i "./tmpl-%02d.png" \
  -c:v libx264 \
  -vf "pad=ceil(iw/2)*2:ceil(ih/2)*2" \
  -r 24 \
  -pix_fmt yuv420p \
  ./iso-png.mp4
{{< / highlight >}}

![Inkscape: Video: Convert PNG Images to MP4][352-ffmpeg-png2mp4]

This command will generate a 30-second MP4 video without sound,
with the result as following video:

{{< embed-video width="512" height="384" src="assets/posts/design/2022/09/candyclone-iso-png.mp4" >}}

#### Command Explanation: Input Output

* `-i "./tmpl-%02d.png"`:
   This is the input file option.
   It tells FFmpeg to use a series of image files as input.
   The %02d is a placeholder for a two-digit number,
   that will be automatically incremented by FFmpeg.
   The command expects a sequence of image files,
   with names like "tmpl-01.png," "tmpl-02.png," and so on.

* `./iso-png.mp4`:
  This is the output file name and format.
  It's specifying that the output video should be named "iso-png.mp4" and will be in the MP4 format.

#### Command Explanation: Video

* `-framerate 1/5`:
  This option specifies the frame rate of the output video. In this case, it's set to 1 frame every 5 seconds. For six frames we've got 30 seconds.

* `-c:v libx264`:
  This option specifies the video codec to use for encoding the video.
  In this case, it's using the libx264 codec,
  which is a popular and efficient H.264 video codec.
  Compatible to be sent as whatsapp status.

* `-vf "pad=ceil(iw/2)*2:ceil(ih/2)*2"`:
   This is a video filter option that applies a filter to the video frames.
   In this case, it's using the "pad" filter,
   to ensure that the video dimensions are even (divisible by 2),
   which is a common requirement for many video codecs.

* `-r 24`:
  This option sets the output frame rate to 24 frames per second.
  It specifies how many frames should be displayed per second in the output video.

* `-pix_fmt yuv420p`:
  This option specifies the pixel format for the output video.
  YUV420p is a common pixel format used in video compression.

#### Getting Audio Sound

The next step involves selecting the right audio sound,
to complement the mood of your slide presentation.
I recommend using WAV format,
as it can be easily combined with `FFmpeg``.
You can find suitable audio from free websites like the one linked below:

* [Free Sound](https://freesound.org)

Please note that I'm not a lawyer,
and I can't mix my public domain content with specific sound files.
Therefore, I won't include an example sound here.

#### Adding Audio Sound to Video

To combine audio with your video,
you can once again use the `FFmpeg`` command in the terminal shell.
Here's the command:

{{< highlight bash >}}
$ ffmpeg \
  -i iso-png.mp4 \
  -i ethno-ambient.wav \
  -shortest -c:v copy -c:a aac \
  iso-va.mp4
{{< / highlight >}}

![Inkscape: Video: Append Audio Channel to MP4][353-ffmpeg-audio]

This command will result in an MP4 presentation video,
with a pleasant background sound.

{{< embed-video width="512" height="384" src="assets/posts/design/2022/09/candyclone-iso-va.mp4" >}}

#### Command Explanation: Input Output

In this command, we work with two input files.
First, we have the original video,
followed by the additional background audio.

* `-i iso-png.mp4`:
  This option specifies the first input file, which is "iso-png.mp4."
  This is the video file that you want to use as the video source.

* `-i ethno-ambient.wav`:
  This option specifies the second input file, which is "ethno-ambient.wav."
  This is the audio file that you want to use as the audio source.

* `iso-va.mp4`:
  This is the output file name and format.
  It specifies that the output video should be named "iso-va.mp4",
  with the specified video and audio settings.

#### Command Explanation: Video

* `-c:v copy`:
  This option specifies that the video codec should be copied,
  from the input to the output without re-encoding.
  In other words, it preserves the video codec used in "iso-png.mp4"
  in the output file "iso-va.mp4.
   This is often done to avoid quality loss from re-encoding.

#### Command Explanation: Audio

Given that the audio and video may have different durations,
we need to determine how to synchronize their lengths.
Additionally, it's essential to ensure that the audio format.,
aligns with the requirements for WhatsApp compatibility.

* `-shortest`:
  This option is used when you have inputs of different durations
  (in this case, a video and an audio file).
  It tells FFmpeg to stop encoding when the shortest input ends.
  In this context, it ensures that the output video will be,
  as long as the shorter of the two input files (either the video or the audio).

* `-c:a aac`:
  This option specifies the audio codec to use for encoding the audio.
  In this case, it's using the AAC audio codec,
  which is a common choice for MP4 files.

#### Mixing Multiple Audio Tracks

> Example Command

Sometimes, simplicity isn't enough, and you may want to,
incorporate various background audio sounds to match your presentation.
To achieve this, I can provide you with a command example,
to mix multiple audio tracks into an existing video that already contains audio.

Here's the command:

{{< highlight bash >}}
$ ffmpeg \
  -i iso-va.mp4 \  
  -i gun-round.wav \
  -i deep-clap.wav \
  -i poodle-barking.m4a \
  -i oversaturated-kick.mp3 \
  -filter_complex "\
    [1]adelay=5000|6000[aud1]; \
    [2]adelay=10000|10000[aud2]; \
    [1]adelay=15000|15000[aud3]; \
    [2]adelay=19000|20000[aud4]; \
    [3]adelay=13000|14000[aud5]; \
    [4]adelay=8000|8000[aud6]; \
    [0][aud1]amix[tmp1]; \
    [tmp1][aud2]amix[tmp2]; \
    [tmp2][aud3]amix[tmp3]; \
    [tmp3][aud4]amix[tmp4]; \
    [tmp4][aud5]amix[tmp5]; \
    [tmp5][aud6]amix" \
  -c:v copy -c:a aac \
  iso-amix.mp4
{{< / highlight >}}

The result can be considered either fabulous or lousy,
depending on your perspective, mood, and the weather in the sky.

{{< embed-video width="512" height="384" src="assets/posts/design/2022/09/candyclone-iso-va.mp4" >}}

I believe we've covered everything related to video production.

-- -- --

### Automation Idea

> Inkscape Python Extension

Considering that we have a substantial number of slides,
manually adding images to videos would be a laborious task.
Fortunately, Inkscape comes equipped with extensions,
allowing us to create customized Python scripts to automate this task.

Regrettably, time constraints have arisen.
My responsibilities have grown,
and I have numerous fundamental articles to complete,
aiming to provide valuable information to readers.
Consequently, I don't foresee having the opportunity,
to create these scripts in the near future.

I suppose this marks the end of our Inkscape template article series.
However, rest assured, I will be writing other articles as well.
Should you have any questions or need assistance,
you can always find me within the community.

-- -- --

### Wishlist

I hope that in the near future,
Inkscape will support internal anchors.
This would allow me to create a Table of Contents,
with internal links right at the beginning of the presentation.

Currently, Inkscape is equipped with an external href link feature.
When I create a PDF copy, it correctly displays the links in a browser.
However, unfortunately, there is currently no support for internal links.

-- -- --

### Conclusion

> Simplifying My Workflow

By utilizing Inkscape as a single application,
I've streamlined my entire workflow,
from creating artwork to delivering presentations.

#### Summary

The motivation for writing thess articles are, based on personal experience.

I believe on these thought:

1. There should be an alternative template in which users have freedom,
   to modify and customize according to their specific needs.

2. Layer structures could be standarized to facilitate
   seamless document exchange within a team.

3. Putting each slide on individual pages enhances workflow efficiency.
   Providing a clear view and allowing for easy selection.

4. Ready-to-use templates enable users
   to focus on content creation rather than design.

5. Layer as a powerful tool deserve a real world example.

6. Showing off in social media as a marketing tools should be easy.

I might be inaccurate, and anyone might have different opinion.

#### Improvement on Daily Basis

I sincerely hope this comprehensive tutorial enhances your skills,
enabling you to complete your work more efficiently.
Automation can significantly increase your productivity,
benefiting your overall projects,
and potentially leading to salary improvements.

Whether you're a student, recent graduate, freelancer, or self-learner,
I hope that this additional skill becomes an extra asset on your resume,
and then opens up opportunities for better job prospects.

I deeply appreciate the time you've dedicated to visit this article,
read its content, practice the Inkscape template,
and apply your newfound skills to your projects.

Farewell for now, and thank you for being a part of this journey!

[//]: <> ( -- -- -- links below -- -- -- )

[content-thumbs-svg]:   {{< baseurl >}}assets/posts/design/2022/09/thumbs-content/thumbs.svg

[example-iso-slides]:   https://github.com/epsi-rns/candyclone-inkscape/blob/main/steps/cc-iso-slides.svg

[isometric-withgap]:    https://github.com/epsi-rns/candyclone-inkscape/blob/main/isometric/isometric-withgap.svg

[content-06-flow-and]:  {{< baseurl >}}assets/posts/design/2022/09/thumbs-content/06-flow-and-milestones.png

[candyclone-iso-01]:    {{< baseurl >}}assets/posts/design/2022/09/isometric/candyclone-iso-01.png

[//]: <> ( -- -- -- links below -- -- -- )

[301-flow-shadow]:      {{< baseurl >}}assets/posts/design/2022/09/301-flow-shadow.png
[302-layer-thumbs]:     {{< baseurl >}}assets/posts/design/2022/09/302-layer-thumbs.png
[303-page-setup]:       {{< baseurl >}}assets/posts/design/2022/09/303-page-setup.png
[304-single-slide]:     {{< baseurl >}}assets/posts/design/2022/09/304-single-slide.png
[305-alignment]:        {{< baseurl >}}assets/posts/design/2022/09/305-alignment.png
[306-transform]:        {{< baseurl >}}assets/posts/design/2022/09/306-transform.png
[307-fill-stroke]:      {{< baseurl >}}assets/posts/design/2022/09/307-fill-stroke.png
[308-drop-shadow]:      {{< baseurl >}}assets/posts/design/2022/09/308-drop-shadow.png
[309-import-slide]:     {{< baseurl >}}assets/posts/design/2022/09/309-import-slide.png
[311-slide-placement]:  {{< baseurl >}}assets/posts/design/2022/09/311-slide-placement.png
[312-thumbs-3x3]:       {{< baseurl >}}assets/posts/design/2022/09/312-thumbs-3x3.png

[//]: <> ( -- -- -- links below -- -- -- )

[321-doc-properties]:   {{< baseurl >}}assets/posts/design/2022/09/321-doc-properties.png
[322-source-rectangle]: {{< baseurl >}}assets/posts/design/2022/09/322-source-rectangle.png
[323-align-tool]:       {{< baseurl >}}assets/posts/design/2022/09/323-align-tool.png
[324-plain-guidance]:   {{< baseurl >}}assets/posts/design/2022/09/324-plain-guidance.png
[325-plain-layer]:      {{< baseurl >}}assets/posts/design/2022/09/325-plain-layer.png
[326-skew-30]:          {{< baseurl >}}assets/posts/design/2022/09/326-skew-30.png
[326-rotate-30]:        {{< baseurl >}}assets/posts/design/2022/09/326-rotate-30.png
[327-iso-guidance]:     {{< baseurl >}}assets/posts/design/2022/09/327-isometric-guidance.png
[328-align-edges]:      {{< baseurl >}}assets/posts/design/2022/09/328-align-edges.png
[329-iso-guidance]:     {{< baseurl >}}assets/posts/design/2022/09/329-isometric-guidance.png

[331-isometric-object]: {{< baseurl >}}assets/posts/design/2022/09/331-isometric-object.png
[332-layer-rename]:     {{< baseurl >}}assets/posts/design/2022/09/332-layer-rename.png
[333-drop-shadow]:      {{< baseurl >}}assets/posts/design/2022/09/333-drop-shadow.png
[334-iso-shadow]:       {{< baseurl >}}assets/posts/design/2022/09/334-iso-shadow.png
[335-iso-multiply]:     {{< baseurl >}}assets/posts/design/2022/09/335-iso-multiply.png
[336-layer-replicate]:  {{< baseurl >}}assets/posts/design/2022/09/336-layer-replicate.png
[337-cc-iso-slides]:    {{< baseurl >}}assets/posts/design/2022/09/337-cc-iso-slides.png
[338-import-slide]:     {{< baseurl >}}assets/posts/design/2022/09/338-import-slide.png
[339-iso-slide-single]: {{< baseurl >}}assets/posts/design/2022/09/339-iso-slide-single.png

[341-iso-slides]:       {{< baseurl >}}assets/posts/design/2022/09/341-iso-slides.png
[342-layer-slides]:     {{< baseurl >}}assets/posts/design/2022/09/342-layer-slides.png

[//]: <> ( -- -- -- links below -- -- -- )

[351-tmpl-slides]:      {{< baseurl >}}assets/posts/design/2022/09/351-tmpl-slides.png
[352-ffmpeg-png2mp4]:   {{< baseurl >}}assets/posts/design/2022/09/352-ffmpeg-png2mp4.png
[353-ffmpeg-audio]:     {{< baseurl >}}assets/posts/design/2022/09/353-ffmpeg-audio.png
