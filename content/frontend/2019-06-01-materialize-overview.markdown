---
type   : post
title  : "Materialize CSS - Overview"
date   : 2019-06-01T09:17:35+07:00
slug   : materialize-overview
categories: [frontend]
tags      : [materialize, css]
keywords  : [plain html, custom theme, summary, overview]
author : epsi
opengraph:
  image: assets-frontend/2019/06/html-materialize-preview.png

toc    : "toc-2020-03-repository"

excerpt:
  Designing real world web page using Materialize, step by step.

---

### Preface

This repository is intended for beginner.

> Goal: Make your own theme using Materialize

Materialize CSS tutorial, from pure html+css, to sass.
Step by step, so any beginner can use this guidance.

#### Repository

* [Materialize CSS Test Drive][repository]

-- -- --

### Chapter Step by Step

#### Tutor 01

> Adding Materialize CSS

* Without Materialize CSS

* Using CDN

* Using Local

![Tutor 01][screenshot-01]

#### Tutor 02

> Using Materialize CSS: Navigation Bar

* Simple

* Full Featured

* With jQuery

* Adding Material Icon

![Tutor 02][screenshot-02]

#### Tutor 03

> Custom SASS

* Custom maximum width class

* Responsive gap using custom sass

![Tutor 03][screenshot-03]

#### Tutor 04

> Helper Class

* Spacing Helper Demo

* Altering Variables

![Tutor 04][screenshot-04]

#### Tutor 05

> HTML Box using Material Design

* Main Blog and Aside Widget

![Tutor 05][screenshot-05]

#### Tutor 06

> Finishing

* Blog Post Example

![Tutor 06][screenshot-06]

-- -- --

### Begin The Guidance

Thank you for cloning the repository.

[//]: <> ( -- -- -- links below -- -- -- )

[repository]:   https://gitlab.com/epsi-rns/tutor-html-materialize

[screenshot-01]:    https://gitlab.com/epsi-rns/tutor-html-materialize/raw/master/tutor-01/html-materialize-preview.png
[screenshot-02]:    https://gitlab.com/epsi-rns/tutor-html-materialize/raw/master/tutor-02/html-materialize-preview.png
[screenshot-03]:    https://gitlab.com/epsi-rns/tutor-html-materialize/raw/master/tutor-03/html-materialize-preview.png
[screenshot-04]:    https://gitlab.com/epsi-rns/tutor-html-materialize/raw/master/tutor-04/html-materialize-preview.png
[screenshot-05]:    https://gitlab.com/epsi-rns/tutor-html-materialize/raw/master/tutor-05/html-materialize-preview.png
[screenshot-06]:    https://gitlab.com/epsi-rns/tutor-html-materialize/raw/master/tutor-06/html-materialize-preview.png

