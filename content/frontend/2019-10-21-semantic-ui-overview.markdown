---
type   : post
title  : "Semantic UI - Overview"
date   : 2019-10-21T09:17:35+07:00
slug   : semantic-ui-overview
categories: [frontend]
tags      : [semantic ui, css]
keywords  : [plain html, custom theme, summary, overview]
author : epsi
opengraph:
  image: assets-frontend/2019/10/html-semantic-ui-preview.png

toc    : "toc-2020-03-repository"

excerpt:
  Experimental Semantic UI Material Design, step by step.

---

### Preface

> Goal: A Semantic UI with Custom Less Experiment.

Franken Semantic UI CSS step by step, from pure html+css, to less,
so any beginner can use this guidance.
Using custom spacing helper, color from materialize CSS,
and a little bit Bulma.

#### Repository

* [Semantic UI Test Drive][repository]

#### Custom Helper

* [Less - Loop - Spacing Class][custom-spacing]

* [Less - Conditional - Color Class][custom-colors]

-- -- --

### Chapter Step by Step

#### Tutor 01

> Adding Semantic UI CSS

* Without Semantic UI CSS

* Using CDN

* Using Local

![Tutor 01][screenshot-01]

#### Tutor 02

> Using Semantic UI: Navigation Bar

* Simple

* Full Featured (with jQuery)

* Using Icon (compatible with FontAwesome)

![Tutor 02][screenshot-02]

#### Tutor 03

> Custom Less

* Custom maximum width class

* Responsive using Semantic UI's

![Tutor 03][screenshot-03]

#### Tutor 04

> Helper Class

* Custom Less Spacing Helper 

* Custom Less Color Helper (Ported from Materialize CSS)

![Tutor 04][screenshot-04]

#### Tutor 05

> HTML Box using Semantic UI

* Main Blog and Aside Widget

![Tutor 05][screenshot-05]

#### Tutor 06

> Finishing

* Blog Post Example

![Tutor 06][screenshot-06]

-- -- --

### Begin The Guidance

Thank you for cloning the repository.

[//]: <> ( -- -- -- links below -- -- -- )

[repository]:       https://gitlab.com/epsi-rns/tutor-html-semantic-ui
[custom-spacing]:   {{< baseurl >}}/frontend/2019/10/03/less-loop-spacing-class/
[custom-colors]:    {{< baseurl >}}/frontend/2019/10/04/less-conditional-color-class/

[screenshot-01]:         https://gitlab.com/epsi-rns/tutor-html-semantic-ui//raw/master/step-01/html-semantic-ui-preview.png
[screenshot-02]:         https://gitlab.com/epsi-rns/tutor-html-semantic-ui//raw/master/step-02/html-semantic-ui-preview.png
[screenshot-03]:         https://gitlab.com/epsi-rns/tutor-html-semantic-ui//raw/master/step-03/html-semantic-ui-preview.png
[screenshot-04]:         https://gitlab.com/epsi-rns/tutor-html-semantic-ui//raw/master/step-04/html-semantic-ui-preview.png
[screenshot-05]:         https://gitlab.com/epsi-rns/tutor-html-semantic-ui//raw/master/step-05/html-semantic-ui-preview.png
[screenshot-06]:         https://gitlab.com/epsi-rns/tutor-html-semantic-ui//raw/master/step-06/html-semantic-ui-preview.png

