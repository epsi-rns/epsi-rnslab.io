---
type   : post
title  : "Practical Blog Development Cycle"
date   : 2020-03-21T09:17:35+07:00
slug   : practical-blog-cycle
categories: [frontend]
tags      : [css, ssg]
keywords  : [summary]
author : epsi
opengraph:
  image: assets-frontend/2019/10/scss-while-each-script.png

excerpt:
  How I Make my Blog.
  Porting CSS Framework.
  Porting SSG.
  Porting Deployment.

---

### Preface

> Goal: Explain The Cycle of Making My Blog

What?
A blog cycle?
Why makes things complicated?

Don't get scared, you can make a blog easily,
by copy-paste from my repository.
I have done the hard stuff for you.

#### How do I make my blog?

> Looking for the right tools?

I arrange the cycle as below:

1. Porting CSS Framework
2. Porting SSG
3. Porting CI/CD

![Blog Development Cycle][practical-cycle]

This is very similar to the oldschool web development task:

1. Frontend
2. Backend
3. Devops

#### Methodology

I never have any formal education related with IT.
So I never know about software development methodology such as,
`extreme programming`, `prototyping`, `waterfall`,
`scrum`, `agile` and so on.
I just did it, and I can't define my own method.
As long as it works, it is fine.

#### How do I start a blog project, as a beginner?

> Jekyll in Github, with any free theme

If you are a beginner,
I suggest you to use simplest stack for your project.
I use this stack a starting point.

Starter Tier: `any theme ➕ Jekyll ➕ github`.

![SSG Flow: Jekyll + Github][flow-jekyll-github]

If you like SSG, you may continue your journey.
Not everybody like SSG stack, some prefer LAMP stack.
I pick SSG to eliminate,
the burden of learning Javascript,
and the burden of learning backend.

#### What to learn First?

Welcome to new era of web development.
First thing to learn is of course, still the same old material

> HTML + CSS

And this one is a must

> Versioning with Git

Welcome to new era of web development
Basic `git` is a must have knowledge,
before you start any web development project.

-- -- --

### The Journey

This journey is my personal blog history.
You can may approach other steps that comfortable for you.

#### Narrative History

> There is always porting process. This takes experiences.

I just started it with my old template,
that is based on native custom CSS, put Bootstrap v3 on Jekyll, 
and move my article from my facebook pages to my blog.

It has been four years since my first commit on may 2016.
My blog grown from just one blog, split into two.
And today I have four blogs.

In that years span, I have been playing around with
CSS frameworks such as Bootstrap, Bulma, Semantic UI, Materialize CSS,
and continue with Custom SASS, LESS, and PostCSS.
No matter what I have been through,
I still feels like n00b when it comes to tailor made Tailwind CSS.

I also put a lot of effort to apply the result above,
to SSG (static site generator).
Jekyll v3 on github as a starting point,
to the super duper fast Hugo on gitlab.
Then Hexo on netlify.
Eleventy (11ty) also on netlify.
I ignore the Sculpin, as it is already a dead project.
I reject Zola for weird reason.
I failed to apply to VuePress, yeah I know I'm dumb.
And finally I choose the easier one, Pelican on bitbucket.
I never have time to learn Gatsby, nor NuxtJS, no NextJS, what a shame.

I remind that, every SSG have their own template.
And an SSGcould choose different template,
such as eleventy might use `nunjucks`, or `EJS`, or `liquid` or else.

After this I have to struggle with deployment.
Gitlab CI, Netlify, Zeit Now (vercel), Travis, Circle-ci
git refspec, git worktree, gh-action (github workflow), 
local git hook (.git/hooks).
I can't find a good day for myself to learn Jenkins.

> Can we summarize these?

From all of those process above,
I finally made my fourth blog with this specification.

1. CSS: Bulma Material Design
2. SSG: Pelican
3. CI/CD: BitBucket+CircleCI

#### Tier Combination

Example tier: `bulma ➕ pelican ➕ bitbucket`.

> How many ways to make a static blog?

The tier combination, and each choices is defined as below:

1. CSS Frameworks:
   At least five choices:
   native, bootstrap, bulma, materialize css, semantic ui, tailwind css.
   This exclude custom tailor made CSS as an extensions of chosen CSS framework.

2. Static Site Generator:
   At least ten SSGs choice:
   jekyll, hugo, hexo, pelican, eleventy (11ty), Zola, Sculpin
   And then JS framework based such as vuepress, nuxtjs, nextjs, gatsby.
   This exclude ant template combination, to simplify our tier.

3. CI/CD deployment:
   At least three kind repository to simplify our tier:
   github, gitlab dan bitbucket.

Now we can count the combination is at least: `5 x 10 x 3` ☺️.
Generally, there are at least 150 `tier` combination to build a blog.

> In short: research

![Blog Research Cycle][practical-cycle-text]

#### Mock-up Design

> What I did not do.

There should be a mock up before making a site theme.
It can be made with photoshop, or inkscape.
But I'm not a designer, and I don't know how to do it.
So I skip the whole design process.

If you need complex javascript, you should also consider,
choosing between the legacy jQuery, Vue, Svelte, React, and Angular.

#### In Short

> So, how is the cycle arrangement anyway?

1. Porting CSS Framework
2. Porting SSG
3. Porting Deployment

You can add mock-up design if you wish in the first place.

> So, what is the `porting` word means?

Making a blog is a continuous development because it is a hobby
without specific requirement.
There are always changes, and many times major changes,
such as porting form one old stuff to another.

> So what?

The first cycle require the deployment to be the last.
But after that, the porting does not have to be,
happened in ordered manners.

#### Not a Generalized Cycle

> Can I apply this cycle to other website making process?

This experience above is not a generalized cycle, for other use case.
For example, if what you want is to make company profile for other people,
making a mock-up i usually the first thing to do.
If you need to make dynamic website,
you need to have a real backend instead of just an SSG.
You also need to consider database, that require specific experience.

If you consider startup. That is usually a complex ecosystem,
such as using AWS S3, git collaboration, docker and such.

With this simple blog cycle example,
a beginner can step up, learning to make a site,
without the burden of backend along with database,
nor the burden of javascript.

-- -- --

### The Flow Inside The cycle

For each steps, I have made repository,
so you can pick anything which comfortable for your project.

I will list the repository in chronologial manners.
So I can explain the reason of making the repository.

You can see how the porting process do not have clear path.
Because the requirement of porting comes impulsively random.
Most comes as a side project.

![Making Blogs Timeline, an Example of Learning The Hard Way][practical-timeline]

As you can see, in 2019 there is no blog release.
All I did was just learning,
and finding the right ingredient for better theme.
Until I finally pour the the knowledge,
to a new theme with flat design style called `Bulma MD`.

Early 2020 is my DevOps period. Learning CI/CD, to deploy SSG.

#### Blog 1: Jekyll + Bootstrap + Github

I started to port my old theme from other project,
as a based for my first blog in Jekyll using Github.

* <https://github.com/epsi-rns/demo-jekyll>

The SSG flow is as below image:

![SSG Flow: Jekyll + Github][flow-jekyll-github]

You can see the old looks here:

* <https://epsi-rns.github.io/demo-jekyll/>

This blog mainly discuss about Linux/BSD Customization.

#### Blog 2: Hugo + Bootstrap + Gitlab

After a while I found that I have a lot of material,
and Jekyll become sluggish to build with hundreds of article.

So I splited the material by making my second blog in Gitlab.
I choose Hugo for the speed reason,
and started to port the boostrap theme from Jekyll to Hugo.

* <https://gitlab.com/epsi-rns/demo-hugo>

The SSG flow is as below image:

![SSG Flow: Any SSG + Gitlab CI][flow-many-gitlab]

You can see the old looks here:

* <https://epsi-rns.gitlab.io/demo-hugo/>

I also make a long tutorial about Hugo in Bootstrap. 

* <https://epsi-rns.gitlab.io/ssg/2018/09/01/hugo-summary/>

This blog mainly discuss about Web Development.

#### CSS Frameworks: Bulma

I need to open myself to broader knowledge by learning other CSS Frameworks.
I started with Bulma, because the dcoumentation seems to be easy to learn with.

* <https://gitlab.com/epsi-rns/tutor-html-bulma>

I felt like the Bulma have better looks, I rewrite the Hugo tutorial in Bulma.

* <https://epsi-rns.gitlab.io/ssg/2019/04/01/hugo-overview/>

I had not not switch my blog from Bootstrap to Bulma yet.
I need to make sure my knowledge is mature enough before doing major theme change.

#### Blog 3: Hexo + Bulma + Netlify

I began to be greedy as usual.
How about learning other SSG as well.
So I learnt Hexo, because my friend was a Hexo user at that time
And finally apply the Bulma design to Hexo.

* <https://gitlab.com/epsi-rns/tutor-hexo-bulma/>

The SSG flow is as below image:

![SSG Flow: Any SSG + Netlify][flow-many-netlify]

And I also experiment with netlify by making the third blog.
This blog mainly discuss about Small Business.

* <https://akutidaktahu.netlify.app/>

I also make a long tutorial about Hexo.

* <https://epsi-rns.gitlab.io/ssg/2019/05/01/hexo-overview/>

Just, after I release the Hexo tutorial, my friend switch to Gatsby.
Now I feel so alone.

#### CSS Frameworks: Materialize CSS

I decided to learn other CSS frameworks.

* <https://gitlab.com/epsi-rns/tutor-html-materialize>

Now I need a place that I can apply this design

#### SSG: Eleventy (11ty)

This one SSG had been on my radar for a long time,
but I havdn't have time to learn out it.
But hey, I finally made it.

* <https://gitlab.com/epsi-rns/tutor-11ty-materialize/>

I also test the live site in netlify

* <https://eleventy-step.netlify.app/>

Why netlify?
Because in that time, I had narrow mind. Netlify is all I know.

Eleventy is a good stuff. But for a while I do not use it for a blog.
Because I do not have any material content yet. This is not a blog.

#### Custom SASS: Bulma MD

What I learn is, Materialize have a good color pallete.
While bootstrap have good spacing helper.
And in that time I really like Bulma for simplicity reason.

> How do I solve this problem?

Of course by making custom SASS.

* <https://gitlab.com/epsi-rns/tutor-sass>

Along with the tutorial

* [sass-loop-spacing-class][sass-spacing]

[sass-spacing]: https://epsi-rns.gitlab.io/frontend/2019/06/21/sass-loop-spacing-class/

And apply both custom class to a whole new Bulma theme,
based on the previous Bulma theme example.

* <https://gitlab.com/epsi-rns/tutor-html-bulma-md>

I name the theme `Bulma Material Design` aka `Bulma MD` aka `FrankenBulma`.

#### SSG: Pelican

Why Zola?
Because I avoid Javascript.

I tried. But the project stalled.
I was busy with my business.

#### SSG: Zola

Why Zola?
Because I avoid Javascript.
I have this kind of fear, about learning Javascript.

I tried Zola. And I'm not happy with theme support.

#### SSG: Vuepress

I tried Vuepress. And I failed miserably with noone helping me.

> I feel dumb

#### CSS Frameworks: Semantic UI

Again I need a broader experience by learning something new.
Just in case I found something interesting,
to be added in my Bulma MD.

* <https://gitlab.com/epsi-rns/tutor-html-semantic-ui>

#### Applying Bulma MD

Allright, I feel confidence with the flat design.

Now it is time to apply the theme to each SSG,
just to see if this theme could works.

* <https://gitlab.com/epsi-rns/tutor-jekyll-bulma-md>

* <https://gitlab.com/epsi-rns/tutor-hugo-bulma-md>

Both works, but it takes for me to have a confidence to use it in my blog.

#### CSS Frameworks: Tailwind CSS

I tried Tailwind CSS. And I realize that it takes effort, guts and time.
So I decide to postpone.

#### SSG: Pelican

After see what I left of, I realize that I can do Pelican.
I know I'm own my own with noone to ask about Pelican.
But I did brave. And succeed.

* <https://gitlab.com/epsi-rns/tutor-pelican-bulma-md>

In that time, I am in require of a new blog.
and I decide to use either Pelican or Eleventy (11ty).

#### Again, Applying Bulma MD

One thing left, test my theme in 11ty.
Just in case I have to port my blog. Or use it for my new blog.

* <https://gitlab.com/epsi-rns/tutor-11ty-bulma-md>

#### Blog 4: Pelican + Bulma MD + Bitbucket

I decide to use Python based Pelican,
because the content material of the new blog takes a lot of python script.

I also decide to use bitbucket, because I already use gitlab, github and netlify. 

But there is an obstacle, making a bitbucket site for use with Pelican is sooo hard to do.

> I took back my smile.

#### Deploying: CI/CD

Actually it is easy now to build a site with bitbucket.
Because I found the tricks.

I takes about three months to learn general devops deployment.
After threemoths I found that there are many ways to deploy SSG.

0. Gitlab CI
1. Netlify
2. Zeit Now
3. Manual git refspec
4. Manual git worktree
5. Travis (github only, bitbucket)
6. Circle-ci + github (git worktree)
7. Circle-ci + bitbucket (git worktree)
8. gh-action (github workflow)
9. local git hook (.git/hooks)
10. Jenkins (self hosted) (not tried yet)

There many kind of flow, one of them is travis, as below image:

![SSG Flow: Any SSG + Travis][flow-many-travis]

#### Blog 4: Pelican + Bulma MD + Bitbucket

As I understand how to deploy in bitbucket,
I continue my works to build my fourth blog.
And succeeded.

* <https://bitbucket.org/epsi/epsi.bitbucket.io/>

The SSG flow is as below image:

![SSG Flow: Any SSG + CircleCI][flow-many-circleci]

And you can see the result here

* <https://epsi.bitbucket.io/>

By this time of writing,
there is only a few content material in that blog.

#### Blog 2: Hugo + Bulma MD + Gitlab

Now That I finished with my fourth blog,
I have confidence (and time) to change my first blog,
from Bootstrap to Bulma MD.

* <https://gitlab.com/epsi-rns/epsi-rns.gitlab.io>

Now the looks is more modern.

* <https://epsi-rns.gitlab.io/>

#### Blog 1: Jekyll + Bulma + Github

Why don't go further with first Blog that looks so modest (read: old).

* <https://github.com/epsi-rns/epsi-rns.github.io>

Now the looks is more modern.

* <https://epsi-rns.github.io/>

#### Summary

As a summary we can list all the common flows as below figure:

![SSG Flow: Summary][flow-summary]

#### What's Next

> I think that is all!

The cycle is not always linier.
It is not just jump between CSS and SSG,
but also jump between blog and deployment.

There are a lot of thing that I want to do,
but I still do not know if I could spare time for this good material,
such as:

1. Port `google material color` to `open color`.
   And remade my old unused bootstrap theme,
   with flat design using `open color`.
   There is always a demand about bootstrap theme tutorial guidance,
   and I think I can do it.

2. I desire to apply this bootstrap theme above in Eleventy.
   But, this time utilizing `PUG` instead of nunjucks.

3. There is also chance that,
   I will remake my theme to `Tailwind CSS` with similar looks.

4. My `Jekyll` blog is still sluggish as usual.
   But I don't know what to do with it.
   I don't know if port to eleventy helps the increase the speed.
   I have to make test, nad now I simply do not have time.

5. I currently do not have any plan to change the third blog on netlify.
   My priority with this blog is content.
   A ready to use material in my head needs to be poured to this good blog.

#### Conclusion

Yes you can also build your own blog.
The cycle is as flexible as you are.

> Be flexible!

#### Finally

Thank you for cloning the repository.

[//]: <> ( -- -- -- links below -- -- -- )

[practical-timeline]:   {{< assets-frontend >}}/2020/03/practical-blog.png
[practical-cycle]:      {{< assets-frontend >}}/2020/03/blog-development-cycle.png
[practical-cycle-text]: {{< assets-frontend >}}/2020/03/blog-development-cycle-with-text.png

[flow-jekyll-github]:   {{< assets-frontend >}}/2020/03/ssg-flow-jekyll-github.png
[flow-many-gitlab]:     {{< assets-frontend >}}/2020/03/ssg-flow-many-gitlab.png
[flow-many-netlify]:    {{< assets-frontend >}}/2020/03/ssg-flow-many-netlify.png
[flow-many-travis]:     {{< assets-frontend >}}/2020/03/ssg-flow-many-travis.png
[flow-many-circleci]:   {{< assets-frontend >}}/2020/03/ssg-flow-many-circleci.png
[flow-summary]:         {{< assets-frontend >}}/2020/03/ssg-flow-summary.png
