---
type   : post
title  : "Starting Web Development"
date   : 2020-10-03T13:13:35+07:00
slug   : starting-web-development
categories: [frontend]
tags      : [presentation, css]
keywords  : [css framework, css preprocessor, advantage, disadvantage]
author : epsi
opengraph:
  image: assets-frontend/2020/04/css-concept-page.png

excerpt:
  A journey guidance on how to be a web developer for beginner.
---

> Goal: A journey guidance on how to be a web developer for beginner.

So you want to make something matters in your life?
You decide to be a web developer.
How should I start?

-- -- --

### Role

First, define your role.

* Do you want to make something? or,

* Do you want somebody to make something for you?

#### I want to be a maker.

Then you should learn.

#### I want someone to make it for me.

Then you should hire an employee.
Or buy an application from a software house.

#### The Roadmap

[roadmap.sh][roadmap.sh] states clearly these different roles.

* Frontend

* Backend

* Devops

You can combine these three into full stack web development.

Just be aware that company that wants a fullstack web developer,
would looks like having small budget.

#### Mobile

You should aware that mobile development is,
different with web development.

-- -- --

#### Where to Learn?

If you are beginner, you should read this first.

* [w3schools.com](https://w3schools.com)

* [roadmap.sh](http://roadmap.sh/)

My first attempt is,
always ask beginner to read [w3schools.com](https://w3schools.com),
and for not so beginner I ask them to
examine diagram in [roadmap.sh](http://roadmap.sh/).
But there are more if you wish.

* [css-tricks.com](https://css-tricks.com/)

* [Style Guide](https://google.github.io/styleguide/htmlcssguide.html)

* [Concept CSS](https://epsi-rns.gitlab.io/frontend/2020/10/11/slides-concept-css/)

If you desire step-by-step guidance,
[youtube](https://www.youtube.com/) is your friendly neighbourhood.
There is a lot of good channel you can watch over there,
both english and local.

-- -- --

### Approach

> The stack: `html+css+js`.

#### What is A Web Page?

A web page is simply:

1. Structure + Presentation + Behaviour

2. Custom User Content: Text and Image (or other media).

![What is a page?][concept-page]

The first one, is a stack we know as `html+css+js`.
The second one is any material we want to stuff to this document page.

#### Technically

> How do I explain from basic stuff to modern era?

![HTML + CSS + JS][html-css-js]

#### The Hidden Tiers

> No complete templating guidance

![Templating Engine and Preprocessor][preprocessor]

Yes, there is a gap in most web development tutorial.

-- -- --

### 0: Must Have Basic Knowledge.

> Find a group, instead of private conversation.

1. English knowledge is a must.
2. Know how to search google and stackoverflow.
3. Read The Fine Manual.
4. Know how to make a screenshot in a group.
5. Know how to communicate nicely in a group.

Tips: Make a note regularly.

_read, write, share_

-- -- --

### 1. HTML

> It is easier to learn plain HTML, with template engine approach.

HTML5 is a broad topic. It is not just about HTML tag.
Consider learn only the basic stuff,
then practice by directly applying simple HTML,
in your own simple project.
A blog or portfolio is a good choice of simple project,
since a blog has no burden of complex database,
nor login stuff.

![Slide - Preface: Learning HTML Step by Step][learning-html]

The steps is simply as below:

1. Basic HTML
   * [w3schools.com/html/](https://www.w3schools.com/html/)

2. Templating Engine
   * Combine with Task Runner.

3. Static Site Generator
   * Complete website, without complicated backend.

4. Backend Web Framework
   * Laravel, Flask, RoR, Koa, Express.

5. Modern Web Framework
   * Svelte, React, Vue.

> Learn how to google, make a screenshot, read documentation, and english.

Tips:

* Hit the w3school book first.

* Enjoy the journey.

_template engine is backend stuff._

-- -- --

### 2. Stylesheet

> The hidden tier is the CSS Preprocessors.

You can learn to use CSS Frameworks,
then learning CSS Preprocessors,
then go back later to customize CSS Frameworks.

![Slide - Preface: Learning CSS Step by Step][learning-css]

The steps is simply as below:

1. Basic CSS
   * [w3schools.com/css/](https://www.w3schools.com/css/)

2. CSS Frameworks
   * Such as Bulma, Materialize CSS, or Semantic UI.

3. CSS Preprocessors
   * To create additional custom CSS.

4. CSS Tools
   * Task Runner, or
   * Bundler.

5. Custom CSS
   * No Frameworks, or
   * Tailwind CSS.

> Learn how to google, make a screenshot, read documentation, and english.

Tips:

* Hit the w3school book first.

* Enjoy the journey.

_stylesheet is frontend stuff._

-- -- --

### 3. Javascript

> I'l be back.

Javascript is a huge topic.
I haven't got any material yet.
I mean, I have plan for this javascript material,
but I'm still not sure about the proper journey for beginner.
So I'd better skip for a while.

-- -- --

### 4. Content

For your simple project, such as blogging or portfolio you need to.

1. Text
   * Learn about writing.
   * Proper english, or local language.

2. Image
   * Capture photo with your smartphone
   * Raster illustration: Learn GIMP
   * Vector illustration: Learn Inkscape

-- -- --

### Finally

Understand the `concept`, instead of just `how-to` do something.

Have fun with coding.

[//]: <> ( -- -- -- links below -- -- -- )

[roadmap.sh]: https://github.com/kamranahmedse/developer-roadmap/blob/master/img/intro.png

[html-css-js]:      {{< assets-frontend >}}/2020/04/html-css-js.png
[preprocessor]:     {{< assets-frontend >}}/2020/04/preprocessor.png
[concept-page]:     {{< assets-frontend >}}/2020/04/css-concept-page.png

[learning-html]:    {{< assets-frontend >}}/2020/preface-learning-html-step-by-step.png
[learning-css]:     {{< assets-frontend >}}/2020/preface-learning-css-step-by-step.png
