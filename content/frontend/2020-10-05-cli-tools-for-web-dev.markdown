---
type   : post
title  : "CLI Tools for Web Development"
date   : 2020-10-05T13:13:35+07:00
slug   : cli-tools-for-web-development
categories: [frontend]
tags      : [cli, npm]
keywords  : [babel, eslint, stylelint, autoprefixer, cssnano]
author : epsi
opengraph:
  image: assets/posts/frontend/2020/10-cli/03-postcss-cssnano.png

excerpt:
  Introducing terminal as web development tools for beginner.
---

### Preface

> Goal: Introducing terminal as web development tools for beginner.

Just in case you haven't notice,
a must have skill for web development is, terminal skill.
CLI is a must have tools for coding.

#### Source Examples

You can obtain source examples here:

* [/dev-tools][source-example]

#### For Beginner

> Terminal is a must have knowledge for modern web development.

There are a lot of `node package` that suit this situation.
Most of the tools can be bundled into javascript
that run in browser without the need of terminal.
It is still recommended to learn basic stuff using CLI.

This article arranged as below:

* Stylesheet
  * 1: Stylelint
  * 2: PostCSS: Autoprefixer
  * 3: PostCSS: CSSnano

* Ecmascript
  * 4: Node itself
  * 5: Eslint
  * 6: Babel
  * 7: Terser

Of course the are other tools as well such as:
`task runner`, `bundler`, `CSS Preprocessor`,
`html preprocessor` or even `testing tools`.
But this is beyond the scope of beginner.

There are also tons of Node packages,
But I limit for few packages only,
as introduction to terminal.

I also limit this article for:
native stylesheet, and vanilla ecmascript.
My intention is showing how important it is,
in modern web development era,
for beginner to embrace terminal.

-- -- --

### 1: Stylesheet: Stylelint

This `stylelint` is useful to check stylesheet rules.

#### Reference: Official Site

* <https://stylelint.io/>

#### Install

{{< highlight bash >}}
$ npm i -s -D stylelint stylelint-config-standard
+ stylelint@13.8.0
+ stylelint-config-standard@20.0.0
updated 3 packages and audited 266 packages in 39.72s
{{< / highlight >}}

![Stylelint: NPM Install][01-stylelint-install]

#### Example Stylelint Configuration

> Do not get intimidated by long configuration!

Suppose that you decide to use `rem` unit instead of `px`,
and decide to run check whether your stylesheet is,
following the new rule.
You can `copy-paste`,
this ready to use configuration below:

* [gitlab.com/.../stylelint/.stylelintrc.json][01-config-stylelintrc]

{{< highlight javascript >}}
{
  "extends": "stylelint-config-recommended",
  "rules": {
    "at-rule-no-unknown": [
      true,
      {
        "ignoreAtRules": ["extends"]
      }
    ],
    "block-no-empty": null,
    "unit-disallowed-list": [
      "px",
      {
        "ignoreProperties": {
          "px": ["/^border/", "/^transform/"]
        },
        "ignoreMediaFeatureNames": {
          "px": ["min-width"]
        }
      }
    ]
  }
}
{{< / highlight >}}

![Stylelint: Configuration][01-stylelint-config]

#### Example Source

Consider checking this stylesheet as below:

* [gitlab.com/.../stylelint/basic-layout.css][01-example-source]

{{< highlight bash >}}
/* Main Layout: Mobile First */

.tabs {
  display: flex;
  flex-direction: column;
}

.tabs * {
  box-sizing: border-box;
}

@media (min-width: 768px) {
  .tabs {
    flex-direction: row;
  }
  
  .tabs>* {
    min-height: 320px;
  }
}
{{< / highlight >}}

#### Example Usage

You can check validity rules of your stylesheet as below:

{{< highlight bash >}}
$ npx stylelint basic-layout.css

basic-layout.css
 18:17  ✖  Unexpected unit "px"   unit-disallowed-list
{{< / highlight >}}

![Stylelint: Usage][01-stylelint-usage]

-- -- --

### 2: Stylesheet: PostCSS: Autoprefixer

This is a `postcss` plugin.
This `autoprefixer` is useful,
to maintain compatibility with old browser.

* <https://autoprefixer.github.io/>

#### Install

You can install `postcss` globally.

{{< highlight bash >}}
$ npm i -s -g postcss
+ postcss@8.2.0
updated 1 package in 1.247s
{{< / highlight >}}

And the rest in respective local folder.

{{< highlight bash >}}
$ npm i -s -D postcss-cli autoprefixer
+ postcss-cli@8.3.0
+ autoprefixer@10.1.0
updated 2 packages and audited 106 packages in 9.555s
{{< / highlight >}}

![PostCSS: Autoprefixer: NPM Install][02-autoprefixer-inst]

#### Example PostCSS Configuration

You can simply put the `autoprefixer` in `postcss.config.js`.

* [gitlab.com/.../autoprefixer/postcss.config.js][02-config-postcss]

{{< highlight javascript >}}
module.exports = {
  plugins: {
    autoprefixer: {}
  }
}
{{< / highlight >}}

#### Example Source

Consider checking this stylesheet as below:

* [gitlab.com/.../autoprefix/example.css][02-example-source]

{{< highlight css >}}
.example {
    display: grid;
    transition: all .5s;
    user-select: none;
    background: linear-gradient(to bottom, white, black);
}
{{< / highlight >}}

This example is taken from

* <http://autoprefixer.github.io/>

#### Example Usage

{{< highlight css >}}
$ postcss --verbose example.css
Processing example.css...
.example {
    display: grid;
    transition: all .5s;
    -webkit-user-select: none;
       -moz-user-select: none;
        -ms-user-select: none;
            user-select: none;
    background: linear-gradient(to bottom, white, black);
}
Finished example.css in 923 ms
{{< / highlight >}}

![PostCSS: Autoprefixer: Usage][02-autoprefixer-usage]

-- -- --

### 3: Stylesheet: PostCSS: CSSnano

This `CSSnano` is useful for CSS minification.

* <https://cssnano.co/>

#### Install

Very similar with `autoprefixer`.

{{< highlight bash >}}
❯ npm i -s -D postcss-cli cssnano
+ postcss-cli@8.3.0
+ cssnano@4.1.10
added 86 packages from 62 contributors, updated 1 package and audited 281 packages in 29.188s
{{< / highlight >}}

![PostCSS: CSSnano: NPM Install][03-cssnano-install]

#### Example PostCSS Configuration

You can simply put the `cssnano` in `postcss.config.js`.

* [gitlab.com/.../cssnano/postcss.config.js][02-config-postcss]

{{< highlight html >}}
// postcss.config.js
const cssnano = require('cssnano')

module.exports = {
  plugins: {
    cssnano: { preset: 'default' }
  }
}
{{< / highlight >}}

#### Example Source

Consider checking this stylesheet as below:

* [gitlab.com/.../cssnano/basic-layout.css][03-example-source]

{{< highlight css >}}
/* Main Layout: Mobile First */

.tabs {
  display: flex;
  flex-direction: column;
}

.tabs * {
  box-sizing: border-box;
}

@media (min-width: 768px) {
  .tabs {
    flex-direction: row;
  }
  
  .tabs>* {
    min-height: 20rem;
  }
}

…
{{< / highlight >}}

#### Example Usage

{{< highlight html >}}
❯ postcss --verbose basic-layout.css
Processing basic-layout.css...
.tabs{display:flex;flex-direction:column}…
{{< / highlight >}}

![PostCSS: CSSnano: Usage][03-cssnano-usage]

-- -- --

### 4: Ecmascript: Node itself

Learning Ecmascript using DOM in inspect element,
can be a daunting task. 
On the other hand, using terminal is far easier,
if you just dare to embrace CLI.

Start using terminal instead,
it is more clear with clean code,
without HTML burden.

#### Example Ecmascript Source

* [gitlab.com/.../babel/songs.js][06-example-source]

{{< highlight javascript >}}
const songs  = [
  { title: "Cantaloupe Island",          tags: ["60s", "jazz"] },
  { title: "Let it Be",                  tags: ["60s", "rock"] },
  { title: "Knockin' on Heaven's Door",  tags: ["70s", "rock"] },
  { title: "Emotion",                    tags: ["70s", "pop"] },
  { title: "The River" }
];

let tagSet = new Set();

// using set feature to collect tag names
songs.forEach(function(song) {
  if( "tags" in song ) {   
    let tags = song.tags;
    console.log(tags);

    for (const tag of tags) {
      tagSet.add(tag);
    }
  }
});

console.log(tagSet);

// normalize to array
let alltags = [...tagSet];

console.log(alltags);
{{< / highlight >}}

#### Example Usage

Yes, you can run `ecmascript`, directly from terminal using `node`.

{{< highlight bash >}}
❯ node songs.js
[ '60s', 'jazz' ]
[ '60s', 'rock' ]
[ '70s', 'rock' ]
[ '70s', 'pop' ]
Set(5) { '60s', 'jazz', 'rock', '70s', 'pop' }
[ '60s', 'jazz', 'rock', '70s', 'pop' ]
{{< / highlight >}}

![NodeJS: Run Ecmascript Directly Using Terminal][04-node-usage]

-- -- --

### 5: Ecmascript: Eslint

This `eslint` is useful to check potential issue in `ecmascript`.

* <https://eslint.org/>

#### Install

{{< highlight bash >}}
❯ npm i -s -D eslint
+ eslint@7.15.0
updated 1 package and audited 109 packages in 13.089s
{{< / highlight >}}

![Eslint: NPM Install][05-eslint-install]

#### Example Eslint Configuration

You can `copy-paste`,
this ready to use configuration below:

* [gitlab.com/.../eslint/.eslintrc.json][05-config-eslintrc]

{{< highlight html >}}
{
    "env": {
        "browser": true,
        "es2021": true
    },
    "extends": "eslint:recommended",
    "parserOptions": {
        "ecmaVersion": 12,
        "sourceType": "module"
    },
    "rules": {
    }
}
{{< / highlight >}}

#### Example Usage: Case 1

Suppose that you have this code below:

* [gitlab.com/.../eslint/song-01.js][05-example-case-01]

{{< highlight javascript >}}
const song  = { title: "Cantaloupe Island", tags: ["60s", "jazz"] };
{{< / highlight >}}

You can check the code using `eslint` in terminal.

{{< highlight html >}}
$ npx eslint song-01.js

/home/epsi/Documents/dev-tools/eslint/song-01.js
  1:7  error  'song' is assigned a value but never used  no-unused-vars

✖ 1 problem (1 error, 0 warnings)
{{< / highlight >}}

![Eslint: Usage: Case 1][05-eslint-usage-01]

#### Example Usage: Case 2

Suppose that you want to,
disable checking for one line as this code below:

* [gitlab.com/.../eslint/song-02.js][05-example-case-02]

{{< highlight javascript >}}
/* eslint-disable no-undef */
song  = { title: "Cantaloupe Island", tags: ["60s", "jazz"] };

console.log(song);
{{< / highlight >}}

You can check the code using `eslint` in terminal.

{{< highlight html >}}
$ npx eslint song-02.js
{{< / highlight >}}

* There will nothing happened, because the code has no issue.

![Eslint: Usage: Case 2][05-eslint-usage-02]

-- -- --

### 6: Ecmascript: Babel

This `babel` is useful, to maintain compatibility, 
with browser with old javascript support.

* <https://babeljs.io/>

#### Install

{{< highlight bash >}}
❯ npm i -s -D @babel/core @babel/cli @babel/preset-env @babel/polyfill
+ @babel/core@7.12.9
+ @babel/cli@7.12.8
+ @babel/preset-env@7.12.7
+ @babel/polyfill@7.12.1
added 160 packages from 74 contributors, updated 2 packages and audited 383 packages in 19.084s
{{< / highlight >}}

![Babel: NPM Install][06-babel-install]

#### Example Configuration

By using `presets`, there is no need for configuration.
for example `@babel/preset-env` will convert the `ecmascript` to `ES5`.

Flexibility, you can have multiple configuration.
For example if you do not want to disable aading `use strict`,
as shown in code as below:

* [gitlab.com/.../babel/babel.es5.config.json][06-babel-config]

{{< highlight javascript >}}
{
  "presets": ["@babel/preset-env"],
  "plugins": ["remove-use-strict"]
}
{{< / highlight >}}

![Babel: Configuration][06-babel-config]

#### Example Source

Consider using previous javasscript:

* [gitlab.com/.../babel/songs.js][06-example-source]

![Babel: Example Ecmascript Source][06-babel-source]

#### Example Usage

You can either use presets:

{{< highlight bash >}}
$ babel songs.js --presets @babel/preset-env
{{< / highlight >}}

Or use configuration

* [gitlab.com/.../babel/babel.es5.config.json][06-config-babel-es5]

{{< highlight bash >}}
$ babel songs.js --config-file ./babel.es5.config.json
{{< / highlight >}}

And finally write directly to file

{{< highlight bash >}}
$ babel songs.js --presets @babel/preset-env > songs.es5.js
{{< / highlight >}}

#### Example Result

The result will contain two parts:

1. A bunch of newly functions to maintain compatibility in above.
2. Altered code, based on the original code.

Both in the same file.

* Functions:

{{< highlight javascript >}}
function _toConsumableArray(arr) { … }

function _nonIterableSpread() { … }

function _iterableToArray(iter) { … }

function _arrayWithoutHoles(arr) { … }

…
{{< / highlight >}}

_

![Babel: Example Ecmascript Result: Function][06-babel-function]

* Code:

{{< highlight javascript >}}
…
songs.forEach(function (song) {
  if ("tags" in song) {
    var tags = song.tags;
    console.log(tags);

    var _iterator = _createForOfIteratorHelper(tags),
        _step;

    try {
      for (_iterator.s(); !(_step = _iterator.n()).done;) {
        var tag = _step.value;
        tagSet.add(tag);
      }
    } catch (err) {
      _iterator.e(err);
    } finally {
      _iterator.f();
    }
  }
});
…
{{< / highlight >}}

_

![Babel: Example Ecmascript Result: Code Content][06-babel-content]

#### Running The Result

You can run the result with `node`.

* [gitlab.com/.../babel/songs.es5.js][06-example-result]

{{< highlight bash >}}
❯ node songs.es5.js
{{< / highlight >}}

#### Tracking Error

If the code does not works,
you should check the original code with `eslint`.
For example if you forget to add the `const` keyword,
before the `songs` name,
the `babel` will successfully translate to `ES5`.
But the code cannot be run under the `use strict`,
in newly created `ecmascript`.

{{< highlight javascript >}}
songs  = [
  …
];
{{< / highlight >}}

-- -- --

### 7: Ecmascript: Terser

This `terser` is `uglify-js` for `es6+`.

* <https://github.com/terser/terser>

#### Install

You can install `terser` globally.

{{< highlight bash >}}
❯ npm i -s -g terser
/home/epsi/.npm-global/bin/terser -> /home/epsi/.npm-global/lib/node_modules/terser/bin/terser
+ terser@5.5.1
added 6 packages from 38 contributors in 2.941s
{{< / highlight >}}

#### Example Usage

{{< highlight bash >}}
$ terser --compress --mangle -- songs.js
{{< / highlight >}}

The result is a minified `ecmascript`.

![Terser: Example Result: Compressed Ecmascript][07-terser-vim-songs]

-- -- --

### Finally

I think that is all.

Have fun with terminal.

[//]: <> ( -- -- -- images below -- -- -- )

[01-stylelint-install]: {{< baseurl >}}assets/posts/frontend/2020/10-cli/01-isd-stylelint.png
[01-stylelint-config]:  {{< baseurl >}}assets/posts/frontend/2020/10-cli/01-stylelintrc-json.png
[01-stylelint-usage]:   {{< baseurl >}}assets/posts/frontend/2020/10-cli/01-stylelintrc-basic-layout.png

[02-autoprefixer-inst]: {{< baseurl >}}assets/posts/frontend/2020/10-cli/02-isd-autoprefixer.png
[02-autoprefixer-usage]:{{< baseurl >}}assets/posts/frontend/2020/10-cli/02-postcss-autoprefixer.png

[03-cssnano-install]:   {{< baseurl >}}assets/posts/frontend/2020/10-cli/03-isd-cssnano.png
[03-cssnano-usage]:     {{< baseurl >}}assets/posts/frontend/2020/10-cli/03-postcss-cssnano.png

[04-node-usage]:        {{< baseurl >}}assets/posts/frontend/2020/10-cli/04-node-source.png

[05-eslint-install]:    {{< baseurl >}}assets/posts/frontend/2020/10-cli/05-isd-eslint.png
[05-eslint-usage-01]:   {{< baseurl >}}assets/posts/frontend/2020/10-cli/05-eslint-case-01.png
[05-eslint-usage-02]:   {{< baseurl >}}assets/posts/frontend/2020/10-cli/05-eslint-case-02.png

[06-babel-install]:     {{< baseurl >}}assets/posts/frontend/2020/10-cli/06-isd-babel.png
[06-babel-config]:      {{< baseurl >}}assets/posts/frontend/2020/10-cli/06-babel-es5-config.png
[06-babel-content]:     {{< baseurl >}}assets/posts/frontend/2020/10-cli/06-babel-songs-presets-env-content.png
[06-babel-function]:    {{< baseurl >}}assets/posts/frontend/2020/10-cli/06-babel-songs-presets-env-function.png
[06-babel-source]:      {{< baseurl >}}assets/posts/frontend/2020/10-cli/06-babel-songs-source.png

[07-terser-vim-songs]:  {{< baseurl >}}assets/posts/frontend/2020/10-cli/07-terser-songs.png

[//]: <> ( -- -- -- links below -- -- -- )

[source-example]:       {{< tutor-ecmascript >}}/dev-tools/

[01-config-stylelintrc]:{{< tutor-ecmascript >}}/dev-tools/stylelint/.stylelintrc.json
[01-example-source]:    {{< tutor-ecmascript >}}/dev-tools/stylelint/basic-layout.css

[02-config-postcss]:    {{< tutor-ecmascript >}}/dev-tools/autoprefixer/postcss.config.js
[02-example-source]:    {{< tutor-ecmascript >}}/dev-tools/autoprefixer/example.css

[03-config-postcss]:    {{< tutor-ecmascript >}}/dev-tools/cssnano/postcss.config.js
[03-example-source]:    {{< tutor-ecmascript >}}/dev-tools/cssnano/basic-layout.css

[05-config-eslintrc]:   {{< tutor-ecmascript >}}/dev-tools/eslint/.eslintrc.json
[05-example-case-01]:   {{< tutor-ecmascript >}}/dev-tools/eslint/song-01.js
[05-example-case-02]:   {{< tutor-ecmascript >}}/dev-tools/eslint/song-02.js

[06-config-babel-es5]:  {{< tutor-ecmascript >}}/dev-tools/babel/babel.es5.config.json
[06-example-source]:    {{< tutor-ecmascript >}}/dev-tools/babel/songs.js
[06-example-result]:    {{< tutor-ecmascript >}}/dev-tools/babel/songs.es5.js
