---
type   : post
title  : "Slides - Concept CSS"
date   : 2020-10-11T13:13:35+07:00
slug   : slides-concept-css
categories: [frontend]
tags      : [presentation, css]
keywords  : [css framework, css preprocessor, advantage, disadvantage]
author : epsi
opengraph:
  image: assets-frontend/2020/04/css-concept-page.png

excerpt:
  Presentation of CSS concept.
  Introduction to CSS framework and CSS preprocessor.

---

### The Document

This article contain all article slides.
Slide by slide, so anyone can copy desired slide,
without the need to open the Impress slide, or the PDF version.

![Teaser Preview: Concept CSS][teaser-preview]

I believe, a ready to use slide,
is useful to help people in the group discussion.

#### Original Presentation

I usually draft a presentation in a simple text based,
that I can write down in any text editor.

Then I show in web based for a few moment (actually months).
This give time to append any additional material freely,
create necessary figure, and also correct any typo, or even dead links.

You can watch the presentation here:

* [Presentation - Concept CSS][local-presentation]

This text based presentation is the most authentic version,
with all the links required provided.

#### Impress

This presentation is made in [LibreOffice Impress][libreoffice-impress],
using [candyclone template][candyclone-template].

You can download the Impress document from a page in here:

* [presentation-concept-css-diagram.odp][document-pdf]

With this downloadable document,
you can use the style of this presentation,
for use with your own presentation works,
either for school material, office works, homeworks, or anything else.

#### PDF

You can also download the exported pdf from a page in here:

* [presentation-concept-css-diagram.pdf][document-impress]

The pdf version cannot contain animation.
So the content might be little different,
compared with the original Impress document.

#### Inkscape

The Impress pages also utilize diagram illustration made in Inkscape.
I also provide the source image in SVG format,
that you can download from a page in here:

* [presentation-concept-css-content.svg][document-inkscape]

Here is the preview.

![Inkscape Illustration: Concept CSS: Thumbs Misc][inkscape-thumbs-misc]

![Inkscape Illustration: Concept CSS: Thumbs Frame][inkscape-thumbs-frame]

![Inkscape Illustration: Concept CSS: Thumbs Drops][inkscape-thumbs-drops]

Or better, you can get it all from candyclone illustration:

* [template-candyclone-content.svg][candyclone-content]

I intentionally share the source SVG image,
so you can alter the content for your own personal use.
I know there are already so many stock images for presentation,
I just need to share, what I have, with tutorial.
This way, anyone can make pretty presentation easier.
Of course you still have to learn Inkscape,
to suit the illustration for your own use.

#### Template

> What is this candyclone rubbish?

Candyclone is an Impress template that I have made for LibreOffice contest.

* [LibreOffice Impress Template Contest by the Indonesian Community][template-contest]

There are also other free templates as well in `lumbung` repository.

* [Lumbung LibreOffice Indonesia Repository][template-lumbung]
 
This candyclone is just an example template that you can use freely.
With this candyclone example you can also learn how to make your own template.
Your very own template to suit your ecosystem.

#### Disagreement

> What if I do not agree?

The source is available, so you can freely make your own slide.
Feel free to express your thoughts, either with text, or illustration.

-- -- --

### The Slides

Here I represent all the slides.

#### Slide 01: Cover

![Slide - Cover][slide-01]

> Frameworks and Tools

Introduction to CSS for beginner

#### Slide 02: About The Author

![Slide - About Author][slide-02]

> I have my own blog.

#### Slide 03: About This Material

![Slide - Preface: HTML+CSS+JS][slide-03]

> Frontend: HTML, CSS, JS

What is this rubbish?

#### Slide 04: Preface: HTML Document

![Slide - Preface: HTML Document][slide-04]

More Reference:

* [va_lesson4.pdf](http://didawiki.cli.di.unipi.it/lib/exe/fetch.php/magistraleinformaticaeconomia/va/2016/va_lesson4.pdf)

A page is a document consist of:

1. HTML Tag: Structure and Formatting
   * (structure = elements in hierarcy fashioned)

2. Stylesheet: Presentation
   * (rule = selector + declaration)
   * (box model, position)
   * (layout: flex, grid, float+calc, table)

3. Javascript: Behaviour

4. Content:
   * Text and Image (or other media)
   
#### Slide 05: Learning HTML Step by Step?

![Slide - Preface: Learning HTML Step by Step][slide-05]

Learning HTML Step by Step?

1. Basic HTML
   * [w3schools.com/html/](https://www.w3schools.com/html/)

2. Templating Engine
   * Combine with Task Runner.

3. Static Site Generator
   * Complete website, without complicated backend.

4. Backend Web Framework
   * Laravel, Flask, RoR, Koa, Express.

5. Modern Web Framework
   * Svelte, React, Vue.

> Learn how to google, make a screenshot, read documentation, and english.

Tips:

* Hit the w3school book first.

* Enjoy the journey.

#### Slide 06: Learning CSS Step by Step?

![Slide - Preface: Learning CSS Step by Step][slide-06]

Learning CSS Step by Step?

1. Basic CSS
   * [w3schools.com/css/](https://www.w3schools.com/css/)

2. CCC Frameworks
   * Such as Bulma, Materialize CSS, or Semantic UI.

3. CSS Preprocessors
   * To create additional custom CSS.

4. CSS Tools
   * Task Runner, or
   * Bundler.

5. Custom CSS
   * No Frameworks, or
   * Tailwind CSS.

> Learn how to google, make a screenshot, read documentation, and english.

Tips:

* Hit the w3school book first.

* Enjoy the journey.

#### Slide 07: Where to Put Stylesheet?

![Slide - Preface: Placement][slide-07]

Where to put stylesheet?

1. Inline,
2. Internal, or
3. External.

Yet another humble oldschool case.

* [HTML - Stylesheet]({{< baseurl >}}frontend/2020/04/02/html-stylesheet/)

#### Slide 08: Chapter Break

![Slide - CSS Frameworks][slide-08]

#### Slide 09: Issue in oldschool CSS Development

![Slide - CSS Frameworks: Oldschool Issue][slide-09]

Issue in `oldschool` CSS Development.

* Reinvent the wheel.
  * Different stylesheet for each project.
  * In need of code reuse.

* Debugging. Most of the time.
  * The horror while testing in different browser.

* No standard for teamwork.
  * Waste of time, thinking of nice name for classes.

* Can we have a generic one?

#### Slide 10: Why CSS Framework?

![Slide - CSS Frameworks: Why CSS Frameworks?][slide-10]

Why CSS Framework?

* Save man hours.
  * Deliver preview quickly.

* Reduce debugging and test.
  * Already well tested. Hence reduce bug.

* Ready to read, official documentation

* Community Friendly
  * Known solution for recurring case.

* Modularity
  * Built on top of CSS preprocessor: Sass, Less, or Stylus,
  * Tailwind on top of PostCSS even more modular.

#### Slide 11: Direct Advantage to Developer

![Slide - CSS Frameworks: Direct Advantage][slide-11]

Direct advantage to developer

* Easy to layout

* Ready to use:
  * reset, element, component, helper

* Predefined properties:
  * color, and such

#### Slide 12: Disadvantage

![Slide - CSS Frameworks: Disadvantage][slide-12]

Disadvantage

* Another learning time

* Bloated
  * unless utilize modular feature.
  * exclude unneeded artefact.

* Feels like witchery.
  * This things work, and nobody knows why.

Disadvantage for beginner

* Basic Design Provided:
  * Every site made, will have similar looks.

#### Slide 13: When not Using CSS Framework?

![Slide - CSS Frameworks: When not Using][slide-13]

When not Using CSS Framework?

* You want to write your own
  * This is technically make sense, for custom design.
  * To make something that suitable for your need.

* You step into a team with legacy project.

* AMP:
  * that require embedded stylesheet.

#### Slide 14: Step by Step Examples?

![Slide - CSS Frameworks: Step by Step Examples][slide-14]

Step by Step Examples?

* Bulma:
  * [Bulma + SASS](https://gitlab.com/epsi-rns/tutor-html-bulma)
  * [Bulma + Custom SASS (Material Design)](https://gitlab.com/epsi-rns/tutor-html-bulma-md)

* Materialize CSS
  * [Materialize CSS + SCSS](https://gitlab.com/epsi-rns/tutor-html-materialize)

* Semantic UI
  * [Semantic UI + Custom LESS (Material Design)](https://gitlab.com/epsi-rns/tutor-html-semantic-ui)

* Bootstrap
  * [Bootstrap + Custom SASS (Open Color)](https://gitlab.com/epsi-rns/tutor-html-bootstrap-oc)

* Custom
  * Make your own case, and share!

#### Slide 15: Chapter Break: Tool: Preprocessor!

![Slide - CSS Preprocessor][slide-15]

What is this CSS prepocessor rubbish?

> In short: CSS with superpowers!

Just the tool we desire.

#### Slide 16: The CSS Frustration

![Slide - CSS Preprocessor: The CSS Frustration][slide-16]

The CSS Frustration

* CSS code get ugly from time to time.
  * Nomore clean code.
  * Hard to read.
  * Hard to maintain.

* Always rewrite, for any changes
  * No variables in old browser.

* Modularization affect HTTP perfomance.
  * Multiple files require more HTTP requests.

> In short: Developing with CSS without tools
> is not scalable for large project.

#### Slide 17: Direct Advantage to Developer

![Slide - CSS Preprocessor: Direct Advantage][slide-17]

Direct advantage to developer

* Modify custom properties:
  * color, and such

* Ready to alter:
  * reset, element, component

* You can claim yourself as a coder.
  * This is a joke.
  * Yes, there is a compilation process.

#### Slide 18: Why CSS Preprocessor?

> Sass, Less or Stylus

![Slide - CSS Preprocessor: Why CSS Preprocessor][slide-18]

#### Slide 19: When not Using CSS Preprocessor?

![Slide - CSS Preprocessor: When not Using][slide-19]

When not Using CSS Preprocessor?

* You want to use unmodifed version CSS Frameworks.
  * Such as using CDN.

* Else, I'm still thinking...

#### Slide 20: Last 2019 State

![Slide - CSS Preprocessor: Current 2019 State][slide-20]

Current 2019 State

* SASS has wide implementation in many language
  * Most common are: dart-sass, node-sass, and deprecated ruby-sass
  * Supported in both Gulp and Grunt.
  * And bundler: webpack, rollup, and parcel.

* PostCSS with Tailwind CSS is a rising star.
  * PostCSS itself is more than just Preprocessor.

* LESS is less used.

* I do not know about this one:
  * Stylus

#### Slide 21: Which One Should I Choose?

![Slide - CSS Preprocessor: Which One][slide-21]

Learning:

* [@html_css_id](https://t.me/html_css_id)

Which One?

* Choose what works for you!

* I don't know. I'm just a blogger.

* Learning:
  * [@html_css_id](https://t.me/html_css_id)

#### Slide 22: Preprocessor Coding Examples?

![Slide - CSS Preprocessor: Examples][slide-22]

CSS Preprocessor Examples

* Custom SASS
  * [SASS: Loop - Spacing Class](/frontend/2019/06/21/sass-loop-spacing-class/)

* Custom LESS
  * [LESS: Loop - Spacing](/frontend/2019/10/03/less-loop-spacing-class/)
  * [LESS: Conditional - Color Class](/frontend/2019/10/04/less-conditional-color-class/)

* Custom PostCSS
  * [PostCSS: Loop - Spacing Class](/frontend/2019/10/04/less-conditional-color-class/)

#### Slide 23: Respect!

![Slide - CSS Preprocessor: Respect][slide-23]

> Respect!

Special thank you for the smart guys/girls,
behind these CSS preprocessor project.

#### Slide 24: Chapter Break

![Slide - Supporting Technologies][slide-24]

#### Slide 25: Supporting Technologies

![Slide - Technologies: Task Runner and Bundlers][slide-25]

CSS supporting Technologies

* Task Runner
  * [Grunt](/frontend/2020/04/11/tools-task-runner-grunt/)
  * Gulp
  * Brunch
  * Broccolli

* Bundler
  * [Webpack](/frontend/2020/04/13/tools-bundler-webpack-01/)
  * [Rollup](/frontend/2020/04/17/tools-bundler-rollup-01/)
  * [Parcel](/frontend/2020/04/19/tools-bundler-parcel/)

Just like you. I'm still learning.

#### Slide 26: Not Covered Yet?

![Slide - Technologies: Not Covered Yet][slide-26]

More about CSS supporting Technologies

* PostCSS:
  * Along with their plugins.

* Custom CSS without Framework:
  * Along with repository example.

* Tailwind:
  * Along with repository example.

This presentation will likely to change.
Depend on my knowledge growth.

#### Slide 27: Chapter Break

![Slide - Summary][slide-27]

#### Slide 28: Summary

![Slide - Summary: Along with Local Communities][slide-28]

Summary: Along with Local Communities

* CSS Framework
  * [Bootstrap](https://t.me/bootstrap_id),
    [Bulma](https://t.me/bulma_id),
    Materialize CSS,
    Semantic UI,
    [Tailwind CSS](https://t.me/TailwindID),
    [Custom CSS](https://t.me/Stylesheet_ID),
  * Local Communities:
    [belajarhtmlcss](https://t.me/belajarhtmlcss),
    [FrontEndID](https://t.me/FrontEndID)

* CSS Preprocessor
  * SASS, LESS, PreCSS (via PostCSS)
  * Local Community: [html_css_id](https://t.me/html_css_id/)

* Useful Tools
  * Task Runner: Gulp, Grunt
  * Bundler: Webpack, Parcel, Roller
  * Local Community: [html_css_id](https://t.me/html_css_id/)

#### Slide 29: What's Next?

![Slide - What's Next][slide-29]

> Leverage to SSG!

Learn to make a fully mature website,
without the burden of complicated backend.

* [Introduction to SSG for beginner](/ssg/2019/02/17/concept-ssg/)

#### Slide 30: Questions

![Slide - Questions][slide-30]

> Don't be shy!

#### Slide 31: Thank You

![Slide - Thank You][slide-31]

> Thank you for your time.

[//]: <> ( -- -- -- links below -- -- -- )

[teaser-preview]:           {{< assets-frontend >}}/2020/04/css-concept-page.png
[local-presentation]:       {{< baseurl >}}frontend/2019/02/15/concept-css/
[candyclone-template]:      https://epsi-rns.gitlab.io/design/2020/09/21/inkscape-impress-slides-01/
[libreoffice-impress]:      https://www.libreoffice.org/discover/impress/
[document-impress]:         {{< berkas2-blob >}}/impress-presentation/03-concept-css/presentation-concept-css-diagram.odp
[document-pdf]:             {{< berkas2-blob >}}/impress-presentation/03-concept-css/presentation-concept-css-diagram.pdf
[document-inkscape]:        {{< berkas2-blob >}}/impress-presentation/03-concept-css/presentation-concept-css-content.svg
[candyclone-content]:       {{< berkas2-blob >}}/impress-template-candyclone/template-candyclone-content.svg
[inkscape-thumbs-misc]:    {{< assets-frontend >}}/2020/css-thumbs-content-misc.png
[inkscape-thumbs-frame]:    {{< assets-frontend >}}/2020/css-thumbs-content-frame.png
[inkscape-thumbs-drops]:    {{< assets-frontend >}}/2020/css-thumbs-content-drops.png
[template-contest]:         https://blog.documentfoundation.org/blog/2020/10/12/libreoffice-impress-template-contest-by-the-indonesian-community/
[template-lumbung]:         https://lumbung.libreoffice.id/

[slide-01]: {{< assets-frontend >}}/2020/css/slide-01-cover.png
[slide-02]: {{< assets-frontend >}}/2020/css/slide-02-about-author.png
[slide-03]: {{< assets-frontend >}}/2020/css/slide-03-preface-html-css-js.png
[slide-04]: {{< assets-frontend >}}/2020/css/slide-04-preface-html-document.png
[slide-05]: {{< assets-frontend >}}/2020/css/slide-05-preface-learning-html-step-by-step.png
[slide-06]: {{< assets-frontend >}}/2020/css/slide-06-preface-learning-css-step-by-step.png
[slide-07]: {{< assets-frontend >}}/2020/css/slide-07-preface-placement.png
[slide-08]: {{< assets-frontend >}}/2020/css/slide-08-css-frameworks.png
[slide-09]: {{< assets-frontend >}}/2020/css/slide-09-css-frameworks-oldschool-issue.png
[slide-10]: {{< assets-frontend >}}/2020/css/slide-10-css-frameworks-why-css-framework.png
[slide-11]: {{< assets-frontend >}}/2020/css/slide-11-css-frameworks-direct-advantage.png
[slide-12]: {{< assets-frontend >}}/2020/css/slide-12-css-frameworks-disadvantage.png
[slide-13]: {{< assets-frontend >}}/2020/css/slide-13-css-frameworks-when-not-using.png
[slide-14]: {{< assets-frontend >}}/2020/css/slide-14-css-frameworks-step-by-step-examples.png
[slide-15]: {{< assets-frontend >}}/2020/css/slide-15-css-preprocessor.png
[slide-16]: {{< assets-frontend >}}/2020/css/slide-16-css-preprocessor-the-css-frustation.png
[slide-17]: {{< assets-frontend >}}/2020/css/slide-17-css-preprocessor-direct-advantage.png
[slide-18]: {{< assets-frontend >}}/2020/css/slide-18-css-preprocessor-why-css-preprocessor.png
[slide-19]: {{< assets-frontend >}}/2020/css/slide-19-css-preprocessor-when-not-using.png
[slide-20]: {{< assets-frontend >}}/2020/css/slide-20-css-preprocessor-current-2019-state.png
[slide-21]: {{< assets-frontend >}}/2020/css/slide-21-css-preprocessor-which-one.png
[slide-22]: {{< assets-frontend >}}/2020/css/slide-22-css-preprocessor-coding-examples.png
[slide-23]: {{< assets-frontend >}}/2020/css/slide-23-css-preprocessor-respect.png
[slide-24]: {{< assets-frontend >}}/2020/css/slide-24-supporting-technologies.png
[slide-25]: {{< assets-frontend >}}/2020/css/slide-25-technologies-task-runner-and-bundlers.png
[slide-26]: {{< assets-frontend >}}/2020/css/slide-26-technologies-not-covered-yet.png
[slide-27]: {{< assets-frontend >}}/2020/css/slide-27-summary.png
[slide-28]: {{< assets-frontend >}}/2020/css/slide-28-summary-along-with-local-communities.png
[slide-29]: {{< assets-frontend >}}/2020/css/slide-29-whats-next.png
[slide-30]: {{< assets-frontend >}}/2020/css/slide-30-questions.png
[slide-31]: {{< assets-frontend >}}/2020/css/slide-31-thank-you.png
