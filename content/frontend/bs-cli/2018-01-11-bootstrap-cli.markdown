---
type   : post
title  : "Bootstrap CLI Webtools - Part One"
date   : 2018-01-11T17:35:15+07:00
slug   : bootstrap-cli-part-one
categories: [frontend]
tags      : [css, js, bootstrap]
keywords  : [command line interface, bower, grunt, gulp]
author : epsi
opengraph:
  image: assets/site/images/topics/bootstrap.png

toc    : "toc-2018-01-bootstrap-cli"

excerpt:
  Embracing CSS Bootstrap Backend By CLI Webtools.
  Playing around with CSS Bootstrap Backend,
  using bower, grunt, composer, gemfile, sass,
  all in command line interface.

---

### Node Module

You may skip this part, but If you are new to NodeJS,
you should read the offical documentation first for both
[Node JS](https://nodejs.org/en/) and [NPM JS](https://www.npmjs.com/).

You may notice this <code>package.json</code> in root directory.

{{< highlight json >}}
{
  "name": "bootstrap",
  "description": "The most popular front-end framework for developing responsive, mobile first projects on the web.",
  "version": "3.3.7"
}
{{< / highlight >}}

![Node Module: package.json][image-ss-node-package-json]

Now you can install node modules in respective directory.
NPM recognize information from <code>package.json</code>.

{{< highlight bash >}}
% cd ~/bootstrap/bootstrap-less-3.3.7/
% npm install
{{< / highlight >}}

[![Node Module: npm install][image-ss-source-npm-install]][photo-ss-source-npm-install]

A newly directory has been created

{{< highlight bash >}}
% tree -d node_modules | head -n 10
node_modules
├── abbrev
├── accepts
├── acorn
│   ├── bin
│   ├── dist
│   ├── rollup
│   └── src
│       ├── bin
│       ├── loose
{{< / highlight >}}

-- -- --

### Bower

#### Bower as General

You may skip this part, but If you are new to <code>Bower</code>,
you should read the offical documentation first for [Bower](https://bower.io/).
The official site said that you should migrate
to <code>Yarn</code> and <code>WebPack</code>.

Consider using sites directory.
You can install bower globally.

{{< highlight bash >}}
% sudo npm install -g bower
{{< / highlight >}}

![Bower: NPM Install Bower][image-ss-npm-install-bower]

After this you can do something cool with bower such as install JQuery

{{< highlight bash >}}
% bower install jquery
{{< / highlight >}}

Or something more complex, as the documentation said.

{{< highlight bash >}}
% bower install desandro/masonry
{{< / highlight >}}

[![Bower: Install JQuery][image-ss-bower-install-jquery]][photo-ss-bower-install-masonry]

#### Bower Inside Bootstrap.

Consider go back to bootstrap-less directory.

You may notice this <code>bower.json</code> in root directory.

{{< highlight json >}}
{
  "name": "bootstrap",
  "description": "The most popular front-end framework for developing responsive, mobile first projects on the web.",
  ...
  "dependencies": {
    "jquery": "1.9.1 - 3"
  }
}
{{< / highlight >}}

Now you can also install dependencies (JQuery) directly using bower.

![Bower: install inside bootstrap][image-ss-bootstrap-bower-install]

#### Bower Outside Bootstrap.

You can also install bootstrap using Bower.

{{< highlight bash >}}
% bower install --save bootstrap-sass
{{< / highlight >}}

![Bower: Install Bootstrap Official][image-ss-bower-install-bootstrap]

-- -- --

### Grunt

#### Grunt as General

You may also skip this part, but If you are new to <code>Grunt</code>,
you should read the offical documentation first for [GruntJS](https://gruntjs.com/).

Consider using sites directory.
You can install grunt globally.

{{< highlight bash >}}
% sudo npm install -g grunt-cli
{{< / highlight >}}

![Grunt: NPM Install Grunt CLI][image-ss-npm-install-grunt]

#### Grunt Inside Bootstrap.

Consider go back to bootstrap-less directory.

You may notice this <code>Gruntfile.js</code> in root directory.

{{< highlight javascript >}}
module.exports = function (grunt) {
  'use strict';

  // Force use of Unix newlines
  grunt.util.linefeed = '\n';
  ...
};
{{< / highlight >}}

Now you can run command as in official documentation.
This will generate all the files needed in respective dist directory.

{{< highlight bash >}}
% grunt dist
{{< / highlight >}}

[![Grunt: dist][image-ss-grunt-dist]][photo-ss-grunt-dist]

Or documentation.

{{< highlight bash >}}
% grunt docs
{{< / highlight >}}

[![Grunt: docs][image-ss-grunt-docs]][photo-ss-grunt-docs]

Or even test

{{< highlight bash >}}
% grunt test
{{< / highlight >}}

-- -- --

### Gulp

There is also alternative to <code>grunt</code>.
I won't cover much since bootstrap does not use it.

#### Reading

Some good read:

* The Offical Documentation: [GulpJS](https://gulpjs.com/).

* Tutorial by Baretto: [Working with Sass, Bootstrap and Gulp](http://david-barreto.com/working-with-sass-bootstrap-and-gulp/)

#### Preview

It can do something cool just like Grunt.

{{< highlight bash >}}
% gulp
{{< / highlight >}}

[![Gulp: example][image-ss-gulp-example]][photo-ss-gulp-example]

-- -- --

### eyeglass

This is a tool to do SASS under NPM umbrella.
I must admit, I do not know about it. Not yet.

-- -- --

### sw.js (webpack)

I haven't explore yet.
I really a n00b in Javascript land.

-- -- --

### What's Next

There are, some interesting topic for <code>Ruby</code>,
such as <code>Gemfile</code>, <code>SASS</code>, and <code>Jekyll</code> (SSG).
Consider finish reading [ [Part Two][local-part-two] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-part-two]:       {{< baseurl >}}frontend/2018/01/12/bootstrap-cli-part-two/

[image-ss-source-npm-install]:      {{< assets-frontend >}}/2018/01/source-npm-install.png
[photo-ss-source-npm-install]:      https://photos.google.com/share/AF1QipMCFikwVY_d7DR9OMOmp-t4qwKDgluWO9lU6qK01_y9IUYA7eorvCdHkmRrRxnatA/photo/AF1QipPWjgC3oq4ebrZ6kNMDk30cX9IiRVK8zcNtcUwc?key=U2l0bFJCRFZuY00xOUlCeUhiRGVEOTJESVo5MmFR

[image-ss-node-package-json]:       {{< assets-frontend >}}/2018/01/vim-package-json.png

[image-ss-npm-install-bower]:       {{< assets-frontend >}}/2018/01/bower-install.png

[image-ss-bower-install-jquery]:    {{< assets-frontend >}}/2018/01/bower-install-jquery.png
[photo-ss-bower-install-masonry]:   https://photos.google.com/share/AF1QipMCFikwVY_d7DR9OMOmp-t4qwKDgluWO9lU6qK01_y9IUYA7eorvCdHkmRrRxnatA/photo/AF1QipMUyBWCK3ygPO_wuLWgZMIQSvJ21gV3SG_Brsdg?key=U2l0bFJCRFZuY00xOUlCeUhiRGVEOTJESVo5MmFR

[image-ss-bootstrap-bower-install]: {{< assets-frontend >}}/2018/01/less-bower-install.png

[image-ss-bower-install-bootstrap]: {{< assets-frontend >}}/2018/01/bower-install-bootstrap-sass-official.png

[image-ss-npm-install-grunt]:       {{< assets-frontend >}}/2018/01/grunt-install.png

[image-ss-grunt-dist]:              {{< assets-frontend >}}/2018/01/grunt-dist.png
[photo-ss-grunt-dist]:              https://photos.google.com/share/AF1QipMCFikwVY_d7DR9OMOmp-t4qwKDgluWO9lU6qK01_y9IUYA7eorvCdHkmRrRxnatA/photo/AF1QipNFkCFoyyTshvZ_CqTiahoLoiFTBB19Xv8ZKIp0?key=U2l0bFJCRFZuY00xOUlCeUhiRGVEOTJESVo5MmFR

[image-ss-grunt-docs]:              {{< assets-frontend >}}/2018/01/grunt-docs.png
[photo-ss-grunt-docs]:              https://photos.google.com/share/AF1QipMCFikwVY_d7DR9OMOmp-t4qwKDgluWO9lU6qK01_y9IUYA7eorvCdHkmRrRxnatA/photo/AF1QipNiZYNzbSLUVseq3dnIHbSrkOl6x18xLZXjE84t?key=U2l0bFJCRFZuY00xOUlCeUhiRGVEOTJESVo5MmFR

[image-ss-gulp-example]:            {{< assets-frontend >}}/2018/01/gulp-final.png
[photo-ss-gulp-example]:            https://photos.google.com/share/AF1QipMCFikwVY_d7DR9OMOmp-t4qwKDgluWO9lU6qK01_y9IUYA7eorvCdHkmRrRxnatA/photo/AF1QipPUPCrq0LMwimNsExNg7loAhJCrweqZLoojEeSy?key=U2l0bFJCRFZuY00xOUlCeUhiRGVEOTJESVo5MmFR
