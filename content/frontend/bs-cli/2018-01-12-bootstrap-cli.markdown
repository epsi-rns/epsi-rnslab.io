---
type   : post
title  : "Bootstrap CLI Webtools - Part Two"
date   : 2018-01-12T17:35:15+07:00
slug   : bootstrap-cli-part-two
categories: [frontend]
tags      : [css, js, bootstrap]
keywords  : [command line interface, bower, grunt, gulp]
author : epsi
opengraph:
  image: assets/site/images/topics/bootstrap.png

toc    : "toc-2018-01-bootstrap-cli"

excerpt:
  Embracing CSS Bootstrap Backend By CLI Webtools.
  Playing around with CSS Bootstrap Backend,
  using bower, grunt, composer, gemfile, sass,
  all in command line interface.

---

### Gemfile

This is pretty easy if yu have ever use Jekyll before.

#### Install bundler

In order to use this <code>Gemfile</code>,
you need to install <code>Bundler</code>.

{{< highlight bash >}}
% gem install bundler
{{< / highlight >}}

![Ruby Gem: Install Bundler][image-ss-gem-install-bundler]

#### Gemfile Inside Bootstrap.

Consider go back to bootstrap-less directory.
You may notice this <code>Gemfile</code> in root directory.

{{< highlight ruby >}}
source 'https://rubygems.org'

group :development, :test do
  gem 'jekyll', '~> 3.1.2'
  gem 'jekyll-sitemap', '~> 0.11.0'
end
{{< / highlight >}}

Now you can run Gemfile.
This will install all the files needed by the dependencies.
I' using RVM, so in my case this will <code>~/.rvm/gems/ruby-2.4.1/</code> directory.

{{< highlight bash >}}
% bundle install
{{< / highlight >}}

[![Gemfile: bundle install][image-ss-gem-bundle-install]][photo-ss-gem-bundle-install]

Do not worry if this does nothing. 
We need this <code> Gemfile</code> information,
if we want to install bootstrap from outside.

#### Gemfile Outside Bootstrap.

You can install bootstrap using Gem.

{{< highlight bash >}}
% gem install bootstrap-sass
{{< / highlight >}}

![Gem: Install Bootstrap SASS][image-ss-gem-install-bootstrap]

-- -- --

### SASS (Ruby)

I cannot find anywhere in bootstrap that I can use as an example of SASS command.
I decide to use my own Jekyll site as a standalone example.
Here it is, I copied my <code>_sass</code>,
and rename it to <code>input</code>.
Then I make an empty <code>output</code> directory.
Now we can run sass.

{{< highlight bash >}}
% sass --help
{{< / highlight >}}

{{< highlight bash >}}
% sass --update input:output  --style compressed --sourcemap=none
{{< / highlight >}}

[![SASS: update][image-ss-sass-standalone]][photo-ss-sass-standalone]

For my Hugo blog, I have my real directory as below:

{{< highlight bash >}}
% sass -I sass --update sass/themes/oriclone:static/assets/css
{{< / highlight >}}

{{< highlight bash >}}
% sass --watch -I sass  sass/themes/oriclone:static/assets/css --style compressed --sourcemap=none
{{< / highlight >}}

And there are also alternative, using Node.

{{< highlight bash >}}
% node-sass _sass/themes --include-path _sass --output assets/themes
{{< / highlight >}}

-- -- --

### Jekyll (Static Site Generator)

Yes, the documentaion is in Jekyll. 
And you can run it offline using <code>localhost:4000</code>.

#### Additional Setup

You need to create this Jekyll's <code>_config.yml</code> as this example below:

{{< highlight yaml >}}
# Site settings
title: "bootstrap docs"

# the subpath of your site, e.g. /blog
baseurl: "" 

# the base hostname & protocol for your site
url: "localhost:4000" 
{{< / highlight >}}

You also need to copy grunt config as well if needed.

#### Run Server

Now you can run the server

{{< highlight bash >}}
% jekyll server
{{< / highlight >}}

![Jekyll: Bootstrap Offline Documentation][image-ss-docs-jekyll-server]

-- -- --

### Sache

There is this [sache](http://www.sache.in/) official documentation.
But I consider this good article:

* [Share your Sass with Sache](http://thesassway.com/intermediate/share-your-sass-with-sache-a-quick-guide)

If you use Bootstrap v4, you might recognize,
this <code>sache.json</code> at root directory.

{{< highlight json >}}
{
  "name": "bootstrap",
  "description": "The most popular HTML, CSS, and JavaScript framework for developing responsive, mobile first projects on the web.",
  "tags": ["bootstrap", "grid", "typography", "buttons", "ui", "responsive-web-design"]
}
{{< / highlight >}}

-- -- --

### What's Next

There are still, some interesting topic for <code>PHP</code>,
the <code>composer</code>.
Consider finish reading [ [Part Three][local-part-three] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )


[local-part-three]:     {{< baseurl >}}frontend/2018/01/13/bootstrap-cli-part-three/

[image-ss-gem-install-bundler]:     {{< assets-frontend >}}/2018/01/gem-install-bundler.png

[image-ss-gem-bundle-install]:      {{< assets-frontend >}}/2018/01/gemfile-bundle-install.png
[photo-ss-gem-bundle-install]:      https://photos.google.com/share/AF1QipMCFikwVY_d7DR9OMOmp-t4qwKDgluWO9lU6qK01_y9IUYA7eorvCdHkmRrRxnatA/photo/AF1QipNGlXWCDssLsHMadN7a29wrBDZ5RPf7Xd0a08HL?key=U2l0bFJCRFZuY00xOUlCeUhiRGVEOTJESVo5MmFR

[image-ss-gem-install-bootstrap]:   {{< assets-frontend >}}/2018/01/gem-install-bootstrap-sass.png

[image-ss-sass-standalone]:         {{< assets-frontend >}}/2018/01/sass-standalone.png
[photo-ss-sass-standalone]:         https://photos.google.com/share/AF1QipMCFikwVY_d7DR9OMOmp-t4qwKDgluWO9lU6qK01_y9IUYA7eorvCdHkmRrRxnatA/photo/AF1QipOeaXtY6fjuRidnRJQscJrXHXbbwOE1maHoptDG?key=U2l0bFJCRFZuY00xOUlCeUhiRGVEOTJESVo5MmFR

[image-ss-docs-jekyll-server]:      {{< assets-frontend >}}/2018/01/docs-jekyll-server.png
