---
type   : post
title  : "Bootstrap - Offline Template Examples"
date   : 2018-08-09T09:35:15+07:00
slug   : bootstrap-cli-offline
categories: [frontend]
tags      : [css, jekyll, bootstrap]
keywords  : [command line interface, npm, bootstrap offline]
author : epsi
opengraph:
  image: assets/site/images/topics/bootstrap.png

toc    : "toc-2018-08-bootstrap-cli"

excerpt:
  Get a local copy of bootstrap template examples,
  using no server.

---

### Preface

> Get a local copy of bootstrap template examples.

You can install the boostrap v4.1 template examples
as a local offline copy that can be launched from open file manager.

*	[getbootstrap.com/docs/4.1/examples/](https://getbootstrap.com/docs/4.1/examples/)

#### Step

*	Get Bootstrap Source Code

*	Generate dist (js + css)

*	Open The Offline Examples

-- -- --

### Get Bootstrap

{{< highlight bash >}}
$ wget -c https://github.com/twbs/bootstrap/archive/v4.1.3.zip

$ unzip v4.1.3.zip

$ cd bootstrap-4.1.3
{{< / highlight >}}

You can extract anywhere to suit your needs.

#### Examples Directory

This is the pure html, if you want to start, using templates

{{< highlight bash >}}
$ cd site/docs/4.1/examples
{{< / highlight >}}

You may consider, clicking the image for bigger resolution.

[![Bootstrap: examples directory][image-ss-twbs-examples]][photo-ss-twbs-examples]

-- -- --

### Generate Dist

We are not done yet.
We need to generate dist (jss + css).

There are two methods: 

*	Using NPM

*	Manually copy and paste

#### Using NPM

There is a file named <code>package.json</code>
in extracted bootstrap directory.
It is an npm package bundle configuration.

{{< highlight json >}}
{
  "name": "bootstrap",
  "description": "The most popular front-end framework for developing responsive, mobile first projects on the web.",
  "version": "4.1.3",
  "keywords": [
    "css",
    "sass",
    "mobile-first",
    "responsive",
    "front-end",
    "framework",
    "web"
  ],
...
}
{{< / highlight >}}

Now you need to run <code>npm install</code> in that folder,
so that the npm can install the right package.

{{< highlight bash >}}
$ cd bootstrap-4.1.3

$ npm install
{{< / highlight >}}

![Bootstrap: npm install][image-ss-twbs-npm-install]

And <code>npm run dist</code> in the very same folder.

{{< highlight bash >}}
$ cd bootstrap-4.1.3

$ npm run dist
{{< / highlight >}}

[![Bootstrap: npm run dist][image-ss-twbs-npm-run-dist]][photo-ss-twbs-npm-run-dist]

#### Copy Using Terminal

{{< highlight bash >}}
$ cd bootstrap-4.1.3

$ cp -a dist site/docs/4.1/
{{< / highlight >}}

![Bootstrap: copy paste dist][image-ss-twbs-dist-copy]

#### Copy Paste Using File Manager

If you have difficulty using terminal,
you may use File Manager.
Depend on your Desktop Environment,
your file manager might be:

*	gnome-shell: nautilus

*	Plasma5: dolphin

*	Cinnamon: nemo

*	Mate: caja

*	XFCE4: thunar

*	LXQT: pcmanfm

*	Pantheon: pantheon-files

I know it looks dumb explaining this,
but sometimes I met people who does not have a clue about this.
I respect all the reader, both beginner and expert.

{{< highlight bash >}}
$ thunar &
{{< / highlight >}}

-- -- --

### Open The Offline Examples

#### No server

You do not need Jekyll to have a local copy of your bootstrap.
Just open it from file manager to browser, such as firefox, or chromium.

![Bootstrap: open from file manager][image-ss-twbs-open-with]

#### Open in Your Browser

And this is the result:

[![Bootstrap: cover using localcopy][image-ss-twbs-cover]][photo-ss-twbs-cover]

#### Fully Working Site

In order to have a fully working site, you need Jekyll.

-- -- --

### What's Next

Consider finish reading [ [Part Two][local-part-two] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-part-two]:	{{< baseurl >}}frontend/2018/08/10/bootstrap-cli-rvm-bundle.html

[image-ss-twbs-examples]:       {{< assets-frontend >}}/2018/08/bootstrap-examples-directory.png
[image-ss-twbs-cover]:          {{< assets-frontend >}}/2018/08/bootstrap-localcopy-cover.png
[image-ss-twbs-open-with]:      {{< assets-frontend >}}/2018/08/bootstrap-open-with.png
[image-ss-twbs-npm-run-dist]:   {{< assets-frontend >}}/2018/08/bootstrap-npm-run-dist.png
[image-ss-twbs-npm-install]:    {{< assets-frontend >}}/2018/08/bootstrap-npm-install.png
[image-ss-twbs-dist-copy]:      {{< assets-frontend >}}/2018/08/bootstrap-dist-copy.png

[photo-ss-twbs-cover]:          https://photos.google.com/share/AF1QipMCFikwVY_d7DR9OMOmp-t4qwKDgluWO9lU6qK01_y9IUYA7eorvCdHkmRrRxnatA/photo/AF1QipNr-NHrZmWFQ38-cSwmnq9O8JQibSS8HGgcHUZ0?key=U2l0bFJCRFZuY00xOUlCeUhiRGVEOTJESVo5MmFR
[photo-ss-twbs-examples]:       https://photos.google.com/share/AF1QipMCFikwVY_d7DR9OMOmp-t4qwKDgluWO9lU6qK01_y9IUYA7eorvCdHkmRrRxnatA/photo/AF1QipN1pglfVjhERNLs0ETKnBXiDVFJeTBr7qZIMqpw?key=U2l0bFJCRFZuY00xOUlCeUhiRGVEOTJESVo5MmFR
[photo-ss-twbs-npm-run-dist]:   https://photos.google.com/share/AF1QipMCFikwVY_d7DR9OMOmp-t4qwKDgluWO9lU6qK01_y9IUYA7eorvCdHkmRrRxnatA/photo/AF1QipMDoZWvnqtW83zacS01IEpmnrmIJSvfzloPIlrm?key=U2l0bFJCRFZuY00xOUlCeUhiRGVEOTJESVo5MmFR
