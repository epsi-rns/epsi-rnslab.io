---
type   : post
title  : "Bootstrap OC - Overview"
date   : 2020-03-01T09:17:35+07:00
slug   : bootstrap-oc-overview
categories: [frontend]
tags      : [bootstrap, css]
keywords  : [plain html, custom theme, summary, overview]
author : epsi
opengraph:
  image: assets/site/images/topics/bootstrap-sass.png

toc    : "toc-2020-03-html-bootstrap-oc-step"

excerpt:
  Designing real world web page using Bootstrap, step by step.
  Summary of article series.

---

### Preface

This article series is intended for beginner.

> Goal: Make your own theme using Bootstrap

With this guidance, you can make your own theme,
for use with blogging, or simple company profile.
You can also enhanced later to create,
your own portfolio based on this knowledge.

To face real life, blog building situation,
there will other guidance, based on this Bootstrap example.

#### Reading

Always refer to official documentation first,
before you start this real world example.

* [getbootstrap.com](https://getbootstrap.com/)

#### Version

> Which Bootstrap?

I'm using Boostrap v4.
By the time this article written,
Bootstrap v5 is still in alpha stage.

#### HTML Tools

> My intention is simplicity

I'm using pure HTML instead of static site generator,
so that any beginner can follow this Bootstrap show case.

You can just open the file in the browser, and this will works.

#### CSS Tools

I'm using Sass to generate CSS,
with `.scss` format instead of `.sass`.
Because Bootstrap use `.scss` format.

Of course you can use any `.sass` format if you want,
for your web design.

#### Learning Stylesheet

> The Scope of CSS Tools

Learning CSS Frameworks is a part of this bigger picture below:

![The Scope of CSS Tools][illustration-css-tools]

-- -- --

### About Bootstrap

Bootstrap is an opensource CSS Framework.
There are other alternative as well,
such as <code>Materialize</code> or <code>Bulma</code>.

#### Why Bootstrap?

Frankly speaking.
I like Bulma, and I desire to learn Tailwind.
But I most beginner start from bootstrap,
and a lot of question comes up.
So I took a step back.
Rewrite my Bulma theme using Bootstrap.

> This theme is a direct port from my Bulma MD theme.

#### Rewrite

My `Bulma MD` theme has these two additional stuff.

* Icons: FontAwesome

* Pallete: Google Material Color

Since the name `Bootstrap MD` has already taken,
I use other pallete, so now we have `Bootstrap OC` name
And also change the icons, just because I want to try something else.
Now we have these two:

* Icons: FeatherIcons

* Pallete: Open Color

#### Part of Bootstrap

Just read again the offical documentation.

-- -- --

### Example Showcase

#### Screenshot

I add some aestethic appeal, with more custom stylesheet.

![Bootstrap: Preview][image-ss-00-bootstrap-preview]

#### Source Code

Source code used in this tutorial, is available at:

*	[gitlab.com/epsi-rns/tutor-html-bootstrap-oc][tutor-bootstrap-master]

This is also intended as a Demo.

#### Tools

> Opening Example in Browser

You can open the `html` file artefact directly from your `file manager`,
or alternatively setup simple web server example such as `browsersync`,
and enjoy the `localhost:3000`.

#### Disclaimer

This is would not be the best blog template that you ever have.
Because I only put most common stuff,
to keep the tutorial simple.

After you learn this guidance,
you understand the fundamental skill.
Thus, a base for you, to make your own blog site.
With your imagination, you may continue,
to build your own super duper Bootstrap site.
Far better than, what I have achieved.

#### Table of Content

The table content is available as header on each tutorial.
There, I present a Bootstrap Tutorial, step by step, for beginners.

-- -- --

### Begin The Guidance

Open Color start from step 04.
Step 01 until step 03 contain Bootstrap preparation.

## Step 01

> Adding Bootstrap CSS

* Without Bootstrap

* Using CDN

* Using Local

![screenshot][screenshot-01]

## Step 02

> Using Bootstrap CSS: Navigation Bar

* From Simple to Full Featured

* Javascript: With jQuery, With Vue JS, Plain Javascript.

* Icons: FontAwesome, Boostrap Icon, Feathers Icon.

![screenshot][screenshot-02]

## Step 03

> Custom SASS

* Custom maximum width class

* Simple responsive layout using Bootstrap

![screenshot][screenshot-03]

## Step 04

> Open Color

* Open Color Demo, using tailor made sass

* Double column demo, responsive gap using custom sass

* Single column demo, simple landing page

![screenshot][screenshot-04]

## Step 05

> HTML Box using Open Color

* Main Blog and Aside Widget

![screenshot][screenshot-05]

## Step 06

> Finishing

* Blog Post Example

![screenshot][screenshot-06]

-- -- --

### What's Next

Thank you for cloning the [Bootstrap Open Color][repository] repository.
Have fun, and enjoy coding.

Consider continue reading [ [Bootstrap - Minimal][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2020/03/02/bootstrap-minimal/

[tutor-bootstrap-master]:   {{< tutor-html-bs-oc >}}/

[image-ss-00-bootstrap-preview]:    {{< assets-frontend >}}/2020/03/bsoc/00-html-bootstrap-oc-preview.png
[illustration-css-tools]:       {{< assets-frontend >}}/2020/03/css-tools.png

[repository]:            https://gitlab.com/epsi-rns/tutor-html-bulma-md/
[screenshot-01]:         https://gitlab.com/epsi-rns/tutor-html-bootstrap-oc/raw/master/step-01/html-bootstrap-oc-preview.png
[screenshot-02]:         https://gitlab.com/epsi-rns/tutor-html-bootstrap-oc/raw/master/step-02/html-bootstrap-oc-preview.png
[screenshot-03]:         https://gitlab.com/epsi-rns/tutor-html-bootstrap-oc/raw/master/step-03/html-bootstrap-oc-preview.png
[screenshot-04]:         https://gitlab.com/epsi-rns/tutor-html-bootstrap-oc/raw/master/step-04/html-bootstrap-oc-preview.png
[screenshot-05]:         https://gitlab.com/epsi-rns/tutor-html-bootstrap-oc/raw/master/step-05/html-bootstrap-oc-preview.png
[screenshot-06]:         https://gitlab.com/epsi-rns/tutor-html-bootstrap-oc/raw/master/step-06/html-bootstrap-oc-preview.png
