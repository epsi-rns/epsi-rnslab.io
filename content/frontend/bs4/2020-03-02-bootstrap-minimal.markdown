---
type   : post
title  : "Bootstrap - Minimal"
date   : 2020-03-02T09:17:35+07:00
slug   : bootstrap-minimal
categories: [frontend]
tags      : [bootstrap, css]
keywords  : [plain html, custom theme, skeleton, bootstrap cdn]
author : epsi
opengraph:
  image: assets/site/images/topics/bootstrap-sass.png

toc    : "toc-2020-03-html-bootstrap-oc-step"

excerpt:
  Make custom theme, step by step.
  Show minimal page in Bootstrap.

---

### Preface

> Goal: Using Bootstrap for the first time

-- -- --

### 1: Layout

No. We do not use Bootstrap yet.
We need to talk about something more fundamental.

#### The Document: HTML Skeleton

This tutorial use `header`, `main`, and `footer`.
There is **html5** semantic tag for this.
But this could be as simple as **html element** below.

* [gitlab.com/.../11-html-layout.html][tutor-01-11-html].

{{< highlight html >}}
<!DOCTYPE html>
<html>

<head>
  <title>Your mission. Good Luck!</title>
</head>

<body>

  <!-- header -->
  <p><i>Your mission, should you decide to accept it.</i></p>
  <br/>

  <!-- main -->
  <div>
    <p>To have, to hold, to love,
    cherish, honor, and protect?</p>

    <p>To shield from terrors known and unknown?
    To lie, to deceive?</p>

    <p>To live a double life,
    to fail to prevent her abduction,
    erase her identity, 
    force her into hiding,
    take away all she has known.</p>
  </div>

  <!-- footer -->
  <br/>
  <p><i>As always, should you be caught or killed,
  any knowledge of your actions will be disavowed.</i></p>

</body>

</html>
{{< / highlight >}}


Open this **html artefact** in any browser,
using your favorite **file manager**.
And have a look at the result.

![html: layout][image-ss-11-layout]

-- -- --

### 2: Loading CSS

There are some alternative to load CSS.

For this tutorial. 
I suggest to use local CSS.
Because we are going to make a custom Bootstrap CSS

#### Using CDN Bootstrap CSS

It is as simple as 

{{< highlight html >}}
  <link rel="stylesheet"
        href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
        integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
        crossorigin="anonymous">
{{< / highlight >}}

And if you need the javascript, you can add:

{{< highlight html >}}
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" 
          integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
          crossorigin="anonymous"></script>
{{< / highlight >}}

* [gitlab.com/.../12-head-cdn.html][tutor-01-12-html].

#### Directory Preparation

Now consider create directory for respective assets:

* css

* js

![Bootstrap: Directory Preparation][image-ss-10-assets]

We only need `css` directory for the rest of this chapter.
But later we need other directory as well such as `images`, `js`, and `sass`.
The `js` part is only needed for `navbar`.

#### Using Local Bootstrap CSS

You need to download `bootstrap.css` first from the official site.
And put the artefact to `css` directory.

{{< highlight html >}}
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
{{< / highlight >}}

* [gitlab.com/.../13-head-local.html][tutor-01-13-html].
  
And the `bootstrap.css` is here:

* [gitlab.com/.../css/bootstrap.css][tutor-01-css-bootstrap].

#### Adding Meta

As the official guidance said.
We have to add a few tag on the `head`.

{{< highlight html >}}
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Your mission. Good Luck!</title>
{{< / highlight >}}

-- -- --

### 3: Bootstrap: Example Class

Now consider having fun for while.
And give more example class, and see what happened.

#### List Group

We need to see if Bootstrap class work correctly.
Consider add this `container` and `list-group` class.

{{< highlight html >}}
  <div class="container">
    <!-- header -->
    <p><i>Your mission, should you decide to accept it.</i></p>
    <br/>

    <!-- main -->
    <ul class="list-group">
      <li class="list-group-item">
      To have, to hold, to love,
      cherish, honor, and protect?</li>

      <li class="list-group-item">
      To shield from terrors known and unknown?
      To lie, to deceive?</li>

      <li class="list-group-item">
      To live a double life,
      to fail to prevent her abduction,
      erase her identity, 
      force her into hiding,
      take away all she has known.</li>
    </ul>

    <!-- footer -->
    <br/>
    <p><i>As always, should you be caught or killed,
    any knowledge of your actions will be disavowed.</i></p>
  </div>
{{< / highlight >}}

#### The Preview

And have a look at the result by opening the file in Browser.

![css: bootstrap][image-ss-13-local]

There is more fun than just this.
We are going to explore later.

-- -- --

### 4: Bootstrap: Navbar Class

For the rest of this show case,
I'm using both `navbar header` and `footer`.

* [gitlab.com/.../15-navbar.html][tutor-01-15-html].

#### As Header

Navbar lies on header part.

{{< highlight html >}}
  <!-- header -->
  <nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <div class="navbar-collapse">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="#">
            Home <span class="sr-only">(current)</span></a>
        </li>
      </ul>
    </div>
  </nav>
{{< / highlight >}}

#### As Footer

Navbar also lies on footer part.

{{< highlight html >}}
  <!-- footer -->
  <footer class="footer">
    <div class="bg-dark text-light text-center">
      &copy; 2020.
    </div>
  </footer>
{{< / highlight >}}

#### Additional CSS

To have proper **layout**,
this navbar require additional CSS.

* [gitlab.com/.../css/main.css][tutor-01-css-main].

{{< highlight css >}}
.layout-base {
  padding-top: 3rem;
  padding-bottom: 2rem;
}
{{< / highlight >}}

And for the sticky footer.
I grab from the official site.

* [gitlab.com/.../css/sticky-footer.css][tutor-01-css-sticky].

{{< highlight css >}}
html {
  position: relative;
  min-height: 100%;
}
body {
  margin-bottom: 60px; /* Margin bottom by footer height */
}
.footer {
  position: absolute;
  bottom: 0;
  width: 100%;
  height: 60px; /* Set the fixed height of the footer here */
  line-height: 60px; /* Vertically center the text there */
  background-color: #f5f5f5;
}
{{< / highlight >}}

You should define `styles` on `<head>` element.

{{< highlight html >}}
  <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="css/main.css">
  <link rel="stylesheet" type="text/css" href="css/sticky-footer.css">
{{< / highlight >}}

#### Screenshot

And have fun with the result.
Consider open the file in Browser form file manager.

![css: navbar][image-ss-15-navbar]

-- -- --

### 5: Summary

As a summary from this chapter,
all the **html element** is as shown below.

{{< highlight html >}}
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Your mission. Good Luck!</title>

  <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="css/main.css">
  <link rel="stylesheet" type="text/css" href="css/sticky-footer.css">
</head>

<body>

  <!-- header -->
  <nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <div class="navbar-collapse">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="#">
            Home <span class="sr-only">(current)</span></a>
        </li>
      </ul>
    </div>
  </nav>

  <!-- main -->
  <div class="layout-base">
    <main role="main" class="container">
      <ul class="list-group">
        <li class="list-group-item">
        To have, to hold, to love,
        cherish, honor, and protect?</li>

        <li class="list-group-item">
        To shield from terrors known and unknown?
        To lie, to deceive?</li>

        <li class="list-group-item">
        To live a double life,
        to fail to prevent her abduction,
        erase her identity, 
        force her into hiding,
        take away all she has known.</li>
      </ul>
    </main>
  </div>

  <!-- footer -->
  <footer class="footer">
    <div class="bg-dark text-light text-center">
      &copy; 2020.
    </div>
  </footer>

</body>

</html>
{{< / highlight >}}

-- -- --

### What is Next ?

There are, some interesting topic
about <code>Navigation Bar</code> in Bootstrap. 
Consider continue reading
[ [Bootstrap - Navigation Bar - Class][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2020/03/03/bootstrap-navbar-class/

[tutor-01-11-html]:     {{< tutor-html-bs-oc >}}/step-01/11-html-layout.html
[tutor-01-12-html]:     {{< tutor-html-bs-oc >}}/step-01/12-head-cdn.html
[tutor-01-13-html]:     {{< tutor-html-bs-oc >}}/step-01/13-head-local.html
[tutor-01-14-html]:     {{< tutor-html-bs-oc >}}/step-01/14-hero.html
[tutor-01-15-html]:     {{< tutor-html-bs-oc >}}/step-01/15-navbar.html

[tutor-01-css]:             {{< tutor-html-bs-oc >}}/step-01/css/
[tutor-01-css-bootstrap]:   {{< tutor-html-bs-oc >}}/step-01/css/bootstrap.css
[tutor-01-css-main]:        {{< tutor-html-bs-oc >}}/step-01/css/main.css
[tutor-01-css-sticky]:      {{< tutor-html-bs-oc >}}/step-01/css/sticky-footer.css

[image-ss-10-assets]:   {{< assets-frontend >}}/2020/03/bsoc/10-tree-assets.png

[image-ss-11-layout]:   {{< assets-frontend >}}/2020/03/bsoc/11-html-layout.png
[image-ss-13-local]:    {{< assets-frontend >}}/2020/03/bsoc/13-head-local.png
[image-ss-14-hero]:     {{< assets-frontend >}}/2020/03/bsoc/14-hero.png
[image-ss-15-navbar]:   {{< assets-frontend >}}/2020/03/bsoc/15-navbar.png
