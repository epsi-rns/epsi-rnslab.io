---
type   : post
title  : "Bootstrap - Navbar - Class"
date   : 2020-03-03T09:17:35+07:00
slug   : bootstrap-navbar-class
categories: [frontend]
tags      : [bootstrap, css]
keywords  : [navigation bar]
author : epsi
opengraph:
  image: assets/site/images/topics/bootstrap-sass.png

toc    : "toc-2020-03-html-bootstrap-oc-step"

excerpt:
  Make custom theme, step by step.
  Regular navigation bar in Bootstrap as starter.

---

### Preface

> Goal: Common navigation bar.

Bootstrap used to be so popular.
Bootstrap navigation bar could make your site,
similar to any other website.
Before planning customization,
you might need to know common things,
to build your very own navigation bar

-- -- --

### 1: Minimal

Having Bootstrap navigation bar is simple.
You can examine each class in Bootstrap official documentation.

* [gitlab.com/.../21-navbar-minimal.html][tutor-02-21-html].

{{< highlight html >}}
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Should you decide to accept.</title>

  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
</head>

<body>

  <nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <div class="navbar-collapse">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="#">
            Home <span class="sr-only">(current)</span></a>
        </li>
      </ul>
    </div>
  </nav>

</body>

</html>
{{< / highlight >}}

You can see the respective result as below figure.

![Bootstrap Navigation Bar: Minimal][image-ss-21-minimal]

Whether you want to add `sr-only` class for accesability is your choice.
I put `sr-only` whenever possible in real life site,
but I omit them for the sake of simplicity in this tutorial.

#### Directory Preparation

Now consider create directory for respective assets:

* css

* js

* images

{{< highlight bash >}}
❯ tree -d step-02
step-02
└── assets
    ├── css
    ├── images
    └── js

4 directories
{{< / highlight >}}

I change the structure a bit.
Now all assets is in `assets` folder.

![Bootstrap: Directory Preparation][image-ss-20-assets]

We are going to need them for the rest of this chapter.

-- -- --

###  2: Simple

Consider change the `body` part of html a bit.
Put some stuff in the navigation header such as logo.

#### Navigation Bar Menu

Here I represent `navbar-menu` for use with **responsive design later**.

* [gitlab.com/.../22-navbar-simple.html][tutor-02-22-html].

{{< highlight html >}}
<body>

  <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top ">
    <a class="navbar-brand" href="#">
       <img src="images/logo-gear.png" alt="Home" />
    </a>
    <div class="collapse navbar-collapse">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="#"
             >Blog <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">About</a>
        </li>
      </ul>
    </div>
  </nav>

</body>
{{< / highlight >}}

You can have a look at the expected result in image below:

![Bootstrap Navigation Bar: Simple][image-ss-22-simple]

Notice that as bootstrap default,
the `blog` menu and `about` menu won't be shown in small screen.
So you need a desktop screen to see this.
Or access with `burger` menu in the next chapter.

#### Assets: Logo

You are free to use your custom logo.
I actually made some logo,
and here we can use it as an example for this guidance.

* [gitlab.com/.../images/logo-gear.png][tutor-images-logo-gear]

![Bootstrap Navigation Bar: Custom Logo][image-logo-gear]

* Size [width x height]: 96px * 96px

#### Stylesheet

Prepare the head first.

{{< highlight html >}}
<head>
  ...

  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="assets/css/main.css">
</head>
{{< / highlight >}}

And we require to add this element rule in stylesheet :

* [gitlab.com/.../assets/css/main.css][tutor-02-css-main]

{{< highlight html >}}
.navbar-brand img {
  width: 32px;
  height: 32px;
  margin-top:    -10px;
  margin-bottom: -10px;
}
{{< / highlight >}}

-- -- --

### 3: Long

> Stuffed with Items

Consider populate the navigation bar with more menu and stuff.
The item is self explanatory.

* [gitlab.com/.../23-navbar-long.html][tutor-02-23-html].

{{< highlight html >}}
<body>

  <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">

    <a class="navbar-brand" href="#">
       <img src="assets/images/logo-gear.png" alt="Home" />
    </a>

    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#"
           >Blog <span class="sr-only">(current)</span></a>
      </li>
    </ul>

    <button class="navbar-toggler" type="button" 
        data-toggle="collapse" data-target="#navbarCollapse" 
        aria-controls="navbarCollapse" aria-expanded="false" 
        aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarCollapse">

      <ul class="navbar-nav mr-auto">
        <li class="nav-item active dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" 
             role="button" data-toggle="dropdown" 
             aria-haspopup="true" aria-expanded="false">Archives
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="#">By Tags</a>
            <a class="dropdown-item" href="#">By Category</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">By Chronology</a>
          </div>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="#">About</a>
        </li>
      </ul>

      <form class="form-inline mt-2 mt-md-0">
        <input class="form-control mr-sm-2" type="text" 
          placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-light my-2 my-sm-0" 
          type="submit">Search</button>
      </form>

    </div>
  </nav>

</body>
{{< / highlight >}}

You can see the respective result as below figure.

![Bootstrap Navigation Bar: Long Stuffed][image-ss-23-long]

#### Dropdown Menu

Bootstrap has a very nice dropdown menu.

{{< highlight html >}}
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" 
             role="button" data-toggle="dropdown" 
             aria-haspopup="true" aria-expanded="false">Archives
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="#">By Tags</a>
            <a class="dropdown-item" href="#">By Category</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">By Chronology</a>
          </div>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="#">About</a>
        </li>
      </ul>
{{< / highlight >}}

Unfortunately, this require javascript.
As will be explained in the next chapter.

![Bootstrap Navigation Bar: Dropdown Menu][image-ss-23-dropdown]

#### Responsive

Consider have a look at the result in small screen.

![Bootstrap Navigation Bar: Responsive][image-ss-23-responsive]

-- -- --

### What is Next ?

There are more, about <code>Navigation Bar</code> in Bootstrap. 
Consider continue reading
[ [Bootstrap - Navigation Bar - Javascript][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2020/03/04/bootstrap-navbar-javascript/

[tutor-02-21-html]:     {{< tutor-html-bs-oc >}}/step-02/21-navbar-minimal.html
[tutor-02-22-html]:     {{< tutor-html-bs-oc >}}/step-02/22-navbar-simple.html
[tutor-02-23-html]:     {{< tutor-html-bs-oc >}}/step-02/23-navbar-long.html

[tutor-02-css-main]:    {{< tutor-html-bs-oc >}}/step-02/assets/css/main.css

[image-ss-20-assets]:       {{< assets-frontend >}}/2020/03/bsoc/20-tree-assets.png
[image-ss-21-minimal]:      {{< assets-frontend >}}/2020/03/bsoc/21-navbar-minimal.png
[image-ss-22-simple]:       {{< assets-frontend >}}/2020/03/bsoc/22-navbar-simple.png
[image-ss-23-long]:         {{< assets-frontend >}}/2020/03/bsoc/23-navbar-long.png
[image-ss-23-dropdown]:     {{< assets-frontend >}}/2020/03/bsoc/23-navbar-menu-dropdown.png
[image-ss-23-responsive]:   {{< assets-frontend >}}/2020/03/bsoc/23-navbar-responsive.png

[tutor-images-logo-gear]:   {{< tutor-html-bs-oc >}}/step-02/assets/images/logo-gear.png
[image-logo-gear]:          {{< assets-frontend >}}/2019/03/logo-gear.png
