---
type   : post
title  : "Bootstrap - Navbar - Javascript"
date   : 2020-03-04T09:17:35+07:00
slug   : bootstrap-navbar-javascript
categories: [frontend]
tags      : [bootstrap, javascript]
keywords  : [navigation bar, jquery, vue, native javascript, plain javascript]
author : epsi
opengraph:
  image: assets/site/images/topics/bootstrap-sass.png

toc    : "toc-2020-03-html-bootstrap-oc-step"

excerpt:
  Make custom theme, step by step.
  Javascript choice for bootstrap navigation bar.

---

### Preface

> Goal: Javascript choice for bootstrap navigation bar.

As default bootstrap use jQuery for navigation bar.
You require this for both `burger` button and `dropdown`.
If what you need is only the navigation bar,
you might consider to use `plain` javascript to make your site lighter.
You might also consider other javascript such as `Vue` or other,
so here we need to learn to get rid of jQuery,
and use `Vue` version of navigation bar javascript.

-- -- --

### 4: jQuery

Bootstrap come with `jQuery` as default and does not provide any choice.
`jQuery` used to be a good choice one decade ago.
And `jQuery` usage are declining time after time.

In order for the burger button and dropdown to works,
we need javascript however.

* [gitlab.com/.../24-navbar-jquery.html][tutor-02-24-html].

#### Prepare The Head

Consider **jQuery** in the `<head>` element.

{{< highlight html >}}
<head>
  ...

  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="assets/css/main.css">

  <script src="assets/js/jquery-slim.min.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>
</head>
{{< / highlight >}}

You do not need anything else, and your `burger` menu would just works.

#### Burger Button

Add burger button in the `<nav>` element.

{{< highlight html >}}
  <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    ...

    <button class="navbar-toggler" type="button" 
        data-toggle="collapse" data-target="#navbarCollapse" 
        aria-controls="navbarCollapse" aria-expanded="false" 
        aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarCollapse">
      ...
    </div>
  </nav>
{{< / highlight >}}

#### Responsive

Consider have a look at the result in small screen,
and click the burger button to open the menu.
The expected result of clicked will be:

![Bootstrap Navigation Bar: jQuery Burger Button][image-ss-24-burger]

And also click the dropdown menu.

![Bootstrap Navigation Bar: jQuery Dropdown Menu][image-ss-24-dropdown]

-- -- --

### 5: Plain

> How about port the script to native?

The only thing I need for my blog is the navbar,
so why should I need `jQuery`?
How about port the functionality to native script?

#### Prepare The Head

Consider this small script in the `<head>` element.

* [gitlab.com/.../25-navbar-native.html][tutor-02-25-html].

{{< highlight html >}}
<head>
  ...

  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="assets/css/main.css">

  <script src="assets/js/bootstrap-navbar-native.js"></script>
</head>
{{< / highlight >}}

This is all you need.

#### The Script

Luckily I found this good link below,
so that I do not need to reinvent the wheel.

* [Matthew Petroff - Bootstrap Navbar without jQuery][petroff]

The `burger` button can be done with this script below:

{{< highlight javascript >}}
document.addEventListener("DOMContentLoaded", function(event) { 
  // Check for click events on the navbar burger icon
  var toggler = document.getElementsByClassName('navbar-toggler')[0];
  var collapse = document.getElementById('navbarCollapse');
  
  // Toggle if navbar menu is open or closed
  function toggleMenu() {
    collapse.classList.toggle('show');
    console.log('Toggle show class in navbar burger menu.');
  }
  
  // Event listeners
  toggler.addEventListener('click', toggleMenu, false);
});
{{< / highlight >}}

And the complete script for `dropdown` menu,
can simplified as script below:

* [gitlab.com/.../assets/js/bootstrap-navbar-native.js][tutor-02-bb-native].

{{< highlight javascript >}}
document.addEventListener("DOMContentLoaded", function(event) { 
  // Check for click events on the navbar burger icon
  var toggler = document.getElementsByClassName('navbar-toggler')[0];
  var collapse = document.getElementById('navbarCollapse');
  var dropdownButton = document.getElementById('navbarDropdown');
  var dropdownMenu   = document.querySelector('[aria-labelledby="navbarDropdown"]');
  
  // Toggle if navbar menu is open or closed
  function toggleMenu() {
    collapse.classList.toggle('show');
    console.log('Toggle show class in navbar burger menu.');
  }

  function toggleDropdown() {
    dropdownMenu.classList.toggle('show');
    console.log('Toggle show class in dropdown submenu.');
  }
  
  // Close dropdowns when screen becomes big enough to switch to open by hover
  function closeMenusOnResize() {    
    if (collapse.classList.contains('show')) {
        console.log('Remove show class before collapsing.');
        dropdownMenu.classList.remove('show');
    }
  }

  // Event listeners
  window.addEventListener('resize', closeMenusOnResize, false);
  toggler.addEventListener('click', toggleMenu, false);
  dropdownButton.addEventListener('click', toggleDropdown, false);
});
{{< / highlight >}}

I do not port the `transition` part.
And not all behaviour is ported to this native javascript.
I simply have no interest for too much effort.

-- -- --

### 6: Vue

The decade of jQuery has been left in the past.
Now we have a some better javascript framework as an option.
Well, `Vue` is not the only option.
You may choose other javascript framework.

#### Prepare The Head

Consider **Vue** in the `<head>` element.

* [gitlab.com/.../26-navbar-vue.html][tutor-02-26-html].

{{< highlight html >}}
<head>
  ...

  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="assets/css/main.css">
  <script src="assets/js/vue.min.js"></script>
  <script src="assets/js/bootstrap-navbar-vue.js"></script>
</head>
{{< / highlight >}}

#### Burger Button

We need to alter the script a bit.
The burger button in the `<nav>` element is a little bit different.

{{< highlight html >}}
  <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark"
       id="navbar-vue-app">
    ...

    <button class="navbar-toggler" type="button"
        @click="toggleOpenMenu"
        data-toggle="collapse" data-target="#navbarCollapse"
        aria-controls="navbarCollapse" aria-expanded="false" 
        aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarCollapse"
         v-bind:class="{'show': isOpenMenu}">
      ...
    </div>
  </nav> 
{{< / highlight >}}

#### Burger Javascript

The Javascript will examine the `#navbar-vue-app` element.

* [gitlab.com/.../assets/js/bootstrap-navbar-vue.js][tutor-02-bb-vue].

{{< highlight javascript >}}
document.addEventListener("DOMContentLoaded", function(event) { 
  console.log('Document is Ready.');

  new Vue({
    el: '#navbar-vue-app',
    data: {
      isOpenMenu: false
    },
    methods: {
      toggleOpenMenu: function (event) {
        this.isOpenMenu = !this.isOpenMenu;
        console.log('Toggle show class in navbar burger menu.');
      }
    }
  });
});
{{< / highlight >}}

I got the script from:

* [belowthebenthic.com/bulma-burger](http://belowthebenthic.com/bulma-burger/)

And I did a few adjustment to make it suitable for bootstrap.

#### Dropdown Menu

With the same logic we can apply to code to `dropdown` submenu.

{{< highlight html >}}
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active dropdown">
          <a class="nav-link dropdown-toggle" id="navbarDropdown"
             @click="toggleOpenSubmenu" 
             href="#" role="button" data-toggle="dropdown" 
             aria-haspopup="true" aria-expanded="false">Archives
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown"
               v-bind:class="{'show': isOpenSubmenu}">
            <a class="dropdown-item" href="#">By Tags</a>
            <a class="dropdown-item" href="#">By Category</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">By Chronology</a>
          </div>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="#">About</a>
        </li>
      </ul>
{{< / highlight >}}

#### Complete Javascript

The Javascript will examine the `#navbar-vue-app` element.

* [gitlab.com/.../assets/js/bootstrap-navbar-vue.js][tutor-02-bb-vue].

{{< highlight javascript >}}
document.addEventListener("DOMContentLoaded", function(event) { 
  console.log('Document is Ready.');

  new Vue({
    el: '#navbar-vue-app',
    data: {
      isOpenMenu: false,
      isOpenSubmenu: false
    },
    methods: {
      toggleOpenMenu: function (event) {
        this.isOpenMenu = !this.isOpenMenu;
        console.log('Toggle show class in navbar burger menu.');
      },
      toggleOpenSubmenu: function (event) {
        this.isOpenSubmenu = !this.isOpenSubmenu;
        console.log('Toggle show class in dropdown submenu.');
      },
      onResize(event) {
        if (this.isOpenSubmenu) {
          console.log('Remove show class before collapsing.');
          this.isOpenSubmenu = false;
        }
      }
    },
    mounted() {
      // Register an event listener when the Vue component is ready
      window.addEventListener('resize', this.onResize)
    },
    beforeDestroy() {
      // Unregister the event listener before destroying this Vue instance
      window.removeEventListener('resize', this.onResize)
    }
  });

});
{{< / highlight >}}

#### Responsive

The expected result of clicked button burger is similar `native` above.

-- -- --

### What is Next ?

Now you can get rid of `jQuery` peacefully.
For the rest of the tutorial I will use the light `native` javascript.

There are more, about <code>Navigation Bar</code> in Bootstrap. 
Consider continue reading
[ [Bootstrap - Navigation Bar - Icons][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2020/03/05/bootstrap-navbar-icons/
[petroff]:              https://mpetroff.net/2015/03/bootstrap-navbar-without-jquery/

[tutor-02-24-html]:     {{< tutor-html-bs-oc >}}/step-02/24-script-jquery.html
[tutor-02-25-html]:     {{< tutor-html-bs-oc >}}/step-02/25-script-native.html
[tutor-02-26-html]:     {{< tutor-html-bs-oc >}}/step-02/26-script-vue.html

[tutor-02-bb-jquery]:   {{< tutor-html-bs-oc >}}/step-02/assets/js/bootstrap-navbar-jquery.js
[tutor-02-bb-vue]:      {{< tutor-html-bs-oc >}}/step-02/assets/js/bootstrap-navbar-vue.js
[tutor-02-bb-native]:   {{< tutor-html-bs-oc >}}/step-02/assets/js/bootstrap-navbar-native.js

[image-ss-24-burger]:       {{< assets-frontend >}}/2020/03/bsoc/24-navbar-burger-open.png
[image-ss-24-dropdown]:     {{< assets-frontend >}}/2020/03/bsoc/24-navbar-jquery-dropdown.png
