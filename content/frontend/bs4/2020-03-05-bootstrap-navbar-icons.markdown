---
type   : post
title  : "Bootstrap - Navbar - Icons"
date   : 2020-03-05T09:17:35+07:00
slug   : bootstrap-navbar-icons
categories: [frontend]
tags      : [bootstrap, css, javascript]
keywords  : [navigation bar, fontawesome, feather icons, bootstrap icons]
author : epsi
opengraph:
  image: assets/site/images/topics/bootstrap-sass.png

toc    : "toc-2020-03-html-bootstrap-oc-step"

excerpt:
  Make custom theme, step by step.
  Icon set choice for bootstrap navigation bar.

---

### Preface

> Goal: Icon set choice for bootstrap navigation bar.

You do not want to make your navigation bar,
similar to any other website.
So consider a few icons cosmetics.

-- -- --

### 7: FontAwesome

Bootstrap does not come with any font icon.
It takes manual setup.

#### Official Documentation

* [fontawesome.com](https://fontawesome.com/)

#### Prepare The Head

Consider **FontAwesome** as CDN in the `<head>` element.

* [gitlab.com/.../27-icons-awesome.html][tutor-02-27-html].

{{< highlight html >}}
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" 
          href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" 
          integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" 
          crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="assets/css/main.css">
{{< / highlight >}}

#### Example

Consider this dropdown menu as example

{{< highlight html >}}
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" 
             role="button" data-toggle="dropdown" 
             aria-haspopup="true" aria-expanded="false">
            <span class="fa fa-link"></span>&nbsp;Archives
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="#">
            <span class="fa fa-tags"></span>&nbsp;By Tags</a>
            <a class="dropdown-item" href="#">
            <span class="fa fa-folder"></span>&nbsp;By Category</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">
            <span class="fa fa-calendar"></span>&nbsp;By Chronology</a>
          </div>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="#">
          <span class="fas fa-child"></span>&nbsp;About</a>
        </li>
      </ul>
{{< / highlight >}}

And the expected result will be:

![Bootstrap Navigation Bar: FontAwesome in Dropdown Menu][image-ss-27-awesome]

-- -- --

### 8: Bootstrap Icons

Bootstrap also develop their own Icons.
By the time this article written, it is stil in alpha stage.
But you can use the icons right away.

The icons use `SVG`, and this alpha version is bit harder to setup,
compared with FontAwesome.
It does not have as many icons as FontAwesome,
but if you do not need that many icons,
Bootstrap Icons can be a good choice.

#### Official Documentation

* [icons.getbootstrap.com](https://icons.getbootstrap.com/)

#### Prepare The Icons

I copy the icons that I need and put it somewhere in my assets folder.

{{< highlight bash >}}
❯ tree step-02/assets/icons-bootstrap
step-02/assets/icons-bootstrap
├── archive-fill.svg
├── calendar2-day-fill.svg
├── calendar2-fill.svg
├── folder-fill.svg
├── house-door-fill.svg
├── person-circle.svg
├── person-fill.svg
└── tag-fill.svg
{{< / highlight >}}

#### Prepare The Head

Consider this `<head>` element.

* [gitlab.com/.../28-icons-bootstrap.html][tutor-02-28-html].

{{< highlight html >}}
  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="assets/css/main.css">
  <link rel="stylesheet" type="text/css" href="assets/css/icons-bootstrap.css">
{{< / highlight >}}

And the custom stylesheet would be similar as below.

* [gitlab.com/.../icons-bootstrap.css][tutor-02-bi-custom].

{{< highlight css >}}

.icon {
  display: inline-block;
  vertical-align: text-bottom;
  width: 20px;
  height: 20px;

  background-size: 20px 20px;
}

.icon-invert {
  filter: invert(100%) brightness(120%);
}

.archive-fill {
  background: url(../icons-bootstrap/archive-fill.svg) no-repeat;
}
.person-fill { 
  background: url(../icons-bootstrap/person-fill.svg) no-repeat; 
}
.house-door-fill { 
  background: url(../icons-bootstrap/house-door-fill.svg) no-repeat;
}
.tag-fill {
  background: url(../icons-bootstrap/tag-fill.svg) no-repeat;
}
.folder-fill {
  background: url(../icons-bootstrap/folder-fill.svg) no-repeat;
}
.calendar2-day-fill {
  background: url(../icons-bootstrap/calendar2-day-fill.svg) no-repeat;
}
{{< / highlight >}}

Note that this stylesheet is tailor made,
so you can named the class with any name that you like.

#### Example

Consider this dropdown menu as example

{{< highlight html >}}
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" 
             role="button" data-toggle="dropdown" 
             aria-haspopup="true" aria-expanded="false">
            <span class="icon icon-invert archive-fill"></span>&nbsp;Archives
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="#">
            <span class="icon tag-fill"></span>&nbsp;By Tags</a>
            <a class="dropdown-item" href="#">
            <span class="icon folder-fill"></span>&nbsp;By Category</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">
            <span class="icon calendar2-day-fill"></span>&nbsp;By Chronology</a>
          </div>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="#">
          <span class="icon icon-invert person-fill"></span>&nbsp;About</a>
        </li>
      </ul>
{{< / highlight >}}

And the expected result will be:

![Bootstrap Navigation Bar: Bootstrap Icons in Dropdown Menu][image-ss-28-bootstrap]

-- -- --

### 9: Feather Icons

Our next example is `feather` icons.
This icon set is also use `SVG`,
but it is loaded by javascript,
so this makes things easier for us.

If you do not need a lot of  icons,
Feather Icons can also be a good choice.

#### Official Documentation

* [feathericons.com](https://feathericons.com/)

#### Prepare The Assets

We need two assets:

* Javascript from official site: `js/feather.min.js`

* Custom Stylesheet: `css/icons-feather.css`

#### Prepare The Head

Consider this `<head>` element.

* [gitlab.com/.../29-icons-feather.html][tutor-02-29-html].

{{< highlight html >}}
<head>
  ...

  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="assets/css/main.css">
  <link rel="stylesheet" type="text/css" href="assets/css/icons-feather.css">

  <script src="assets/js/bootstrap-navbar-native.js"></script>
  <script src="assets/js/feather.min.js"></script>
</head>
{{< / highlight >}}

And the custom stylesheet would be similar as below.

* [gitlab.com/.../icons-feather.css][tutor-02-feather].

{{< highlight css >}}
.feather {
  width: 20px;
  height: 20px;
  stroke: currentColor;
  stroke-width: 2;
  stroke-linecap: round;
  stroke-linejoin: round;
  fill: none;
  
  vertical-align: text-top;
}

.feather-14 {
  width: 14px;
  height: 14px;
}
{{< / highlight >}}

Note that the name in this stylesheet is hardcoded.

#### Prepare The Body

At the bottom of the `<body>` element.

{{< highlight html >}}
<body>
  ...

  <script>
    feather.replace()
  </script>
</body>
{{< / highlight >}}

#### Example

Consider this dropdown menu as example

{{< highlight html >}}
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" 
             role="button" data-toggle="dropdown" 
             aria-haspopup="true" aria-expanded="false">
            <i data-feather="archive"></i>&nbsp;Archives
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="#">
            <i data-feather="tag"></i>&nbsp;By Tags</a>
            <a class="dropdown-item" href="#">
            <i data-feather="folder"></i>&nbsp;By Category</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">
            <i data-feather="calendar"></i>&nbsp;By Chronology</a>
          </div>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="#">
          <i data-feather="user"></i>&nbsp;About</a>
        </li>
      </ul>
{{< / highlight >}}

And the expected result will be:

![Bootstrap Navigation Bar: Feather Icons in Dropdown Menu][image-ss-29-feather]

-- -- --

### What is Next ?

For the rest of the tutorial I will use the light `feather` icons.

I think that is all,
about <code>Navigation Bar</code> variation in Bootstrap. 

There is, an introduction about <code>SASS</code> in Bootstrap. 
Consider continue reading [ [Bootstrap - SASS - Introduction][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2020/03/07/bootstrap-sass-intro/

[tutor-02-27-html]:     {{< tutor-html-bs-oc >}}/step-02/27-icons-awesome.html
[tutor-02-28-html]:     {{< tutor-html-bs-oc >}}/step-02/28-icons-bootstrap.html
[tutor-02-29-html]:     {{< tutor-html-bs-oc >}}/step-02/29-icons-feather.html
[tutor-02-bi-custom]:   {{< tutor-html-bs-oc >}}/step-02/assets/css/icons-bootstrap.css
[tutor-02-feather]:     {{< tutor-html-bs-oc >}}/step-02/assets/css/icons-feather.css

[image-ss-27-awesome]:      {{< assets-frontend >}}/2020/03/bsoc/27-navbar-awesome.png
[image-ss-28-bootstrap]:    {{< assets-frontend >}}/2020/03/bsoc/28-navbar-boostrap.png
[image-ss-29-feather]:      {{< assets-frontend >}}/2020/03/bsoc/29-navbar-feather.png
