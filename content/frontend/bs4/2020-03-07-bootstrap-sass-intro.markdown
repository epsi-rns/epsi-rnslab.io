---
type   : post
title  : "Bootstrap - Sass - Introduction"
date   : 2020-03-07T09:17:35+07:00
slug   : bootstrap-sass-intro
categories: [frontend]
tags      : [bootstrap, sass]
keywords  : [custom sass, altering bootstrap]
author : epsi
opengraph:
  image: assets/site/images/topics/bootstrap-sass.png

toc    : "toc-2020-03-html-bootstrap-oc-step"

excerpt:
  Make custom theme, step by step.
  Using Bootstrap SASS for the first time

---

### Preface

> Goal: Using Bootstrap SASS for the first time

Understanding SASS is a must for theme maker.
You should be able to alter bootstrap variables.

#### Reading

If you do not have any idea about SASS you should read this first:

* <https://sass-lang.com/>

-- -- --

### 1: Overview

First thing first.

#### Directory Preparation

Add SASS to your current working directory.

* [gitlab.com/.../sass/][tutor-03-sass].

{{< highlight bash >}}
❯ tree -d step-03 -L 2
step-03
├── assets
│   ├── css
│   ├── images
│   └── js
└── sass
    ├── bootstrap
    └── css

7 directories
{{< / highlight >}}

![Bootstrap SASS: Directory Preparation][image-ss-30-tree-sass]

We are going to need them for the rest of this chapter.

#### Download Bootstrap SASS

Add Bootstrap to your vendors directory.

* [gitlab.com/.../sass/bootstrap/][tutor-03-bootstrap-dir].

{{< highlight bash >}}
❯ tree -d step-03/sass/bootstrap -L 2
step-03/sass/bootstrap
├── mixins
├── utilities
└── vendor

3 directories
{{< / highlight >}}

Now you should see inside Bootstrap.

![Bootstrap SASS: Inside Bootstrap][image-ss-30-tree-twbs]

#### Custom SASS

Now, it is about the right time to create custom SASS.
Let's say we create this

* [gitlab.com/.../sass/css/bootstrap.scss][tutor-03-sass-bootstrap].

{{< highlight scss >}}
@import 
  "../bootstrap/functions",
  "variables",
  "../bootstrap/bootstrap"
;
{{< / highlight >}}

The import partials is from `sass/css`.
We want to compile it to `assets/css`.

#### SASS Tools

You may compile with any SASS compiler that you need.

* [SASS Compiler][sass-compiler]

I made a `bash` shortcut to my `dart sass` binary.

{{< highlight bash >}}
alias dart-sass='/media/Works/bin/dart-sass/sass'
{{< / highlight >}}

Windows user can use `chocolatey` to install `sass`.

#### Compile SASS

Consider compile to `css` directory.

{{< highlight bash >}}
❯ cd step-03
❯ dart-sass --watch -I sass sass/css:assets/css
Compiled sass/css/bootstrap.scss to assets/css/bootstrap.css.
Compiled sass/css/main.scss to assets/css/main.css.
Sass is watching for changes. Press Ctrl-C to stop.
{{< / highlight >}}

![Bootstrap SASS: Compile][image-ss-30-sass-compile]

Now you should see generated CSS.

{{< highlight bash >}}
❯ tree assets/css
assets/css
├── bootstrap.css
├── bootstrap.css.map
├── main.css
└── main.css.map

0 directories, 4 files
{{< / highlight >}}

![Bootstrap SASS: Generated CSS][image-ss-30-generated]

This should be the same as regular official Bootstrap.

-- -- --

### 2: Example: Altering Bootstrap

This is the basic bootstrap customization.

#### The Sylesheet

Altering standard Bootstrap is simple.
Consider change the behaviour by setting initial variable.

* [gitlab.com/.../sass/css/_variables.scss][tutor-03-sass-variables].
  
{{< highlight scss >}}
// Variables

$pink:  #e83e8c !default;
$primary:  darken($pink, 10%) !default;
{{< / highlight >}}

![Bootstrap SASS: Example: Pink Navigation Bar][image-ss-31-navbar-pink]

-- -- --

### What is Next ?

There is, a topic about <code>Theme</code> in Bootstrap.
About making your tailor made stylesheets.
Consider continue reading [ [Bootstrap - SASS - Custom Class][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2020/03/08/bootstrap-sass-custom-class/
[sass-compiler]:        https://epsi-rns.gitlab.io/frontend/2019/01/13/sass-common-compiler/

[tutor-03-31-html]:     {{< tutor-html-bs-oc >}}/step-03/31-navbar.html

[tutor-03-sass]:            {{< tutor-html-bs-oc >}}/step-03/sass/
[tutor-03-bootstrap-dir]:   {{< tutor-html-bs-oc >}}/step-03/sass/bootstrap/
[tutor-03-sass-bootstrap]:  {{< tutor-html-bs-oc >}}/step-03/sass/css/bootstrap.scss
[tutor-03-sass-variables]:  {{< tutor-html-bs-oc >}}/step-03/sass/css/_variables.scss

[image-ss-30-tree-sass]:    {{< assets-frontend >}}/2020/03/bsoc/30-tree-assets-sass.png
[image-ss-30-tree-twbs]:    {{< assets-frontend >}}/2020/03/bsoc/30-tree-assets-sass-bootstrap.png
[image-ss-30-tree-css]:     {{< assets-frontend >}}/2020/03/bsoc/30-tree-assets-css.png
[image-ss-30-sass-compile]: {{< assets-frontend >}}/2020/03/bsoc/30-sass-compile.png
[image-ss-30-generated]:    {{< assets-frontend >}}/2020/03/bsoc/30-generated-css.png
[image-ss-30-assets-sass]:  {{< assets-frontend >}}/2020/03/bsoc/30-tree-assets-sass.png

[image-ss-31-sass-compile]: {{< assets-frontend >}}/2020/03/bsoc/31-sass-compile.png
[image-ss-31-navbar-pink]:  {{< assets-frontend >}}/2020/03/bsoc/31-navbar-pink.png
