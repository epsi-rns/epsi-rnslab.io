---
type   : post
title  : "Bootstrap - Sass - Custom Class"
date   : 2020-03-08T09:17:35+07:00
slug   : bootstrap-sass-custom-class
categories: [frontend]
tags      : [bootstrap, sass]
keywords  : [custom sass, altering bootstrap]
author : epsi
opengraph:
  image: assets/site/images/topics/bootstrap-sass.png

toc    : "toc-2020-03-html-bootstrap-oc-step"

excerpt:
  Make custom theme, step by step.
  Making modular SASS for your theme.

---

### Preface

> Goal: Making modular SASS for your theme.

Understanding SASS is a must for theme maker.
You should be able to make your tailor made stylesheets.

#### Reading

Again, the official documentation is your friend.
If you do not have any idea about SASS you should read this first:

* <https://sass-lang.com/>


-- -- --

### 3: Example: Modular SASS

> You can add as many additional custom SASS in your theme.

For a starter, this SASS is completely separated with bootstrap.
We are going to use bootstrap variables inside additional SASS later,
in the next chapter.

#### Directory Preparation

We need proper looks for our project.
From the beginning, consider organizer your stuff well.

* [gitlab.com/.../sass/main][tutor-03-sass-main-dir].

{{< highlight bash >}}
❯ tree sass/css/main
sass/css/main
├── _decoration.scss
├── _icons-feather.scss
├── _layout-content.scss
├── _layout-page.scss
├── _logo.scss
└── _sticky-footer.scss

0 directories, 6 files
{{< / highlight >}}

![Bootstrap SASS: Additional Directory Preparation][image-ss-30-tree-main]

* __

#### The Sylesheet: Additional SASS

And of course the `main.scss` will follow the above file list,
to relative to `sass/css` directory.

* [gitlab.com/.../sass/css/main.scss][tutor-03-sass-main].

{{< highlight scss >}}
@import 
  "main/layout-page",
  "main/layout-content",
  "main/logo",
  "main/decoration",
  "main/sticky-footer",
  "main/icons-feather"
;
{{< / highlight >}}

#### The Sylesheet: Compiled CSS

The result of compiled `CSS` can be seen in left pane in figure below:

![Bootstrap SASS: Compiled CSS][image-ss-30-panes-sass]

In the right pane is, simple example of modular `SASS`.
The `SASS` is exactly the same as `CSS`,
without nested curly bracket.

#### Stylesheets: Some Sources

If you wish this is the stylesheets in SASS, required for this page.
We can make this very modular, as shown in below file artefacts:

I put a nice background, using subtle pattern.

* [gitlab.com/.../sass/css/main/_decoration.scss][tutor-03-sass-decoration].

{{< highlight scss >}}
body {
  background-image: url("../images/light-grey-terrazzo.png");
}
{{< / highlight >}}

* [gitlab.com/.../sass/css/main/_layout-page.scss][tutor-03-sass-layout-page].

{{< highlight scss >}}
.layout-base {
  padding-top: 5rem;
  padding-bottom: 1rem;
}
{{< / highlight >}}

And grabbed from official bootstrap site.

* [gitlab.com/.../sass/css/main/_sticky-footer.scss][tutor-03-sass-sticky-footer].

{{< highlight scss >}}
html {
  position: relative;
  min-height: 100%;
}
body {
  margin-bottom: 60px;
}
.footer {
  position: absolute;
  bottom: 0;
  width: 100%;
  height: 60px;
  line-height: 60px;
}
{{< / highlight >}}

We have to make these stylesheets modular,
because we are going to make a lot of changes for each step,
for the rest of article series.

#### More Stylesheets

We also require these for later use:

* `_logo.scss`,

* `_icons-feather.scss`

* `_layout-content.scss`

But for now just leave it empty.

#### Generated Stylesheet

You should see the result of the compilation in

* [gitlab.com/.../assets/css/main.scss][tutor-03-css-main].

{{< highlight css >}}
.layout-base {
  padding-top: 5rem;
  padding-bottom: 1rem;
}

...
{{< / highlight >}}

The original file is so long.
You can check at the repository.

#### Prepare The Head

Consider the `<head>` element.

{{< highlight html >}}
<head>
  ...

  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="assets/css/main.css">
</head>
{{< / highlight >}}

#### The Document: Example Page.

What good is it SASS compilation without example page?
Here we are, I represent a page, complete with header (Navigation Bar),
main content and footer.

* [gitlab.com/.../31-navbar.html][tutor-03-31-html].

{{< highlight html >}}
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Desperate times, desperate measures.</title>

  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="assets/css/main.css">
</head>

<body>
  <!-- header -->
  <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-primary">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#"
           >Home <span class="sr-only">(current)</span></a>
      </li>
    </ul>
  </nav>

  <!-- main -->
  <div class="layout-base">
    <main role="main" class="container">
      <ul class="list-group">
        <li class="list-group-item">
        To have, to hold, to love,
        cherish, honor, and protect?</li>

        <li class="list-group-item">
        To shield from terrors known and unknown?
        To lie, to deceive?</li>

        <li class="list-group-item">
        To live a double life,
        to fail to prevent her abduction,
        erase her identity, 
        force her into hiding,
        take away all she has known.</li>
      </ul>
    </main>
  </div>

  <!-- footer -->
  <footer class="footer">
    <div class="bg-primary text-light text-center">
      &copy; 2020.
    </div>
  </footer>

</body>
</html>
{{< / highlight >}}

You can have a look at the expected result in image below:

![Bootstrap SASS: Example: Navigation Bar][image-ss-31-navbar]

-- -- --

### What is Next ?

There is, a topic about <code>Theme</code> in Bootstrap.
Using ready to use theme with Bootswatch.
Consider continue reading [ [Bootstrap - SASS - Bootswatch][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2020/03/09/bootstrap-sass-bootswatch/
[sass-compiler]:        https://epsi-rns.gitlab.io/frontend/2019/01/13/sass-common-compiler/

[tutor-03-31-html]:     {{< tutor-html-bs-oc >}}/step-03/31-navbar.html

[tutor-03-sass-main-dir]:   {{< tutor-html-bs-oc >}}/step-03/sass/css/main/
[tutor-03-sass-main]:       {{< tutor-html-bs-oc >}}/step-03/sass/css/main.scss
[tutor-03-css-main]:        {{< tutor-html-bs-oc >}}/step-03/assets/css/main.scss

[tutor-03-sass-decoration]:     {{< tutor-html-bs-oc >}}/step-03/sass/css/main/_decoration.scss
[tutor-03-sass-layout-page]:    {{< tutor-html-bs-oc >}}/step-03/sass/css/main/_layout-page.scss
[tutor-03-sass-sticky-footer]:  {{< tutor-html-bs-oc >}}/step-03/sass/css/main/_sticky-footer.scss

[image-ss-30-tree-main]:    {{< assets-frontend >}}/2020/03/bsoc/30-tree-assets-sass-main.png
[image-ss-30-assets-sass]:  {{< assets-frontend >}}/2020/03/bsoc/30-tree-assets-sass.png
[image-ss-30-panes-sass]:   {{< assets-frontend >}}/2020/03/bsoc/30-panes-sass-main.png

[image-ss-31-navbar]:       {{< assets-frontend >}}/2020/03/bsoc/31-navbar.png

