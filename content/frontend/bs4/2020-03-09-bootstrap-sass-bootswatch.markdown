---
type   : post
title  : "Bootstrap - Sass - Bootswatch"
date   : 2020-03-09T09:17:35+07:00
slug   : bootstrap-sass-bootswatch
categories: [frontend]
tags      : [bootstrap, sass]
keywords  : [modular sass, swatch, fontawesome]
author : epsi
opengraph:
  image: assets/site/images/topics/bootstrap-sass.png

toc    : "toc-2020-03-html-bootstrap-oc-step"

excerpt:
  More about custom theme, step by step.
  Using a ready to used Bootstrap SASS theme.

---

### Preface

> Goal: Using a ready to used Bootstrap SASS theme.

This article is integral part of previous article.
You should read previous article before continue.

-- -- --

### 1: Bootstrap Swatch

Working with theme is fun.

![Bootstrap SASS: Bootswatch Animation][image-ss-32-swatch-animation]

#### The Sylesheet: Original Code

There is a ready to used Bootstrap theme named Bootstrap Swatch.

* [bootswatch.com](https://bootswatch.com/)

Or the source code, if you prefer:

* [github.com/thomaspark/bootswatch](https://github.com/thomaspark/bootswatch)

Consider have a look at the original code:

* [gitlab.com/.../cerulean/bootstrapswatch.scss][tutor-03-cerulean].

{{< highlight scss >}}
// Cerulean 4.1.3
// Bootswatch


@mixin btn-shadow($color){
  @include gradient-y-three-colors(lighten($color, 8%), $color, 60%, darken($color, 4%));
}

...
{{< / highlight >}}

This is  along code

#### The Sylesheet: Using  Bootswatch

Now for use with our project, adapt with our directory structure.

* [gitlab.com/.../sass/bootswatch.scss][tutor-03-sass-bootswatch].

{{< highlight scss >}}
@import
    "../bootswatch/cerulean/variables",
    "../bootstrap/bootstrap",
    "../bootswatch/cerulean/bootswatch"
;
{{< / highlight >}}

Consider compile

{{< highlight bash >}}
$ dart-sass --watch -I sass sass/css:assets/css --style=compressed --no-source-map
Sass is watching for changes. Press Ctrl-C to stop.

Compiled sass/css/bootswatch.scss to assets/css/bootswatch.css.
{{< / highlight >}}

![Bootstrap SASS: Compile Bootstrapswatch][image-ss-32-compile-swatch]

#### Prepare The Head

Consider the `<head>` element.

{{< highlight html >}}
<head>
  ...

  <link rel="stylesheet" type="text/css" href="assets/css/bootswatch.css">
  <link rel="stylesheet" type="text/css" href="assets/css/main.css">
</head>
{{< / highlight >}}

#### The Document: Example

Consider using our last example, except change the `bootstrap.css`
to any selected `bootswatch.css` theme.

* [gitlab.com/.../32-bootswatch.html][tutor-03-32-html].

{{< highlight html >}}
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Desperate times, desperate measures.</title>

  <link rel="stylesheet" type="text/css" href="assets/css/bootswatch.css">
  <link rel="stylesheet" type="text/css" href="assets/css/main.css">
</head>

<body>
  <!-- header -->
  <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-primary">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#"
           >Home <span class="sr-only">(current)</span></a>
      </li>
    </ul>
  </nav>

  <!-- main -->
  <div class="layout-base">
    <main role="main" class="container">
      <ul class="list-group">
        <li class="list-group-item active">
        <h4>Your Mission!</h4></li>

        <li class="list-group-item">
        To have, to hold, to love,
        cherish, honor, and protect?</li>

        <li class="list-group-item">
        To shield from terrors known and unknown?
        To lie, to deceive?</li>

        <li class="list-group-item">
        To live a double life,
        to fail to prevent her abduction,
        erase her identity, 
        force her into hiding,
        take away all she has known.</li>
      </ul>
    </main>
  </div>

  <!-- footer -->
  <footer class="footer">
    <div class="bg-primary text-light text-center">
      &copy; 2020.
    </div>
  </footer>

</body>
</html>
{{< / highlight >}}

And open the html artefact using file manager.

![Bootstrap SASS: Bootswatch Theme][image-ss-32-bootswatch]

-- -- --

### What is Next ?

More SASS customization is our next topic.
Just a few example, but diverse.
This is going to be more interesting.
Consider continue reading [ [Bootstrap - SASS - Responsive][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2020/03/10/bootstrap-sass-responsive/

[tutor-03-32-html]:     {{< tutor-html-bs-oc >}}/step-03/32-bootswatch.html

[tutor-03-cerulean]:    {{< tutor-html-bs-oc >}}/step-03/sass/bootswatch/cerulean/bootswatch.scss

[tutor-03-sass-bootswatch]:     {{< tutor-html-bs-oc >}}/step-03/sass/css/bootswatch.scss

[image-ss-32-compile-swatch]:   {{< assets-frontend >}}/2020/03/bsoc/32-compile-swatch.png
[image-ss-32-bootswatch]:       {{< assets-frontend >}}/2020/03/bsoc/32-bootswatch-cerulean.png
[image-ss-32-swatch-animation]: {{< assets-frontend >}}/2020/03/bsoc/32-bootswatch-animation.gif
