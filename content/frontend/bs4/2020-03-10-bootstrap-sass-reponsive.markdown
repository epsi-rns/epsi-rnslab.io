---
type   : post
title  : "Bootstrap - Sass - Responsive"
date   : 2020-03-10T09:17:35+07:00
slug   : bootstrap-sass-responsive
categories: [frontend]
tags      : [bootstrap, sass]
keywords  : [layout, responsive spacing, gap, maxwidth]
author : epsi
opengraph:
  image: assets/site/images/topics/bootstrap-sass.png

toc    : "toc-2020-03-html-bootstrap-oc-step"

excerpt:
  More about custom theme, step by step.
  Reponsive Layout using Bootstrap.

---

### Preface

> Goal: Reponsive Layout using Bootstrap.

-- -- --

### 1: General Preview

This below is general preview of,
what responsive page that we want to achieve.
Just keep in mind that,
responsive design is based on consideration.
So different website might have different preview.

![Bootstrap OC: General Preview of Responsive Page Design][image-png-layout-page]

Source image is available in inkscape SVG format,
so you can modify, and make your own preview quickly.

* [Responsive Page: Image Source][image-svg-layout-page]

-- -- --

### 2: Page Skeleton

So far we have these three elements:

* Header part: Navigation Bar.

* Main part: Content

* Footer part.

#### Layout

The internal layout for this show case can be summarized as below:

{{< highlight html >}}
<html>
<head>
  <!--head section -->
</head>
<body>

  <!-- header -->
  <nav role="navigation">
       ...
  </nav>
 
  <!-- main: responsive layout -->
  <div class="row">

    <main class="col-md-8">
      <!-- content -->
    </main>

    <aside class="col-md-4">
      <!-- aside -->
    </aside>

  </div>

  <!-- footer -->
  <footer>
    ...
  </footer>

</body>
</html>
{{< / highlight >}}

Of course you can make your own skeleton to suit your needs.

#### The Columns

Now we can have this two elements inside `main` parts.

* `main` element: page content or blog post content

* `aside` element: side panel

What is this `col-md-8` and `col-md-4` class?
Bootstrap is twelve columns based layout.
This means the total column should be `12`.
For `tow-third` plus `one-third` layout,
you use `col-md-8` and `col-md-4`.

#### Example Layout Page

Now we require real example page, as usual.

* [gitlab.com/.../33-responsive-minimal.html][tutor-03-33-html].

We would like to apply a simple responsive design for this two elements.

{{< highlight html >}}
  <!-- responsive layout -->
  <div class="row layout-base">

    <main class="col-md-8 p-4 bg-warning">
       Page Content
    </main>

    <aside class="col-md-4 p-4 bg-info">
      Side Menu
    </aside>

  </div>
{{< / highlight >}}

For desktop screen the result is similar to below figure:

![Bootstrap Layout: Desktop No Gap][image-ss-33-desktop-nogap]

For mobile screen the result is similar to below figure:

![Bootstrap Layout: Mobile No Gap][image-ss-33-mobile-nogap]

-- -- --

### 3: Responsive Spacing

What if I want gap between column,
vertically and horizontally?

#### Vertical on Mobile Screen

This `layout-content` just a custom class.

* [gitlab.com/.../sass/css/main/_layout-content.scss][tutor-03-sass-layout-content].

{{< highlight scss >}}
// responsive - mobile
.layout-base main {
  margin-bottom: 20px;
}

@media only screen and (min-width: 768px) {
  .layout-base main {
    margin-bottom: 0px;
  }
}
{{< / highlight >}}

This will only give gaps, for mobile screen.

For mobile screen the result is similar to below figure:

![Bootstrap Layout: Mobile Gap][image-ss-33-mobile-gap]

#### Horizontal on Desktop Screen

Unfortunately, bootstrap can't do that.
We will deal with this in next chapter with wrapper class.

-- -- --

### 4: Responsive Maxwidth

And, what is this `maxwidth` class anyway ?

I have a smartphone, tablet, sometimes medium screen,
and mostly I'm working with large screen.
What is good for my regular screen, looks ugly in large screen.
My solution is to create maxwidth,
so my content would not be stretched horizontally.

#### Container Class

> Why not using Container Class?

Sometimes we can't rely on bootstrap class.
For example `container` is good for some case.
`container` class has its own maximum width.
But it has margin issue with other case.
So we have to make our own maxwidth class.

#### Stylesheet

* [gitlab.com/.../sass/css/main/_layout-page.scss][tutor-03-sass-layout-page].

Add this rule below:

{{< highlight scss >}}
.maxwidth {
  margin-right: 0;
  margin-left: 0;
}

@media only screen and (min-width: 1200px) {
  .maxwidth {
    max-width: 1200px;
    margin-right: auto;
    margin-left: auto;
  }
}
{{< / highlight >}}

![Bootstrap Layout: Wide Maxwidth][image-ss-33-wide-maxwith]

Remember that in this chapter,
we haven't access bootstrap variables yet.
In the next chapter we are going to use responsive mixin.

{{< highlight scss >}}
@include media-breakpoint-up(xl) {
  ...
}
{{< / highlight >}}

-- -- --

### 5: Example Content

> Put the whole stuff together 

Before we step our foot into next chapter,
consider to put the whole stuff together.
And also we need to have a look,
at a complete page with content.
So you can have the feels.

There is nothing new with this.
Just a conclusion of what we have learn.

#### Stylesheet

These below are additional stylesheet that we need.

* [gitlab.com/.../sass/css/main/_logo.scss][tutor-03-sass-logo].

{{< highlight scss >}}
.navbar-brand img {
  width: 32px;
  height: 32px;
  margin-top:    -10px;
  margin-bottom: -10px;
}
{{< / highlight >}}

* [gitlab.com/.../sass/css/main/_icons-feather.scss][tutor-03-sass-icons-feather].

{{< highlight scss >}}
.feather {
  width: 20px;
  height: 20px;
  stroke: currentColor;
  stroke-width: 2;
  stroke-linecap: round;
  stroke-linejoin: round;
  fill: none;
  
  vertical-align: text-top;
}
{{< / highlight >}}

#### HTML Document

* [gitlab.com/.../34-responsive-content.html][tutor-03-34-html].

I would like to apply a simple responsive design for this two elements.

{{< highlight html >}}
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Desperate times, desperate measures.</title>

  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="assets/css/main.css">

  <script src="assets/js/bootstrap-navbar-native.js"></script>
  <script src="assets/js/feather.min.js"></script>
</head>

<body>

  <!-- header -->
  <nav class="navbar navbar-expand-md navbar-dark maxwidth fixed-top bg-dark">

    <a class="navbar-brand" href="#">
       <img src="assets/images/logo-gear.png" alt="Home" />
    </a>

    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">
          <i data-feather="activity"></i>&nbsp;Blog
          <span class="sr-only">(current)</span></a>
      </li>
    </ul>

    <button class="navbar-toggler" type="button" 
        data-toggle="collapse" data-target="#navbarCollapse" 
        aria-controls="navbarCollapse" aria-expanded="false" 
        aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarCollapse">

      <ul class="navbar-nav mr-auto">
        <li class="nav-item active dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" 
             role="button" data-toggle="dropdown" 
             aria-haspopup="true" aria-expanded="false">
            <i data-feather="archive"></i>&nbsp;Archives
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="#">
            <i data-feather="tag"></i>&nbsp;By Tags</a>
            <a class="dropdown-item" href="#">
            <i data-feather="folder"></i>&nbsp;By Category</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">
            <i data-feather="calendar"></i>&nbsp;By Chronology</a>
          </div>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="#">
          <i data-feather="user"></i>&nbsp;About</a>
        </li>
      </ul>

      <form class="form-inline mt-2 mt-md-0">
        <input class="form-control mr-sm-2" type="text" 
          placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-light my-2 my-sm-0" 
          type="submit">Search</button>
      </form>

    </div>
  </nav>

  <!-- responsive main -->
  <div class="row layout-base maxwidth">

    <main class="col-md-8 p-4 bg-warning">
      <header>
        <h2>Your mission. Good Luck!</h2>
      </header>

      <article>
        <ul class="list-group">
          <li class="list-group-item">
          To have, to hold, to love,
          cherish, honor, and protect?</li>

          <li class="list-group-item">
          To shield from terrors known and unknown?
          To lie, to deceive?</li>

          <li class="list-group-item">
          To live a double life,
          to fail to prevent her abduction,
          erase her identity, 
          force her into hiding,
          take away all she has known.</li>
        </ul>
      </article>
    </main>

    <aside class="col-md-4 p-4 bg-dark text-light">
      Side Menu
    </aside>

  </div>

  <!-- footer -->
  <footer class="footer">
    <div class="maxwidth bg-dark text-light text-center">
      &copy; 2020.
    </div>
  </footer>

  <script>
    feather.replace()
  </script>

</body>
</html>
{{< / highlight >}}

For desktop screen the result is similar to below figure:

![Bootstrap Layout: Desktop Content][image-ss-34-desktop-content]

-- -- --

### 6: Default Gutter

Bootstrap, is actually has a default gutter.
The issue is, you have to put the those `element` inside a `col class`.

#### HTML Document

I would like to apply a default bootstrap gutter on,
simple responsive design for this two elements.
This would have a slight difference with previous example.

* [gitlab.com/.../35-responsive-gutter.html][tutor-03-35-html].

{{< highlight html >}}
  <!-- responsive main -->
  <div class="row layout-base maxwidth">

    <main class="col-md-8">
      <section class="p-4 bg-warning h-100">
      <header>
        <h2>Your mission. Good Luck!</h2>
      </header>

      <article>
        ...
      </article>
      </section>
    </main>

    <aside class="col-md-4">
      <section class="p-4 bg-dark text-light h-100">
      Side Menu
      </section>
    </aside>

  </div>
{{< / highlight >}}

For desktop screen the result is similar to below figure:

![Bootstrap Layout: Default Gutter][image-ss-35-default-gutter]

-- -- --

### What is Next ?

We are going to continue to enhance our responsive material.
But before we do that, let me introduce you about `open color`.
Consider continue reading [ [Bootstrap OC - Sass - Open Color][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2020/03/11/bootstrap-oc-sass-open-color/

[tutor-03-33-html]:     {{< tutor-html-bs-oc >}}/step-03/33-responsive-minimal.html
[tutor-03-34-html]:     {{< tutor-html-bs-oc >}}/step-03/34-responsive-content.html
[tutor-03-35-html]:     {{< tutor-html-bs-oc >}}/step-03/35-responsive-content.html

[tutor-03-sass-layout-page]:    {{< tutor-html-bs-oc >}}/step-03/sass/css/main/_layout-page.scss
[tutor-03-sass-icons-feather]:  {{< tutor-html-bs-oc >}}/step-03/sass/css/main/_icons-feather.scss
[tutor-03-sass-layout-content]: {{< tutor-html-bs-oc >}}/step-03/sass/css/main/_layout-content.scss
[tutor-03-sass-logo]:           {{< tutor-html-bs-oc >}}/step-03/sass/css/main/_logo.scss

[image-ss-33-desktop-nogap]:{{< assets-frontend >}}/2020/03/bsoc/33-layout-desktop-nogap.png
[image-ss-33-mobile-nogap]: {{< assets-frontend >}}/2020/03/bsoc/33-layout-mobile-nogap.png
[image-ss-33-mobile-gap]:   {{< assets-frontend >}}/2020/03/bsoc/33-layout-mobile-gap.png
[image-ss-33-wide-maxwith]: {{< assets-frontend >}}/2020/03/bsoc/33-layout-wide-maxwidth.png

[image-ss-34-desktop-content]:  {{< assets-frontend >}}/2020/03/bsoc/34-layout-desktop-content.png
[image-ss-35-default-gutter]:   {{< assets-frontend >}}/2020/03/bsoc/35-layout-default-gutter.png

[image-png-layout-page]:    {{< assets-frontend >}}/2019/12/layout-page.png
[image-svg-layout-page]:    {{< assets-frontend >}}/2019/12/layout-page.svg
