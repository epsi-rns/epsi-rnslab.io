---
type   : post
title  : "Bootstrap OC - Sass - Open Color"
date   : 2020-03-11T09:17:35+07:00
slug   : bootstrap-oc-sass-open-color
categories: [frontend]
tags      : [bootstrap, sass]
keywords  : [open color, z-depth, shadow, gradient]
author : epsi
opengraph:
  image: assets/site/images/topics/bootstrap-sass.png

toc    : "toc-2020-03-html-bootstrap-oc-step"

excerpt:
  Custom theme using Open Color, step by step.
  Open Color as basic pallete for theme customization.

---

### Preface

> Goal: Open Color as basic pallete for theme customization.

I have been using `Google Material Color` for a few years.
I search for alternative and find this `Open Color` with `MIT license`.
The color is not as nice as `GMC`, 
but `OC` is the best alternative so far.
Actually, I cannot find any open source pallete that can beat these two.

#### Official Site

* [yeun.github.io/open-color](https://yeun.github.io/open-color/)

-- -- --

### 1: Open Color: SASS Build

> Inspired by Materialize CSS

I use the color from `OC` pallete,
but I strip down the `sass` version,
and make my own color classes.

Inspired by Materialize CSS,
I use two  `SASS` the variables, and the classes.

#### Variables

> The spectrum array is start from zero

* [gitlab.com/.../sass/css/open-color/_open-color-variables.scss][tutor-04-sass-oc-vars]

{{< highlight scss >}}
$oc-general-list: (
  "white": #ffffff,
  "black": #000000
);

$oc-gray-list: (
  "0": #f8f9fa,
  ...
  "9": #212529
);

$oc-red-list: (
  "0": #fff5f5,
  ...
  "9": #c92a2a
);

...

$oc-color-list: (
  "gray":   $oc-gray-list,
  "red":    $oc-red-list,
  "pink":   $oc-pink-list,
  "grape":  $oc-grape-list,
  "violet": $oc-violet-list,
  "indigo": $oc-indigo-list,
  "blue":   $oc-blue-list,
  "cyan":   $oc-cyan-list,
  "teal":   $oc-teal-list,
  "green":  $oc-green-list,
  "lime":   $oc-lime-list,
  "yellow": $oc-yellow-list,
  "orange": $oc-orange-list
);
{{< / highlight >}}

![Open Color: Variables][image-ss-41-oc-vars]

#### Classes

* [gitlab.com/.../sass/css/open-color/_open-color-classes.scss][tutor-04-sass-oc-class]

{{< highlight scss >}}
// Color Classes

@each $name, $color in $oc-color-list {
  @each $spectrum, $value in $color {
    .oc-#{$name}-#{$spectrum} {
      background-color: $value !important;
    }
    .oc-#{$name}-#{$spectrum}-text {
      color: $value !important;
    }
  }
}
{{< / highlight >}}

If you do not understand how it works,
you might consider to read this article first:

* [Sass - Loop - Spacing Class][local-sass]

I also add loop to build `black` and `white` class.

{{< highlight scss >}}
@each $color, $value in $oc-general-list {
  .oc-#{$color} {
    background-color: $value !important;
  }
  .oc-#{$color}-text {
    color: $value !important;
  }
}
{{< / highlight >}}

![Open Color: Classes][image-ss-41-oc-class]

#### Build Result

The build result is as simple code below:

* [gitlab.com/.../assets/css/helper.css][tutor-04-css-helper]

{{< highlight css >}}
.oc-gray-0 {
  background-color: #f8f9fa !important;
}

.oc-gray-0-text {
  color: #f8f9fa !important;
}

...

.oc-gray-9 {
  background-color: #212529 !important;
}

.oc-gray-9-text {
  color: #212529 !important;
}

.oc-red-0 {
  background-color: #fff5f5 !important;
}

.oc-red-0-text {
  color: #fff5f5 !important;
}

...

.oc-red-9 {
  background-color: #c92a2a !important;
}

.oc-red-9-text {
  color: #c92a2a !important;
}
{{< / highlight >}}

![Open Color: Build Result][image-ss-41-oc-helper]

-- -- --

### 2: Other Helper

Open Color is not the only thing I need for this templates to works.
I also use materialize shadow as almost verbatim copy,
that contain `z-depth` and `hoverable` classes.

#### Directory Structure

It is good time to organize SASS folder.

{{< highlight bash >}}
❯ tree sass/css
sass/css
├── bootstrap.scss
├── feather
│   └── _icons-feather.scss
├── helper.scss
├── main
│   ├── _decoration.scss
│   ├── _layout-content.scss
│   ├── _layout-page.scss
│   ├── _logo.scss
│   └── _sticky-footer.scss
├── main.scss
├── materialize
│   └── _shadow.scss
├── open-color
│   ├── _open-color-classes.scss
│   └── _open-color-variables.scss
└── _variables.scss

4 directories, 13 files
{{< / highlight >}}

* __.__

![Open Color: Restructure SASS Directory][image-ss-41-tree-sass]

#### Stylesheet: Helper

I move the feather icons to helper.

* [gitlab.com/.../sass/css/helper.scss][tutor-04-sass-helper]

{{< highlight scss >}}
@charset "UTF-8";

// Materialize CSS
@import "materialize/shadow";

// Feather Icons
@import "feather/icons-feather";

// Heeyeun's Open Color
@import "open-color/_open-color-variables";
@import "open-color/_open-color-classes";
{{< / highlight >}}

#### Stylesheet: Main

And begin to give access to bootstrap variables.

* [gitlab.com/.../sass/css/main.scss][tutor-04-sass-main]

{{< highlight scss >}}
@import 
  // variables
  "../bootstrap/functions",
  "variables",
  "../bootstrap/variables",
  "../bootstrap/mixins/breakpoints",

  // custom
  "main/layout-page",
  "main/layout-content",
  "main/logo",
  "main/decoration",
  "main/sticky-footer"
;
{{< / highlight >}}

Now we can access bootstrap variable.
We will use this variable in a few places for example:

{{< highlight scss >}}
@include media-breakpoint-up(md) {
  ...
}

@include media-breakpoint-up(xl) {
  ...
}
{{< / highlight >}}

#### Stylesheet: Bootstrap Variables

And begin to give access to bootstrap variables.

* [gitlab.com/.../sass/css/_variables.scss][tutor-04-sass-bs-vars]

{{< highlight scss >}}
$brand-red:  #802020;
$brand-gray: #404040;

$primary: $brand-red;
{{< / highlight >}}

-- -- --

### 3: Preview

Consider the result with this document

#### HTML Document

* [gitlab.com/.../41-opencolor.html][tutor-04-41-html].

{{< highlight html >}}
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>I have a job for you</title>

  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="assets/css/main.css">
  <link rel="stylesheet" type="text/css" href="assets/css/helper.css">

  <script src="assets/js/bootstrap-navbar-native.js"></script>
  <script src="assets/js/feather.min.js"></script>
</head>

<body>

  <!-- header -->
  <nav class="navbar navbar-expand-md navbar-light fixed-top maxwidth
              oc-white z-depth-3 hoverable">
    ...
  </nav>

  <!-- responsive main -->
  <div class="row layout-base maxwidth">

    <main class="col-md-8 p-4 oc-white z-depth-3 hoverable">
      <article>
        <ul class="list-group">
          <li class="list-group-item oc-blue-9 text-light">
          <h4>Your Mission</h4></li>

          <li class="list-group-item oc-blue-7">
          To have, to hold, to love,
          cherish, honor, and protect?</li>

          <li class="list-group-item oc-blue-5">
          To shield from terrors known and unknown?
          To lie, to deceive?</li>

          <li class="list-group-item oc-blue-3">
          To live a double life,
          to fail to prevent her abduction,
          erase her identity, 
          force her into hiding,
          take away all she has known.</li>

          <li class="list-group-item oc-blue-1">
          <blockquote class="bq bq-blue-dark">
            As always, should you be caught or killed,
            any knowledge of your actions will be disavowed.
          </blockquote>
          </li>

        </ul>
      </article>
    </main>

    <aside class="col-md-4 p-4 oc-white z-depth-3 hoverable">
      Side Menu
    </aside>

  </div>

  <!-- footer -->
  <footer class="footer">
    <div class="maxwidth text-dark text-center
                oc-white z-depth-3 hoverable">
      &copy; 2020.
    </div>
  </footer>

  <script>
    feather.replace()
  </script>
</body>
</html>
{{< / highlight >}}

#### Content Skeleton

The classes works this way.
Notice the `oc-` gradient, the `z-depth`. 

{{< highlight html >}}
  <!-- responsive main -->
  <div class="row layout-base maxwidth">

    <main class="col-md-8 p-4 oc-white z-depth-3 hoverable">
      <article>
        <ul class="list-group">
          <li class="list-group-item oc-blue-9 text-light">...</li>

          <li class="list-group-item oc-blue-7">...</li>

          <li class="list-group-item oc-blue-5">...</li>

          <li class="list-group-item oc-blue-3">...</li>

          <li class="list-group-item oc-blue-1">
          <blockquote class="bq bq-blue-dark">...</blockquote>
          </li>

        </ul>
      </article>
    </main>

    <aside class="col-md-4 p-4 oc-white z-depth-3 hoverable">
      Side Menu
    </aside>

  </div>
{{< / highlight >}}

### Stylesheet Layout

Still using previous responsive.

* [gitlab.com/.../sass/css/main/_layout-content.scss][tutor-03-sass-layout-content].

{{< highlight scss >}}
// responsive - mobile
.layout-base main {
  margin-bottom: 20px;
}

@include media-breakpoint-up(md) {
  .layout-base main {
    margin-bottom: 0px;
  }
}
{{< / highlight >}}

#### Browser Preview

For desktop screen the result is similar to below figure:

![Bootstrap OC: Preview Content][image-ss-41-oc-desktop]

The looks of the page increase dramatically with the right colors.

-- -- --

### 4: Preview with Default Gutter

With default Gutter, you can have gap between column.
This gutter feature is easily forgotten,
so I have to remind again again.

#### HTML Document

{{< highlight html >}}
  <!-- responsive main -->
  <div class="row layout-base maxwidth">

    <main class="col-md-8">
      <article class="p-4 oc-white z-depth-3 hoverable"
               style="height:100%;">
        ...
      </article>
    </main>

    <aside class="col-md-4">
      <section class="p-4 oc-white z-depth-3 hoverable"
               style="height:100%;">
        Side Menu
      </section>
    </aside>

  </div>
{{< / highlight >}}

* [gitlab.com/.../42-gutter.html][tutor-04-42-html].

#### Browser Preview

For desktop screen the result is similar to below figure:

![Bootstrap OC: Preview Content][image-ss-42-oc-desktop]

In the next step we are not going to use default gutter,
but rather a custom made padding.
This custom padding is required,
if you need different behaviour and looks,
than what bootstrap have.
Or maybe you just need for flexibility reason.

-- -- --

### What is Next ?

We are ready to continue to enhance our responsive with multicolor design.
Consider continue reading [ [Bootstrap OC - Sass - Responsive][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2020/03/12/bootstrap-oc-sass-responsive/
[local-sass]:           {{< baseurl >}}frontend/2019/10/03/less-loop-spacing-class/

[tutor-04-41-html]:     {{< tutor-html-bs-oc >}}/step-04/41-opencolor.html
[tutor-04-42-html]:     {{< tutor-html-bs-oc >}}/step-04/42-gutter.html

[tutor-03-sass-layout-content]: {{< tutor-html-bs-oc >}}/step-03/sass/css/main/_layout-content.scss

[tutor-04-sass-oc-vars]:    {{< tutor-html-bs-oc >}}/step-04/sass/css/open-color/_open-color-variables.scss
[tutor-04-sass-oc-class]:   {{< tutor-html-bs-oc >}}/step-04/sass/css/open-color/_open-color-classes.scss

[tutor-04-css-helper]:      {{< tutor-html-bs-oc >}}/step-04/assets/css/helper.css
[tutor-04-sass-main]:       {{< tutor-html-bs-oc >}}/step-04/sass/css/main.scss
[tutor-04-sass-helper]:     {{< tutor-html-bs-oc >}}/step-04/sass/css/helper.scss
[tutor-04-sass-bs-vars]:    {{< tutor-html-bs-oc >}}/step-04/sass/css/_variables.scss

[image-ss-41-oc-helper]:    {{< assets-frontend >}}/2020/03/bsoc/41-open-color-build-helper.png
[image-ss-41-oc-class]:     {{< assets-frontend >}}/2020/03/bsoc/41-open-color-classes.png
[image-ss-41-oc-vars]:      {{< assets-frontend >}}/2020/03/bsoc/41-open-color-variables.png
[image-ss-41-oc-desktop]:   {{< assets-frontend >}}/2020/03/bsoc/41-opencolor-desktop.png
[image-ss-42-oc-desktop]:   {{< assets-frontend >}}/2020/03/bsoc/42-opencolor-desktop-gutter.png
[image-ss-41-tree-sass]:    {{< assets-frontend >}}/2020/03/bsoc/41-tree-sass-directory.png
