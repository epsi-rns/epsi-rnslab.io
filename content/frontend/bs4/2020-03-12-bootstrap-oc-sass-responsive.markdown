---
type   : post
title  : "Bootstrap OC - Sass - Responsive"
date   : 2020-03-12T09:17:35+07:00
slug   : bootstrap-oc-sass-responsive
categories: [frontend]
tags      : [bootstrap, sass]
keywords  : [layout, solving gap issue, responsive spacing]
author : epsi
opengraph:
  image: assets/site/images/topics/bootstrap-sass.png

toc    : "toc-2020-03-html-bootstrap-oc-step"

excerpt:
  Custom theme using Open Color, step by step.
  Wrapper class to solve bootstrap horizontal gap issue.

---

### Preface

> Goal: Wrapper class to solve bootstrap horizontal gap issue.

As we have seen in previous responsive chapter,
bootstrap can't have gap between column directly.
Bootstrap use gutter by putting `element` class  inside `column` class.
We can also make our own custom `wrapper` class inside `column`.
We require to leave bootstrap behind for our new component class.

-- -- --

### 1: Responsive Gap

#### How does it Works?

You can use `div` element inside `div` element,
or for `html5` you can utilize `section` inside `main`.

#### HTML Skeleton

For bootstrap reason,
we need to reset the horizontal padding `px` for column.

{{< highlight html >}}
  <!-- responsive main -->
  <div class="row layout-base maxwidth">

    <main class="col-md-8 px-0">
      <section class="main-wrapper p-4">
        <article>
          Page Content
        </article>
      </section>
    </main>

    <aside class="col-md-4 px-0">
      <section class="aside-wrapper p-4">
        Side Menu
      </section>
    </aside>

  </div>
{{< / highlight >}}

#### Stylesheet

It is basically just, adding `margin-left` and `margin-right`.

{{< highlight scss >}}
// responsive - mobile
.main-wrapper {
  margin-right: 0px;
}
.aside-wrapper {
  margin-left: 0px;
}

@include media-breakpoint-up(md) {
  .main-wrapper {
    margin-right: 10px;
  }
  .aside-wrapper {
    margin-left: 10px;
  }
}
{{< / highlight >}}

We can omit default margin for mobile,
since bootstrap has zero margin anyway.

![Bootstrap OC: Horizontal Gap Column][image-ss-43-gap-desktop]

For both horizontal gap and vertical gap,
the style sheet is shown as below:

{{< highlight scss >}}
// responsive - mobile
.main-wrapper {
  margin-bottom: 20px;
}

@include media-breakpoint-up(md) {
  .main-wrapper {
    margin-bottom: 0px;
  }
  .main-wrapper {
    margin-right: 10px;
  }
  .aside-wrapper {
    margin-left: 10px;
  }
}
{{< / highlight >}}

![Bootstrap OC: Vertical Gap Column][image-ss-43-gap-mobile]

Notice that now we are accessing bootstrap breakpoint variable.

{{< highlight scss >}}
$grid-breakpoints: (
  xs: 0,
  sm: 576px,
  md: 768px,
  lg: 992px,
  xl: 1200px
) !default;
{{< / highlight >}}

-- -- --

### 2: Dual Column

I would like to give an aesthetic accent.

#### Row Edge: Desktop

For desktop screen,
the double columns should not touch the edge of the screen.

* [gitlab.com/.../sass/css/main/_layout-content.scss][tutor-04-sass-layout-content].

{{< highlight scss >}}
@include media-breakpoint-up(md) {
  .main-wrapper {
    margin-right: 10px;
    margin-left: 10px;
  }
  .aside-wrapper {
    margin-left: 10px;
    margin-right: 10px;
  }
}
{{< / highlight >}}

![Bootstrap OC: Row Edge Margin on Desktop Screen][image-ss-43-margin-desktop]

#### Row Edge: Wide

But for wide screen, this should be the same size,
as both navbar header and footer.

{{< highlight scss >}}
@include media-breakpoint-up(xl) {
  .main-wrapper {
    margin-left: 0px;
  }
  .aside-wrapper {
    margin-right: 0px;
  }
}
{{< / highlight >}}

![Bootstrap OC: Row Edge Margin on Wide Screen][image-ss-43-margin-wide]

#### HTML Content

Now consider give it a content, and see how it looks.

* [gitlab.com/.../43-responsive.html][tutor-04-43-html].

{{< highlight html >}}
  <!-- responsive main -->
  <div class="row layout-base maxwidth">

    <main class="col-md-8 px-0">
      <section class="main-wrapper p-4 oc-white z-depth-3 hoverable">
        <article>
          <ul class="list-group">
            <li class="list-group-item oc-blue-9 text-light">
            <h4>Your Mission</h4></li>

            <li class="list-group-item oc-blue-7">
            To have, to hold, to love,
            cherish, honor, and protect?</li>

            <li class="list-group-item oc-blue-5">
            To shield from terrors known and unknown?
            To lie, to deceive?</li>

            <li class="list-group-item oc-blue-3">
            To live a double life,
            to fail to prevent her abduction,
            erase her identity, 
            force her into hiding,
            take away all she has known.</li>

            <li class="list-group-item oc-blue-1">
            <blockquote class="bq bq-blue-dark">
              As always, should you be caught or killed,
              any knowledge of your actions will be disavowed.
            </blockquote>
            </li>

          </ul>
        </article>
      </section>
    </main>

    <aside class="col-md-4 px-0">
      <section class="aside-wrapper p-4 oc-white z-depth-3 hoverable">
        Side Menu
      </section>
    </aside>

  </div>
{{< / highlight >}}

![Bootstrap OC: Responsive Gap: Double Column Test][image-ss-43-double-column]

-- -- --

### 3: Single Column

> Test with Landing Page

We are not finished yet.
We have to test for single column,
such as `main` content without `aside` sidebar.
Feature is likely to fail,
if we do not have enough test in the first place.

#### Stylesheet

For single column, we need to add this one `single` class,
to fix `margin-right` for wide screen.

* [gitlab.com/.../sass/css/main/_layout-content.scss][tutor-04-sass-layout-content].

{{< highlight scss >}}
@include media-breakpoint-up(xl) {
  .main-wrapper {
    margin-left: 0px;
  }
  .aside-wrapper {
    margin-right: 0px;
  }
  .main-wrapper.single {
    margin-right: 0px;
  }
}
{{< / highlight >}}

#### HTML Content

My favorite single column is, `landing page`.

* [gitlab.com/.../44-landing-page.html][tutor-04-44-html].

{{< highlight html >}}
  <!-- responsive main -->
  <div class="row layout-base maxwidth">

    <main class="col px-0">
      <section class="main-wrapper single text-center p-4
                  oc-white z-depth-3 hoverable">
        <article>
          <p>
            <a href="#" 
               class="btn btn-primary my-2"
              >Articles Sorted by Month</a>
            <a href="#" 
               class="btn btn-secondary my-2"
              >Articles Sorted by Tag</a>
          </p>
          <p class="text-muted">
              As always,
              should you be caught or killed,
              any knowledge of your actions will be disavowed.</p>
        </article>
      </section>
    </main>

  </div>
{{< / highlight >}}

![Bootstrap OC: Responsive Gap: Single Column Test][image-ss-44-single-column]

You should try the gap yourself between screen: mobile, desktop, and wide.

#### Summary

As a conclusion,
here below is the complete responsive gap class for bootstrap.

* [gitlab.com/.../sass/css/main/_layout-content.scss][tutor-04-sass-layout-content].

{{< highlight scss >}}
// responsive - mobile
.main-wrapper {
  margin-bottom: 20px;
}

@include media-breakpoint-up(md) {
  .main-wrapper {
    margin-bottom: 0px;
  }
  .main-wrapper {
    margin-right: 10px;
    margin-left: 10px;
  }
  .aside-wrapper {
    margin-left: 10px;
    margin-right: 10px;
  }
}

@include media-breakpoint-up(xl) {
  .main-wrapper {
    margin-left: 0px;
  }
  .aside-wrapper {
    margin-right: 0px;
  }
  .main-wrapper.single {
    margin-right: 0px;
  }
}
{{< / highlight >}}

The result of compiled `CSS` can be seen in left pane in figure below,
while in the right pane is the `SASS` source.

![Bootstrap SASS: Compiled CSS][image-ss-40-panes-sass]

-- -- --

### What is Next ?

While we design the layout skeleton in this article,
now we need to fill of each layout such as blog post and side widget.

Consider continue reading [ [Bootstrap OC - Sass - Widget][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2020/03/13/bootstrap-oc-sass-widget/
[local-sass]:           {{< baseurl >}}frontend/2019/10/03/less-loop-spacing-class/

[tutor-04-43-html]:     {{< tutor-html-bs-oc >}}/step-04/43-responsive.html
[tutor-04-44-html]:     {{< tutor-html-bs-oc >}}/step-04/44-landing-page.html

[tutor-04-sass-layout-content]: {{< tutor-html-bs-oc >}}/step-04/sass/css/main/_layout-content.scss

[image-ss-43-gap-desktop]:      {{< assets-frontend >}}/2020/03/bsoc/43-responsive-gap-desktop.png
[image-ss-43-gap-mobile]:       {{< assets-frontend >}}/2020/03/bsoc/43-responsive-gap-mobile.png
[image-ss-43-margin-desktop]:   {{< assets-frontend >}}/2020/03/bsoc/43-responsive-gap-desktop-margin.png
[image-ss-43-margin-wide]:      {{< assets-frontend >}}/2020/03/bsoc/43-responsive-gap-wide-margin.png
[image-ss-43-double-column]:    {{< assets-frontend >}}/2020/03/bsoc/43-responsive-gap-double-column.png
[image-ss-44-single-column]:    {{< assets-frontend >}}/2020/03/bsoc/44-responsive-gap-single-column.png
[image-ss-40-panes-sass]:       {{< assets-frontend >}}/2020/03/bsoc/40-panes-sass-responsive.png
