---
type   : post
title  : "Bootstrap OC - Sass - Widget"
date   : 2020-03-13T09:17:35+07:00
slug   : bootstrap-oc-sass-widget
categories: [frontend]
tags      : [bootstrap, sass]
keywords  : [panel, widget, list ,tags]
author : epsi
opengraph:
  image: assets/site/images/topics/bootstrap-sass.png

toc    : "toc-2020-03-html-bootstrap-oc-step"

excerpt:
  Custom theme using Open Color, step by step.
  Building multicolor widget with flat design.

---

### Preface

> Goal: Building multicolor widget with flat design.

Since I'm not a web designer, I rarely make my own theme.
I simply don't have the sense of art to make the mockup with inkscape.
So here is what I do, I make a pure css theme.
One of them is this flat design, featuring multicolored widget.

#### Responsive Requirement

This article is not a bootstrap tutorial anymore,
but more like a custom theme tutorial.
Even though the page based on bootstrap.

> Custom Class

Yes. I have to do it myself. And it is fun.

#### Multicolor Requirement

> Palette

We can either use Google Materialize Color, or Open Color.

-- -- --

### 1: Responsive Gap

Consider go further from our previous chapter.
From plain looks, to multicolor.

#### Stylesheet

The very basic part of this multicolor is,
adding padding to our previous layout.

* [gitlab.com/.../sass/css/main/_layout-content.scss][tutor-05-sass-layout-content].

{{< highlight scss >}}
// layout

.main-wrapper,
.aside-wrapper {
  padding: 0 10px 0 10px;
}
{{< / highlight >}}

#### HTML Skeleton

The basic layout is the same as previous,
except with additional colors.

* [gitlab.com/.../51-responsive][tutor-05-51-html].

{{< highlight html >}}
  <!-- responsive colored main layout -->
  <div class="row layout-base maxwidth">

    <main class="col-md-8 px-0">
      <section class="main-wrapper oc-blue-5">
        <div class="oc-white z-depth-3 hoverable">
          Page Content
        </div>
      </section>
    </main>

    <aside class="col-md-4 px-0">   
      <section class="aside-wrapper oc-green-5">
        <div class="oc-white z-depth-3 hoverable">
          Side Menu
        </div>
      </section>
    </aside>

  </div>
{{< / highlight >}}

In mobile screen it is simply as below:

![Bootstrap OC: Responsive Multicolor in Mobile Screen][image-ss-51-responsive]

#### HTML Content

Consider give a horizontal padding to a content,
and see how it looks in desktop screen.

* [gitlab.com/.../52-blog][tutor-05-52-html].

{{< highlight html >}}
  <!-- responsive colored main layout -->
  <div class="row layout-base maxwidth">

    <main class="col-md-8 px-0">
      <section class="main-wrapper oc-blue-5">
        <div class="blog px-2 oc-white z-depth-3 hoverable">
          <article>
            <h4>Cause You're Different!</h4>
            <p>When you can do the things that I can, but you don't, 
            and then the bad things happen,
            they happen because of you.</p>
          </article>
        </div>
      </section>
    </main>

    <aside class="col-md-4 px-0">   
      <section class="aside-wrapper oc-green-5">
        <div class="widget px-2 oc-white z-depth-3 hoverable">     
          <p>How many times has he been,
          betrayed, disavowed, cast aside?</p>
        </div>
      </section>

      <section class="aside-wrapper oc-teal-5">
        <div class="widget px-2 oc-white z-depth-3 hoverable">
          <p>And how long before a man like that has had enough?</p>
        </div>
      </section>
    </aside>

  </div>
{{< / highlight >}}

![Bootstrap OC: Responsive Multicolor in Desktop Screen][image-ss-52-blog]

-- -- --

### 2: Header and Footer

Consider separate design in each multicolor section,
so that each have their own `header` and `footer`.

#### Stylesheet

The very basic part of this multicolor is,
adding padding to our previous layout.

* [gitlab.com/.../sass/css/main/_layout-content.scss][tutor-05-sass-layout-content].

{{< highlight scss >}}
// gap main and aside

.blog {
  padding: 0px;
}
.blog-header {
  padding: 5px 10px 5px 10px;
}
.blog-body {
  padding: 5px 10px 5px 10px;
}
.widget {
  padding: 0px;
}
.widget-header {
  padding: 5px 10px 5px 10px;
}
.widget-body {
  padding: 5px 10px 5px 10px;
}
{{< / highlight >}}

![Bootstrap OC: Custom SASS for Widget][image-ss-50-vim-widget]

And the vertical gap between widget can be solved with `:last-child`.

{{< highlight scss >}}
// responsive

.main-wrapper,
.aside-wrapper {
  margin-bottom: 20px;
}

.aside-wrapper:last-child {
  margin-bottom: 0px;
}
{{< / highlight >}}

#### HTML Skeleton

We have many additional `element` layer here.
Each `blog` and the `widget`, has `header` and `body`.

* Blog: `main.col > section.main-wrapper > div.blog > article.blog-body`

{{< highlight html >}}
    <main class="col-md-8 px-0">
      <section class="main-wrapper oc-blue-5">
        <div class="blog oc-white z-depth-3 hoverable">

          <section class="blog-header oc-blue-1">
            ...
          </section>

          <article class="blog-body">
            ...
          </article>

        </div>
      </section>
    </main>
{{< / highlight >}}

* Widget: `aside.col > section.aside-wrapper > div.widget > div.widget-body`

{{< highlight html >}}
    <aside class="col-md-4 px-0">   
      <section class="aside-wrapper oc-green-5">
        <div class="widget oc-white z-depth-3 hoverable">     

          <div class="widget-header oc-green-1">
            ...
          </div>

          <div class="widget-body">
            ...
          </div>

        </div>
      </section>
    </aside>
{{< / highlight >}}

#### HTML Content

Consider give multiple widget content,
and see how it looks in desktop screen.

* [gitlab.com/.../53-widget][tutor-05-53-html].

{{< highlight html >}}
  <!-- responsive colored main layout -->
  <div class="row layout-base maxwidth">

    <main class="col-md-8 px-0">
      <section class="main-wrapper oc-blue-5">
        <div class="blog oc-white z-depth-3 hoverable">
          <section class="blog-header oc-blue-1">
            <h4 itemprop="name headline">
              <a href="#">Cause You're Different!</a></h4>
          </section>

          <article class="blog-body" itemprop="articleBody">
            <p>When you can do the things that I can, but you don't, 
            and then the bad things happen,
            they happen because of you.</p>
          </article>
        </div>
      </section>
    </main>

    <aside class="col-md-4 px-0">   
      <section class="aside-wrapper oc-green-5">
        <div class="widget oc-white z-depth-3 hoverable">     

          <div class="widget-header oc-green-1">
            <strong>How many times ?</strong>
            <i data-feather="zap" class="float-right"></i>
          </div>

          <div class="widget-body">
            <p>Has he been, betrayed, disavowed, cast aside.</p>
          </div>

        </div>
      </section>

      <section class="aside-wrapper oc-teal-5">
        <div class="widget oc-white z-depth-3 hoverable">

          <div class="widget-header oc-teal-1">
            <strong>And how long?</strong>
            <i data-feather="feather" class="float-right"></i>
          </div>

          <div class="widget-body">
            <p>Before a man like that has had enough.</p>
          </div>

        </div>
      </section>
    </aside>

  </div>
{{< / highlight >}}

![Bootstrap OC: Multicolor Widget in Desktop Screen][image-ss-53-widget]

-- -- --

### 3: List

I have four blogs, and each blog contain nine kind of widgets.
These two are the basic most widget.

* List

* Tags

Consider begin with List.

#### Stylesheet

I must admit I have difficulties with `SVG`.
Using font such as `FontAwesome` is easier with `CSS`.
My workaround is grab the `chevron-right.svg` from feathericons.
Then well.... just resize the icon to suit widget looks.
Resize and changing color is very easy with inkscape.
I just need one for list icon anyway.

* [gitlab.com/.../sass/css/main/_list.scss][tutor-05-sass-list].

{{< highlight scss >}}
// -- -- -- -- --
// _list.scss

ul.widget-list li:hover {
  background: map-get($oc-yellow-list, '3');
}

ul.widget-list {
  padding: 0px;
}

ul.widget-list {
  padding-left: 0;
  font-size: 13px;
  margin-left: 20px;
  list-style-image: url(../icons/chevrons-right-resized.svg);
}

ul.widget-list li:hover:before {
  color: map-get($oc-yellow-list, '5');
}
{{< / highlight >}}

`map-get` is `sass` language.

* [sass-lang.com/documentation/values/maps](https://sass-lang.com/documentation/values/maps)__.__

Do not forget to alter the `main.scss`.

* [gitlab.com/.../sass/css/main.scss][tutor-05-sass-main].

{{< highlight scss >}}
@import 
  ...

  // custom
  "main/layout-page",
  "main/layout-content",
  ...
  "main/list"
;
{{< / highlight >}}

#### HTML Content

Consider this list widget content.

* [gitlab.com/.../54-list][tutor-05-54-html].

{{< highlight html >}}
    <aside class="col-md-4 px-0">   
      <section class="aside-wrapper oc-green-5">
        <div class="widget oc-white z-depth-3 hoverable">     

          <div class="widget-header oc-green-1">
            <strong>Isle of Friends</strong>
            <i data-feather="users" class="float-right"></i>
          </div>

          <div class="widget-body">
            <ul class="widget-list">
              <li><a href="http://epsi-rns.github.io/">Linux/BSD Desktop Customization</a></li>
              <li><a href="http://epsi-rns.gitlab.io/">Web Development Blog</a></li>
              <li><a href="http://oto-spies.info/">Car Painting and Body Repair.</a></li>
            </ul>
          </div>

        </div>
      </section>
    </aside>
{{< / highlight >}}

![Bootstrap OC: List Widget Example][image-ss-54-list]

-- -- --

### 4: Tags

Other basic widget example is the `tags` widget.

#### Stylesheet

Tags with feathericons require a bit of stylesheet adjustment.
I use `feather-14` to reszie the icon.

* [gitlab.com/.../sass/css/feather/_icons-feather.scss][tutor-05-sass-icons-feathers].

{{< highlight scss >}}
.feather {
  width: 20px;
  height: 20px;
  stroke: currentColor;
  stroke-width: 2;
  stroke-linecap: round;
  stroke-linejoin: round;
  fill: none;
  
  vertical-align: text-top;
}

.feather-14 {
  width: 14px;
  height: 14px;
}
{{< / highlight >}}

#### HTML Content

Consider this list widget content.

* [gitlab.com/.../55-tags][tutor-05-55-html].

{{< highlight html >}}
    <aside class="col-md-4 px-0">
      <section class="aside-wrapper oc-cyan-5">
        <div class="widget oc-white z-depth-3 hoverable">     

          <div class="widget-header oc-cyan-1">
            <strong>Categories</strong>
            <i data-feather="folder" class="float-right"></i>
          </div>

          <div class="widget-body">
            <a href="#">
              <span class="badge oc-cyan-3 z-depth-1 hoverable">
              mission&nbsp;
              <i data-feather="folder" class="feather-14"></i>
            </span></a>&nbsp;
            <a href="#">
              <span class="badge oc-cyan-3 z-depth-1 hoverable">
              accomplished&nbsp;
              <i data-feather="folder" class="feather-14"></i>
            </span></a>&nbsp;
          </div>

        </div>
      </section>

      <section class="aside-wrapper oc-teal-5">
        <div class="widget oc-white z-depth-3 hoverable">     

          <div class="widget-header oc-teal-1">
            <strong>Tags</strong>
            <i data-feather="tag" class="float-right"></i>
          </div>

          <div class="widget-body">
            <a href="#">
              <span class="badge oc-white z-depth-1 hoverable">
              command&nbsp;
              <i data-feather="tag" class="feather-14"></i>
            </span></a>&nbsp;
            <a href="#">
              <span class="badge oc-white z-depth-1 hoverable">
              agent&nbsp;
              <i data-feather="tag" class="feather-14"></i>
            </span></a>&nbsp;
            <a href="#">
              <span class="badge oc-white z-depth-1 hoverable">
              duty&nbsp;
              <i data-feather="tag" class="feather-14"></i>
            </span></a>&nbsp;
          </div>

        </div>
      </section>
    </aside>
{{< / highlight >}}

![Bootstrap OC: Tags Widget Example][image-ss-55-tags]

-- -- --

### What is Next ?

The last part is about content, the blog post and its decoration.
Consider continue reading [ [Bootstrap OC - Sass - Blog Post][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2020/03/15/bootstrap-oc-sass-blog-post/

[tutor-05-51-html]:     {{< tutor-html-bs-oc >}}/step-05/51-responsive.html
[tutor-05-52-html]:     {{< tutor-html-bs-oc >}}/step-05/52-blog.html
[tutor-05-53-html]:     {{< tutor-html-bs-oc >}}/step-05/53-widget.html
[tutor-05-54-html]:     {{< tutor-html-bs-oc >}}/step-05/54-list.html
[tutor-05-55-html]:     {{< tutor-html-bs-oc >}}/step-05/55-tags.html

[tutor-05-sass-icons-feathers]: {{< tutor-html-bs-oc >}}/step-05/sass/css/feather/_icons-feather.scss
[tutor-05-sass-layout-content]: {{< tutor-html-bs-oc >}}/step-05/sass/css/main/_layout-content.scss
[tutor-05-sass-list]:   {{< tutor-html-bs-oc >}}/step-05/sass/css/main/_list.scss
[tutor-05-sass-main]:   {{< tutor-html-bs-oc >}}/step-05/sass/css/main.scss

[image-ss-51-responsive]:       {{< assets-frontend >}}/2020/03/bsoc/51-responsive-multicolor-mobile.png
[image-ss-52-blog]:             {{< assets-frontend >}}/2020/03/bsoc/52-responsive-multicolor-desktop.png
[image-ss-53-widget]:           {{< assets-frontend >}}/2020/03/bsoc/53-multicolor-widget.png
[image-ss-54-list]:             {{< assets-frontend >}}/2020/03/bsoc/54-widget-list.png
[image-ss-55-tags]:             {{< assets-frontend >}}/2020/03/bsoc/55-widget-tags.png
[image-ss-50-vim-widget]:       {{< assets-frontend >}}/2020/03/bsoc/50-vim-main-widget.png
