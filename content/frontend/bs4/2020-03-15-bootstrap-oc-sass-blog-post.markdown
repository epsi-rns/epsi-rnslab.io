---
type   : post
title  : "Bootstrap OC - Sass - Blog Post"
date   : 2020-03-15T09:17:35+07:00
slug   : bootstrap-oc-sass-blog-post
categories: [frontend]
tags      : [bootstrap, sass]
keywords  : [post header, post navigation, landing page]
author : epsi
opengraph:
  image: assets/site/images/topics/bootstrap-sass.png

toc    : "toc-2020-03-html-bootstrap-oc-step"

excerpt:
  Custom theme using Open Color, step by step.
  Prepare common blog post theme for use later with framework.

---

### Preface

> Goal: Prepare common blog post theme for use later with framework.

This chapter is focusing on content page.

-- -- --

### 1: Prepare

This chapter also summarize what we have done so far.

#### Directory Preparation

We need a new `SASS` section for this `post`.

* [gitlab.com/.../sass/][tutor-06-sass].

{{< highlight bash >}}
❯ tree sass/css
sass/css
├── bootstrap.scss
├── feather
│   └── _icons-feather.scss
├── helper.scss
├── main
│   ├── _decoration.scss
│   ├── _layout-content.scss
│   ├── _layout-page.scss
│   ├── _list.scss
│   ├── _logo.scss
│   └── _sticky-footer.scss
├── main.scss
├── materialize
│   └── _shadow.scss
├── open-color
│   ├── _open-color-classes.scss
│   └── _open-color-variables.scss
├── post
│   ├── _content.scss
│   └── _navigation.scss
└── _variables.scss

5 directories, 16 files
{{< / highlight >}}

This only require two new file artefact.

* _content.scss

* _navigation.scss __.__

![Bootstrap OC: Restructure SASS Directory][image-ss-61-tree-sass]

In real world blogging,
this post section has a more `SASS` stylesheet document.
I have about five or seven, depend on the framework that I choose.

#### Main SASS

Of course you have to update your main SASS to reflect the above

* [gitlab.com/.../sass/css/main.scss][tutor-06-sass-main].

{{< highlight scss >}}
// Import partials from `sass_dir` (defaults to `sass/css`)
@import 
  // Bootstrap Related
  "../bootstrap/functions",
  "variables",
  "../bootstrap/variables",
  "../bootstrap/mixins/breakpoints",

  // Heeyeun's Open Color
  "open-color/_open-color-variables",

  // Tailor Made
  "main/layout-page",
  "main/layout-content",
  "main/logo",
  "main/decoration",
  "main/sticky-footer",
  "main/list",
  
  "post/content",
  "post/navigation"
;
{{< / highlight >}}

![Bootstrap OC: Tree and Main SASS][image-ss-63-nerdtree-main]

-- -- --

### 2: Header

Blog post header customization is very common.
I utilize badge (or tag).
Luckily most CSS framework has built in support to display tag.

#### Stylesheet

* [gitlab.com/.../sass/css/post/_content.scss][tutor-06-sass-content].

{{< highlight scss >}}
h1, h2, h3, h4, h5, h6 {
  font-family: "Playfair Display", Georgia, "Times New Roman", serif;
}

/*
 * Blog posts
 */

.blog-body {
  margin-bottom: 1rem;
}
.blog-header {
  h1, h2, h3, h4, h5, h6 {
    margin-bottom: .25rem;
    a {
      color: map-get($oc-gray-list, "9");
    }
    a:hover {
      color: map-get($oc-gray-list, "6") !important;
    }
  }
}
.blog-header .meta {
  margin-bottom: 2.0rem;
}
.blog-body p {
  margin-top: 0.5rem;
  margin-bottom: 0.5rem;
}
{{< / highlight >}}

* __.__

#### HTML Content

I know this example is long.
this is just a demo of tag elements,
along with feather icons.

* [gitlab.com/.../61-post-header][tutor-06-61-html].

{{< highlight html >}}
    <main class="col-md-8 px-0">
      <section class="main-wrapper oc-blue-5">
        <div class="blog oc-white z-depth-3 hoverable">
          <section class="blog-header oc-blue-1">
            <h4 class="font-weight-bold" itemprop="name headline">
              <a href="#">Cause You're Different!</a></h4>

            <div class="clearfix"></div>

            <div class="field meta">
              <div class="float-left">
              <span class="meta_author badge oc-indigo-7
                           oc-white-text z-depth-1">
                  <i data-feather="user" class="feather-14"></i>
                  &nbsp;
                  <span itemprop="author"
                        itemscope itemtype="http://schema.org/Person">
                  <span itemprop="name">epsi</span></span>
              </span>
              &nbsp;
              </div>

              <div class="float-left">
                <span class="meta-time badge oc-cyan-7
                             oc-white-text z-depth-1">
                  <i data-feather="clock" class="feather-14"></i>
                  &nbsp;A few days ago.
                </span>
                &nbsp;
              </div>

              <div class="float-right">
                  <a href="#">
                    <span class="badge oc-teal-7
                                 oc-white-text z-depth-1 hoverable"
                      ><i data-feather="tag" class="feather-14"></i>
                      &nbsp;love</span></a>
                &nbsp;
              </div>

              <div class="float-right">
                  <a href="#">
                    <span class="badge oc-blue-7
                                 oc-white-text z-depth-1 hoverable"
                      ><i data-feather="folder" class="feather-14"></i>
                      &nbsp;rock</span></a>
                &nbsp;
              </div>
            </div>

            <div class="clearfix"></div>
          </section>

          <article class="blog-body" itemprop="articleBody">
            <p>When you can do the things that I can, but you don't, 
            and then the bad things happen,
            they happen because of you.</p>
          </article>
        </div>
      </section>
    </main>
{{< / highlight >}}

![Bootstrap OC: Blog Post Header][image-ss-61-post-header]

-- -- --

### 3: Navigation

Blog post navigation along with pagination customization,
is also very common.
And most CSS framework has built in support to navigation.

#### Stylesheet

Bootstrap navigation in bootstrap is harcoded to `pagination`.
You cannot rename class to `navigation`.

* [gitlab.com/.../sass/css/post/_navigation.scss][tutor-06-sass-nav].

{{< highlight scss >}}
li.post-previous a:after {
  content: " previous";
}

li.post-next a:before {
    content: "next ";
}

.blog-body .pagination {
  margin-top: 1rem;
  margin-bottom: 1rem;
}
{{< / highlight >}}

#### HTML Content

Just add navigation code after the `article` element.

* [gitlab.com/.../62-post-navigation[tutor-06-62-html].

{{< highlight html >}}
          <article class="blog-body" itemprop="articleBody">
            <p>When you can do the things that I can, but you don't, 
            and then the bad things happen,
            they happen because of you.</p>

            <ul class="pagination justify-content-between" 
                role="navigation" aria-labelledby="pagination-label">

              <li class="page-item post-previous">
                <a class="page-link" 
                   href="#" 
                   title="next article name">&laquo; </a>
              </li>

              <li class="page-item post-next">
                <a class="page-link" 
                   href="#" 
                   title="previous article name"> &raquo;</a>
              </li>

            </ul>
          </article>
{{< / highlight >}}

![Bootstrap OC: Blog Post Navigation][image-ss-62-post-nav]

-- -- --

### 4: Landing Page

Again, test our single page design, with a more complete landing page.

* [gitlab.com/.../63-landing-page][tutor-06-63-html].

{{< highlight html >}}
  <!-- responsive multicolor main layout -->
  <div class="row layout-base maxwidth">

    <main class="col px-0">
      <section class="main-wrapper single oc-blue-5">
        <div class="blog oc-white z-depth-3 hoverable">
          <section class="blog-header oc-blue-1 text-center">
            <h3 class="font-weight-bold" itemprop="name headline">
              <a href="#">Your Mission</a></h3>
            <h4 class="font-weight-normal">
              <a href="#">Should you decide to accept</a>
          </section>

          <article class="blog-body text-center" itemprop="articleBody">
            <p>
              <a href="#" 
                 class="btn btn-primary my-2"
                >Articles Sorted by Month</a>
              <a href="#" 
                 class="btn btn-secondary my-2"
                >Articles Sorted by Tag</a>
            </p>

            <p>
              As always,
              should you be caught or killed,
              any knowledge of your actions will be disavowed.</p>

            <img src="assets/images/one-page.png" 
                 alt="business card">

            <p class="text-muted">
              <small>
              <i data-feather="home" class="feather-14"></i>&nbsp;
                Whitewood Street, Monday Market,
                East Jakarta, 55112, Indonesia.
              </small>
            </p>
          </article>
        </div>
      </section>
    </main>

  </div>
{{< / highlight >}}

![Bootstrap OC: Landing Page][image-ss-63-landing-page]

Landing page is not really blog post post.
I just put here just because I don't want to make new article.

I just keep this article simple.

-- -- --

### 5: Summary

As a summary, here is the final looks,
of our blog post in desktop screen.

![Bootstrap OC: Blog Post Desktop][image-ss-62-post-desktop]

-- -- --

### What is Next ?

The last part is a bonus.
It is more like a javacript tutorial,
which use a bunch of Bootstrap class.

Consider continue reading [ [Bootstrap OC - Javascript Toggler][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2020/03/16/bootstrap-oc-javascript-toggler/

[tutor-06-sass]:        {{< tutor-html-bs-oc >}}/step-06/sass/

[tutor-06-61-html]:     {{< tutor-html-bs-oc >}}/step-06/61-post-header.html
[tutor-06-62-html]:     {{< tutor-html-bs-oc >}}/step-06/62-post-navigation.html
[tutor-06-63-html]:     {{< tutor-html-bs-oc >}}/step-06/63-landing-page.html

[tutor-06-sass-main]:   {{< tutor-html-bs-oc >}}/step-06/sass/css/main.scss
[tutor-06-sass-content]:{{< tutor-html-bs-oc >}}/step-06/sass/css/post/_content.scss
[tutor-06-sass-nav]:    {{< tutor-html-bs-oc >}}/step-06/sass/css/post/_navigation.scss

[image-ss-61-tree-sass]:    {{< assets-frontend >}}/2020/03/bsoc/61-tree-sass-directory.png
[image-ss-61-post-header]:  {{< assets-frontend >}}/2020/03/bsoc/61-post-header.png
[image-ss-62-post-nav]:     {{< assets-frontend >}}/2020/03/bsoc/62-post-navigation.png
[image-ss-62-post-desktop]: {{< assets-frontend >}}/2020/03/bsoc/62-post-desktop.png
[image-ss-63-landing-page]: {{< assets-frontend >}}/2020/03/bsoc/63-landing-page.png
[image-ss-63-nerdtree-main]:{{< assets-frontend >}}/2020/03/bsoc/60-nerdtree-main-sass.png
