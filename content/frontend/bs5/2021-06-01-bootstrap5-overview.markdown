---
type   : post
title  : "Bootstrap - Overview"
date   : 2021-06-01T09:17:35+07:00
slug   : bootstrap5-overview
categories: [frontend]
tags      : [bootstrap, css]
keywords  : [plain html, custom theme, summary, overview]
author : epsi
opengraph:
  image: assets/posts/frontend/2021/06/001-preview-06.png

toc    : "toc-2021-06-html-bootstrap5-step"

excerpt:
  Designing real world web page using Bootstrap5, step by step.
  Summary of article series.

---

### Preface

This article series is intended for beginner.

> Goal: Make your own theme using Bootstrap5

With this guidance, you can make your own theme,
for use with blogging, or simple company profile.
You can also enhanced later to create,
your own portfolio based on this knowledge.

To face real life, web building situation,
there will be other guidance, based on this Bootstrap example.

#### Reading

Always refer to official documentation first,
before you start this real world example.

* [getbootstrap.com](https://getbootstrap.com/)

#### Version

> Which Bootstrap?

I'm using Boostrap v5.
By the time this article written,

The obsolete Bootstrap v4 article series is also available.

* [Bootstrap OC - Overview][local-bootstrap-v4]

#### HTML Tools

> My intention is simplicity

I'm using pure HTML instead of static site generator,
so that any beginner can follow this Bootstrap show case.

You can just open the file in the browser, and this will works.

#### Nunjucks

> Modularized

Since there is a repetitive reusable part,
such as navbar, footer, and so on.
For development, I use templating engine.

Explaining HTML as building block ,
is also easier with templating engine.

My choice is Nunjucks.
And the automation can be done with Gulpjs.

#### CSS Tools

I'm using Sass to generate CSS,
with `.scss` format instead of `.sass`.
Because Bootstrap use `.scss` format.

Of course you can use any `.sass` format if you want,
for your web design.

#### Learning Stylesheet

> The Scope of CSS Tools

As stated in previous Bootstrap V4 article series.
Learning CSS Frameworks is a part of this bigger picture below:

![The Scope of CSS Tools][illustration-css-tools]

-- -- --

### About Bootstrap

Bootstrap is an opensource CSS Framework.
There are other alternative as well,
such as <code>Materialize</code> or <code>Bulma</code>.

#### Why Bootstrap?

Although Bootstrap is not
the most popular CSS framework anymore,
Bootstrap is still en entry point for beginner.

Frankly speaking.
I like Bulma, and I desire to learn Tailwind.

> This theme is a direct port from my Bulma MD theme.

#### Choices

Beside palin framework,
I also include additional options.

* Icons: FontAwesome, Bootstrap Icon

* Pallete: Google Material Color, Open Color, BS5

You can see example of FeatherIcons in my previous Bootstrap v4 tutorial.

#### Part of Bootstrap

Just read again the offical documentation.
There is more complete than here.

-- -- --

### Example Showcase

#### Screenshot

I add some aestethic appeal, with more custom stylesheet.

![Bootstrap: Preview][002-post-navigation]

#### Source Code

Source code used in this tutorial, is available at:

*	[gitlab.com/epsi-rns/tutor-html-bootstrap5][tutor-bootstrap-master]

This is also intended as a Demo.

#### Tools

> Opening Example in Browser

You can open the `html` file artefact directly from your `file manager`,
or alternatively setup simple web server example such as `browsersync`,
and enjoy the `localhost:3000`.

#### Disclaimer

This is would not be the best blog template that you ever have.
Because I only put most common stuff,
to keep the tutorial simple.

After you learn this guidance,
you understand the fundamental skill.
Thus, a base for you, to make your own blog site.
With your imagination, you may continue,
to build your own super duper Bootstrap site.
Far better than, what I have achieved.

#### Table of Content

The table content is available as header on each tutorial.
There, I present a Bootstrap Tutorial, step by step, for beginners.

-- -- --

### Begin The Guidance

#### Step 00

> Preparation

* Without Bootstrap

* Nunjucks Example

![screenshot][screenshot-00]

#### Step 01

> Adding Bootstrap CSS

* Using CDN

* Using Local

![screenshot][screenshot-01]

-- -- --

#### Step 02

> Using Bootstrap CSS: Navigation Bar

* From Simple to Full Featured

![screenshot][screenshot-02]

-- -- --

#### Step 03

> Icon Choices

* Icons: FontAwesome, Boostrap Icon.

* Navbar and Sidebar

![screenshot][screenshot-03]

-- -- --

#### Step 04

> Custom SASS

* Custom maximum width class

* Simple responsive layout using Bootstrap

![screenshot][screenshot-04]

-- -- --

#### Step 05

> Color Choices

* Color using tailor made sass

* Open Color, Bootstrap5 Color, Google Material Color

* Double column demo, responsive gap using custom sass

* Single column demo, simple landing page

* Dark Mode

![screenshot][screenshot-05]

-- -- --

#### Step 06

> HTML Box using Google Material Color

* Main Blog and Aside Widget

![screenshot][screenshot-06]

-- -- --

#### Step 07

> Blog Post Example

* Post Header

* Post Navigation

* Landing Page

* Javascript Toggler

![screenshot][screenshot-07]

-- -- --

#### Step 08

> Custom Javascript

* Hover and and Animation

* Numerator

* Progress Bar

![screenshot][screenshot-08]

-- -- --

#### Step 09

> Form

* Login Form

* User Form

![screenshot][screenshot-09]

This form section deserve more coverage, more examples.

-- -- --

#### Step 10

> Extra

Gather all together.

* Datatables

* ChartJS

* Dashboard

![screenshot][screenshot-10]

-- -- --

### What is Next 🤔?

Thank you for cloning the [Bootstrap5][repository] repository.
Have fun, and enjoy coding.

Consider continue reading [ [Bootstrap 5 - Nunjucks Template][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2021/06/03/bootstrap5-preparation/
[local-bootstrap-v4]:   {{< baseurl >}}frontend/2020/03/01/bootstrap-oc-overview/

[tutor-bootstrap-master]:   {{< tutor-html-bs5 >}}/

[illustration-css-tools]:   {{< assets-frontend >}}/2020/03/css-tools.png

[repository]:           https://gitlab.com/epsi-rns/tutor-html-bootstrap5/

[//]: <> ( -- -- -- links below -- -- -- )

[screenshot-00]:        {{< baseurl >}}assets/posts/frontend/2021/06/001-preview-00.png
[screenshot-01]:        {{< baseurl >}}assets/posts/frontend/2021/06/001-preview-01.png
[screenshot-02]:        {{< baseurl >}}assets/posts/frontend/2021/06/001-preview-02.png
[screenshot-03]:        {{< baseurl >}}assets/posts/frontend/2021/06/001-preview-03.png
[screenshot-04]:        {{< baseurl >}}assets/posts/frontend/2021/06/001-preview-04.png
[screenshot-05]:        {{< baseurl >}}assets/posts/frontend/2021/06/001-preview-05.png
[screenshot-06]:        {{< baseurl >}}assets/posts/frontend/2021/06/001-preview-06.png
[screenshot-07]:        {{< baseurl >}}assets/posts/frontend/2021/06/001-preview-07.png
[screenshot-08]:        {{< baseurl >}}assets/posts/frontend/2021/06/001-preview-08.png
[screenshot-09]:        {{< baseurl >}}assets/posts/frontend/2021/06/001-preview-09.png
[screenshot-10]:        {{< baseurl >}}assets/posts/frontend/2021/06/001-preview-10.png

[002-post-navigation]:  {{< baseurl >}}assets/posts/frontend/2021/06/002-post-navigation.png