---
type   : post
title  : "Bootstrap - Preparation"
date   : 2021-06-03T09:17:35+07:00
slug   : bootstrap5-preparation
categories: [frontend]
tags      : [bootstrap, css]
keywords  : [plain html, custom theme, summary, overview]
author : epsi
opengraph:
  image: assets/posts/frontend/2021/06/001-preview-00.png

toc    : "toc-2021-06-html-bootstrap5-step"

excerpt:
  Prepare modular HTML using Nunjucks template

---

### Preface

> Step Zero: Prepare modular HTML using Nunjucks template

A template system can help you produce healty code.
Instead of long HTML, you can separate chunks of HTML element,
into different files.
You can find your element easily this way.
So you can focus on specific HTML element.

Now I can put recurring long parts such as navbar,
in separate file, and focus on small parts,
such as widget, or blog post layout.
Using one big HTML only without template used to be exhausting,
but now it is easier with modular files.

This is basically a template introduction.
It looks like learning frontend, using backend approach.
Of course you need some adaptation to use template approach.
Learning process may take while, but it is worth to learn.

-- -- --

### 1: Ready to Use Examples.

Bootstrap examples provided in with this article series.
I intentionally make result of compiled templates for each chapter.
You can just open HTML file in the browser, and see the result.
This way is easier for beginner.

#### Repository

You can start the journey by clone the git repository.

{{< highlight bash >}}
❯ git clone https://gitlab.com/epsi-rns/tutor-html-bootstrap5/
{{< / highlight >}}

You can see all chapters here, form Step-00 to Step-10.

![Bootstrap5: All Chapters][003-exa-root]

#### Open HTML using File Manager

There is two ways to open this file.

1. Using file protocol
2. Using http protocol

You just can open the ready to use HTML file for each chapter,
by right-clicking the file in file manager and open the HTML file,
in your favorite browser, such as chrome, edge, or firefox.

![Bootstrap5: File Protocol][004-protocol-file]

This compiled HTML files are available for all chapters.

#### Open HTML using Browser Sync

For convenience, you can also open HTML file using HTTP protocol.
This can be done using using a few options,
such as javascript based browser-sync.

{{< highlight bash >}}
❯ browser-sync start --server --directory
{{< / highlight >}}

Alternatively you can use Python.

{{< highlight bash >}}
❯ python -m http.server
{{< / highlight >}}

Or even PHP

{{< highlight bash >}}
❯ php -S localhost:8000
{{< / highlight >}}

Since we want to also use Nunjucks template later on,
we'd better use browser-sync for easy integration with Nunjucks.

{{< highlight bash >}}
❯ npm i -g browser-sync
❯ cd ~/bs5/tutor-html-bootstrap5
❯ browser-sync start --server --directory
[Browsersync] Access URLs:
 --------------------------------------
       Local: http://localhost:3000
    External: http://192.168.0.191:3000
 --------------------------------------
          UI: http://localhost:3001
 UI External: http://localhost:3001
 --------------------------------------
[Browsersync] Serving files from: ./
{{< / highlight >}}

![Bootstrap5: BrowserSync][004-browser-sync]

Now you can open [localhost:3000](http://localhost:3000) in your browser.
You can change also change the port number if you need.
The browser can navigate into all chapters.

![Bootstrap5: Open File in BrowserSync][004-localhost-3000]

Then you can open the file that you need.

![Bootstrap5: HTTP Protocol][004-protocol-http]

You can check the URL in below section of that Qutebrowser,
is different than previous file protocol.

-- -- --

### 2: Modularization

I use different approach,
while developing each case.
Instead of just giving raw HTML files,
I modularized each HTML parts into chuncks.

#### Gulp, Grunt, Webpack?

The thing is I don't want to repeat myself.
This could be done using Nunjucks and Grunt.

* [Template - Grunt - Nunjucks][nunjucks-with-grunt]

Since I have already make an article about Nunjucks and Grunt.
I think I'd better use other method.
After some research, I found this good article below:

* [Karatasebu - Nunjucks with Gulp][nunjucks-with-gulp]

I decide to use Gulp instead of Grunt.

#### Directory Structure

Modular approach is easier for beginner.
For each steps, we have this directory structure.

![Bootstrap5: Step One][003-exa-step-00]

{{< highlight bash >}}
❯ tree
.
├── 001-html-layout.njk
├── contents
│   └── main.njk
├── heads
│   └── meta.njk
├── layouts
│   └── base.njk
└── shared
    ├── footer.njk
    └── header.njk
{{< / highlight >}}

You can see all the separated templates here.

![Bootstrap5: Summary One][009-pane-summary]

#### Using Nunjucks

We need to install Nunjucks first.

{{< highlight bash >}}
❯ npm i -g nunjucks nunjucks-cli
{{< / highlight >}}

We can operate the Nunjucks in CLI.

{{< highlight bash >}}
❯ cd ~/bs5/tutor-html-bootstrap5
❯ cd step-00/views
❯ nunjucks 001-html-layout.njk -p . -o .
Rendering: 001-html-layout.njk
~/bs/tutor-html-bootstrap5/step-00/views
{{< / highlight >}}

Now check the directory,
we can see a new HTML file has generated there

{{< highlight bash >}}
❯ exa
001-html-layout.html  contents  layouts
001-html-layout.njk   heads     shared
{{< / highlight >}}

We are going to automate this later,
in this article.

-- -- --

### 3: Partials Nunjucks

For better explanation,
consider examine the HTML source,
as partial templates.

#### Source Code: Step-00

Source code for this chapter can be obtained here:

* [gitlab.com/.../step-00/][tutor-step-00]

#### Simple Layout

You can begin learning HTML document,
without any framework.
Our simple HTML5 layout could be like this.

{{< highlight jinja >}}
<!DOCTYPE html>
<html>

<head>
  {% include './heads/meta.njk' %}
</head>

<body>
  {% include './shared/header.njk' %}
  {% include './contents/main.njk' %}
  {% include './shared/footer.njk' %}
</body>

</html>
{{< / highlight >}}

#### Base Layout

For flexibility reason we can split code above
using inhertance.

Base file:

* [gitlab.com/.../views/layouts/base.njk][njk-l-base]

{{< highlight jinja >}}
<!DOCTYPE html>
<html>

<head>
  {% block htmlhead %}
  {% endblock %}
</head>

<body>
  {% block header %}
  {% endblock %}

  {% block content %}
  {% endblock %}

  {% block footer %}
  {% endblock %}
</body>

</html>
{{< / highlight >}}

Paired with blocks:

* [gitlab.com/.../views/001-html-layout.njk][njk--001-layout]

{{< highlight jinja >}}
{% extends './layouts/base.njk' %}

{% block htmlhead %}
  {% include './heads/meta.njk' %}
{% endblock %}

{% block header %}
  {% include './shared/header.njk' %}
{% endblock %}

{% block content %}
  {% include './contents/main.njk' %}
{% endblock %}

{% block footer %}
  {% include './shared/footer.njk' %}
{% endblock %}
{{< / highlight >}}

![Bootstrap5: Nunjucks: Base][005-l-base]

Mostly I start from page layout,
then HTML elements later.

#### Meta and Main

And the rest, Meta and Main.

* [gitlab.com/.../views/heads/meta.njk][njk-h-meta]
* [gitlab.com/.../views/contents/main.njk][njk-c-main]

{{< highlight jinja >}}

  <title>Your mission. Good Luck!</title>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Meta][005-h-meta]

{{< highlight jinja >}}

  <!-- main -->
  <div>
    <p>To have, to hold, to love,
    cherish, honor, and protect?</p>

    <p>To shield from terrors known and unknown?
    To lie, to deceive?</p>

    <p>To live a double life,
    to fail to prevent her abduction,
    erase her identity, 
    force her into hiding,
    take away all she has known.</p>
  </div>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Main][005-c-main]

#### Header and Footer

Also Header and Footer.

* [gitlab.com/.../views/shared/header.njk][njk-s-header]
* [gitlab.com/.../views/shared/footer.njk][njk-s-footer]

{{< highlight jinja >}}

  <!-- header -->
  <p><i>Your mission, should you decide to accept it.</i></p>
  <br/>
{{< / highlight >}}

{{< highlight jinja >}}

  <!-- footer -->
  <br/>
  <p><i>As always, should you be caught or killed,
  any knowledge of your actions will be disavowed.</i></p>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Header and Footer][005-s-headfoot]

#### HTML Output

> Static Page

The compiled output is long, 

* [gitlab.com/.../001-html-layout.html][html-001-layout]

This would be shown as below figure:

![Bootstrap5: HTML Output: Source][006-html-output]

Now we can open it in browsersync or using file directly.

![Bootstrap5: HTML Output: in Browser][006-simple]

-- -- --

### 4: Automation

Disclaimer: _For new bootstrap user, you can skip this part entirely_.

Typing command to compile Nunjucks is cumbersome.
We can utilize `Gulp` and automate each chapter.
We need to configure Gulp to do these task:

* Generate HTML: One chapter at a time
* Generate All HTML
* Watch Changes

#### Gulp Configuration

The configuration can be obtained here:

* [gitlab.com/.../tutor-html-bootstrap5/gulpfile.js][js-gulpfile]

#### Installation

You can refer to:

* [Karatasebu - Nunjucks with Gulp][nunjucks-with-gulp]

With exception I add 'merge-stream' package.

#### Prepare Gulpfile

First we need to initialize required variables:

{{< highlight javascript >}}
const gulp = require("gulp")
const nunjucksRender = require("gulp-nunjucks-render")
const browserSync = require("browser-sync").create();
const merge = require('merge-stream');
{{< / highlight >}}

And also defined all chapters,
that need to be automated.

{{< highlight javascript >}}
const folders = [
  'step-00',
  'step-01',
  'step-02',
  'step-03',
  'step-04',
  'step-05',
  'step-06',
  'step-07',
  'step-08',
  'step-09',
  'step-10'
];
{{< / highlight >}}

![Bootstrap5: Gulpfile.js: Require Const][007-gulp-const]

#### One chapter at a time

> Generate HTMLs

{{< highlight javascript >}}
folders.map( (task) => {
  const folder = './'+task;
  gulp.task(task, () => {  
    return gulp.src(folder+'/views/*.njk')
      .pipe(nunjucksRender({
        path: [folder+'/views'],
      }))
      .pipe(gulp.dest(folder+'/'))
  });
});
{{< / highlight >}}

![Bootstrap5: Gulpfile.js: Task Step][007-task-step]

Then you can run in CLI

{{< highlight bash >}}
❯ gulp step-00
[22:23:18] Using gulpfile ~/bs5/tutor-html-bootstrap5/gulpfile.js
[22:23:18] Starting 'step-00'...
[22:23:18] Finished 'step-00' after 50 ms
{{< / highlight >}}

#### All chapters at once

> Generate HTMLs

{{< highlight javascript >}}
gulp.task("all-html", () => {
  const tasks = folders.map( (folder) => {
    return gulp.src(folder+'/views/*.njk')
      .pipe(nunjucksRender({
        path: [folder+'/views']
      }))
      .pipe(gulp.dest(folder+'/'))
  });

  return merge(tasks);
});
{{< / highlight >}}

![Bootstrap5: Gulpfile.js: Task All HTML][007-task-all]

In CLI, you can type.

{{< highlight bash >}}
❯ gulp all-html
[22:23:41] Using gulpfile ~/bs5/tutor-html-bootstrap5/gulpfile.js
[22:23:41] Starting 'all-html'...
[22:23:42] Finished 'all-html' after 522 ms
{{< / highlight >}}

#### Watching Task

> Regenerate for any changes.

{{< highlight javascript >}}
gulp.task("watch", () => {
  browserSync.init({
    server: {
      baseDir: "./"
    },
    directory: true
  })

  gulp.watch('./**/*.html')
    .on("change", browserSync.reload);

  folders.map( (task) => {
    folder = './' + task
    gulp.watch(folder+'/views/**/*.njk',
      gulp.series(task));
  });
});
{{< / highlight >}}

In CLI, you can type.

{{< highlight bash >}}
❯ gulp watch
[22:24:04] Using gulpfile ~/bs5/tutor-html-bootstrap5/gulpfile.js
[22:24:04] Starting 'watch'...
[Browsersync] Access URLs:
 --------------------------------------
       Local: http://localhost:3000
    External: http://192.168.0.191:3000
 --------------------------------------
          UI: http://localhost:3001
 UI External: http://localhost:3001
 --------------------------------------
[Browsersync] Serving files from: ./
{{< / highlight >}}

![Bootstrap5: Gulpfile.js: Task Watch][007-task-watch]

#### Consideration

> Is Gulp overkill?

Making this configuration is considered easy for me.
And once it runs, I can automate all chapters,
So I can sit on my chair,
and get my brain focused on HTML code.
But I know gulp is not for everyone.
So it is your choice to decide.

Again, for new bootstrap user,
_you can skip this part entirely_.
**You don't need Gulp to learn bootstrap**.
But you'd better go back later,
when you have time, to learn javascript.

-- -- --

### What is Next 🤔?

After prepartion, we can use bootstrap right away.

Consider continue reading [ [Bootstrap 5 - Minimal][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2021/06/05/bootstrap5-minimal/
[nunjucks-with-gulp]:   https://karatasebu.medium.com/nunjucks-with-gulp-be05c1ca7f4f
[nunjucks-with-grunt]:  https://epsi-rns.gitlab.io/frontend/2020/05/06/template-grunt-nunjucks/

[tutor-step-00]:        {{< tutor-html-bs5 >}}/step-00/

[//]: <> ( -- -- -- links below -- -- -- )

[003-exa-root]:         {{< baseurl >}}assets/posts/frontend/2021/06/003-exa-root.png
[003-exa-step-00]:      {{< baseurl >}}assets/posts/frontend/2021/06/003-exa-step-00.png

[004-browser-sync]:     {{< baseurl >}}assets/posts/frontend/2021/06/004-browser-sync.png
[004-localhost-3000]:   {{< baseurl >}}assets/posts/frontend/2021/06/004-localhost-3000.png
[004-protocol-file]:    {{< baseurl >}}assets/posts/frontend/2021/06/004-protocol-file.png
[004-protocol-http]:    {{< baseurl >}}assets/posts/frontend/2021/06/004-protocol-http.png

[005-c-main]:           {{< baseurl >}}assets/posts/frontend/2021/06/005-c-main.png
[005-h-meta]:           {{< baseurl >}}assets/posts/frontend/2021/06/005-h-meta.png
[005-l-base]:           {{< baseurl >}}assets/posts/frontend/2021/06/005-l-base.png
[005-nunjucks-cli]:     {{< baseurl >}}assets/posts/frontend/2021/06/005-nunjucks-cli.png
[005-s-headfoot]:       {{< baseurl >}}assets/posts/frontend/2021/06/005-s-headfoot.png

[006-html-output]:      {{< baseurl >}}assets/posts/frontend/2021/06/006-html-output.png
[006-simple]:           {{< baseurl >}}assets/posts/frontend/2021/06/006-simple.png

[007-gulp-const]:       {{< baseurl >}}assets/posts/frontend/2021/06/007-gulp-const.png
[007-task-all]:         {{< baseurl >}}assets/posts/frontend/2021/06/007-task-all.png
[007-task-step]:        {{< baseurl >}}assets/posts/frontend/2021/06/007-task-step.png
[007-task-watch]:       {{< baseurl >}}assets/posts/frontend/2021/06/007-task-watch.png

[009-pane-summary]:     {{< baseurl >}}assets/posts/frontend/2021/06/009-pane-summary.png

[//]: <> ( -- -- -- links below -- -- -- )

[js-gulpfile]:          {{< tutor-html-bs5 >}}/gulpfile.js

[njk--001-layout]:      {{< tutor-html-bs5 >}}/step-00/views/001-html-layout.njk
[html-001-layout]:      {{< tutor-html-bs5 >}}/step-00/001-html-layout.html

[njk-c-main]:           {{< tutor-html-bs5 >}}/step-00/views/contents/main.njk
[njk-h-meta]:           {{< tutor-html-bs5 >}}/step-00/views/heads/meta.njk
[njk-l-base]:           {{< tutor-html-bs5 >}}/step-00/views/layouts/base.njk
[njk-s-header]:         {{< tutor-html-bs5 >}}/step-00/views/shared/header.njk
[njk-s-footer]:         {{< tutor-html-bs5 >}}/step-00/views/shared/footer.njk