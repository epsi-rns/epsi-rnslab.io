---
type   : post
title  : "Bootstrap - Minimal"
date   : 2021-06-05T09:17:35+07:00
slug   : bootstrap5-minimal
categories: [frontend]
tags      : [bootstrap, css]
keywords  : [plain html, custom theme, skeleton, bootstrap cdn]
author : epsi
opengraph:
  image: assets/posts/frontend/2021/06/001-preview-01.png

toc    : "toc-2021-06-html-bootstrap5-step"

excerpt:
  Make custom theme, step by step.
  Show minimal page in Bootstrap.

---

### Preface

> Step One: Using Bootstrap for the first time

From HTML5 without any stylesheet.
We can add CSS framework.
And try cool bootstrap components.

{{< embed-video width="480" height="480"
    src="assets/posts/frontend/2021/06/015-bootstrap-navbar.mp4" >}}

-- -- --

### 1: Chapter Overview

Consider examine the directory structure.

#### Source Code: Step-01

The source code for this chapter is,
available for download at the following link:

* [gitlab.com/.../step-01/][tutor-step-01]

#### Bootstrap v4

The obsolete Bootstrap v4 article series is also available.

* [Bootstrap - Minimal][local-bootstrap-v4]

#### General Preview

This below is general preview of,
what page that we want to achieve.

![Bootstrap5: General Preview of Simple Page Layout][layout-simple]

#### Plain HTML

> Compiled HTML Document

The outputs, will be generated as below:

{{< highlight bash >}}
❯ ls -1 *.html
012-head-cdn.html
013-head-local.html
015-navbar.html
{{< / highlight >}}

![Bootstrap5: Directory Structure: Compiled][010-exa-htmls]

We have three document to examine.

#### Assets

> CSS and Javascript

We are going to use some assets.
Official style and javascript from bootstraps.
And also custom css.

{{< highlight bash >}}
❯ tree -C css js
css
├── bootstrap.css
├── main.css
└── sticky-footer.css
js
└── bootstrap.min.js

2 directories, 4 files
{{< / highlight >}}

![Bootstrap5: Directory Structure: Assets][010-exa-assets]

#### Templates

> Nunjucks

More document, means more templates.
But don't worry, I made reusable templates.
So we don't need to repeat ourselves.

{{< highlight bash >}}
❯ tree -C views
views
├── 012-head-cdn.njk
├── 013-head-local.njk
├── 015-navbar.njk
├── contents
│   ├── 011-main.njk
│   └── 015-main.njk
├── heads
│   ├── 011-meta.njk
│   ├── 012-links.njk
│   ├── 013-links.njk
│   └── 015-links.njk
├── layouts
│   ├── 011-base.njk
│   └── 015-base.njk
└── shared
    ├── 011-footer.njk
    ├── 011-header.njk
    ├── 015-footer.njk
    └── 015-header.njk

5 directories, 15 files
{{< / highlight >}}

![Bootstrap5: Directory Structure: Templates][010-nerd-views-blocks]

We can now start coding the HTML documents.

-- -- --

### 2: Loading Assets

There are some alternative to load CSS.

For this tutorial. 
I suggest to use local CSS.
Because we are going to make a custom Bootstrap CSS

#### Using CDN Bootstrap Assets

It is as simple as 

{{< highlight html >}}
  <link rel="stylesheet"
    href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" 
    integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" 
    crossorigin="anonymous">
{{< / highlight >}}

And if you need the javascript, you can add:

{{< highlight html >}}
  <script
    src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4"
    crossorigin="anonymous"></script>
{{< / highlight >}}

The sample code is here:

* [gitlab.com/.../views/heads/012-links.njk][njk-h-012-links]

![Bootstrap5: Nunjucks: Assets: CDN][012-h-links]

#### Using Local Bootstrap Assets

You need to download `bootstrap.css`,
and also `bootstrap.min.js` from the official site.
And put the artefact to `css` and `js` directory.

* [gitlab.com/.../views/heads/013-links.njk][njk-h-013-links]

{{< highlight html >}}
  <link rel="stylesheet"
    type="text/css" href="./css/bootstrap.css">
  <script src="./js/bootstrap.min.js"></script>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Assets: Local][013-h-links]

Some bootstrap functionality required javascript.

#### Adding Meta

As the official guidance said.
We have to add a few meta tags on the `head`.

* [gitlab.com/.../views/heads/011-meta.njk][njk-h-011-meta]

{{< highlight html >}}
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,
    initial-scale=1, shrink-to-fit=no">
  <title>Your mission. Good Luck!</title>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Heads: Meta][011-h-meta]

#### HTML Head

We have already shown the HTML chunks above.

* 011-meta.njk
* 012-links.njk
* 013-links.njk

We are going to use these chunks in the next section.

-- -- --

### 3: CDN and Local Templates

Consider build the HTML document right away.

#### Templates

The specific templates that we need for
bootstrap using CDN are shown below:

{{< highlight bash >}}
views
├── 012-head-cdn.njk
└── heads
    ├── 011-meta.njk
    └── 012-links.njk
{{< / highlight >}}

The specific templates that we need for
bootstrap using local assets are shown below:

{{< highlight bash >}}
views
├── 013-head-local.njk
└── heads
    ├── 011-meta.njk
    └── 013-links.njk
{{< / highlight >}}

Since we have similar part for `012` and `013` document,
all part other than assets links is considered reusable chunks.
I use `011` number for reusable chunks.

{{< highlight bash >}}
views
├── contents
│   └── 011-main.njk
├── layouts
│   └── 011-base.njk
└── shared
    ├── 011-footer.njk
    └── 011-header.njk
{{< / highlight >}}

#### Layout

> Reusable Parent Template

This is the skeleton for of the building block.
The template itself is rather a reusable template.

* [gitlab.com/.../views/layouts/011-base.njk][njk-l-011-base]

{{< highlight jinja >}}
<!DOCTYPE html>
<html>

<head>
  {% block htmlhead %}{% endblock %}
</head>

<body>
  <div class="container">
    {% block header %}{% endblock %}
    {% block content %}{% endblock %}
    {% block footer %}{% endblock %}
  </div>
</body>

</html>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Base Layout: Parent Template][011-l-base]

#### Blocks: for CDN

> Nunjucks Document Source

We can put different chunk for each block.

* [gitlab.com/.../views/012-head-cdn.njk][njk--012-head-cdn]

{{< highlight jinja >}}
{% extends './layouts/011-base.njk' %}

{% block htmlhead %}
  {% include './heads/011-meta.njk' %}
  {% include './heads/012-links.njk' %}
{% endblock %}

{% block header %}
  {% include './shared/011-header.njk' %}
{% endblock %}

{% block content %}
  {% include './contents/011-main.njk' %}
{% endblock %}

{% block footer %}
  {% include './shared/011-footer.njk' %}
{% endblock %}
{{< / highlight >}}

![Bootstrap5: Nunjucks: Source: CDN][012-njk-head-cdn]

#### Blocks: for Local

> Nunjucks Document Source

The only difference with the CDN document is the HTML assets.

* [gitlab.com/.../views/013-head-local.njk][njk--013-head-local]

{{< highlight jinja >}}
{% extends './layouts/011-base.njk' %}

{% block htmlhead %}
  {% include './heads/011-meta.njk' %}
  {% include './heads/013-links.njk' %}
{% endblock %}

{% block header %}
  {% include './shared/011-header.njk' %}
{% endblock %}

{% block content %}
  {% include './contents/011-main.njk' %}
{% endblock %}

{% block footer %}
  {% include './shared/011-footer.njk' %}
{% endblock %}
{{< / highlight >}}

![Bootstrap5: Nunjucks: Source: Local][013-njk-head-local]

Of course we can optimize this repetition using nunjucks variable.
But I leave this this way, I do not want to go deep into Nunjucks,'
and forget the bootstrap stuff.

-- -- --

### 4: Reusable Templates

For both HTML target: CDN and Local.

#### Overview

Beside the layout, we have this common templates.

{{< highlight bash >}}
views
├── contents
│   └── 011-main.njk
├── heads
│   └── 011-meta.njk
└── shared
    ├── 011-footer.njk
    └── 011-header.njk
{{< / highlight >}}

![Bootstrap5: Nunjucks: Pane Summary][011-pane-summary]

Consider examine one by one.

#### Main

> Content

Now consider having fun for while.
And give more example class, and see what happened.

* [gitlab.com/.../views/contents/011-main.njk][njk-c-011-main]

{{< highlight html >}}
    <!-- main -->
    <ul class="list-group">
      <li class="list-group-item">
      To have, to hold, to love,
      cherish, honor, and protect?</li>

      <li class="list-group-item">
      To shield from terrors known and unknown?
      To lie, to deceive?</li>

      <li class="list-group-item">
      To live a double life,
      to fail to prevent her abduction,
      erase her identity, 
      force her into hiding,
      take away all she has known.</li>
    </ul>
{{< / highlight >}}

#### List Group

We need to see if Bootstrap class work correctly.
Consider add this `container` and `list-group` class.

![Bootstrap5: Nunjucks: Content: Main][011-c-main]

#### Header and Footer

> Header

Just a simple header.

* [gitlab.com/.../views/shared/011-header.njk][njk-s-011-header]

{{< highlight html >}}
    <!-- header -->
    <p><i>Your mission,
        should you decide to accept it.</i></p>
    <br/>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Shared: Header][011-s-header]

> Footer

And another simple footer.

* [gitlab.com/.../views/shared/011-footer.njk][njk-s-011-footer]

{{< highlight html >}}
    <!-- footer -->
    <br/>
    <p><i>As always, should you
        be caught or killed,
        any knowledge of your actions
        will be disavowed.</i></p>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Shared: Footer][011-s-footer]

#### HTML Preview: CDN

> HTML Document Target

Hve a look at the result by opening the file in Browser.

* [gitlab.com/.../step-01/012-head-cdn.html][html-012-head-cdn]

![Bootstrap5: Qutebrowser: CDN][012-qb-head-cdn]

And also the HTML source.

![Bootstrap5: Qutebrowser: CDN: Source][012-qb-source]

#### HTML Preview: Local

> HTML Document Target

The local output in browser is exactly the same.
The only different is the head part of the source.

* [gitlab.com/.../step-01/013-head-local.html][html-013-head-local]

![Bootstrap5: Qutebrowser: Local][013-qb-head-local]

There is more fun than just this.
We are going to explore later.

-- -- --

### 5: Bootstrap: Navbar Class

> As the site grows...

Let's try something else,
that is not too simple.

For the rest of this show case,
I'm using both `navbar header` and `footer`.

#### Overview

Beside the layout, we have this common templates.

{{< highlight bash >}}
views
├── contents
│   ├── 015-main.njk
│   └── 015-links.njk
├── heads
│   └── 015-meta.njk
└── shared
    ├── 015-footer.njk
    └── 015-header.njk
{{< / highlight >}}

![Bootstrap5: Nunjucks: Pane Summary][015-pane-summary]

Consider examine one by one.

#### As Header

Navbar lies on header part.

* [gitlab.com/.../views/shared/015-header.njk][njk-s-015-header]

{{< highlight html >}}
  <!-- header -->
  <nav class="navbar navbar-expand-md
      navbar-dark bg-dark">
    <div class="navbar-collapse">
      <ul class="navbar-nav mx-2">
        <li class="nav-item">
          <a class="nav-link active"
             href="#">Home
            <span class="visually-hidden"
            >(current)</span></a>
        </li>
      </ul>
    </div>
  </nav>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Shared: Header][015-s-header]

#### As Footer

Navbar also lies on footer part.

* [gitlab.com/.../views/shared/015-footer.njk][njk-s-015-footer]

{{< highlight html >}}
  <!-- footer -->
  <footer class="footer">
    <div class="bg-dark text-light text-center">
      &copy; 2021.
    </div>
  </footer>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Shared: Footer][015-s-footer]

#### Assets

> Additional CSS

We need a little tweak
To have proper **layout**,
this navbar require additional CSS.

* [gitlab.com/.../step-01/css/main.css][css-010-main]

{{< highlight bash >}}
❯ tree -C css js
css
├── main.css
└── sticky-footer.css
{{< / highlight >}}

{{< highlight css >}}
.layout-base {
  padding-top: 3rem;
  padding-bottom: 2rem;
}
{{< / highlight >}}

![Bootstrap5: Assets: CSS: Main][010-css-main]

And for the sticky footer.
I grab from the official site.

* [gitlab.com/.../step-01/css/sticky-footer.css][css-010-sticky-footer]

{{< highlight css >}}
html {
  position: relative;
  min-height: 100%;
}
body {
  margin-bottom: 60px; /* Margin bottom by footer height */
}
.footer {
  position: absolute;
  bottom: 0;
  width: 100%;
  height: 60px; /* Set the fixed height of the footer here */
  line-height: 60px; /* Vertically center the text there */
  background-color: #f5f5f5;
}
{{< / highlight >}}

![Bootstrap5: Assets: CSS: Sticky][010-css-sticky]

#### HTML Head

You should define `styles` on `<head>` element.
We will do it in templates.

* [gitlab.com/.../views/heads/015-links.njk][njk-h-015-links]

{{< highlight html >}}
  <link rel="stylesheet"
    type="text/css" href="./css/bootstrap.css">
  <link rel="stylesheet"
    type="text/css" href="./css/main.css">
  <link rel="stylesheet"
    type="text/css" href="./css/sticky-footer.css">
{{< / highlight >}}

![Bootstrap5: Nunjucks: Heads: Links][015-h-links]

#### Templates

> Nunjucks

No reusable templates here,
except the HTML head meta.

{{< highlight bash >}}
views
├── 015-navbar.njk
├── contents
│   └── 015-main.njk
├── heads
│   ├── 011-meta.njk
│   └── 015-links.njk
├── layouts
│   └── 015-base.njk
└── shared
    ├── 015-footer.njk
    └── 015-header.njk

5 directories, 15 files
{{< / highlight >}}

#### Layout

Consider change the layout into simpler HTML tags as below:

* [gitlab.com/.../views/layouts/015-base.njk][njk-l-015-base]

{{< highlight jinja >}}
<!DOCTYPE html>
<html>

<head>
  {% block htmlhead %}{% endblock %}
</head>

<body>
  {% block header %}{% endblock %}
  {% block content %}{% endblock %}
  {% block footer %}{% endblock %}
</body>

</html>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Layout: Base][015-l-base]

#### Blocks

> Nunjucks Document Source

Well... pretty similar with the tree above right?

* [gitlab.com/.../views/015-navbar.njk][njk--015-navbar]

{{< highlight jinja >}}
{% extends './layouts/015-base.njk' %}

{% block htmlhead %}
  {% include './heads/011-meta.njk' %}
  {% include './heads/015-links.njk' %}
{% endblock %}

{% block header %}
  {% include './shared/015-header.njk' %}
{% endblock %}

{% block content %}
  {% include './contents/015-main.njk' %}
{% endblock %}

{% block footer %}
  {% include './shared/015-footer.njk' %}
{% endblock %}
{{< / highlight >}}

![Bootstrap5: Nunjucks: Blocks: Navbar][015-njk-navbar]

#### Main

It is almost the same with previous,
except the additional custom CSS class the `layout-base`.

* [gitlab.com/.../views/contents/015-main.njk][njk-c-015-main]

{{< highlight html >}}
  <!-- main -->
  <div class="layout-base">
    <main role="main" class="container">
      <ul class="list-group">
        <li class="list-group-item">
        To have, to hold, to love,
        cherish, honor, and protect?</li>

        <li class="list-group-item">
        To shield from terrors known and unknown?
        To lie, to deceive?</li>

        <li class="list-group-item">
        To live a double life,
        to fail to prevent her abduction,
        erase her identity, 
        force her into hiding,
        take away all she has known.</li>
      </ul>
    </main>
  </div>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Contents: Main][015-c-main]

#### Browser Preview

> Screenshot

And have fun with the result.
Consider open the file in Browser.

![Bootstrap5: Qutebrowser: Minimal Navigation Bar][015-qb-navbar]

#### Static Page

> HTML Document Target

As a summary from this chapter,
all the **html element** is as shown below.

* [gitlab.com/.../step-01/015-navbar.html][html-015-navbar]

Consider examine the HTML output, above.

-- -- --

### What is Next 🤔?

Thank you for reading.
I know it still feels like a template tutorial,
than stylesheet one. That is why we have to go on.
There are, some interesting topic
about <code>Navigation Bar</code> in Bootstrap 5. 

Consider continue reading [ [Bootstrap5 - Navigation Bar - Class][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2021/06/11/bootstrap5-navbar-class/
[local-bootstrap-v4]:   {{< baseurl >}}frontend/2020/03/02/bootstrap-minimal/

[tutor-step-01]:        {{< tutor-html-bs5 >}}/step-01/

[//]: <> ( -- -- -- links below -- -- -- )

[layout-simple]:        {{< baseurl >}}assets/posts/frontend/2021/06/layout-simple.png

[010-exa-assets]:       {{< baseurl >}}assets/posts/frontend/2021/06/010-exa-assets.png
[010-exa-htmls]:        {{< baseurl >}}assets/posts/frontend/2021/06/010-exa-htmls.png
[010-exa-views]:        {{< baseurl >}}assets/posts/frontend/2021/06/010-exa-views.png
[010-nerd-views-blocks]:{{< baseurl >}}assets/posts/frontend/2021/06/010-nerd-views-blocks.png
[010-css-main]:         {{< baseurl >}}assets/posts/frontend/2021/06/010-css-main.png
[010-css-sticky]:       {{< baseurl >}}assets/posts/frontend/2021/06/010-css-sticky.png

[011-c-main]:           {{< baseurl >}}assets/posts/frontend/2021/06/011-c-main.png
[011-h-meta]:           {{< baseurl >}}assets/posts/frontend/2021/06/011-h-meta.png
[011-l-base]:           {{< baseurl >}}assets/posts/frontend/2021/06/011-l-base.png
[011-s-footer]:         {{< baseurl >}}assets/posts/frontend/2021/06/011-s-footer.png
[011-s-header]:         {{< baseurl >}}assets/posts/frontend/2021/06/011-s-header.png

[012-h-links]:          {{< baseurl >}}assets/posts/frontend/2021/06/012-h-links.png
[012-njk-head-cdn]:     {{< baseurl >}}assets/posts/frontend/2021/06/012-njk-head-cdn.png

[013-h-links]:          {{< baseurl >}}assets/posts/frontend/2021/06/013-h-links.png
[013-njk-head-local]:   {{< baseurl >}}assets/posts/frontend/2021/06/013-njk-head-local.png

[015-c-main]:           {{< baseurl >}}assets/posts/frontend/2021/06/015-c-main.png
[015-h-links]:          {{< baseurl >}}assets/posts/frontend/2021/06/015-h-links.png
[015-l-base]:           {{< baseurl >}}assets/posts/frontend/2021/06/015-l-base.png
[015-njk-navbar]:       {{< baseurl >}}assets/posts/frontend/2021/06/015-njk-navbar.png
[015-s-footer]:         {{< baseurl >}}assets/posts/frontend/2021/06/015-s-footer.png
[015-s-header]:         {{< baseurl >}}assets/posts/frontend/2021/06/015-s-header.png

[012-qb-head-cdn]:      {{< baseurl >}}assets/posts/frontend/2021/06/012-qb-head-cdn.png
[012-qb-source]:        {{< baseurl >}}assets/posts/frontend/2021/06/012-qb-source.png
[013-qb-head-local]:    {{< baseurl >}}assets/posts/frontend/2021/06/013-qb-head-local.png
[015-qb-navbar]:        {{< baseurl >}}assets/posts/frontend/2021/06/015-qb-navbar.png

[011-pane-summary]:     {{< baseurl >}}assets/posts/frontend/2021/06/011-pane-summary.png
[015-pane-summary]:     {{< baseurl >}}assets/posts/frontend/2021/06/015-pane-summary.png

[//]: <> ( -- -- -- links below -- -- -- )

[css-010-main]:         {{< tutor-html-bs5 >}}/step-01/css/main.css
[css-010-sticky-footer]:{{< tutor-html-bs5 >}}/step-01/css/sticky-footer.css

[njk--012-head-cdn]:    {{< tutor-html-bs5 >}}/step-01/views/012-head-cdn.njk
[njk--013-head-local]:  {{< tutor-html-bs5 >}}/step-01/views/013-head-local.njk
[njk--015-navbar]:      {{< tutor-html-bs5 >}}/step-01/views/015-navbar.njk
[html-012-head-cdn]:    {{< tutor-html-bs5 >}}/step-01/012-head-cdn.html
[html-013-head-local]:  {{< tutor-html-bs5 >}}/step-01/013-head-local.html
[html-015-navbar]:      {{< tutor-html-bs5 >}}/step-01/015-navbar.html

[njk-c-011-main]:       {{< tutor-html-bs5 >}}/step-01/views/contents/011-main.njk
[njk-c-015-main]:       {{< tutor-html-bs5 >}}/step-01/views/contents/015-main.njk
[njk-h-011-meta]:       {{< tutor-html-bs5 >}}/step-01/views/heads/011-meta.njk
[njk-h-012-links]:      {{< tutor-html-bs5 >}}/step-01/views/heads/012-links.njk
[njk-h-013-links]:      {{< tutor-html-bs5 >}}/step-01/views/heads/013-links.njk
[njk-h-015-links]:      {{< tutor-html-bs5 >}}/step-01/views/heads/015-links.njk
[njk-l-011-base]:       {{< tutor-html-bs5 >}}/step-01/views/layouts/011-base.njk
[njk-l-015-base]:       {{< tutor-html-bs5 >}}/step-01/views/layouts/015-base.njk
[njk-s-011-footer]:     {{< tutor-html-bs5 >}}/step-01/views/shared/011-footer.njk
[njk-s-011-header]:     {{< tutor-html-bs5 >}}/step-01/views/shared/011-header.njk
[njk-s-015-footer]:     {{< tutor-html-bs5 >}}/step-01/views/shared/015-footer.njk
[njk-s-015-header]:     {{< tutor-html-bs5 >}}/step-01/views/shared/015-header.njk
