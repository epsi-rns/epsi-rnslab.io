---
type   : post
title  : "Bootstrap - Navbar - Icons"
date   : 2021-06-16T09:17:35+07:00
slug   : bootstrap5-navbar-icons
categories: [frontend]
tags      : [bootstrap, css]
keywords  : [navigation bar, bootstrap icons]
author : epsi
opengraph:
  image: assets/posts/frontend/2021/06/001-preview-03.png

toc    : "toc-2021-06-html-bootstrap5-step"

excerpt:
  Make custom theme, step by step.
  Icon set choice for bootstrap navigation bar,
  using Bootstrap Icons.

---

### Preface

> Step Three: Icon set choice for bootstrap navigation bar.
> using Bootstrap Icons.

This icons topic is a two parts article.
For better comprehension of this article,
it is advised to read the previous article beforehand.

![Bootstrap5: Qutebrowser: Navigation Bar: Bootstrap Icons: SVG][034-qb-navbar-ibs-sm]

-- -- --

### 3: Navbar: Bootstrap Icons: CSS

Bootstrap also develop their own Icons.
Bootstrap Icons can be a good choice.

There are a few method to use this icons.
This article provide at least three technique.

* Using separated SVG icons, and put it in custom CSS.
* Using sprite SVG from CDN.
* Using inline SVG symbols

#### Official Documentation

* [icons.getbootstrap.com](https://icons.getbootstrap.com/)

#### Using Icon Set

Using Bootstrap Icon with separate SVG icon is,
as simple as this below:

{{< highlight html >}}
<span class="icon icon-invert house-door-fill"></span>
{{< / highlight >}}

For a real world example, you can examine this section below.

#### Assets

> CSS, JS, Images

I copy the icons that I need
and put it somewhere in my assets folder.

* [gitlab.com/.../assets/icons-bootstrap][dir-icons-bootstrap]

{{< highlight bash >}}
assets
├── css
│   ├── bootstrap.css
│   └── icons-bootstrap.css
├── icons-bootstrap
│   ├── archive-fill.svg
│   ├── calendar2-day-fill.svg
│   ├── calendar2-fill.svg
│   ├── folder-fill.svg
│   ├── house-door-fill.svg
│   ├── person-circle.svg
│   ├── person-fill.svg
│   └── tag-fill.svg
├── images
│   └── logo-gear.png
└── js
    ├── bootstrap.min.js
    └── popper.min.js
{{< / highlight >}}

#### Templates

> Nunjucks

{{< highlight bash >}}
views
├── 032-navbar-ibs-css.njk
├── heads
│   ├── 030-meta.njk
│   ├── 032-links.njk
│   └── 033-links.njk
├── layouts
│   └── base.njk
└── shared
    ├── 032-navbar-collapse.njk
    ├── 032-navbar-dropdown.njk
    ├── 032-navbar.njk
    └── navbar-button.njk
{{< / highlight >}}

#### Blocks

> Nunjucks Document Source

* [gitlab.com/.../views/032-navbar-ibs-css.njk][njk--032-nav-ibs-css]

{{< highlight jinja >}}
{% extends './layouts/base.njk' %}

{% block htmlhead %}
  {% include './heads/030-meta.njk' %}
  {% include './heads/032-links.njk' %}
{% endblock %}

{% block navbar %}
  {% include './shared/032-navbar.njk' %}
{% endblock %}
{{< / highlight >}}

![Bootstrap5: Nunjucks: Blocks: Navigation Bar: Bootstrap Icons][032-njk-navbar-ibs-css]

#### HTML Head

> Assets: Resource Links

* [gitlab.com/.../step-03/views/heads/032-links.njk][032-h-links]

{{< highlight html >}}
  <link rel="stylesheet" type="text/css"
    href="assets/css/bootstrap.css">

  <link rel="stylesheet" type="text/css"
    href="assets/css/icons-bootstrap.css">

  <script src="assets/js/popper.min.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Heads: Links][032-h-links]

#### Stylesheet

The custom stylesheet would be similar as below.

* [gitlab.com/.../assets/css/icons-bootstrap.css][css-icons-bootstrap]

{{< highlight css >}}

.icon {
  display: inline-block;
  vertical-align: text-bottom;
  width: 1.25em;
  height: 1.25em;

  background-size: 1.25em 1.25em;
}

.icon-invert {
  filter: invert(100%) brightness(120%);
}

.archive-fill {
  background: url(../icons-bootstrap/archive-fill.svg) no-repeat;
}
.person-fill { 
  background: url(../icons-bootstrap/person-fill.svg) no-repeat; 
}
.house-door-fill { 
  background: url(../icons-bootstrap/house-door-fill.svg) no-repeat;
}
.tag-fill {
  background: url(../icons-bootstrap/tag-fill.svg) no-repeat;
}
.folder-fill {
  background: url(../icons-bootstrap/folder-fill.svg) no-repeat;
}
.calendar2-day-fill {
  background: url(../icons-bootstrap/calendar2-day-fill.svg) no-repeat;
}
{{< / highlight >}}

#### Navigation Bar

* [gitlab.com/.../views/shared/032-navbar.njk][njk-s-032-navbar]

{{< highlight html >}}
  <nav class="navbar navbar-expand-sm navbar-dark
    fixed-top bg-dark">

    <div class="container-fluid">
      <a class="navbar-brand ms-4" href="#">
         <img src="assets/images/logo-gear.png"
           alt="Home" width="32" height="32"/>
      </a>

      {% include './navbar-button.njk' %}
      {% include './032-navbar-collapse.njk' %}
    </div>

  </nav>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Blocks: Navigation Bar][032-s-navbar]

#### Navbar: Collapsible

* [gitlab.com/.../views/shared/032-navbar-collapse.njk][njk-s-032-navbar-coll]

{{< highlight html >}}
<div class="collapse navbar-collapse"
    id="navbarCollapse">

  <ul class="navbar-nav me-auto">
    <li class="nav-item">
      <a class="nav-link active" href="#"
        ><span class="icon icon-invert house-door-fill"
        ></span>&nbsp;Blog <span class="visually-hidden"
        >(current)</span></a>
    </li>
    <li class="nav-item dropdown">
      {% include './032-navbar-dropdown.njk' %}
    </li>
    <li class="nav-item">
      <a class="nav-link active" href="#"
        ><span class="icon icon-invert person-fill"
        ></span>&nbsp;About</a>
    </li>
  </ul>
  <form class="d-flex form-inline mt-2 mt-md-0
      d-sm-none d-md-flex">
    <input class="form-control mr-sm-2" type="text" 
      placeholder="Search"
      aria-label="Search" role="search">
    <button class="btn btn-outline-light my-2 my-sm-0" 
      type="submit">Search</button>
  </form>

</div>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Blocks: Navigation Bar][032-s-navbar-collapse]

#### Navbar: Dropdown

Consider this dropdown menu as example

* [gitlab.com/.../views/shared/032-navbar-dropdown.njk][njk-s-032-navbar-drop]

{{< highlight html >}}
      <a  href="#"
          class="nav-link active dropdown-toggle"
          data-bs-toggle="dropdown"
          aria-expanded="false"
        ><span class="icon icon-invert archive-fill"
        ></span>&nbsp;Archives</a>
      <ul class="dropdown-menu">
        <li><a class="dropdown-item" href="#"
              ><span class="icon tag-fill"
              ></span>&nbsp;By Tags</a></li>
        <li><a class="dropdown-item" href="#"
              ><span class="icon folder-fill"
              ></span>&nbsp;By Category</a></li>
        <li><hr class="dropdown-divider"></li>
        <li><a class="dropdown-item" href="#"
              ><span class="icon calendar2-day-fill"
              ></span>&nbsp;By Chronology</a></li>
      </ul>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Blocks: Navigation Bar][032-s-navbar-dropdown]

#### Browser Preview

> Screenshot

And the expected result will vary beetween these two.

Using `sm` size for landscape tablet phone.

![Bootstrap5: Qutebrowser: Navigation Bar: Bootstrap Icons][032-qb-navbar-ibs-sm]

Using `xs` size portrait tablet phone.

![Bootstrap5: Qutebrowser: Navigation Bar: Bootstrap Icons][032-qb-navbar-ibs-xs]

#### Static Page

> HTML Document Target

* [gitlab.com/.../step-03/032-navbar-ibs-css.html][html-032-nav-ibs-css]

-- -- --

### 4: Navbar: Bootstrap Icons: CDN

This is basically the same as previous.

#### Using Icon Set

Using Bootstrap Icon with CDN is,
as simple as this below:

{{< highlight html >}}
<i class="bi-house-door-fill"
   style="font-size: 1.1rem; color: white;">
</i>
{{< / highlight >}}

For a real world example, you can examine this section below.

#### Assets

> CSS, JS, Images

{{< highlight bash >}}
assets
├── css
│   └── bootstrap.css
├── images
│   └── logo-gear.png
└── js
    ├── bootstrap.min.js
    └── popper.min.js
{{< / highlight >}}

#### Templates

> Nunjucks

{{< highlight bash >}}
views
├── 033-navbar-ibs-cdn.njk
├── heads
│   ├── 030-meta.njk
│   └── 033-links.njk
├── layouts
│   └── base.njk
└── shared
    ├── 032-navbar.njk
    ├── 033-navbar-collapse.njk
    ├── 033-navbar-dropdown.njk
    ├── 033-navbar.njk
    └── navbar-button.njk
{{< / highlight >}}

#### Blocks

> Nunjucks Document Source

* [gitlab.com/.../views/033-navbar-ibs-cdn.njk][njk--033-nav-ibs-cdn]

{{< highlight jinja >}}
{% extends './layouts/base.njk' %}

{% block htmlhead %}
  {% include './heads/030-meta.njk' %}
  {% include './heads/033-links.njk' %}
{% endblock %}

{% block navbar %}
  {% include './shared/033-navbar.njk' %}
{% endblock %}

{{< / highlight >}}

![Bootstrap5: Nunjucks: Blocks: Navigation Bar: Bootstrap Icons][033-njk-navbar-ibs-cdn]

#### HTML Head

> Assets: Resource Links

* [gitlab.com/.../views/heads/033-links.njk][033-h-links]

{{< highlight html >}}
  <link rel="stylesheet" type="text/css"
    href="assets/css/bootstrap.css">

  <script src="assets/js/popper.min.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>

  <link rel="stylesheet"
    href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.4/font/bootstrap-icons.css">
{{< / highlight >}}

![Bootstrap5: Nunjucks: Heads: Links][033-h-links]

#### Navigation Bar

* [gitlab.com/.../views/shared/033-navbar.njk][njk-s-033-navbar]

{{< highlight html >}}
  <nav class="navbar navbar-expand-sm navbar-dark
    fixed-top bg-dark">

    <div class="container-fluid">
      <a class="navbar-brand ms-4" href="#">
         <img src="assets/images/logo-gear.png"
           alt="Home" width="32" height="32"/>
      </a>

      {% include './navbar-button.njk' %}
      {% include './033-navbar-collapse.njk' %}
    </div>

  </nav>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Blocks: Navigation Bar][033-s-navbar]

#### Navbar: Collapsible

* [gitlab.com/.../views/shared/033-navbar-collapse.njk][njk-s-033-navbar-coll]

{{< highlight html >}}
<div class="collapse navbar-collapse"
  id="navbarCollapse">

  <ul class="navbar-nav me-auto">
    <li class="nav-item">
      <a href="#" class="nav-link active"
          aria-current="page">
        <svg class="bi pe-none me-2"
             width="16" height="16"
          ><use xlink:href="#house-door-fill"/></svg>
          Blog <span class="visually-hidden"
        >(current)</span>
      </a>
    </li>
    <li class="nav-item dropdown">
      {% include './034-navbar-dropdown.njk' %}
    </li>
    <li class="nav-item">
      <a href="#" class="nav-link active"
          aria-current="page">
        <svg class="bi pe-none me-2"
              width="16" height="16"
          ><use xlink:href="#person-fill"/></svg>
          About</a>
    </li>
  </ul>
  <form class="d-flex form-inline mt-2 mt-md-0
      d-sm-none d-md-flex">
    <input class="form-control mr-sm-2" type="text" 
      placeholder="Search"
      aria-label="Search" role="search">
    <button class="btn btn-outline-light my-2 my-sm-0" 
      type="submit">Search</button>
  </form>

</div>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Blocks: Navigation Bar][033-s-navbar-collapse]

#### Navbar: Dropdown

* [gitlab.com/.../views/shared/033-navbar-dropdown.njk][njk-s-033-navbar-drop]

{{< highlight html >}}
      <a  href="#"
          class="nav-link active dropdown-toggle"
          aria-current="page"
          data-bs-toggle="dropdown"
          aria-expanded="false">
        <svg class="bi pe-none me-2"
              width="16" height="16"
          ><use xlink:href="#archive-fill"/></svg>
          Archives</a>
      <ul class="dropdown-menu">
        <li><a href="#" class="dropdown-item">
          <svg class="bi pe-none me-2"
                width="16" height="16"
            ><use xlink:href="#tag-fill"/></svg>
            By Tags</a></li>
        <li><a href="#" class="dropdown-item">
          <svg class="bi pe-none me-2"
                width="16" height="16"
            ><use xlink:href="#folder-fill"/></svg>
            By Category</a></li>
        <li><hr class="dropdown-divider"></li>
        <li><a href="#" class="dropdown-item">
          <svg class="bi pe-none me-2"
                width="16" height="16"
            ><use xlink:href="#calendar2-day-fill"/></svg>
            By Chronology</a></li>
      </ul>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Blocks: Navigation Bar][033-s-navbar-dropdown]

#### Browser Preview

> Screenshot

![Bootstrap5: Qutebrowser: Navigation Bar: Bootstrap Icons][033-qb-navbar-ibs-sm]

#### Static Page

> HTML Document Target

* [gitlab.com/.../step-03/033-navbar-ibs-cdn.html][html-033-nav-ibs-cdn]

-- -- --

### 5: Navbar: Bootstrap Icons: SVG

This one is a little bit different.

#### Using Icon Set

Using Bootstrap Icon with inline SVG symbol is,
as simple as this below:

{{< highlight html >}}
<svg class="bi pe-none me-2" width="16" height="16">
  <use xlink:href="#house-door-fill"/></svg>
{{< / highlight >}}

For a real world example, you can examine this section below.

#### Assets

> CSS, JS, Images

{{< highlight bash >}}
assets
├── css
│   └── bootstrap.css
├── images
│   └── logo-gear.png
└── js
    ├── bootstrap.min.js
    └── popper.min.js
{{< / highlight >}}

#### Templates

> Nunjucks

{{< highlight bash >}}
views
├── 034-navbar-ibs-svg.njk
├── heads
│   ├── 030-meta.njk
│   └── 033-links.njk
├── layouts
│   └── base.njk
└── shared
    ├── 034-navbar-collapse.njk
    ├── 034-navbar-dropdown.njk
    ├── 034-navbar.njk
    ├── 034-symbols.njk
    └── navbar-button.njk
{{< / highlight >}}

#### Blocks

> Nunjucks Document Source

* [gitlab.com/.../views/034-navbar-ibs-svg.njk][njk--034-nav-ibs-svg]

{{< highlight jinja >}}
{% extends './layouts/base.njk' %}

{% block htmlhead %}
  {% include './heads/030-meta.njk' %}
  {% include './heads/033-links.njk' %}
  <style>
    .bi {
      vertical-align: -.125em;
      fill: currentColor;
    }
  </style>
{% endblock %}

{% block navbar %}
  {% include './shared/034-symbols.njk' %}
  {% include './shared/034-navbar.njk' %}
{% endblock %}
{{< / highlight >}}

![Bootstrap5: Nunjucks: Blocks: Navigation Bar: Bootstrap Icons: SVG][034-njk-navbar-ibs-svg]

#### HTML Head

> Assets: Resource Links

The same as previous.

#### SVG Symbol

* [gitlab.com/.../views/shared/034-symbols.njk][njk-s-034-symbols]

{{< highlight html >}}
  <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
    <symbol id="home" viewBox="0 0 16 16">
      <path d="M8.354 1.146a.5.5 0 0 0-.708 0l-6 6A.5.5 0 0 0 1.5 7.5v7a.5.5 0 0 0 .5.5h4.5a.5.5 0 0 0 .5-.5v-4h2v4a.5.5 0 0 0 .5.5H14a.5.5 0 0 0 .5-.5v-7a.5.5 0 0 0-.146-.354L13 5.793V2.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.293L8.354 1.146zM2.5 14V7.707l5.5-5.5 5.5 5.5V14H10v-4a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5v4H2.5z"/>
    </symbol>
    <symbol id="house-door-fill" viewBox="0 0 16 16">
      <path d="M6.5 14.5v-3.505c0-.245.25-.495.5-.495h2c.25 0 .5.25.5.5v3.5a.5.5 0 0 0 .5.5h4a.5.5 0 0 0 .5-.5v-7a.5.5 0 0 0-.146-.354L13 5.793V2.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.293L8.354 1.146a.5.5 0 0 0-.708 0l-6 6A.5.5 0 0 0 1.5 7.5v7a.5.5 0 0 0 .5.5h4a.5.5 0 0 0 .5-.5Z"/>
    </symbol>
    <symbol id="person-fill" viewBox="0 0 16 16">
      <path d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3Zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6Z"/>
    </symbol>
    <symbol id="archive-fill" viewBox="0 0 16 16">
      <path d="M12.643 15C13.979 15 15 13.845 15 12.5V5H1v7.5C1 13.845 2.021 15 3.357 15h9.286zM5.5 7h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1 0-1zM.8 1a.8.8 0 0 0-.8.8V3a.8.8 0 0 0 .8.8h14.4A.8.8 0 0 0 16 3V1.8a.8.8 0 0 0-.8-.8H.8z"/>
    </symbol>
    <symbol id="tag-fill" viewBox="0 0 16 16">
      <path d="M2 1a1 1 0 0 0-1 1v4.586a1 1 0 0 0 .293.707l7 7a1 1 0 0 0 1.414 0l4.586-4.586a1 1 0 0 0 0-1.414l-7-7A1 1 0 0 0 6.586 1H2zm4 3.5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z"/>
    </symbol>
    <symbol id="folder-fill" viewBox="0 0 16 16">
      <path d="M9.828 3h3.982a2 2 0 0 1 1.992 2.181l-.637 7A2 2 0 0 1 13.174 14H2.825a2 2 0 0 1-1.991-1.819l-.637-7a1.99 1.99 0 0 1 .342-1.31L.5 3a2 2 0 0 1 2-2h3.672a2 2 0 0 1 1.414.586l.828.828A2 2 0 0 0 9.828 3zm-8.322.12C1.72 3.042 1.95 3 2.19 3h5.396l-.707-.707A1 1 0 0 0 6.172 2H2.5a1 1 0 0 0-1 .981l.006.139z"/>
    </symbol>
    <symbol id="calendar2-day-fill" viewBox="0 0 16 16">
      <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zm9.954 3H2.545c-.3 0-.545.224-.545.5v1c0 .276.244.5.545.5h10.91c.3 0 .545-.224.545-.5v-1c0-.276-.244-.5-.546-.5zm-2.24 4.855a.428.428 0 1 0 0-.855.426.426 0 0 0-.429.43c0 .238.192.425.43.425zm.337.563h-.672v4.105h.672V8.418zm-6.867 4.105v-2.3h2.261v-.61H4.684V7.801h2.464v-.61H4v5.332h.684zm3.296 0h.676V9.98c0-.554.227-1.007.953-1.007.125 0 .258.004.329.015v-.613a1.806 1.806 0 0 0-.254-.02c-.582 0-.891.32-1.012.567h-.02v-.504H7.98v4.105z"/>
    </symbol>
    <symbol id="chevron-right" viewBox="0 0 16 16">
      <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"/>
    </symbol>
  </svg>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Blocks: SVG Symbols][034-s-symbols]

#### Navigation Bar

* [gitlab.com/.../views/shared/034-navbar.njk][njk-s-034-navbar]

{{< highlight html >}}
  <nav class="navbar navbar-expand-sm navbar-dark
    fixed-top bg-dark">

    <div class="container-fluid">
      <a class="navbar-brand ms-4" href="#">
         <img src="assets/images/logo-gear.png"
           alt="Home" width="32" height="32"/>
      </a>

      {% include './navbar-button.njk' %}
      {% include './034-navbar-collapse.njk' %}
    </div>

  </nav>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Blocks: Navigation Bar][034-s-navbar]

#### Navbar: Collapsible

* [gitlab.com/.../views/shared/034-navbar-collapse.njk][njk-s-034-navbar-coll]

{{< highlight html >}}
<div class="collapse navbar-collapse"
  id="navbarCollapse">

  <ul class="navbar-nav me-auto">
    <li class="nav-item">
      <a href="#" class="nav-link active"
          aria-current="page">
        <svg class="bi pe-none me-2"
             width="16" height="16"
          ><use xlink:href="#house-door-fill"/></svg>
          Blog <span class="visually-hidden"
        >(current)</span>
      </a>
    </li>
    <li class="nav-item dropdown">
      {% include './034-navbar-dropdown.njk' %}
    </li>
    <li class="nav-item">
      <a href="#" class="nav-link active"
          aria-current="page">
        <svg class="bi pe-none me-2"
              width="16" height="16"
          ><use xlink:href="#person-fill"/></svg>
          About</a>
    </li>
  </ul>
  <form class="d-flex form-inline mt-2 mt-md-0
      d-sm-none d-md-flex">
    <input class="form-control mr-sm-2" type="text" 
      placeholder="Search"
      aria-label="Search" role="search">
    <button class="btn btn-outline-light my-2 my-sm-0" 
      type="submit">Search</button>
  </form>

</div>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Blocks: Navigation Bar][034-s-navbar-collapse]

#### Navbar: Dropdown

* [gitlab.com/.../views/shared/034-navbar-dropdown.njk][njk-s-034-navbar-drop]

{{< highlight html >}}
      <a  href="#"
          class="nav-link active dropdown-toggle"
          aria-current="page"
          data-bs-toggle="dropdown"
          aria-expanded="false">
        <svg class="bi pe-none me-2"
              width="16" height="16"
          ><use xlink:href="#archive-fill"/></svg>
          Archives</a>
      <ul class="dropdown-menu">
        <li><a href="#" class="dropdown-item">
          <svg class="bi pe-none me-2"
                width="16" height="16"
            ><use xlink:href="#tag-fill"/></svg>
            By Tags</a></li>
        <li><a href="#" class="dropdown-item">
          <svg class="bi pe-none me-2"
                width="16" height="16"
            ><use xlink:href="#folder-fill"/></svg>
            By Category</a></li>
        <li><hr class="dropdown-divider"></li>
        <li><a href="#" class="dropdown-item">
          <svg class="bi pe-none me-2"
                width="16" height="16"
            ><use xlink:href="#calendar2-day-fill"/></svg>
            By Chronology</a></li>
      </ul>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Blocks: Navigation Bar][034-s-navbar-dropdown]

#### Browser Preview

> Screenshot

![Bootstrap5: Qutebrowser: Navigation Bar: Bootstrap Icons: SVG][034-qb-navbar-ibs-sm]

#### Static Page

> HTML Document Target

* [gitlab.com/.../step-03/034-navbar-ibs-svg.html][html-034-nav-ibs-svg]

-- -- --

### What is Next 🤔?

I think that is all for navigation bar.
There are some more to explore in nav bar,
but for example I think this is enough.
We need to move on to other tutorial as well.

We are done with Navbar,
now it is time to learn sidebar for use with dashboard.

Consider continue reading [ [Bootstrap5 - Sidebar][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2021/06/18/bootstrap5-sidebar/
[local-bootstrap-v4]:   {{< baseurl >}}/frontend/2020/03/05/bootstrap-navbar-icons/

[tutor-step-03]:        {{< tutor-html-bs5 >}}/step-03/

[//]: <> ( -- -- -- links below -- -- -- )

[030-exa-htmls]:        {{< baseurl >}}assets/posts/frontend/2021/06/030-exa-htmls.png
[030-h-meta]:           {{< baseurl >}}assets/posts/frontend/2021/06/030-h-meta.png
[030-l-base]:           {{< baseurl >}}assets/posts/frontend/2021/06/030-l-base.png
[030-s-navbar-button]:  {{< baseurl >}}assets/posts/frontend/2021/06/030-s-navbar-button.png

[032-h-links]:          {{< baseurl >}}assets/posts/frontend/2021/06/032-h-links.png
[033-h-links]:          {{< baseurl >}}assets/posts/frontend/2021/06/033-h-links.png

[034-s-symbols]:        {{< baseurl >}}assets/posts/frontend/2021/06/034-s-symbols.png

[032-s-navbar]:         {{< baseurl >}}assets/posts/frontend/2021/06/032-s-navbar.png
[032-s-navbar-collapse]:{{< baseurl >}}assets/posts/frontend/2021/06/032-s-navbar-collapse.png
[032-s-navbar-dropdown]:{{< baseurl >}}assets/posts/frontend/2021/06/032-s-navbar-dropdown.png
[033-s-navbar]:         {{< baseurl >}}assets/posts/frontend/2021/06/033-s-navbar.png
[033-s-navbar-collapse]:{{< baseurl >}}assets/posts/frontend/2021/06/033-s-navbar-collapse.png
[033-s-navbar-dropdown]:{{< baseurl >}}assets/posts/frontend/2021/06/033-s-navbar-dropdown.png
[034-s-navbar]:         {{< baseurl >}}assets/posts/frontend/2021/06/034-s-navbar.png
[034-s-navbar-collapse]:{{< baseurl >}}assets/posts/frontend/2021/06/034-s-navbar-collapse.png
[034-s-navbar-dropdown]:{{< baseurl >}}assets/posts/frontend/2021/06/034-s-navbar-dropdown.png

[032-njk-navbar-ibs-css]:   {{< baseurl >}}assets/posts/frontend/2021/06/032-njk-navbar-ibs-css.png
[033-njk-navbar-ibs-cdn]:   {{< baseurl >}}assets/posts/frontend/2021/06/033-njk-navbar-ibs-cdn.png
[034-njk-navbar-ibs-svg]:   {{< baseurl >}}assets/posts/frontend/2021/06/034-njk-navbar-ibs-svg.png

[032-qb-navbar-ibs-sm]:     {{< baseurl >}}assets/posts/frontend/2021/06/032-qb-navbar-ibs-sm.png
[032-qb-navbar-ibs-xs]:     {{< baseurl >}}assets/posts/frontend/2021/06/032-qb-navbar-ibs-xs.png
[033-qb-navbar-ibs-sm]:     {{< baseurl >}}assets/posts/frontend/2021/06/033-qb-navbar-ibs-sm.png
[034-qb-navbar-ibs-sm]:     {{< baseurl >}}assets/posts/frontend/2021/06/034-qb-navbar-ibs-sm.png

[//]: <> ( -- -- -- links below -- -- -- )

[css-icons-bootstrap]:  {{< tutor-html-bs5 >}}/step-03/assets/css/icons-bootstrap.css
[dir-icons-bootstrap]:  {{< tutor-html-bs5 >}}/step-03/assets/icons-bootstrap

[030-l-base]:           {{< tutor-html-bs5 >}}/step-03/views/layouts/base.njk
[030-h-meta]:           {{< tutor-html-bs5 >}}/step-03/views/heads/030-meta.njk
[030-s-navbar-button]:  {{< tutor-html-bs5 >}}/step-03/views/shared/navbar-button.njk

[032-h-links]:          {{< tutor-html-bs5 >}}/step-03/views/heads/032-links.njk
[033-h-links]:          {{< tutor-html-bs5 >}}/step-03/views/heads/033-links.njk

[njk-s-032-navbar]:     {{< tutor-html-bs5 >}}/step-03/views/shared/032-navbar.njk
[njk-s-032-navbar-coll]:{{< tutor-html-bs5 >}}/step-03/views/shared/032-navbar-collapse.njk
[njk-s-032-navbar-drop]:{{< tutor-html-bs5 >}}/step-03/views/shared/032-navbar-dropdown.njk
[njk-s-033-navbar]:     {{< tutor-html-bs5 >}}/step-03/views/shared/033-navbar.njk
[njk-s-033-navbar-coll]:{{< tutor-html-bs5 >}}/step-03/views/shared/033-navbar-collapse.njk
[njk-s-033-navbar-drop]:{{< tutor-html-bs5 >}}/step-03/views/shared/033-navbar-dropdown.njk
[njk-s-034-navbar]:     {{< tutor-html-bs5 >}}/step-03/views/shared/034-navbar.njk
[njk-s-034-navbar-coll]:{{< tutor-html-bs5 >}}/step-03/views/shared/034-navbar-collapse.njk
[njk-s-034-navbar-drop]:{{< tutor-html-bs5 >}}/step-03/views/shared/034-navbar-dropdown.njk
[njk-s-034-symbols]:    {{< tutor-html-bs5 >}}/step-03/views/shared/034-symbols.njk

[njk--032-nav-ibs-css]: {{< tutor-html-bs5 >}}/step-03/views/032-navbar-ibs-css.njk
[njk--033-nav-ibs-cdn]: {{< tutor-html-bs5 >}}/step-03/views/033-navbar-ibs-cdn.njk
[njk--034-nav-ibs-svg]: {{< tutor-html-bs5 >}}/step-03/views/034-navbar-ibs-svg.njk

[html-032-nav-ibs-css]: {{< tutor-html-bs5 >}}/step-03/032-navbar-ibs-css.html
[html-033-nav-ibs-cdn]: {{< tutor-html-bs5 >}}/step-03/033-navbar-ibs-cdn.html
[html-034-nav-ibs-svg]: {{< tutor-html-bs5 >}}/step-03/034-navbar-ibs-svg.html
