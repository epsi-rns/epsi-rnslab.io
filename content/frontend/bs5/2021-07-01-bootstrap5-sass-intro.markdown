---
type   : post
title  : "Bootstrap - Sass - Introduction"
date   : 2021-07-01T09:17:35+07:00
slug   : bootstrap5-sass-intro
categories: [frontend]
tags      : [bootstrap, sass]
keywords  : [custom sass, altering bootstrap]
author : epsi
opengraph:
  image: assets/posts/frontend/2021/06/001-preview-04.png

toc    : "toc-2021-06-html-bootstrap5-step"

excerpt:
  Make custom theme, step by step.
  Using Bootstrap SASS for the first time

---

### Preface

> Step Four: Using Bootstrap SASS for the first time

Understanding SASS is a must for theme maker.
You should be able to alter bootstrap variables.

{{< embed-video width="480" height="480"
    src="assets/posts/frontend/2021/07/040-bootstrap5-sass.mp4" >}}

#### Reading

If you do not have any idea about SASS you should read this first:

* <https://sass-lang.com/>

-- -- --

### 1: Overview

First thing first.

#### Bootstrap v4

The obsolete Bootstrap v4 article series is also available.

* [Bootstrap - Sass - Introduction][local-bootstrap-v4]

#### Directory Preparation

Add SASS to your current working directory.

* [gitlab.com/.../sass/][dir-sass]

{{< highlight bash >}}
❯ tree -d -L 1 assets sass
assets
├── css
├── images
└── js
sass
├── bootstrap5
└── css

7 directories
{{< / highlight >}}

![Bootstrap SASS: Directory Preparation][040-sass-tree-01]

We are going to need them for the rest of this chapter.

#### Download Bootstrap SASS

Add Bootstrap to your vendors directory.

* [gitlab.com/.../sass/bootstrap5][dir-bootstrap5]

{{< highlight bash >}}
❯ tree -d sass/bootstrap5
sass/bootstrap5
├── forms
├── helpers
├── mixins
├── utilities
└── vendor

6 directories
{{< / highlight >}}

Now you should see inside Bootstrap.

![Bootstrap SASS: Inside Bootstrap][040-sass-tree-02]

#### Custom SASS

Now, it is about the right time to create custom SASS.
Let's say we create this

* [gitlab.com/.../sass/css/bootstrap.scss][scss-bootstrap]

{{< highlight scss >}}
@import 
  "../bootstrap/functions",
  "variables",
  "../bootstrap/bootstrap"
;
{{< / highlight >}}

The import partials is from `sass/css`.
We want to compile it to `assets/css`.

![Bootstrap SASS: SCSS: Bootstrap][040-sass-bootstrap]

#### SASS Tools

You may compile with any SASS compiler that you need.

* [SASS Compiler][local-sass-compiler]

I user `artix` linux, so I use `pacman` to install `dart-sass`.

{{< highlight bash >}}
❯ sudo pacman -S dart-sass
resolving dependencies...
looking for conflicting packages...

Packages (1) dart-sass-1.62.0-1

Total Installed Size:  8.99 MiB

:: Proceed with installation? [Y/n] y
(1/1) checking keys in keyring                    
(1/1) checking package integrity                  
(1/1) loading package files                       
(1/1) checking for file conflicts                 
(1/1) checking available disk space               
:: Processing package changes...
(1/1) installing dart-sass 
{{< / highlight >}}

![Bootstrap SASS: Install Dart CSS][040-sass-pacman]

Windows user can use `chocolatey` to install `sass`.

#### Compile SASS

Consider compile to `css` directory.

{{< highlight bash >}}
❯ cd step-04
❯ sass --watch -I sass sass/css:assets/css
[2023-04-20 17:20:59] Compiled sass/css/bootstrap.scss to assets/css/bootstrap.css.
Sass is watching for changes. Press Ctrl-C to stop.
{{< / highlight >}}

![Bootstrap SASS: Compile][040-sass-watch]

Now you should see generated CSS, along with the CSS mapping.
The CSS mapping is useful for debugging while inspecting in browser.
You can exminae the uncompressed bootstrap CSS here:

* [gitlab.com/.../assets/css/bootstrap.css][css-bootstrap]

{{< highlight bash >}}
❯ tree assets/css
assets/css
├── bootstrap.css
└── bootstrap.css.map

1 directory, 2 files
{{< / highlight >}}

![Bootstrap SASS: Generated CSS][040-sass-assets]

This should be the same as regular official Bootstrap.

-- -- --

### 2: Example: Altering Bootstrap

This is the basic bootstrap customization.

#### The Bootstrap Sylesheet

Altering standard Bootstrap is simple.
Consider change the behaviour by setting initial variable.

* [gitlab.com/.../sass/css/_variables.scss][scss-variables].
  
{{< highlight scss >}}
// Variables

$pink:  #e83e8c !default;
$primary:  darken($pink, 10%) !default;
{{< / highlight >}}

![Bootstrap SASS: SCSS: Bootstrap][040-sass-variables]

#### The Custom Sylesheet

Also add custom stylesheet.
 
{{< highlight scss >}}
.layout-base {
  padding-top: 5rem;
  padding-bottom: 1rem;
}
{{< / highlight >}}

![Bootstrap SASS: SCSS: Bootstrap][040-sass-main]

#### Browser Preview

> Screenshot

You can open the file, and check the page in browser.
This should be pink now.

![Bootstrap SASS: Example: Pink Navigation Bar][040-qb-pink-base]

The result is not pretty, we are going to fix this.

-- -- --

### 3: Example Template

Wait, what HTML file do I use in browser?
Where's that document comes from?

#### Source Code: Step-04

Source code for this chapter can be obtained here:

* [gitlab.com/.../step-04/][tutor-step-04]

#### Templates

> Nunjucks

We have two reusable templates,
the `navbar-button` and `meta` head.

{{< highlight bash >}}
❯ tree -C views
views
├── 041-navbar-minimal.njk
├── contents
│   └── 041-main.njk
├── heads
│   ├── 041-links.njk
│   └── 041-meta.njk
├── layouts
│   └── base.njk
└── shared
    ├── 041-footer.njk
    └── 041-navbar.njk

5 directories, 7 files
{{< / highlight >}}

![Bootstrap5: Directory Structure: Templates][040-tree-views]

#### Layout and Blocks

> Reusable Parent Template

We have parent, and child.

* [gitlab.com/.../views/layouts/base.njk][njk-041-l-base]

{{< highlight jinja >}}
<!DOCTYPE html>
<html>

<head>
  {% block htmlhead %}{% endblock %}
</head>

<body>
  {% block header   %}{% endblock %}
  {% block content  %}{% endblock %}
  {% block footer   %}{% endblock %}
</body>

</html>
{{< / highlight >}}

> Nunjucks Document Source

* [gitlab.com/.../views/041-navbar-minimal.njk][njk--041-navbar-mini]

{{< highlight jinja >}}
{% extends './layouts/base.njk' %}

{% block htmlhead %}
  {% include './heads/041-meta.njk' %}
  {% include './heads/041-links.njk' %}
{% endblock %}

{% block header %}
  {% include './shared/041-navbar.njk' %}
{% endblock %}

{% block content %}
  {% include './contents/041-main.njk' %}
{% endblock %}

{% block footer %}
  {% include './shared/041-footer.njk' %}
{% endblock %}
{{< / highlight >}}

![Bootstrap5: Nunjucks: Base Layout: Parent Template][040-l-base-root]

#### HTML Head: Reusable Meta

We do not need fancy meta yet.
All we need is just title.

* [gitlab.com/.../views/heads/041-meta.njk][njk-041-h-meta]

{{< highlight html >}}
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,
    initial-scale=1, shrink-to-fit=no">
  <title>Desperate times, desperate measures.</title>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Heads: Meta][041-h-head-meta]

#### Reusable Navigation Bar and Footer

For this chapter, we only need simple navbar and footer.
Let's make it simple generic.

> Navigation Bar

* [gitlab.com/.../views/shared/041-navbar.njk][njk-041-s-navbar]

{{< highlight html >}}
  <!-- header -->
  <nav class="navbar navbar-expand-md navbar-dark
      fixed-top bg-primary">
    <div class="navbar-collapse">
      <ul class="navbar-nav mx-2">
        <li class="nav-item">
          <a class="nav-link active"
             href="#">Home <span class="visually-hidden"
            >(current)</span></a>
        </li>
      </ul>
    </div>
  </nav>
{{< / highlight >}}

> Footer

* [gitlab.com/.../views/shared/041-footer.njk][njk-041-s-footer]

{{< highlight html >}}

  <!-- footer -->
  <footer class="footer">
    <div class="bg-primary text-light text-center">
      &copy; 2021.
    </div>
  </footer>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Reusable Navigation Bar and Footer][041-s-header-footer]

#### Specific Content

This is our specific content, for our target HTML page.

> HTML Head: Assets

Beside the common `bootstrap.css`,
we need this additional `main.css` compiled by sass,
to arrange the layout using vertical margin.

* [gitlab.com/.../views/heads/041-links.njk][njk-041-h-links]

{{< highlight html >}}
  <link rel="stylesheet" type="text/css" 
    href="assets/css/bootstrap.css">
  <link rel="stylesheet" type="text/css"
    href="assets/css/main.css">
{{< / highlight >}}

> Main Content

Add here is the content,
to test how the page work.

* [gitlab.com/.../views/contents/041-main.njk][njk-041-c-main]

{{< highlight html >}}
  <!-- main -->
  <div class="layout-base">
    <main role="main" class="container">
      <ul class="list-group">
        <li class="list-group-item">
        To have, to hold, to love,
        cherish, honor, and protect?</li>

        <li class="list-group-item">
        To shield from terrors known and unknown?
        To lie, to deceive?</li>

        <li class="list-group-item">
        To live a double life,
        to fail to prevent her abduction,
        erase her identity, 
        force her into hiding,
        take away all she has known.</li>
      </ul>
    </main>
  </div>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Contents: Links and Main][041-c-links-main]

#### Static Page

> HTML Document Target

You can check the compiled result here:

* [gitlab.com/.../step-04/041-navbar-minimal.html][html-041-navbar-mini]

-- -- --

### 4: Example Custom SASS

> Modular Stylesheet

The idea is, to make the stylesheet modular.

#### Directory Structure

Consider add some stylesheet for our beloved HTML pages.

{{< highlight bash >}}
❯ tree -C sass/css
sass/css
├── bootstrap.scss
├── main
│   ├── _decoration.scss
│   ├── _layout-page.scss
│   └── _sticky-footer.scss
├── main.scss
└── _variables.scss
{{< / highlight >}}

![Bootstrap SASS: Pane: Custom SASS Summary][041-pane-sass-custom]

#### Custom SASS

For each file, it is still simple.
As the site grow, it is going to get more complex.

* [gitlab.com/.../step-04/sass/css/main.scss][scss-main]

{{< highlight scss >}}
// Import partials from `sass_dir` (defaults to `sass/css`)
@import 
  "main/layout-page",
  "main/decoration",
  "main/sticky-footer"
;
{{< / highlight >}}

* [gitlab.com/.../sass/css/main/_layout-page.scss][scss-layout-page]

{{< highlight scss >}}
.layout-base {
  padding-top: 5rem;
  padding-bottom: 1rem;
}
{{< / highlight >}}

* [gitlab.com/.../sass/css/main/_decoration.scss][scss-decoration]

{{< highlight scss >}}
body {
  background-image: url("../images/light-grey-terrazzo.png");
}
{{< / highlight >}}

* [gitlab.com/.../sass/css/main/_sticky-footer.scss][scss-sticky-footer]

{{< highlight scss >}}
/* Sticky footer styles
-------------------------------------------------- */
html {
  position: relative;
  min-height: 100%;
}
body {
  margin-bottom: 60px; /* Margin bottom by footer height */
}
.footer {
  position: absolute;
  bottom: 0;
  width: 100%;
  height: 60px; /* Set the fixed height of the footer here */
  line-height: 60px; /* Vertically center the text there */
}
{{< / highlight >}}

#### Browser Preview

> Screenshot

Consider have a look again at the page after the tweaks.

![Bootstrap SASS: Example: Pink Navigation Bar][041-qb-pink-base]

Now it looks better.

-- -- --

### What is Next 🤔?

We need to create our own custom theme
But before we do that, consider have a look at,
a ready to use theme.

Consider continue reading [ [Bootstrap - Sass - Bootswatch][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2021/07/05/bootstrap5-sass-bootswatch/

[local-bootstrap-v4]:   {{< baseurl >}}frontend/2020/03/07/bootstrap-sass-intro/
[local-sass-compiler]:  {{< baseurl >}}frontend/2019/01/13/sass-common-compiler/

[tutor-step-04]:        {{< tutor-html-bs5 >}}/step-04/

[//]: <> ( -- -- -- links below -- -- -- )

[040-sass-assets]:      {{< baseurl >}}assets/posts/frontend/2021/07/040-sass-assets.png
[040-sass-bootstrap]:   {{< baseurl >}}assets/posts/frontend/2021/07/040-sass-bootstrap.png
[040-sass-pacman]:      {{< baseurl >}}assets/posts/frontend/2021/07/040-sass-pacman.png
[040-sass-tree-01]:     {{< baseurl >}}assets/posts/frontend/2021/07/040-sass-tree-01.png
[040-sass-tree-02]:     {{< baseurl >}}assets/posts/frontend/2021/07/040-sass-tree-02.png
[040-sass-watch]:       {{< baseurl >}}assets/posts/frontend/2021/07/040-sass-watch.png
[040-sass-variables]:   {{< baseurl >}}assets/posts/frontend/2021/07/040-sass-variables.png
[040-sass-main]:        {{< baseurl >}}assets/posts/frontend/2021/07/040-sass-main.png

[040-qb-pink-base]:     {{< baseurl >}}assets/posts/frontend/2021/07/040-qb-pink-base.png
[041-qb-pink-base]:     {{< baseurl >}}assets/posts/frontend/2021/07/041-qb-pink-base.png


[040-l-base-root]:      {{< baseurl >}}assets/posts/frontend/2021/07/040-l-base-root.png
[040-tree-views]:       {{< baseurl >}}assets/posts/frontend/2021/07/040-tree-views.png

[041-h-head-meta]:      {{< baseurl >}}assets/posts/frontend/2021/07/041-h-head-meta.png
[041-s-header-footer]:  {{< baseurl >}}assets/posts/frontend/2021/07/041-s-header-footer.png
[041-c-links-main]:     {{< baseurl >}}assets/posts/frontend/2021/07/041-c-links-main.png

[041-pane-sass-custom]: {{< baseurl >}}assets/posts/frontend/2021/07/041-pane-sass-custom.png

[//]: <> ( -- -- -- links below -- -- -- )

[css-bootstrap]:        {{< tutor-html-bs5 >}}/step-04/assets/css/bootstrap.css
[dir-sass]:             {{< tutor-html-bs5 >}}/step-04/sass/
[dir-bootstrap5]:       {{< tutor-html-bs5 >}}/step-04/sass/bootstrap5
[dir-custom]:           {{< tutor-html-bs5 >}}/step-04/sass/css
[scss-bootstrap]:       {{< tutor-html-bs5 >}}/step-04/sass/css/bootstrap.scss
[scss-variables]:       {{< tutor-html-bs5 >}}/step-04/sass/css/_variables.scss

[scss-decoration]:      {{< tutor-html-bs5 >}}/step-04/sass/css/main/_decoration.scss
[scss-layout-page]:     {{< tutor-html-bs5 >}}/step-04/sass/css/main/_layout-page.scss
[scss-sticky-footer]:   {{< tutor-html-bs5 >}}/step-04/sass/css/main/_sticky-footer.scss
[scss-main]:            {{< tutor-html-bs5 >}}/step-04/sass/css/main.scss

[njk-041-c-main]:       {{< tutor-html-bs5 >}}/step-04/views/contents/041-main.njk
[njk-041-h-links]:      {{< tutor-html-bs5 >}}/step-04/views/heads/041-links.njk
[njk-041-h-meta]:       {{< tutor-html-bs5 >}}/step-04/views/heads/041-meta.njk
[njk-041-l-base]:       {{< tutor-html-bs5 >}}/step-04/views/layouts/base.njk
[njk-041-s-footer]:     {{< tutor-html-bs5 >}}/step-04/views/shared/041-footer.njk
[njk-041-s-navbar]:     {{< tutor-html-bs5 >}}/step-04/views/shared/041-navbar.njk
[njk--041-navbar-mini]: {{< tutor-html-bs5 >}}/step-04/views/041-navbar-minimal.njk
[html-041-navbar-mini]: {{< tutor-html-bs5 >}}/step-04/041-navbar-minimal.html
