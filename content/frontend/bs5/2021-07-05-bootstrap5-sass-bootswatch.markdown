---
type   : post
title  : "Bootstrap - Sass - Bootswatch"
date   : 2021-07-05T09:17:35+07:00
slug   : bootstrap5-sass-bootswatch
categories: [frontend]
tags      : [bootstrap, sass]
keywords  : [modular sass, swatch, custom sass]
author : epsi
opengraph:
  image: assets/posts/frontend/2021/07/bootswatch5/cerulean.png

toc    : "toc-2021-06-html-bootstrap5-step"

excerpt:
  More about custom theme, step by step.
  Using a ready to used Bootstrap SASS theme.

---

### Preface

> Step Four: Using a ready to used Bootstrap SASS theme.

Working with theme is fun.

#### Bootstrap v4

The obsolete Bootstrap v4 article series is also available.

* [Bootstrap - Sass - Bootswatch][local-bootstrap-v4]

#### Official Site

There is a ready to used Bootstrap theme named Bootstrap Swatch.

* [bootswatch.com](https://bootswatch.com/)

Or the source code, if you prefer:

* [github.com/thomaspark/bootswatch](https://github.com/thomaspark/bootswatch)

Bootswatch 5 provide 25 ready to be used theme for bootstrap5
while Bootswatch 4 only provide 21 theme for bootstrap4.

![Bootstrap SASS: Bootswatch: Folder][042-folder-caja]

#### Preview in Browser

Generating bootswatch theme with SASS is easy.
We can also combine with our own custom CSS.

Then I can just make screehost of all themes.

![Bootstrap SASS: Bootswatch: PReview][042-preview-caja]

And compile it as GIF animation with GIMP.

![Bootstrap SASS: Bootswatch Animation][042-swatch-animation]

So you can have the idea about the theme with just one this image.

-- -- --

### 1: HTML Preparation

This article is integral part of previous article.
You should read previous article before continue.

#### Templates

> Nunjucks

We can use previous templates
with a few exceptions.

{{< highlight bash >}}
❯ tree -C views
views
├── 042-bootswatch.njk
├── contents
│   └── 042-main.njk
├── heads
│   ├── 041-meta.njk
│   └── 042-links.njk
├── layouts
│   └── base.njk
└── shared
    ├── 041-footer.njk
    └── 041-navbar.njk
{{< / highlight >}}

So the only things that different is the head links,
and main content.

> Nunjucks Document Source

So the code should be shown as below:

* [gitlab.com/.../views/042-bootswatch.njk][njk--042-bootswatch]

{{< highlight jinja >}}
{% extends './layouts/base.njk' %}

{% block htmlhead %}
  {% include './heads/041-meta.njk' %}
  {% include './heads/042-links.njk' %}
{% endblock %}

{% block header %}
  {% include './shared/041-navbar.njk' %}
{% endblock %}

{% block content %}
  {% include './contents/042-main.njk' %}
{% endblock %}

{% block footer %}
  {% include './shared/041-footer.njk' %}
{% endblock %}
{{< / highlight >}}

#### Specific Content

This is our specific content, for our target HTML page.

> HTML Head: Assets

We are going to replace the `bootstrap.css`,
with `bootswatch5` compiled by sass.

* [gitlab.com/.../views/heads/042-links.njk][njk-042-h-links]

{{< highlight html >}}

  <link rel="stylesheet" type="text/css" 
    href="assets/css/bootswatch5.css">
  <link rel="stylesheet" type="text/css"
    href="assets/css/main.css">
{{< / highlight >}}

> Main Content

Add here is the content,
to test how the page work.

* [gitlab.com/.../views/contents/042-main.njk][njk-042-c-main]

{{< highlight html >}}
  <!-- main -->
  <div class="layout-base">
    <main role="main" class="container">
      <ul class="list-group">
        <li class="list-group-item active">
        <h4>Your Mission!</h4></li>

        <li class="list-group-item">
        To have, to hold, to love,
        cherish, honor, and protect?</li>

        <li class="list-group-item">
        To shield from terrors known and unknown?
        To lie, to deceive?</li>

        <li class="list-group-item">
        To live a double life,
        to fail to prevent her abduction,
        erase her identity, 
        force her into hiding,
        take away all she has known.</li>
      </ul>
    </main>
  </div>
{{< / highlight >}}

#### Static Page

> HTML Document Target

You can check the compiled result here:

* [gitlab.com/.../step-04/042-bootswatch.html][html-042-bootswatch]

![Bootstrap SASS: Bootswatch Compiled HTML][042-html-bootswatch5]

Now that the HTML document is ready,
we need to build the stylesheet using SASS.

-- -- --

### 2: Bootstrap Swatch

By altering bootstrap variable,
we can change the looks of bootstrap.
And we can also change further with SASS,
as shown in bootswatch example.

Consider examine at one example theme.
The Cerulean theme.

#### Directory

We need only two files for each theme.

{{< highlight bash >}}
❯ tree -C sass/bootswatch5
sass/bootswatch5
├── cerulean
│   ├── _bootswatch.scss
│   └── _variables.scss
├── cosmo
│   ├── _bootswatch.scss
│   └── _variables.scss
├── cyborg
│   ├── _bootswatch.scss
│   └── _variables.scss
├── darkly
│   ├── _bootswatch.scss
│   └── _variables.scss
{{< / highlight >}}

![Bootstrap SASS: Bootswatch Tree][042-tree-bootswatch]

#### The Sylesheet

> Original Code

Consider have a look at the original code:

* [gitlab.com/.../sass/bootswatch5/cerulean/_bootswatch.scss][scss-bootswatch]

By altering bootstrap variable,
we can change the looks of bootstrap.

{{< highlight scss >}}
// Cerulean 5.2.3
// Bootswatch

$theme: "cerulean" !default;

//
// Color system
//

$white:    #fff !default;
$gray-100: #f8f9fa !default;
$gray-200: #e9ecef !default;
$gray-300: #dee2e6 !default;
$gray-400: #ced4da !default;
$gray-500: #adb5bd !default;
$gray-600: #868e96 !default;
$gray-700: #495057 !default;
$gray-800: #343a40 !default;
$gray-900: #212529 !default;
$black:    #000 !default;

...
{{< / highlight >}}

![Bootstrap SASS: SCSS: Variables][042-scss-variables]

Along with this code, we can go further.

* [gitlab.com/.../sass/bootswatch5/cerulean/_variables.scss][scss-variables]

{{< highlight scss >}}
// Cerulean 5.2.3
// Bootswatch


// Variables

$text-shadow: 0 1px 0 rgba(0, 0, 0, .05) !default;

:root {
  color-scheme: light;
}

...
{{< / highlight >}}

![Bootstrap SASS: Bootswatch][042-scss-bootswatch]

Ypu can examine the altered variable by yourself.

#### The Sylesheet: Using Bootswatch

Now for use with our project, adapt with our directory structure.

* [gitlab.com/.../sass/css/bootswatch5.scss][scss-bootswatch5]

{{< highlight scss >}}
@import
    "../bootswatch5/cerulean/variables",
    "../bootstrap/bootstrap",
    "../bootswatch5/cerulean/bootswatch"
;
{{< / highlight >}}

Consider compile

{{< highlight bash >}}
$ dart-sass --watch -I sass sass/css:assets/css --style=compressed --no-source-map
Sass is watching for changes. Press Ctrl-C to stop.

Compiled sass/css/bootswatch.scss to assets/css/bootswatch.css.
{{< / highlight >}}

![Bootstrap SASS: SCSS: Bootswatch5][042-scss-bootswatch5]

#### Prepare The Head

Consider the `<head>` elemento of our last example,
instead of using the `bootstrap.css`,
change it to `bootswatch.css` theme.

{{< highlight html >}}
<head>
  ...

  <link rel="stylesheet" type="text/css" href="assets/css/bootswatch5.css">
  <link rel="stylesheet" type="text/css" href="assets/css/main.css">
</head>
{{< / highlight >}}

#### The Document: Example

The looks of the theme in browser for `cerulean` theme,
would looks like below.

![Bootstrap SASS: Bootswatch Theme][042-qb-cerulean]

-- -- --

### What is Next 🤔?

We need to create our own custom theme with custo page layout.
In order to do this, we need to make our theme is responsive.
So that later we can free playing, added decoration and so on.

Consider continue reading [ [Bootstrap - Sass - Responsive][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2021/07/07/bootstrap5-sass-responsive/

[local-bootstrap-v4]:   {{< baseurl >}}frontend/2020/03/09/bootstrap-sass-bootswatch/

[tutor-step-04]:        {{< tutor-html-bs5 >}}/step-04/

[//]: <> ( -- -- -- links below -- -- -- )

[042-swatch-animation]: {{< baseurl >}}assets/posts/frontend/2021/07/042-swatch-all.gif

[042-folder-caja]:      {{< baseurl >}}assets/posts/frontend/2021/07/042-folder-caja.png
[042-preview-caja]:     {{< baseurl >}}assets/posts/frontend/2021/07/042-preview-caja.png
[042-tree-bootswatch]:  {{< baseurl >}}assets/posts/frontend/2021/07/042-tree-bootswatch.png

[042-scss-bootswatch]:  {{< baseurl >}}assets/posts/frontend/2021/07/042-scss-bootswatch.png
[042-scss-variables]:   {{< baseurl >}}assets/posts/frontend/2021/07/042-scss-variables.png
[042-scss-bootswatch5]: {{< baseurl >}}assets/posts/frontend/2021/07/042-scss-bootswatch5.png

[042-html-bootswatch5]: {{< baseurl >}}assets/posts/frontend/2021/07/042-html-compiled.png

[042-qb-cerulean]:      {{< baseurl >}}assets/posts/frontend/2021/07/bootswatch5/cerulean.png

[//]: <> ( -- -- -- links below -- -- -- )

[scss-bootswatch]:      {{< tutor-html-bs5 >}}/step-04/sass/bootswatch5/cerulean/_bootswatch.scss
[scss-variables]:       {{< tutor-html-bs5 >}}/step-04/sass/bootswatch5/cerulean/_variables.scss
[scss-bootswatch5]:     {{< tutor-html-bs5 >}}/step-04/sass/css/bootswatch5.scss

[njk-042-c-main]:       {{< tutor-html-bs5 >}}/step-04/views/contents/042-main.njk
[njk-042-h-links]:      {{< tutor-html-bs5 >}}/step-04/views/heads/042-links.njk
[njk--042-bootswatch]:  {{< tutor-html-bs5 >}}/step-04/views/042-bootswatch.njk
[html-042-bootswatch]:  {{< tutor-html-bs5 >}}/step-04/042-bootswatch.html
