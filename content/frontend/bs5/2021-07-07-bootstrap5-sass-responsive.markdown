---
type   : post
title  : "Bootstrap - Sass - Responsive"
date   : 2021-07-07T09:17:35+07:00
slug   : bootstrap5-sass-responsive
categories: [frontend]
tags      : [bootstrap, sass]
keywords  : [layout, responsive spacing, gap, maxwidth]
author : epsi
opengraph:
  image: assets/posts/frontend/2021/06/001-preview-04.png

toc    : "toc-2021-06-html-bootstrap5-step"

excerpt:
  More about custom theme, step by step.
  Reponsive Layout using Bootstrap.

---

### Preface

> Step Four: Reponsive Layout using Bootstrap.

From template, to SASS,
now we need to focus on the HTML itself,
especially for bootstrap default class.

-- -- --

### 1: Overview

#### General Preview

This below is general preview of,
what responsive page that we want to achieve.
Just keep in mind that,
responsive design is based on consideration.
So different website might have different preview.

![Bootstrap OC: General Preview of Responsive Page Design][image-png-layout-page]

Source image is available in inkscape SVG format,
so you can modify, and make your own preview quickly.

* [Responsive Page: Image Source][image-svg-layout-page]

#### Bootstrap v4

The obsolete Bootstrap v4 article series is also available.

* [Bootstrap - Sass - Responsive][local-bootstrap-v4]

#### Source Code: Step-04

The source code for this chapter is,
available for download at the following link:

* [gitlab.com/.../step-04/][tutor-step-04]

#### SASS Directory Structure

For each step as the site grow,
we are going to alter the SASS, one by one.
After its complete we are going to have this structure. 

{{< highlight bash >}}
❯ tree -C sass/css
sass/css
├── bootstrap.scss
├── bootswatch5.scss
├── main
│   ├── _decoration.scss
│   ├── _layout-content.scss
│   ├── _layout-page.scss
│   ├── _logo.scss
│   └── _sticky-footer.scss
├── main.scss
└── _variables.scss

2 directories, 9 files
{{< / highlight >}}

![Bootstrap SASS: Directory Structure][044-tree-sass]

#### Custom SASS: Main

So that the main file is going to be like this one below:

* [gitlab.com/.../step-04/sass/css/main.scss][scss-main]

{{< highlight scss >}}
@import 
  "main/layout-page",
  "main/layout-content",
  "main/logo",
  "main/decoration",
  "main/sticky-footer"
;
{{< / highlight >}}

![Bootstrap SASS: SCSS: Main][044-scss-main]

-- -- --

### 2: Simple Responsive

> Using bootstrap class.

We require real example page, as usual.

#### Example Layout Page

> Responsive Class

We would like to apply a simple responsive design for this two elements.

* [gitlab.com/.../views/contents/043-main.njk][njk-c-043-main]

{{< highlight html >}}
  <!-- responsive -->
  <div class="row layout-base maxwidth">

    <main class="col-sm-8 p-4 bg-warning">
       Page Content
    </main>

    <aside class="col-sm-4 p-4 bg-info">
      Side Menu
    </aside>

  </div>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Contents: Main][043-c-main]

#### Page Skeleton

So far we have these three elements:

* Header part: Navigation Bar.

* Main part: Content

* Footer part.

The internal layout for this show case can be summarized as below:

{{< highlight html >}}
<html>
<head>
  <!--head section -->
</head>
<body>

  <!-- header -->
  <nav role="navigation">
       ...
  </nav>
 
  <!-- main: responsive layout -->
  <div class="row">

    <main class="col-md-8">
      <!-- content -->
    </main>

    <aside class="col-md-4">
      <!-- aside -->
    </aside>

  </div>

  <!-- footer -->
  <footer>
    ...
  </footer>

</body>
</html>
{{< / highlight >}}

Of course you can make your own skeleton to suit your needs.

#### The Columns

Now we can have this two elements inside `main` parts.

* `main` element: page content or blog post content

* `aside` element: side panel

What is this `col-md-8` and `col-md-4` class?
Bootstrap is twelve columns based layout.
This means the total column should be `12`.
For `tow-third` plus `one-third` layout,
you use `col-md-8` and `col-md-4`.

#### Blocks

> Nunjucks Document Source

The template for this article is definitely the same as previous code,
the ony differences is the content.

* [gitlab.com/.../views/043-responsive-minimal.njk][njk--043-resp-mini]

{{< highlight jinja >}}
{% extends './layouts/base.njk' %}

{% block htmlhead %}
  {% include './heads/041-meta.njk' %}
  {% include './heads/041-links.njk' %}
{% endblock %}

{% block header %}
  {% include './shared/041-navbar.njk' %}
{% endblock %}

{% block content %}
  {% include './contents/043-main.njk' %}
{% endblock %}

{% block footer %}
  {% include './shared/041-footer.njk' %}
{% endblock %}
{{< / highlight >}}

![Bootstrap5: Nunjucks: Blocks: Minimal][043-njk-resp-mini]

#### Static Page

> HTML Document Target

The generated HTML result can be found here:

* [gitlab.com/.../step-04/043-responsive-minimal.html][html-043-resp-mini]

#### Browser Preview

> Screenshot

For desktop screen the result is similar to below figure:

![Bootstrap Layout: Desktop No Gap][043-qb-resp-mini-sm]

For mobile screen the result is similar to below figure:

![Bootstrap Layout: Mobile No Gap][043-qb-resp-nogap]

-- -- --

### 3: Responsive Spacing

What if I want gap between column,
vertically and horizontally?

#### Vertical on Mobile Screen

This `layout-content` just a custom class.

* [gitlab.com/.../sass/css/main/_layout-content.scss][scss-layout-content]

{{< highlight scss >}}
// responsive - mobile
.layout-base main {
  margin-bottom: 20px;
}

@media only screen and (min-width: 576px) {
  .layout-base main {
    margin-bottom: 0px;
  }
}
{{< / highlight >}}

This will only give gaps, for mobile screen.

![Bootstrap SASS: SCSS: Layout Content][044-scss-layout-cont]

For real life blog I prefer to use `md` vreakpoint instead of `sm`.
so the media screen in my real life blog is:

{{< highlight scss >}}
@media only screen and (min-width: 768px) {
  ...
}
{{< / highlight >}}

Or alternatively, use responsive `mixin` provided by bootstrap.

#### Browser Preview

> Screenshot

For mobile screen the result is similar to below figure:

![Bootstrap Layout: Mobile With Gap][043-qb-resp-mini-xs]

#### Horizontal on Desktop Screen

Bootstrap 5 has grid and flex.
The grid implement native css gap,
so you can use freely.

Unfortunately, bootstrap flex can't do that.
We will deal with this in next chapter with wrapper class.

-- -- --

### 4: Navigation Bar

> Navbar for this chapter.

The next example require a complete page,
so we can examine responsive for the whole page.

For the sake of responsive example page,
I decide to bring back the navbar for this chapter.

This section deal with HTML preparation.
We will focus on the responsive itself in the next section.

#### Templates

> Nunjucks

{{< highlight bash >}}
❯ tree -C views
views
├── 044-responsive-content.njk
├── contents
│   └── 044-main.njk
├── heads
│   └── 044-links.njk
├── layouts
│   └── base.njk
└── shared
    ├── 044-footer.njk
    ├── 044-navbar-collapse.njk
    ├── 044-navbar-dropdown.njk
    ├── 044-navbar.njk
    └── navbar-button.njk

5 directories, 9 files
{{< / highlight >}}

![Bootstrap5: Directory Structure: Templates][044-tree-views]

#### Navbar: Reusable Button

My most favorite part is this reusable chunk.

* [gitlab.com/.../views/shared/navbar-button.njk][njk-s-044-nb-button]

{{< highlight html >}}
      <button type="button"
         class="navbar-toggler"
         data-bs-toggle="collapse"
         data-bs-target="#navbarCollapse"
         aria-controls="navbarCollapse"
         aria-expanded="false"
         aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Shared: Navbar: Button][040-s-navbar-button]

#### Navigation Bar

Now we can go straight with the navbar.

* [gitlab.com/.../views/shared/044-navbar.njk][njk-s-044-navbar]

{{< highlight html >}}
  <!-- header -->
  <nav class="navbar navbar-expand-sm navbar-dark
    maxwidth fixed-top bg-dark">

    <div class="container-fluid">
      <a class="navbar-brand ms-4" href="#">
         <img src="assets/images/logo-gear.png"
           alt="Home" width="32" height="32"/>
      </a>

      {% include './navbar-button.njk' %}
      {% include './044-navbar-collapse.njk' %}
    </div>

  </nav>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Blocks: Navigation Bar][044-s-navigation-bar]

#### Navbar: Collapsible

And go deep into the collapsible detail.

* [gitlab.com/.../views/shared/044-navbar-collapse.njk][njk-s-044-navbar-coll]

{{< highlight html >}}
<div class="collapse navbar-collapse"
    id="navbarCollapse">

<ul class="navbar-nav me-auto">
  <li class="nav-item">
    <a class="nav-link active" href="#">
        <span class="fa fa-home"></span>
        &nbsp;Blog <span class="visually-hidden"
        >(current)</span></a>
  </li>
  <li class="nav-item dropdown">
    {% include './044-navbar-dropdown.njk' %}
  </li>
  <li class="nav-item">
    <a class="nav-link active" href="#">
        <span class="fas fa-child"></span>
        &nbsp;About</a>
  </li>
</ul>
<form class="d-flex form-inline mt-2 mt-md-0">
  <input class="form-control mr-sm-2" type="text"
    placeholder="Search"
    aria-label="Search" role="search">
    &nbsp;
  <button class="btn btn-outline-light my-2 my-sm-0"
    type="submit">Search</button>
</form>

</div>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Blocks: Navigation Bar][044-s-navbar-collapse]

#### Navbar: Dropdown

And again go deeper into the dropdown detail in collapsible chunk.

* [gitlab.com/.../views/shared/044-navbar-dropdown.njk][njk-s-044-navbar-drop]

{{< highlight html >}}
<a  href="#"
    class="nav-link active dropdown-toggle"
    data-bs-toggle="dropdown"
    aria-expanded="false">
    <span class="fa fa-link"></span>
    &nbsp;Archives</a>
<ul class="dropdown-menu">
  <li><a class="dropdown-item" href="#">
    <span class="fa fa-tags"></span>
    &nbsp;By Tags</a></li>
  <li><a class="dropdown-item" href="#">
    <span class="fa fa-folder"></span>
    &nbsp;By Category</a></li>
  <li><hr class="dropdown-divider"></li>
  <li><a class="dropdown-item" href="#">
    <span class="fa fa-calendar"></span>
    &nbsp;By Chronology</a></li>
</ul>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Blocks: Navigation Bar][044-s-navbar-dropdown]

#### Stylesheet

The stylesheet res;onsible for logo in navigation bar can be found as below:

* [gitlab.com/.../sass/css/main/_logo.scss][scss-logo]

{{< highlight scss >}}
.navbar-brand img {
  width: 32px;
  height: 32px;
  margin-top:    -10px;
  margin-bottom: -10px;
}
{{< / highlight >}}

This will only give gaps, for mobile screen.

![Bootstrap SASS: SCSS: Logo][044-scss-logo]

Now we are ready to focus on the responsive itself.

-- -- --

### 5: Responsive Maxwidth

I add a custom class named `maxwidth`.
And, what is this `maxwidth` class anyway ?

I have a smartphone, tablet, sometimes medium screen,
and mostly I'm working with large screen.
What is good for my regular screen, looks ugly in large screen.
My solution is to create maxwidth,
so my content would not be stretched horizontally.

#### Example Layout Page

> Responsive Class

The responsive class for HTML document is similar as above:

* [gitlab.com/.../views/contents/044-main.njk][njk-c-044-main]

{{< highlight html >}}

  <!-- responsive main -->
  <div class="row layout-base maxwidth">

    <main class="col-sm-8 p-4 bg-warning">
      <header>
        <h2>Your mission. Good Luck!</h2>
      </header>

      <article>
        <ul class="list-group">
          ...
        </ul>
      </article>
    </main>

    <aside class="col-sm-4 p-4 bg-dark text-light">
      Side Menu
    </aside>

  </div>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Contents: Main][044-c-main]

#### Container Class

> Why not using Container Class?

Sometimes we can't rely on bootstrap class.
For example `container` is good for some case.
`container` class has its own maximum width.
But it has margin issue with other case.
So we have to make our own maxwidth class.

#### Stylesheet

* [gitlab.com/.../sass/css/main/_layout-page.scss][scss-layout-page].

Add this rule below:

{{< highlight scss >}}
.maxwidth {
  margin-right: 0;
  margin-left: 0;
}

@media only screen and (min-width: 1200px) {
  .maxwidth {
    max-width: 1200px;
    margin-right: auto;
    margin-left: auto;
  }
}
{{< / highlight >}}

![Bootstrap Layout: Wide Maxwidth][044-scss-layout-page]

Remember that in this chapter,
we haven't access bootstrap variables yet.
In the next chapter we are going to use responsive mixin.

{{< highlight scss >}}
@include media-breakpoint-up(xl) {
  ...
}
{{< / highlight >}}

#### Blocks

> Nunjucks Document Source

I need to show how this `maxwidth` works with navbar and footer.
So the templates is completely different now.

* [gitlab.com/.../views/044-responsive-content.njk][njk--044-resp-cont]

{{< highlight jinja >}}
{% extends './layouts/base.njk' %}

{% block htmlhead %}
  {% include './heads/041-meta.njk' %}
  {% include './heads/044-links.njk' %}
{% endblock %}

{% block header %}
  {% include './shared/044-navbar.njk' %}
{% endblock %}

{% block content %}
  {% include './contents/044-main.njk' %}
{% endblock %}

{% block footer %}
  {% include './shared/044-footer.njk' %}
{% endblock %}
{{< / highlight >}}

![Bootstrap5: Nunjucks: Blocks: Content][044-njk-resp-cont]

#### HTML Head

> Assets: Resource Links

Since I utilize navbar,
I also add icons as assets, such as FontAwesome.

* [gitlab.com/.../views/heads/044-links.njk][njk-h-044-links]

{{< highlight html >}}
  <link rel="stylesheet" type="text/css" 
    href="assets/css/bootstrap.css">
  <link rel="stylesheet" type="text/css"
    href="assets/css/main.css">
  <link rel="stylesheet" 
        href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" 
        integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" 
        crossorigin="anonymous">

  <script src="assets/js/popper.min.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Heads: Links][044-h-links]

#### Header and Footer

The detail have already provided in previous section.

* [gitlab.com/.../views/shared/044-navbar.njk][njk-s-044-navbar]

{{< highlight html >}}

  <!-- header -->
  <nav class="navbar navbar-expand-sm navbar-dark
    maxwidth fixed-top bg-dark">

    <div class="container-fluid">
      ...
    </div>

  </nav>
{{< / highlight >}}

* [gitlab.com/.../views/shared/044-footer.njk][njk-s-044-footer]

{{< highlight html >}}

  <!-- footer -->
  <footer class="footer">
    <div class="maxwidth bg-dark text-light text-center">
      &copy; 2021.
    </div>
  </footer>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Reusable Navigation Bar and Footer][044-s-header-footer]

#### Static Page

> HTML Document Target

The generated HTML result can be found here:

* [gitlab.com/.../step-04/044-responsive-content.html][html-044-resp-cont]

#### Browser Preview

> Screenshot

For landscape mobile phone screen the result is similar to below figure:

![Bootstrap Layout: Desktop Content][044-qb-resp-cont-sm]

For portrait mobile screen the result is similar to below figure:

![Bootstrap Layout: Mobile Content][044-qb-resp-cont-xs]

#### Browser Preview: Maxwidth

Do not forget our focus, the maxwidth.

For large display, we limit the maximum width.

![Bootstrap Layout: Mobile Content][044-qb-resp-maxwidth]

-- -- --

### 6: Default Gutter

Bootstrap has a default gutter, actually.
The issue is, you have to put the those `element` inside a `col class`.

#### Example Layout Page

> Responsive Class

Here the responsive class for HTML document.

* [gitlab.com/.../views/contents/045-main.njk][njk-c-045-main]

{{< highlight html >}}

  <!-- responsive main -->
  <div class="row layout-base maxwidth">

    <main class="col-sm-8">
      <section class="p-4 bg-warning h-100">
      <header>
        <h2>Your mission. Good Luck!</h2>
      </header>

      <article>
        <ul class="list-group">
          ...
        </ul>
      </article>
      </section>
    </main>

    <aside class="col-sm-4"> 
      <section class="p-4 bg-dark text-light h-100">
      Side Menu
      </section>
    </aside>

  </div>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Contents: Main][045-c-main]

#### Blocks

> Nunjucks Document Source

* [gitlab.com/.../views/045-responsive-gutter.njk][njk--045-resp-gutt]

{{< highlight jinja >}}
{% extends './layouts/base.njk' %}

{% block htmlhead %}
  {% include './heads/041-meta.njk' %}
  {% include './heads/044-links.njk' %}
{% endblock %}

{% block header %}
  {% include './shared/044-navbar.njk' %}
{% endblock %}

{% block content %}
  {% include './contents/045-main.njk' %}
{% endblock %}

{% block footer %}
  {% include './shared/044-footer.njk' %}
{% endblock %}
{{< / highlight >}}

![Bootstrap5: Nunjucks: Blocks: Gutter][045-njk-resp-gutt]

#### Static Page

> HTML Document Target

The generated HTML result can be found here:

* [gitlab.com/.../step-04/045-responsive-gutter.html][html-045-resp-gutt]:

#### Browser Preview

> Screenshot

For desktop screen the result is similar to below figure:

![Bootstrap Layout: Default Gutter: Desktop Content][045-qb-resp-gutt-sm]

For mobile screen the result is similar to below figure:

![Bootstrap Layout: Default Gutter: Mobile Content][045-qb-resp-gutt-xs]

We have the basic, so we can manually enhance later.

-- -- --

### What is Next 🤔?

We are going to continue to enhance
our responsive material.
But before we do that,
let me introduce you to
some color palletes option available.

Consider continue reading [ [Bootstrap - Sass - Color Pallete][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2021/07/11/bootstrap5-sass-pallete/

[local-bootstrap-v4]:   {{< baseurl >}}frontend/2020/03/10/bootstrap-sass-responsive/

[tutor-step-04]:        {{< tutor-html-bs5 >}}/step-04/

[image-png-layout-page]:    {{< assets-frontend >}}/2019/12/layout-page.png
[image-svg-layout-page]:    {{< assets-frontend >}}/2019/12/layout-page.svg

[//]: <> ( -- -- -- links below -- -- -- )

[044-tree-views]:       {{< baseurl >}}assets/posts/frontend/2021/07/044-tree-views.png
[044-tree-sass]:        {{< baseurl >}}assets/posts/frontend/2021/07/044-tree-sass.png

[044-scss-main]:        {{< baseurl >}}assets/posts/frontend/2021/07/044-scss-main.png
[044-scss-logo]:        {{< baseurl >}}assets/posts/frontend/2021/07/044-scss-logo.png
[044-scss-layout-cont]: {{< baseurl >}}assets/posts/frontend/2021/07/044-scss-layout-content.png
[044-scss-layout-page]: {{< baseurl >}}assets/posts/frontend/2021/07/044-scss-layout-page.png

[044-s-navigation-bar]: {{< baseurl >}}assets/posts/frontend/2021/07/044-s-navbar.png
[044-s-navbar-button]:  {{< baseurl >}}assets/posts/frontend/2021/07/044-s-navbar-button.png
[044-s-navbar-collapse]:{{< baseurl >}}assets/posts/frontend/2021/07/044-s-navbar-coll.png
[044-s-navbar-dropdown]:{{< baseurl >}}assets/posts/frontend/2021/07/044-s-navbar-drop.png

[043-c-main]:           {{< baseurl >}}assets/posts/frontend/2021/07/043-c-main.png
[044-c-main]:           {{< baseurl >}}assets/posts/frontend/2021/07/044-c-main.png
[045-c-main]:           {{< baseurl >}}assets/posts/frontend/2021/07/045-c-main.png

[044-h-links]:          {{< baseurl >}}assets/posts/frontend/2021/07/044-h-links.png

[044-s-header-footer]:  {{< baseurl >}}assets/posts/frontend/2021/07/044-s-header-footer.png

[043-njk-resp-mini]:    {{< baseurl >}}assets/posts/frontend/2021/07/043-njk-resp-mini.png
[044-njk-resp-cont]:    {{< baseurl >}}assets/posts/frontend/2021/07/044-njk-resp-cont.png
[045-njk-resp-gutt]:    {{< baseurl >}}assets/posts/frontend/2021/07/045-njk-resp-gutt.png

[043-qb-resp-mini-sm]:  {{< baseurl >}}assets/posts/frontend/2021/07/043-qb-resp-mini-sm.png
[043-qb-resp-nogap]:    {{< baseurl >}}assets/posts/frontend/2021/07/043-qb-resp-nogap.png
[043-qb-resp-mini-xs]:  {{< baseurl >}}assets/posts/frontend/2021/07/043-qb-resp-mini-xs.png
[044-qb-resp-cont-sm]:  {{< baseurl >}}assets/posts/frontend/2021/07/044-qb-resp-cont-sm.png
[044-qb-resp-cont-xs]:  {{< baseurl >}}assets/posts/frontend/2021/07/044-qb-resp-cont-xs.png
[044-qb-resp-maxwidth]: {{< baseurl >}}assets/posts/frontend/2021/07/044-qb-resp-maxwidth.png
[045-qb-resp-gutt-sm]:  {{< baseurl >}}assets/posts/frontend/2021/07/045-qb-resp-gutt-sm.png
[045-qb-resp-gutt-xs]:  {{< baseurl >}}assets/posts/frontend/2021/07/045-qb-resp-gutt-xs.png

[//]: <> ( -- -- -- links below -- -- -- )

[scss-main]:            {{< tutor-html-bs5 >}}/step-04/sass/css/main.scss
[scss-logo]:            {{< tutor-html-bs5 >}}/step-04/sass/css/main/_logo.scss
[scss-layout-content]:  {{< tutor-html-bs5 >}}/step-04/sass/css/main/_layout-content.scss
[scss-layout-page]:     {{< tutor-html-bs5 >}}/step-04/sass/css/main/_layout-page.scss

[njk-s-044-nb-button]:  {{< tutor-html-bs5 >}}/step-04/views/shared/navbar-button.njk

[njk-h-044-links]:       {{< tutor-html-bs5 >}}/step-04/views/heads/044-links.njk
[njk-s-044-footer]:      {{< tutor-html-bs5 >}}/step-04/views/shared/044-footer.njk
[njk-s-044-navbar]:      {{< tutor-html-bs5 >}}/step-04/views/shared/044-navbar.njk
[njk-s-044-navbar-coll]: {{< tutor-html-bs5 >}}/step-04/views/shared/044-navbar-collapse.njk
[njk-s-044-navbar-drop]: {{< tutor-html-bs5 >}}/step-04/views/shared/044-navbar-dropdown.njk

[njk-c-043-main]:       {{< tutor-html-bs5 >}}/step-04/views/contents/043-main.njk
[njk-c-044-main]:       {{< tutor-html-bs5 >}}/step-04/views/contents/044-main.njk
[njk-c-045-main]:       {{< tutor-html-bs5 >}}/step-04/views/contents/045-main.njk

[njk--043-resp-mini]:   {{< tutor-html-bs5 >}}/step-04/views/043-responsive-minimal.njk
[njk--044-resp-cont]:   {{< tutor-html-bs5 >}}/step-04/views/044-responsive-content.njk
[njk--045-resp-gutt]:   {{< tutor-html-bs5 >}}/step-04/views/045-responsive-gutter.njk

[html-043-resp-mini]:   {{< tutor-html-bs5 >}}/step-04/043-responsive-minimal.html
[html-044-resp-cont]:   {{< tutor-html-bs5 >}}/step-04/044-responsive-content.html
[html-045-resp-gutt]:   {{< tutor-html-bs5 >}}/step-04/045-responsive-gutter.html

