---
type   : post
title  : "Bootstrap - Sass - Palette"
date   : 2021-07-11T09:17:35+07:00
slug   : bootstrap5-sass-palette
categories: [frontend]
tags      : [bootstrap, sass]
keywords  : [open color, material, z-depth, shadow, gradient]
author : epsi
opengraph:
  image: assets/posts/frontend/2021/07/051-qb-material-color-xs.png

toc    : "toc-2021-06-html-bootstrap5-step"

excerpt:
  Creating pallete color for theme customization
  using SASS tools for Materialize, Bootstrap, Open Color.

---

### Preface

> Step Five: Creating pallete color for theme customization.

When you framework is not enough,
you have to make your own component,
or your own set of CSS such as color pallete.

![Bootstrap5: Qutebrowser: Gradient Preview][051-qb-matl-color-xs]

-- -- --

### 1: Chapter Overview

Consider examine the directory structure.

#### Pallete Options

I try three palletes:

* Google Material Color
* Open Color
* bootstrap Color

I will put this color classes in SASS helper,
so that it would generate CSS helper.

For each pallete, I make a helper.
So that now we have three sass file:

* `helper-gmc.sass`,
* `helper-oc.sass`, and
* `helper-bs5.sass`.

#### Source Code: Step-05

Source code for this chapter can be obtained here:

* [gitlab.com/.../step-05/][tutor-step-05]

#### Related Articles

The obsolete Bootstrap v4 article:

* [Bootstrap OC - Sass - Open Color][local-bootstrap-v4]

The old Bulma article:

* [Bulma MD - Helper][local-bulma-helper]

SASS Example

* [Sass - Loop - Spacing Class][local-sass-spacing]

LESS Example

* [Less - Conditional - Color Class][local-less-colors]

-- -- --

### 2: Material Color: SASS Build

For color classes,
I start from verbatim copy from Materialize CSS.
Then I make a few enhacement to suit my needs.

#### Google Material Color

The google material color system can be found at:

* [The color system][ref-gmc]

#### Original SASS Code

This is the source of the code:

* [Dogfalo: _color-variables.scss][ref-color-variables]
* [Dogfalo: _color-classes.scss][ref-color-classes]

Then I copy the code from Dogfalo to my SASS folder as shown below.

#### SASS Directory Structure

It is good time to organize SASS folder.

{{< highlight bash >}}
❯ tree -C css
css
├── bootstrap.scss
├── helper-gmc.scss
├── main
│   └── ...
├── main.scss
├── materialize
│   ├── _color-classes.scss
│   ├── _color-variables.scss
│   └── _shadow.scss
└── _variables.scss
{{< / highlight >}}

![Bootstrap SASS: SASS Directory Structure: Google Material Color][051-tree-sass-gmc]

#### SASS: Helper

* [gitlab.com/.../step-05/sass/css/helper-gmc.scss][scss-helper-gmc]

{{< highlight scss >}}
@charset "UTF-8";

// Materialize CSS
@import "materialize/shadow";

// Color
@import "materialize/color-variables";
@import "materialize/color-classes";
{{< / highlight >}}

![Bootstrap SASS: SCSS: Helper: GMC][051-scss-helper-gmc]

#### SASS: Variables

The color variables could looks like below code:

* [gitlab.com/.../step-05/sass/css/materialize/_color-variables.scss][scss-color-gmc-vars]

{{< highlight scss >}}
$pink: (
  "base":       #e91e63,
  "lighten-5":  #fce4ec,
  "lighten-4":  #f8bbd0,
  "lighten-3":  #f48fb1,
  "lighten-2":  #f06292,
  "lighten-1":  #ec407a,
  "darken-1":   #d81b60,
  "darken-2":   #c2185b,
  "darken-3":   #ad1457,
  "darken-4":   #880e4f,
  "accent-1":    #ff80ab,
  "accent-2":    #ff4081,
  "accent-3":    #f50057,
  "accent-4":    #c51162
);
{{< / highlight >}}

![Bootstrap SASS: SCSS: Helper: GMC: Variables: Pink][051-scss-gmc-vars-one]

At the end of CSS, it also list all the available colors.

{{< highlight scss >}}
$colors: (
  "materialize-red": $materialize-red,
  "red": $red,
  "pink": $pink,
  "purple": $purple,
  "deep-purple": $deep-purple,
  "indigo": $indigo,
  "blue": $blue,
  "light-blue": $light-blue,
  "cyan": $cyan,
  "teal": $teal,
  "green": $green,
  "light-green": $light-green,
  "lime": $lime,
  "yellow": $yellow,
  "amber": $amber,
  "orange": $orange,
  "deep-orange": $deep-orange,
  "brown": $brown,
  "blue-grey": $blue-grey,
  "grey": $grey,
  "shades": $shades
) !default;
{{< / highlight >}}

![Bootstrap SASS: SCSS: Helper: GMC: Variables: All][051-scss-gmc-vars-all]

#### SASS: Classes

We need some processing here.

* [gitlab.com/.../step-05/sass/css/materialize/_color-classes.scss][scss-color-gmc-class]

{{< highlight scss >}}
@each $name, $color in $colors {
  @each $spectrum, $value in $color {
    @if $spectrum == "base" {
      .#{$name} {
        background-color: $value !important;
      }
      .#{$name}-text {
        color: $value !important;
      }
    }
    @else if $name != "shades" {
      .#{$name}.#{$spectrum} {
        background-color: $value !important;
      }
      .#{$name}-text.text-#{$spectrum} {
        color: $value !important;
      }
    }
  }
}
{{< / highlight >}}

and also

{{< highlight scss >}}
// Shade classes
@each $name, $value in $shades {
  .#{$name} {
    background-color: $value !important;
  }
  .#{$name}-text {
    color: $value !important;
  }
}
{{< / highlight >}}

![Bootstrap SASS: SCSS: Helper: GMC: Classes][051-scss-gmc-classes]

#### SASS: Shadow

For shadow, I also shamefully grab from materialize SCSS.

* [gitlab.com/.../step-05/sass/css/materialize/_shadow.scss][scss-shadow]

{{< highlight scss >}}
/* 6dp elevation modified*/
.z-depth-2 {
  box-shadow: 0 4px 5px 0 rgba(0,0,0,0.14),
              0 1px 10px 0 rgba(0,0,0,0.12),
              0 2px 4px -1px rgba(0,0,0,0.3);
}
{{< / highlight >}}

![Bootstrap SASS: SCSS: Helper: GMC: Shadow][051-scss-shadow]

Note that bootstrap5 has their own version of shadow.
But I feel like, I like materialize version is more flexible.

#### Compiled CSS

* [gitlab.com/.../step-05/assets/css/helper-gmc.css][css-helper-gmc]

{{< highlight scss >}}
.pink {
  background-color: #e91e63 !important;
}

.pink-text {
  color: #e91e63 !important;
}

.pink.lighten-5 {
  background-color: #fce4ec !important;
}

.pink-text.text-lighten-5 {
  color: #fce4ec !important;
}

.pink.lighten-4 {
  background-color: #f8bbd0 !important;
}

.pink-text.text-lighten-4 {
  color: #f8bbd0 !important;
}

.pink.lighten-3 {
  background-color: #f48fb1 !important;
}
{{< / highlight >}}

![Bootstrap SASS: Compiled CSS: Helper: GMC][051-css-helper-gmc]

Now that we have the CSS ready to be used,
we still need to prepare the HTML code.

-- -- --

### 2: Material Color: Nunjucks Build

#### Example Layout Page

> Color Class

We would like to apply google material color design for all parts.
Especially the main chuncks.

* [gitlab.com/.../views/contents/051-main-gmc.njk][njk-c-051-main-gmc]

{{< highlight html >}}
  <!-- responsive main -->
  <div class="row layout-base maxwidth">

    <main class="col-md-8 p-4
        white z-depth-3 hoverable">
      <article>
        {% include './051-article-gmc.njk' %}
      </article>
    </main>

    <aside class="col-md-4 p-4
        white z-depth-3 hoverable">
      Side Menu
    </aside>

  </div>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Contents: Main][051-c-main-gmc]

#### Gradient Test

We can try Gradient in article chunck,
such as this class: `blue darken-2 z-depth-3 hoverable`.

* [gitlab.com/.../views/contents/051-article-gmc.njk][njk-c-051-article-gmc]

{{< highlight html >}}
<ul class="list-group">
  <li class="list-group-item
    blue darken-4 text-light">
    <h4>Your Mission</h4></li>

  <li class="list-group-item
    blue darken-2 z-depth-3 hoverable">
    To have, to hold, to love,
    cherish, honor, and protect?</li>

  <li class="list-group-item
    blue lighten-1 z-depth-3 hoverable">
    To shield from terrors known and unknown?
    To lie, to deceive?</li>

  <li class="list-group-item
    blue lighten-3 z-depth-3 hoverable">
    To live a double life,
    to fail to prevent her abduction,
    erase her identity, 
    force her into hiding,
    take away all she has known.</li>

  <li class="list-group-item
    blue lighten-5 z-depth-3 hoverable">
    <blockquote class="bq bq-blue-dark">
      As always, should you be
      caught or killed,
      any knowledge of your actions
      will be disavowed.
    </blockquote>
  </li>

</ul>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Contents: Article][051-c-article-gmc]

#### Nunjucks Directory Structure

The directory structure is typical.

{{< highlight bash >}}
❯ tree -C views
views
├── 051-material-color.njk
├── contents
│   ├── 051-article-gmc.njk
│   └── 051-main-gmc.njk
├── heads
│   ├── links-gmc.njk
│   └── meta.njk
├── layouts
│   └── base.njk
└── shared
    ├── footer-gmc.njk
    ├── navbar-button.njk
    ├── navbar-collapse-gmc.njk
    ├── navbar-dropdown-gmc.njk
    └── navbar-gmc.njk

5 directories, 11 files
{{< / highlight >}}

![Bootstrap SASS: Views Directory Structure: Google Material Color][051-tree-views-gmc]

I don't think that I need to go in detail for each file.
You can examine the code yourself from my github repository.

* [gitlab.com/.../step-05/views/][tutor-step-05-views]

#### Browser Preview

> Screenshot

And have fun with the result.
Consider open the file in Browser.

![Bootstrap5: Qutebrowser: Google Material Color][051-qb-material-color]

It is nice right?

#### Static Page

> HTML Document Target

As a summary from this section,
all the **html element** is as shown below.

* [gitlab.com/.../step-05/051-material-color.html][html-051-matl-color]

Consider examine the HTML output, above.

-- -- --

### 3: Open Color: SASS Build

I have been using `Google Material Color`
for a few years.
AndI wonder if there is an alternate pallete
for google material color.
I search for alternative and
find this `Open Color` with `MIT license`.
The color is not as nice as `GMC`, 
but `OC` is the best alternative so far.
Actually, I cannot find any open source pallete
that can beat these two.

#### Official Site

* [yeun.github.io/open-color](https://yeun.github.io/open-color/)

#### SASS Build

> Inspired by Materialize CSS

I use the color from `OC` pallete,
but I strip down the `sass` version,
and make my own color classes.

Inspired by Materialize CSS,
I use two  `SASS` the variables, and the classes.

#### SASS Directory Structure

With similar structure,
we can do the same with open color.

{{< highlight bash >}}
❯ tree -C css
css
├── bootstrap.scss
├── helper-oc.scss
├── main
│   └── ...
├── main.scss
├── materialize
│   └── _shadow.scss
├── open-color
│   ├── _open-color-classes.scss
│   └── _open-color-variables.scss
└── _variables.scss
{{< / highlight >}}

![Bootstrap SASS: SASS Directory Structure: Open Color][051-tree-sass-oc]

#### SASS: Helper

* [gitlab.com/.../step-05/sass/css/helper-oc.scss][scss-helper-oc]

{{< highlight scss >}}
@charset "UTF-8";

// Materialize CSS
@import "materialize/shadow";

// Heeyeun's Open Color
@import "open-color/_open-color-variables";
@import "open-color/_open-color-classes";
{{< / highlight >}}

![Bootstrap SASS: SCSS: Helper: Open Color][051-scss-helper-oc]

Instead of making a new shadow file,
I use pre-existing sahdow from materialize folder.

#### SASS: Variables

> The spectrum array is start from zero

* [gitlab.com/.../step-05/sass/css/materialize/_color-variables.scss][scss-color-variables]

{{< highlight scss >}}
$oc-pink-list: (
  "0": #fff0f6,
  "1": #ffdeeb,
  "2": #fcc2d7,
  "3": #faa2c1,
  "4": #f783ac,
  "5": #f06595,
  "6": #e64980,
  "7": #d6336c,
  "8": #c2255c,
  "9": #a61e4d
);
{{< / highlight >}}

![Bootstrap SASS: SCSS: Helper: Open Color: Variables: Pink][051-scss-oc-vars-one]

At the end of CSS, it also list all the available colors.

{{< highlight scss >}}
$oc-color-spectrum:   9;

$oc-color-list: (
  "gray":   $oc-gray-list,
  "red":    $oc-red-list,
  "pink":   $oc-pink-list,
  "grape":  $oc-grape-list,
  "violet": $oc-violet-list,
  "indigo": $oc-indigo-list,
  "blue":   $oc-blue-list,
  "cyan":   $oc-cyan-list,
  "teal":   $oc-teal-list,
  "green":  $oc-green-list,
  "lime":   $oc-lime-list,
  "yellow": $oc-yellow-list,
  "orange": $oc-orange-list
);
{{< / highlight >}}

![Bootstrap SASS: SCSS: Helper: Open Color: Variables: All][051-scss-oc-vars-all]

#### SASS: Classes

SASS is easy.
You can make the loop yourself.

* [gitlab.com/.../step-05/sass/css/materialize/_color-classes.scss][scss-color-gmc-class]

{{< highlight scss >}}
@each $name, $color in $oc-color-list {
  @each $spectrum, $value in $color {
    .oc-#{$name}-#{$spectrum} {
      background-color: $value !important;
    }
    .oc-#{$name}-#{$spectrum}-text {
      color: $value !important;
    }
  }
}
{{< / highlight >}}

If you do not understand how it works,
you might consider to read this article first:

* [Sass - Loop - Spacing Class][local-sass-spacing]

I also add loop to build `black` and `white` class.

{{< highlight scss >}}
@each $color, $value in $oc-general-list {
  .oc-#{$color} {
    background-color: $value !important;
  }
  .oc-#{$color}-text {
    color: $value !important;
  }
}
{{< / highlight >}}

![Bootstrap SASS: SCSS: Helper: Open Color: Classes][051-scss-oc-classes]

#### SASS Shadow

Open Color is not the only thing I need for this templates to works.
I also use materialize shadow as almost verbatim copy,
that contain `z-depth` and `hoverable` classes.

#### Compiled CSS

* [gitlab.com/.../step-05/assets/css/helper-gmc.css][css-helper-oc]

{{< highlight scss >}}
.oc-pink-0 {
  background-color: #fff0f6 !important;
}

.oc-pink-0-text {
  color: #fff0f6 !important;
}

.oc-pink-1 {
  background-color: #ffdeeb !important;
}

.oc-pink-1-text {
  color: #ffdeeb !important;
}

.oc-pink-2 {
  background-color: #fcc2d7 !important;
}

.oc-pink-2-text {
  color: #fcc2d7 !important;
}
{{< / highlight >}}

![Bootstrap SASS: Compiled CSS: Helper: Open Color][051-css-helper-oc]

-- -- --

### 3: Open Color: Nunjucks Build

Very similar with previous,
but different color class.

#### Example Layout Page

> Color Class

We would like to apply open color design for all parts.
Especially the main chuncks.

* [gitlab.com/.../views/contents/051-main-oc.njk][njk-c-051-main-oc]

{{< highlight html >}}
  <!-- responsive main -->
  <div class="row layout-base maxwidth">

    <main class="col-md-8 p-4
        oc-white z-depth-3 hoverable">
      <article>
        {% include './051-article-oc.njk' %}
      </article>
    </main>

    <aside class="col-md-4 p-4
        oc-white z-depth-3 hoverable">
      Side Menu
    </aside>
  </div>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Contents: Main][051-c-main-oc]

#### Gradient Test

We can try Gradient in article chunck,
such as this class: `oc-blue-9 text-light`.

* [gitlab.com/.../views/contents/051-article-oc.njk][njk-c-051-article-oc]

{{< highlight html >}}
<ul class="list-group">
  <li class="list-group-item oc-blue-9 text-light">
    <h4>Your Mission</h4></li>

  <li class="list-group-item oc-blue-7">
    To have, to hold, to love,
    cherish, honor, and protect?</li>

  <li class="list-group-item oc-blue-5">
    To shield from terrors known and unknown?
    To lie, to deceive?</li>

  <li class="list-group-item oc-blue-3">
    To live a double life,
    to fail to prevent her abduction,
    erase her identity, 
    force her into hiding,
    take away all she has known.</li>

  <li class="list-group-item oc-blue-1">
    <blockquote class="bq bq-blue-dark">
      As always, should you be
      caught or killed,
      any knowledge of your actions
      will be disavowed.
    </blockquote>
  </li>

</ul>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Contents: Article][051-c-article-oc]

#### Nunjucks Directory Structure

The directory structure is typical.

{{< highlight bash >}}
❯ tree -C views
views
├── 051-material-color.njk
├── contents
│   ├── 051-article-gmc.njk
│   └── 051-main-gmc.njk
├── heads
│   ├── links-gmc.njk
│   └── meta.njk
├── layouts
│   └── base.njk
└── shared
    ├── footer-gmc.njk
    ├── navbar-button.njk
    ├── navbar-collapse-gmc.njk
    ├── navbar-dropdown-gmc.njk
    └── navbar-gmc.njk

5 directories, 11 files
{{< / highlight >}}

![Bootstrap SASS: Views Directory Structure: Open Color][051-tree-views-oc]

I don't think that I need to go in detail for each file.
You can examine the code yourself from my github repository.

* [gitlab.com/.../step-05/views/][tutor-step-05-views]

#### Browser Preview

> Screenshot

And have fun with the result.
Consider open the file in Browser.

![Bootstrap5: Qutebrowser: Open Color][051-qb-open-color]

It looks good,
although google material looks better.

#### Static Page

> HTML Document Target

As a summary from this section,
all the **html element** is as shown below.

* [gitlab.com/.../step-05/051-open-color.html][html-051-open-color]

Consider examine the HTML output, above.

-- -- --

### 4: Bootstrap Color: SASS Build

I don't think that  bootstrap color is designed for gradient.
But I decide to give it a try, and see how it goes.

#### Bootstrap Color

The bootstrap color can be found at:

* [Color][ref-bs5]

The color is buried inside bootstrap sass,
so we have to include it somewhere.

#### SASS Directory Structure

Consider arrange our folder again,
but this time for bootstrap color.

{{< highlight bash >}}
❯ tree -C css
css
├── bootstrap.scss
├── bs5-colors
│   ├── _bs5-color-classes.scss
│   └── _bs5-color-variables.scss
├── helper-bs5.scss
├── main
│   └── ...
├── main.scss
├── materialize
│   └── _shadow.scss
└── _variables.scss
{{< / highlight >}}

![Bootstrap SASS: SASS Directory Structure: Boostrap Color][051-tree-sass-bs5]

#### SASS: Helper

* [gitlab.com/.../step-05/sass/css/helper-bs5.scss][scss-helper-bs5]

{{< highlight scss >}}
@charset "UTF-8";

// Materialize CSS
@import "materialize/shadow";

// Color
@import "bs5-colors/_bs5-color-variables";
@import "bs5-colors/_bs5-color-classes";
{{< / highlight >}}

![Bootstrap SASS: SCSS: Helper: BS5][051-scss-helper-bs5]

Instead of making a new shadow file,
I use pre-existing sahdow from materialize folder.

#### SASS: Variables

The color variables can be imported from internal bootstrap sass.

* [gitlab.com/.../step-05/sass/css/bs5-colors/_color-variables.scss][scss-color-bs5-vars]

{{< highlight scss >}}
@import 
  // pre variables
  "../../bootstrap5/functions",
  "../../bootstrap5/variables"
;
{{< / highlight >}}

![Bootstrap SASS: SCSS: Helper: BS5 Variables: ][051-scss-bs5-vars-one]

At the end of CSS, it also list all the available colors.

{{< highlight scss >}}
$bs-color-list: (
  "blue":       $blues,
  "indigo":     $indigos,
  "purple":     $purples,
  "pink":       $pinks,
  "red":        $reds,
  "orange":     $oranges,
  "yellow":     $yellows,
  "green":      $greens,
  "teal":       $teals,
  "cyan":       $cyans
);
{{< / highlight >}}

![Bootstrap SASS: SCSS: Helper: BS5: Variables: All][051-scss-bs5-vars-all]

#### SASS: Classes

We need some processing here.

* [gitlab.com/.../step-05/sass/css/materialize/_color-classes.scss][scss-color-bs5-class]

{{< highlight scss >}}
@each $name, $color in $bs-color-list {
  @each $spectrum, $value in $color {
    .bs-#{$spectrum} {
      background-color: $value !important;
    }
    .bs-#{$spectrum}-text {
      color: $value !important;
    }
  }
  .bq-#{$name}-dark {
    border-left: 5px solid map-get($color , "#{$name}-700");
  }
  .bq-#{$name}-light {
    border-left: 5px solid map-get($color , "#{$name}-300");
  }
}
{{< / highlight >}}

and depend on your need,
you can append other classes as well.

{{< highlight scss >}}
@each $spectrum, $value in $grays {
  .bs-gray-#{$spectrum} {
    background-color: $value !important;
  }
  .bs-gray-#{$spectrum}-text {
    color: $value !important;
  }
}
.bq-gray-dark {
  border-left: 5px solid map-get($grays , "700");
}
.bq-gray-light {
  border-left: 5px solid map-get($grays , "300");
}
{{< / highlight >}}

![Bootstrap SASS: SCSS: Helper: BS5: Classes][051-scss-bs5-classes]

#### SASS: Shadow

I also use materialize shadow as almost verbatim copy,
that contain `z-depth` and `hoverable` classes.

#### Compiled CSS

* [gitlab.com/.../step-05/assets/css/helper-bs5.css][css-helper-bs5]

{{< highlight scss >}}
.bs-pink-100 {
  background-color: #f7d6e6 !important;
}

.bs-pink-100-text {
  color: #f7d6e6 !important;
}

.bs-pink-200 {
  background-color: #efadce !important;
}

.bs-pink-200-text {
  color: #efadce !important;
}

.bs-pink-300 {
  background-color: #e685b5 !important;
}

.bs-pink-300-text {
  color: #e685b5 !important;
}
{{< / highlight >}}

![Bootstrap SASS: Compiled CSS: Helper: BS5][051-css-helper-bs5]

Now that we have the CSS ready to be used,
we still need to prepare the HTML code.

-- -- --

### 4: Bootstrap Color: Nunjucks Build

Also very similar with previous,
but different color class.

#### Example Layout Page

> Color Class

We would like to apply bootstrap color design for all parts.
Especially the main chuncks.

* [gitlab.com/.../views/contents/051-main-bs5.njk][njk-c-051-main-bs5]

{{< highlight html >}}
  <!-- responsive main -->
  <div class="row layout-base maxwidth">

    <main class="col-md-8 p-4
        bs-white z-depth-3 hoverable">
      <article>
        {% include './051-article-bs5.njk' %}
      </article>
    </main>

    <aside class="col-md-4 p-4
        bs-white z-depth-3 hoverable">
      Side Menu
    </aside>

  </div>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Contents: Main][051-c-main-bs5]

#### Gradient Test

We can try Gradient in article chunck,
such as this class: `bs-blue-900 text-light`.

* [gitlab.com/.../views/contents/051-article-oc.njk][njk-c-051-article-bs5]

{{< highlight html >}}
<ul class="list-group">
  <li class="list-group-item bs-blue-900 text-light">
    <h4>Your Mission</h4></li>

  <li class="list-group-item bs-blue-700">
    To have, to hold, to love,
    cherish, honor, and protect?</li>

  <li class="list-group-item bs-blue-500">
    To shield from terrors known and unknown?
    To lie, to deceive?</li>

  <li class="list-group-item bs-blue-300">
    To live a double life,
    to fail to prevent her abduction,
    erase her identity, 
    force her into hiding,
    take away all she has known.</li>

  <li class="list-group-item bs-blue-100">
    <blockquote class="bq bq-blue-dark">
      As always, should you be
      caught or killed,
      any knowledge of your actions
      will be disavowed.
    </blockquote>
  </li>

</ul>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Contents: Article][051-c-article-bs5]

#### Nunjucks Directory Structure

The directory structure is self explanatory.
Very similar with previous.

{{< highlight bash >}}
❯ tree -C views
views
├── 051-bootstrap-color.njk
├── contents
│   ├── 051-article-bs5.njk
│   └── 051-main-bs5.njk
├── heads
│   ├── links-bs5.njk
│   └── meta.njk
├── layouts
│   └── base.njk
└── shared
    ├── footer-bs5.njk
    ├── navbar-bs5.njk
    ├── navbar-button.njk
    ├── navbar-collapse-bs5.njk
    └── navbar-dropdown-bs5.njk

5 directories, 11 files
{{< / highlight >}}

![Bootstrap SASS: Views Directory Structure: Bootstrap Color][051-tree-views-bs5]

I don't think that I need to go in detail for each file.
You can examine the code yourself from my github repository.

* [gitlab.com/.../step-05/views/][tutor-step-05-views]

#### Browser Preview

> Screenshot

And have fun with the result.
Consider open the file in Browser.

![Bootstrap5: Qutebrowser: Bootstrap Color][051-qb-boots-color]

It looks darker.
But that's okay.

#### Static Page

> HTML Document Target

As a summary from this section,
all the **html element** is as shown below.

* [gitlab.com/.../step-05/051-material-color.html][html-051-boots-color]

Consider examine the HTML output, above.

-- -- --

### What is Next 🤔?

We are ready to continue to enhance our responsive with multicolor design.

We still have real life decoration
before experiment with dark mode,
So we have to move on with responsive, widget, and blog post.

Consider continue reading [ [Bootstrap - Sass - Responsive][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2021/07/15/bootstrap5-sass-responsive-gap/

[local-bootstrap-v4]:   {{< baseurl >}}frontend/2020/03/11/bootstrap-oc-sass-open-color/
[local-bulma-helper]:   {{< baseurl >}}frontend/2019/06/12/bulma-md-helper/
[local-sass-spacing]:   {{< baseurl >}}frontend/2019/06/21/sass-loop-spacing-class/
[local-less-colors]:    {{< baseurl >}}frontend/2019/10/04/less-conditional-color-class/

[tutor-step-05]:        {{< tutor-html-bs5 >}}/step-05/
[tutor-step-05-views]:  {{< tutor-html-bs5 >}}/step-05/views/

[ref-gmc]:              https://m2.material.io/design/color/the-color-system.html
[ref-bs5]:              https://getbootstrap.com/docs/5.0/customize/color/

[ref-color-variables]:  https://github.com/Dogfalo/materialize/blob/v1-dev/sass/components/_color-variables.scss
[ref-color-classes]:    https://github.com/Dogfalo/materialize/blob/v1-dev/sass/components/_color-classes.scss

[//]: <> ( -- -- -- links below -- -- -- )

[051-tree-sass-gmc]:    {{< baseurl >}}assets/posts/frontend/2021/07/051-tree-sass-gmc.png
[051-tree-sass-oc]:     {{< baseurl >}}assets/posts/frontend/2021/07/051-tree-sass-oc.png
[051-tree-sass-bs5]:    {{< baseurl >}}assets/posts/frontend/2021/07/051-tree-sass-bs5.png

[051-tree-views-gmc]:   {{< baseurl >}}assets/posts/frontend/2021/07/051-tree-views-gmc.png
[051-tree-views-oc]:    {{< baseurl >}}assets/posts/frontend/2021/07/051-tree-views-oc.png
[051-tree-views-bs5]:   {{< baseurl >}}assets/posts/frontend/2021/07/051-tree-views-bs5.png

[051-css-helper-gmc]:   {{< baseurl >}}assets/posts/frontend/2021/07/051-css-helper-gmc.png
[051-css-helper-oc]:    {{< baseurl >}}assets/posts/frontend/2021/07/051-css-helper-oc.png
[051-css-helper-bs5]:   {{< baseurl >}}assets/posts/frontend/2021/07/051-css-helper-bs5.png
[051-scss-helper-gmc]:  {{< baseurl >}}assets/posts/frontend/2021/07/051-scss-helper-gmc.png
[051-scss-helper-oc]:   {{< baseurl >}}assets/posts/frontend/2021/07/051-scss-helper-oc.png
[051-scss-helper-bs5]:  {{< baseurl >}}assets/posts/frontend/2021/07/051-scss-helper-bs5.png

[051-scss-gmc-vars-all]:{{< baseurl >}}assets/posts/frontend/2021/07/051-scss-gmc-vars-all.png
[051-scss-gmc-vars-one]:{{< baseurl >}}assets/posts/frontend/2021/07/051-scss-gmc-vars-one.png
[051-scss-gmc-classes]: {{< baseurl >}}assets/posts/frontend/2021/07/051-scss-gmc-classes.png
[051-scss-shadow]:      {{< baseurl >}}assets/posts/frontend/2021/07/051-scss-gmc-shadow.png

[051-scss-oc-vars-all]: {{< baseurl >}}assets/posts/frontend/2021/07/051-scss-oc-vars-all.png
[051-scss-oc-vars-one]: {{< baseurl >}}assets/posts/frontend/2021/07/051-scss-oc-vars-one.png
[051-scss-oc-classes]:  {{< baseurl >}}assets/posts/frontend/2021/07/051-scss-oc-classes.png

[051-scss-bs5-vars-all]:{{< baseurl >}}assets/posts/frontend/2021/07/051-scss-bs5-vars-all.png
[051-scss-bs5-vars-one]:{{< baseurl >}}assets/posts/frontend/2021/07/051-scss-bs5-vars-one.png
[051-scss-bs5-classes]: {{< baseurl >}}assets/posts/frontend/2021/07/051-scss-bs5-classes.png

[051-c-main-gmc]:       {{< baseurl >}}assets/posts/frontend/2021/07/051-c-main-gmc.png
[051-c-article-gmc]:    {{< baseurl >}}assets/posts/frontend/2021/07/051-c-article-gmc.png
[051-c-main-oc]:        {{< baseurl >}}assets/posts/frontend/2021/07/051-c-main-oc.png
[051-c-article-oc]:     {{< baseurl >}}assets/posts/frontend/2021/07/051-c-article-oc.png
[051-c-main-bs5]:       {{< baseurl >}}assets/posts/frontend/2021/07/051-c-main-bs5.png
[051-c-article-bs5]:    {{< baseurl >}}assets/posts/frontend/2021/07/051-c-article-bs5.png

[051-qb-matl-color-xs]: {{< baseurl >}}assets/posts/frontend/2021/07/051-qb-material-color-xs.png
[051-qb-boots-color]:   {{< baseurl >}}assets/posts/frontend/2021/07/051-qb-bootstrap-color.png
[051-qb-material-color]:{{< baseurl >}}assets/posts/frontend/2021/07/051-qb-material-color.png
[051-qb-open-color]:    {{< baseurl >}}assets/posts/frontend/2021/07/051-qb-open-color.png

[//]: <> ( -- -- -- links below -- -- -- )

[css-helper-gmc]:       {{< tutor-html-bs5 >}}/step-05/assets/css/helper-gmc.css
[scss-helper-gmc]:      {{< tutor-html-bs5 >}}/step-05/sass/css/helper-gmc.scss
[scss-color-gmc-class]: {{< tutor-html-bs5 >}}/step-05/sass/css/materialize/_color-classes.scss
[scss-color-gmc-vars]:  {{< tutor-html-bs5 >}}/step-05/sass/css/materialize/_color-variables.scss
[scss-shadow]:          {{< tutor-html-bs5 >}}/step-05/sass/css/materialize/_shadow.scss

[css-helper-oc]:        {{< tutor-html-bs5 >}}/step-05/assets/css/helper-oc.css
[scss-helper-oc]:       {{< tutor-html-bs5 >}}/step-05/sass/css/helper-oc.scss
[scss-color-oc-class]:  {{< tutor-html-bs5 >}}/step-05/sass/css/open-color/_open-color-classes.scss
[scss-color-oc-vars]:   {{< tutor-html-bs5 >}}/step-05/sass/css/open-color/_open-color-variables.scss

[css-helper-bs5]:       {{< tutor-html-bs5 >}}/step-05/assets/css/helper-bs5.css
[scss-helper-bs5]:      {{< tutor-html-bs5 >}}/step-05/sass/css/helper-bs5.scss
[scss-color-bs5-class]: {{< tutor-html-bs5 >}}/step-05/sass/css/open-color/_bs5-color-classes.scss
[scss-color-bs5-vars]:  {{< tutor-html-bs5 >}}/step-05/sass/css/open-color/_bs5-color-variables.scss

[njk-c-051-main-gmc]:   {{< tutor-html-bs5 >}}/step-05/views/contents/051-main-gmc.njk
[njk-c-051-article-gmc]:{{< tutor-html-bs5 >}}/step-05/views/contents/051-article-gmc.njk
[njk-c-051-main-oc]:    {{< tutor-html-bs5 >}}/step-05/views/contents/051-main-oc.njk
[njk-c-051-article-oc]: {{< tutor-html-bs5 >}}/step-05/views/contents/051-article-oc.njk
[njk-c-051-main-bs5]:   {{< tutor-html-bs5 >}}/step-05/views/contents/051-main-bs5.njk
[njk-c-051-article-bs5]:{{< tutor-html-bs5 >}}/step-05/views/contents/051-article-bs5.njk

[html-051-matl-color]:  {{< tutor-html-bs5 >}}/step-05/051-material-color.html
[html-051-boots-color]: {{< tutor-html-bs5 >}}/step-05/051-bootstrap-color.html
[html-051-open-color]:  {{< tutor-html-bs5 >}}/step-05/051-opencolor.html

