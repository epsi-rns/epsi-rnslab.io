---
type   : post
title  : "Bootstrap - Sass - Responsive Gap"
date   : 2021-07-15T09:17:35+07:00
slug   : bootstrap5-sass-responsive-gap
categories: [frontend]
tags      : [bootstrap, sass]
keywords  : [layout, solving gap issue, responsive spacing]
author : epsi
opengraph:
  image: assets/posts/frontend/2021/07/053-qb-gap-md.png

toc    : "toc-2021-06-html-bootstrap5-step"

excerpt:
  Custom theme using Google Material Color, step by step.
  Wrapper class to solve bootstrap horizontal gap issue in flex.

---

### Preface

> Step Five: Wrapper class to solve bootstrap horizontal gap issue in flex

We can creatively make bootstrap based theme,
with trade off making additional custom stylesheet .

-- -- --

### 1: Chapter Overview

As we have seen in previous responsive chapter,
bootstrap flex can't have gap between column directly.
Bootstrap flex use gutter by putting `element` class  inside `column` class.
We can also make our own custom `wrapper` class inside `column`.
We require to leave bootstrap behind for our new component class.

{{< embed-video width="480" height="520"
    src="assets/posts/frontend/2021/07/054-bootstrap5-responsive.mp4" >}}

#### Source Code: Step-05

Source code for this chapter can be obtained here:

* [gitlab.com/.../step-05/][tutor-step-05]

#### Related Articles

The obsolete Bootstrap v4 article:

* [Bootstrap OC - Sass - Responsive][local-bootstrap-v4]

-- -- --

### 2: Responsive Gap

#### How does it Works?

You can use `div` element inside `div` element,
or for `html5` you can utilize `section` inside `main`.

#### Nunjucks Directory Structure

I don't think that I need to go in detail for each file.
You can examine the code yourself from my github repository.

* [gitlab.com/.../step-05/views/][tutor-step-05-views]

{{< highlight bash >}}
❯ tree -C views
views
├── 052-gutter.njk
├── contents
│   └── 052-main.njk
├── heads
│   ├── links-gmc.njk
│   └── meta.njk
├── layouts
│   └── base.njk
└── shared
    ├── footer-gmc.njk
    ├── navbar-button.njk
    ├── navbar-collapse-gmc.njk
    ├── navbar-dropdown-gmc.njk
    └── navbar-gmc.njk
{{< / highlight >}}

![Bootstrap SASS: Views Directory Structure: Gutter][052-tree-views-gutter]

The directory structure is typical.

#### Main Content

For bootstrap reason,
we need to reset the horizontal padding `px` for column.

* [gitlab.com/.../views/contents/052-main.njk][njk-c-052-main]

{{< highlight html >}}
  <!-- responsive main -->
  <div class="row layout-base maxwidth">

    <main class="col-md-8 px-0">
      <section class="main-wrapper p-4
           white z-depth-3 hoverable">
        <article>
          Page Content
        </article>
      </section>
    </main>

    <aside class="col-md-4 px-0">
      <section class="aside-wrapper p-4
           white z-depth-3 hoverable">
        Side Menu
      </section>
    </aside>

  </div>
{{< / highlight >}}

#### Stylesheet

It is basically just, adding `margin-left` and `margin-right`.

{{< highlight scss >}}
// responsive - mobile
.main-wrapper {
  margin-right: 0px;
}
.aside-wrapper {
  margin-left: 0px;
}

@include media-breakpoint-up(sm) {
  .main-wrapper {
    margin-right: 10px;
  }
  .aside-wrapper {
    margin-left: 10px;
  }
}
{{< / highlight >}}

We can omit default margin for mobile,
since bootstrap has zero margin anyway.

![Bootstrap OC: Horizontal Gap Column][052-qb-gap1-sm]

For both horizontal gap and vertical gap,
the style sheet is shown as below:

{{< highlight scss >}}
// responsive - mobile
.main-wrapper {
  margin-bottom: 20px;
}

@include media-breakpoint-up(sm) {
  .main-wrapper {
    margin-bottom: 0px;
  }
  .main-wrapper {
    margin-right: 10px;
  }
  .aside-wrapper {
    margin-left: 10px;
  }
}
{{< / highlight >}}

![Bootstrap OC: Vertical Gap Column][052-qb-gap1-xs]

Notice that now we are accessing bootstrap breakpoint variable.

{{< highlight scss >}}
$grid-breakpoints: (
  xs: 0,
  sm: 576px,
  md: 768px,
  lg: 992px,
  xl: 1200px,
  xxl: 1400px
) !default;
{{< / highlight >}}

-- -- --

### 3: Dual Column

I would like to give an aesthetic accent.

#### Row Edge: Desktop

For desktop screen,
the double columns should not touch the edge of the screen.

* [gitlab.com/.../sass/css/main/_layout-content.scss][scss-layout-content].

{{< highlight scss >}}
@include media-breakpoint-up(sm) {
  .main-wrapper {
    margin-right: 10px;
    margin-left: 10px;
  }
  .aside-wrapper {
    margin-left: 10px;
    margin-right: 10px;
  }
}
{{< / highlight >}}

![Bootstrap OC: Row Edge Margin on Desktop Screen][052-qb-gap2-sm]

#### Row Edge: Wide

But for wide screen, this should be the same size,
as both navbar header and footer.

{{< highlight scss >}}
@include media-breakpoint-up(md) {
  .main-wrapper {
    margin-left: 0px;
  }
  .aside-wrapper {
    margin-right: 0px;
  }
}
{{< / highlight >}}

![Bootstrap OC: Row Edge Margin on Wide Screen][052-qb-gap2-md]

I must admit that I still don't know how to make good eye dark mode theme.
So I experiment with this minimalist dark mode.

![Bootstrap OC: Row Edge Margin on Wide Screen: Dark Mode][052-qb-gap2-md-dark]

#### Main Article

Now consider give it a content,

* [gitlab.com/.../views/contents/053-main.njk][njk-c-053-main]

{{< highlight html >}}
  <!-- responsive main -->
  <div class="row layout-base maxwidth">

    <main class="col-sm-8 px-0">
      <section class="main-wrapper p-4
           white z-depth-3 hoverable">
        <article>
          {% include './052-article.njk' %}
        </article>
      </section>
    </main>

    <aside class="col-sm-4 px-0">
      <section class="aside-wrapper p-4
           white z-depth-3 hoverable">
        Side Menu
      </section>
    </aside>

  </div>
{{< / highlight >}}

And see how the looks goes.

* [gitlab.com/.../views/contents/053-article.njk][njk-c-053-article]

{{< highlight html >}}
<ul class="list-group">
  <li class="list-group-item blue darken-4
    text-light z-depth-3 hoverable">
    <h4>Your Mission</h4></li>

  <li class="list-group-item blue darken-2
    z-depth-3 hoverable">
    To have, to hold, to love,
    cherish, honor, and protect?</li>

  <li class="list-group-item blue lighten-1
    z-depth-3 hoverable">
    To shield from terrors known and unknown?
    To lie, to deceive?</li>

  <li class="list-group-item blue lighten-3
    z-depth-3 hoverable">
    To live a double life,
    to fail to prevent her abduction,
    erase her identity, 
    force her into hiding,
    take away all she has known.</li>

  <li class="list-group-item blue lighten-5
    z-depth-3 hoverable">
    <blockquote class="bq bq-blue-dark">
      As always, should you be
      caught or killed,
      any knowledge of your actions
      will be disavowed.
    </blockquote>
  </li>

</ul>
{{< / highlight >}}

This looks okay with white color.

![Bootstrap5: Responsive Gap: Double Column Test][053-qb-gap-md]

I never been good with dark mode.
But I provide glowing dark anyway.

![Bootstrap5: Responsive Gap: Double Column Test: Dark][053-qb-gap-md-dark]

I simply don't know how to do it in dark mode.

-- -- --

### 4: Single Column

> Test with Landing Page

We are not finished yet.
We have to test for single column,
such as `main` content without `aside` sidebar.
Feature is likely to fail,
if we do not have enough test in the first place.

#### Stylesheet

For single column, we need to add this one `single` class,
to fix `margin-right` for wide screen.

* [gitlab.com/.../sass/css/main/_layout-content.scss][scss-layout-content]

{{< highlight scss >}}
@include media-breakpoint-up(xl) {
  .main-wrapper {
    margin-left: 0px;
  }
  .aside-wrapper {
    margin-right: 0px;
  }
  .main-wrapper.single {
    margin-right: 0px;
  }
}
{{< / highlight >}}

#### HTML Content

My favorite single column is, `landing page`.

* [gitlab.com/.../views/contents/054-main.njk][njk-c-054-main]

{{< highlight html >}}
  <!-- responsive main -->
  <div class="row layout-base maxwidth">

    <main class="col px-0">
      <section class="main-wrapper single
          text-center p-4 white
          z-depth-3 hoverable">
        <article>
          {% include './054-article.njk' %}
        </article>
      </section>
    </main>

  </div>
{{< / highlight >}}

While the article contain

* [gitlab.com/.../views/contents/054-article.njk][njk-c-054-article]

{{< highlight html >}}
<p>
  <a href="#" 
    class="btn btn-primary my-2
        z-depth-1 hoverable"
    >Articles Sorted by Month</a>
  <a href="#" 
    class="btn btn-secondary my-2
        z-depth-1 hoverable"
    >Articles Sorted by Tag</a>
</p>

<p class="text-muted">
    As always,
    should you be caught or killed,
    any knowledge of your actions
    will be disavowed.</p>
{{< / highlight >}}

![Bootstrap5: Responsive Gap: Single Column Test][054-qb-landing]

And again also experiment with dark mode.

![Bootstrap5: Responsive Gap: Single Column Test: Dark Mode][054-qb-landing-dark]

You should try the gap yourself between screen: mobile, desktop, and wide.

-- -- --

### 5: Summary

> Compiled CSS

Here below is the CSS compiled result of above SASS.

* [gitlab.com/.../sass/css/main/_layout-content.scss][css-main]

{{< highlight scss >}}
.main-wrapper {
  margin-bottom: 20px;
}

@media (min-width: 576px) {
  .main-wrapper {
    margin-bottom: 0px;
  }
  .main-wrapper {
    margin-right: 10px;
    margin-left: 10px;
  }
  .aside-wrapper {
    margin-left: 10px;
    margin-right: 10px;
  }
}
@media (min-width: 768px) {
  .main-wrapper {
    margin-left: 0px;
  }
  .aside-wrapper {
    margin-right: 0px;
  }
  .main-wrapper.single {
    margin-right: 0px;
  }
}
{{< / highlight >}}

The result of compiled `CSS` can be seen in left pane in figure below,
while in the right pane is the `SASS` source.

![Bootstrap SASS: Compiled CSS][052-scss-pane-compare]

-- -- --

### What is Next 🤔?

While we design the layout skeleton in this article,
now we need to fill of each layout such as blog post and side widget.

Consider continue reading [ [Bootstrap - Sass - Widget][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2021/07/21/bootstrap5-sass-widget/

[local-bootstrap-v4]:   {{< baseurl >}}frontend/2020/03/11/bootstrap-oc-sass-open-color/

[tutor-step-05]:        {{< tutor-html-bs5 >}}/step-05/
[tutor-step-05-views]:  {{< tutor-html-bs5 >}}/step-05/views/

[//]: <> ( -- -- -- links below -- -- -- )

[052-tree-views-gutter]:{{< baseurl >}}assets/posts/frontend/2021/07/052-tree-views-gutter.png

[052-scss-pane-compare]:{{< baseurl >}}assets/posts/frontend/2021/07/052-scss-pane-compare.png

[052-qb-gap1-xs]:       {{< baseurl >}}assets/posts/frontend/2021/07/052-qb-gap1-xs.png
[052-qb-gap1-sm]:       {{< baseurl >}}assets/posts/frontend/2021/07/052-qb-gap1-sm.png

[052-qb-gap2-sm]:       {{< baseurl >}}assets/posts/frontend/2021/07/052-qb-gap2-sm.png
[052-qb-gap2-md]:       {{< baseurl >}}assets/posts/frontend/2021/07/052-qb-gap2-md.png
[052-qb-gap2-md-dark]:  {{< baseurl >}}assets/posts/frontend/2021/07/052-qb-gap2-md-dark.png
[053-qb-gap-md]:        {{< baseurl >}}assets/posts/frontend/2021/07/053-qb-gap-md.png

[053-qb-gap-md-dark]:   {{< baseurl >}}assets/posts/frontend/2021/07/053-qb-gap-md-dark.png

[054-qb-landing]:       {{< baseurl >}}assets/posts/frontend/2021/07/054-qb-landing.png
[054-qb-landing-dark]:  {{< baseurl >}}assets/posts/frontend/2021/07/054-qb-landing-dark.png

[//]: <> ( -- -- -- links below -- -- -- )

[css-main]:             {{< tutor-html-bs5 >}}/step-05/assets/css/main.css
[scss-layout-content]:  {{< tutor-html-bs5 >}}/step-05/sass/css/main/_layout-content.scss

[njk-c-052-main]:       {{< tutor-html-bs5 >}}/step-05/views/contents/052-main.njk

[njk-c-053-article]:    {{< tutor-html-bs5 >}}/step-05/views/contents/053-article.njk
[njk-c-053-main]:       {{< tutor-html-bs5 >}}/step-05/views/contents/053-main.njk

[njk-c-054-article]:    {{< tutor-html-bs5 >}}/step-05/views/contents/054-article.njk
[njk-c-054-main]:       {{< tutor-html-bs5 >}}/step-05/views/contents/054-main.njk