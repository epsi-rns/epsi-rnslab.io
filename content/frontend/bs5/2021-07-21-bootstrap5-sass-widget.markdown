---
type   : post
title  : "Bootstrap - Sass - Widget"
date   : 2021-07-21T09:17:35+07:00
slug   : bootstrap5-sass-widget
categories: [frontend]
tags      : [bootstrap, sass]
keywords  : [panel, widget, list ,tags]
author : epsi
opengraph:
  image: assets/posts/frontend/2021/07/65-qb-tags-sm.png

toc    : "toc-2021-06-html-bootstrap5-step"

excerpt:
  Custom theme using Google Material Color, step by step.
  Building multicolor widget with flat design.

---

### Preface

> Step Six: Building multicolor widget with flat design.

We can use blog post as an easy example of web page.
This blog post is usually consist of main content and aside.
In aside elements, you can put a bunch of widgets over there.
So what we need to is focusing on development of this various widgets.

{{< embed-video width="480" height="480"
    src="assets/posts/frontend/2021/07/065-bootstrap5-tags.mp4" >}}

-- -- --

### 1: Chapter Overview

Since I'm not a web designer, I rarely make my own theme.
I simply don't have the sense of art to make the mockup with inkscape.
So here is what I do, I make a pure css theme.
One of them is this flat design, featuring multicolored widget.

#### Source Code: Step-06

The source code for this chapter is,
available for download at the following link::

* [gitlab.com/.../step-06/][tutor-step-06]

#### Related Articles

The obsolete Bootstrap v4 article:

* [Bootstrap OC - Sass - Widget][local-bootstrap-v4]

#### Responsive Requirement

This article is not a bootstrap tutorial anymore,
but more like a custom theme tutorial.
Even though the page based on bootstrap.

> Custom Class

Yes. I have to do it myself. And it is fun.

#### Multicolor Requirement

> Palette

We can either use Google Materialize Color, or Open Color.

#### SASS Directory Structure

I believe it's unnecessary to provide
an in-depth analysis of every file,
as the code is available for your examination
on my GitHub repository.

The only new file is the `list.scss`.

* [gitlab.com/.../step-06/sass/css/][tutor-step-06-sass]

{{< highlight bash >}}
❯ tree -C sass/css
sass/css
├── bootstrap.scss
├── helper-dark.scss
├── helper.scss
├── main
│   ├── _decoration-dark.scss
│   ├── _decoration.scss
│   ├── _layout-content.scss
│   ├── _layout-page.scss
│   ├── _list.scss
│   ├── _logo.scss
│   └── _sticky-footer.scss
├── main-dark.scss
├── main.scss
├── materialize
│   ├── _color-classes.scss
│   ├── _color-variables.scss
│   ├── _glow.scss
│   └── _shadow.scss
└── _variables.scss

3 directories, 17 files
{{< / highlight >}}

![Bootstrap SASS: SASS Directory Structure][060-tree-sass-css]

#### Nunjucks Directory Structure

Rather than providing
an exhaustive analysis of each file,
I invite you to inspect the code yourself
on my GitHub repository.

* [gitlab.com/.../step-06/views/][tutor-step-06-views]

{{< highlight bash >}}
❯ tree -C views
views
├── 061-responsive.njk
├── 062-blog.njk
├── 063-widget.njk
├── 064-list.njk
├── 065-tags.njk
├── aside
│   ├── 061-aside.njk
│   ├── 062-aside.njk
│   ├── 063-aside.njk
│   ├── 064-aside.njk
│   ├── 065-widget-01.njk
│   └── 065-widget-02.njk
├── contents
│   ├── 061-main.njk
│   ├── 062-main.njk
│   ├── 063-main.njk
│   ├── 064-main.njk
│   └── 065-main.njk
├── heads
│   ├── links.njk
│   └── meta.njk
├── layouts
│   └── base.njk
└── shared
    ├── footer.njk
    ├── navbar-button.njk
    ├── navbar-collapse.njk
    ├── navbar-dark.njk
    ├── navbar-dropdown.njk
    └── navbar.njk

6 directories, 25 files
{{< / highlight >}}

![Bootstrap SASS: Views Directory Structure][060-tree-views]

#### Blocks

> Nunjucks Document Source

The basic template is similar with previous chapter,
except the `content block` that contain two columns.
We have `main` and `aside` differently for each page.

* [gitlab.com/.../views/061-responsive.njk][njk--061-responsive]

{{< highlight jinja >}}
{% block content %}
  <!-- responsive colored main layout -->
  <div class="row layout-base maxwidth">
    {% include './contents/061-main.njk' %}
    {% include './aside/061-aside.njk' %}
  </div>
{% endblock %}
{{< / highlight >}}

![Bootstrap5: Nunjucks: Blocks: Responsive][061-v-responsive]

-- -- --

### 2: Responsive Gap

Let's take a step forward from our previous chapter
and explore multicolored layouts.

#### Stylesheet

To begin with, we can add padding to our existing layout
to create a simple multicolor design.

* [gitlab.com/.../sass/css/main/_layout-content.scss][scss-layout-content]

{{< highlight scss >}}
// layout

.main-wrapper,
.aside-wrapper {
  padding: 0 10px 0 10px;
}
{{< / highlight >}}

![Bootstrap SASS: Custom SASS for Main and Aside][061-scss-layout-cont]

#### Main and Aside

The basic layout is the same as previous,
except with additional colors.

* [gitlab.com/.../views/contents/061-main.njk][njk-c-061-main]

{{< highlight html >}}
<main class="col-sm-8 px-0">
  <section class="main-wrapper blue">
    <div class="white z-depth-3 hoverable">
      Page Content
    </div>
  </section>
</main>
{{< / highlight >}}

While the aside is

* [gitlab.com/.../views/aside/061-aside.njk][njk-c-061-aside]

{{< highlight html >}}
<aside class="col-sm-4 px-0">
  <section class="aside-wrapper green">
    <div class="white z-depth-3 hoverable">
      Side Menu
    </div>
  </section>
</aside>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Contents: Main Aside][061-c-main-aside]

#### Preview on Browser

On portrait mobile phone screens, the layout appears as follows:

![Bootstrap5: Responsive Multicolor in Mobile Screen][061-qb-responsive-xs]

The layout on landscape mobile phone  screens is illustrated in the following image:

![Bootstrap5: Responsive Multicolor in Desktop][061-qb-responsive-sm]

The image below demonstrates my experimentation with glowing dark mode:

![Bootstrap5: Responsive Multicolor in Desktop: Dark][061-qb-responsive-smd]

I must admit that the dark mode is not good.
But I jump into dark mode anyway.
So I can customize when I have time.

-- -- --

### 3: Blog Main and Aside

From simple layout, we can go further to blog layout.

#### HTML Content

Consider give a horizontal padding `px-2` to a content,
and see how it looks.

* [gitlab.com/.../views/contents/062-main.njk][njk-c-062-main]

{{< highlight html >}}
<main class="col-sm-8 px-0">
  <section class="main-wrapper blue">
    <div class="blog px-2 white
        z-depth-3 hoverable">
      <article>
        <h4>Cause You're Different!</h4>
        <p>When you can do the things
          that I can, but you don't, 
          and then the bad things happen,
          they happen because of you.</p>
      </article>
    </div>
  </section>
</main>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Contents: Main][062-c-main]

While the aside is

* [gitlab.com/.../views/aside/062-aside.njk][njk-c-062-aside]

{{< highlight html >}}

<aside class="col-sm-4 px-0">
  <section class="aside-wrapper green">
    <div class="widget px-2 white
        z-depth-3 hoverable">
      <p>How many times has he been,
      betrayed, disavowed, cast aside?</p>
    </div>
  </section>

  <section class="aside-wrapper teal">
    <div class="widget px-2 white
        z-depth-3 hoverable">
      <p>And how long before a man
        like that has had enough?</p>
    </div>
  </section>
</aside>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Contents: side][062-a-aside]

On portrait mobile phone, the layout appears as follows:

![Bootstrap5: Responsive Multicolor in Desktop Screen][062-qb-blog-smc]

#### Stylesheet

The very basic part of this multicolor is,
adding padding to our previous layout.

* [gitlab.com/.../sass/css/main/_layout-content.scss][scss-layout-content]

{{< highlight scss >}}
// gap main and aside

.blog {
  padding: 0px;
}
.blog-header {
  padding: 5px 10px 5px 10px;
}
.blog-body {
  padding: 5px 10px 5px 10px;
}
{{< / highlight >}}

![Bootstrap SASS: Custom SASS for Widget][062-scss-lc-blog]

And the vertical gap between widget can be solved with `:last-child`.

{{< highlight scss >}}
// responsive

.main-wrapper,
.aside-wrapper {
  margin-bottom: 20px;
}

.aside-wrapper:last-child {
  margin-bottom: 0px;
}
{{< / highlight >}}

![Bootstrap SASS: Custom SASS for Widget Wrapper][062-scss-lc-resp]

#### Preview on Browser

The following image displays how the layout appears on portrait mobile phone screen:

![Bootstrap5: Responsive Multicolor in Mobile Screen][062-qb-blog-xs]

The following image showcases how the layout appears on landscape mobile phone screen:

![Bootstrap5: Responsive Multicolor in Desktop][062-qb-blog-sm]

As can be seen in the following image, I have also experimented with dark mode:

![Bootstrap5: Responsive Multicolor in Desktop: Dark][062-qb-blog-sm-dark]

-- -- --

### 4: Blog Header and Footer

Consider separate design in each multicolor section,
so that each have their own `header` and `footer`.

#### Stylesheet

The very basic part of this multicolor is,
adding padding to our previous layout.

* [gitlab.com/.../sass/css/main/_layout-content.scss][scss-layout-content]

{{< highlight scss >}}
// gap main and aside

.blog {
  padding: 0px;
}
.blog-header {
  padding: 5px 10px 5px 10px;
}
.blog-body {
  padding: 5px 10px 5px 10px;
}
.widget {
  padding: 0px;
}
.widget-header {
  padding: 5px 10px 5px 10px;
}
.widget-body {
  padding: 5px 10px 5px 10px;
}
{{< / highlight >}}

![Bootstrap SASS: Custom SASS for Widget][062-scss-lc-widget]

#### HTML Skeleton

We have many additional `element` layer here.
Each `blog` and the `widget`, has `header` and `body`.

* Blog: `main.col > section.main-wrapper > div.blog > article.blog-body`

{{< highlight html >}}
    <main class="col-md-8 px-0">
      <section class="main-wrapper blue">
        <div class="blog white z-depth-3 hoverable">

          <section class="blog-header blue lighten-5">
            ...
          </section>

          <article class="blog-body">
            ...
          </article>

        </div>
      </section>
    </main>
{{< / highlight >}}

* Widget: `aside.col > section.aside-wrapper > div.widget > div.widget-body`

{{< highlight html >}}
    <aside class="col-md-4 px-0">   
      <section class="aside-wrapper green">
        <div class="widget white z-depth-3 hoverable">

          <div class="widget-header green lighten-5">
            ...
          </div>

          <div class="widget-body">
            ...
          </div>

        </div>
      </section>
    </aside>
{{< / highlight >}}

The compiled HTML can be found here:

* [gitlab.com/.../step-06/063-widget.html][html-063-widget]

#### HTML Content

Consider give a horizontal padding `px-2` to a content,
and see how it looks.

* [gitlab.com/.../views/contents/063-main.njk][njk-c-063-main]

{{< highlight html >}}

<main class="col-sm-8 px-0">
  <section class="main-wrapper blue">
    <div class="blog white z-depth-3 hoverable">
      <section class="blog-header
          blue lighten-5">
        <h4 itemprop="name headline">
          <a href="#">Cause You're Different!</a></h4>
      </section>

      <article
          class="blog-body"
          itemprop="articleBody">
        <p>When you can do the things
          that I can, but you don't, 
          and then the bad things happen,
          they happen because of you.</p>
      </article>
    </div>
  </section>
</main>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Contents: Main][063-c-main]

While the aside is

* [gitlab.com/.../views/aside/063-aside.njk][njk-c-063-aside]

{{< highlight html >}}

<aside class="col-sm-4 px-0">   
  <section class="aside-wrapper green">
    <div class="widget white z-depth-3 hoverable">

      <div class="widget-header green lighten-5">
        <strong>How many times ?</strong>
        <span class="fa fa-child
          float-end"></span>
      </div>

      <div class="widget-body">
        <p>Has he been, betrayed,
          disavowed, cast aside.</p>
      </div>

    </div>
  </section>

  <section class="aside-wrapper teal">
    <div class="widget white z-depth-3 hoverable">

      <div class="widget-header teal lighten-5">
        <strong>And how long?</strong>
        <span class="fas fa-fingerprint
          float-end"></span>
      </div>

      <div class="widget-body">
        <p>Before a man like that
          has had enough.</p>
      </div>

    </div>
  </section>
</aside>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Contents: side][063-a-aside]

#### Preview on Browser

On portrait mobile phone screen, the layout appears as follows:

![Bootstrap5: Responsive Multicolor in Mobile Screen][063-qb-widget-xs]

The layout on landscape mobile phone screen is illustrated in the following image:

![Bootstrap5: Responsive Multicolor in Desktop][063-qb-widget-sm]

The image below demonstrates my experimentation with glowing dark mode:

![Bootstrap5: Responsive Multicolor in Desktop: Dark][063-qb-widget-sm-dark]

-- -- --

### 5: List Widget

I have four blogs, and each blog contain nine kind of widgets.
These two are the basic most widget.

* List

* Tags

Consider begin with List.

#### Stylesheet

I must admit I have difficulties with `SVG`.
Using font such as `FontAwesome` is easier with `CSS`.
My workaround is grab the `chevron-right.svg` from feathericons.
Then well.... just resize the icon to suit widget looks.
Resize and changing color is very easy with inkscape.
I just need one for list icon anyway.

* [gitlab.com/.../sass/css/main/_list.scss][scss-list]

{{< highlight scss >}}
ul.widget-list li:hover {
  background: map-get($yellow, 'lighten-2');
}

ul.widget-list {
  padding: 0px;
}

ul.widget-list {
  padding-left: 0;
  list-style: none;
  font-size: 13px;
  margin-left: 20px;
}

ul.widget-list li:before {
  font-family: "Font Awesome 5 Free";
  font-weight: 900;
  content: "\f054";
  color: map-get($grey, 'base');
  margin: 0 5px 0 -12px;
}

ul.widget-list li:hover:before {
  color: map-get($yellow, 'base');
}
{{< / highlight >}}

![Bootstrap SASS: Custom SASS for List][064-scss-list]

`map-get` is `sass` language.

* [sass-lang.com/documentation/values/maps](https://sass-lang.com/documentation/values/maps)__.__

Do not forget to alter the `main.scss`.

* [gitlab.com/.../sass/css/main.scss][scss-main]

{{< highlight scss >}}
@import 
  ...

  // custom
  "main/layout-page",
  "main/layout-content",
  ...
  "main/list"
;
{{< / highlight >}}

![Bootstrap SASS: Main Custom SASS][064-scss-main]

#### HTML Content

Consider this list widget content.

* [gitlab.com/.../views/aside/064-aside.njk][njk-c-064-aside]

{{< highlight html >}}

<aside class="col-sm-4 px-0">   
  <section class="aside-wrapper green">
    <div class="widget white z-depth-3 hoverable">

      <div class="widget-header green lighten-5">
        <strong>Isle of Friends</strong>
        <span class="fa fa-child
          is-pulled-right"></span>
      </div>

      <div class="widget-body">
        <ul class="widget-list">
          <li><a href="http://epsi-rns.github.io/"
            >Linux/BSD Desktop Customization
            </a></li>
          <li><a href="http://epsi-rns.gitlab.io/"
            >Web Development Blog
            </a></li>
          <li><a href="http://oto-spies.info/"
            >Car Painting and Body Repair.
            </a></li>
        </ul>
      </div>

    </div>
  </section>
</aside>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Contents: side][064-a-aside]

#### Preview on Browser

The following image displays how the layout appears on portrait mobile phone screen:

![Bootstrap5: List Widget in Mobile Screen][064-qb-list-xs]

The following image showcases how the layout appears on landscape mobile phone screen:

![Bootstrap5: List Widget in Desktop][064-qb-list-sm]

As can be seen in the following image, I have also experimented with dark mode:

![Bootstrap5: List Widget in Desktop: Dark][064-qb-list-sm-dark]

-- -- --

### 6: Tags Widget

Other basic widget example is the `tags` widget.

#### Stylesheet

No need additional stylesheet.
We can use ready to use bootstrap class,
and also the fontawesome icon.

#### Blocks

> Nunjucks Document Source

For this demo, I also refactor the HTML element, for further use.
So we need a different content blocks.

* [gitlab.com/.../views/065-tags.njk][njk--065-tags]

{{< highlight jinja >}}
{% block content %}
  <!-- responsive colored main layout -->
  <div class="row layout-base maxwidth">
    <main class="col-sm-8 px-0">
      {% include './contents/065-main.njk' %}
    </main>

    <aside class="col-sm-4 px-0">
      {% include './aside/065-widget-01.njk' %}
      {% include './aside/065-widget-02.njk' %}
    </aside>
  </div>
{% endblock %}
{{< / highlight >}}

![Bootstrap5: Nunjucks: Blocks: Responsive][065-njk-tags]

#### HTML Content

Consider this list widget content.

* [gitlab.com/.../views/aside/065-widget-01.njk][njk-c-065-widget-01]

{{< highlight html >}}

<section class="aside-wrapper cyan">
  <div class="widget white z-depth-3 hoverable">

    <div class="widget-header cyan lighten-4">
      <strong>Categories</strong>
      <span class="fa fa-folder
        is-pulled-right"></span>
    </div>

    <div class="widget-body">
      <a href="#">
        <span class="badge text-dark
          cyan lighten-3 z-depth-1 hoverable">
        mission&nbsp;
        <span class="fa fa-folder"></span>
      </span></a>&nbsp;
      <a href="#">
        <span class="badge text-dark
          cyan lighten-3 z-depth-1 hoverable">
        accomplished&nbsp;
        <span class="fa fa-folder"></span>
      </span></a>&nbsp;
    </div>

  </div>
</section>
{{< / highlight >}}

![Bootstrap SASS: Custom SASS for Widget][065-njk-a-widget-01]

And also the second widget.

* [gitlab.com/.../views/aside/065-widget-02.njk][njk-c-065-widget-02]

{{< highlight html >}}

<section class="aside-wrapper teal">
  <div class="widget white z-depth-3 hoverable">

    <div class="widget-header teal lighten-4">
      <strong>Tags</strong>
      <span class="fa fa-tag
        is-pulled-right"></span>
    </div>

    <div class="widget-body">
      <a href="#">
        <span class="badge text-dark
          white z-depth-1 hoverable">
        command&nbsp;
        <span class="fa fa-folder"></span>
      </span></a>&nbsp;
      <a href="#">
        <span class="badge text-dark
          white z-depth-1 hoverable">
        agent&nbsp;
        <span class="fa fa-folder"></span>
      </span></a>&nbsp;
      <a href="#">
        <span class="badge text-dark
          white z-depth-1 hoverable">
        duty&nbsp;
        <span class="fa fa-folder"></span>
      </span></a>&nbsp;
    </div>

  </div>
</section>
{{< / highlight >}}

![Bootstrap SASS: Custom SASS for Widget][065-njk-a-widget-02]

#### Preview on Browser

In portrait mobile phone screen it is simply as below:

![Bootstrap5: Tags Widget in Mobile Screen][065-qb-tags-xs]

While in landscape mobile phone, it is as shown as below:

![Bootstrap5: Tags Widget in Desktop][065-qb-tags-sm]

And again also experiment with glowing dark mode.

![Bootstrap5: Tags Widget in Desktop: Dark][065-qb-tags-sm-dark]

I wish somebody could pointing me out to make nice looking dark mode.

-- -- --

### What is Next 🤔?

The next part is about content, the blog post and its decoration.

Consider continue reading [ [Bootstrap - Sass - Blog Post][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2021/07/23/bootstrap5-sass-blog-post/

[local-bootstrap-v4]:   {{< baseurl >}}frontend/2020/03/13/bootstrap-oc-sass-widget/

[tutor-step-06]:        {{< tutor-html-bs5 >}}/step-06/
[tutor-step-06-views]:  {{< tutor-html-bs5 >}}/step-06/views/
[tutor-step-06-sass]:   {{< tutor-html-bs5 >}}/step-06/sass/css/

[//]: <> ( -- -- -- links below -- -- -- )

[060-tree-views]:       {{< baseurl >}}assets/posts/frontend/2021/07/060-tree-views.png
[060-tree-sass-css]:    {{< baseurl >}}assets/posts/frontend/2021/07/060-tree-sass-css.png

[061-scss-layout-cont]: {{< baseurl >}}assets/posts/frontend/2021/07/061-scss-layout-content.png
[062-scss-lc-blog]:     {{< baseurl >}}assets/posts/frontend/2021/07/062-scss-layout-content-blog.png
[062-scss-lc-widget]:   {{< baseurl >}}assets/posts/frontend/2021/07/062-scss-layout-content-widget.png
[062-scss-lc-resp]:     {{< baseurl >}}assets/posts/frontend/2021/07/062-scss-layout-content-responsive.png
[064-scss-list]:        {{< baseurl >}}assets/posts/frontend/2021/07/064-scss-list.png
[064-scss-main]:        {{< baseurl >}}assets/posts/frontend/2021/07/064-scss-main.png

[061-v-responsive]:     {{< baseurl >}}assets/posts/frontend/2021/07/061-v-responsive.png

[061-c-main-aside]:     {{< baseurl >}}assets/posts/frontend/2021/07/061-njk-c-main-aside.png
[062-c-main]:           {{< baseurl >}}assets/posts/frontend/2021/07/062-njk-c-main.png
[062-a-aside]:          {{< baseurl >}}assets/posts/frontend/2021/07/062-njk-a-aside.png

[063-c-main]:           {{< baseurl >}}assets/posts/frontend/2021/07/063-njk-c-main.png
[063-a-aside]:          {{< baseurl >}}assets/posts/frontend/2021/07/063-njk-a-aside.png

[064-a-aside]:          {{< baseurl >}}assets/posts/frontend/2021/07/064-njk-a-aside.png

[065-njk-a-widget-01]:  {{< baseurl >}}assets/posts/frontend/2021/07/065-njk-a-widget-01.png
[065-njk-a-widget-02]:  {{< baseurl >}}assets/posts/frontend/2021/07/065-njk-a-widget-02.png
[065-njk-tags]:         {{< baseurl >}}assets/posts/frontend/2021/07/065-njk-tags.png

[061-qb-responsive-xs]: {{< baseurl >}}assets/posts/frontend/2021/07/61-qb-responsive-xs.png
[061-qb-responsive-sm]: {{< baseurl >}}assets/posts/frontend/2021/07/61-qb-responsive-sm.png
[061-qb-responsive-smd]:{{< baseurl >}}assets/posts/frontend/2021/07/61-qb-responsive-sm-dark.png
[062-qb-blog-xs]:       {{< baseurl >}}assets/posts/frontend/2021/07/062-qb-blog-xs.png
[062-qb-blog-sm]:       {{< baseurl >}}assets/posts/frontend/2021/07/062-qb-blog-sm.png
[062-qb-blog-smc]:      {{< baseurl >}}assets/posts/frontend/2021/07/062-qb-blog-smc.png
[062-qb-blog-sm-dark]:  {{< baseurl >}}assets/posts/frontend/2021/07/062-qb-blog-sm-dark.png
[063-qb-widget-xs]:     {{< baseurl >}}assets/posts/frontend/2021/07/063-qb-widget-xs.png
[063-qb-widget-sm]:     {{< baseurl >}}assets/posts/frontend/2021/07/063-qb-widget-sm.png
[063-qb-widget-sm-dark]:{{< baseurl >}}assets/posts/frontend/2021/07/063-qb-widget-sm-dark.png
[064-qb-list-xs]:       {{< baseurl >}}assets/posts/frontend/2021/07/064-qb-list-xs.png
[064-qb-list-sm]:       {{< baseurl >}}assets/posts/frontend/2021/07/064-qb-list-sm.png
[064-qb-list-sm-dark]:  {{< baseurl >}}assets/posts/frontend/2021/07/064-qb-list-sm-dark.png
[065-qb-tags-xs]:       {{< baseurl >}}assets/posts/frontend/2021/07/065-qb-tags-xs.png
[065-qb-tags-sm]:       {{< baseurl >}}assets/posts/frontend/2021/07/065-qb-tags-sm.png
[065-qb-tags-sm-dark]:  {{< baseurl >}}assets/posts/frontend/2021/07/065-qb-tags-sm-dark.png

[//]: <> ( -- -- -- links below -- -- -- )

[scss-layout-content]:  {{< tutor-html-bs5 >}}/step-06/sass/css/main/_layout-content.scss
[scss-list]:            {{< tutor-html-bs5 >}}/step-06/sass/css/main/_list.scss
[scss-main]:            {{< tutor-html-bs5 >}}/step-06/sass/css/main.scss

[njk--061-responsive]:  {{< tutor-html-bs5 >}}/step-06/views/061-responsive.njk
[html-061-responsive]:  {{< tutor-html-bs5 >}}/step-06/061-responsive.html

[njk--063-widget]:      {{< tutor-html-bs5 >}}/step-06/views/063-widget.njk
[html-063-widget]:      {{< tutor-html-bs5 >}}/step-06/063-widget.html

[njk--065-tags]:        {{< tutor-html-bs5 >}}/step-06/065-tags.njk

[njk-c-061-main]:       {{< tutor-html-bs5 >}}/step-06/views/contents/061-main.njk
[njk-c-062-main]:       {{< tutor-html-bs5 >}}/step-06/views/contents/062-main.njk
[njk-c-063-main]:       {{< tutor-html-bs5 >}}/step-06/views/contents/063-main.njk
[njk-c-064-main]:       {{< tutor-html-bs5 >}}/step-06/views/contents/064-main.njk
[njk-c-065-main]:       {{< tutor-html-bs5 >}}/step-06/views/contents/065-main.njk

[njk-c-061-aside]:       {{< tutor-html-bs5 >}}/step-06/views/aside/061-aside.njk
[njk-c-062-aside]:       {{< tutor-html-bs5 >}}/step-06/views/aside/062-aside.njk
[njk-c-063-aside]:       {{< tutor-html-bs5 >}}/step-06/views/aside/063-aside.njk
[njk-c-064-aside]:       {{< tutor-html-bs5 >}}/step-06/views/aside/064-aside.njk
[njk-c-065-widget-01]:   {{< tutor-html-bs5 >}}/step-06/views/aside/065-widget-01.njk
[njk-c-065-widget-02]:   {{< tutor-html-bs5 >}}/step-06/views/aside/065-widget-02.njk

