---
type   : post
title  : "Bootstrap - Sass - Blog Post"
date   : 2021-07-23T09:17:35+07:00
slug   : bootstrap5-sass-blog-post
categories: [frontend]
tags      : [bootstrap, sass]
keywords  : [post header, post navigation, landing page]
author : epsi
opengraph:
  image: assets/posts/frontend/2021/06/001-preview-07.png

toc    : "toc-2021-06-html-bootstrap5-step"

excerpt:
  Custom theme using Google Material Color, step by step.
  Prepare common blog post theme for use later with framework.

---

### Preface

> Step Seven: Prepare common blog post theme for use later with framework.

This chapter is focusing on parts of blog post.

{{< embed-video width="480" height="480"
    src="assets/posts/frontend/2021/07/072-post-navigation.mp4" >}}

-- -- --

### 1: Chapter Overview

This chapter also summarize what we have done so far.

#### Source Code: Step-07

You can access the source code for this chapter
by clicking on the following link:

* [gitlab.com/.../step-07/][tutor-step-07]

#### Related Articles

The obsolete Bootstrap v4 article:

* [Bootstrap OC - Sass - Blog Post][local-bootstrap-v4]

#### SASS Directory Structure

We need a new `SASS` section for this `post`.

* [gitlab.com/.../step-07/sass/css/][tutor-step-07-sass]

{{< highlight bash >}}
❯ tree -C css
css
├── bootstrap.scss
├── helper.scss
├── main
│   ├── _decoration.scss
│   ├── _layout-content.scss
│   ├── _layout-page.scss
│   ├── _list.scss
│   ├── _logo.scss
│   └── _sticky-footer.scss
├── main.scss
├── materialize
│   ├── _color-classes.scss
│   ├── _color-variables.scss
│   └── _shadow.scss
├── post
│   ├── _content.scss
│   └── _navigation.scss
└── _variables.scss

4 directories, 15 files
{{< / highlight >}}

This only require two new file artefact.

* _content.scss

* _navigation.scss

![Bootstrap5: SASS Directory Structure][070-tree-sass-css]

In real world blogging,
this post section has a more `SASS` stylesheet document.
I have about five or seven, depend on the framework that I choose.

#### Main SASS

Of course you have to update your main SASS to reflect the above

* [gitlab.com/.../sass/css/main.scss][scss-main]

{{< highlight scss >}}
// Import partials from `sass_dir` (defaults to `sass/css`)
@import 
  // Bootstrap Related
  "../bootstrap/functions",
  "variables",
  "../bootstrap/variables",
  "../bootstrap/mixins/breakpoints",

  // Heeyeun's Open Color
  "open-color/_open-color-variables",

  // Tailor Made
  "main/layout-page",
  "main/layout-content",
  "main/logo",
  "main/decoration",
  "main/sticky-footer",
  "main/list",
  
  "post/content",
  "post/navigation"
;
{{< / highlight >}}

![Bootstrap SASS: NERDTree and Main Custom SASS][070-scss-main]

#### Nunjucks Directory Structure

Luckily for this chapter, we have common base layout.

I invite you to inspect the code yourself
on my GitHub repository.

* [gitlab.com/.../step-07/views/][tutor-step-07-views]

{{< highlight bash >}}
❯ tree -C views
views
├── 071-post-header.njk
├── 072-post-navigation.njk
├── 073-landing-page.njk
├── 074-javascript-toggler.njk
├── aside
│   ├── 071-aside.njk
│   └── 074-aside.njk
├── blog
│   ├── 071-badge.njk
│   ├── 072-badge.njk
│   ├── 072-navigation.njk
│   └── 074-toggler.njk
├── contents
│   ├── 071-main.njk
│   ├── 072-main.njk
│   ├── 073-cover.njk
│   ├── 073-main.njk
│   └── 074-main.njk
├── heads
│   ├── links.njk
│   └── meta.njk
├── layouts
│   └── base.njk
└── shared
    ├── 074-footer.njk
    ├── 074-navbar.njk
    ├── footer.njk
    ├── navbar-button.njk
    ├── navbar-collapse.njk
    ├── navbar-dropdown.njk
    └── navbar.njk

7 directories, 25 files
{{< / highlight >}}

![Bootstrap SASS: Views Directory Structure][071-nerd-post-header]

-- -- --

### 2: Header

Blog post header customization is very common.
I utilize badge (or tag).
Luckily most CSS framework,
has built in support to display tag.

#### Stylesheet

* [gitlab.com/.../sass/css/post/_content.scss][scss-post-content]

{{< highlight scss >}}
h1, h2, h3, h4, h5, h6 {
  font-family: "Playfair Display", Georgia, "Times New Roman", serif;
}

/*
 * Blog posts
 */

.blog-body {
  margin-bottom: 1rem;
}
.blog-header {
  h1, h2, h3, h4, h5, h6 {
    margin-bottom: .25rem;
    a {
      color: map-get($grey, "darken-4");
    }
    a:hover {
      color: map-get($grey, "darken-1") !important;
    }
  }
}
.blog-header .meta {
  margin-bottom: 2.0rem;
}
.blog-body p {
  margin-top: 0.5rem;
  margin-bottom: 0.5rem;
}
{{< / highlight >}}

![Bootstrap SASS: Custom SASS for Post Content][071-scss-post-content]

#### Nunjucks: Main

The fundamental structure of the layout remains consistent
with the code from the previous chapter.
Inside that we have `blog-header` and `blog-body`.

* [gitlab.com/.../views/contents/071-main.njk][njk-c-071-main]

{{< highlight html >}}
<main class="col-sm-8 px-0">
  <section class="main-wrapper blue">
    <div class="blog white z-depth-3 hoverable">
      <section class="blog-header blue lighten-5">
        ...
      </section>

      <article class="blog-body"
          itemprop="articleBody">
        ...
      </article>
    </div>
  </section>
</main>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Contents: Main: Outer][071-c-main-01]

Inside that we have `blog-header` and `blog-body`.

{{< highlight html >}}
      <section class="blog-header blue lighten-5">
        <h4 class="font-weight-bold"
            itemprop="name headline">
          <a href="#">Cause You're Different!</a></h4>

        {% include '../blog/071-badge.njk' %}
      </section>

      <article class="blog-body" itemprop="articleBody">
        <p>When you can do the things
          that I can, but you don't, 
          and then the bad things happen,
          they happen because of you.</p>
      </article>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Contents: Main: Inner][071-c-main-02]

In `blog-header` element, there is this badge template.

#### Badge Template

I know this template is long.
this is just a demo of tag elements,
along with FontAwesome icons.

* [gitlab.com/.../views/blog/071-badge.njk][njk-b-071-badge]

{{< highlight html >}}
<div class="clearfix"></div>

<div class="field meta">
  <div class="float-start">
  <span class="meta_author badge indigo darken-2
                white-text z-depth-1">
      <span class="fa fa-user"></span>
      &nbsp;
      <span itemprop="author"
            itemscope
            itemtype="http://schema.org/Person">
      <span itemprop="name">epsi</span></span>
  </span>
  &nbsp;
  </div>

  <div class="float-start">
    <span class="meta-time badge cyan darken-2
            white-text z-depth-1">
      <span class="fas fa-clock"></span>
      &nbsp;A few days ago.
    </span>
    &nbsp;
  </div>

  <div class="float-end">
      <a href="#">
        <span class="badge teal darken-2
                white-text z-depth-1 hoverable"
          ><span class="fa fa-tag"></span>
          &nbsp;love</span></a>
    &nbsp;
  </div>

  <div class="float-end">
      <a href="#">
        <span class="badge blue darken-2
                white-text z-depth-1 hoverable"
          ><span class="fa fa-folder"></span>
          &nbsp;rock</span></a>
    &nbsp;
  </div>
</div>

<div class="clearfix"></div>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Blog Post: Badge][071-b-badge]

#### Preview on Browser

> So you can imagine how it looks.

In mobile screen it is simply as below:

![Bootstrap5: Badge Template in Mobile Screen][071-qb-post-header-xs]

While in small desktop, it is shown as below:

![Bootstrap5: Badge Template in Desktop][071-qb-post-header-sm]

That is the first step in blog post.

-- -- --

### 3: Navigation

Blog post navigation along with pagination customization,
is also very common.
And most CSS framework has built in support to navigation.

#### Stylesheet

Bootstrap navigation in bootstrap is harcoded to `pagination`.
You cannot rename class to `navigation`.

* [gitlab.com/.../sass/css/post/_navigation.scss][scss-post-navigation]

{{< highlight scss >}}
li.post-previous a:after {
  content: " previous";
}

li.post-next a:before {
    content: "next ";
}

.blog-body .pagination {
  margin-top: 1rem;
  margin-bottom: 1rem;
}
{{< / highlight >}}

![Bootstrap5: Custom SASS for Post Navigation][072-scss-post-navigat]

#### Main

We can just put the navigation anywhere in our blog post.
In my case I put the navigation below article in `blog-body`.

* [gitlab.com/.../views/contents/072-main.njk][njk-c-072-main]

{{< highlight html >}}
      <article class="blog-body"
          itemprop="articleBody">
        <p>When you can do the things
          that I can, but you don't, 
          and then the bad things happen,
          they happen because of you.</p>

        {% include '../blog/072-navigation.njk' %}
      </article>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Contents: Main][072-c-main]

#### Navigation Template

In `blog-body` element, there is this navigation template.
We are going to use bootstrap class here.

* [gitlab.com/.../views/blog/072-navigation.njk][njk-b-072-navigation]

{{< highlight html >}}
<ul class="pagination justify-content-between" 
    role="navigation"
    aria-labelledby="pagination-label">

  <li class="page-item post-previous">
    <a  class="page-link" 
        href="#" 
        title="next article name">&laquo; </a>
  </li>

  <li class="page-item post-next">
    <a  class="page-link" 
        href="#" 
        title="previous article name"> &raquo;</a>
  </li>

</ul>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Blog Post: Navigation][072-b-navigation]

#### Preview on Browser

> Standard pagination looks.

On portrait mobile phone display, the layout appears as follows:

![Bootstrap5: Blog Post Navigation in Mobile Screen][072-qb-post-nav-xs]

The layout on landscape mobile phone display is illustrated in the following image:

![Bootstrap5: Blog Post Navigation in Small Desktop][072-qb-post-nav-sm]

-- -- --

### 4: Landing Page

Landing page is not really blog post post.
I just put here just because I don't want to make new article.

Again, test our single page design, with a more complete landing page.

#### Blocks

> Nunjucks Document Source

We do not have aside here.
Only single column.

* [gitlab.com/.../views/073-landing-page.njk][njk-v-073-landing]

{{< highlight jinja >}}
{% block content %}
  <!-- responsive colored main layout -->
  <div class="row layout-base maxwidth">
    {% include './contents/073-cover.njk' %}
  </div>
{% endblock %}
{{< / highlight >}}

![Bootstrap5: Nunjucks: Blocks: Landing Page][073-v-landing-page]

#### Cover Template

Here is the aestethic wrapper for main template.

* [gitlab.com/.../views/contents/073-cover.njk][njk-c-073-cover]

{{< highlight html >}}
<main class="col px-0">
  <section class="main-wrapper single blue">
    <div class="blog white z-depth-3 hoverable">
      <section class="blog-header
          blue lighten-5 text-center">
        <h3 class="font-weight-bold" itemprop="name headline">
          Your Mission</h3>
        <h4 class="font-weight-normal">
          Should you decide to accept</h4>
      </section>

      <article
          class="blog-body text-center" itemprop="articleBody">
        {% include './073-main.njk' %}
      </article>
    </div>
  </section>
</main>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Contents: Cover][073-c-cover]

In `blog-body` element, there is this main landing page template.

#### Main Template

In `blog-body` element, there is this navigation template.

* [gitlab.com/.../views/contents/073-main.njk][njk-c-073-main]

{{< highlight html >}}
  <p>
    <a href="#" 
        class="btn btn-primary my-2
          z-depth-1 hoverable"
      >Articles Sorted by Month</a>
    <a href="#" 
        class="btn btn-secondary my-2
          z-depth-1 hoverable"
      >Articles Sorted by Tag</a>
  </p>

  <p>
    As always,
    should you be caught or killed,
    any knowledge of your actions
    will be disavowed.</p>

  <img src="assets/images/one-page.png" 
        alt="business card">

  <p class="text-muted">
    <small>
    <span class="fas fa-home"></span>&nbsp;
      Whitewood Street, Monday Market,
      East Jakarta, 55112, Indonesia.
    </small>
  </p>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Contents: Main][073-c-main]

#### Preview on Browser

The landing page on landscape mobile phone screen is illustrated in the following image:

![Bootstrap5: Landing Page in Small Desktop][073-qb-landing-sm]

I just keep this article simple.

-- -- --

### 5: Summary

As a summary, here is the final looks,
of our blog post in desktop screen.

![Bootstrap5: Blog Post Navigation in Desktop][072-qb-post-nav-md]

-- -- --

### What is Next 🤔?

The next part is a bonus.
It is more like a javacript tutorial,
which use a bunch of Bootstrap class.

Consider continue reading [ [Bootstrap - Javascript Toggler][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2021/07/25/bootstrap5-javascript-toggler/

[local-bootstrap-v4]:   {{< baseurl >}}frontend/2020/03/15/bootstrap-oc-sass-blog-post/

[tutor-step-07]:        {{< tutor-html-bs5 >}}/step-07/
[tutor-step-07-views]:  {{< tutor-html-bs5 >}}/step-07/views/
[tutor-step-07-sass]:   {{< tutor-html-bs5 >}}/step-07/sass/css/

[//]: <> ( -- -- -- links below -- -- -- )

[070-tree-sass-css]:    {{< baseurl >}}assets/posts/frontend/2021/07/070-tree-sass-css.png
[070-scss-main]:        {{< baseurl >}}assets/posts/frontend/2021/07/070-nerd-sass-main.png
[071-scss-post-content]:{{< baseurl >}}assets/posts/frontend/2021/07/071-scss-post-content.png
[072-scss-post-navigat]:{{< baseurl >}}assets/posts/frontend/2021/07/072-scss-post-navigation.png

[071-nerd-post-header]: {{< baseurl >}}assets/posts/frontend/2021/07/071-nerd-post-header.png

[073-v-landing-page]:   {{< baseurl >}}assets/posts/frontend/2021/07/073-v-landing-page.png

[071-b-badge]:          {{< baseurl >}}assets/posts/frontend/2021/07/071-njk-b-badge.png
[072-b-navigation]:     {{< baseurl >}}assets/posts/frontend/2021/07/072-njk-b-navigation.png

[071-c-main-01]:        {{< baseurl >}}assets/posts/frontend/2021/07/071-njk-c-main-01.png
[071-c-main-02]:        {{< baseurl >}}assets/posts/frontend/2021/07/071-njk-c-main-02.png
[072-c-main]:           {{< baseurl >}}assets/posts/frontend/2021/07/072-njk-c-main.png
[073-c-cover]:          {{< baseurl >}}assets/posts/frontend/2021/07/073-njk-c-cover.png
[073-c-main]:           {{< baseurl >}}assets/posts/frontend/2021/07/073-njk-c-main.png

[071-qb-post-header-xs]:{{< baseurl >}}assets/posts/frontend/2021/07/071-qb-post-header-xs.png
[071-qb-post-header-sm]:{{< baseurl >}}assets/posts/frontend/2021/07/071-qb-post-header-sm.png
[072-qb-post-nav-xs]:   {{< baseurl >}}assets/posts/frontend/2021/07/072-qb-post-nav-xs.png
[072-qb-post-nav-sm]:   {{< baseurl >}}assets/posts/frontend/2021/07/072-qb-post-nav-sm.png
[072-qb-post-nav-md]:   {{< baseurl >}}assets/posts/frontend/2021/07/072-qb-post-nav-md.png

[073-qb-landing-sm]:    {{< baseurl >}}assets/posts/frontend/2021/07/073-qb-landing-sm.png

[//]: <> ( -- -- -- links below -- -- -- )

[scss-main]:            {{< tutor-html-bs5 >}}/step-07/sass/css/main.scss
[scss-post-content]:    {{< tutor-html-bs5 >}}/step-07/sass/css/post/_content.scss
[scss-post-navigation]: {{< tutor-html-bs5 >}}/step-07/sass/css/post/_navigation.scss

[njk-v-073-landing]:    {{< tutor-html-bs5 >}}/step-07/views/073-landing-page.njk

[njk-b-071-badge]:      {{< tutor-html-bs5 >}}/step-07/views/blog/071-badge.njk
[njk-b-072-badge]:      {{< tutor-html-bs5 >}}/step-07/views/blog/072-badge.njk
[njk-b-072-navigation]: {{< tutor-html-bs5 >}}/step-07/views/blog/072-navigation.njk

[njk-c-071-main]:       {{< tutor-html-bs5 >}}/step-07/views/contents/071-main.njk
[njk-c-072-main]:       {{< tutor-html-bs5 >}}/step-07/views/contents/072-main.njk
[njk-c-073-cover]:      {{< tutor-html-bs5 >}}/step-07/views/contents/073-cover.njk
[njk-c-073-main]:       {{< tutor-html-bs5 >}}/step-07/views/contents/073-main.njk

