---
type   : post
title  : "Bootstrap - Javascript Toggler"
date   : 2021-07-25T09:17:35+07:00
slug   : bootstrap5-javascript-toggler
categories: [frontend]
tags      : [bootstrap, javascript]
keywords  : [toggler button]
author : epsi
opengraph:
  image: assets/posts/frontend/2021/07/074-qb-toggler-sm.png

toc    : "toc-2021-06-html-bootstrap5-step"

excerpt:
  Custom theme using Google Material Color, step by step.
  Toggle layout apperance with plain javascript.

---

### Preface

> Step Seven: Toggle layout apperance with plain javascript.

This is more like a javacript tutorial,
which use a bunch of Bootstrap class.

{{< embed-video width="540" height="270"
    src="assets/posts/frontend/2021/07/075-js-toggler.mp4" >}}

-- -- --

### 1: Chapter Overview

We have done a lot of, nunjucks templates, and also Sass.
This time we deal with javascript.

I have made two variations of this toggle javascript.
* For this article, I use `sm`.
* But for my real blog, I use `md` instead.

#### Source Code: Step-07

To obtain the source code for this chapter,
please follow the link provided below:

* [gitlab.com/.../step-07/][tutor-step-07]

#### Related Articles

The obsolete Bootstrap v4 article:

* [Bootstrap OC - Sass - Javascript Toggler][local-bootstrap-v4]

#### General Preview

This below is general preview of,
what javascript toggler page design that we want to achieve.

![Bulma SASS: General Preview of Javascript Toggler Design][png-layout-toggler]

Source image is available in inkscape SVG format,
so you can modify, and make your own preview quickly.

* [Javascript Toggler Page Design: Image Source][svg-layout-toggler]

#### Additional Assets

All we need just this one custom toggler javascript:

* [gitlab.com/.../step-07/js/][tutor-step-07-js]

{{< highlight bash >}}
❯ tree -C assets/js
assets/js
├── bootstrap.min.js
├── custom-toggler.js
└── popper.min.js

1 directory, 3 files
{{< / highlight >}}

![Bootstrap SASS: Javascript Directory Structure][074-tree-js]

Append this javascript in our HTML head.

* [gitlab.com/.../views/074-javascript-toggler.njk][njk-v-074-toggler]

{{< highlight jinja >}}
{% block htmlhead %}
  {% include './heads/meta.njk' %}
  {% include './heads/links.njk' %}
  <script src="assets/js/custom-toggler.js"></script>
{% endblock %}
{{< / highlight >}}

![Bootstrap5: Nunjucks: Blocks: Javascript Toggler][074-v-toggler-01]

-- -- --

### 2: Toggler Component

Sidebar design is siginficantly cool for short article,
but for long article this would make your appearance looks unbalanced.
Sidebar is necessary, but it takes space on your screen.
Talking about space real estate, sometimes we need to be more flexible,
while reading an article in a blog.
This means we can let user to decide how our layout arranged,
by in the same time we provide default layout.

Instead of changing the whole design layout alltogether,
this article show how we can achieve this without too much effort.

#### Two Buttons

Our toggler component consist of two buttons:

* Left Button:
  Release `maxwidth` class.
  Applied for tablet screen or beyond.

* Right Button:
  Hide Sidebar.
  Applied for tablet screen or beyond.

#### Stylesheet

The responsible stylesheet for this is:

* [gitlab.com/.../sass/css/main/_layout_page.scss][scss-layout-page]

{{< highlight scss >}}
@include media-breakpoint-up(md) {
   // Desktop
  .maxwidth {
    max-width: map-get($grid-breakpoints, "md");
    margin-right: auto;
    margin-left: auto;
  }
}
{{< / highlight >}}

![Bootstrap SASS: Custom SASS for Layout Page][074-scss-layout-page]

For wider maxwidth you can replace the `md` with `xl`.

#### Blocks

> Nunjucks Document Source

We need to specify `maxwidth-toggler` class on the element.

* [gitlab.com/.../views/074-javascript-toggler.njk][njk-v-074-toggler]

{{< highlight jinja >}}
{% block content %}
  <!-- responsive colored main layout -->
  <div class="row layout-base
              maxwidth maxwidth_toggler">
    {% include './contents/074-main.njk' %}
    {% include './aside/074-aside.njk' %}
  </div>
{% endblock %}
{{< / highlight >}}

![Bootstrap5: Nunjucks: Blocks: Javascript Toggler][074-v-toggler-02]

This class does not need any stylesheet.
We are going to use this class in javascript,
to manipulate visibility of some elements,
as we will see below.

#### Toggler Template

Our toggler component is simply a HTML document,
with a bunch Bootstrap class, and two awesome icons.

* [gitlab.com/.../views/blog/074-toggler.njk][njk-b-074-toggler]

{{< highlight html >}}
<!-- toggler -->
<section class="m-1 d-none d-sm-block">
  <button id="left_toggler"
          class="btn btn-sm btn-light
                  float-start text-dark
                  left_toggler_active">
    <span class="icon is-small">
      <i class="fas fa-angle-double-left"></i>
    </span>
  </button>
  <button id="right_toggler"
          class="btn btn-sm btn-light
                  float-end text-dark
                  right_toggler_active">
    <span class="icon is-small">
      <i class="fas fa-angle-double-right"></i>
    </span>
  </button>
  <div class="clearfix"></div>
</section>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Blog Post: Toggler][074-b-toggler]

You can use any icon set to suit your need,
such as Feather Icons, Bootstrap Icons, or FontAwesome.

![Bootstrap5: Blog Post: Toggler Component][074-qb-toggler-crop]

This is just an example.
Your are free working with your own design.

#### Tablet Only

Since toggler is set applied for tablet screen,
we can hide the toggler component in mobile screen.

{{< highlight html >}}
  <section class="m-1 d-none d-sm-block">
    ...
  </section>
{{< / highlight >}}

#### Button ID

We need to give each button an ID,
so that javascript can locate the chosen element.
We also need to give a class to detect its state.

Left Button: `left_toggler` ID with `left_toggler_active` class.

{{< highlight html >}}
    <button id="left_toggler"
            class="btn btn-sm btn-light
                   float-left text-dark
                   left_toggler_active">
        ...
    </button>
{{< / highlight >}}

Right Button: `right_toggler` ID with `right_toggler_active` class.

{{< highlight html >}}
    <button id="right_toggler"
            class="btn btn-sm btn-light
                   float-right text-dark
                   right_toggler_active">
        ...
    </button>
{{< / highlight >}}

For wider maxwidth you can replace the `sm` with `md`.

#### Javascript: Tailor Made

Our first javascript example should be simple.
This script will handle `onclick` event on both element,
in an unobstrusive fashioned.
The simplified version of the code can be shown below:

* [gitlab.com/.../assets/js/custom-toggler.js][js-custom-toggler]

{{< highlight javascript >}}
document.addEventListener("DOMContentLoaded", function(event) { 
  const leftToggler  = document.getElementById("left_toggler");
  const rightToggler = document.getElementById("right_toggler");

  leftToggler.onclick = function() {
    leftToggler.classList.toggle("left_toggler_active");
    return false;
  }

  rightToggler.onclick = function() {
    rightToggler.classList.toggle("right_toggler_active");
    return false;
  }
});
{{< / highlight >}}

Note that the real code will be longer,
with additional functionality.

![Bootstrap5: Custom Javascript Toggler][074-js-toggler-01]

The problem with custom scripting for specific design is,
the script should reflect the html tag from the design.
**Thus script cannot be portable**.

To port for your own design,
you need to change with a bit of adjustment.

#### Inspect Element

You can load the page, click each button,
and check whether, the `active` class is toggled.

![Bootstrap5: Inspect Toggle Active Class][074-inspect-toggler]

-- -- --

### 3: Toggling Icon

> Visual First

Both toggle icon should reflect the state,
whether `active` or `inactive`.
We can utilize different icon,
flip the each arrow.

#### Four State

Now we have four states.

* Left Right:

![Bootstrap5: Toggle: Left Right ][arrow-074-le-ri]

* Left Left:

![Bootstrap5: Toggle: Left Left  ][arrow-074-le-le]

* Right Left:

![Bootstrap5: Toggle: Right Left ][arrow-074-ri-le]

* Right Right:

![Bootstrap5: Toggle: Right Right][arrow-074-ri-ri]

#### Main Script

Consider add a few lines to handle `onclick` event.

{{< highlight javascript >}}
  const leftToggler  = document.getElementById("left_toggler");
  const rightToggler = document.getElementById("right_toggler");

  leftToggler.onclick = function() {
    leftToggler.classList.toggle("left_toggler_active"); 

    toggleLeftIcon();
    leftToggler.blur();

    return false;
  }

  rightToggler.onclick = function() {
    rightToggler.classList.toggle("right_toggler_active");

    toggleRightIcon();
    rightToggler.blur();

    return false;
  }
{{< / highlight >}}

At the end of each script, we are going to blur the focus,
so that the clicked element will get clean looks,
instead of a bordered icon.

#### Left Icon

This will flip AwesomeIcon from `chevrons-left.svg`
to `chevrons-right.svg`, and flip back on the next click.

{{< highlight javascript >}}
  function toggleLeftIcon() {
    const isActiveLeft = leftToggler.classList
      .contains("left_toggler_active");
   
    const leftIcon = leftToggler
      .getElementsByTagName("img")[0];

    if (isActiveLeft) {
      leftIcon.src = "assets/icons/chevrons-left.svg";
      console.log("left toggler class is active");
    } else {
      leftIcon.src = "assets/icons/chevrons-right.svg"
      console.log("left toggler class is inactive");
    }
  }
{{< / highlight >}}

#### Right Icon

On the opposite icon,
this will flip AwesomeIcon from `chevrons-right.svg`
to `chevrons-left.svg`, and flip back on the next click.

{{< highlight javascript >}}
  function toggleRightIcon() {
    const isActiveRight = rightToggler.classList
      .contains("right_toggler_active");

    const rightIcon = rightToggler
      .getElementsByTagName("img")[0];
  
    if (isActiveRight) {
      rightIcon.src = "assets/icons/chevrons-right.svg"      
      console.log("right toggler class is active");
    } else {
      rightIcon.src = "assets/icons/chevrons-left.svg"
      console.log("right toggler class is inactive");
    }
  }
{{< / highlight >}}

#### How does it works?

These functions rely on checking each state,
by using `isActiveLeft` or `isActiveRight`.

Left Icon:

{{< highlight javascript >}}
  function toggleLeftIcon() {
    const isActiveLeft = leftToggler.classList
      .contains("left_toggler_active");
    ...
    
    if (isActiveLeft) {
      ...
    } else {
      ...
    }
  }
{{< / highlight >}}

Right Icon:

{{< highlight javascript >}}
  function toggleRightIcon() {
    const isActiveRight = rightToggler.classList
      .contains("right_toggler_active");
    ...
  
    if (isActiveRight) {
      ...
    } else {
      ...
    }
  }
{{< / highlight >}}

#### Inspect Element

You can load the page, click each button,
and check the `console` tab.

![Bootstrap5: Toggle Console Log][074-inspect-console]

-- -- --

### 4: Toggling Layout Appearance

> This should be functional.

From icon appearance, we go further to explore, what each clicks does.
Ecah click would change the layout documents using Bootstrap class.

#### HTML Content

> Which Layout?

We need to alter a bit our layout,
give the respective element a marker class.

Maxwidth: `maxwidth_toggler`

* [gitlab.com/.../views/074-javascript-toggler.njk][njk-v-074-toggler]

{{< highlight jinja >}}
{% block content %}
  <!-- responsive colored main layout -->
  <div class="row layout-base
              maxwidth maxwidth_toggler">
    {% include './contents/074-main.njk' %}
    {% include './aside/074-aside.njk' %}
  </div>
{% endblock %}
{{< / highlight >}}

And so is the header:

* [gitlab.com/.../views/shared/074-navbar.njk][njk-s-074-navbar]

{{< highlight jinja >}}
  <!-- header -->
  <nav class="navbar navbar-expand-md navbar-light
      fixed-top maxwidth maxwidth_toggler
      white z-depth-3 hoverable">
    ...
  </nav>
{% endblock %}
{{< / highlight >}}

And also the footer:

* [gitlab.com/.../views/shared/074-footer.njk][njk-s-074-footer]

{{< highlight jinja >}}
{% block content %}
  <!-- footer -->
  <footer class="footer">
    <div class="text-dark text-center
                maxwidth maxwidth_toggler
                white z-depth-3 hoverable">
      &copy; 2021.
    </div>
  </footer>
{% endblock %}
{{< / highlight >}}

Sidebar: `main_toggler`, `main_toggler_wrapper`, and `aside_toggler`.

* [gitlab.com/.../views/contents/074-main.njk][njk-c-074-main]

{{< highlight html >}}
<main id="main_toggler"
      class="col-sm-8 px-0">
  <section id="main_toggler_wrapper"
            class="main-wrapper blue">
        ...
  </section>
</main>
{{< / highlight >}}

* [gitlab.com/.../views/aside/074-aside.njk][njk-a-074-aside]

{{< highlight html >}}
<aside id="aside_toggler" class="col-sm-4 px-0">
  <section class="aside-wrapper green">
      ...
  </section>
</aside>
{{< / highlight >}}

#### Main Script

Again, add more lines to handle `onclick` event.

{{< highlight javascript >}}
  const leftToggler  = document.getElementById("left_toggler");
  const rightToggler = document.getElementById("right_toggler");

  leftToggler.onclick = function() {
    leftToggler.classList.toggle("left_toggler_active");
 
    toggleLeftIcon();
    toggleLeftLayout(); 

    leftToggler.blur();
    return false;
  }

  rightToggler.onclick = function() {
    rightToggler.classList.toggle("right_toggler_active");

    toggleRightIcon();
    toggleRightLayout();

    rightToggler.blur();
    return false;
  }
{{< / highlight >}}

At the end of each script, we are going to blur the focus,
so that the clicked element will get clean looks,
instead of a bordered icon.

#### Left Layout

> Disable maxwidth class.

This function will enable or disable custom made `maxwidth` class,
for use with wide screen

{{< highlight javascript >}}
  function toggleLeftLayout() {
    const maxWidthTogglers = document
      .getElementsByClassName("maxwidth_toggler");

    // ECMAScript 2015 
    for (let mwt of maxWidthTogglers) {
      mwt.classList.toggle("maxwidth");
    }
  }
{{< / highlight >}}


I'm using `ECMAScript 2015` for loop example.
You can use the old school while loop,
for compatibility with old browser.

{{< highlight javascript >}}
    const maxWidthTogglers = document
      .getElementsByClassName("maxwidth_toggler);

    var i=0;
    while (i < maxWidthTogglers.length) {
      maxWidthResizers[i].classList.toggle("maxwidth");
      i++;
    }
{{< / highlight >}}

From the normal view as this below:

![Bootstrap5: Left Button: Wide Normal][074-qb-wide-normal]

To a wider view as this below,
after clicking the left button (disable maxwidth).

![Bootstrap5: Left Button: Wide Active][074-qb-wide-active]

And even wider content,
after clicking the right button (hiding sidebar),
as we wil see in the next on click event handler.

![Bootstrap5: Both Button: Wide][074-qb-wide-both]

#### Right Layout

This is our toggle sidebar wit two task.

* Hide sidebar with `d-none d-sm-block d-md-none` class.

* Change width of main content from `col-sm-8` to `col-sm-12` class.
  And add custom `single` class to fix margin issue.

{{< highlight javascript >}}
  function toggleRightLayout() {
    const isActiveRight = rightToggler.classList
      .contains("right_toggler_active");

    const mainToggler  = document.getElementById("main_toggler");
    const mainTogglerW = document.getElementById("main_toggler_wrapper");
    const asideToggler = document.getElementById("aside_toggler");

    if (isActiveRight) {
      mainToggler.classList.add("col-sm-8");
      mainToggler.classList.remove("col-sm-12");

      mainTogglerW.classList.remove("single");

      asideToggler.classList.remove("d-block"); 
      asideToggler.classList.remove("d-sm-none"); 
    } else {
      mainToggler.classList.add("col-sm-12");
      mainToggler.classList.remove("col-sm-8");

      mainTogglerW.classList.add("single");

      asideToggler.classList.add("d-block");
      asideToggler.classList.add("d-sm-none");
    }
  }
{{< / highlight >}}

For wider maxwidth you can replace the `sm` with `md`.
Or better with something like:

{{< highlight javascript >}}
      if (isActiveRight) {
        mainToggler.classList.add("col-md-8");
        mainToggler.classList.remove("col-md-12");
  
        mainTogglerW.classList.remove("single");
  
        asideToggler.classList.remove("d-none"); 
        asideToggler.classList.remove("d-sm-block");
        asideToggler.classList.remove("d-md-none");
      } else {
        mainToggler.classList.add("col-md-12");
        mainToggler.classList.remove("col-md-8");
  
        mainTogglerW.classList.add("single");
  
        asideToggler.classList.add("d-none");
        asideToggler.classList.add("d-sm-block");
        asideToggler.classList.add("d-md-none");
      }
{{< / highlight >}}

#### Preview in Browser

Now from the normal view as below:

![Bootstrap5: Right Button: Normal][074-qb-right-normal]

You can switch to this:

![Bootstrap5: Right Button: Hide][074-qb-right-hide]

I usually enjoy wider view while reading on the internet.

-- -- --

### 5: Fix Edge and Gap in Layout

Just in case you didn't notice,
my design has a flaw with code above.
The edge on the left edge is lost.

![Bootstrap5: Gap Issue][074-qb-edge-issue]

It can be fix as below:

#### Main Script

Again, add more lines to handle `onclick` event.

{{< highlight javascript >}}
  const leftToggler  = document.getElementById("left_toggler");
  const rightToggler = document.getElementById("right_toggler");

  leftToggler.addEventListener("click", () => {
    leftToggler.classList.toggle("left_toggler_active");
 
    toggleLeftIcon();
    toggleLeftLayout(); 
    fixGapAndEdge();

    leftToggler.blur();
    return false;
  });

  rightToggler.addEventListener("click", () => {
    rightToggler.classList.toggle("right_toggler_active");

    toggleRightIcon();
    toggleRightLayout();
    fixGapAndEdge();

    rightToggler.blur();
    return false;
  });
{{< / highlight >}}

#### Fixing Gap

> Problem solved

Fixing edge and gap for both of two columns,
is the harder part of this script,
because it require deep knowledge about the design itself.

{{< highlight javascript >}}
  // Fix gap between two columns
  function fixGapAndEdge() {
    const isActiveLeft = leftToggler.classList
      .contains("left_toggler_active");
    const isActiveRight = rightToggler.classList
      .contains("right_toggler_active");

    const mainToggler  = document.getElementById("main_toggler");
    const asideToggler = document.getElementById("aside_toggler");

    // Fix Gap
    mainToggler.classList.remove("pr-0");
    asideToggler.classList.remove("pl-0");

    if (!isActiveLeft && isActiveRight) {
      mainToggler.classList.add("pr-0");
      asideToggler.classList.add("pl-0");
    }

    // Fix Edge
    if (isActiveLeft) {
      mainToggler.classList.add("px-0");
      asideToggler.classList.add("px-0");
    } else {
      mainToggler.classList.remove("px-0");
      asideToggler.classList.remove("px-0");
    }

    console.log("Fix gap and edge class.");
  }
{{< / highlight >}}

But don't worry. It is fixed now.

![Bootstrap5: Right Button: Edge Fix][074-qb-edge-fix]

-- -- --

### 6: Separation of Concern

> Refactoring Consideration

Consider this event handler below:

{{< highlight javascript >}}
  leftToggler.addEventListener("click", () => {
    leftToggler.classList.toggle("left_toggler_active");
 
    toggleLeftIcon();
    toggleLeftLayout(); 
    fixGapAndEdge();

    leftToggler.blur();
    return false;
  });
{{< / highlight >}}

Why don't I put the whole thing in just one functions.
Of course I can do that,
but the script becomes more unreadable,
and getting hard to debug.
So it is better that we focus each function,
to its own task as below example:

{{< highlight javascript >}}
  function toggleRightIcon() {
    const isActiveRight = ...
    ...
  }

  function toggleRightLayout() {
    const isActiveRight = ...
    ...
  }

  function fixGap() {
    const isActiveLeft = ...
    const isActiveRight = ...

    ...
  }
{{< / highlight >}}

Of course there is a trade-off,
by repeating those `const` above.
But, it would be easier to understand.

-- -- --

### What is Next 🤔?

One simple thing you can do to enhance your simple blog is animation.
We are going to explore this before we go down to dashboard.

Consider continue reading [ [Bootstrap - Javascript Numerator][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2021/08/03/bootstrap5-javascript-numerator/

[local-bootstrap-v4]:   {{< baseurl >}}frontend/2020/03/16/bootstrap-oc-javascript-toggler/

[tutor-step-07]:        {{< tutor-html-bs5 >}}/step-07/
[tutor-step-07-views]:  {{< tutor-html-bs5 >}}/step-07/views/
[tutor-step-07-js]:     {{< tutor-html-bs5 >}}/step-07/js/

[png-layout-toggler]: {{< assets-frontend >}}/2019/12/layout-toggler.png
[svg-layout-toggler]: {{< assets-frontend >}}/2019/12/layout-toggler.svg

[//]: <> ( -- -- -- links below -- -- -- )

[074-scss-layout-page]:  {{< baseurl >}}assets/posts/frontend/2021/07/074-scss-layout-page.png

[arrow-074-le-le]:       {{< baseurl >}}assets/posts/frontend/2021/07/074-arrow-left-left.png
[arrow-074-le-ri]:       {{< baseurl >}}assets/posts/frontend/2021/07/074-arrow-left-right.png
[arrow-074-ri-le]:       {{< baseurl >}}assets/posts/frontend/2021/07/074-arrow-right-left.png
[arrow-074-ri-ri]:       {{< baseurl >}}assets/posts/frontend/2021/07/074-arrow-right-right.png

[074-tree-js]:          {{< baseurl >}}assets/posts/frontend/2021/07/074-tree-js.png

[074-js-toggler-01]:    {{< baseurl >}}assets/posts/frontend/2021/07/074-js-toggler-01.png

[074-inspect-toggler]:  {{< baseurl >}}assets/posts/frontend/2021/07/074-inspect-toggler-active.png
[074-inspect-console]:  {{< baseurl >}}assets/posts/frontend/2021/07/074-inspect-toggler-console.png


[074-b-toggler]:        {{< baseurl >}}assets/posts/frontend/2021/07/074-njk-b-toggler.png


[074-v-toggler-01]:     {{< baseurl >}}assets/posts/frontend/2021/07/074-v-javascript-toggler-01.png
[074-v-toggler-02]:     {{< baseurl >}}assets/posts/frontend/2021/07/074-v-javascript-toggler-02.png

[074-qb-toggler-crop]:  {{< baseurl >}}assets/posts/frontend/2021/07/074-qb-toggler-xs-crop.png
[074-qb-toggler-sm]:    {{< baseurl >}}assets/posts/frontend/2021/07/074-qb-toggler-sm.png


[074-qb-wide-normal]:   {{< baseurl >}}assets/posts/frontend/2021/07/074-qb-wide-normal.png
[074-qb-wide-active]:   {{< baseurl >}}assets/posts/frontend/2021/07/074-qb-wide-active.png
[074-qb-wide-both]:     {{< baseurl >}}assets/posts/frontend/2021/07/074-qb-wide-both.png
[074-qb-right-normal]:  {{< baseurl >}}assets/posts/frontend/2021/07/074-qb-right-normal.png
[074-qb-right-hide]:    {{< baseurl >}}assets/posts/frontend/2021/07/074-qb-right-hide.png
[074-qb-edge-issue]:    {{< baseurl >}}assets/posts/frontend/2021/07/074-qb-edge-issue.png
[074-qb-edge-fix]:      {{< baseurl >}}assets/posts/frontend/2021/07/074-qb-edge-fix.png

[//]: <> ( -- -- -- links below -- -- -- )

[js-custom-toggler]:    {{< tutor-html-bs5 >}}/step-07/assets/js/custom-toggler.js

[scss-layout-page]:     {{< tutor-html-bs5 >}}/step-06/sass/css/main/_layout_page.scss

[njk-v-074-toggler]:    {{< tutor-html-bs5 >}}/step-07/views/074-javascript-toggler.njk

[njk-b-074-toggler]:    {{< tutor-html-bs5 >}}/step-07/views/blog/074-toggler.njk

[njk-s-074-navbar]:    {{< tutor-html-bs5 >}}/step-07/views/shared/074-navbar.njk
[njk-s-074-footer]:    {{< tutor-html-bs5 >}}/step-07/views/shared/074-footer.njk

[njk-a-074-aside]:     {{< tutor-html-bs5 >}}/step-07/views/aside/074-aside.njk
[njk-c-074-main]:      {{< tutor-html-bs5 >}}/step-07/views/contents/074-main.njk

