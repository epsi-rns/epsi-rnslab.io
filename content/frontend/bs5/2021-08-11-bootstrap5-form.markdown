---
type   : post
title  : "Bootstrap - Form"
date   : 2021-08-11T09:17:35+07:00
slug   : bootstrap5-form
categories: [frontend]
tags      : [bootstrap]
keywords  : [signin form, user form]
author : epsi
opengraph:
  image: assets/posts/frontend/2021/08/092-qb-user-form-sm.png

toc    : "toc-2021-06-html-bootstrap5-step"

excerpt:
  Preparing Bootstrap's form for dynamic data handling.

---

### Preface

> Step Nine: Preparing Bootstrap's form for dynamic data handling.

After years with static blog,
I continue my journey to dynamic web,
with data reading and writing.
So I prepare.

_Using form in bootstrap required more exposure_.

-- -- --

### 1: Chapter Overview

This chapter is short.
Because I do not have enough experience yet.

_This time, I make a mediocre article_.

#### Source Code: Step-09

To obtain the source code for this chapter,
please follow the link provided below:

* [gitlab.com/.../step-09/][tutor-step-09]

#### Icons Sets

Instead of my usual FontAwesome,
I start to use Bootstrap Icons instead.

#### Stylesheet Assets

For simplicity reason,
I shamefully copy and paste stylesheet,
from official bootstrap example,

* [gitlab.com/.../step-09/assets/css/][tutor-step-09-css]

{{< highlight bash >}}
❯ tree -C css
css
├── bootstrap.css
├── bootstrap.css.map
├── form-common.css
├── helper.css
├── helper.css.map
├── main.css
├── main.css.map
└── signin.css

1 directory, 8 files
{{< / highlight >}}

![Bootstrap5: Stylesheet Assets][090-tree-assets-css]

We can see two new files:

* form-common.css
* signin.css

#### Nunjucks Directory Structure

The code structure is very common with previous article.
No need further explanation.

* [gitlab.com/.../step-09/views/][tutor-step-09-views]

{{< highlight bash >}}
❯ tree -C views
views
├── 091-login-page.njk
├── 092-forms.njk
├── contents
│   ├── 091-login-form.njk
│   ├── 091-main.njk
│   ├── 092-main.njk
│   └── 092-user-form.njk
├── heads
│   ├── links-blog.njk
│   ├── meta.njk
│   └── style-icons.njk
├── layouts
│   └── base.njk
└── shared
    ├── footer.njk
    ├── navbar-button.njk
    ├── navbar-collapse.njk
    ├── navbar-dropdown.njk
    ├── navbar.njk
    └── symbols.njk

5 directories, 16 files
{{< / highlight >}}

![Bootstrap5: Nunjucks NERDTree][090-nerd-nunjucks]

#### Block: Common HTML Head

The blocks for HTML Head is similar with:

{{< highlight jinja >}}
{% block htmlhead %}
  {% include './heads/meta.njk' %}
  {% include './heads/links-blog.njk' %}
  {% include './heads/style-icons.njk' %}

  ...
{% endblock %}
{{< / highlight >}}

![Bootstrap5: Nunjucks NERDTree: HTML Head][090-nerd-heads]

-- -- --

### 2: Login Page

I grab this form boostrap example,
with very few modification,
then put signin form inside my own theme.

#### Block: HTML Head

The blocks for HTML Head have this additional assets:

{{< highlight jinja >}}
{% block htmlhead %}
  {% include './heads/meta.njk' %}
  {% include './heads/links-blog.njk' %}
  {% include './heads/style-icons.njk' %}

  <link rel="stylesheet" type="text/css"
    href="assets/css/signin.css">
{% endblock %}
{{< / highlight >}}

#### Stylesheet

The responsible stylesheet for signin form is:

* [gitlab.com/.../assets/css/signin.css][css-signin]

{{< highlight scss >}}
.form-signin {
  max-width: 330px;
  padding: 15px;
}

.form-signin .form-floating:focus-within {
  z-index: 2;
}

.form-signin input[type="email"] {
  margin-bottom: -1px;
  border-bottom-right-radius: 0;
  border-bottom-left-radius: 0;
}

.form-signin input[type="password"] {
  margin-bottom: 10px;
  border-top-left-radius: 0;
  border-top-right-radius: 0;
}
{{< / highlight >}}

I do not create this stylesheet,
so I won't make a screenshot.

#### Block: Content

The blocks for Content is:

* [gitlab.com/.../views/091-login-page.njk][njk-v-091-login-page]

{{< highlight jinja >}}
{% block content %}
  <!-- responsive colored main layout -->
  <div class="row layout-base maxwidth">
    {% include './contents/091-main.njk' %}
  </div>
{% endblock %}
{{< / highlight >}}

#### Nunjucks: Main

While the main itself contain an inner template:

* [gitlab.com/.../views/contents/091-main.njk][njk-c-091-main]

{{< highlight html >}}
<main class="col px-0">
  ...
    {% include './091-login-form.njk' %}
  ...
</main>
{{< / highlight >}}

We need to go deeper then.

#### Nunjucks: Login Form

I think it is easier to read in modular template this way.

* [gitlab.com/.../views/contents/091-login-form.njk][njk-c-091-login-form]

{{< highlight html >}}
<div class="form-signin w-100 m-auto">
  <form>
    ...

    <div class="form-floating hoverable">
      <input type="email" 
        class="form-control"
        id="floatingInput"
        placeholder="name@example.com">
      <label for="floatingInput"
        >Email address</label>
    </div>
    <div class="form-floating hoverable">
      ...
    </div>

    ...
  </form>
</div>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Contents: Login Form][091-c-login-form]

#### Preview on Browser

Let's see the result!

![Bootstrap5: Form: Login: xs][091-qb-login-form-xs]

Just a copy from official example.
I won't be so excited.

-- -- --

### 3: User Form

I also grab this form boostrap example,
with very few modification,
then also put the form inside my own theme.

#### Block: HTML Head

The blocks for HTML Head have this additional assets:

{{< highlight jinja >}}
{% block htmlhead %}
  {% include './heads/meta.njk' %}
  {% include './heads/links-blog.njk' %}
  {% include './heads/style-icons.njk' %}

  <link rel="stylesheet" type="text/css"
    href="assets/css/form-common.css">
{% endblock %}
{{< / highlight >}}

#### Stylesheet

The responsible stylesheet common form is:

* [gitlab.com/.../assets/css/form-common.css][css-form-common]

{{< highlight scss >}}
.form-common {
  max-width: 500px;
  padding: 15px;
}
{{< / highlight >}}

I forget who created this stylesheet,
so I won't make a screenshot, either.
I guess I copied from the signin form.

#### Block: Content

The blocks for Content is:

* [gitlab.com/.../views/092-forms.njk][njk-v-092-forms]

{{< highlight jinja >}}
{% block content %}
  <!-- responsive colored main layout -->
  <div class="row layout-base maxwidth">
    {% include './contents/092-main.njk' %}
  </div>
{% endblock %}
{{< / highlight >}}

#### Nunjucks: Main

While the main itself contain an inner template:

* [gitlab.com/.../views/contents/092-main.njk][njk-c-092-main]

{{< highlight html >}}
<main class="col px-0">
  ...
    {% include './092-user-form.njk' %}
  ...
</main>
{{< / highlight >}}

Again, we need to go deeper.

#### Nunjucks: User Form

I think it is easier to read in modular template this way.

* [gitlab.com/.../views/contents/092-user-form.njk][njk-c-092-user-form]

{{< highlight html >}}
  <div class="form-common w-100 m-auto">
  <h4 class="mb-3">Players and Staffs</h4>
  <form class="needs-validation" novalidate>
    <div class="row g-3">
      <div class="col-12">
        <label for="name" class="form-label"
          >Name</label>
        <input type="text" class="form-control"
          id="name" placeholder=""
          value="" required>
        <div class="invalid-feedback">
          Valid name is required.
        </div>
      </div>

      ...
    </div>

    <hr class="my-4">

    <button
      class="w-100 btn btn-primary btn-lg"
      type="submit">Continue to register</button>
    <br/>
  </form>
</div>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Contents: user Form][092-c-user-form]

#### Preview on Browser

Let's see the result!

![Bootstrap5: Form: User: xs][092-qb-user-form-xs]

Just a copy from official example.
I won't be so excited.

-- -- --

### What is Next 🤔?

We need to learn parts of dashboard content,
before gathering up all together into one dashboard page.

Consider continue reading [ [Bootstrap 5 - Tables and Chart][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2021/08/13/bootstrap5-extra/

[tutor-step-09]:        {{< tutor-html-bs5 >}}/step-09/
[tutor-step-09-views]:  {{< tutor-html-bs5 >}}/step-09/views/
[tutor-step-09-js]:     {{< tutor-html-bs5 >}}/step-09/assets/js/
[tutor-step-09-css]:    {{< tutor-html-bs5 >}}/step-09/assets/css/

[//]: <> ( -- -- -- links below -- -- -- )

[090-nerd-nunjucks]:    {{< baseurl >}}assets/posts/frontend/2021/08/090-nerd-nunjucks.png
[090-nerd-heads]:       {{< baseurl >}}assets/posts/frontend/2021/08/090-nerd-heads.png

[090-tree-assets-css]:  {{< baseurl >}}assets/posts/frontend/2021/08/090-tree-assets-css.png

[091-c-login-form]:     {{< baseurl >}}assets/posts/frontend/2021/08/091-njk-c-login-form.png
[092-c-user-form]:      {{< baseurl >}}assets/posts/frontend/2021/08/092-njk-c-user-form.png

[091-qb-login-form-sm]: {{< baseurl >}}assets/posts/frontend/2021/08/091-qb-login-form-sm.png
[091-qb-login-form-xs]: {{< baseurl >}}assets/posts/frontend/2021/08/091-qb-login-form-xs.png
[092-qb-user-form-sm]:  {{< baseurl >}}assets/posts/frontend/2021/08/092-qb-user-form-sm.png
[092-qb-user-form-xs]:  {{< baseurl >}}assets/posts/frontend/2021/08/092-qb-user-form-xs.png

[//]: <> ( -- -- -- links below -- -- -- )

[css-signin]:           {{< tutor-html-bs5 >}}/step-09/assets/css/signin.css
[css-form-common]:      {{< tutor-html-bs5 >}}/step-09/assets/css/form-common.css

[njk-v-091-login-page]: {{< tutor-html-bs5 >}}/step-09/views/091-login-page.njk

[njk-c-091-main]:       {{< tutor-html-bs5 >}}/step-09/views/contents/091-main.njk
[njk-c-091-login-form]: {{< tutor-html-bs5 >}}/step-09/views/contents/091-login-form.njk
[njk-c-092-main]:       {{< tutor-html-bs5 >}}/step-09/views/contents/092-main.njk
[njk-c-092-user-form]:  {{< tutor-html-bs5 >}}/step-09/views/contents/092-user-form.njk