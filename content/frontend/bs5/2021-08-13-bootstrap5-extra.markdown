---
type   : post
title  : "Bootstrap - Extra"
date   : 2021-08-13T09:17:35+07:00
slug   : bootstrap5-extra
categories: [frontend]
tags      : [bootstrap, javascript]
keywords  : [toggler button]
author : epsi
opengraph:
  image: assets/posts/frontend/2021/08/103-qb-chartjs-md.png

toc    : "toc-2021-06-html-bootstrap5-step"

excerpt:
  Using third party library.

---

### Preface

> Step Ten: Using third party library.

In web development context,
we need to be coexist with other library,
such as `datatable` and `chartjs`.

-- -- --

### 1: Chapter Overview

I want to explore dashboard.
But what good is dashboard without content?
So I decide to create the content first,
that we can add it later to our beloved dashboard.

There is no need to setup any internal javascript nor stylesheet.

#### Nunjucks Directory Structure

I invite you to inspect the code yourself on my GitHub repository

* [gitlab.com/.../step-10/views/][tutor-step-10-views]

{{< highlight bash >}}
❯ tree -C views
views
├── 101-soccer-table.njk
├── 102-datatables.njk
├── 103-chartjs.njk
├── contents
│   ├── 101-main.njk
│   ├── 101-soccer-table.njk
│   ├── 102-data-table.njk
│   ├── 102-main.njk
│   ├── 103-chart.njk
│   ├── 103-main.njk
│   ├── soccer-tbody.njk
│   └── soccer-thead.njk
├── heads
│   ├── links-blog.njk
│   ├── links-chart.njk
│   ├── links-datatables.njk
│   ├── meta.njk
│   └── style-icons.njk
├── layouts
│   └── base.njk
└── shared
    ├── footer.njk
    ├── navbar-button.njk
    ├── navbar-collapse.njk
    ├── navbar-dropdown.njk
    ├── navbar.njk
    └── symbols.njk

5 directories, 23 files
{{< / highlight >}}

![Bootstrap5: Nunjucks NERDTree][100-nerd-views]

I don't think that I need to explain in detail for each file.

#### Source Code: Step-10

To obtain the source code for this chapter,
please follow the link provided below:

* [gitlab.com/.../step-10/][tutor-step-10]

-- -- --

### 2: Soccer Table

Before we go down to `datatables`,
we need to know native bootstrap class.

#### Related Articles

You don't need to know any SQL.
It is just, how I built the table.

* [Soccer - Overview][local-soccer-overview]

You are free to skip this article above.
It has nothing to do with this article anyway.

#### Blocks

> Nunjucks Document Source

The required chuncks is typical, as shown below:

* [gitlab.com/.../views/101-soccer-table.njk][njk-v-101-soccertable]

{{< highlight jinja >}}
{% extends './layouts/base.njk' %}

{% block htmlhead %}
  {% include './heads/meta.njk' %}
  {% include './heads/links-blog.njk' %}
  {% include './heads/style-icons.njk' %}
{% endblock %}

{% block header %}
  {% include './shared/symbols.njk' %}
  {% include './shared/navbar.njk' %}
{% endblock %}

{% block content %}
  <!-- responsive colored main layout -->
  <div class="row layout-base maxwidth">
    {% include './contents/101-soccer-table.njk' %}
  </div>
{% endblock %}

{% block footer %}
  {% include './shared/footer.njk' %}
{% endblock %}
{{< / highlight >}}

#### Table Class

There is nothing special in main class.
It is just wrapper element.

* [gitlab.com/.../views/contents/101-main.njk][njk-c-101-main]

The table element is,
using native bootstrap class.

* [gitlab.com/.../views/contents/101-soccer-table.njk][njk-c-101-table]

{{< highlight html >}}
<table class="table table-striped table-hover">
  {% include './soccer-thead.njk' %}
  {% include './soccer-tbody.njk' %}
</table>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Contents: Main: Table][101-c-main]

#### Table: Head and Body

The table head is also plain.

* [gitlab.com/.../views/contents/101-thead.njk][njk-c-101-thead]

{{< highlight html >}}
<thead>
  <tr>
    <th scope="col">id</th>
    <th scope="col">name</th>
    <th scope="col">email</th>
    <th scope="col">age</th>
    <th scope="col">gender</th>
  </tr>
</thead>
{{< / highlight >}}

The table body are also plain.

* [gitlab.com/.../views/contents/101-tbody.njk][njk-c-101-tbody]

{{< highlight html >}}
<tbody>
  <tr>
    <th scope="row">1</td>
    <td>Takumi Sato</td>
    <td><a href="mailto:takumi.sato@example.com"
      >takumi.sato@example.com</a></td>
    <td>17</td>
    <td>Male</td>
  </tr>
  ...
</tbody>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Contents: Table: thead and tbody][101-c-table]

#### Preview in Browser

On landscape mobile display, the layout appears as follows:

![Bootstrap5: Soccer Table in Small Desktop][101-qb-soccertable-sm]

The layout on tablet display is illustrated in the following image:

![Bootstrap5: Soccer Table in Desktop][101-qb-soccertable-md]

-- -- --

### 3: Datatables

This is a third party javascript,
to add advanced interaction controls
to HTML tables the free and easy way.

#### Official Site

For a more comprehensive resource,
I recommend reviewing the official site.

* [datatables.net/](https://datatables.net/)

#### Blocks: HTML Head

> Nunjucks Document Source

We need additional assets.

* [gitlab.com/.../views/102-datatables.njk][njk-v-102-datatables]

{{< highlight jinja >}}
{% block htmlhead %}
  {% include './heads/meta.njk' %}
  {% include './heads/links-blog.njk' %}
  {% include './heads/style-icons.njk' %}
  {% include './heads/links-datatables.njk' %}
{% endblock %}
{{< / highlight >}}

#### Stylesheet and Javascript

While the required assets can be obtained using CDN.

* [gitlab.com/.../views/heads/links-datatables.njk][njk-h-102-links]

{{< highlight html >}}
  <link rel="stylesheet"
        href="https://cdn.datatables.net/1.13.4/css/jquery.dataTables.css" />

  <script type="text/javascript" language="javascript"
    src="https://code.jquery.com/jquery-3.5.1.js"></script>
  <script
    src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.js"></script>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Heads: Links: Datatables][102-h-links]

#### Table Class

The only difference is the additional table classes and id.
While the `thead` and `tbody` remains intact.

* [gitlab.com/.../views/contents/102-data-table.njk][njk-c-102-table]

{{< highlight html >}}
<table
    id="myTable"
    class="display table 
           table-striped table-hover">
  {% include './soccer-thead.njk' %}
  {% include './soccer-tbody.njk' %}
</table>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Contents: Main: Table][102-c-table]

#### Blocks: Footer: Javascript Configuration

> Nunjucks Document Source

Now we need to pull the trigger for specific `id` element.
I put the javascript configuration inside the HTML document.
For longer configuration I prefer external javascript,
with `defer` parameter.

* [gitlab.com/.../views/102-datatables.njk][njk-v-102-datatables]

{{< highlight javascript >}}
{% block footer %}
  {% include './shared/footer.njk' %}

  <script>
  $(document).ready( function () {
    $('#myTable').DataTable({
      "pageLength": 5 
    });
  } );
  </script>
{% endblock %}
{{< / highlight >}}

There are so many parameter that we can configure.
You can find out yourself in official site.

![Bootstrap5: Nunjucks: Datatables: Javascript Configuration][102-v-datatables]

That's all, the tables will magically shown differently.

#### Preview in Browser

On landscape mobile display, the layout appears as follows:

![Bootstrap5: Datatables in Small Desktop][102-qb-datatables-sm]

The layout on tablet display is illustrated in the following image:

![Bootstrap5: Datatables in Desktop][102-qb-datatables-md]

-- -- --

### 4: ChartJS

There are some good charting libraries.
`chartjs` is just one of them.
This means, you might consider to try other library as well.

#### Official Site

To obtain a deeper understanding of the subject matter,
I suggest referring to the official site.

* [chartjs.org/](https://www.chartjs.org/)

#### Blocks: HTML Head

> Nunjucks Document Source

As usual, we need to manage our assets.

* [gitlab.com/.../views/103-chartjs.njk][njk-v-103-chartjs]

{{< highlight jinja >}}
{% block htmlhead %}
  {% include './heads/meta.njk' %}
  {% include './heads/links-blog.njk' %}
  {% include './heads/style-icons.njk' %}
  {% include './heads/links-datatables.njk' %}
{% endblock %}
{{< / highlight >}}

![Bootstrap5: Nunjucks: ChartJS: HTML Heads][103-v-chartjs]

#### Javascript: Library

While the required assets can be obtained using CDN.

* [gitlab.com/.../views/heads/links-chart.njk][njk-h-103-chart]

{{< highlight html >}}
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script defer src="assets/js/barchart-config.js"></script>
{{< / highlight >}}

Note that I put the configuration as external javascript.
Since the configuration is long,
I prefer external javascript with `defer` parameter.

![Bootstrap5: Nunjucks: Heads: Links: ChartJS][103-h-links]

#### Javascript: Configuration

> Easy right?

The official documentation give you various interesting configuration.

* [gitlab.com/.../assets/js/barchart-config.js][js-barchart-config]

First we need to prepare the data.

{{< highlight javascript >}}
(() => {
  'use strict'

  const ctx = document.getElementById('myChart')

  const data = [
    { year: 2010, count: 10, color: '#BBDEFB' },
    { year: 2011, count: 20, color: '#90CAF9' },
    { year: 2012, count: 15, color: '#64B5F6' },
    { year: 2013, count: 25, color: '#42A5F5' },
    { year: 2014, count: 22, color: '#1E88E5' },
    { year: 2015, count: 30, color: '#1976D2' },
    { year: 2016, count: 28, color: '#1565C0' },
  ];
  
  const myChart = new Chart(ctx, {
    ...
  })
})()
{{< / highlight >}}

![Bootstrap5: Javascript: ChartJS Configuration][103-js-chart-01]

Then we set the parameter configuration

{{< highlight javascript >}}
(() => {
  ...
  
  const myChart = new Chart(ctx, {
    type: 'bar',
    data: {
      labels: data.map(row => row.year),
      datasets: [{
        label: 'year',
        data: data.map(row => row.count),
        borderWidth: 1,
        backgroundColor: data.map(row => row.color),
      }]
    },
    options: {
      scales: {
        y: [{
          ticks: {
            beginAtZero: false
          }
        }]
      },
      legend: {
        display: false
      }
    }
  })
})()
{{< / highlight >}}

![Bootstrap5: Javascript: ChartJS Configuration][103-js-chart-02]

It is self explanatory.

#### Preview in Browser

On landscape mobile display, the layout appears as follows:

![Bootstrap5: ChartJS in Small Desktop][103-qb-chartjs-sm]

The layout on tablet display is illustrated in the following image:

![Bootstrap5: ChartJS in Desktop][103-qb-chartjs-md]

I don't know how the internal really works.
I guess magic stuff happened.

-- -- --

### What is Next 🤔?

Now that the parts are ready, 
We need to gathering up all into one dashboard page.

Consider continue reading [ [Bootstrap 5 - Dashboard][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2021/08/15/bootstrap5-dashboard/

[local-soccer-overview]:{{< baseurl >}}backend/2021/05/01/soccer-overview/

[tutor-step-10]:        {{< tutor-html-bs5 >}}/step-10/
[tutor-step-10-views]:  {{< tutor-html-bs5 >}}/step-10/views/
[tutor-step-10-js]:     {{< tutor-html-bs5 >}}/step-10/assets/js/
[tutor-step-10-css]:    {{< tutor-html-bs5 >}}/step-10/assets/css/

[//]: <> ( -- -- -- links below -- -- -- )

[100-tree-views]:       {{< baseurl >}}assets/posts/frontend/2021/08/100-tree-views.png
[100-nerd-views]:       {{< baseurl >}}assets/posts/frontend/2021/08/100-nerd-views.png

[103-js-chart-01]:      {{< baseurl >}}assets/posts/frontend/2021/08/103-js-chart-01.png
[103-js-chart-02]:      {{< baseurl >}}assets/posts/frontend/2021/08/103-js-chart-02.png

[102-h-links]:          {{< baseurl >}}assets/posts/frontend/2021/08/102-njk-h-links.png
[103-h-links]:          {{< baseurl >}}assets/posts/frontend/2021/08/103-njk-h-links.png

[101-c-main]:           {{< baseurl >}}assets/posts/frontend/2021/08/101-njk-c-main.png
[101-c-table]:          {{< baseurl >}}assets/posts/frontend/2021/08/101-njk-c-table.png
[102-c-table]:          {{< baseurl >}}assets/posts/frontend/2021/08/102-njk-c-table.png

[102-v-datatables]:     {{< baseurl >}}assets/posts/frontend/2021/08/102-v-datatables.png
[103-v-chartjs]:        {{< baseurl >}}assets/posts/frontend/2021/08/103-v-chartjs.png

[101-qb-soccertable-md]:{{< baseurl >}}assets/posts/frontend/2021/08/101-qb-soccer-table-md.png
[101-qb-soccertable-sm]:{{< baseurl >}}assets/posts/frontend/2021/08/101-qb-soccer-table-sm.png
[102-qb-datatables-md]: {{< baseurl >}}assets/posts/frontend/2021/08/102-qb-datatables-md.png
[102-qb-datatables-sm]: {{< baseurl >}}assets/posts/frontend/2021/08/102-qb-datatables-sm.png
[103-qb-chartjs-md]:    {{< baseurl >}}assets/posts/frontend/2021/08/103-qb-chartjs-md.png
[103-qb-chartjs-sm]:    {{< baseurl >}}assets/posts/frontend/2021/08/103-qb-chartjs-sm.png

[//]: <> ( -- -- -- links below -- -- -- )

[js-barchart-config]:   {{< tutor-html-bs5 >}}/step-10/assets/js/barchart-config.js

[njk-h-102-links]:      {{< tutor-html-bs5 >}}/step-10/views/heads/links-datatables.njk
[njk-h-103-links]:      {{< tutor-html-bs5 >}}/step-10/views/heads/links-chartjs.njk

[njk-c-101-main]:       {{< tutor-html-bs5 >}}/step-10/views/contents/101-main.njk
[njk-c-101-table]:      {{< tutor-html-bs5 >}}/step-10/views/contents/101-soccer-table.njk
[njk-c-101-thead]:      {{< tutor-html-bs5 >}}/step-10/views/contents/101-thead.njk
[njk-c-101-tbody]:      {{< tutor-html-bs5 >}}/step-10/views/contents/101-tbody.njk
[njk-c-102-table]:      {{< tutor-html-bs5 >}}/step-10/views/contents/102-data-table.njk

[njk-v-101-soccertable]:{{< tutor-html-bs5 >}}/step-10/views/101-soccer-table.njk
[njk-v-102-datatables]: {{< tutor-html-bs5 >}}/step-10/views/102-datatables.njk
[njk-v-103-chartjs]:    {{< tutor-html-bs5 >}}/step-10/views/103-chartjs.njk
