---
type   : post
title  : "Bootstrap - Dashboard"
date   : 2021-08-15T09:17:35+07:00
slug   : bootstrap5-dashboard
categories: [frontend]
tags      : [bootstrap]
keywords  : [dashboard]
author : epsi
opengraph:
  image: assets/posts/frontend/2021/08/106-qb-dashboard-md.png

toc    : "toc-2021-06-html-bootstrap5-step"

excerpt:
  Dashboard with native bootstrap class.

---

### Preface

> Step Ten: Dashboard with native bootstrap class.

We can make dashboard using only native bootstrap class.

-- -- --

### 1: Chapter Overview

We have the example content such as `datatable` and `chartjs`.
Now we can explore dashboard.

#### Source Code: Step-10

To obtain the source code for this chapter,
please follow the link provided below:

* [gitlab.com/.../step-10/][tutor-step-10]

#### Sidebar

One most important part of the dashboard is the sidebar menu.
This have been discussed in previous article.

* [Bootstrap 5 - Side Bar][local-sidebar]

We have already experience with sidebar in previous article,
now we can fill the dashboard.

![Bootstrap5: Qutebrowser: Sidebar: Bootstrap Icons][037-qb-sidebar-ibs-sm]

#### Nunjucks Directory Structure

I invite you to inspect the code yourself on my GitHub repository.

* [gitlab.com/.../step-10/views/][tutor-step-10-views]

{{< highlight bash >}}
❯ tree -C views
views
├── 105-dashboard.njk
├── 106-dashboard.njk
├── contents
│   ├── 105-main.njk
│   ├── 106-chart-header.njk
│   ├── 106-chart.njk
│   ├── 106-main.njk
│   ├── soccer-tbody.njk
│   └── soccer-thead.njk
├── heads
│   ├── links-chart.njk
│   ├── links-dashboard.njk
│   ├── links-datatables.njk
│   ├── links-sidebar.njk
│   ├── meta.njk
│   └── style-icons.njk
├── layouts
│   └── base.njk
└── shared
    ├── dashboard-collapsible.njk
    ├── dashboard-header.njk
    ├── dashboard-sidebar.njk
    └── symbols.njk

5 directories, 19 files
{{< / highlight >}}

![Bootstrap5: Nunjucks NERDTree][106-nerd-views]

As usual, no need to go in explain for each file.

-- -- --

### 2: Simple Dashboard

Consider start from simple example.

#### Blocks

> Nunjucks Document Source

We have assets, and `dashboard-header`.

* [gitlab.com/.../views/105-dashboard.njk][njk-v-105-dashboard]

{{< highlight jinja >}}
{% extends './layouts/base.njk' %}

{% block htmlhead %}
  {% include './heads/meta.njk' %}
  {% include './heads/links-dashboard.njk' %}
  {% include './heads/links-sidebar.njk' %}
  {% include './heads/style-icons.njk' %}
{% endblock %}

{% block header %}
  {% include './shared/symbols.njk' %}
  {% include './shared/dashboard-header.njk' %}
{% endblock %}
{{< / highlight >}}

![Bootstrap5: Nunjucks: Blocks: Dashboard][105-njk-v-dashbrd-01]

The sidebar reside in `content` block.
This time our block full of HTML tags.

{{< highlight html >}}
{% block content %}
<div class="container-fluid">
  <div class="row">
    <nav id="sidebarMenu"
          class="col-md-3 col-lg-3 col-xl-2 d-md-block
            bg-light sidebar collapse">
      {% include './shared/dashboard-sidebar.njk' %}
    </nav>

    <main class="col-md-9 col-lg-9 col-xl-10
            ms-sm-auto px-md-4 bg-white">
      {% include './contents/105-main.njk' %}
    </main>
  </div>
</div>  
{% endblock %}
{{< / highlight >}}

![Bootstrap5: Nunjucks: Blocks: Dashboard][105-njk-v-dashbrd-02]

#### Nunjucks: Links

To avoid confusion, the dashboard assets is just common assets.

* [gitlab.com/.../views/heads/links-dashboard.njk][njk-105-h-links]

{{< highlight html >}}

  <link rel="stylesheet" type="text/css" 
    href="assets/css/bootstrap.css">
  <link rel="stylesheet" type="text/css"
    href="assets/css/helper.css">

  <script src="assets/js/popper.min.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>

{{< / highlight >}}

![Bootstrap5: Nunjucks: Heads: Assets Links][105-h-links]

#### Dashboard Header

 I grab the example code from bootstrap official.

* [gitlab.com/.../views/shared/header.njk][njk-s-dashboard-head]

{{< highlight html >}}
  <header class="navbar navbar-dark sticky-top
      bg-dark flex-md-nowrap p-0 shadow">
    <a class="navbar-brand col-md-3 col-lg-2
         me-0 px-3 fs-6" 
       href="#">Company name</a>
    <button type="button"
        class="navbar-toggler position-absolute
          d-md-none collapsed"
        data-bs-toggle="collapse"
        data-bs-target="#sidebarMenu"
        aria-controls="sidebarMenu"
        aria-expanded="false"
        aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <input type="text"
      class="form-control form-control-dark
        w-100 rounded-0 border-0"
      placeholder="Search"
      aria-label="Search">
    <div class="navbar-nav">
      <div class="nav-item text-nowrap">
        <a class="nav-link px-3"
           href="#">Sign out</a>
      </div>
    </div>
  </header>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Shared: Dashboard Header][105-s-dashboard-head]

#### Nunjucks: Main

After the complex `content` block above,
we got our simple main template.

* [gitlab.com/.../views/contents/105-main.njk][njk-c-105-main]

{{< highlight html >}}
<h2>Community Soccer Club</h2>
<div class="table-responsive">

  <table class="table table-striped table-hover">
    {% include './soccer-thead.njk' %}
    {% include './soccer-tbody.njk' %}
  </table>

</div>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Contents: Main][105-c-main-01]

The rest is, you have already know about this `thead` and `tbody` chunks.

No we are ready for our preview page.

#### Preview in Browser

The following image showcases how the layout appears on tablet display:

![Bootstrap5: Simple Dashboard][105-qb-dashboard-md]

-- -- --

### 3: More Dashboard Example

We are ready for example with more contents.

#### Blocks

> Nunjucks Document Source

We have some more assets now.

* [gitlab.com/.../views/106-dashboard.njk][njk-v-106-dashboard]

{{< highlight jinja >}}
{% extends './layouts/base.njk' %}

{% block htmlhead %}
  {% include './heads/meta.njk' %}
  {% include './heads/links-dashboard.njk' %}
  {% include './heads/links-sidebar.njk' %}
  {% include './heads/links-chart.njk' %}
  {% include './heads/links-datatables.njk' %}
  {% include './heads/style-icons.njk' %}
{% endblock %}

{% block header %}
  {% include './shared/symbols.njk' %}
  {% include './shared/dashboard-header.njk' %}
{% endblock %}
{{< / highlight >}}

![Bootstrap5: Nunjucks: Blocks: Dashboard][106-njk-v-dashbrd-01]

The sidebar reside in `content` block.
This is just basically changing the main template

{{< highlight html >}}
{% block content %}
<div class="container-fluid">
  <div class="row">
    <nav id="sidebarMenu"
          class="col-md-3 col-lg-3 col-xl-2 d-md-block
            bg-white sidebar collapse">
      {% include './shared/dashboard-sidebar.njk' %}
    </nav>

    <main class="col-md-9 col-lg-9 col-xl-10
            ms-sm-auto px-md-4 bg-white">
      {% include './contents/106-main.njk' %}
    </main>
  </div>
</div>
{% endblock %}
{{< / highlight >}}

![Bootstrap5: Nunjucks: Blocks: Dashboard][106-njk-v-dashbrd-02]

And this time with javascript to trigger the chart.

{{< highlight html >}}
{% block footer %}
  <script>
    $(document).ready( function () {
      $('#myTable').DataTable({
        "pageLength": 5 
      });
    });
  </script>
{% endblock %}
{{< / highlight >}}

![Bootstrap5: Nunjucks: Blocks: Dashboard][106-njk-v-dashbrd-03]

#### Nunjucks: Main

Again, after the complex `content` block above,
we got our simple main template.

* [gitlab.com/.../views/contents/105-main.njk][njk-c-105-main]

{{< highlight html >}}
{% include './106-chart-header.njk' %}
{% include './106-chart.njk' %}

<h2>Community Soccer Club</h2>
<div class="table-responsive">

  <table id="myTable"
      class="display table
              table-striped table-hover">
    {% include './soccer-thead.njk' %}
    {% include './soccer-tbody.njk' %}
  </table>

</div>
{{< / highlight >}}

![Bootstrap5: Nunjucks: Contents: Main][105-c-main-01]

Really just this simple. Only chart addition above.

#### Nunjucks: Chart Header

I also gran this example form bootstrap official site.

* [gitlab.com/.../views/contents/106-chart-header.njk][njk-c-106-chartheader]

{{< highlight html >}}
<div class="d-flex justify-content-between
    flex-wrap flex-md-nowrap align-items-center
    pt-3 pb-2 mb-3 border-bottom">
  <h1 class="h2">Dashboard</h1>
  <div class="btn-toolbar mb-2 mb-md-0">
    <div class="btn-group me-2">
      <button type="button"
        class="btn btn-sm btn-outline-secondary"
        >Share</button>
      <button type="button"
        class="btn btn-sm btn-outline-secondary"
        >Export</button>
    </div>
    <button type="button" class="btn btn-sm
        btn-outline-secondary dropdown-toggle">
      <span data-feather="calendar"
        class="align-text-bottom"></span>
      This Year
    </button>
  </div>
</div>
{{< / highlight >}}

#### Nunjucks: ChartJS

And the ChartJS itself is so simple

* [gitlab.com/.../views/contents/106-chart.njk][njk-c-106-chart]

{{< highlight html >}}
  <canvas class="my-4 w-100" 
        id="myChart" 
        width="900" 
        height="180"></canvas>
{{< / highlight >}}

No we are ready for our preview page.

#### Preview in Browser

The layout on tablet display is illustrated in the following figure:

![Bootstrap5: More Dashboard Example][106-qb-dashboard-md]

-- -- --

### 4: Admin LTE

> Third Party

-- -- --

### What is Next 🤔?

[//]: <> ( -- -- -- links below -- -- -- )

[local-sidebar]:        {{< baseurl >}}frontend/2021/06/18/bootstrap5-sidebar/

[tutor-step-10]:        {{< tutor-html-bs5 >}}/step-10/
[tutor-step-10-views]:  {{< tutor-html-bs5 >}}/step-10/views/
[tutor-step-10-js]:     {{< tutor-html-bs5 >}}/step-10/assets/js/
[tutor-step-10-css]:    {{< tutor-html-bs5 >}}/step-10/assets/css/

[//]: <> ( -- -- -- links below -- -- -- )

[106-nerd-views]:       {{< baseurl >}}assets/posts/frontend/2021/08/106-nerd-views.png

[105-s-dashboard-head]: {{< baseurl >}}assets/posts/frontend/2021/08/105-s-dashboard-head.png

[105-njk-v-dashbrd-01]: {{< baseurl >}}assets/posts/frontend/2021/08/105-njk-v-dashboard-01.png
[105-njk-v-dashbrd-02]: {{< baseurl >}}assets/posts/frontend/2021/08/105-njk-v-dashboard-02.png
[106-njk-v-dashbrd-01]: {{< baseurl >}}assets/posts/frontend/2021/08/106-njk-v-dashboard-01.png
[106-njk-v-dashbrd-02]: {{< baseurl >}}assets/posts/frontend/2021/08/106-njk-v-dashboard-02.png
[106-njk-v-dashbrd-03]: {{< baseurl >}}assets/posts/frontend/2021/08/106-njk-v-dashboard-03.png

[105-h-links]:          {{< baseurl >}}assets/posts/frontend/2021/08/105-h-links-dashboard.png

[105-c-main-01]:        {{< baseurl >}}assets/posts/frontend/2021/08/105-njk-c-main-01.png

[037-qb-sidebar-ibs-sm]:{{< baseurl >}}assets/posts/frontend/2021/08/037-qb-sidebar-ibs-sm.png

[105-qb-dashboard-md]:  {{< baseurl >}}assets/posts/frontend/2021/08/105-qb-dashboard-md.png
[106-qb-dashboard-md]:  {{< baseurl >}}assets/posts/frontend/2021/08/106-qb-dashboard-md.png

[//]: <> ( -- -- -- links below -- -- -- )

[njk-s-dashboard-head]: {{< tutor-html-bs5 >}}/step-10/views/shared/dashboard-header.njk

[njk-v-105-dashboard]:  {{< tutor-html-bs5 >}}/step-10/views/105-dashboard.njk
[njk-v-106-dashboard]:  {{< tutor-html-bs5 >}}/step-10/views/106-dashboard.njk

[njk-105-h-links]:      {{< tutor-html-bs5 >}}/step-08/views/heads/links-dashboard.njk

[njk-c-105-main]:       {{< tutor-html-bs5 >}}/step-07/views/contents/105-main.njk

[njk-c-106-main]:       {{< tutor-html-bs5 >}}/step-07/views/contents/106-main.njk
[njk-c-106-chart]:      {{< tutor-html-bs5 >}}/step-07/views/contents/106-chart.njk
[njk-c-106-chartheader]:{{< tutor-html-bs5 >}}/step-07/views/contents/106-chart-header.njk