---
type   : post
title  : "Bulma - Overview"
date   : 2019-03-01T09:17:35+07:00
slug   : bulma-overview
categories: [frontend]
tags      : [bulma, css]
keywords  : [plain html, custom theme, summary, overview]
author : epsi
opengraph:
  image: assets/site/images/topics/bulma-sass.png

toc    : "toc-2019-03-html-bulma-step"

excerpt:
  Designing real world web page using Bulma, step by step.
  Summary of article series.

---

### Preface

This article series is intended for beginner.

> Goal: Make your own theme using Bulma

With this guidance, you can make your own theme,
for use with blogging, or simple company profile.
You can also enhanced later to create,
your own portfolio based on this knowledge.

To face real life, blog building situation,
there will other guidance, based on this Bulma example.

#### Reading

Always refer to official documentation first,
before you start this real world example.

* [bulma.io/](https://bulma.io/)

#### HTML Tools

> My intention is simplicity

I'm using pure HTML instead of static site generator,
so that any beginner can follow this Bulma show case.

You can just open the file in the browser, and this will works.

#### CSS Tools

I'm using Sass to generate CSS,
with `.sass` format instead of `.scss`.
Because Bulma use `.sass` format.

Of course you can use any `.scss` format if you want,
for your web design.

#### Learning Stylesheet

> The Scope of CSS Tools

Learning CSS Frameworks is a part of this bigger picture below:

![The Scope of CSS Tools][illustration-css-tools]

-- -- --

### About Bulma

Bulma is an opensource CSS Framework.
There are other alternative as well,
such as <code>Materialize</code> or <code>Bootstrap</code>.

#### Why Bulma ?

There are a few of things actually:

*	Good documentation

	I really like the way Bulma make their documentation.
	It is very comfortable to follow.
	I'm not considering myself as web designer,
	and this Bulma documentation makes me understand a lot of stuff.

*	Modular SASS

	Compared with other alternative,
	I can see how clear Bulma structure folder is.
	I can easily alter, or create my own element or component,
	based on this partial SASS.

*	No Javascript

	Freedom to choose my Javascript.

#### Issue with Bulma

*	No complete helper

	The Bulma Developer does not want Bulma to be bloated.
	So we have to use third party SASS.
	And this shoul be no problem,
	as later we can show you easy this can be achieved.

#### Custom CSS

	Create necessary sass, as you need.
	However, still Bulma can save you,
	from writing a lot of CSS code.

#### Part of Bulma

Just read again the offical documentation.

-- -- --

### Example Showcase

#### Screenshot

I add some aestethic appeal, with more custom stylesheet.

![Bulma: Preview][image-ss-00-bulma-preview]

#### Source Code

Source code used in this tutorial, is available at:

*	[gitlab.com/epsi-rns/tutor-html-bulma][tutor-bulma-master]

This is also intended as a Demo.

#### Disclaimer

This is would not be the best blog template that you ever have.
Because I only put most common stuff,
to keep the tutorial simple.

After you learn this guidance,
you understand the fundamental skill.
Thus, a base for you, to make your own blog site.
With your imagination, you may continue,
to build your own super duper Bulma site.
Far better than, what I have achieved.

#### Table of Content

The table content is available as header on each tutorial.
There, I present a Bulma Tutorial, step by step, for beginners.

-- -- --

### Begin The Guidance

Let's get the tutorial started.

Consider continue reading [ [Bulma - Minimal][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2019/03/02/bulma-minimal/

[tutor-bulma-master]:   {{< tutor-html-bulma >}}/

[image-ss-00-bulma-preview]:    {{< assets-frontend >}}/2019/03/00-html-bulma-preview.png
[illustration-css-tools]:       {{< assets-frontend >}}/2020/03/css-tools.png
