---
type   : post
title  : "Bulma - Minimal"
date   : 2019-03-02T09:17:35+07:00
slug   : bulma-minimal
categories: [frontend]
tags      : [bulma, css]
keywords  : [plain html, custom theme, skeleton, bulma cdn]
author : epsi
opengraph:
  image: assets/site/images/topics/bulma-sass.png

toc    : "toc-2019-03-html-bulma-step"

excerpt:
  Make custom theme, step by step.
  Show minimal page in Bulma.

---

### Preface

> Goal: Using Bulma for the first time

-- -- --

### 1: Layout

No. We do not use Bulma yet.
We need to talk about something more fundamental.

#### The Document: HTML Skeleton

This tutorial use `header`, `main`, and `footer`.
There is **html5** semantic tag for this.
But this could be as simple as **html element** below.

* <code>tutor-01/11-html-layout.html</code>
  : [gitlab.com/.../11-html-layout.html][tutor-01-11-html].

{{< highlight html >}}
<html>

<head>
  <title>Letters to my Bulma</title>
</head>

<body>

  <!-- header -->
  <p><i>My one, and only dream,</i></p>
  <br/>

  <!-- main -->
  <div id="content">
    <p>There are so many things to say
    to Bulma, Chici, and that Android 18.
    I don't want to live in regrets.
    So I wrote this for my love.</p>
  </div>

  <!-- footer -->
  <br/>
  <p><i>Wishful thinking.</i></p>

</body>
</html>
{{< / highlight >}}


Open this **html artefact** in any browser,
using your favorite **file manager**.
And have a look at the result.

![html: layout][image-ss-11-layout]

-- -- --

### 2: Loading CSS

There are some alternative to load CSS.

For this tutorial. 
I suggest to use local CSS.
Because we are going to make a custom Bulma CSS

#### Using CDN Bulma CSS

It is as simple as 

{{< highlight html >}}
    <link rel="stylesheet" 
          href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.4/css/bulma.min.css">
{{< / highlight >}}

* <code>tutor-01/12-head-cdn.html</code>
  : [gitlab.com/.../12-head-cdn.html][tutor-01-12-html].

#### Directory Preparation

Now consider create directory for respective assets:

* css

![Bulma: Directory Preparation][image-ss-10-assets]

We only need `css` directory for the rest of this chapter.
But later we need other directory as well such as `images`, `js`, and `sass`.

#### Using Local Bulma CSS

You need to download `bulma.css` first from the official site.
And put the artefact to `css` directory.

{{< highlight html >}}
    <link rel="stylesheet" type="text/css" href="css/bulma.css">
{{< / highlight >}}

* <code>tutor-01/13-head-local.html</code>
  : [gitlab.com/.../13-head-local.html][tutor-01-13-html].
  
And the `bulma.css` is here:

* <code>tutor-01/css/bulma.css</code>
  : [gitlab.com/.../css/bulma.css][tutor-01-css-bulma].

#### Adding Meta

As the official guidance said.
We have to add a few tag on the `head`.

{{< highlight html >}}
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Letters to my Bulma</title>
{{< / highlight >}}

#### Bulma Example Class

We need to see if Bulma class work correctly.
Consider add this `container` and `box` class.

{{< highlight html >}}
  <!-- main -->
  <main role="main" class="container">
    <div class="box">
      <p>There are so many things to say
      to Bulma, Chici, and that Android 18.
      I don't want to live in regrets.
      So I wrote this for my love.</p>
    </div>
  </main>
{{< / highlight >}}

#### The Preview

And have a look at the result by opening the file in Browser.

![css: bulma][image-ss-13-local]

We should have white background by now, and also a box inside it.

-- -- --

### 3: Bulma: Example Class

Now consider having fun for while.
And give more example class, and see what happened.

* <code>tutor-01/14-hero.html</code>
  : [gitlab.com/.../14-hero.html][tutor-01-14-html].

#### As Header

What lies on header part is up to you.

{{< highlight html >}}
  <!-- header -->
  <header>
    <div class="hero is-info">
      <div class="hero-body">
        <article class="container">

               <p><i>My one, and only dream,</i></p>

        </article>
      </div>
    </div>
  </header>
  <br/>
{{< / highlight >}}

#### As Footer

What lies on footer part is also up to you.

{{< highlight html >}}
  <!-- footer -->
  <br/>
  <footer class="footer">
    <div class="content has-text-centered">
      <p><i>Wishful thinking.</i></p>
    </div>
  </footer>
{{< / highlight >}}

#### Screenshot

And have fun with the result.
Consider open the file in Browser form file manager.

![css: hero][image-ss-14-hero]

-- -- --

### 4: Bulma: Navbar Class

For the rest of this show case,
I'm using navbar for both `header` and `footer`.

* <code>tutor-01/15-navbar.html</code>
  : [gitlab.com/.../15-navbar.html][tutor-01-15-html].

#### As Header

Navbar lies on header part.

{{< highlight html >}}
  <!-- header -->
  <nav class="navbar is-fixed-top is-dark">
    <div class="navbar-brand">
      <a class="navbar-item">
        Home
      </a>
    </div>
  </nav>
{{< / highlight >}}

#### As Footer

Navbar also lies on footer part.

{{< highlight html >}}
  <!-- footer -->
  <footer class="navbar is-fixed-bottom 
        is-dark has-text-centered is-vcentered">
    <div class="column">
      &copy; 2019.
    </div>
  </footer>
{{< / highlight >}}

#### Additional CSS

To have proper **layout**,
this navbar require additional CSS.

* <code>tutor-01/css/main.css</code>
  : [gitlab.com/.../css/main.css][tutor-01-css-main].

{{< highlight css >}}
.layout-base {
  padding-top: 5rem;
  padding-bottom: 5rem;
}
{{< / highlight >}}

You should define `styles` on `<head>` element.

{{< highlight html >}}
    <link rel="stylesheet" type="text/css" href="css/bulma.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
{{< / highlight >}}

#### Screenshot

And have fun with the result.
Consider open the file in Browser form file manager.

![css: navbar][image-ss-15-navbar]

#### Fix Navbar

Notice that Bulma has these navbar class to handle `padding`

* `has-navbar-fixed-top` or,

* `has-navbar-fixed-bottom`

We can choose to use those navbar class above,
or simply ignore it for more flexibility while using custom CSS.

-- -- --

### 5: Summary

As a summary from this chapter,
all the **html element** is as shown below.

{{< highlight html >}}
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Letters to my Bulma</title>

    <link rel="stylesheet" type="text/css" href="css/bulma.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>

  <!-- header -->
  <nav class="navbar is-fixed-top is-dark">
    <div class="navbar-brand">
      <a class="navbar-item">
        Home
      </a>
    </div>
  </nav>

  <!-- main -->
  <div class="layout-base">
    <main role="main" class="container">
      <div class="box">
        <p>There are so many things to say
        to Bulma, Chici, and that Android 18.
        I don't want to live in regrets.
        So I wrote this for my love.</p>
      </div>
    </main>
  </div>

  <!-- footer -->
  <footer class="navbar is-fixed-bottom 
        is-dark has-text-centered is-vcentered">
    <div class="column">
      &copy; 2019.
    </div>
  </footer>

</body>
</html>
{{< / highlight >}}

-- -- --

### What is Next ?

There are, some interesting topic
about <code>Navigation Bar</code> in Bulma. 
Consider continue reading [ [Bulma - Navigation Bar][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2019/03/03/bulma-navbar/

[tutor-01-11-html]:     {{< tutor-html-bulma >}}/tutor-01/11-html-layout.html
[tutor-01-12-html]:     {{< tutor-html-bulma >}}/tutor-01/12-head-cdn.html
[tutor-01-13-html]:     {{< tutor-html-bulma >}}/tutor-01/13-head-local.html
[tutor-01-14-html]:     {{< tutor-html-bulma >}}/tutor-01/14-hero.html
[tutor-01-15-html]:     {{< tutor-html-bulma >}}/tutor-01/15-navbar.html

[tutor-01-css]:         {{< tutor-html-bulma >}}/tutor-01/css/
[tutor-01-css-bulma]:   {{< tutor-html-bulma >}}/tutor-01/css/bulma.css
[tutor-01-css-main]:    {{< tutor-html-bulma >}}/tutor-01/css/main.css

[image-ss-10-assets]:   {{< assets-frontend >}}/2019/03/10-tree-assets.png

[image-ss-11-layout]:   {{< assets-frontend >}}/2019/03/11-html-layout.png
[image-ss-13-local]:    {{< assets-frontend >}}/2019/03/13-head-local.png
[image-ss-14-hero]:     {{< assets-frontend >}}/2019/03/14-hero.png
[image-ss-15-navbar]:   {{< assets-frontend >}}/2019/03/15-navbar.png
