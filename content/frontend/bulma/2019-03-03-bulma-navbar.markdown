---
type   : post
title  : "Bulma - Navigation Bar"
date   : 2019-03-03T09:17:35+07:00
slug   : bulma-navbar
categories: [frontend]
tags      : [bulma, css]
keywords  : [plain html, custom theme, navigation bar, jquery, vue, fontawesome]
author : epsi
opengraph:
  image: assets/site/images/topics/bulma-sass.png

toc    : "toc-2019-03-html-bulma-step"

excerpt:
  Make custom theme, step by step.
  Regular navigation bar in Bulma in Depth.

---

### Preface

> Goal: Common navigation bar.

It is smilar to bootstrap,
that would make your site the same as any other website.

-- -- --

### 1: Minimal

Having Bulma navigation bar is simple.
You can examine each class in Bulma official documentaion.

* <code>tutor-02/21-navbar-minimal.html</code>
  : [gitlab.com/.../21-navbar-minimal.html][tutor-02-21-html].

{{< highlight html >}}
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Letters to my Bulma</title>

    <link rel="stylesheet" type="text/css" href="css/bulma.css">

</head>
<body>

  <!-- header -->
  <nav class="navbar is-fixed-top is-dark">
    <div class="navbar-brand">
      <a class="navbar-item">
        Home
      </a>
    </div>
  </nav>

</body>
</html>
{{< / highlight >}}

You can see the respective result as below figure.

![Bulma Navigation Bar: Minimal][image-ss-21-minimal]

#### Directory Preparation

Now consider create directory for respective assets:

* css

* js

* images

![Bulma: Directory Preparation][image-ss-20-assets]

We are going to need them for the rest of this chapter.

-- -- --

###  2: Simple

Consider change the `body` part of html a bit.
Put some stuff in the navigation header such as logo.

#### Skeleton

Here I represent `navbar-menu` for use with **responsive design later**.

* <code>tutor-02/22-navbar-simple.html</code>
  : [gitlab.com/.../22-navbar-simple.html][tutor-02-22-html].

{{< highlight html >}}
<body>

  <!-- header -->
  <nav role="navigation" aria-label="main navigation"
       class="navbar is-fixed-top is-dark">
    <div class="navbar-brand">
      <a class="navbar-item" href="#">
        <img src="images/logo-gear.png" alt="Home" />
      </a>
      <a class="navbar-item">
        Blog
      </a>
    </div>

    <div id="navbarBulma" class="navbar-menu">
      <div class="navbar-start">
        <a class="navbar-item">
          About
        </a>
      </div>
    </div>
  </nav>

</body>
{{< / highlight >}}

You can have a look at the expected result in image below:

![Bulma Navigation Bar: Simple][image-ss-22-simple]

#### Assets: Logo

You are free to use your custom logo.
I actually made some logo,
and here we can use it as an example for this guidance.

* <code>tutor-02/images/logo-gear.png</code>
  : [gitlab.com/.../images/logo-gear.png][tutor-images-logo-gear]

![Bulma Navigation Bar: Custom Logo][image-logo-gear]

* Size [width x height]: 96px * 96px

-- -- --

### 3: Long

> Stuffed with Items

Consider populate the navigation bar with more menu and stuff.
The item is self explanatory.

* <code>tutor-02/23-navbar-long.html</code>
  : [gitlab.com/.../23-navbar-long.html][tutor-02-23-html].

{{< highlight html >}}
<body>

  <!-- header -->
  <nav role="navigation" aria-label="main navigation"
       class="navbar is-fixed-top is-dark">
    <div class="navbar-brand">
      <a class="navbar-item" href="#">
        <img src="images/logo-gear.png" alt="Home" />
      </a>
      <a class="navbar-item">
        Blog
      </a>
    </div>

    <div id="navbarBulma" class="navbar-menu">
      <div class="navbar-start">
        <div class="navbar-item has-dropdown is-hoverable">
          <a class="navbar-link">
            Archives
          </a>

          <div class="navbar-dropdown">
            <a class="navbar-item">
              By Tags
            </a>
            <a class="navbar-item">
              By Category
            </a>
            <hr class="navbar-divider">
            <a class="navbar-item">
              By Date
            </a>
          </div>
        </div>

        <a class="navbar-item">
          About
        </a>
      </div>

      <div class="navbar-end">
        <form class="is-marginless" action="/pages/search/" method="get">
        <div class="navbar-item">
          <input class="" type="text" name="q"
            placeholder="Search..." aria-label="Search">
          &nbsp;
          <button class="button is-light" 
            type="submit">Search</button>
        </div>
        </form>
      </div>

    </div>
  </nav>

</body>
{{< / highlight >}}

You can see the respective result as below figure.

![Bulma Navigation Bar: Long Stuffed][image-ss-23-long]

Notice that The original screenshot took longer window.
I changed the sass a bit to make the screenshot fit with `640px;`.

#### Dropdown Menu

Just like bootstrap, Bulma also has the dropdown menu.

{{< highlight html >}}
    <div id="navbarBulma" class="navbar-menu">
      <div class="navbar-start">
        <div class="navbar-item has-dropdown is-hoverable">
          <a class="navbar-link">
            Archives
          </a>

          <div class="navbar-dropdown">
            <a class="navbar-item">
              By Tags
            </a>
            <a class="navbar-item">
              By Category
            </a>
            <hr class="navbar-divider">
            <a class="navbar-item">
              By Date
            </a>
          </div>
        </div>

        <a class="navbar-item">
          About
        </a>
      </div>
    </div>
{{< / highlight >}}

![Bulma Navigation Bar: Dropdown Menu][image-ss-23-dropdown]

#### Responsive

Consider have a look at the result in `480x` screen.

![Bulma Navigation Bar: Responsive][image-ss-23-responsive]

Notice that the `Blog` link will always be shown.
If you want to hide, you can put this link in `navbar-menu`.

-- -- --

### 4: jQuery

Bulma does not come with any javascript.
Or you say Bulma is javascript agnostic.
In order for the burger button to works,
we need javascript however.

* <code>tutor-02/24-navbar-jquery.html</code>
  : [gitlab.com/.../24-navbar-jquery.html][tutor-02-24-html].

#### Prepare

Consider **jQuery** in the `<head>` element.

{{< highlight html >}}
<head>
    ...

    <link rel="stylesheet" type="text/css" href="css/bulma.css">
    <script src="js/jquery-slim.min.js"></script>
    <script src="js/bulma-burger-jquery.js"></script>
</head>
{{< / highlight >}}

#### Burger Javascript

The Javascript will examine respected DOM.

* <code>tutor-02/js/bulma-burger-jquery.js</code>
  : [gitlab.com/.../bulma-burger-jquery.js][tutor-02-bb-jquery].

{{< highlight javascript >}}
$(document).ready(function() {

  // Check for click events on the navbar burger icon
  $(".navbar-burger").click(function() {

      // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
      $(".navbar-burger").toggleClass("is-active");
      $(".navbar-menu").toggleClass("is-active");

  });
});
{{< / highlight >}}

#### Burger Button

Add burger button in the `<body>` element.

{{< highlight html >}}
<body>

  <!-- header -->
  <nav role="navigation" aria-label="main navigation"
       class="navbar is-fixed-top is-dark">
    <div class="navbar-brand">
      <a class="navbar-item" href="#">
        <img src="images/logo-gear.png" alt="Home" />
      </a>
      <a class="navbar-item">
        Blog
      </a>

      <a role="button" class="navbar-burger burger"
         aria-label="menu" aria-expanded="false" data-target="navbarBulma">
        <span aria-hidden="true"></span>
        <span aria-hidden="true"></span>
        <span aria-hidden="true"></span>
      </a>
    </div>

    <div id="navbarBulma" class="navbar-menu">
    ...
    </div>
  </nav>

</body>
{{< / highlight >}}

#### Responsive

Consider have a look at the result in `480x` screen.

![Bulma Navigation Bar: Burger Button][image-ss-24-burger]

And the expected result of clicked will be:

![Bulma Navigation Bar: jQuery Dropdown Menu][image-ss-24-dropdown]

-- -- --

### 5: Vue

The decade of jQuery has been left in the past.
Now we have a some better javascript framework as an option.

* <code>tutor-02/25-navbar-vue.html</code>
  : [gitlab.com/.../25-navbar-vue.html][tutor-02-25-html].

#### Prepare

Consider **Vue** in the `<head>` element.

{{< highlight html >}}
<head>
    ...

    <script src="js/vue.min.js"></script>
</head>
{{< / highlight >}}

And load in the end of `<body>` element.

{{< highlight html >}}
  ...

  <!-- JavaScript -->
  <!-- Placed at the end of the document -->
  <script src="js/bulma-burger-vue.js"></script>
</body>
</html>
{{< / highlight >}}

#### Burger Javascript

The Javascript will examine the `#navbar-vue-app` element.

* <code>tutor-02/js/bulma-burger-vue.js</code>
  : [gitlab.com/.../bulma-burger-vue.js][tutor-02-bb-vue].

{{< highlight javascript >}}
new Vue({
  el: '#navbar-vue-app',
  data: {
    isOpen: false
  }
});
{{< / highlight >}}

I got the script from

* [belowthebenthic.com/bulma-burger](http://belowthebenthic.com/bulma-burger/)

#### Burger Button

The burger button in the `<nav>` element is a little bit different.

{{< highlight html >}}
      <a role="button" class="navbar-burger burger" 
         aria-label="menu" aria-expanded="false" data-target="navbarBulma"
         @click="isOpen = !isOpen" v-bind:class="{'is-active': isOpen}">
        <span aria-hidden="true"></span>
        <span aria-hidden="true"></span>
        <span aria-hidden="true"></span>
      </a>
{{< / highlight >}}

#### Navigation Menu

The navigation menu in the `<nav>` element is also little bit different.

{{< highlight html >}}
    <div id="navbarBulma" class="navbar-menu"
         v-bind:class="{'is-active': isOpen}">
      <div class="navbar-start">
        ...
      </div>
      <div class="navbar-end">
        ...
      </div>
    </div>
{{< / highlight >}}

#### Responsive

The expected result of clicked button burger is similar.

![Bulma Navigation Bar: Vue Dropdown Menu][image-ss-25-dropdown]

-- -- --

### 6: Plain

If you want your page to be lightweight,
without any javascript framework,
you can use plain javascript.

* <code>tutor-02/26-navbar-plain.html</code>
  : [gitlab.com/.../26-navbar-plain.html][tutor-02-26-html].

#### Prepare

Consider `js/bulma-burger-plain.js` in the `<head>` element.
This is a custom tailor made, plain vanilla javascript,
to handle burger menu.

{{< highlight html >}}
<head>
  ...

  <link rel="stylesheet" type="text/css" href="css/bulma.css">
  <script src="js/bulma-burger-plain.js"></script>
</head>
{{< / highlight >}}

#### Burger Javascript

The Javascript will examine respected DOM.

* <code>tutor-02/js/bulma-burger-plain.js</code>
  : [gitlab.com/.../bulma-burger-plain.js][tutor-02-bb-plain].

{{< highlight javascript >}}
document.addEventListener("DOMContentLoaded", function(event) { 
  // Check for click events on the navbar burger icon
  var navbarBurgers = document.getElementsByClassName("navbar-burger");
  var navbarMenus   = document.getElementsByClassName("navbar-menu");
  
  navbarBurgers[0].onclick = function() {
    navbarBurgers[0].classList.toggle("is-active");
    navbarMenus[0].classList.toggle("is-active");
    console.log('Toggle is-active class in navbar burger menu');
  } 
});
{{< / highlight >}}

Or even alternatively:

{{< highlight javascript >}}
document.addEventListener("DOMContentLoaded", function(event) { 
  // Check for click events on the navbar burger icon
  const navbarBurger = document.getElementsByClassName("navbar-burger")[0];
  const navbarBulma  = document.getElementById("navbarBulma");

  // Toggle if navbar menu is open or closed
  function toggleMenu() {
    navbarBurger.classList.toggle("is-active");
    navbarBulma.classList.toggle("is-active");
    console.log('Toggle is-active class in navbar burger menu');
  }

  // Event listeners
  navbarBurger.addEventListener('click', toggleMenu, false);
});
{{< / highlight >}}

#### Burger Button

The same document structure with `jquery`.

-- -- --

### 7: FontAwesome

Bulma also does not come with any font icon.
It takes manual setup.

* <code>tutor-02/27-navbar-awesome.html</code>
  : [gitlab.com/.../27-navbar-awesome.html][tutor-02-27-html].

#### Prepare

Consider **FontAwesome** as CDN in the `<head>` element.

{{< highlight html >}}
    <link rel="stylesheet" type="text/css" href="css/bulma.css">
    <link rel="stylesheet" 
          href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" 
          integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" 
          crossorigin="anonymous">
{{< / highlight >}}

#### Example

Consider this dropdown menu as example

{{< highlight html >}}
        <div class="navbar-item has-dropdown is-hoverable">
          <a class="navbar-link">
            Archives
          </a>

          <div class="navbar-dropdown">
            <a class="navbar-item">
              <span class="fa fa-tags"></span>&nbsp;By Tags
            </a>
            <a class="navbar-item">
              <span class="fa fa-folder"></span>&nbsp;By Category
            </a>
            <hr class="navbar-divider">
            <a class="navbar-item">
              <span class="fa fa-calendar"></span>&nbsp;By Date
            </a>
          </div>
        </div>
{{< / highlight >}}

And the expected result will be:

![Bulma Navigation Bar: FontAwesome in Dropdown Menu][image-ss-27-awesome]

-- -- --

### What is Next ?

I think that is all, about <code>Navigation Bar</code> in Bulma. 

There is, an introduction about <code>SASS</code> in Bulma. 
Consider continue reading [ [Bulma SASS - Introduction][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2019/03/04/bulma-sass-intro/

[tutor-02-21-html]:     {{< tutor-html-bulma >}}/tutor-02/21-navbar-minimal.html
[tutor-02-22-html]:     {{< tutor-html-bulma >}}/tutor-02/22-navbar-simple.html
[tutor-02-23-html]:     {{< tutor-html-bulma >}}/tutor-02/23-navbar-long.html
[tutor-02-24-html]:     {{< tutor-html-bulma >}}/tutor-02/24-navbar-jquery.html
[tutor-02-25-html]:     {{< tutor-html-bulma >}}/tutor-02/25-navbar-vue.html
[tutor-02-26-html]:     {{< tutor-html-bulma >}}/tutor-02/26-navbar-plain.html
[tutor-02-27-html]:     {{< tutor-html-bulma >}}/tutor-02/27-navbar-awesome.html

[tutor-02-bb-jquery]:   {{< tutor-html-bulma >}}/tutor-02/js/bulma-burger-jquery.js
[tutor-02-bb-vue]:      {{< tutor-html-bulma >}}/tutor-02/js/bulma-burger-vue.js
[tutor-02-bb-plain]:    {{< tutor-html-bulma >}}/tutor-02/js/bulma-burger-plain.js

[image-ss-20-assets]:       {{< assets-frontend >}}/2019/03/20-tree-assets.png
[image-ss-21-minimal]:      {{< assets-frontend >}}/2019/03/21-navbar-minimal.png
[image-ss-22-simple]:       {{< assets-frontend >}}/2019/03/22-navbar-simple.png
[image-ss-23-long]:         {{< assets-frontend >}}/2019/03/23-navbar-long.png
[image-ss-23-dropdown]:     {{< assets-frontend >}}/2019/03/23-navbar-menu-dropdown.png
[image-ss-23-responsive]:   {{< assets-frontend >}}/2019/03/23-navbar-responsive.png
[image-ss-24-burger]:       {{< assets-frontend >}}/2019/03/24-navbar-burger.png
[image-ss-24-dropdown]:     {{< assets-frontend >}}/2019/03/24-navbar-jquery-dropdown.png
[image-ss-25-dropdown]:     {{< assets-frontend >}}/2019/03/25-navbar-vue-dropdown.png
[image-ss-27-awesome]:      {{< assets-frontend >}}/2019/03/27-navbar-awesome.png

[tutor-images-logo-gear]:   {{< tutor-html-bulma >}}/tutor-02/images/logo-gear.png
[image-logo-gear]:          {{< assets-frontend >}}/2019/03/logo-gear.png
