---
type   : post
title  : "Bulma SASS - Introduction"
date   : 2019-03-04T09:17:35+07:00
slug   : bulma-sass-intro
categories: [frontend]
tags      : [bulma, sass]
keywords  : [plain html, custom theme, custom sass, altering bulma]
author : epsi
opengraph:
  image: assets/site/images/topics/bulma-sass.png

toc    : "toc-2019-03-html-bulma-step"

excerpt:
  Make custom theme, step by step.
  Using Bulma SASS for the first time

---

### Preface

> Goal: Using Bulma SASS for the first time

Understanding SASS is a must for theme maker.

#### Reading

If you do not have any idea about SASS you should read this first:

* <https://sass-lang.com/>

-- -- --

### 1: Overview

First thing first.

#### Directory Preparation

Add SASS to your current working directory.

* <code>tutor-03/sass/</code>
  : [gitlab.com/.../sass/][tutor-03-sass].

And add vendors directory.

![Bulma SASS: Directory Preparation][image-ss-30-tree-vendors]

We are going to need them for the rest of this chapter.

#### Download Bulma SASS

Add Bulma to your vendors directory.

* <code>tutor-03/sass/vendors/bulma/</code>
  : [gitlab.com/.../sass/vendors/bulma/][tutor-03-v-bulma].

{{< highlight bash >}}
$ tree -d tutor-03/sass 
tutor-03/sass
└── vendors
    └── bulma
        ├── base
        ├── components
        ├── elements
        ├── grid
        ├── layout
        └── utilities

8 directories
{{< / highlight >}}

Now you should see inside Bulma.
A tidy modular SASS.

![Bulma SASS: Inside Bulma][image-ss-30-tree-bulma]

#### Custom SASS

Now, it is about the right time to create custom SASS.
Let's say we create this

* <code>tutor-03/sass/css-30/bulma.sass</code>
  : [gitlab.com/.../sass/css-30/bulma.sass][tutor-03-css-30-bulma].

{{< highlight scss >}}
@import "vendors/bulma"
{{< / highlight >}}

#### SASS Tools

You may compile with any SASS compiler that you need.

* [SASS Compiler][sass-compiler]

I made a `bash` shortcut to my `dart sass` binary.

{{< highlight bash >}}
alias dart-sass='/media/Works/bin/dart-sass/sass'
{{< / highlight >}}

Windows user can use `chocolatey` to install `sass`.

#### Compile SASS

Consider compile to `css` directory.

{{< highlight bash >}}
$ cd tutor-03
$ dart-sass -I sass sass/css-30:css/
{{< / highlight >}}

or better

{{< highlight bash >}}
$ dart-sass --watch -I sass sass/css-30:css/ 
Compiled sass/css-30/bulma.sass to css/bulma.css.
Sass is watching for changes. Press Ctrl-C to stop.
{{< / highlight >}}

![Bulma SASS: Compile][image-ss-30-sass-compile]

Now you should see generated CSS.

![Bulma SASS: Generated CSS][image-ss-30-generated]

This should be the same as regular official Bulma.

-- -- --

### 2: Example

#### The Sylesheet: Altering Bulma

Altering standard Bulma is simple.
Consider change the behaviour by setting initial variable.

* <code>tutor-03/sass/css-31/bulma.sass</code>
  : [gitlab.com/.../sass/css-31/bulma.sass][tutor-03-css-31-bulma].

{{< highlight scss >}}
@import "initial-variables"
@import "vendors/bulma"
@import "derived-variables"
{{< / highlight >}}

* <code>tutor-03/sass/css-31/_initial-variables.sass</code>
  : [gitlab.com/.../sass/css-31/_initial-variables.sass][tutor-03-css-31-initial].
  
{{< highlight scss >}}
// Initial Variables

$pink:  #e83e8c !default
$primary:  lighten($pink, 20%)

$navbar-breakpoint: 769px
{{< / highlight >}}

* <code>tutor-03/sass/css-31/_derived-variables.sass</code>
  : [gitlab.com/.../sass/css-31/_derived-variables.sass][tutor-03-css-31-derived].

{{< highlight scss >}}
// Derived Variables
{{< / highlight >}}

#### The Sylesheet: Additional SASS

* <code>tutor-03/sass/css-31/main.sass</code>
  : [gitlab.com/.../sass/css-31/main.sass][tutor-03-css-31-main].

{{< highlight scss >}}
.layout-base
  padding-top: 2rem
  padding-bottom: 5rem
{{< / highlight >}}

{{< highlight bash >}}
$ dart-sass --watch -I sass sass/css-30:css/ 
Compiled sass/css-31/bulma.sass to css/bulma.css.
Compiled sass/css-31/main.sass to css/main.css.
Sass is watching for changes. Press Ctrl-C to stop.
{{< / highlight >}}

![Bulma SASS: Compile][image-ss-30-sass-compile]

You should see the result of the compilation in

* <code>tutor-03/css/main.css</code>

{{< highlight css >}}
.layout-base {
  padding-top: 2rem;
  padding-bottom: 5rem;
}

/*# sourceMappingURL=main.css.map */
{{< / highlight >}}

#### The Document: Example Page.

What good is it SASS compilation without example page?
Here we are, I represent a page, complete with header (Navigation Bar),
main content and footer.

* <code>tutor-03/31-navbar.html</code>
  : [gitlab.com/.../31-navbar.html][tutor-03-31-html].

{{< highlight html >}}
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Letters to my Bulma</title>

    <link rel="stylesheet" type="text/css" href="css/bulma.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">

    <script src="js/vue.min.js"></script>
</head>
<body class="has-navbar-fixed-top">

  <!-- header -->
  <nav role="navigation" aria-label="main navigation"
       class="navbar is-fixed-top is-primary maxwidth"
       id="navbar-vue-app">
    <div class="navbar-brand">
      <a class="navbar-item">
        Home
      </a>

      <a role="button" class="navbar-burger burger" 
         aria-label="menu" aria-expanded="false" data-target="navbarBulma"
         @click="isOpen = !isOpen" v-bind:class="{'is-active': isOpen}">
        <span aria-hidden="true"></span>
        <span aria-hidden="true"></span>
        <span aria-hidden="true"></span>
      </a>
    </div>

    <div id="navbarBulma" class="navbar-menu"
         v-bind:class="{'is-active': isOpen}">
      <div class="navbar-start">
      </div>
      <div class="navbar-end">
        <form class="is-marginless" action="/pages/search/" method="get">
        <div class="navbar-item">
          <input class="" type="text" name="q"
            placeholder="Search..." aria-label="Search">
          &nbsp;
          <button class="button is-light" 
            type="submit">Search</button>
        </div>
        </form>
      </div>
    </div>
  </nav>

  <!-- main -->
  <div class="layout-base maxwidth">
    <main role="main" class="container">
      <div class="box">
        <p>There are so many things to say
        to Bulma, Chici, and that Android 18.
        I don't want to live in regrets.
        So I wrote this for my love.</p>
      </div>
    </main>
  </div>

  <!-- footer -->
  <footer class="site-footer">
    <div class="navbar is-fixed-bottom maxwidth
          is-primary has-text-centered is-vcentered">
      <div class="column">
        &copy; 2019.
      </div>
    </div>
  </footer>

  <!-- JavaScript -->
  <!-- Placed at the end of the document -->
  <script src="js/bulma-burger-vue.js"></script>
</body>
</html>
{{< / highlight >}}

You can have a look at the expected result in image below:

![Bulma SASS: Example: Navigation Bar][image-ss-31-navbar]

-- -- --

### What is Next ?

There is, a topic about <code>Theme</code> in Bulma.
From using ready to use theme, and adapt Bulma class into your own.
Consider continue reading [ [Bulma SASS - Theme][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2019/03/05/bulma-sass-theme/
[sass-compiler]:        https://epsi-rns.gitlab.io/frontend/2019/01/13/sass-common-compiler/

[tutor-03-31-html]:     {{< tutor-html-bulma >}}/tutor-03/31-navbar.html

[tutor-03-sass]:         {{< tutor-html-bulma >}}/tutor-03/sass/
[tutor-03-v-bulma]:      {{< tutor-html-bulma >}}/tutor-03/sass/vendors/bulma/
[tutor-03-css-30-bulma]: {{< tutor-html-bulma >}}/tutor-03/sass/css-30/bulma.sass
[tutor-03-css-31-bulma]: {{< tutor-html-bulma >}}/tutor-03/sass/css-31/bulma.sass
[tutor-03-css-31-main]:  {{< tutor-html-bulma >}}/tutor-03/sass/css-31/main.sass
[tutor-03-css-31-initial]:  {{< tutor-html-bulma >}}/tutor-03/sass/css-31/_initial-variables.sass
[tutor-03-css-31-derived]:  {{< tutor-html-bulma >}}/tutor-03/sass/css-31/_derived-variables.sass

[image-ss-30-tree-vendors]: {{< assets-frontend >}}/2019/03/30-tree-assets-vendors.png
[image-ss-30-tree-bulma]:   {{< assets-frontend >}}/2019/03/30-tree-assets-vendors-bulma.png
[image-ss-30-tree-sass]:    {{< assets-frontend >}}/2019/03/30-tree-assets-css.png
[image-ss-30-sass-compile]: {{< assets-frontend >}}/2019/03/30-sass-compile.png
[image-ss-30-generated]:    {{< assets-frontend >}}/2019/03/30-generated-css.png
[image-ss-30-assets-sass]:  {{< assets-frontend >}}/2019/03/30-tree-assets-sass.png

[image-ss-31-sass-compile]: {{< assets-frontend >}}/2019/03/31-sass-compile.png
[image-ss-31-navbar]:       {{< assets-frontend >}}/2019/03/31-navbar.png
