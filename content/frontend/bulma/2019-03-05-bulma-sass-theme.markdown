---
type   : post
title  : "Bulma SASS - Inside a Theme"
date   : 2019-03-05T09:17:35+07:00
slug   : bulma-sass-theme
categories: [frontend]
tags      : [bulma, sass]
keywords  : [plain html, custom theme, modular sass, swatch, fontawesome]
author : epsi
opengraph:
  image: assets/site/images/topics/bulma-sass.png

toc    : "toc-2019-03-html-bulma-step"

excerpt:
  More about custom theme, step by step.
  Miscellanous knowledge before you make your custom theme.

---

### Preface

> Goal: Miscellanous knowledge before you make your custom theme.

This article is integral part of previous article.
You should read previous article before continue.

-- -- --

### 1: Swatch

Working with theme is fun.

![Bulma SASS: Bulmaswatch Animation][image-ss-32-swatch-animation]

#### The Sylesheet: Original Code

There is a ready to used Bulma theme, port from Bootstrap Swatch.

* [jenil.github.io/bulmaswatch/](https://jenil.github.io/bulmaswatch/)

Or the source code, if you prefer:

* [github.com/jenil/bulmaswatch](https://github.com/jenil/bulmaswatch)

Consider have a look at the original code,
at <code>cerulean/bulmaswatch.scss</code>.

{{< highlight scss >}}
/*! bulmaswatch v0.7.2 | MIT License */
@import "variables";
@import "bulma";
@import "overrides";
{{< / highlight >}}

#### The Sylesheet: Adapted Code

Now for use with our project, adapt with our directory structure.

* <code>tutor-03/sass/bulmaswatch/cerulean/bulmaswatch.scss</code>.
  : [gitlab.com/.../cerulean/bulmaswatch.scss][tutor-03-cerulean].

{{< highlight scss >}}
@import "variables";
@import "vendors/bulma";
@import "overrides";
{{< / highlight >}}

Consider compile

{{< highlight bash >}}
$ dart-sass --watch -I sass sass/bulmaswatch:css/swatch --style=compressed --no-source-map
{{< / highlight >}}

![Bulma SASS: Compile Bulmaswatch][image-ss-32-compile-swatch]

#### The Document: Example

Consider using our last example, except change the `bulma.css`
to any selected `bulmaswatch.css` theme.

* <code>tutor-03/32-swatch.html</code>
  : [gitlab.com/.../32-swatch.html][tutor-03-32-html].

{{< highlight html >}}
    <link rel="stylesheet" type="text/css" href="css/swatch/cerulean/bulmaswatch.css">
{{< / highlight >}}

And open the html artefact using file manager.

![Bulma SASS: Bulmaswatch Theme][image-ss-32-bulmaswatch]

-- -- --

### 2: Modular SASS

You can add as many additional custom SASS in your theme.

#### The Sylesheet: Some Sources

For example this:

* <code>tutor-03/sass/css-33/_layout.sass</code>.
  : [gitlab.com/.../sass/css-33/_layout.sass][tutor-03-css-33-layout].

{{< highlight scss >}}
html
  height: 100%

body
  min-height: 100%
  
.maxwidth
  margin-right: 10px
  margin-left: 10px

+desktop
  .maxwidth
    margin-right: auto
    margin-left: auto
    max-width: 1346px   // 1366 - 10 -10

.layout-base
  padding-top: 5rem

footer.site-footer
  padding-top: 5rem
{{< / highlight >}}

I put a nice background, using subtle pattern.

* <code>tutor-03/sass/css-33/_decoration.sass</code>.
  : [gitlab.com/.../sass/css-33/_decoration.sass][tutor-03-css-33-decoration].

{{< highlight scss >}}
body
  background-image: url("../images/funky-lines.png")
{{< / highlight >}}

And put that sass artefact in main css.

* <code>tutor-03/sass/css-33/main.sass</code>
  : [gitlab.com/.../sass/css-33/main.sass][tutor-03-css-33-main].

{{< highlight scss >}}
@import "vendors/bulma/utilities/mixins"
@import "layout"
@import "decoration"
{{< / highlight >}}

#### The Sylesheet: CSS Result

Consider compile as:

{{< highlight bash >}}
$ dart-sass --watch -I sass sass/css-33:css/
Compiled sass/css-33/main.sass to css/main.css.
Sass is watching for changes. Press Ctrl-C to stop.
{{< / highlight >}}

And the result would be:

{{< highlight css >}}
html {
  height: 100%;
}

body {
  min-height: 100%;
}

.maxwidth {
  margin-right: 10px;
  margin-left: 10px;
}

@media screen and (min-width: 1088px) {
  .maxwidth {
    margin-right: auto;
    margin-left: auto;
    max-width: 1346px;
  }
}
.layout-base {
  padding-top: 5rem;
}

footer.site-footer {
  padding-top: 5rem;
}

body {
  background-image: url("../images/funky-lines.png");
}

/*# sourceMappingURL=main.css.map */
{{< / highlight >}}

#### Maxwidth

And, what is this `maxwidth` anyway ?

I have a smartphone, tablet, sometimes medium screen,
and mostly I'm working with large screen.
What is good for my regular screen, looks ugly in large screen.
My solution is to create maxwidth,
so my content would not be stretched horizontally.

![Bulma SASS: Maximum Width][image-ss-31-maxwidth]

-- -- --

### 3: FontAwesome

There are possibility that you need third party SASS in your theme,
such as `FontAwesome` or `BulmaHelpers`.

#### Download Source

Download FontAwesome SASS and put it on respective directory.

![Bulma SASS: Assets][image-ss-34-assets-sass]

#### The Sylesheet: Custom SASS

I intentionally using `.scss` format.
So you know that sass works with both format `.sass` and `.scss` withut an issue.

* <code>tutor-03/sass/css-34/fontawesome.scss</code>
  : [gitlab.com/.../sass/css-34/fontawesome.scss][tutor-03-css-34-fontawesome].

{{< highlight scss >}}
$fa-font-path:        "https://use.fontawesome.com/releases/v5.2.0/webfonts";

// Import partials from `sass_dir` (defaults to `_sass`)
@import "vendors/font-awesome-5/fontawesome";
@import "vendors/font-awesome-5/solid";
@import "vendors/font-awesome-5/brands";
{{< / highlight >}}

As you can see, I still use CDN for webfonts.
But local css.

#### The Document: Example HTML

All you need to do is, to include stylesheed,
in the `<head>` element of the html artefact.

{{< highlight html >}}
    <link rel="stylesheet" type="text/css" href="css/fontawesome.css">
{{< / highlight >}}

-- -- --

### 4: Summary

While the complete code is below:

* <code>tutor-03/34-awesome.html</code>
  : [gitlab.com/.../34-awesome.html][tutor-03-34-html].

{{< highlight html >}}
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Letters to my Bulma</title>

    <link rel="stylesheet" type="text/css" href="css/bulma.css">
    <link rel="stylesheet" type="text/css" href="css/fontawesome.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">

    <script src="js/vue.min.js"></script>
</head>
<body class="has-navbar-fixed-top">

  <!-- header -->
  <nav role="navigation" aria-label="main navigation"
       class="navbar is-fixed-top is-light maxwidth"
       id="navbar-vue-app">
    <div class="navbar-brand">
      <a class="navbar-item" href="#">
        <img src="images/logo-gear.png" alt="Home" />
      </a>

      <a role="button" class="navbar-burger burger" 
         aria-label="menu" aria-expanded="false" data-target="navbarBulma"
         @click="isOpen = !isOpen" v-bind:class="{'is-active': isOpen}">
        <span aria-hidden="true"></span>
        <span aria-hidden="true"></span>
        <span aria-hidden="true"></span>
      </a>
    </div>

    <div id="navbarBulma" class="navbar-menu"
         v-bind:class="{'is-active': isOpen}">
      <div class="navbar-start">
        <div class="navbar-item has-dropdown is-hoverable">
          <a class="navbar-link">
            Archives
          </a>

          <div class="navbar-dropdown">
            <a class="navbar-item">
              <span class="fa fa-tags"></span>&nbsp;By Tags
            </a>
            <a class="navbar-item">
              <span class="fa fa-folder"></span>&nbsp;By Category
            </a>
            <hr class="navbar-divider">
            <a class="navbar-item">
              <span class="fa fa-calendar"></span>&nbsp;By Date
            </a>
          </div>
        </div>

        <a class="navbar-item">
          About
        </a>
      </div>
      <div class="navbar-end">
        <form class="is-marginless" action="/pages/search/" method="get">
        <div class="navbar-item">
          <input class="" type="text" name="q"
            placeholder="Search..." aria-label="Search">
          &nbsp;
          <button class="button is-light" 
            type="submit">Search</button>
        </div>
        </form>
      </div>
    </div>
  </nav>

  <!-- main -->
  <div class="layout-base maxwidth">
    <main role="main" 
          class="column is-full has-background-white">
        <p>There are so many things to say
        to Bulma, Chici, and that Android 18.
        I don't want to live in regrets.
        So I wrote this for my love.</p>
    </main>
  </div>

  <!-- footer -->
  <footer class="site-footer">
    <div class="navbar is-fixed-bottom maxwidth
          is-light has-text-centered is-vcentered">
      <div class="column">
        &copy; 2019.
      </div>
    </div>
  </footer>

  <!-- JavaScript -->
  <!-- Placed at the end of the document -->
  <script src="js/bulma-burger-vue.js"></script>
</body>
</html>
{{< / highlight >}}

The respective result screenshot would be as figure below:

![Bulma SASS: Light Color][image-ss-34-light]

Not pretty yet!
We are going to make the color elegant in the next guidance.

-- -- --

### What is Next ?

More SASS customization is our next topic.
Just a few example, but diverse.
This is going to be more interesting.
Consider continue reading [ [Bulma SASS - Custom][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2019/03/06/bulma-sass-custom/

[tutor-03-32-html]:     {{< tutor-html-bulma >}}/tutor-03/32-swatch.html
[tutor-03-34-html]:     {{< tutor-html-bulma >}}/tutor-03/34-awesome.html

[tutor-03-cerulean]:    {{< tutor-html-bulma >}}/tutor-03/sass/bulmaswatch/cerulean/bulmaswatch.scss

[tutor-03-css-33-main]:         {{< tutor-html-bulma >}}/tutor-03/sass/css-33/main.sass
[tutor-03-css-33-layout]:       {{< tutor-html-bulma >}}/tutor-03/sass/css-33/_layout.sass
[tutor-03-css-33-decoration]:   {{< tutor-html-bulma >}}/tutor-03/sass/css-33/_decoration.sass

[tutor-03-css-34-fontawesome]:  {{< tutor-html-bulma >}}/tutor-03/sass/css-34/fontawesome.scss

[image-ss-31-maxwidth]:         {{< assets-frontend >}}/2019/03/31-maxwidth.png
[image-ss-32-compile-swatch]:   {{< assets-frontend >}}/2019/03/32-compile-swatch.png
[image-ss-32-bulmaswatch]:      {{< assets-frontend >}}/2019/03/32-bulmaswatch.png
[image-ss-32-swatch-animation]: {{< assets-frontend >}}/2019/03/32-bulma-swatch-animation.gif
[image-ss-34-assets-sass]:      {{< assets-frontend >}}/2019/03/34-tree-assets-sass.png
[image-ss-34-primary]:          {{< assets-frontend >}}/2019/03/34-primary.png
[image-ss-34-light]:            {{< assets-frontend >}}/2019/03/34-light.png
