---
type   : post
title  : "Bulma SASS - Custom Class"
date   : 2019-03-06T09:17:35+07:00
slug   : bulma-sass-custom
categories: [frontend]
tags      : [bulma, sass]
keywords  : [plain html, custom theme, brand color, shadow, border, rounded, altering navigation breakpoint]
author : epsi
opengraph:
  image: assets/site/images/topics/bulma-sass.png

toc    : "toc-2019-03-html-bulma-step"

excerpt:
  More about custom theme, step by step.
  Make custom class based on Bulma class.

---

### Preface

> Goal: Make custom class based on Bulma class.

-- -- --

### 1: Prepare

#### The Sylesheet: Brand Color

As a summary, I change the color, and put brand color, for theming.
But we won't use brand color yet,
but we are going to use this brand color,
s preparation for the rest of the show case.

* <code>tutor-04/sass/css-41/_initial-variables.sass</code>.
  : [gitlab.com/.../sass/css-41/_initial-variables.sass][tutor-04-css-41-initial]

{{< highlight scss >}}
// Initial Variables

$brand-red:  #802020
$dark:   $brand-red

$navbar-breakpoint: 769px
{{< / highlight >}}

The primary colour has been restored.
Now we don't have pink as primary colour anymore.

-- -- --

### 2: Decoration

My first impression of Bulma is the lack of helper,
such as hover on shadow, border, rounded corner, and such stuff.
Or things are hardcoded in Bulma class,
and other stuff I cannot get rid of.

But this give me better idea, to use css class instead of,
styling on html using a lot of class.
Now I ended up with a more consistent web looks,
becuase all it takes is just one pack of additional class.

Looks like this easy to custom, is the power of Bulma.

#### Bulma Box Original Class

Consider have a look at this **original** Bulma box:

* <code>tutor-04/sass/vendors/bulma/elements/box.sass</code>
  : [gitlab.com/.../sass/vendors/bulma/elements/box.sass][tutor-04-v-box]

{{< highlight scss >}}
$box-color: $text !default
$box-background-color: $white !default
$box-radius: $radius-large !default
$box-shadow: 0 2px 3px rgba($black, 0.1), 0 0 0 1px rgba($black, 0.1) !default
$box-padding: 1.25rem !default

$box-link-hover-shadow: 0 2px 3px rgba($black, 0.1), 0 0 0 1px $link !default
$box-link-active-shadow: inset 0 1px 2px rgba($black, 0.2), 0 0 0 1px $link !default

.box
  @extend %block
  background-color: $box-background-color
  border-radius: $box-radius
  box-shadow: $box-shadow
  color: $box-color
  display: block
  padding: $box-padding

a.box
  &:hover,
  &:focus
    box-shadow: $box-link-hover-shadow
  &:active
    box-shadow: $box-link-active-shadow
{{< / highlight >}}

#### Custom Box Class

Consider have a look at this **altered** Bulma box:

* <code>tutor-04/sass/css-41/_box-decoration.sass</code>.
  : [gitlab.com/.../sass/css-41/_box-decoration.sass][tutor-04-css-41-decoration].
  
{{< highlight scss >}}
/* copy-paste from box.sass */

$box-color: $text !default

/* decoration : border, shadow, rounded */

$deco-radius: 10px

.box-deco
  border-radius: $deco-radius
  color: $box-color
  display: block

a.box-deco
  &:hover,
  &:focus,
  &:active
    box-shadow: 0 1rem 3rem rgba($black, .175)
    transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s

.box-deco
  border: 1px solid darken($light, 10%)

.box-deco
  box-shadow: 0 0.5rem 1rem rgba(0,0,0,0.15)
  &:hover,
  &:focus
    box-shadow: 0 1rem 3rem rgba($black, .175)
    border: 1px solid darken($light, 20%)
{{< / highlight >}}

And do not forget to add the sass artefact in `main`:

* <code>tutor-04/css/main.sass</code>

{{< highlight scss >}}
@import "initial-variables"
@import "vendors/bulma/utilities/initial-variables"
@import "vendors/bulma/utilities/functions"
@import "vendors/bulma/utilities/derived-variables"

@import "vendors/bulma/utilities/mixins"
@import "layout"
@import "decoration"
@import "box-decoration"
{{< / highlight >}}

We also need `functions.sass` in order this box decoration to works.

#### Header and Footer

Still in the same artefact we can class for header and footer.

* <code>tutor-04/css/_box_decoration.sass</code>

{{< highlight scss >}}
/* copy-paste from box.sass */

$box-color: $text !default

/* decoration : border, shadow, rounded */

$deco-radius: 10px

.header-deco,
.footer-deco
  border: 1px solid $dark

.header-deco,
.footer-deco
  box-shadow: 0 0.5rem 1rem rgba(0,0,0,0.15)
  &:hover,
  &:focus
    box-shadow: 0 1rem 3rem rgba($black, .175)
    transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s

.header-deco
  border-bottom-left-radius: $deco-radius
  border-bottom-right-radius: $deco-radius

.footer-deco
  border-top-left-radius: $deco-radius
  border-top-right-radius: $deco-radius

.blog-item
  border-bottom: 1px solid $dark
  box-shadow: 0 0.5rem 1rem rgba(0,0,0,0.15)
  padding: 10px
  &:hover,
  &:focus
    box-shadow: 0 1rem 3rem rgba($black, .175)
    transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s
{{< / highlight >}}

#### The Document: Example Page

Now add this box decoration to each element,
with the complete code as below:

* <code>tutor-04/41-decoration.html</code>
  : [gitlab.com/.../41-decoration.html][tutor-04-41-html].

{{< highlight html >}}
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Letters to my Bulma</title>

    <link rel="stylesheet" type="text/css" href="css/bulma.css">
    <link rel="stylesheet" type="text/css" href="css/fontawesome.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>

  <!-- header -->
  <nav role="navigation" aria-label="main navigation"
       class="navbar is-fixed-top is-white maxwidth header-deco"
       id="navbar-vue-app">
    <div class="navbar-brand">
      <a class="navbar-item" href="#">
        <img src="images/logo-gear.png" alt="Home" />
      </a>
      <a class="navbar-item">
        Blog
      </a>
    </div>

    <div id="navbarBulma" class="navbar-menu">
      <div class="navbar-start">
      </div>
      <div class="navbar-end">
      </div>
    </div>
  </nav>

  <!-- main -->
  <div class="layout-base maxwidth">
    <main role="main" 
          class="column is-full box-deco has-background-white">
        <p>There are so many things to say
        to Bulma, Chici, and that Android 18.
        I don't want to live in regrets.
        So I wrote this for my love.</p>
    </main>
  </div>

  <!-- footer -->
  <footer class="site-footer">
    <div class="navbar is-fixed-bottom maxwidth
          is-white has-text-centered is-vcentered footer-deco">
      <div class="column">
        &copy; 2019.
      </div>
    </div>
  </footer>
</body>
</html>
{{< / highlight >}}

We can expect the page in the browser to show as this figure below:

![Bulma SASS: Custom Box Decoration][image-ss-41-decoration]

Now we can have our custom class with behaviour that we need.

-- -- --

### 3: Navigation Bar

Sometimes, make custom class is not the best choice.
I once had an issue with navigation bar,
while putting dropdown on small screen.
After a while, I realize that this is a responsive issue.
Finally I ended up with altering the internal Bulma.

#### Bulma Navigation Bar Original Class

Consider have a look at this original Bulma navigation bar:

* <code>tutor-04/sass/vendors/bulma/components/navbar.sass</code>
  : [gitlab.com/.../sass/vendors/bulma/components/navbar.sass][tutor-04-v-navbar]

{{< highlight scss >}}
...

+until($navbar-breakpoint)
  .navbar > .container
    display: block
  .navbar-brand,
  ....

+from($navbar-breakpoint)
  .navbar,
  .navbar-menu,
  .navbar-start,
  .navbar-end
    align-items: stretch
    display: flex
  ...
  .navbar-item
    display: flex
    ...
  ...
  .navbar-dropdown
    ...
  ...
{{< / highlight >}}

#### The Document: Example Code

And if I put dropdown element navigation bar as below:

* <code>tutor-04/42-navbar.html</code>
  : [gitlab.com/.../42-navbar.html][tutor-04-42-html].

{{< highlight html >}}
  <!-- header -->
  <nav role="navigation" aria-label="main navigation"
       class="navbar is-fixed-top is-white maxwidth header-deco"
       id="navbar-vue-app">
    <div class="navbar-brand">
      <a class="navbar-item" href="#">
        <img src="images/logo-gear.png" alt="Home" />
      </a>
      <a class="navbar-item">
        Blog
      </a>

      <div class="navbar-item has-dropdown is-hoverable">
        <a class="navbar-link">
          Archives
        </a>

        <div class="navbar-dropdown">
          <a class="navbar-item">
            <span class="fa fa-tags"></span>&nbsp;By Tags
          </a>
          <a class="navbar-item">
            <span class="fa fa-folder"></span>&nbsp;By Category
          </a>
          <hr class="navbar-divider">
          <a class="navbar-item">
            <span class="fa fa-calendar"></span>&nbsp;By Date
          </a>
        </div>
      </div>

      <a role="button" class="navbar-burger burger" 
         aria-label="menu" aria-expanded="false" data-target="navbarBulma"
         @click="isOpen = !isOpen" v-bind:class="{'is-active': isOpen}">
        <span aria-hidden="true"></span>
        <span aria-hidden="true"></span>
        <span aria-hidden="true"></span>
      </a>
    </div>

    <div id="navbarBulma" class="navbar-menu"
         v-bind:class="{'is-active': isOpen}">
      <div class="navbar-start">
        <a class="navbar-item">
          About
        </a>
      </div>
      <div class="navbar-end">
        <form class="is-marginless" action="/pages/search/" method="get">
        <div class="navbar-item">
          <input class="" type="text" name="q"
            placeholder="Search..." aria-label="Search">
          &nbsp;
          <button class="button is-light" 
            type="submit">Search</button>
        </div>
        </form>
      </div>
    </div>
  </nav>
{{< / highlight >}}

This will show undesired result in `480px;` screen.

![Bulma SASS: Navbar Broken][image-ss-42-navbar-broken]

#### Altered Navigation Bar Class: Bulma 7.x

The point of that `sass` below is,
changing the responsive by using `+from(0)` for only the needed behaviour.

Consider have a look at this altered Bulma navigation bar:

* <code>tutor-04/sass/vendors/bulma/components/navbar-altered.sass</code>
  : [gitlab.com/.../sass/vendors/bulma/components/navbar-altered.sass][tutor-04-v-navbar-a]

{{< highlight scss >}}
+until($navbar-breakpoint)
  .navbar > .container
    display: block
  .navbar-brand,
  ....

+from($navbar-breakpoint)
  .navbar,
  .navbar-menu,
  .navbar-start,
  .navbar-end
    align-items: stretch
    display: flex
  ....

+from($navbar-breakpoint)
  .navbar-item
    &.has-dropdown
      align-items: stretch

+from(0)
  .navbar-item
    display: flex
    ...
  .navbar-dropdown
    ...
{{< / highlight >}}

Do not forget to change the source `import` of the altered `sass`.

* <code>tutor-04/sass/vendors/bulma/components/_all.sass</code>

{{< highlight scss >}}
@charset "utf-8"

...
@import "navbar-altered.sass"
@import "pagination.sass"
@import "panel.sass"
@import "tabs.sass"
{{< / highlight >}}

This will show desired result in `480px;` screen.

![Bulma SASS: Navbar Fix][image-ss-42-navbar-fixed]

Now it fixed. Done!

#### Altered Navigation Bar Class: Bulma 8.x

Bulma 8.0 has been released.
And I have to update this article.

{{< highlight scss >}}
+from($navbar-breakpoint)
  .navbar,
  .navbar-menu,
  .navbar-start,
  .navbar-end
    align-items: stretch
    display: flex
  .navbar
    min-height: $navbar-height
    ...
  .navbar-link
    ...
  // Move this: .navbar-item
  .navbar-menu
    ...
  .navbar-end
    ...
  // Move this: .navbar-dropdown
  .navbar-divider
    ...


+from($navbar-breakpoint)
  .navbar-item
    &.has-dropdown
      align-items: stretch

+from(0)
  .navbar-item
    // &.has-dropdown
    //  align-items: stretch
    &.has-dropdown-up
      ...
    ...
  .navbar-dropdown
    ...
{{< / highlight >}}

The changes in 8.x is more complex than in 7.x.

-- -- --

### What is Next ?

More SASS customization is our next topic.
Mostly about layout, a little spacing trick to enhance responsive experience.
Consider continue reading [ [Bulma SASS - Layout][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2019/03/07/bulma-sass-layout/

[tutor-04-41-html]:     {{< tutor-html-bulma >}}/tutor-04/41-decoration.html
[tutor-04-42-html]:     {{< tutor-html-bulma >}}/tutor-04/42-navbar.html

[tutor-04-css-41-decoration]:   {{< tutor-html-bulma >}}/tutor-04/sass/css-41/_box-decoration.sass
[tutor-04-css-41-initial]:      {{< tutor-html-bulma >}}/tutor-05/sass/css-41/_initial-variables.sass
[tutor-04-v-box]:       {{< tutor-html-bulma >}}/tutor-04/sass/vendors/bulma/elements/box.sass
[tutor-04-v-navbar]:    {{< tutor-html-bulma >}}/tutor-04/sass/vendors/bulma/components/navbar.sass
[tutor-04-v-navbar-a]:  {{< tutor-html-bulma >}}/tutor-04/sass/vendors/bulma/components/navbar-altered.sass

[image-ss-41-decoration]:   {{< assets-frontend >}}/2019/03/41-decoration.png
[image-ss-42-navbar-broken]:    {{< assets-frontend >}}/2019/03/42-navbar-broken.png
[image-ss-42-navbar-fixed]:     {{< assets-frontend >}}/2019/03/42-navbar-fixed.png
[image-ss-32-bulmaswatch]:      {{< assets-frontend >}}/2019/03/32-bulmaswatch.png
