---
type   : post
title  : "Bulma SASS - Responsive Layout"
date   : 2019-03-07T09:17:35+07:00
slug   : bulma-sass-layout
categories: [frontend]
tags      : [bulma, sass]
keywords  : [plain html, custom theme, page skeleton, responsive spacing]
author : epsi
opengraph:
  image: assets/site/images/topics/bulma-sass.png

toc    : "toc-2019-03-html-bulma-step"

excerpt:
  More about custom theme, step by step.
  Separate Page Layout using Bulma.

---

### Preface

> Goal: Separate Page Layout using Bulma.

-- -- --

### 1: General Preview

This below is general preview of,
what responsive page that we want to achieve.
Just keep in mind that,
responsive design is based on consideration.
So different website might have different preview.

![Bulma SASS: General Preview of Responsive Page Design][image-png-layout-page]

Source image is available in inkscape SVG format,
so you can modify, and make your own preview quickly.

* [Responsive Page: Image Source][image-svg-layout-page]

-- -- --

### 2: Page Skeleton

So far we have these three elements:

* Header part: Navigation Bar.

* Main part: Content

* Footer part.

#### Layout

The internal layout for this show case can be summarized as below:

{{< highlight html >}}
<html>
<head>
  <!--head section -->
</head>
<body>

  <!-- header -->
  <nav role="navigation">
       ...
  </nav>

  <!-- main -->
  <div class="columns">

    <main role="main">
      <!-- content -->
    </main>

    <aside>
      <!-- aside -->
    </aside>
  </div>

  <!-- footer -->
  <footer>
    ...
  </footer>

  <!-- JavaScript -->
  <!-- Placed at the end of the document -->
  ...
</body>
</html>
{{< / highlight >}}

Of course you can make your own skeleton to suit your needs.

#### The Columns

Now we can have this two elements inside `main` parts.

* `main` element: page content or blog post content

* `aside` element: side panel

We would like to apply a simple responsive design for this two elements.

{{< highlight html >}}
  <!-- main -->
  <div class="columns is-8 ">

    <main role="main" class="column is-two-thirds">
      <!-- page content -->
    </main>

    <aside class="column is-one-thirds">
      <!-- side panel -->
    </aside>
  </div>
{{< / highlight >}}

What is this `is-8` class?
With Bulma, you can have eight column or any other number,
instead hardcoded twelve columns.

Bulma has handle this good with this two class

* `column is-two-thirds`

* `column is-one-thirds`

#### Example Layout Page

Now we require real example page, as usual.

* <code>tutor-05/51-responsive.html</code>
  : [gitlab.com/.../51-responsive.html][tutor-05-51-html].

{{< highlight bash >}}
  <!-- main -->
  <div class="columns is-8 layout-base maxwidth">

    <main role="main" 
          class="column is-two-thirds box-deco has-background-white">
      Page Content
    </main>

    <aside class="column is-one-thirds box-deco has-background-white">
      <div class="blog-sidebar">
        Side Menu
      </div>
    </aside>
  </div>
{{< / highlight >}}

#### Issue

The issue is spacing between this two `column` in responsive manners,
as shown in this example below:

![Bulma SASS: No Spacing][image-ss-51-r-nospacing]

-- -- --

### 3: Responsive Spacing

#### The Sylesheet: Additional Class

What is this `blog-column` class anyway?
Well, this is actually just a custom class.

* <code>tutor-05/sass/css-51/_blog.sass</code>
  : [gitlab.com/.../sass/css-51/_blog.sass][tutor-05-css-51-blog].

{{< highlight scss >}}
+mobile
  .blog-column
    margin-bottom: 20px

+tablet
  .blog-column
    margin-right: 20px
{{< / highlight >}}

If you are curious about what this `+mobile` and `+tablet` is all about,
they are all defined ib Bulma's `mixins.sass`.

{{< highlight scss >}}
=mobile
  @media screen and (max-width: $tablet - 1px)
    @content

=tablet
  @media screen and (min-width: $tablet), print
    @content
{{< / highlight >}}

More on official documentation:

* [bulma.io/documentation/overview/responsiveness/](https://bulma.io/documentation/overview/responsiveness/)

#### The Sylesheet: Packed All

Do not forget to add this `_blog.sass` artefact ito `main.sass`,
to update the `main.css` assets. I know it is getting longer each step.

* <code>tutor-05/sass/css-51/main.sass</code>
  : [gitlab.com/.../sass/css-51/main.sass][tutor-05-css-51-main].

{{< highlight scss >}}
// dart-sass --watch -I sass sass/css-51:css/ --style=compressed --source-map

@import "initial-variables"
@import "vendors/bulma/utilities/initial-variables"
@import "vendors/bulma/utilities/functions"
@import "vendors/bulma/utilities/derived-variables"

@import "vendors/bulma/utilities/mixins"
@import "layout"
@import "decoration"
@import "box-decoration"
@import "blog"
{{< / highlight >}}

And compile

{{< highlight bash >}}
$ dart-sass --watch -I sass sass/css-51:css/
Compiled sass/css-51/main.sass to css/main.css.
Sass is watching for changes. Press Ctrl-C to stop.
{{< / highlight >}}

#### Example Layout Page

Put this `blog-column` class in `main` element.

{{< highlight bash >}}
    <main role="main" 
          class="column is-two-thirds blog-column box-deco has-background-white">
      Page Content
    </main>
{{< / highlight >}}

Consider test this simple trick spacing in different screen,
Tablet and Mobile.

#### Mobile Screenshot Preview

The result of

{{< highlight scss >}}
+mobile
  .blog-column
    margin-bottom: 20px
{{< / highlight >}}

![Bulma SASS: Layout Mobile][image-ss-51-r-mobile]

#### Tablet Screenshot Preview

The result of

{{< highlight scss >}}
+mobile
  .blog-column
    margin-bottom: 20px

+tablet
  .blog-column
    margin-right: 20px
{{< / highlight >}}

![Bulma SASS: Layout Tablet][image-ss-51-r-tablet]

-- -- --

### What is Next ?

While we design the layout skeleton in this article,
now we need to fill of each layout such as blog post and side panel.
Consider continue reading [ [Bulma SASS - Panels][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2019/03/08/bulma-sass-panels/

[tutor-05-51-html]:     {{< tutor-html-bulma >}}/tutor-05/51-responsive.html

[tutor-05-css-51-main]:     {{< tutor-html-bulma >}}/tutor-05/sass/css-51/main.sass
[tutor-05-css-51-blog]:     {{< tutor-html-bulma >}}/tutor-05/sass/css-51/_blog.sass

[image-ss-51-r-nospacing]:  {{< assets-frontend >}}/2019/03/51-nospacing-480px.png
[image-ss-51-r-mobile]: {{< assets-frontend >}}/2019/03/51-responsive-480px.png
[image-ss-51-r-tablet]: {{< assets-frontend >}}/2019/03/51-responsive-800px.png

[image-png-layout-page]:    {{< assets-frontend >}}/2019/12/layout-page.png
[image-svg-layout-page]:    {{< assets-frontend >}}/2019/12/layout-page.svg
