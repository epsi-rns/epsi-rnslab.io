---
type   : post
title  : "Bulma SASS - Widget"
date   : 2019-03-08T09:17:35+07:00
slug   : bulma-sass-panels
categories: [frontend]
tags      : [bulma, sass]
keywords  : [plain html, custom theme, widget, custom stylesheet]
author : epsi
opengraph:
  image: assets/site/images/topics/bulma-sass.png

toc    : "toc-2019-03-html-bulma-step"

excerpt:
  More about custom theme, step by step.
  Custom sass for each part of the layout.

---

### Preface

> Goal: Custom sass for each part of the layout.

Especially the panel.

-- -- --

### 1: Main Page

Sure, we have to start from content page container.
Decorate the blog post first, before junmp into the panel.
This is not a Bulma part, but more like a custom SASS.
We need to make it a little bit pretty before continue.

#### The Document: Example Code

* <code>tutor-05/52-responsive.html</code>
  : [gitlab.com/.../52-responsive.html][tutor-05-52-html].

{{< highlight html >}}
  <!-- main -->
  <div class="columns is-8 layout-base maxwidth">

    <main role="main" 
          class="column is-two-thirds blog-column box-deco has-background-white">
      <article class="blog-post">
        <h4 class="title is-4">Letters to my Bulma</h4>
        <p>There are so many things to say
        to Bulma, Chici, and that Android 18.
        I don't want to live in regrets.
        So I wrote this for my love.</p>
      </article>
    </main>

    <aside class="column is-one-thirds box-deco has-background-white">
      <div class="blog-sidebar">
        Side menu
      </div>
    </aside>
  </div>
{{< / highlight >}}

#### The Sylesheet: Example Class

Most of this `style` I grab from Bootstrap example.

* <code>tutor-05/sass/css-52/_blog.sass</code>
  : [gitlab.com/.../sass/css-52/_blog.sass][tutor-05-css-52-blog].

{{< highlight scss >}}
/* stylelint-disable selector-list-comma-newline-after */

h1, h2, h3, h4, h5, h6
  font-family: "Playfair Display", Georgia, "Times New Roman", serif

/*
 * Blog posts
 */

.blog-post
  margin-bottom: 4rem

.blog-post-title
  margin-bottom: .25rem
  font-size: 2.5rem

.blog-post-meta
  margin-bottom: 1.25rem
  color: #999

.blog-sidebar, .blog-post
  margin-left: 20px
  margin-right: 20px

.blog-post-meta
  margin-bottom: 5px

+mobile
  .blog-column
    margin-bottom: 20px

+tablet
  .blog-column
    margin-right: 20px

{{< / highlight >}}

#### The Preview

![Bulma SASS: Blog Element][image-ss-52-blog]

If you want you can add this font that I took from Bootstrap example.

{{< highlight html >}}
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Playfair+Display:700,900">
{{< / highlight >}}

-- -- --

### 2: Side Panel

I also use a modified version of,
Bulma's `message.sass` class to enhance my panel.

#### Bulma Original Message Class

Consider have a look at this **original** Bulma box:

* <code>tutor-05/sass/vendors/bulma/components/message.sass</code>
  : [gitlab.com/.../sass/vendors/bulma/components/message.sass][tutor-05-v-message]

{{< highlight scss >}}
.message
  @extend %block
  background-color: $message-background-color
  ...

.message-header
  align-items: center
  background-color: $message-header-background-color
  ...

.message-body
  border-color: $message-body-border-color
  border-radius: $message-body-radius
  ...
{{< / highlight >}}

#### Custom Box Class

Consider have a look at this **altered** Bulma message,
that I turned into panel class:

* <code>tutor-05/sass/css-53/_panel.sass</code>.
  : [gitlab.com/.../sass/css-53/_panel.sass][tutor-05-css-53-panel].

{{< highlight scss >}}
.panel
  //@extend %block
  background-color: $panel-background-color
  font-size: $size-normal
  ...

.panel-header
  align-items: center
  background-color: $panel-header-background-color
  ...

.panel-body
  color: $panel-body-color
  padding: $panel-body-padding
 ...
{{< / highlight >}}

#### The Main Stylesheet

And consider make the `main` sass a line longer.

* <code>tutor-05/sass/css-53/main.sass</code>.
  : [gitlab.com/.../sass/css-53/main.sass][tutor-05-css-53-main].

{{< highlight scss >}}
...

@import "blog"
@import "panel"
{{< / highlight >}}

Always recompile the **CSS**.

{{< highlight bash >}}
$ dart-sass --watch -I sass sass/css-53:css/
Compiled sass/css-53/main.sass to css/main.css.
Sass is watching for changes. Press Ctrl-C to stop.
{{< / highlight >}}

#### The Document: Example Code

Now execute the panel stylesheet in respective hypertext markup language.

* <code>tutor-05/53-panel.html</code>
  : [gitlab.com/.../53-panel.html][tutor-05-53-html].

{{< highlight html >}}
    <aside class="sidebar column is-one-thirds is-paddingless">
      <section class="panel is-light">
        <div class="panel-header">
          <p>Isle of Friends</p>
          <span class="fa fa-child"></span>
        </div>
        <div class="panel-body has-background-white">
          Side Menu
        </div>
      </section>
    </aside>
{{< / highlight >}}

#### The Preview

And voila!. The white magic happened.

![Bulma SASS: Blog Panel][image-ss-53-panel]

-- -- --

### 3: List

With the same method, we can make a lot of panel in right side.
But we still need to adjust to make it prettier.

#### The Stylesheet: Altered Class

* <code>tutor-05/sass/css-54/_list.sass</code>.
  : [gitlab.com/.../sass/css-54/_list.sass][tutor-05-css-54-list].

{{< highlight scss >}}
// -- -- -- -- --
// _list.scss

ul.panel-list li:hover
    background: lighten($yellow, 15%)

ul.panel-list
  padding: 0px

ul.panel-list
  padding-left: 0
  list-style: none
  font-size: 13px
  margin-left: 20px

ul.panel-list li:before
  font-family: "Font Awesome 5 Free"
  //font-weight: 900
  content: '\f054'
  color: $gray
  margin: 0 5px 0 -12px

ul.panel-list li:hover:before
  color: $yellow
{{< / highlight >}}

What is this `$gray` and `$yellow` anyway?
It is time to add `_initial-variables`.

* <code>tutor-05/sass/css-54/_initial-variables.sass</code>.
  : [gitlab.com/.../sass/css-54/_initial-variables.sass][tutor-05-css-54-initial]

{{< highlight scss >}}
// Initial Variables

$brand-red:  #802020
$brand-gray: #404040

$gray:   #7b8a8b
$yellow: #f7d316
$blue:   #2980b9

$dark:   $brand-red

$navbar-breakpoint: 769px
{{< / highlight >}}

#### The Main Stylesheet

And consider make the `main` sass a line longe, again.

{{< highlight scss >}}
...

@import "blog"
@import "panel"
@import "list"
{{< / highlight >}}

#### The Document: Example Code

* <code>tutor-05/54-list.html</code>
  : [gitlab.com/.../54-list.html][tutor-05-54-html].

{{< highlight html >}}
    <aside class="sidebar column is-one-thirds is-paddingless">
      <section class="panel is-light">
        <div class="panel-header">
          <p>Isle of Friends</p>
          <span class="fa fa-child"></span>
        </div>
        <div class="panel-body has-background-white">
          <ul class="panel-list">
            <li><a href="http://epsi-rns.github.io/">Linux/BSD Desktop Customization</a></li>
            <li><a href="http://epsi-rns.gitlab.io/">Web Development Blog</a></li>
            <li><a href="http://oto-spies.info/">Car Painting and Body Repair.</a></li>
          </ul>
        </div>
      </section>
    </aside>
{{< / highlight >}}

#### The Preview

This is not magic. Just an ordinary regular styleheet.

![Bulma SASS: Panel: List][image-ss-54-list]

-- -- --

### 4: Tags

> Multi Panel Sidebar Example

This one example is easier,
instead of making new class, we can utilize Bulma built in class.
Just go straight to HTML without any custom stylesheet.

#### The Document: Example Code

* <code>tutor-05/55-tags.html</code>
  : [gitlab.com/.../55-tags.html][tutor-05-55-html].

{{< highlight html >}}
    <aside class="sidebar column is-one-thirds is-paddingless">
      <section class="panel is-light">
        <div class="panel-header">
          <p>Categories</p>
         <span class="fa fa-folder"></span>
        </div>
        <div class="panel-body has-background-white">    
          <a class="tag is-small is-info" href="#">
            love&nbsp;<span class="fa fa-folder"></span>
          </a>&nbsp;
    
          <a class="tag is-small is-info" href="#">
            lyric&nbsp;<span class="fa fa-folder"></span>
          </a>&nbsp;
        </div>
      </section>

      <section class="panel is-light">
        <div class="panel-header">
          <p>Tags</p>
         <span class="fa fa-tag"></span>
        </div>
        <div class="panel-body has-background-white">
          <a class="tag is-small is-light has-background-white"
             href="#">
            <span class="fa fa-folder"></span>&nbsp;indie
          </a>&nbsp;

          <a class="tag is-small is-light has-background-white"
             href="#">
            <span class="fa fa-folder"></span>&nbsp;pop
          </a>&nbsp;

          <a class="tag is-small is-light has-background-white"
             href="#">
            <span class="fa fa-folder"></span>&nbsp;rock
          </a>&nbsp;
        </div>
      </section>
    </aside>
{{< / highlight >}}

Well, a lot of font awesome involved.

#### The Preview

It is just pretty code. No Magic.

![Bulma SASS: Panel: Tags and Categories][image-ss-55-tags]

-- -- --

### What is Next ?

More SASS customization is our next topic.
This is going to be more interesting.
Consider continue reading [ [Bulma SASS - Post][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2019/03/09/bulma-sass-post/

[tutor-05-v-message]:   {{< tutor-html-bulma >}}/tutor-05/sass/vendors/bulma/components/message.sass

[tutor-05-52-html]:     {{< tutor-html-bulma >}}/tutor-05/52-blog.html
[tutor-05-53-html]:     {{< tutor-html-bulma >}}/tutor-05/53-panel.html
[tutor-05-54-html]:     {{< tutor-html-bulma >}}/tutor-05/54-list.html
[tutor-05-55-html]:     {{< tutor-html-bulma >}}/tutor-05/55-tags.html

[tutor-05-css-52-blog]:     {{< tutor-html-bulma >}}/tutor-05/sass/css-52/_blog.sass
[tutor-05-css-53-main]:     {{< tutor-html-bulma >}}/tutor-05/sass/css-53/main.sass
[tutor-05-css-53-panel]:    {{< tutor-html-bulma >}}/tutor-05/sass/css-53/_panel.sass
[tutor-05-css-54-list]:     {{< tutor-html-bulma >}}/tutor-05/sass/css-54/_list.sass
[tutor-05-css-54-initial]:  {{< tutor-html-bulma >}}/tutor-05/sass/css-54/_initial-variables.sass

[image-ss-52-blog]:     {{< assets-frontend >}}/2019/03/52-blog.png
[image-ss-53-panel]:    {{< assets-frontend >}}/2019/03/53-panel.png
[image-ss-54-list]:     {{< assets-frontend >}}/2019/03/54-list.png
[image-ss-55-tags]:     {{< assets-frontend >}}/2019/03/55-tags.png
