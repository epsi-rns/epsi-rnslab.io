---
type   : post
title  : "Bulma SASS - Post"
date   : 2019-03-09T09:17:35+07:00
slug   : bulma-sass-post
categories: [frontend]
tags      : [bulma, sass]
keywords  : [plain html, custom theme, content, header, navigation, custom sass]
author : epsi
opengraph:
  image: assets/site/images/topics/bulma-sass.png

toc    : "toc-2019-03-html-bulma-step"

excerpt:
  More about custom theme, step by step.
  Focusing on content page.

---

### Preface

> Goal: Focusing on content page.

Especially the panel.

-- -- --

### 1: Content

It is very similar as the previous one,
with just a few CSS adjustment.

#### The Sylesheet: Content

* <code>tutor-06/sass/css-61/_post.sass</code>
  : [gitlab.com/.../sass/css-61/_post.sass][tutor-06-css-61-post].

{{< highlight scss >}}
.blog-post
  margin-bottom: 1rem

.blog-post-title
  margin-bottom: .25rem
  font-size: 2.5rem

.blog-post-meta
  margin-bottom: 1.25rem

.blog-post p
  margin-top: 0.5rem
  margin-bottom: 0.5rem
{{< / highlight >}}

#### The Main Stylesheet

Again, consider make the `main` sass a line longer.

* <code>tutor-06/sass/css-61/main.sass</code>.
  : [gitlab.com/.../sass/css-61/main.sass][tutor-06-css-61-main].

{{< highlight scss >}}
...

@import "blog"
@import "panel"
@import "list"

@import "post"
{{< / highlight >}}

Always recompile the **CSS**.

{{< highlight bash >}}
$ dart-sass --watch -I sass sass/css-61:css/
Compiled sass/css-61/main.sass to css/main.css.
Sass is watching for changes. Press Ctrl-C to stop.
{{< / highlight >}}

#### The Document: Example Code

And also very few adjustment, by adding the `blog-post` class.

* <code>tutor-06/61-post-content.html</code>
  : [gitlab.com/.../61-post-content.html][tutor-06-61-html].

{{< highlight html >}}
    <main role="main" 
          class="column is-two-thirds blog-column box-deco has-background-white">
      <div class="blog-post" itemscope itemtype="http://schema.org/BlogPosting">
        <article class="main-content" itemprop="articleBody">
          <h4 class="title is-4">Letters to my Bulma</h4>
          <p>There are so many things to say
          to Bulma, Chici, and that Android 18.
          I don't want to live in regrets.
          So I wrote this for my love.</p>
        </article>
      </div>
    </main>
{{< / highlight >}}

#### The Preview

You can see, the not so different look as below:

![Bulma SASS: Post: Content][image-ss-61-content]

-- -- --

### 2: Header

This takes a very few SASS line, but a long hypertext document.

#### The Sylesheet: Content

* <code>tutor-06/sass/css-62/_post-header.sass</code>
  : [gitlab.com/.../sass/css-62/_post-header.sass][tutor-06-css-62-header].

{{< highlight scss >}}

.main_title
  background: rgba(256, 256, 256, 0.5)
  a
    color: $primary
  a:hover
    color: $gray
{{< / highlight >}}

We also need to adjust this `$primary` color to match the brand logo:
It is time to add `_initial-variables`.

* <code>tutor-06/sass/css-62/_initial-variables.sass</code>.
  : [gitlab.com/.../sass/css-62/_initial-variables.sass][tutor-06-css-62-initial]

{{< highlight scss >}}
// Initial Variables

$brand-red:  #802020
$brand-gray: #404040

$gray:   #7b8a8b
$yellow: #f7d316
$blue:   #2980b9

$dark:    $brand-red
$primary: $brand-gray


$navbar-breakpoint: 769px
{{< / highlight >}}

#### The Main Stylesheet

Again, consider make the `main` sass a line longer.

* <code>tutor-06/sass/css-62/main.sass</code>.
  : [gitlab.com/.../sass/css-62/main.sass][tutor-06-css-62-main].

{{< highlight scss >}}
...

@import "post"
@import "post-header"
{{< / highlight >}}

Always recompile the **CSS**.

{{< highlight bash >}}
$ dart-sass --watch -I sass sass/css-62:css/
Compiled sass/css-62/main.sass to css/main.css.
Sass is watching for changes. Press Ctrl-C to stop.
{{< / highlight >}}

#### The Document: Example Code

* <code>tutor-06/62-post-header.html</code>
  : [gitlab.com/.../62-post-header.html][tutor-06-62-html].

As I promise, a longer document line.
It is basically just tags.

{{< highlight html >}}
    <main role="main" 
          class="column is-two-thirds blog-column box-deco has-background-white">
      <div class="blog-post" itemscope itemtype="http://schema.org/BlogPosting">
        <section class="box">
          <div class="main_title"> 
            <h4 class="title is-4 blog-post-title" itemprop="name headline">
              <a href="#">Letters to my Bulma</a></h4>
          </div>

          <div class="is-clearfix"></div>

          <div class="field">
            <div class="is-pulled-left">
            <span class="meta_author tag is-primary is-small">
                <span class="fa fa-user"></span>
                &nbsp;
                <span itemprop="author" itemscope itemtype="http://schema.org/Person">
                <span itemprop="name">epsi</span></span>
            </span>
            &nbsp;
            </div>

            <div class="is-pulled-left">
              <span class="meta-time tag is-light is-small">
                A few days ago.
              </span>
              &nbsp;
            </div>

            <div class="is-pulled-right">
                <a href="#">
                  <span class="tag is-light is-small"><span class="fa fa-tag"></span>&nbsp;love</span></a>
              &nbsp;
            </div>

            <div class="is-pulled-right">
                <a href="#">
                  <span class="tag is-dark is-small"><span class="fa fa-folder"></span>&nbsp;rock</span></a>
              &nbsp;
            </div>
          </div>

          <div class="is-clearfix"></div>
        </section>

        <article class="main-content" itemprop="articleBody">
          <p>There are so many things to say
          to Bulma, Chici, and that Android 18.
          I don't want to live in regrets.
          So I wrote this for my love.</p>
        </article>
      </div>
    </main>
{{< / highlight >}}

#### The Preview

Consider have a look at the preview from your favorite web browser.

![Bulma SASS: Post: Header][image-ss-62-header]

-- -- --

### 3: Navigation

This content page will be better if we can give navigation.
We're almost done with this content.

#### The Sylesheet: Content

* <code>tutor-06/sass/css-63/_post-navigation.sass</code>
  : [gitlab.com/.../sass/css-63/_post-navigation.sass][tutor-06-css-63-navi].

{{< highlight scss >}}
a.post-previous:after
    content: " previous"

a.post-next:before
    content: "next "
{{< / highlight >}}

#### The Main Stylesheet

Again, consider make the `main` sass a line longer.

* <code>tutor-06/sass/css-63/main.sass</code>.
  : [gitlab.com/.../sass/css-63/main.sass][tutor-06-css-63-main].

{{< highlight scss >}}
...

@import "post"
@import "post-header"
@import "post-navigation"
{{< / highlight >}}

Always recompile the **CSS**.

{{< highlight bash >}}
$ dart-sass --watch -I sass sass/css-63:css/
Compiled sass/css-63/main.sass to css/main.css.
Sass is watching for changes. Press Ctrl-C to stop.
{{< / highlight >}}

#### The Document: Example Code

* <code>tutor-06/63-post-navigation.html</code>
  : [gitlab.com/.../63-post-navigation.html][tutor-06-63-html].

{{< highlight html >}}
    <main role="main" 
          class="column is-two-thirds blog-column box-deco has-background-white">
      <div class="blog-post" itemscope itemtype="http://schema.org/BlogPosting">
        <section class="box">
          ...
        </section>

        <article class="main-content" itemprop="articleBody">
          <p>There are so many things to say
          to Bulma, Chici, and that Android 18.
          I don't want to live in regrets.
          So I wrote this for my love.</p>
        </article>

        <br/>
        <nav class="pagination is-centered" 
             role="navigation" aria-label="pagination">

            <a href="#" class="pagination-previous post-previous"
               title="next article name">&laquo;&nbsp;</a>

            <a href="#" class="pagination-next post-next"
               title="previous article name">&nbsp;&raquo;</a>
        </nav>
      </div>
    </main>
{{< / highlight >}}

#### The Preview

Consider open from file manager,
so you can examine in your favorite web browser.

![Bulma SASS: Post: Navigation][image-ss-63-navi]

This actually in 480px, or you can say under `mobile` breakpoint.

-- -- --

### 4: Summary

As a summary, consider to see the page again in 800px,
or you can say under `desktop` breakpoint.

![Bulma SASS: Post: Desktop][image-ss-64-desktop]

-- -- --

### What is Next ?

We are not finished yet with SASS in Bulma.
There are still issues we have to face in real world website stylesheet.
Consider continue reading [ [Bulma SASS - Helpers][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2019/03/10/bulma-sass-helpers/

[tutor-06-61-html]:     {{< tutor-html-bulma >}}/tutor-06/61-post-content.html
[tutor-06-62-html]:     {{< tutor-html-bulma >}}/tutor-06/62-post-header.html
[tutor-06-63-html]:     {{< tutor-html-bulma >}}/tutor-06/63-post-navigation.html

[tutor-06-css-61-post]:     {{< tutor-html-bulma >}}/tutor-06/sass/css-61/_post.sass
[tutor-06-css-61-main]:     {{< tutor-html-bulma >}}/tutor-06/sass/css-61/main.sass

[tutor-06-css-62-header]:   {{< tutor-html-bulma >}}/tutor-06/sass/css-62/_post-header.sass
[tutor-06-css-62-main]:     {{< tutor-html-bulma >}}/tutor-06/sass/css-62/main.sass
[tutor-06-css-62-initial]:  {{< tutor-html-bulma >}}/tutor-06/sass/css-62/_initial-variables.sass

[tutor-06-css-63-navi]:     {{< tutor-html-bulma >}}/tutor-06/sass/css-63/_post-navigation.sass
[tutor-06-css-63-main]:     {{< tutor-html-bulma >}}/tutor-06/sass/css-63/main.sass

[image-ss-61-content]:  {{< assets-frontend >}}/2019/03/61-post-content.png
[image-ss-62-header]:   {{< assets-frontend >}}/2019/03/62-post-header.png
[image-ss-63-navi]:     {{< assets-frontend >}}/2019/03/63-post-navigation.png
[image-ss-64-desktop]:  {{< assets-frontend >}}/2019/03/64-post-desktop.png

