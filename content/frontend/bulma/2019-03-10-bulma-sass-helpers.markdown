---
type   : post
title  : "Bulma SASS - Helpers"
date   : 2019-03-10T09:17:35+07:00
slug   : bulma-sass-helpers
categories: [frontend]
tags      : [bulma, sass]
keywords  : [plain html, custom theme, bulma helpers]
author : epsi
opengraph:
  image: assets/site/images/topics/bulma-sass.png

toc    : "toc-2019-03-html-bulma-step"

excerpt:
  More about custom theme, step by step.
  Getting tools not available in Bulma.

---

### Preface

> Goal: Getting tools not available in Bulma.

Unlike Bootstrap, Bulma does not come with complete utilities.
This can be an issue while migrating from,
any theme with a lot of padding and margin classes.

-- -- --

### 1: Using Bulma Helpers

#### Why Another Repository?

Compared with Bootstrap CSS framework,
Bulma developer does not want the framework to be bloated.
Both approach has pros and cons.

One of my favorites bootstrap utility is padding and margin.
This can be done in Bulma by altering each SASS of elements or components.
I can undertand that rookie developer need a quick fix,
by designing in HTML document just like what we have in bootstrap.

So here it is, I represent you the Bulma Helpers.

* [github.com/jmaczan/bulma-helpers](https://github.com/jmaczan/bulma-helpers)

#### The Issue

Bulma Helpers is a big toolkit.
You will end up with CSS, even bigger than Bulma itself.
Luckily Bulma Helper is modular, so we can have only what we need.

> The power of SASS

#### Prepare

Assume that what you need is only padding class and margin class.
Consider put, the cloned `bulma-helpers` in to `vendors` directory.

![Bulma SASS: Tree Vendors][image-ss-71-tree]

And change the `bulma-helpers.sass` from:

{{< highlight scss >}}
@charset "utf-8"

@import "bulma-helpers/mixins/_all"
@import "bulma-helpers/helpers/_all"
{{< / highlight >}}

To this one:

* <code>tutor-04/sass/vendors/bulma-helpers.sass</code>
  : [gitlab.com/.../sass/vendors/bulma-helpers.sass][tutor-07-v-helpers]

{{< highlight scss >}}
@charset "utf-8"

@import "bulma-helpers/variables/_all"
@import "bulma-helpers/mixins/_all"
@import "bulma-helpers/helpers/spacing/_all"
{{< / highlight >}}

#### Where to Put the Stylesheet?

You can either make a different css such as `bulma-helpers.css`.

* <code>tutor-07/sass/css-71/bulma-helpers.sass</code>
  : [gitlab.com/.../sass/css-71/bulma-helpers.sass][tutor-07-css-71-helpers].

{{< highlight scss >}}
@import "vendors/bulma-helpers"
{{< / highlight >}}

Or packed in `bulma.css`.

* <code>tutor-07/sass/css-71/bulma.sass</code>
  : [gitlab.com/.../sass/css-71/bulma.sass][tutor-07-css-71-bulma].

{{< highlight scss >}}
@import "initial-variables"
@import "vendors/bulma"
@import "vendors/bulma-helpers"
@import "derived-variables"
{{< / highlight >}}

It is all up to you, actually.
Not mine to decide.

#### The Documents: Example Code

As usual, use the class in the document, and see if this works.

* <code>tutor-07/71-bulma-helpers.html</code>
  : [gitlab.com/.../71-bulma-helpers.html][tutor-07-71-html].

{{< highlight html >}}
    <main role="main" 
          class="column is-full blog-column
                 box-deco has-background-white has-text-centered">
      <article class="blog-post">
        <h2 class="title is-5">Welcome to My Bulma Portfolio</h2>

        <br/>

        <p>
          <a class="button is-dark has-margin-bottom-5"
             href="#">Articles Sorted by Month</a>
          <a class="button is-primary has-margin-bottom-5"
             href="#">Articles Sorted by Tag</a>
        </p>

        <br/>

        <p>There are so many things to say
          to Bulma, Chici, and that Android 18.
          I don't want to live in regrets.
          So I wrote this for my love.</p>
      </article>
    </main>
{{< / highlight >}}

#### The Preview

Now there is space between buttons.

![Bulma SASS: Helpers][image-ss-71-helpers]

Problem solved.

-- -- --

### 2: Tailor Made Helpers

Alternatively, you can generate your own custom spacing classes.
I have a long article, with thorough example,
a step by step guidance, to make margin and padding classes,
utilizing sass loop.

* [SASS - Loop - Spacing Class][local-loop-spacing]

#### Official Spacing Helpers

> Update

This article was made in March 2019 with Bulma 0.7.

In June 2020, Bulma 0.9 comes out.
And Bulma 0.9 is equipped with spacing helpers.

* [Spacing helpers](https://bulma.io/documentation/helpers/spacing-helpers/)

-- -- --

### Conclusion

Thank you for reading.

We are finished yet with this Bulma article series.
Consider continue to [ [Bulma MD - Overview][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2019/06/11/bulma-md-overview/
[local-loop-spacing]:   {{< baseurl >}}frontend/2019/06/21/sass-loop-spacing-class/

[tutor-07-v-helpers]:   {{< tutor-html-bulma >}}/tutor-07/sass/vendors/bulma-helpers.sass
[tutor-07-css-71-bulma]:    {{< tutor-html-bulma >}}/tutor-07/sass/css-71/bulma.sass
[tutor-07-css-71-helpers]:  {{< tutor-html-bulma >}}/tutor-07/sass/css-71/bulma-helpers.sass

[image-ss-71-tree]:     {{< assets-frontend >}}/2019/03/71-tree-vendors.png

[tutor-07-71-html]:     {{< tutor-html-bulma >}}/tutor-07/71-bulma-helpers.html

[image-ss-71-helpers]:  {{< assets-frontend >}}/2019/03/71-bulma-helpers.png

