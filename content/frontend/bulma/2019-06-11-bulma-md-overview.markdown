---
type   : post
title  : "Bulma MD - Overview"
date   : 2019-06-11T09:17:35+07:00
slug   : bulma-md-overview
categories: [frontend]
tags      : [bulma, css]
keywords  : [plain html, custom theme, summary, overview, material design]
author : epsi
opengraph:
  image: assets-frontend/2019/06/html-bulma-md-preview.png

toc    : "toc-2019-03-html-bulma-step"

excerpt:
  Experimental Bulma Material Design, step by step.

---

### Preface

This article series is successor of previous bulma article series.

> Goal: A Bulma Material Design Experiment.

After using Bulma for a while,
I decide to add custom classes for my tailor made flat theme.

#### Previous Article

This Bulma MD article series start from Step-04.
Step-01 until step-03 contain Bulma preparation.
You can read those article here:

* [Bulma - Overview][local-bulma-overview]

In fact, you should read this first,
before you continue with step-04 and after.

#### Repository

* [Bulma Material Design Test Drive][repository]

#### Tools

> Opening Example in Browser

You can open the `html` file artefact directly from your `file manager`,
or alternatively setup simple web server example such as `browsersync`,
and enjoy the `localhost:3000`.

#### Table of Content

The table content is available as header on each tutorial.
There, I present a Bootstrap Tutorial, step by step, for beginners.

-- -- --

### Begin The Guidance

Material Design start from Step-04.
Step-01 until step-03 contain Bulma preparation.

#### Step 01

> Adding Bulma CSS

* Without Bulma

* Using CDN

* Using Local

![screenshot][screenshot-01]

#### Step 02

> Using Bulma CSS: Navigation Bar

* Simple

* Full Featured

* With jQuery

* With Vue JS

* Adding Font Awesome

![screenshot][screenshot-02]

#### Step 03

> Bulma Custom SASS

* Custom maximum width class

* Responsive gap using custom sass

![screenshot][screenshot-03]

#### Step 04

> Material Design

* Material Color Demo

* Spacing Helper Demo

![screenshot][screenshot-04]

#### Step 05

> HTML Box using Material Design

* Main Blog and Aside Widget

![screenshot][screenshot-05]

#### Step 06

> Finishing

* Blog Post Example

![screenshot][screenshot-06]

-- -- --

### Repository

Have fun, and enjoy coding.

[Bulma Material Design][repository]

Thank you for cloning the repository.

-- -- --

### Begin The Guidance

Let's get the tutorial started.

Consider continue reading [ [Bulma MD - Helper][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2019/06/12/bulma-md-helper/
[local-bulma-overview]: {{< baseurl >}}frontend/2019/03/01/bulma-overview/

[repository]:       https://gitlab.com/epsi-rns/tutor-html-bulma-md/

[screenshot-01]:    https://gitlab.com/epsi-rns/tutor-html-bulma-md/raw/master/step-01/html-bulma-md-preview.png
[screenshot-02]:    https://gitlab.com/epsi-rns/tutor-html-bulma-md/raw/master/step-02/html-bulma-md-preview.png
[screenshot-03]:    https://gitlab.com/epsi-rns/tutor-html-bulma-md/raw/master/step-03/html-bulma-md-preview.png
[screenshot-04]:    https://gitlab.com/epsi-rns/tutor-html-bulma-md/raw/master/step-04/html-bulma-md-preview.png
[screenshot-05]:    https://gitlab.com/epsi-rns/tutor-html-bulma-md/raw/master/step-05/html-bulma-md-preview.png
[screenshot-06]:    https://gitlab.com/epsi-rns/tutor-html-bulma-md/raw/master/step-06/html-bulma-md-preview.png
