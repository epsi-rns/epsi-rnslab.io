---
type   : post
title  : "Bulma MD - Helper"
date   : 2019-06-12T09:17:35+07:00
slug   : bulma-md-helper
categories: [frontend]
tags      : [bulma, css]
keywords  : [helper, material design, shadow, color]
author : epsi
opengraph:
  image: assets-frontend/2019/06/html-bulma-md-preview.png

toc    : "toc-2019-03-html-bulma-step"

excerpt:
  Experimental Bulma Material Design, step by step.
  Using Helper from Materialize CSS and Custom Spacing Class.

---

### Preface

> Goal: Using Helper from Materialize CSS and Custom Spacing Class.

Once, I would like to use pure CSS without framework at all.
But It is going to be long hard article for beginner.
So I use Bulma as a starter,
and pick some goodies from other CSS framework, as helper.

1. Color Classes: taken verbatim copy from Materialize CSS.

2. Spacing Classes: inspired from Bootstrap.

This custom theme is based on this helper.

#### Related Articles

1. [Sass - Loop - Spacing Class][local-sass-spacing]

2. [Less - Conditional - Color Class][local-less-colors]

-- -- --

### 1: Prepare

We should change start from the big picture.
It is how we arrange the `helper.scss`.

#### Directory Structure

It is good time to organize SASS folder.

{{< highlight bash >}}
❯ tree sass/css-41
sass/css-41
├── bulma
│   ├── _derived-variables.sass
│   └── _initial-variables.sass
├── bulma.sass
├── fontawesome.scss
├── helper
│   └── _spacing.sass
├── helper.scss
├── main
│   ├── _decoration.sass
│   └── _layout-page.sass
├── main.sass
└── materialize
    ├── _color-classes.scss
    ├── _color-variables.scss
    └── _shadow.scss

4 directories, 12 files
{{< / highlight >}}

* __.__

![Bulma MD: Restructure SASS Directory][image-ss-40-tree-sass]

#### Stylesheet: Helper

The helper is as simple as importing these four artefacts.

* [gitlab.com/.../sass/css/helper.scss][tutor-04-sass-helper]

{{< highlight scss >}}
@charset "UTF-8";

// Padding and Margin
@import "helper/spacing";

// Global
@import "materialize/shadow";

// Color
@import "materialize/color-variables";
@import "materialize/color-classes";
{{< / highlight >}}

#### Stylesheet: Main

And also, begin to give access to bulma variables.

* [gitlab.com/.../sass/css/main.scss][tutor-04-sass-main]

{{< highlight scss >}}
@import 
  // variables
  "../bootstrap/functions",
  "variables",
  "../bootstrap/variables",
  "../bootstrap/mixins/breakpoints",

  // custom
  "main/layout-page",
  "main/layout-content",
  "main/logo",
  "main/decoration",
  "main/sticky-footer"
;
{{< / highlight >}}

Now we can access bulma variable and mixin.
We will use this variable in a few places for example:

{{< highlight scss >}}
+widescreen
  .maxwidth
    max-width: $widescreen
{{< / highlight >}}

-- -- --

### 2: Materialize Shadow

A verbatim copy from materialize CSS.

* [gitlab.com/.../sass/css/materialize/_shadow.scss][tutor-04-sass-shadow]

{{< highlight scss >}}
// Z-levels
.z-depth-0 {
  box-shadow: none !important;
}

...
{{< / highlight >}}

-- -- --

### 3: Apply to Page: Header and Footer

Consider apply `z-depth` class into a page.

#### Header

{{< highlight html >}}
  <!-- header -->
  <nav role="navigation" aria-label="main navigation"
       class="navbar is-fixed-top maxwidth
              white z-depth-3 hoverable">
    <div class="navbar-brand">
      <a class="navbar-item" href="#">
        <img src="images/logo-gear.png" alt="Home" />
      </a>
      <a class="navbar-item">
        Blog
      </a>
    </div>

    <div id="navbarBulma" class="navbar-menu">
      <div class="navbar-start">
      </div>
      <div class="navbar-end">
      </div>
    </div>
  </nav>
{{< / highlight >}}

#### Footer

{{< highlight html >}}
  <!-- footer -->
  <footer class="site-footer">
    <div class="navbar is-fixed-bottom maxwidth
          has-text-centered is-vcentered
          white z-depth-3 hoverable">
      <div class="column">
        &copy; 2019.
      </div>
    </div>
  </footer>
{{< / highlight >}}

#### Preview

![Bulma MD: Header and Footer][image-ss-40-header-footer]

-- -- --

### 4: Materialize Color

Also a verbatim copy from materialize CSS.

* [gitlab.com/.../sass/css/materialize/_color-variables.scss][tutor-04-sass-variables]

{{< highlight scss >}}
...

$red: (
  "base":       #F44336,
  ...
  "accent-4":    #D50000
);

$pink: (
  "base":       #e91e63,
  ...
  "accent-4":    #c51162
);

...
{{< / highlight >}}

* [gitlab.com/.../sass/css/materialize/_color-classes.scss][tutor-04-sass-classes]

{{< highlight scss >}}
// Color Classes

@each $color_name, $color in $colors {
  @each $color_type, $color_value in $color {
    ...
  }
}

...
{{< / highlight >}}

Alternatively you can use `Open Colors`,
that I use for my `bootstrap OC` project.

* [Bootstrap OC - Sass - Open Color][local-open-colors]

-- -- --

### 5: Spacing Classes

I have already made special article,
to create this bootstrap like spacing class from scratch.

* [Sass - Loop - Spacing Class][local-sass-spacing]

#### Official Spacing Helpers

> Update

This article was made in June 2019 with Bulma 0.7.

In June 2020, Bulma 0.9 comes out.
And Bulma 0.9 is equipped with spacing helpers.

* [Spacing helpers](https://bulma.io/documentation/helpers/spacing-helpers/)

We won't be using custom spacing helper anymore.

#### Expected Class Result

{{< highlight scss >}}
.m-0 {
  margin: 0px !important;
}

.m-t-0 {
  margin-top: 0px !important;
}

.m-b-0 {
  margin-bottom: 0px !important;
}
{{< / highlight >}}

![SCSS: output][image-ss-css]

#### Source Code

I started the loop from zero, to enable reset `margin` or `padding`.

* [gitlab.com/.../sass/css/helper/_spacing.sass][tutor-04-sass-spacing]

{{< highlight sass >}}
// variable initialization
$loop-begin:  0 !default
$loop-end:   25 !default
$interval:    5 !default

// sub-property: abbreviation
$sides:  (top: t, bottom: b, left: l, right: r)
$sidesy: (top, bottom)
$sidesx: (left, right)
$properties: (margin: m, padding: p)

// loop
$cursor: $loop-begin

@while $cursor <= $loop-end
  @each $prop, $name in $properties
    .#{$name}-#{$cursor}
      #{$prop}: #{$cursor}px !important

    @each $side, $subname in $sides
      .#{$name}-#{$subname}-#{$cursor}
        #{$prop}-#{$side}: #{$cursor}px !important

    .#{$name}-y-#{$cursor}
      @each $side in $sidesy
        #{$prop}-#{$side}: #{$cursor}px !important

    .#{$name}-x-#{$cursor}
      @each $side in $sidesx
        #{$prop}-#{$side}: #{$cursor}px !important

  $cursor: $cursor + $interval
{{< / highlight >}}

![SCSS: output][image-ss-sass]

-- -- --

### 6: Color Gradient Example

Consider apply the color class into a page,
along with shadow class.

{{< highlight html >}}
  <!-- main -->
  <main class="layout-base maxwidth section">

    <div role="main" 
          class="column is-full mb-1
                 blue darken-4 blue-text text-lighten-3
                 z-depth-3 hoverable">
        <h4 class="title is-4 has-text-light">Your Mission</h4>
    </div>

    <div role="main" 
          class="column is-full mb-1
                 blue darken-2 z-depth-3 hoverable">
        <p>To have, to hold, to love,
        cherish, honor, and protect?</p>
    </div>

    <div role="main" 
          class="column is-full mb-1
                 blue lighten-1 z-depth-3 hoverable">
        <p>To shield from terrors known and unknown?
        To lie, to deceive?</p>
    </div>

    <div role="main" 
          class="column is-full mb-1
                 blue lighten-3 z-depth-3 hoverable">
        <p>To live a double life,
        to fail to prevent her abduction,
        erase her identity, 
        force her into hiding,
        take away all she has known.</p>
    </div>
    
    <div role="main" 
          class="column is-full mb-1
                 blue lighten-5 z-depth-3 hoverable">
        <blockquote>
        As always, should you be caught or killed,
        any knowledge of your actions will be disavowed
        </blockquote>
    </div>

  </main>
{{< / highlight >}}

#### Preview

![Bulma MD: Gradient Colors][image-ss-41-colors]

-- -- --

### 7: Landing Page Example

Once again, try this color for landing page.

{{< highlight html >}}
  <!-- main -->
  <div class="layout-base maxwidth">
    <main role="main" 
          class="column is-full has-text-centered
                 white z-depth-3 hoverable">
      <article class="blog-post my-5">
        <p>
          <a class="button is-dark blue-grey darken-2 hoverable mb-1"
             href="#">Articles Sorted by Month</a>
          <a class="button is-dark blue-grey darken-1 hoverable mb-1"
             href="#">Articles Sorted by Tag</a>
        </p>

        <br/>

        <p>As always,
        should you be caught or killed,
        any knowledge of your actions will be disavowed.</p>
      </article>
    </main>
  </div>
{{< / highlight >}}

#### Preview

![Bulma MD: Landing Page][image-ss-42-landing-page]

The looks of the page increase significantly with the right colors.

-- -- --

### What is Next ?

It is going to be fun to apply those helpers to wdiget.
Consider continue reading [ [Bulma MD - Widget][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2019/06/13/bulma-md-widget/

[local-sass-spacing]:   {{< baseurl >}}frontend/2019/06/21/sass-loop-spacing-class/
[local-less-colors]:    {{< baseurl >}}frontend/2019/10/04/less-conditional-color-class/
[local-open-colors]:    {{< baseurl >}}frontend/2020/03/11/bootstrap-oc-sass-open-color/

[tutor-04-sass-main]:       {{< tutor-html-bulma-md >}}/step-04/sass/css/main.sass
[tutor-04-sass-helper]:     {{< tutor-html-bulma-md >}}/step-04/sass/css/helper.sass

[tutor-04-sass-spacing]:    {{< tutor-html-bulma-md >}}/step-04/sass/css-41/helper/_spacing.sass
[tutor-04-sass-classes]:    {{< tutor-html-bulma-md >}}/step-04/sass/css-41/materialize/_color-classes.sass
[tutor-04-sass-variables]:  {{< tutor-html-bulma-md >}}/step-04/sass/css-41/materialize/_color-variables.sass
[tutor-04-sass-shadow]:     {{< tutor-html-bulma-md >}}/step-04/sass/css-41/materialize/_shadow.sass

[image-ss-sass]:    {{< assets-frontend >}}/2019/10/scss-while-each-script.png
[image-ss-css]:     {{< assets-frontend >}}/2019/10/scss-while-each-output.png

[image-ss-40-tree-sass]:    {{< assets-frontend >}}/2019/06/40-tree-sass-directory.png
[image-ss-40-header-footer]:{{< assets-frontend >}}/2019/06/40-header-footer.png
[image-ss-41-colors]:       {{< assets-frontend >}}/2019/06/41-colors.png
[image-ss-42-landing-page]: {{< assets-frontend >}}/2019/06/42-landing-page.png

