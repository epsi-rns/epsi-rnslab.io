---
type   : post
title  : "Bulma MD - Widget"
date   : 2019-06-13T09:17:35+07:00
slug   : bulma-md-widget
categories: [frontend]
tags      : [bulma, css]
keywords  : [helper, material design, shadow, color]
author : epsi
opengraph:
  image: assets-frontend/2019/06/html-bulma-md-preview.png

toc    : "toc-2019-03-html-bulma-step"

excerpt:
  Experimental Bulma Material Design, step by step.
  Building multicolor widget with flat design.

---

### Preface

> Goal: Building multicolor widget with flat design.

Since I'm not a web designer, I rarely make my own theme.
I simply don't have the sense of art to make the mockup with inkscape.
So here is what I do, I make a pure css theme.
One of them is this flat design, featuring multicolored widget.

#### Responsive Requirement

This article is not a bootstrap tutorial anymore,
but more like a custom theme tutorial.
Even though the page based on bootstrap.

> Custom Class

Yes. I have to do it myself. And it is fun.

#### Multicolor Requirement

> Palette

We can either use Google Materialize Color, or Open Color.

-- -- --

### 1: Responsive Gap

Consider go further from our previous chapter.
From plain looks, to multicolor.

#### How does it Works?

> putting `wrapper` class inside `column` class.

You can use `div` element inside `div` element,
or for `html5` you can utilize `section` inside `main`.

#### Other Reference

> I don't want to repeat myslef.

To see more thorough example of how the responsive gap works,
you might consider to look at this `bootstrap` article.
If you don't understand, just do fast-reading the article below:

* [Bootstrap OC - Sass - Responsive][local-responsive]

The difference is responsive in Bulma is easier.
And this article series is using `.sass` format,
rather than `.scss`.

#### Stylesheet

The very basic part of this multicolor is,
adding padding to our previous layout.

* [gitlab.com/.../sass/css-51/main/_layout-content.scss][tutor-05-51-sass-content].

{{< highlight scss >}}
// gap main and aside

.blog
  padding: 10px

.widget
  padding: 10px

// decoration

main.column,
aside.column
  padding: 0 !important

.main-wrapper,
.aside-wrapper
  padding: 0 10px 0 10px

// responsive

.main-wrapper,
.aside-wrapper
  margin-bottom: 20px

+tablet
  .main-wrapper
    margin-right: 10px
    margin-left: 10px
  .aside-wrapper
    margin-left: 10px
    margin-right: 10px

+widescreen
  .main-wrapper
    margin-left: 0px
  .aside-wrapper
    margin-right: 0px
{{< / highlight >}}

#### HTML Skeleton

The basic layout is the same as previous,
except with additional colors.

* [gitlab.com/.../51-responsive.html][tutor-05-51-html].

{{< highlight html >}}
  <!-- main -->
  <div class="columns is-8 layout-base maxwidth">

    <main role="main" 
          class="column is-two-thirds">
      <div class="main-wrapper blue">
        <div class="blog white z-depth-3 hoverable">
          Page Content
        </div>
      </div>
    </main>

    <aside class="column is-one-thirds">
      <div class="aside-wrapper green">
        <div class="widget white z-depth-3 hoverable">
          Side Menu
        </div>
      </div>
    </aside>
  </div>
{{< / highlight >}}

In mobile screen it is simply as below:

![Bootstrap OC: Responsive Multicolor in Mobile Screen][image-ss-51-responsive]

#### HTML Content

Consider give a horizontal padding to a content,
and see how it looks in desktop screen.

* [gitlab.com/.../52-blog.html][tutor-05-52-html].

{{< highlight html >}}
  <!-- main -->
  <div class="columns is-8 layout-base maxwidth">

    <main role="main" 
          class="column is-two-thirds">

      <section class="main-wrapper blue">
        <div class="blog px-2 white z-depth-3 hoverable">
          <article class="blog-post">
            <h4 class="title is-4">Cause You're Different!</h4>
            <p>When you can do the things that I can, but you don't, 
            and then the bad things happen,
            they happen because of you.</p>
          </article>
        </div>
      </section>

    </main>

    <aside class="column is-one-thirds">

      <section class="aside-wrapper green">
        <div class="widget px-2 white z-depth-3 hoverable">
          <p>How many times has he been,
          betrayed, disavowed, cast aside?</p>
        </div>
      </section>

      <section class="aside-wrapper teal">
        <div class="widget p-x-10 white z-depth-3 hoverable">
          <p>And how long before a man like that has had enough?</p>
        </div>
      </section>

    </aside>
  </div>
{{< / highlight >}}

![Bootstrap OC: Responsive Multicolor in Desktop Screen][image-ss-52-blog]

-- -- --

### 2: Header and Footer

Consider separate design in each multicolor section,
so that each have their own `header` and `footer`.

#### Stylesheet

The very basic part of this multicolor is,
adding padding to our previous layout.

* [gitlab.com/.../sass/css-53/main/_layout-content.scss][tutor-05-53-sass-content].

{{< highlight scss >}}
// gap main and aside

.blog
  padding: 0px

.blog-header
  padding: 5px 10px 5px 10px

.blog-body
  padding: 5px 10px 5px 10px

.widget
  padding: 0px

.widget-header
  padding: 5px 10px 5px 10px

.widget-body
  padding: 5px 10px 5px 10px
{{< / highlight >}}

And the vertical gap between widget can be solved with `:last-child`.

{{< highlight scss >}}
.main-wrapper,
.aside-wrapper
  margin-bottom: 20px

.aside-wrapper:last-child
  margin-bottom: 0px
{{< / highlight >}}

#### HTML Skeleton

We have many additional `element` layer here.
Each `blog` and the `widget`, has `header` and `body`.

* Blog: `main.column > section.main-wrapper > div.blog > article.blog-body`

{{< highlight html >}}
    <main role="main" class="column is-two-thirds">

      <section class="main-wrapper blue">
        <div class="blog white z-depth-3 hoverable">

          <section class="blog-header blue lighten-5">
            ...
          </section>

          <article class="blog-body">
            ...
          </article>

        </div>
      </section>

    </main>
{{< / highlight >}}

* Widget: `aside.column > section.aside-wrapper > div.widget > div.widget-body`

{{< highlight html >}}
    <aside class="column is-one-thirds">

      <section class="aside-wrapper green">
        <div class="widget white z-depth-3 hoverable">

          <div class="widget-header green lighten-4">
            ...
          </div>

          <div class="widget-body">
            ...
          </div>

        </div>
      </section>

    </aside>
{{< / highlight >}}

#### HTML Content

Consider give multiple widget content,
and see how it looks in desktop screen.

* [gitlab.com/.../53-widget.html][tutor-05-53-html].

{{< highlight html >}}
  <!-- main -->
  <div class="columns is-8 layout-base maxwidth">

    <main role="main" 
          class="column is-two-thirds">

      <section class="main-wrapper blue">
        <div class="blog white z-depth-3 hoverable">

          <section class="blog-header blue lighten-5">
            <div class="main_title"> 
              <h4 class="title is-4" itemprop="name headline">
                <a href="#">Cause You're Different!</a></h4>
            </div>
          </section>

          <article class="blog-body" itemprop="articleBody">
            <p>When you can do the things that I can, but you don't, 
            and then the bad things happen,
            they happen because of you.</p>
          </article>

        </div>
      </section>

    </main>

    <aside class="column is-one-thirds">

      <section class="aside-wrapper green">
        <div class="widget white z-depth-3 hoverable">

          <div class="widget-header green lighten-4">
            <strong>How many times ?</strong>
            <span class="fas fa-biking is-pulled-right"></span>
          </div>

          <div class="widget-body">
            <p>Has he been, betrayed, disavowed, cast aside.</p>
          </div>

        </div>
      </section>

      <section class="aside-wrapper teal">
        <div class="widget white z-depth-3 hoverable">

          <div class="widget-header teal lighten-4">
            <strong>And how long?</strong>
            <span class="fas fa-cat is-pulled-right"></span>
          </div>

          <div class="widget-body">
            <p>Before a man like that has had enough.</p>
          </div>

        </div>
      </section>

    </aside>
  </div>
{{< / highlight >}}

![Bootstrap OC: Multicolor Widget in Desktop Screen][image-ss-53-widget]

-- -- --

### 3: List

I have four blogs, and each blog contain nine different widgets.
These two are the basic most widget.

* List

* Tags

Consider begin with List.

#### Stylesheet

Using font such as `FontAwesome` with `CSS` is easier than using `SVG`.

* [gitlab.com/.../sass/css-54/main/_list.scss][tutor-05-54-sass-list].

{{< highlight scss >}}
// -- -- -- -- --
// _list.scss

ul.widget-list li:hover
  background: map-get($yellow, 'lighten-2')

ul.widget-list
  padding: 0px

ul.widget-list
  padding-left: 0
  list-style: none
  font-size: 13px
  margin-left: 20px

ul.widget-list li:before
  font-family: "Font Awesome 5 Free"
  font-weight: 900
  content: "\f054"
  color: map-get($grey, 'base')
  margin: 0 5px 0 -12px

ul.widget-list li:hover:before
  color: map-get($yellow, 'base')
{{< / highlight >}}

`map-get` is `sass` language.

* [sass-lang.com/documentation/values/maps](https://sass-lang.com/documentation/values/maps)__.__

Do not forget to alter the `main.scss`.

* [gitlab.com/.../sass/css-54/main.scss][tutor-05-54-sass-main].

{{< highlight scss >}}
...

// Tailor Made
@import "main/layout-page"
@import "main/layout-content"
@import "main/decoration"

@import "post/content"
@import "main/list"
{{< / highlight >}}

#### HTML Content

Consider this list widget content.

* [gitlab.com/.../54-list.html][tutor-05-54-html].

{{< highlight html >}}
      <section class="aside-wrapper green">
        <div class="widget white z-depth-3 hoverable">

          <div class="widget-header green lighten-4">
            <strong>Isle of Friends</strong>
            <span class="fa fa-child is-pulled-right"></span>
          </div>

          <div class="widget-body">
            <ul class="widget-list">
              <li><a href="http://epsi-rns.github.io/">Linux/BSD Desktop Customization</a></li>
              <li><a href="http://epsi-rns.gitlab.io/">Web Development Blog</a></li>
              <li><a href="http://oto-spies.info/">Car Painting and Body Repair.</a></li>
            </ul>
          </div>

        </div>
      </section>
{{< / highlight >}}

![Bootstrap OC: List Widget Example][image-ss-54-list]

-- -- --

### 4: Tags

Other basic widget example is the `tags` widget.

#### Stylesheet

We don't need any.
Tag class have been provided by Bulma.

#### HTML Content

Consider this list widget content.

* [gitlab.com/.../55-tags.html][tutor-05-55-html].

{{< highlight html >}}
    <aside class="column is-one-thirds">

      <section class="aside-wrapper green">
        <div class="widget white z-depth-3 hoverable">

          <div class="widget-header green lighten-4">
            <strong>Categories</strong>
            <span class="fa fa-folder is-pulled-right"></span>
          </div>

          <div class="widget-body">
            <a class="tag is-small green lighten-4 z-depth-1" href="#">
              mission&nbsp;<span class="fa fa-folder"></span>
            </a>&nbsp;
    
            <a class="tag is-small green lighten-4 z-depth-1" href="#">
              accomplished&nbsp;<span class="fa fa-folder"></span>
            </a>&nbsp;
          </div>

        </div>
      </section>

      <section class="aside-wrapper teal">
        <div class="widget white z-depth-3 hoverable">

          <div class="widget-header teal lighten-4">
            <strong>Tags</strong>
            <span class="fa fa-tag is-pulled-right"></span>
          </div>

          <div class="widget-body">
            <a class="tag is-small is-light white"
               href="#">
              <span class="fa fa-folder"></span>&nbsp;command
            </a>&nbsp;

            <a class="tag is-small is-light white"
               href="#">
              <span class="fa fa-folder"></span>&nbsp;agent
            </a>&nbsp;

            <a class="tag is-small is-light white"
               href="#">
              <span class="fa fa-folder"></span>&nbsp;duty
          </a>&nbsp;
          </div>

        </div>
      </section>

    </aside>
{{< / highlight >}}

![Bootstrap OC: Tags Widget Example][image-ss-55-tags]

-- -- --

### What is Next ?

The last part is about content, the blog post and its decoration.
Consider continue reading [ [Bulma MD - Blog Post][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2019/06/14/bulma-md-blog-post/
[local-responsive]:     {{< baseurl >}}frontend/2020/03/12/bootstrap-oc-sass-responsive/

[tutor-05-51-html]:     {{< tutor-html-bulma-md >}}/step-05/51-responsive.html
[tutor-05-52-html]:     {{< tutor-html-bulma-md >}}/step-05/52-blog.html
[tutor-05-53-html]:     {{< tutor-html-bulma-md >}}/step-05/53-widget.html
[tutor-05-54-html]:     {{< tutor-html-bulma-md >}}/step-05/54-list.html
[tutor-05-55-html]:     {{< tutor-html-bulma-md >}}/step-05/55-tags.html

[tutor-05-51-sass-content]: {{< tutor-html-bulma-md >}}/step-05/sass/css-51/main/_layout-content.sass
[tutor-05-53-sass-content]: {{< tutor-html-bulma-md >}}/step-05/sass/css-53/main/_layout-content.sass
[tutor-05-54-sass-list]:    {{< tutor-html-bulma-md >}}/step-05/sass/css-54/main/_list.sass
[tutor-05-54-sass-main]:    {{< tutor-html-bulma-md >}}/step-05/sass/css-54/main.sass

[image-ss-51-responsive]:       {{< assets-frontend >}}/2019/06/51-responsive-multicolor-mobile.png
[image-ss-52-blog]:             {{< assets-frontend >}}/2019/06/52-responsive-multicolor-desktop.png
[image-ss-53-widget]:           {{< assets-frontend >}}/2019/06/53-multicolor-widget.png
[image-ss-54-list]:             {{< assets-frontend >}}/2019/06/54-widget-list.png
[image-ss-55-tags]:             {{< assets-frontend >}}/2019/06/55-widget-tags.png

