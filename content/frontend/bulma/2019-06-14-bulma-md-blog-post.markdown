---
type   : post
title  : "Bulma MD - Blog Post"
date   : 2019-06-14T09:17:35+07:00
slug   : bulma-md-blog-post
categories: [frontend]
tags      : [bulma, css]
keywords  : [helper, material design, shadow, color]
author : epsi
opengraph:
  image: assets-frontend/2019/06/html-bulma-md-preview.png

toc    : "toc-2019-03-html-bulma-step"

excerpt:
  Experimental Bulma Material Design, step by step.
  Prepare common blog post theme for use later with framework.
  Focusing on content page.

---

### Preface

> Goal: Focusing on content page.

This chapter is about, 
preparing common blog post theme for use later with framework.

-- -- --

### 1: Prepare

This chapter also summarize what we have done so far.

#### Directory Preparation

We need a new `SASS` section for this `post`.

* [gitlab.com/.../sass/][tutor-06-sass].

{{< highlight bash >}}
❯ tree sass/css-62
sass/css-62
├── bulma
│   ├── _derived-variables.sass
│   └── _initial-variables.sass
├── bulma.sass
├── fontawesome.scss
├── helper
│   └── _spacing.sass
├── helper.scss
├── main
│   ├── _decoration.sass
│   ├── _layout-content.sass
│   ├── _layout-page.sass
│   └── _list.sass
├── main.sass
├── materialize
│   ├── _color-classes.scss
│   ├── _color-variables.scss
│   └── _shadow.scss
└── post
    ├── _content.sass
    ├── _header.sass
    └── _navigation.sass

5 directories, 17 files
{{< / highlight >}}

This only require two new file artefact.

* _content.scss

* _header.scss

* _navigation.scss __.__

![Bulma MD: Restructure SASS Directory][image-ss-61-tree-sass]

In real world blogging,
this post section has a more `SASS` stylesheet document.
I have about five or seven, depend on the framework that I choose.

#### Main SASS

Of course you have to update your main SASS to reflect the above

* [gitlab.com/.../sass/css-61/main.scss][tutor-06-61-sass-main].

{{< highlight scss >}}
...

// Tailor Made
@import "main/layout-page"
@import "main/layout-content"
@import "main/decoration"
@import "main/list"

@import "post/content"
@import "post/header"
@import "post/navigation"
{{< / highlight >}}

![Bulma MD: Tree and Main SASS][image-ss-63-nerdtree-main]

-- -- --

### 2: Header

Blog post header customization is very common.
I utilize badge (or tag).
Luckily most CSS framework has built in support to display tag.

#### Stylesheet

* [gitlab.com/.../sass/css-61/post/_content.scss][tutor-06-61-sass-content].

{{< highlight scss >}}
h1, h2, h3, h4, h5, h6
  font-family: "Playfair Display", Georgia, "Times New Roman", serif

/*
 * Blog posts
 */

.blog-body
  margin-bottom: 1rem

.blog-header .title
  margin-bottom: .25rem

.blog-header .meta
  margin-bottom: 2.0rem

.blog-body p
  margin-top: 0.5rem
  margin-bottom: 0.5rem
{{< / highlight >}}

* [gitlab.com/.../sass/css-61/post/_header.scss][tutor-06-61-sass-header].

{{< highlight scss >}}

.blog-header .title,
.blog-header .subtitle
  a
    color: map-get($grey, "darken-3")
  a:hover
    color: map-get($blue, "base")
{{< / highlight >}}

#### HTML Content

I know this example is long.
this is just a demo of tag elements,
along with feather icons.

* [gitlab.com/.../61-post-header.html][tutor-06-61-html].

{{< highlight html >}}
    <main role="main" 
          class="column is-two-thirds">

      <section class="main-wrapper blue">
        <div class="blog white z-depth-3 hoverable">

          <section class="blog-header blue lighten-5">

            <div class="main_title"> 
              <h4 class="title is-4" itemprop="name headline">
                <a href="#">Cause You're Different!</a></h4>
            </div>

            <div class="is-clearfix"></div>

            <div class="field meta">
              <div class="is-pulled-left">
              <span class="meta_author tag is-small is-dark
                           indigo z-depth-1 hoverable">
                  <span class="fa fa-user"></span>
                  &nbsp;
                  <span itemprop="author"
                        itemscope itemtype="http://schema.org/Person">
                  <span itemprop="name">epsi</span></span>
              </span>
              &nbsp;
              </div>

              <div class="is-pulled-left">
                <span class="meta-time tag is-small is-dark
                             green z-depth-1 hoverable">
                  A few days ago.
                </span>
                &nbsp;
              </div>

              <div class="is-pulled-right">
                  <a href="#">
                    <span class="tag is-small is-dark
                                 teal z-depth-1 hoverable"
                      ><span class="fa fa-tag"></span>&nbsp;
                      love</span></a>
                &nbsp;
              </div>

              <div class="is-pulled-right">
                  <a href="#">
                    <span class="tag is-small is-dark
                                 blue z-depth-1 hoverable"
                      ><span class="fa fa-folder"></span>&nbsp;
                      rock</span></a>
                &nbsp;
              </div>
            </div>

            <div class="is-clearfix"></div>

          </section>

          <article class="blog-body" itemprop="articleBody">
            <p>When you can do the things that I can, but you don't, 
            and then the bad things happen,
            they happen because of you.</p>
          </article>

        </div>
      </section>

    </main>
{{< / highlight >}}

![Bulma MD: Blog Post Header][image-ss-61-post-header]

-- -- --

### 3: Navigation

Blog post navigation along with pagination customization,
is also very common.
And most CSS framework has built in support to navigation.

#### Stylesheet

* [gitlab.com/.../sass/css/post/_navigation.scss][tutor-06-62-sass-nav].

{{< highlight scss >}}
a.post-previous:after
    content: " previous"

a.post-next:before
    content: "next "

.blog-body .pagination
  margin-top: 1rem
  margin-bottom: 1rem
{{< / highlight >}}

#### HTML Content

Just add navigation code after the `article` element.

* [gitlab.com/.../62-post-navigation.html][tutor-06-62-html].

{{< highlight html >}}
          <article class="blog-body" itemprop="articleBody">
            <p>When you can do the things that I can, but you don't, 
            and then the bad things happen,
            they happen because of you.</p>

            <nav class="pagination is-centered" 
                 role="navigation" aria-label="pagination">

                <a href="#" class="pagination-previous post-previous"
                   title="next article name">&laquo;&nbsp;</a>

                <a href="#" class="pagination-next post-next"
                   title="previous article name">&nbsp;&raquo;</a>
            </nav>
          </article>
{{< / highlight >}}

![Bulma MD: Blog Post Navigation][image-ss-62-post-nav]

-- -- --

### 4: Landing Page

Again, test our single page design, with a more complete landing page.

* [gitlab.com/.../63-landing-page.html][tutor-06-63-html].

{{< highlight html >}}
  <!-- main -->
  <div class="columns is-8 layout-base maxwidth">

    <main role="main" 
          class="column is-full">

      <section class="main-wrapper blue">
        <div class="blog white z-depth-3 hoverable">

          <section class="blog-header has-text-centered blue lighten-5">

            <div class="main_title"> 
              <h3 class="title is-3 is-spaced"
                  itemprop="name headline">
                <a href="#">Your Mission</a></h3>
              <h4 class="subtitle is-4">
                <a href="#">Should you decide to accept</a></h4>
            </div>
          </section>

          <article class="blog-body has-text-centered"
                   itemprop="articleBody">

            <br/>

            <p>
              <a class="button is-dark
                        blue-grey darken-2 hoverable m-b-5"
                 href="#">Articles Sorted by Month</a>
              <a class="button is-dark
                        blue-grey darken-1 hoverable m-b-5"
                 href="#">Articles Sorted by Tag</a>
            </p>

            <p>As always,
            should you be caught or killed,
            any knowledge of your actions will be disavowed.</p>

            <div class="justify-content-center">
              <img src="images/one-page.png" 
                   alt="business card">
            </div>

            <p class="lead text-muted">
              <span class="fas fa-home"></span>&nbsp;
                Whitewood Street, Monday Market,
                East Jakarta, 55112, Indonesia.
            </p>

          </article>
        </div>
      </section>

    </main>

  </div>
{{< / highlight >}}

![Bulma MD: Landing Page][image-ss-63-landing-page]

Landing page is not really blog post post.
I just put here just becaue I don't want to make new article.

I just keep this article simple.

-- -- --

### 5: Summary

As a summary, here is the final looks,
of our blog post in desktop screen.

![Bulma MD: Blog Post Desktop][image-ss-62-post-desktop]

-- -- --

### What is Next ?

The last part is a bonus.
It is more like a javacript tutorial,
which use a bunch of Bulma class.
Consider continue reading [ [Bulma MD - Javascript Toggler][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2019/06/15/bulma-md-javascript-toggler/

[tutor-06-sass]:        {{< tutor-html-bulma-md >}}/step-06/sass/

[tutor-06-61-html]:     {{< tutor-html-bulma-md >}}/step-06/61-post-header.html
[tutor-06-62-html]:     {{< tutor-html-bulma-md >}}/step-06/62-post-navigation.html
[tutor-06-63-html]:     {{< tutor-html-bulma-md >}}/step-06/63-landing-page.html

[tutor-06-61-sass-main]:   {{< tutor-html-bulma-md >}}/step-06/sass/css-61/main.sass
[tutor-06-61-sass-content]:{{< tutor-html-bulma-md >}}/step-06/sass/css-61/post/_content.sass
[tutor-06-61-sass-header]: {{< tutor-html-bulma-md >}}/step-06/sass/css-61/post/_header.sass
[tutor-06-62-sass-nav]:    {{< tutor-html-bulma-md >}}/step-06/sass/css-62/post/_navigation.sass

[image-ss-61-tree-sass]:    {{< assets-frontend >}}/2019/06/61-tree-sass-directory.png
[image-ss-61-post-header]:  {{< assets-frontend >}}/2019/06/61-post-header.png
[image-ss-62-post-nav]:     {{< assets-frontend >}}/2019/06/62-post-navigation.png
[image-ss-62-post-desktop]: {{< assets-frontend >}}/2019/06/62-post-desktop.png
[image-ss-63-landing-page]: {{< assets-frontend >}}/2019/06/63-landing-page.png
[image-ss-63-nerdtree-main]:{{< assets-frontend >}}/2019/06/61-nerdtree-main-sass.png
