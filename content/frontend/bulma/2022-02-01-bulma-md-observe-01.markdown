---
type   : post
title  : "Animation Observer - Part One"
date   : 2022-02-01T09:17:35+07:00
slug   : animation-observer-01
categories: [frontend]
tags      : [bulma, css, javascript]
keywords  : [animate, intersection observer]
author : epsi
opengraph:
  image: assets/posts/frontend/2022/02-observer/01-preview-wide.png

toc    : "toc-2022-02-animation"

excerpt:
  Trigger animation on scroll using intersection observer.

---

### Preface

> Goal: Trigger animation on scroll using intersection observer.

I have been impressed by how easy can be done using CSS only.

Two very cool CSS library that I use in my blog are
1. [animate.css](https://animate.style/)
2. [hover.css](https://github.com/IanLunn/Hover)

Still amazed by this `animate.style` library,
I need a way to show the animation,' over and over again.
For example, when the element shown by scrolling up and down,
I want the animation to be replayed again.

Note that, we do not need to do anything with `hover.css`.
The hover triggered well with pure css.

#### Intersection Observer

Since by the time this article is written,
there is no pure CSS approach to do such thing,
I need javascript to trigger the animation,
by using scroll event.

The method has an API called intersection observer

* [MDN Web Docs - Intersection Observer API][MDN]

-- -- ---- -- --

### 1: General Preview

This is what javascript animate observer that we want to achieve.
To make it easier for you to get the idea,
you can examine the video below.

{{< embed-video width="480" height="340"
    src="assets/posts/frontend/2022/02-observer/10-working.mp4" >}}

-- -- --

### 2: Page Preparation

I'm using Bulma tutorial made years ago, with a few enhacement.

#### Source Code

The last Bulma step is `Tutor 06`,
and for this tutorial I give it a name as `Tutor 07`.

The source code can be found below:

* [Tutor-07: Animate Observer][step-07]

#### Adding Image

You can just clone the repository,
and open the `html` example in browser,
along with the CSS and JS.
Without any need to compile the CSS.

However this below, is additional SCSS,
to differ from our last tutorial.

{{< highlight css >}}
.layout-base
  padding-top: 0.5rem
  padding-bottom: 0.5rem

#sitedesc_image
  width: 500px
  height:100px
  max-width: 100%
  background: transparent url(../images/blue-sfme.png) no-repeat left top
  background-size: 100% auto
  float: none
  margin-top: 0
  margin-bottom: 10px
  margin-right: auto
  margin-left: auto
{{< / highlight >}}

Now we can apply the `html`.

{{< highlight html >}}
    <main role="main" 
          class="column is-full">

      <div class="zoomIn"
              id="sitedesc_image"></div>

      <section class="main-wrapper blue">
        <div class="blog white z-depth-3 hoverable">
          ...
        </div>
      </section>

    </main>
{{< / highlight >}}

Here is what we want to achieve.

![Site Description][image-site-desc]

We are going to make this bounce later.

-- -- --

### 3: Animate CSS

To make the animation works,
you can examine the CSS for each effect.

#### HTML Head

First add this stylesheet in the HTML head.

* [gitlab.com/.../71-library.html][tutor-07-71-html].

{{< highlight html >}}
  <link rel="stylesheet" type="text/css"
        href="css-71/animate-zoom.css">
{{< / highlight >}}

#### Stylesheet: Keyframe

I grabbed the stylesheet from the original CSS,
and strip down for tutorial purpose.

* [gitlab.com/.../css-71/animate-zoom.css][tutor-07-animate-css].

The stylesheet contain keyframes,
and the related classes.

{{< highlight css >}}
@keyframes zoomIn {
  from { opacity: 0.3; transform: scale(0.3, 0.3); }
  50%  { opacity: 1; }
}

@keyframes zoomOut {
  from { opacity: 1; }
  50%  { opacity: 0.3; transform: scale(0.3, 0.3); }
  to   { opacity: 0.5; }
}
{{< / highlight >}}

#### Stylesheet: Classes

And of course, the class itself, in verbose format.

{{< highlight css >}}
.zoomIn_hover:hover,
.zoomIn {
  animation-duration: 5s;
  animation-delay: 2s;
  animation-fill-mode: both;
  animation-name: zoomIn;
  animation-iteration-count: 3;
  animation-timing-function: ease;
  animation-direction: normal;
}
{{< / highlight >}}

And the shorter brief format if you like.

{{< highlight css >}}
.zoomOut_hover:hover,
.zoomOut {
  animation: zoomOut 4s ease 1s 4 alternate;
}
{{< / highlight >}}

You can learn about animation in w3schools.com or anywhere.
Just `google` or `duckduckgo`.
I won't go further to explain the animation behind the scene.

#### HTML: Animate: Zoom In

Now you can add zoom in animation, by adding class.

{{< highlight html >}}
      <div class="zoomIn"
              id="sitedesc_image"></div>
{{< / highlight >}}

And also its hover sidekick.

{{< highlight html >}}
<div class="justify-content-center">
   <img src="images/one-page.png"
      class="zoomIn_hover"
        alt="business card">
</div>
{{< / highlight >}}

With the result as below:

{{< embed-video width="480" height="440"
    src="assets/posts/frontend/2022/02-observer/12-working.mp4" >}}

#### HTML: Animate: Zoom Out

And also this weird zoom out animation.

{{< highlight html >}}
      <div class="zoomOut"
              id="sitedesc_image"></div>
{{< / highlight >}}

With the result as below:

{{< embed-video width="480" height="280"
    src="assets/posts/frontend/2022/02-observer/13-working.mp4" >}}

-- -- --

### 4: Hover CSS

The same applied with hover animation.
I stripped the script for tutorial purpose.

* [gitlab.com/.../css-71/hover-buzz.css][tutor-07-hover-css].

The stylesheet contain keyframes,
and the related classes.

#### HTML Head

First add this stylesheet in the HTML head.

{{< highlight html >}}
  <link rel="stylesheet" type="text/css"
        href="css-71/hover-buzz.css">
{{< / highlight >}}

#### Stylesheet: Keyframe

{{< highlight css >}}
/* Buzz */
@keyframes hvr-buzz {
  50% {
    transform: translateX(3px) rotate(2deg);
  }
  100% {
    transform: translateX(-3px) rotate(-2deg);
  }
}
{{< / highlight >}}

*

#### Stylesheet: Classes

And of course, the class itself, in verbose format.

{{< highlight css >}}
.hvr-buzz {
  display: inline-block;
  vertical-align: middle;
  transform: perspective(1px) translateZ(0);
  box-shadow: 0 0 1px rgba(0, 0, 0, 0);
}
.hvr-buzz:hover, .hvr-buzz:focus, .hvr-buzz:active {
  animation-name: hvr-buzz;
  animation-duration: 0.15s;
  animation-timing-function: linear;
  animation-iteration-count: infinite;
}
{{< / highlight >}}

#### HTML: Animate: Hover Buzz

Just add `hvr-buzz` class.

{{< highlight html >}}
<p>
  <a class="button is-dark hvr-buzz
            blue-grey darken-2 hoverable mb-1"
     href="#">Articles Sorted by Month</a>
  <a class="button is-dark hvr-buzz
            blue-grey darken-1 hoverable mb-1"
     href="#">Articles Sorted by Tag</a>
</p>
{{< / highlight >}}

With the result as below:

{{< embed-video width="480" height="240"
    src="assets/posts/frontend/2022/02-observer/11-working.mp4" >}}

Pretty cool right?

-- -- --

### What is Next ?

This should be enough for animation introduction.
We are going to continue with intersection observer.

Consider continue reading [ [Animation Observer - Part Two][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2022/02/03/animation-observer-02/

[MDN]:      https://developer.mozilla.org/en-US/docs/Web/API/Intersection_Observer_API
[step-07]:  https://gitlab.com/epsi-rns/tutor-html-bulma-md/-/tree/master/step-07

[image-site-desc]:      {{< baseurl >}}assets/posts/frontend/2022/02-observer/01-sitedesc-sfme.png

[tutor-07-71-html]:     {{< tutor-html-bulma-md >}}/step-07/71-library.html
[tutor-07-animate-css]: {{< tutor-html-bulma-md >}}/step-07/css-71/animate-zoom.css
[tutor-07-hover-css]:   {{< tutor-html-bulma-md >}}/step-07/css-71/hover-buzz.css
