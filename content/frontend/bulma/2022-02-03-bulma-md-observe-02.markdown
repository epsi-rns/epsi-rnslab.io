---
type   : post
title  : "Animation Observer - Part Two"
date   : 2022-02-03T09:17:35+07:00
slug   : animation-observer-02
categories: [frontend]
tags      : [bulma, css, javascript]
keywords  : [animate, intersection observer]
author : epsi
opengraph:
  image: assets/posts/frontend/2022/02-observer/01-preview-wide.png

toc    : "toc-2022-02-animation"

excerpt:
  Trigger animation on scroll using intersection observer.

---

### Preface

> Goal: Trigger animation on scroll using intersection observer.

The MDN explanation is good.
But when I need a real life example I search.

#### Itzami Example

After duckduckwent for a while,
I found this simple, but good example.

* [Boost your CSS animations with Intersection Observer API][itzami-boost]

And that's it!

-- -- --

### 1: Specific Observer

So how to bind the observer script to animation style class?
We can modify the example above and apply to our simple need.

* [gitlab.com/.../72-observe.html][tutor-07-72-html].

#### HTML Head

First add these two stylesheets in the HTML head.

* [gitlab.com/.../css/animate.css][tutor-07-animate-css].
* [gitlab.com/.../css/hover.css][tutor-07-hover-css].

These stylesheets is the full version,
from the original repository.

{{< highlight html >}}
  <link rel="stylesheet" type="text/css" href="css/hover.css">
  <link rel="stylesheet" type="text/css" href="css/animate.css">
{{< / highlight >}}

Then add adapted version of the example script.

* [gitlab.com/.../js-72/observer-bounce.js][tutor-07-bounce-js].

{{< highlight html >}}
  <script src="js-72/observer-bounce.js"></script>
{{< / highlight >}}

#### Javascript Skeleton

I'm using native javascript.

{{< highlight javascript >}}
document.addEventListener(
  "DOMContentLoaded", function(event) { 

  let bounceObserver =
    new IntersectionObserver( (entries) => {...});

  const bouncesToObserve = document
    .querySelectorAll(".animate__observe__bounce");

  bouncesToObserve.forEach((element) => {
    bounceObserver.observe(element);
  });
});
{{< / highlight >}}

#### HTML: Animation Class: No Observer

As usual, you can add zoom in animation, by adding class.
but this won't get replayed.

{{< highlight html >}}
<div class="animate__animated animate__rubberBand"
     id="sitedesc_image"></div>
{{< / highlight >}}

#### HTML: Animation Class: With Observer

The business card image, will be bounce.
Try scroll the page, up and down,
until the image shown after being hidden.

{{< highlight html >}}
      <div class="justify-content-center">
        <img src="images/one-page.png"
           class="animate__animated animate__bounce
                  animate__observe__bounce"
             alt="business card">
      </div>
{{< / highlight >}}

All I do is adding this `animate__observe__bounce` class.

#### Javascript: Bounce Observer

> But how does it works?

It is simply by adding and removing class.
Consider examine the detail:

{{< highlight javascript >}}
  let bounceObserver =
    new IntersectionObserver( (entries) => {
      entries.forEach((entry) => {
        const cl = entry.target.classList;
        if (entry.intersectionRatio > 0) {
          cl.add('animate__bounce');
        } else {
          cl.remove('animate__bounce');
        }
      });
  });
{{< / highlight >}}

#### The Issue

This approach has a few issue.
I should write this long procedure for each effect.
And the javascript become unbearable longer.

-- -- --

### 2: Generic Observer

However this can enhance the script above to make a generic one.

* [gitlab.com/.../73-observe.html][tutor-07-73-html].

#### HTML Head

Just change the javascript into our final form.

* [gitlab.com/.../js/animate-observer.js][tutor-07-observer-js].

{{< highlight html >}}
  <script src="js/animate-observer.js"></script>
{{< / highlight >}}

#### What effect?

First to do is to write down all selected effect in an array.
Only what I might need, and exclude the rest.
This means you can write down your own effect.

{{< highlight javascript >}}
  const effects = [
    'bounce', 'rubberBand', 'wobble', 'swing',
    'rollIn', 'zoomIn', 'flash', 'flip', 'pulse',
    'slideInLeft', 'slideInRight', 'headShake'
  ];
{{< / highlight >}}

#### Javascript Skeleton

I'm still using native javascript, as usual.

{{< highlight javascript >}}
document.addEventListener(
  "DOMContentLoaded", function(event) { 

  // only what I might need, and exclude the rest.
  const effects = [...];

  let elementObserver =
    new IntersectionObserver( (entries) => {...});

  const elementsToObserve = document
    .querySelectorAll(".animate__observe");

  elementsToObserve.forEach((element) => {
    elementObserver.observe(element);
  });
});
{{< / highlight >}}

#### HTML: Animation Data

Instead of class, I store the animation name in dataset.
So we can have different effect freely.

For example, this use `rubberBand` animation.

{{< highlight html >}}
<div class="animate__animated animate__observe"
     data-animate="rubberBand"
     id="sitedesc_image"></div>
{{< / highlight >}}

And this one use `bounce` animation.

{{< highlight html >}}
  <div class="justify-content-center">
    <img src="images/one-page.png"
       class="animate__animated animate__observe"
data-animate="bounce"
         alt="business card">
  </div>
{{< / highlight >}}

Note that I still need to add this `animate__observe` class.

#### Javascript: Element Observer

> But how does it works?

It is simply by adding and removing class,
based on the stored dataset.

* [gitlab.com/.../js/animate-observer.js][tutor-07-observer-js].

Consider examine the detail:

{{< highlight javascript >}}
  let elementObserver =
    new IntersectionObserver( (entries) => {
      entries.forEach((entry) => {
        const el = entry.target;
        const animate = el.dataset.animate;

        if (effects.includes(animate)) {
          const effect = 'animate__' + animate;

          if (entry.intersectionRatio > 0) {
            el.classList.add(effect);
          } else {
            el.classList.remove(effect);
          }
        }
      });
  });
{{< / highlight >}}

#### Preview

Enjoy the result

{{< embed-video width="480" height="290"
    src="assets/posts/frontend/2022/02-observer/14-working.mp4" >}}

-- -- --

### 3: Animating Widget

> Apply to other page as well.

One of my favorite is animating the whole blog page.
A blog page consist of the content and the widget.

* [gitlab.com/.../74-widget.html][tutor-07-74-html].

I usually use `zoomIn` effect,
but for this tutorial purpose,
I will use slide-in effect.

1. slideInLeft for blog content.
2. slideInRight for widget.
3. Addtional hover effect.

#### HTML Head

No changed.

#### HTML: Animate: Blog Content

The whole `main` tag.

{{< highlight html >}}
    <main role="main"
          class="column is-two-thirds
                 animate__animated animate__observe"
          data-animate="slideInLeft">
      ...
    </main>
{{< / highlight >}}

#### HTML: Animate: Widget

For each section inside `aside` tag.

{{< highlight html >}}
    <aside class="column is-one-thirds">

      <section class="aside-wrapper green
                      animate__animated animate__observe"
        data-animate="slideInRight">
        ...
      </section>

      <section class="aside-wrapper deep-orange
                      animate__animated animate__observe"
        data-animate="slideInRight">
        ...
      </section>

    </aside>
{{< / highlight >}}

#### HTML: Hover: Link

> hvr-underline-from-left

No need any observer.

{{< highlight html >}}
<div class="widget-body">
  <ul class="widget-list">
    <li><a href="http://epsi-rns.github.io/"
          class="hvr-underline-from-left"
         >Linux/BSD Desktop Customization</a></li>
    <li><a href="http://epsi-rns.gitlab.io/"
          class="hvr-underline-from-left"
         >Web Development Blog</a></li>
    <li><a href="http://oto-spies.info/"
          class="hvr-underline-from-left"
         >Car Painting and Body Repair.</a></li>
  </ul>
</div>
{{< / highlight >}}

#### HTML: Hover: Tag

> hvr-buzz

No need any observer.

{{< highlight html >}}
<div class="widget-body">

  <a class="tag is-small is-dark hvr-buzz
            deep-orange z-depth-1 hoverable p-b-5"
      href="https://epsi-rns.gitlab.io/categories/backend"
    >backend&nbsp;<span class="fa fa-folder"></span>
  </a>&nbsp;
  <a ...>...</a>&nbsp;
  <a ...>...</a>&nbsp;
  <a ...>...</a>&nbsp;
  <a ...>...</a>&nbsp;
  <a ...>...</a>&nbsp;
</div>
{{< / highlight >}}

#### Preview

Enjoy the result

{{< embed-video width="480" height="260"
    src="assets/posts/frontend/2022/02-observer/16-working.mp4" >}}

-- -- --

### What is Next ?

How about other example?
Of course I have example in my mind, that I can share.

Consider continue reading [ [Animation Observer - Part Three][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2022/02/05/animation-observer-03/

[itzami-boost]: https://www.itzami.com/blog/boost-your-css-animations-with-intersection-observer-api

[tutor-07-72-html]:     {{< tutor-html-bulma-md >}}/step-07/72-observe.html
[tutor-07-73-html]:     {{< tutor-html-bulma-md >}}/step-07/73-observe.html
[tutor-07-74-html]:     {{< tutor-html-bulma-md >}}/step-07/74-widget.html

[tutor-07-animate-css]: {{< tutor-html-bulma-md >}}/step-07/css/animate.css
[tutor-07-hover-css]:   {{< tutor-html-bulma-md >}}/step-07/css/hover.css
[tutor-07-bounce-js]:   {{< tutor-html-bulma-md >}}/step-07/js-72/observer-bounce.js
[tutor-07-observer-js]: {{< tutor-html-bulma-md >}}/step-07/js/animate-observer.js
