---
type   : post
title  : "Animation Observer - Part Three"
date   : 2022-02-05T09:17:35+07:00
slug   : animation-observer-03
categories: [frontend]
tags      : [bulma, css, javascript]
keywords  : [animate, intersection observer]
author : epsi
opengraph:
  image: assets/posts/frontend/2022/02-observer/01-preview-wide.png

toc    : "toc-2022-02-animation"

excerpt:
  Trigger animation on scroll using intersection observer.

---

### Preface

> Goal: Trigger animation on scroll using intersection observer.

Instead of using ready made animation,
we can be brave enough to make our own custom animation.
Such as this two cases.

1. Javascript only, no stylesheet.
2. Animation in stylesheet.

-- -- --

### 1: Numerator

> Javascript only, no stylesheet.

#### Original Source

I have seen jquery numerator,
and I want the native one.
Luckily I found this one.

* [Animated Number Counter with Vanilla JavaScript][codepen]

I use this source code,
with a simple modification,
to suit my need.

#### HTML Head

Add this javascript in the HTML head.

* [gitlab.com/.../js-75/numerator.js][tutor-07-numerator-js].

{{< highlight html >}}
  <script src="js-75/numerator.js"></script>
{{< / highlight >}}

#### HTML: Bulma: Level

I'm using layout level from Bulma.

* [Bulma - Layout - Level][bulma-level]

With additional dataset for counter number,
that could be read from javascript.

* [gitlab.com/.../75-numerator.html][tutor-07-75-html].

{{< highlight html >}}

<nav class="level">
  <div class="level-item has-text-centered">
    <div>
      <p class="heading">Code</p>
      <p class="title numerator"
         data-number="8"
        >0</p>
    </div>
  </div>
  <div ...>
  <div ...>
  <div ...>
  <div ...>
</nav>
{{< / highlight >}}

#### Javascript Skeleton

Consider adapt the javascript above:

* [gitlab.com/.../js-75/numerator.js][tutor-07-numerator-js].

{{< highlight javascript >}}
document.addEventListener(
  "DOMContentLoaded", function(event) { 

  const ...

  let counterObserver = 
    new IntersectionObserver( (entries) => {
      entries.forEach((entry) => {
        if (entry.intersectionRatio > 0) {
          const ...
          const animate = () => {...}
          animate();
        }
    });
  });

  countersToObserve.forEach((element) => {
    counterObserver.observe(element);
  });

});
{{< / highlight >}}

I know this is a long script.

#### Javascript: Variable Initialization

{{< highlight javascript >}}
  const countersToObserve = document
    .querySelectorAll('.numerator');
  const stepfactor = 20;
  const timeout    = Math.ceil(1000/stepfactor);
{{< / highlight >}}

#### Javascript: Scroll Event

If the element is happened to be displayed,
we animate from zero to counter number.

{{< highlight javascript >}}
if (entry.intersectionRatio > 0) {
  const counter = entry.target;
  const maxnum  = +counter.dataset.number;

  // reset on scroll
  counter.innerText = 0;

  const animate   = () => {...}

  animate();
}
{{< / highlight >}}

#### Javascript: Animate Counter

And finally the numerator animation itself

{{< highlight javascript >}}
  const animate   = () => {
    const current = +counter.innerText;
    const append  = (maxnum-current) / stepfactor;

    if (current < maxnum) {
      newcurrent = Math.ceil(current + append);
      counter.innerText = newcurrent;
      setTimeout(animate, timeout);
    } else {
      counter.innerText = maxnum;
    }
  }
{{< / highlight >}}

It is home made.
And I open for better enhancement.

#### Preview

Enjoy the result.

{{< embed-video width="480" height="162"
    src="assets/posts/frontend/2022/02-observer/17-working.mp4" >}}

-- -- --

### 2: Progressbar

> Animation in stylesheet.

#### HTML Head

Add both, stylesheet and javascript in the HTML head.

* [gitlab.com/.../js-76/progressbar.js][tutor-07-progress-js].
* [gitlab.com/.../css-76/progressbar.css][tutor-07-progress-css].

{{< highlight html >}}
  <link rel="stylesheet" type="text/css" href="css-76/progressbar.css">
  <script src="js-76/progressbar.js"></script>
{{< / highlight >}}

#### HTML: Custom Progress Bar

> Franken Component

I don't feel like bulma progressbar suit my need,
so I grab other html code from w3school,
but with modification.
And I also grab the stylesheet,
from somewhere in the internet.
But I wrote the javascript myself.
I know it is rather a frankenstein,
but it works, at least for me.

* [gitlab.com/.../76-progressbar.html][tutor-07-76-html].

{{< highlight html >}}
  <div id="container__bar"
       class="has-text-left"
       data-count="114">

    <div class="my__progressbar"
       data-count="8">
      <div class="my__progressbar_text"
        >Code: <span class="my__progressbar_perc"
          ></span></div>
      <div class="my__inner_bar"></div>
    </div>

    <div ...>

    <div ...>

    <div ...>

    <div ...>

  </div>
{{< / highlight >}}

Note that I also set the total count.
Sum of all element, on the first place.

#### Stylesheet: Progress Bar Animation

The keyframes is very simple.

{{< highlight css >}}
@keyframes my__progress__frames {
  from { width: 0%; }
  to   { width: 100%; }
}
{{< / highlight >}}

I'm using material color gradient for background.

{{< highlight css >}}
.my__inner_bar {
  background-color: #BBDEFB;
  background-image: linear-gradient(
        45deg, #2196F3 10px, 
        transparent 20px, transparent 25%, 
        #64B5F6 50%, #90CAF9 75%,
        transparent 75%, transparent);
}
{{< / highlight >}}

The animation is

{{< highlight css >}}
.my__inner_bar {
  animation-duration: 5s;
  animation-fill-mode: both;
  animation-name: my__progress__frames;
  animation-iteration-count: 1;
  animation-timing-function: ease;
  animation-direction: normal;
}
{{< / highlight >}}

And additional cosmetics.

{{< highlight css >}}
.my__progressbar_text {
  white-space: nowrap;
}

.my__inner_bar {
  height: 10px;
  border-radius: 10px;
}
{{< / highlight >}}

#### Javascript Skeleton

We need to make custom javascript:

* [gitlab.com/.../js-75/numerator.js][tutor-07-numerator-js].

{{< highlight javascript >}}
document.addEventListener(
  "DOMContentLoaded", function(event) { 

  ...

  let containerBarObserver = 
    new IntersectionObserver( (entries) => {
      entries.forEach((entry) => {
        ...
    });
  });

  containerBarObserver.observe(containerBar);
});
{{< / highlight >}}

I know this is script is longer.

#### Javascript: Variable Initialization

{{< highlight javascript >}}
  const progressbars = document
    .querySelectorAll(".my__progressbar");
  const myBars       = document
    .querySelectorAll(".my__inner_bar");
  const containerBar = document
    .getElementById("container__bar");
  const total = containerBar.dataset.count;
{{< / highlight >}}

#### Javascript: Scroll Event

If the element is happened to be displayed,
we reanimate each progressbar.
The point is just toggling,
the presence of each `my__inner_bar` class,
as the page scrolled.

{{< highlight javascript >}}
new IntersectionObserver( (entries) => {
  entries.forEach((entry) => {
    if (entry.intersectionRatio > 0) {
      // reset on scroll
      myBars.forEach((element) => {
        element.classList.add('my__inner_bar');
      });

      progressbars.forEach((element) => {...});
    } else {
      myBars.forEach((element) => {
        element.classList.remove('my__inner_bar');
      });
    }
});
{{< / highlight >}}

#### Javascript: Animate Counter

And finally the numerator animation itself.

{{< highlight javascript >}}
progressbars.forEach((element) => {
  const count = element.dataset.count;
  const width = Math.round((count/total)*100);
  element.style.width = width + "%";

  const percText = element
   .getElementsByClassName("my__progressbar_perc")[0]
   percText.innerText = width + "%";
});
{{< / highlight >}}

Again, it is home made.
And I open for better enhancement.


#### Preview

Enjoy the result.

{{< embed-video width="480" height="334"
    src="assets/posts/frontend/2022/02-observer/18-working.mp4" >}}

-- -- --

### Conclusion

> What do you think?

Thank you for reading.

Farewell.
We shall meet again.

[//]: <> ( -- -- -- links below -- -- -- )

[codepen]:      https://codepen.io/akhijannat/pen/JjYQgNK
[bulma-level]:  https://bulma.io/documentation/layout/level/

[tutor-07-75-html]:     {{< tutor-html-bulma-md >}}/step-07/75-numerator.html
[tutor-07-76-html]:     {{< tutor-html-bulma-md >}}/step-07/76-progressbar.html

[tutor-07-numerator-js]:{{< tutor-html-bulma-md >}}/step-07/js-75/numerator.js
[tutor-07-progress-js]: {{< tutor-html-bulma-md >}}/step-07/js-76/progressbar.js
[tutor-07-progress-css]:{{< tutor-html-bulma-md >}}/step-07/js-76/progressbar.css
