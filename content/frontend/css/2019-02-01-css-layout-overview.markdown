---
type   : post
title  : "CSS Layout - Overview"
date   : 2019-02-01T09:17:35+07:00
slug   : css-layout-overview
categories: [frontend]
tags      : [css]
keywords  : [plain html, layout, overview, summary]
author : epsi

toc    : "toc-2019-02-css-layout"

excerpt:
  CSS layout with pure CSS has evolved from time to time.

---

### Preface

In case you haven't notice,
CSS provide many method to create page layout.

> Goal: Create Basic Layout with Pure CSS.

#### Without CSS Framework?

The last time I design layout with pure CSS might be in year of 2010.
After that I use Bootstrap CSS Framework, and this year I step to Bulma.
Time has passes by, now we have HTML5 semantic tags.
As we have CSS3, it has been a pleasure,
to see how the CSS standard has become.
But I never really have time to learn this pure CSS.

Until now!

#### Method Provided

CSS layout with pure CSS has changed from time to time.
Here I represent four example layouts.
From the latest and easiest to the ancient one.

* Flex

* Grid

* Float with Calc

* Fake Table

Of course there are other method as I can recall.
This include float with positioning.
But I do not think that I have time to ressurect my old works.

> I'm mostly posting codes so I won't have any problems finding it in the future.

#### Table of Content

The table content is available as header on each tutorial.
There, I present a Hugo Tutorial, step by step, for beginners.

#### Source Code

You can download the source code of this article series here.

* [example-css-layout-method.tar][tar-layout-source]

-- -- --

### 1: Prepare Document and Stylesheet

Before dive into each CSS method, 
consider this simple `hello world` page.

> First thing first.

Each example has three equal steps

* 1: Prepare Document and Stylesheet

* 2: Equal Column Height

* 3: Page Layout with Sticky Footer

The first step provided here, as a basic required knowledge,
before you continue step into the rest (the other two steps).

#### Document

{{< highlight html >}}
<html>
<head>
  <title>Prepare</title>
  <link rel="stylesheet" type="text/css" href="01-common.css">
</head>
<body>
  <div class="container">

    <p>Hello world!</p>

    <p>lorem ipsum<br/>lorem ipsum<br/>lorem ipsum<br/>
    lorem ipsum<br/>lorem ipsum<br/>lorem ipsum</p>

  </div>
</body>
</html>
{{< / highlight >}}

#### Stylesheet

Nothing fancy here.
Just reset, and colors.

{{< highlight css >}}
body, html {
  height: 100%;
  padding: 0;
  margin: 0;

  background-color: white;
}

.container {
  background-color: white;
  border: 2px dashed #0d47a1;

  padding-left: 1rem;
  padding-right: 1rem;
}
{{< / highlight >}}

#### Browser Preview

Consider have a look at the result in your favorite browser

![CSS: Simple Page][image-css-01]

Pretty easy right?

-- -- --

### Begin The Guidance

Let's get the method example started.

We are going to use `flex` layout,
before reader experience the difference, with other layout.
Consider continue reading [ [CSS Layout - Flex][local-whats-next] ].

And yeah...
Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2019/02/03/css-layout-flex/
[tar-layout-source]:    {{< assets-frontend >}}/2019/02/example-css-layout-method.tar
[image-css-01]:         {{< assets-frontend >}}/2019/02/layout-01-prepare.png

