---
type   : post
title  : "CSS Layout - Flex"
date   : 2019-02-03T09:17:35+07:00
slug   : css-layout-flex
categories: [frontend]
tags      : [css]
keywords  : [plain html, layout, flex]
author : epsi

toc    : "toc-2019-02-css-layout"

excerpt:
  CSS Flex Layout give you flexibility
  to make basic layout with pure CSS for modern browser.

---

### Preface

> Goal: Create Basic Layout with Flex Method.

#### Table of Content

* Preface: Table of Content

* 1: Prepare Document and Stylesheet

* 2: Equal Column Height

* 3: Page Layout with Sticky Footer

* Conclusion

#### Reading

To understand the concept of `flex`,
this article might interest you.

* [css-tricks.com/snippets/css/a-guide-to-flexbox/][css-tricks-flexbox]

#### Source Code

You can download the source code of this article series here.

* [example-css-layout-method.tar][tar-layout-source]

-- -- --

### 1: Prepare Document and Stylesheet

This basic required kowledge has been provided in previus article.
Please read [CSS Layout - Overview][local-overview] before continue.

-- -- --

### 2: Equal Column Height

Now our very first Flex,
how do I define two columns with equal height?

#### Document

{{< highlight html >}}
<html>
<head>
  <title>Two Columns Flex Example</title>
  <link rel="stylesheet" type="text/css" href="02-flex.css">
</head>
<body>
  <div class="container">

    <aside id="leftside">
      <p>Side</p>
    </aside>

    <main id="mainside">
      <p>Hello world!</p>

      <p>lorem ipsum<br/>lorem ipsum<br/>lorem ipsum<br/>
      lorem ipsum<br/>lorem ipsum<br/>lorem ipsum</p>
    </main>

  </div>
</body>
</html>
{{< / highlight >}}

#### Stylesheet

It turned out that this is very simple with `flex`.

{{< highlight css >}}
body, html {
  height: 100%;
  padding: 0;
  margin: 0;

  background-color: white;
}

.container {
  display: flex;
  min-height: 100%;
}

#leftside {
  flex: 25%;

  background-color: #2196f3;
  border: 2px dashed #0d47a1;

  margin-right: 5%;

  padding-left: 1rem;
  padding-right: 1rem;
}

#mainside {
  flex: 70%;
  
  background-color: #1976d2;
  border: 2px dashed #0d47a1;

  padding-left: 1rem;
  padding-right: 1rem;
}
{{< / highlight >}}

#### Browser Preview

Consider have a look at the result in your favorite browser

![CSS: Equal Column Height][image-css-02]

#### How does It Works ?

Just this CSS to make it happened.
All summed to be a hundred percent width.

{{< highlight css >}}
.container {
  display: flex;
}

#leftside {
  flex: 25%;
  margin-right: 5%;
}

#mainside {
  flex: 70%;
}
{{< / highlight >}}

I also add this below for comfort.

{{< highlight css >}}
.container {
  min-height: 100%;
}
{{< / highlight >}}

-- -- --

### 3: Page Layout with Sticky Footer

And now page layout.
How do I make sticky footer inside element?

#### Document

{{< highlight html >}}
<html>
<head>
  <title>Page Layout Flex Example</title>
  <link rel="stylesheet" type="text/css" href="03-flex.css">
</head>
<body>
  <div class="container">

    <aside id="leftside">
      <p>Side</p>
    </aside>

    <main id="mainside">
      <header id="top">
        <p>Top</p>
      </header>

      <article id="content">
        <h1>Content</h1>

        <p>Hello world!</p>

        <p>lorem ipsum<br/>lorem ipsum<br/>lorem ipsum<br/>
        lorem ipsum<br/>lorem ipsum<br/>lorem ipsum</p>
      </article>

      <footer id="bottom">
        <p>Bottom</p>
      </footer>
    </main>

  </div>
</body>
</html>
{{< / highlight >}}

#### Stylesheet

It also easy with `flex`.

{{< highlight css >}}
body, html {
  height: 100%;
  padding: 0;
  margin: 0;

  background-color: white;
}

.container {
  display: flex;
  min-height: 100%;
}

#leftside {
  flex: 25%;

  background-color: #2196f3;
  border: 2px dashed #0d47a1;
  
  margin-right: 5%;
  
  padding-left: 1rem;
  padding-right: 1rem;
}

#mainside {
  flex: 70%;
  
  display: flex;
  flex-direction: column;
  
  background-color: #1976d2;
  border: 2px dashed #0d47a1;
  
  padding-left: 1rem;
  padding-right: 1rem;
}

#top, #bottom, #content {
  text-align: center;
  border: 2px dotted #0d47a1;
}

#top {
  height: 3rem;

  background-color: #64b5f6;
}

#content {
  margin-top: 2rem;
  margin-bottom: 2rem;
  
  flex: 1 0 auto;
  
  background-color: #bbdefb;
}

#bottom {
  height: 3rem;
  
  flex-shrink: 2;

  background-color: #64b5f6;
}
{{< / highlight >}}

#### Browser Preview

Consider have a look at the result in your favorite browser

![CSS: Page Layout][image-css-03]

#### How does It Works ?

Just this CSS arrangement.

{{< highlight css >}}
#mainside {
  display: flex;
  flex-direction: column;
}

#content {
  flex: 1 0 auto;
}
{{< / highlight >}}

Optionally, to prevent the element from shrinking.

{{< highlight css >}}
#bottom {
  flex-shrink: 0;
}
{{< / highlight >}}

-- -- --

### Conclusion

Flex layout can mindblowingy be achieved without great effort.

We are going to use other layout, so reader can experience the difference.
Consider continue reading [ [CSS Layout - Grid][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2019/02/05/css-layout-grid/
[local-overview]:       {{< baseurl >}}frontend/2019/02/01/css-layout-overview/
[tar-layout-source]:    {{< assets-frontend >}}/2019/02/example-css-layout-method.tar

[css-tricks-flexbox]:   https://css-tricks.com/snippets/css/a-guide-to-flexbox/

[image-css-02]:     {{< assets-frontend >}}/2019/02/layout-02-flex-two-colums.png
[image-css-03]:     {{< assets-frontend >}}/2019/02/layout-03-flex-page.png
