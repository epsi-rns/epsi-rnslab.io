---
type   : post
title  : "CSS Layout - Grid System"
date   : 2019-02-05T09:17:35+07:00
slug   : css-layout-grid
categories: [frontend]
tags      : [css]
keywords  : [plain html, layout, grid]
author : epsi

toc    : "toc-2019-02-css-layout"

excerpt:
  CSS Grid Layout had been a standardized method
  to make basic layout with pure CSS for modern browser.

---

### Preface

> Goal: Create Basic Layout with Grid Method.

#### Table of Content

* Preface: Table of Content

* 1: Prepare Document and Stylesheet

* 2: Equal Column Height

* 3: Page Layout with Sticky Footer

* Conclusion

#### Reading

To understand the concept of `grid system`,
this article might interest you.

* [css-tricks.com/snippets/css/complete-guide-grid/][css-tricks-grid]

#### Source Code

You can download the source code of this article series here.

* [example-css-layout-method.tar][tar-layout-source]

-- -- --

### 1: Prepare Document and Stylesheet

This basic required kowledge has been provided in previus article.
Please read [CSS Layout - Overview][local-overview] before continue.

-- -- --

### 2: Equal Column Height

Now our very first Grid System,
how do I define two columns with equal height?

#### Document

{{< highlight html >}}
<html>
<head>
  <title>Two Columns Grid Example</title>
  <link rel="stylesheet" type="text/css" href="02-grid.css">
</head>
<body>
  <div class="container">

    <aside id="leftside">
      <p>Side</p>
    </aside>

    <main id="mainside">
      <p>Hello world!</p>

      <p>lorem ipsum<br/>lorem ipsum<br/>lorem ipsum<br/>
      lorem ipsum<br/>lorem ipsum<br/>lorem ipsum</p>
    </main>

  </div>
</body>
</html>
{{< / highlight >}}

#### Stylesheet

We can define the layout with this `grid system` arrangement.

{{< highlight css >}}
body, html {
  height: 100%;
  padding: 0;
  margin: 0;

  background-color: white;
}

.container {
  display: grid;
  grid-template-areas: 'leftside mainside';
  grid-template-columns: 25% 70%;
  grid-gap: 5%;

  min-height: 100%;
}

#leftside {
  grid-area: leftside;

  background-color: #f44336;
  border: 2px dashed #b71c1c;

  padding-left: 1rem;
  padding-right: 1rem;
}

#mainside {
  grid-area: mainside;

  background-color: #d32f2f;
  border: 2px dashed #b71c1c;

  padding-left: 1rem;
  padding-right: 1rem;
}
{{< / highlight >}}

#### Browser Preview

Consider have a look at the result in your favorite browser

![CSS: Equal Column Height][image-css-02]

#### How does It Works ?

Just this CSS to make it happened.
All summed to be a hundred percent width.

{{< highlight css >}}
.container {
  display: grid;
  grid-template-areas: 'leftside mainside';
  grid-template-columns: 25% 70%;
  grid-gap: 5%;
}

#leftside {
  grid-area: leftside;
}

#mainside {
  grid-area: mainside;
}
{{< / highlight >}}

I also add this below for comfort.

{{< highlight css >}}
.container {
  min-height: 100%;
}
{{< / highlight >}}

-- -- --

### 3: Page Layout with Sticky Footer

And now page layout.
How do I make sticky footer inside element?

#### Document

{{< highlight html >}}
<html>
<head>
  <title>Page Layout Grid Example</title>
  <link rel="stylesheet" type="text/css" href="03-grid.css">
</head>
<body>
  <div class="container">

    <aside id="leftside">
      <p>Side</p>
    </aside>

    <header id="top">
      <p>Top</p>
    </header>

    <article id="content">
      <h1>Content</h1>

      <p>Hello world!</p>

      <p>lorem ipsum<br/>lorem ipsum<br/>lorem ipsum<br/>
      lorem ipsum<br/>lorem ipsum<br/>lorem ipsum</p>
    </article>

    <footer  id="bottom">
      <p>Bottom</p>
    </footer>

  </div>
</body>
</html>
{{< / highlight >}}

#### Stylesheet

Now it become a little bit complex.

{{< highlight css >}}
body, html {
  height: 100%;
  padding: 0;
  margin: 0;

  background-color: white;
}

.container {
  display: grid;
  grid-template-areas: 
    'leftside top'
    'leftside content'
    'leftside bottom';
  grid-template-columns: 25% 65%;
  grid-template-rows: 3rem calc(auto-8px) 3rem;
  grid-column-gap: 5%;
  grid-row-gap: 2rem;

  min-height: 100%;
}

#leftside {
  grid-area: leftside;

  background-color: #f44336;
  border: 2px dashed #b71c1c;
  
  padding-left: 1rem;
  padding-right: 1rem;
}


#top, #bottom, #content {
  text-align: center;
  border: 2px dotted #b71c1c;
}

#top {
  grid-area: top;
  background-color: #e57373;
}

#content {
  grid-area: content; 
  background-color: #ffcdd2;
}

#bottom {
  grid-area: bottom;  
  background-color: #e57373;
}
{{< / highlight >}}

#### Browser Preview

Consider have a look at the result in your favorite browser

![CSS: Page Layout][image-css-03]

#### How does It Works ?

Just this CSS arrangement.

{{< highlight css >}}
.container {
  display: grid;
  grid-template-areas: 
    'leftside top'
    'leftside content'
    'leftside bottom';
  grid-template-columns: 25% 65%;
  grid-template-rows: 3rem calc(auto-8px) 3rem;
  grid-column-gap: 5%;
  grid-row-gap: 2rem;

  min-height: 100%;
}

#top {
  grid-area: top;
}

#content {
  grid-area: content; 
}

#bottom {
  grid-area: bottom;  
}
{{< / highlight >}}

-- -- --

### Conclusion

Grid system is magnificient, for a rigid layout requirement.

We are going to use other layout, so reader can experience the difference.
Consider continue reading [ [CSS Layout - Float with Calc][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2019/02/07/css-layout-calc/
[local-overview]:       {{< baseurl >}}frontend/2019/02/01/css-layout-overview/
[tar-layout-source]:    {{< assets-frontend >}}/2019/02/example-css-layout-method.tar

[css-tricks-grid]:  https://css-tricks.com/snippets/css/complete-guide-grid/

[image-css-02]:     {{< assets-frontend >}}/2019/02/layout-02-grid-two-colums.png
[image-css-03]:     {{< assets-frontend >}}/2019/02/layout-03-grid-page.png
