---
type   : post
title  : "CSS Layout - Float with Calc"
date   : 2019-02-07T09:17:35+07:00
slug   : css-layout-calc
categories: [frontend]
tags      : [css]
keywords  : [plain html, layout, float with calc]
author : epsi

toc    : "toc-2019-02-css-layout"

excerpt:
  CSS Float for Layout is belong to the past,
  however this calc method is still considerably good
  to make basic layout with pure CSS.

---

### Preface

CSS Float for Layout is belong to the past,
however this calc method is still considerably good
to make basic layout with pure CSS.

> Goal: Create Basic Layout with Float Method with the help of CSS3's Calc.

I found this cool `calc` method in stackoverflow,
but has never use it in my life.
In this era, I was away from website stuff,
and doing other business, not related with computer.

#### Table of Content

* Preface: Table of Content

* 1: Prepare Document and Stylesheet

* 2: Equal Column Height

* 3: Page Layout with Sticky Footer

* Conclusion

#### Source Code

You can download the source code of this article series here.

* [example-css-layout-method.tar][tar-layout-source]

-- -- --

### 1: Prepare Document and Stylesheet

This basic required kowledge has been provided in previus article.
Please read [CSS Layout - Overview][local-overview] before continue.

-- -- --

### 2: Equal Column Height

Now our very first Grid System,
how do I define two columns with equal height?

#### Document

{{< highlight html >}}
<html>
<head>
  <title>Two Columns Calc Example</title>
  <link rel="stylesheet" type="text/css" href="02-calc.css">
</head>
<body>
  <div class="container">

    <aside id="leftside">
      <p>Side</p>
    </aside>

    <main id="mainside">
      <p>Hello world!</p>

      <p>lorem ipsum<br/>lorem ipsum<br/>lorem ipsum<br/>
      lorem ipsum<br/>lorem ipsum<br/>lorem ipsum</p>
    </main>

  </div>
</body>
</html>
{{< / highlight >}}

#### Stylesheet

It takes some calculation, this can be provided with `calc`.
`calc` itself is belong to **CSS3** specification.

{{< highlight css >}}
body, html {
  height: 100%;
  padding: 0;
  margin: 0;

  background-color: white;
}

.container {
  width: 100%;
  min-height: 100%;
  
  overflow: hidden;
}

#leftside {
  float: left;
  width: 25%;
  
  background-color: #4caf50;
  border: 2px dashed #1b5e20;

  margin-right: calc(5%-8px);
  
  padding-bottom: 500em;
  margin-bottom: -500em;
}

#mainside {
  float: right;
  width: 70%;
  
  background-color: #388e3c;
  border: 2px dashed #1b5e20;
  
  padding-bottom: 500em;
  margin-bottom: -500em;
}

#leftside p,
#mainside p {
  padding-left: 1rem;
  padding-right: 1rem;
}
{{< / highlight >}}

#### Browser Preview

Consider have a look at the result in your favorite browser

![CSS: Equal Column Height][image-css-02]

#### How does It Works ?

Just this CSS to make it happened.
All summed to be a hundred percent width.

{{< highlight css >}}
#leftside {
  float: left;
  width: 25%;

  margin-right: calc(5%-8px);

#mainside {
  float: right;
  width: 70%;
}
{{< / highlight >}}

Notice that border width matters,
so we have still have to substract the equation.

* 2*2px*2 (leftside, mainside) 

How about equal height?
This require a little CSS manipulation.

{{< highlight css >}}
#leftside {
  padding-bottom: 500em;
  margin-bottom: -500em;
}

#mainside {
  padding-bottom: 500em;
  margin-bottom: -500em;
}
{{< / highlight >}}

-- -- --

### 3: Page Layout with Sticky Footer

And now page layout.
How do I make sticky footer inside element?

#### Document

{{< highlight html >}}
<html>
<head>
  <title>Page Layout Calc Example</title>
  <link rel="stylesheet" type="text/css" href="03-calc.css">
</head>
<body>
  <div class="container">

    <aside id="leftside">
      <p>Side</p>
    </aside>

    <main id="mainside">
      <header id="top">
        <p>Top</p>
      </header>

      <article id="content">
        <h1>Content</h1>

        <p>Hello world!</p>

        <p>lorem ipsum<br/>lorem ipsum<br/>lorem ipsum<br/>
        lorem ipsum<br/>lorem ipsum<br/>lorem ipsum</p>

      </article>

      <footer  id="bottom">
        <p>Bottom</p>
      </footer>
    </main>

  </div>
</body>
</html>
{{< / highlight >}}

#### Stylesheet

It also easy with flex.

{{< highlight css >}}
body, html {
  height: 100%;
  padding: 0;
  margin: 0;

  background-color: white;
}

.container {
  width: 100%;
  min-height: 100%;
  
  overflow: hidden;
}

.container:after {
  content: "";
  display: table;
  clear: both;
}

#leftside {
  float: left;
  width: 25%;
  
  background-color: #4caf50;
  border: 2px dashed #1b5e20;

  margin-right: calc(5%-8px);
  
  padding-bottom: 500em;
  margin-bottom: -500em;
}

#mainside {
  float: right;
  width: 70%;

  background-color: #388e3c;
  border: 2px dashed #1b5e20;
  
  padding-bottom: 500em;
  margin-bottom: -500em;
}

#leftside p,
#mainside p {
  padding-left: 1rem;
  padding-right: 1rem;
}

#top, #bottom, #content {
  text-align: center;
  border: 2px dotted #1b5e20;
}

#top {
  height: 3rem;

  background-color: #81c784;
}

#content {
  min-height: calc(100vh - 10rem - 16px);

  margin-top: 2rem;
  margin-bottom: 2rem;

  background-color: #c8e6c9;
}

#bottom {
  height: 3rem;

  background-color: #81c784;
  background-color: #81c784;
}
{{< / highlight >}}

#### Browser Preview

Consider have a look at the result in your favorite browser

![CSS: Page Layout][image-css-03]

#### How does It Works ?

Just this calculation by CSS3.

{{< highlight css >}}
#content {
  min-height: calc(100vh - 10rem - 16px);
}
{{< / highlight >}}

Notice that border width matters,
so we have still have to substract the equation.

* 2*2px*3 (top, content, bottom) 

* 2*2px (mainside)

-- -- --

### Conclusion

Float is still great, if you can calculate right.

We are going to use other layout, so reader can experience the difference.
Consider continue reading [ [CSS Layout - Fake Table][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2019/02/09/css-layout-table/
[local-overview]:       {{< baseurl >}}frontend/2019/02/01/css-layout-overview/
[tar-layout-source]:    {{< assets-frontend >}}/2019/02/example-css-layout-method.tar

[image-css-02]:     {{< assets-frontend >}}/2019/02/layout-02-calc-two-colums.png
[image-css-03]:     {{< assets-frontend >}}/2019/02/layout-03-calc-page.png
