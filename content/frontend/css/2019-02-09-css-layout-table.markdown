---
type   : post
title  : "CSS Layout - Fake Table"
date   : 2019-02-09T09:17:35+07:00
slug   : css-layout-fake-table
categories: [frontend]
tags      : [css]
keywords  : [plain html, layout, fake table]
author : epsi

toc    : "toc-2019-02-css-layout"

excerpt:
  CSS Fake Table Layout is not the right method
  to make basic layout with pure CSS for modern browser.

---

### Preface

> Goal: Create Basic Layout with Fake Table Method.

Table is the right method to create a CSS table.
But table is not the the right method to achieve a page layout.
We can manipulate element so that it can behave like table.
But some people say that table layout,
represent everything wrong with CSS layout.

However, we need to experience the situation.

#### Table of Content

* Preface: Table of Content

* 1: Prepare Document and Stylesheet

* 2: Equal Column Height

* 3: Page Layout with Sticky Footer

* Conclusion

#### Source Code

You can download the source code of this article series here.

* [example-css-layout-method.tar][tar-layout-source]

-- -- --

### 1: Prepare Document and Stylesheet

This basic required kowledge has been provided in previus article.
Please read [CSS Layout - Overview][local-overview] before continue.

-- -- --

### 2: Equal Column Height

Now our very first Grid System,
how do I define two columns with equal height?

#### Document

{{< highlight html >}}
<html>
<head>
  <title>Two Columns Fake Table Example</title>
  <link rel="stylesheet" type="text/css" href="02-table.css">
</head>
<body>
  <div class="container">

    <aside id="leftside">
      <p>Side</p>
    </aside>

    <main id="mainside">
      <p>Hello world!</p>

      <p>lorem ipsum<br/>lorem ipsum<br/>lorem ipsum<br/>
      lorem ipsum<br/>lorem ipsum<br/>lorem ipsum</p>
    </main>

  </div>
</body>
</html>
{{< / highlight >}}

#### Stylesheet

Consider make the element behave like `table`.

{{< highlight css >}}
body, html {
  height: 100%;
  padding: 0;
  margin: 0;

  background-color: white;
}

.container {
  display: table;
  width: 100%;
  min-height: 100%;

  border-spacing: 10px 0;
  margin-left:   -1rem; 
  margin-right:  -1rem; 
}

#leftside {
  width: 25%;

  display: table-cell;

  background-color: #00bcd4;
  border: 2px dashed #006064;
}

#mainside {
  width: 70%;
  
  display: table-cell;

  background-color: #0097a7;
  border: 2px dashed #006064;
}

#leftside p,
#mainside p {
  padding-left: 1rem;
  padding-right: 1rem;
}
{{< / highlight >}}

#### Browser Preview

Consider have a look at the result in your favorite browser

![CSS: Equal Column Height][image-css-02]

#### How does It Works ?

Just this CSS to make it happened.
All summed to be a hundred percent width.

{{< highlight css >}}
.container {
  display: table;
  width: 100%;
}

#leftside {
  width: 25%;
  display: table-cell;
}

#mainside {
  width: 70%;
  display: table-cell;
}
{{< / highlight >}}

How about gap between table?

{{< highlight css >}}
.container {
  display: table;

  border-spacing: 10px 0;
  margin-left:   -1rem; 
  margin-right:  -1rem; 
}
{{< / highlight >}}

It left, blank space on the right side.

-- -- --

### 3: Page Layout with Sticky Footer

And now page layout.
How do I make sticky footer inside element?

#### Document

{{< highlight html >}}
<html>
<head>
  <title>Page Layout Fake Table Example</title>
  <link rel="stylesheet" type="text/css" href="03-table.css">
</head>
<body>
  <div class="container">

    <aside id="leftside">
      <p>Side</p>
    </aside>

    <main id="mainside">
      <div id="innermain">
        <header id="top">
          <p>Top</p>
        </header>

        <article id="content">
          <h1>Content</h1>

          <p>Hello world!</p>

          <p>lorem ipsum<br/>lorem ipsum<br/>lorem ipsum<br/>
          lorem ipsum<br/>lorem ipsum<br/>lorem ipsum</p>

        </article>

        <footer  id="bottom">
          <p>Bottom</p>
        </footer>
      </div>
    </main>

  </div>
</body>
</html>
{{< / highlight >}}

#### Stylesheet

It also easy with flex.

{{< highlight css >}}
body, html {
  height: 100%;
  padding: 0;
  margin: 0;

  background-color: white;
}

.container {
  display: table;
  width: 100%;
  min-height: 100%;
  
  border-spacing: 1rem 0;
  margin-left:   -1rem; 
  margin-right:  -1rem; 
}

#leftside {
  width: 25%;
  
  display: table-cell;

  background-color: #00bcd4;
  border: 2px dashed #006064;
}

#mainside {
  width: 70%;
  
  display: table-cell;
  border-spacing: 0 1rem;
  
  background-color: #0097a7;
  border: 2px dashed #006064;
}

#innermain {
  display: table;
  width: 100%;

  margin-top:    -1rem; 
  margin-bottom: -1rem; 
}

#leftside p,
#mainside p {
  padding-left: 1rem;
  padding-right: 1rem;
}

#top, #bottom, #content {
  text-align: center;
  border: 2px dotted #006064;
}

#top {
  height: 3rem;
  display: table-row;
  background-color: #4dd0e1;
}

#content {
  display: table-row;  
  background-color: #b2ebf2;
}

#bottom {
  height: 3rem;  
  display: table-row;
  background-color: #4dd0e1;
}
{{< / highlight >}}

#### Browser Preview

Consider have a look at the result in your favorite browser

![CSS: Page Layout][image-css-03]

#### How does It Works ?

First we require inner element.

{{< highlight html >}}
    <main id="mainside">
      <div id="innermain">
        ...
      </div>
    </main>
{{< / highlight >}}

And this CSS arrangement.

{{< highlight css >}}
#innermain {
  display: table;

  margin-top:    -1rem; 
  margin-bottom: -1rem; 
}

#top {
  height: 3rem;
  display: table-row;
}

#content {
  display: table-row;  
}

#bottom {
  display: table-row;
}
{{< / highlight >}}

It also left, blank space on the bottom side.

-- -- --

### Conclusion

We can use `table` ro make a table.
But not for `layout`.

Thank you for reading.
Consider going back to [ [CSS Layout - Overview][local-whats-next] ].

Until next time.
Farewell.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2019/02/01/css-layout-overview/
[local-overview]:       {{< baseurl >}}frontend/2019/02/01/css-layout-overview/
[tar-layout-source]:    {{< assets-frontend >}}/2019/02/example-css-layout-method.tar

[css-tricks-grid]:  https://css-tricks.com/snippets/css/complete-guide-grid/

[image-css-02]:     {{< assets-frontend >}}/2019/02/layout-02-table-two-colums.png
[image-css-03]:     {{< assets-frontend >}}/2019/02/layout-03-table-page.png
