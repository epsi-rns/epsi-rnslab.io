---
type   : post
title  : "HTML - Overview"
date   : 2020-04-01T09:17:35+07:00
slug   : html-overview
categories: [frontend]
tags      : [css]
keywords  : [inline, internal, external]
author : epsi
opengraph:
  image: assets-frontend/2020/04/vim-html-basic.png

toc    : "toc-2020-04-html"

excerpt:
  A brief guidance at a glance for beginner.

---

### Preface

> Goal: A brief guidance at a glance for beginner.

After years of SSG, I have to answer a lot of basic question,
from beginner in web development community.
For what I learn so far, beginner need example codes.

#### Where to Learn

If you are beginner, you should read these first.

* [w3schools.com](https://w3schools.com)

* [roadmap.sh](http://roadmap.sh/)

My first attempt is,
always ask beginner to read [w3schools.com](https://w3schools.com),
and for not so beginner I ask them to
examine diagram in [roadmap.sh](http://roadmap.sh/).

#### What is A Web Page?

A web page is simply

1. Structure + Presentation + Behaviour

2. Custom User Content: Text and Image (or other media).

![What is a page?][concept-page]

The first one, is a stack we know as `html+css+js`.
The second one is any material we want to stuff to this document page.

#### The Needs

> How do I explain from basic stuff to modern era?

![HTML + CSS + JS][html-css-js]

After sometime, I feel the needs,
a series article for beginner who want to step into web development.
So that they can learn from the very basic stuff to the modern edge,
without having too much time spending time searching on the internet.
I finally decide to make my own article, so I can answer my own article.
Tell the story that I know so well, because I'm the one who wrote it.
Now the issue is, how would this article series be arranged.

#### The Gap

> No complete templating guidance

![Templating Engine and Preprocessor][preprocessor]

There are already so many material over there in internet,
but my friend see a gap, the `templating engine`.
This is where this article series fill the gaps.
With this article series,
beginner can build their own complete pages,
from scratch without any frameworks.

#### Step By Step.

> Beginner need examples!

I arrange this article series as below:

1. Fundamental Stuff: `html+css+js`

2. Tips: Tools (simple web server), Browser (inspect element)

3. Stack Examples: `pug+stylus+coffescript`.

4. Task Runner and Bundler: Grunt and Webpack

I use as pure basic stuff as possible,
and avoid CSS framework, backend and SSG.

![Supporting Technologies?][illustration-supporting]

#### On Radar

> No Idea

I'm still not sure about this.

5. SSR (Server Side Rendering): vue and svelte.

Time SSR is inevitable, but I still haven't got time to explore it yet.

#### Source Examples

You can obtain source examples here:

* [html-basic][source-example]

-- -- --

### Example HTML

If you are a beginner and never know what HTML code looks like,
you may examine this document below:

{{< highlight html >}}
<html>
<head>
  <title>Example HTML</title>>
</head>
<body>
  <h2>Relationship Alerts</h2>

  <p>
    <strong>Danger!</strong>:
    Red light indicates door a secured.
  </p>

</body>
</html>
{{< / highlight >}}

Save the code into a file, such as `alert.html` or something similar,
and open it in your browser.

![HTML: A very simple HTML example][html-example]

-- -- --

### What's Next?

Let's get it on with `stylesheet` and `javascript`.

Consider continue reading [ [HTML - Stylesheet][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}frontend/2020/04/02/html-stylesheet/

[source-example]:           {{< tutor-css-tools >}}/html-basic/

[html-example]:             {{< assets-frontend >}}/2020/04/00-html-example.png
[html-css-js]:              {{< assets-frontend >}}/2020/04/html-css-js.png
[preprocessor]:             {{< assets-frontend >}}/2020/04/preprocessor.png
[concept-page]:             {{< assets-frontend >}}/2020/04/css-concept-page.png
[illustration-supporting]:  {{< assets-frontend >}}/2020/04/concept-supporting-technologies.png
