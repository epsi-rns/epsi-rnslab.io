---
type   : post
title  : "HTML - Stylesheet"
date   : 2020-04-02T09:17:35+07:00
slug   : html-stylesheet
categories: [frontend]
tags      : [css]
keywords  : [inline, internal, external]
author : epsi
opengraph:
  image: assets-frontend/2020/04/vim-html-basic.png

toc    : "toc-2020-04-html"

excerpt:
  Explain Where to Put Stylesheet

---

### Preface

> Goal: Explain Where to Put Stylesheet

Most common question,
from beginner that learning HTML, 
is where to place external assets.
I tought that, it is very easy,
but the question comes over and over again.

#### Where do we start

Basic stack example: `HTML+CSS+Javascript`.

#### Three Places

I need an article that provide example,
so that I do not need to explain it over and over again.

This article explain where to put stylesheet:
1. Oldschool Inline
2. Internal Code
3. External File

![Where to Put Stylesheet?][illustration-where-to-put]

#### Source Examples

You can obtain source examples here:

* [html-basic/01-stylesheet][source-example]

-- -- --

### Example Case

For every beginner that comes to my local group,
at [@htmlindonesia](https://t.me/htmlindonesia),
I always ask them to read [w3schools.com](https://www.w3schools.com/) first.

#### Original Case

I decide to build the case based on `w3schools`.

* [w3css alerts](https://www.w3schools.com/w3css/w3css_alerts.asp)

Let's skip this.

#### Skeleton

Usually, your `HTML5` document, have skeleton like this below:

{{< highlight html >}}
<!DOCTYPE html>
<html>
<head>
  <title>...</title>
  ...
</head>
<body>
  ...
</body>
</html>
{{< / highlight >}}

#### Oldschool Example

Consider start from simple Example,
the old school `HTML` might looks like these below:

* [gitlab.com/.../01-alert-inline.html][01-alert-inline].

{{< highlight html >}}
<html>
<head>
  <title>Example Inline Stylesheet</title>
</head>
<body>
  <h2>Relationship Alerts</h2>

  <div style="color: #fff; background-color: #f44336;
              padding: 16px; margin: 16px;">
    <p>
      <strong>Danger!</strong>:
      Red light indicates door a secured.
    </p>
  </div>
</body>
</html>
{{< / highlight >}}

The early html document in 90s use inline stylesheet.

{{< highlight css >}}
style="color: #fff; background-color: #f44336; padding: 16px; margin: 16px;"
{{< / highlight >}}

![Stylesheet: Oldschool Inline][stylesheet-inline]

#### Internal Stylesheet

After some time CSS become more complex.
We require reusable class for occuring element.
Number of pages growing, stylesheet code become longer and complicated.
So we need better management.

Consider have a look at resuable element using class below:

* [gitlab.com/.../02-alert-internal.html][02-alert-internal].

The class in this example grabbed `w3c.css`.
I stripped down the original css to use only what this page require.

{{< highlight html >}}
<!DOCTYPE html>
<html>
<head>
  <title>Example Simple Stylesheet</title>
  <meta charset="utf-8">
<style>
.container,
.panel {
  padding: 0.01em 24px;
  margin-top: 16px;
  margin-bottom: 16px;
}

.red {
  color: #fff !important;
  background-color: #f44336 !important;
}
.yellow {
  color: #000 !important;
  background-color: #ffeb3b !important;
}
.green {
  color: #fff !important;
  background-color: #4CAF50 !important;
}
.blue {
  color: #fff !important;
  background-color: #2196F3 !important;
}
</style>
</head>
<body>
  <div class="container">

    <h2>Relationship Alerts</h2>

    <div class="panel red">
      <p><strong>Danger!</strong>:
        Red light indicates door a secured.</p>
    </div>

    <div class="panel yellow">
      <p><strong>Warning!</strong>:
        Yellow light indicates that something could go wrong.</p>
    </div>

    <div class="panel green">
      <p><strong>Success!</strong>:
        Green light indicates on schedule, on budget, all good.</p>
    </div>

    <div class="panel blue">
      <p><strong>Info!</strong>:
        Blue light indicates that you are in electronics mode.</p>
    </div>

  </div>
</body>
</html>
{{< / highlight >}}

![Stylesheet: Reusable class in internal CSS][stylesheet-internal]

As you can see, we can have four box with different color,
instead of just one box.
All box utilize `panel` class.
And in the same time each box use different color class.

#### Complete Stylesheet

Now consider add button at the upper right corner.
We will add functionality later, now all we need is just the html design.
In this `HTML` code below,
I add `button large display-topright` class in `span` tag.

{{< highlight html >}}
    <div class="panel red display-container">
      <span class="button large display-topright">&times;</span>
      <p><strong>Danger!</strong>:
        Red light indicates door a secured.</p>
    </div>
{{< / highlight >}}

Still taken from `w3c.css`, add the stylesheet class as below:

{{< highlight css >}}

.button {
  border: none;
  display: inline-block;
  padding: 8px 16px;
  vertical-align: middle;
  overflow: hidden;
  text-decoration: none;
  color: inherit;
  background-color: transparent;
  text-align: center;
  cursor: pointer;
  white-space: nowrap;
}

.button {
  -webkit-touch-callout: none;
  -webkit-user-select: none;
  -khtml-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
} 

.button:hover{
  color: #000 !important;
  background-color: #ccc !important;
}

.large {
  font-size: 18px !important;
}

.display-topright {
  position: absolute;
  right: 0;
  top: 0;
}
{{< / highlight >}}

Now you see how the stylesheet class can grow,
and become complex for just one `span` tag.

* [gitlab.com/.../03-alert-complete.html][03-alert-complete].

{{< highlight html >}}
<body>
  <div class="container">

    <h2>Relationship Alerts</h2>

    <div class="panel red display-container">
      <span class="button large display-topright">&times;</span>
      <p><strong>Danger!</strong>:
        Red light indicates door a secured.</p>
    </div>

    <div class="panel yellow display-container">
      <span class="button large display-topright">&times;</span>
      <p><strong>Warning!</strong>:
        Yellow light indicates that something could go wrong.</p>
    </div>

    <div class="panel green display-container">
      <span class="button large display-topright">&times;</span>
      <p><strong>Success!</strong>:
        Green light indicates on schedule, on budget, all good.</p>
    </div>

    <div class="panel blue display-container">
      <span class="button large display-topright">&times;</span>
      <p><strong>Info!</strong>:
        Blue light indicates that you are in electronics mode.</p>
    </div>

  </div>
</body>
{{< / highlight >}}

![Stylesheet: More CSS Class][stylesheet-complete]

#### External Stylesheet

Internal stylesheet makes a page longer.
We also require to utilize the stylesheet,
so that it become reusable across different pages.

Consider change the head to use external stylesheet as below:

{{< highlight html >}}
<head>
  <title>Example Complete Stylesheet</title>
  <link rel="stylesheet" href="style.css">
</head>
{{< / highlight >}}

A real world web page might require more than one stylesheet file.
It is a good practice to use an assets folder for stylesheet.
For this example, I use `css` folder and put `style.css` in there.

You can have the complete script here:

* [gitlab.com/.../04-alert-external.html][04-alert-external].

and the stylesheet here:

* [gitlab.com/.../css/style.css][04-alert-style].

{{< highlight html >}}
<head>
  <title>Example Complete Stylesheet</title>
  <meta charset="utf-8">
  <link rel="stylesheet" href="css/style.css">
</head>
{{< / highlight >}}

The page looks, is exactly similar as previous example.
The only difference is your source code become cleaner now.

-- -- --

### What's Next?

Consider continue reading [ [HTML - Unobtrusive Javascript][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}frontend/2020/04/03/html-javascript/
[stylesheet-inline]:        {{< assets-frontend >}}/2020/04/11-stylesheet-inline.png
[stylesheet-internal]:      {{< assets-frontend >}}/2020/04/12-stylesheet-internal.png
[stylesheet-complete]:      {{< assets-frontend >}}/2020/04/13-stylesheet-complete.png
[illustration-where-to-put]:{{< assets-frontend >}}/2020/04/concept-css-where-to-put.png

[00-alert-original]:        {{< tutor-css-tools >}}/html-basic/01-stylesheet/00-alert-original.html
[01-alert-inline]:          {{< tutor-css-tools >}}/html-basic/01-stylesheet/01-alert-inline.html
[02-alert-internal]:        {{< tutor-css-tools >}}/html-basic/01-stylesheet/02-alert-internal.html
[03-alert-complete]:        {{< tutor-css-tools >}}/html-basic/01-stylesheet/03-alert-complete.html
[04-alert-external]:        {{< tutor-css-tools >}}/html-basic/01-stylesheet/04-alert-external.html
[04-alert-style]:           {{< tutor-css-tools >}}/html-basic/01-stylesheet/css/style.css

[source-example]:           {{< tutor-css-tools >}}/html-basic/01-stylesheet/
