---
type   : post
title  : "HTML - Javascript"
date   : 2020-04-03T09:17:35+07:00
slug   : html-javascript
categories: [frontend]
tags      : [javascript]
keywords  : [unobtrusive javascript, inline, internal, external]
author : epsi
opengraph:
  image: assets-frontend/2020/04/vim-html-basic.png

toc    : "toc-2020-04-html"

excerpt:
  Explain where to put Javascript also unobtrusive Javascript

---

### Preface

> Goal: Explain where to put Javascript also unobtrusive Javascript

Most developer these days know complex javascript framework.
Beginner who step in must know about the basic,
the unobstrusive javascript.
I use no javascript framework,
so that everyone can enjoy this beginner example.

#### Three Places

This article also explain where to put javascript:
1. Oldschool Inline
2. Internal Code
3. External File

![Where to Put Javascript?][illustration-where-to-put]

#### Source Examples

You can obtain source examples here:

* [html-basic/02-javascript][source-example]

-- -- --

### Example Case

Consider continue from  our previous example.

#### Oldschool Example

The old school `javascript` might looks like these below.
The javascript is attached on an event in a tag.
In this case the `onclick` event.

* [gitlab.com/.../01-alert-inline.html][01-alert-inline].

{{< highlight html >}}
    <div class="panel red display-container">
      <span onclick="this.parentElement.style.display='none'"
            class="button large display-topright">&times;</span>
      <p><strong>Danger!</strong>:
        Red light indicates door a secured.</p>
    </div>
{{< / highlight >}}

The early html document in 90s use inline stylesheet.

{{< highlight css >}}
onclick="this.parentElement.style.display='none'"
{{< / highlight >}}

Now you can click each alert box,
and the clicked box will be vanished.

![Javascript: Oldschool Inline][javascript-inline]

This looks ugly, I know,
because the javascript code is obstrusively written inside a tag.
This method tends to be error prone,
while it comes to complex javascript response.
That is why we need to find other way.

#### Skeleton

Usually, your `HTML5` document, have skeleton like this below:

{{< highlight html >}}
<!DOCTYPE html>
<html>
<head>
  <title>...</title>
  <style>
    ...
  </style>
  <script>
      ...
  </script>
</head>
<body>
    ...
</body>
</html>
{{< / highlight >}}

Let's apply the javascript there.

#### Unobtrusive Javascript Event

We can rewrite the `onclick` event above as below code.
Don't get intimidate by this long code.
This will be clearer soon.

{{< highlight html >}}
<body>
  <div class="container">
    ...
  </div>
  <script>
    var alertButtons = document.getElementsByClassName("dismissable");
    
    for (var i=0; i < alertButtons.length; i++) {
      alertButtons[i].onclick = function() {
        this.parentElement.style.display='none';
        return false;
      };
    };
  </script>
</body>
{{< / highlight >}}

We require a class to identify the DOM (domain object model).
Let's name it `dismissible`, and put on each `span` tag.

{{< highlight html >}}
<body>
  <div class="container">

    <h2>Relationship Alerts</h2>
    <p>To close the alerts, click on the X in the upper right corner:</p>

    <div class="panel red display-container">
      <span class="dismissable button large display-topright">&times;</span>
      <p><strong>Danger!</strong>:
        Red light indicates door a secured.</p>
    </div>

    ...

  </div>
  <script>
    ...
  </script>
</body>
{{< / highlight >}}

Now you can see that the `span` tag become cleaner.
Your page design should be free from cryptic code.

But hey, there is downside of this method!
The script can only be placed at the bottom of the document.

#### Event Listener

In order to put the javascript on above document,
we need to add an event listener that triggered when the page ready.
The event is named as `DOMContentLoaded`.

We can rewrite the the function inside `head` tag, as below code:

{{< highlight html >}}
<!DOCTYPE html>
<html>
<head>
...
<script>
  document.addEventListener("DOMContentLoaded", function(event) { 
    var alertButtons = document.getElementsByClassName("dismissable");
    
    for (var i=0; i < alertButtons.length; i++) {
      alertButtons[i].onclick = function() {
        this.parentElement.style.display='none';
        console.log('Close Button. Element ' + this.parentElement.id + ' dismissed');
        return false;
      };
    };
  });
</script>
</head>
<body>
  ...
</body>
</html>
{{< / highlight >}}

In your text editor, this could be shown as below:

![HTML Basic: Geany Editor: Internal][geany-html-internal]

I also put a `console.log` to show the `id` of the `span` tag ,
so that you can debug easily using `inspect element`.
Now the code should looks like something below:

{{< highlight html >}}
    <div class="panel red display-container" id="red-danger">
      <span class="dismissable button large display-topright">&times;</span>
      <p><strong>Danger!</strong>:
        Red light indicates door a secured.</p>
    </div>
{{< / highlight >}}

Or in a complete fashionen you can examine the code below:

* [gitlab.com/.../02-alert-internal.html][02-alert-internal].

{{< highlight html >}}
<!DOCTYPE html>
<html>
<head>
   ...
</head>
<body>
  <div class="container">

    <h2>Relationship Alerts</h2>
    <p>To close the alerts, click on the X in the upper right corner:</p>

    <div class="panel red display-container" id="red-danger">
      <span class="dismissable button large display-topright">&times;</span>
      <p><strong>Danger!</strong>:
        Red light indicates door a secured.</p>
    </div>

    <div class="panel yellow display-container" id="yellow-warning">
      <span class="dismissable button large display-topright">&times;</span>
      <p><strong>Warning!</strong>:
        Yellow light indicates that something could go wrong.</p>
    </div>

    <div class="panel green display-container" id="green-success">
      <span class="dismissable button large display-topright">&times;</span>
      <p><strong>Success!</strong>:
        Green light indicates on schedule, on budget, all good.</p>
    </div>

    <div class="panel blue display-container" id="blue-info">
      <span class="dismissable button large display-topright">&times;</span>
      <p><strong>Info!</strong>:
        Blue light indicates that you are in electronics mode.</p>
    </div>

  </div>
</body>
</html>
{{< / highlight >}}

Again, you can click the dismissable alert button.
And see each button vanished one by one.

![Javascript: Unobstrusive OnClick Event][javascript-internal]

#### External Javascript

As we did with stylesheet,
we can put the javascript into external files.
So that we can reuse the code in other page.

{{< highlight html >}}
<head>
  <title>Example Assets</title>
  <script src="js/script.js"></script>
</head>
{{< / highlight >}}

You can have the complete script here:

* [gitlab.com/.../03-alert-external.html][03-alert-external].

and the javascript here:

* [gitlab.com/.../js/script.js][03-alert-script].

In your text editor, this could be shown as below:

![HTML Basic: Geany Editor: External][geany-html-external]

One of my favorites javascript is `livejs`,
so that whenever change happened in `localhost`,
the page will be automatically refreshed.
And I also like `meta` tags.
Now we can complete our `head` tag as below:

{{< highlight html >}}
<head>
  <title>Example Assets</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible"
        content="IE=edge,chrome=1">
  <meta name="viewport"
        content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="css/style.css">
  <script src="js/script.js"></script>
  <script src="js/live.js">
</head>
{{< / highlight >}}

The result in browser is the same as previous example.

-- -- --

### What's Next?

Consider continue reading [ [HTML - Developing Tips][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}frontend/2020/04/05/html-developing-tips/

[illustration-where-to-put]:{{< assets-frontend >}}/2020/04/concept-js-where-to-put.png

[javascript-inline]:        {{< assets-frontend >}}/2020/04/21-javascript-inline.png
[javascript-internal]:      {{< assets-frontend >}}/2020/04/22-javascript-internal.png

[geany-html-internal]:      {{< assets-frontend >}}/2020/04/22-geany-html-internal.png
[geany-html-external]:      {{< assets-frontend >}}/2020/04/23-geany-html-external.png

[01-alert-inline]:          {{< tutor-css-tools >}}/html-basic/02-javascript/01-alert-inline.html
[02-alert-internal]:        {{< tutor-css-tools >}}/html-basic/02-javascript/02-alert-internal.html
[03-alert-external]:        {{< tutor-css-tools >}}/html-basic/02-javascript/03-alert-external.html
[03-alert-script]:          {{< tutor-css-tools >}}/html-basic/02-javascript/js/script.js

[source-example]:           {{< tutor-css-tools >}}/html-basic/02-javascript/
