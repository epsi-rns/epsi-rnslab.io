---
type   : post
title  : "HTML - Developing Tips"
date   : 2020-04-05T09:17:35+07:00
slug   : html-developing-tips
categories: [frontend]
tags      : [browser]
keywords  : [javascript, browser]
author : epsi
opengraph:
  image: assets-frontend/2020/04/vim-html-basic.png

toc    : "toc-2020-04-html"

excerpt:
  Good tips while developing HTML.
  Simple web server, and live reload.

---

### Preface

> Goal: A few good tips while developing HTML.

-- -- --

### Simple Web Server

This below only server static html files along with static assets.

#### BrowserSync

Before I close this article there is one trick that I want to share.
I found a good tools to show your directory in browser.
It is called `browsersync`, you can install it using `npm`.

{{< highlight bash >}}
$ browser-sync start --server --directory
{{< / highlight >}}

The looks of browsersync is as figure below.

![HTML Basic: Browsersync Root][browsersync-root]

Now you can load your page directly from web browser,
instead of launch the page from file manager.
Minimize switching, therefore more efficiency.

![HTML Basic: Browsersync Assets][browsersync-assets]

You can also walk through assets directory.
And see the stylesheet directly in browser,
and also have a look at javascript in browser.

#### Python Simple HTTP Server

Another result can be done using python3 as below:

{{< highlight bash >}}
$ python -m http.server
{{< / highlight >}}

![HTML Basic: Python Simple HTTP Server][python-http-server]

But it does not play well with `LiveJS`.

#### PHP Simple HTTP Server

How about PHP?

{{< highlight bash >}}
$ php -S localhost:8000
{{< / highlight >}}

![HTML Basic: Python Simple HTTP Server][python-http-server]

This does not play well with `LiveJS` either.

#### Ruby Webrick

How about Ruby?
Webrick is HTTP server toolkit.

{{< highlight bash >}}
$ ruby -run -e httpd -- -p 5000
{{< / highlight >}}

![HTML Basic: ruby Webrick: HTTP server Toolkit][ruby-webrick]

This does not play well with `LiveJS` either.

-- -- --

### Live JS

Have you ever get tired to refresh every time?
With LiveJS, the browser will automatically refresh,
whenever change occured on your document.
So you do not need to refresh manually,
everytime you save your `.html` file.

* [livejs.com](https://livejs.com)

{{< highlight html >}}
<head>
  <title>One Simple Example Page</title>
  <link rel="stylesheet" href="02-assets/style.css">
  <script src="02-assets/script.js"></script>
   <script src="live.js">
</head>
{{< / highlight >}}

> `Live.js` doesn't support the `file` protocol. It needs `http`.


-- -- --

### What's Next?

Consider continue reading [ [HTML - Inspect Element][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}frontend/2020/04/06/html-inspect-element/

[browser-html-basic]:       {{< assets-frontend >}}/2020/04/01-browser-html-basic.png
[browsersync-root]:         {{< assets-frontend >}}/2020/04/01-browsersync-root.png
[browsersync-assets]:       {{< assets-frontend >}}/2020/04/01-browsersync-assets.png
[python-http-server]:       {{< assets-frontend >}}/2020/04/01-python-http-server.png
[ruby-webrick]:             {{< assets-frontend >}}/2020/04/01-ruby-webrick.png
