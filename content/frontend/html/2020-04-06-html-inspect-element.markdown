---
type   : post
title  : "HTML - Inspect Element"
date   : 2020-04-06T09:17:35+07:00
slug   : html-inspect-element
categories: [frontend]
tags      : [browser]
keywords  : [javascript, coffescript, ]
author : epsi
opengraph:
  image: assets-frontend/2020/04/inspect-change-color.png

toc    : "toc-2020-04-html"

excerpt:
  Examining HTML document using Inspect Element.

---

### Preface

> Goal: Explaning how to inspect HTML document.

This is also should be, an easy task to do.
But I found that many beginner don't know how to examine HTML,
and of course change stylesheet behaviour.
So I decide to make this short article,
using our last example.

-- -- --

### Other Browser Tools

> This is a must have knowledge

Before inspecting Element consider see built tools that your browser might have have.

First, open the previous example, using `browsersync` in `localhost:3000`.

* <http://localhost:3000/02-alert-external.html>

![Browser Tools: HTML Page][browser-alert-external]

#### Source Code

If you are using Firefox, you can press `Ctrl+U` to show source code.
This is very useful if you work with backend or SSG (static site generator).

![Browser Tools: Source Code][browser-source-code]

Chromium has similar feature.

#### Meta

If you are using Firefox, you can press `Ctrl+I` to show `meta header`.
This looks silly when you have a few meta,
but you are going to need this feature,
once you had many meta such as `opengraph`, `twitter` and such SEO stuff.

![Browser Tools: Meta Header][browser-meta-header]

-- -- --

### Inspect Element

You can open `inspect element` from context menu.

![Browser Tools: Meta Header][browser-context-menu]

And you can either dock the `inspect element` within the browser,
put let the `inspect element` be separate window.

#### HTML

Consider see the result if click the `red alert`,
and inspect the element.

![Inspector: Element][inspector-element]

#### Stylesheet

One of the most useful thing in `element inspector` is,
we can change the stylesheet behaviour in a live page.
It means, we don't need to change the source code.
Nor we need to refresh the page.

Consider have a look at the source code of the previous example:

* [gitlab.com/.../css/style.css][04-alert-style].

{{< highlight css >}}
.red {
  color: #fff !important;
  background-color: #f44336 !important;
}
{{< / highlight >}}

Now consider changing the value in inspect element.
Change the color of the text of the `red alert`,
from white `fff` to yellow `ff0`!

![Inspector: Change Color Element][inspector-change]

And see the result as below:

![Browser: Result of the Color Change][browser-change-color]

#### Javascript

How about the Javascript?
Remember the old days when people use `pop-up alert( ... )`?
Now we have `console.log( ... )`.

Consider have a look at the unobtrusive javascript below:

* [gitlab.com/.../js/script.js][03-alert-script].

{{< highlight javascript >}}
document.addEventListener("DOMContentLoaded", function(event) { 
  var alertButtons = document.getElementsByClassName("dismissable");
    
  for (var i=0; i < alertButtons.length; i++) {
    alertButtons[i].onclick = function() {
      this.parentElement.style.display='none';
      console.log('Close Button. Element ' + this.parentElement.id + ' dismissed');
      return false;
    };
  };
});
{{< / highlight >}}

And also how the trigger be done in HTML code,
by just defining the `dismissable` class.

{{< highlight html >}}
    <div class="panel red display-container" id="red-danger">
      <span class="dismissable button large display-topright">&times;</span>
      <p><strong>Danger!</strong>:
        Red often indicates a dangerous or negative situation.</p>
    </div>

    <div class="panel yellow display-container" id="yellow-warning">
      <span class="dismissable button large display-topright">&times;</span>
      <p><strong>Warning!</strong>:
        Yellow light indicates that something could go wrong.</p>
    </div>

    <div class="panel green display-container" id="green-success">
      <span class="dismissable button large display-topright">&times;</span>
      <p><strong>Success!</strong>:
        Green light indicates on schedule, on budget, all good.</p>
    </div>

    <div class="panel blue display-container" id="blue-info">
      <span class="dismissable button large display-topright">&times;</span>
      <p><strong>Info!</strong>:
        Blue light indicates that you are in electronics mode.</p>
    </div>
{{< / highlight >}}

Now, consider click `x` in each alert box.
We are going to see the result in console.

![Inspector: Javascript Console][inspector-console]

#### Other Feature

There are some other feature,
but this is beyond the scope this tutorial for beginner.

For example, you can try this `network` tab yourself.
This is useful to track if there are any asset that failed to be loaded.

![Inspector: Network][inspector-network]

I think that is all.

-- -- --

### What's Next?

Consider continue reading [ [Tools - HTML - Pug][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}frontend/2020/04/07/tools-html-pug/

[04-alert-style]:           {{< tutor-css-tools >}}/html-basic/01-stylesheet/css/style.css
[03-alert-script]:          {{< tutor-css-tools >}}/html-basic/02-javascript/js/script.js

[browser-alert-external]:   {{< assets-frontend >}}/2020/04/03-browser-alert-external.png
[browser-source-code]:      {{< assets-frontend >}}/2020/04/03-browser-source-code.png
[browser-meta-header]:      {{< assets-frontend >}}/2020/04/03-browser-meta-header.png
[browser-context-menu]:     {{< assets-frontend >}}/2020/04/03-browser-context-menu.png
[inspector-element]:        {{< assets-frontend >}}/2020/04/02-inspect-element.png
[inspector-change]:         {{< assets-frontend >}}/2020/04/02-inspect-change-color.png
[inspector-console]:        {{< assets-frontend >}}/2020/04/02-inspect-console.png
[inspector-network]:        {{< assets-frontend >}}/2020/04/02-inspect-network.png
[browser-change-color]:     {{< assets-frontend >}}/2020/04/03-browser-change-color.png
