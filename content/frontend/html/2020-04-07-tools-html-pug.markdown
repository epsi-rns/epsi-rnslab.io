---
type   : post
title  : "Tools - HTML - Pug"
date   : 2020-04-07T09:17:35+07:00
slug   : tools-html-pug
categories: [frontend]
tags      : [html, pug]
keywords  : [template]
author : epsi
opengraph:
  image: assets-frontend/2020/04/31-vim-include-source.png

toc    : "toc-2020-04-html"

excerpt:
  An example of Pug, HTML templating language for beginner.
  
---

### Preface

> Goal: An example of Pug, HTML templating language for beginner.

Templating language is something you would use in the backend,
that would produce your frontend content. Confused yet?

#### Where do continue

An example of template stack: `Pug+Stylus+Coffeescript`.

We have start the tutorial from basic stack example: `HTML+CSS+Javascript`.
Now we continue to an example of template stack: `Pug+Stylus+Coffeescript`.
Note that in real world, there are many combination of template stack,
and you might consider using other than what in this article series.

#### About The Stack

There are many templating language for HTML,
such as Nunjucks, EJS, liquid, Jade/Pug and so on.
My favorites is:

1. HTML: Nunjucks
2. CSS: SASS
3. Javascript: No favorites yet.

Here, I represent you,a cool template stack: `Pug+Stylus+Coffeescript`.
Not my favorites, but very unique,
because all templates has indented style like python language.
All are NodeJS based.

1. HTML: Pug
2. CSS: Stylus
3. Javascript: Coffeescript

#### Source Examples

You can obtain source examples here:

* [html-basic/03-template][source-example]

Let's get it on. Start with a HTML preprocessor called `Pug`.

-- -- --

### PUG

#### Official Documentation

* [pugjs.org](https://pugjs.org/)

#### Install

{{< highlight bash >}}
$ npm i -g pug-cli
{{< / highlight >}}

#### Simple Source

Consider this indented document PUG as below:

* [gitlab.com/.../alert.pug][03-alert-pug].

{{< highlight haskell >}}
doctype html
html(lang="en")

  head
    title Example Template

  body
    .container
      h2 Relationship Alerts
      p To close the alerts, click on the X in the upper right corner:

      div.panel.red.display-container#red-danger
        span.dismissable.button.large.display-topright &times;
        p
          strong Danger!
          | : Red light indicates door a secured.
{{< / highlight >}}

![Jade/Pug: Pug Source Code][31-vim-pug]

#### Command Line

Now you can run this command:

{{< highlight bash >}}
$ pug < alert.pug > alert.html
{{< / highlight >}}

And the result would be a looong code as below

{{< highlight html >}}
<!DOCTYPE html><html lang="en"><head><title>Example Template</title></head><body><div class="container"><h2>Relationship Alerts</h2><p>To close the alerts, click on the X in the upper right corner:</p><div class="panel red display-container" id="red-danger"><span class="dismissable button large display-topright">&times;</span><p><strong>Danger!</strong>: Red light indicates door a secured.</p></div></div></body></html>
{{< / highlight >}}

![Jade/Pug: plain HTML result][31-vim-html]

It is hard to examine if you got the right result or not,
so we need other tool called `prettier`.

#### Pretty Printing

{{< highlight bash >}}
$ pug -P < alert.pug > alert.html
{{< / highlight >}}

And the result would be a prettier one.

* [gitlab.com/.../alert.html][03-alert-html].

{{< highlight html >}}
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Example Template</title>
  </head>
  <body>
    <div class="container">
      <h2>Relationship Alerts</h2>
      <p>To close the alerts, click on the X in the upper right corner:</p>
      <div class="panel red display-container" id="red-danger">
        <span class="dismissable button large display-topright">&times;</span>
        <p><strong>Danger!</strong>: Red light indicates door a secured.</p>
      </div>
    </div>
  </body>
</html>
{{< / highlight >}}

![Jade/Pug: HTML result with prettier][31-vim-html-prettier]

#### Watch

You do not need to run command for every changes.

{{< highlight bash >}}
$ pug -w -P -p . alert.pug
{{< / highlight >}}

Notice that you use `alert.pug` directly instead of stdin `< alert.pug`.


-- -- --

### More about Pug

The `Pug` is enough to build a simple static webpage.

#### Directory Structure

How about restructure the directory tree as below:

![Jade/Pug: Tree Directories][31-tree-directories]

In this scheme, I use `partials` as `include path`.
But you can have any names that you want.

#### Include

You can separate HTML part in other file artefact,
and use it later using `include` directive.
You can also include assets, as inline style or script.

* [gitlab.com/.../pug/alert-include.pug][03-alert-include-pug].

{{< highlight haskell >}}
doctype html
html(lang="en")

  head
    meta(charset='utf-8')
    title Example Template
    style
      include ../css/style.css
    script
      include ../js/script.js
    script(src='../js/live.js')

  body
    include partials/body.pug
{{< / highlight >}}

You can see that your project become modular,
and could contain reusable `pug` parts such as `partials/body.pug`.

#### Partials

The body code is shown as below.

* [gitlab.com/.../partials/body.pug][03-body-pug].

{{< highlight haskell >}}
.container
  h2 Relationship Alerts
  p To close the alerts, click on the X in the upper right corner:

  div.panel.red.display-container#red-danger
    span.dismissable.button.large.display-topright &times;
    p
      strong Danger!
      | : Red light indicates door a secured.

  div.panel.yellow.display-container#yellow-warning
    span.dismissable.button.large.display-topright &times;
    p
      strong Warning!
      | : Yellow light indicates that something could go wrong.

  div.panel.green.display-container#green-success
    span.dismissable.button.large.display-topright &times;
    p
      strong Success!
      | : Green light indicates on schedule, on budget, all good.

  div.panel.blue.display-container#blue-info
    span.dismissable.button.large.display-topright &times;
    p
      strong Info!
      | : Blue light indicates that you are in electronics mode.
{{< / highlight >}}

Be aware of the indentation of the `body.pug`.

#### Command Line Using Path

Be aware of the file path in command line.
you can solved this by using path `-p .` directive.

{{< highlight haskell >}}
$ cd pug
$ pug --pretty -p . < alert-include.pug > ../html/alert-include.html
{{< / highlight >}}

Notice that this example, I utilize `.\pug` as working directory.
You can have your own working directory, just be sure,
that the `include` directive use the right path.

#### Current Build

The build result for this step can be shown,
in `html` directory inside tree, as below figure:

![Jade/Pug: Tree Files][31-tree-files-current]

#### Watch Inside Directory

Watch inside directory is about the same as usual

{{< highlight bash >}}
$ cd pug
$ pug -w -P -p . -o ../html alert-include.pug
{{< / highlight >}}

![Jade/Pug: CLI watch][31-cli-watch]

You can also watch all folder

{{< highlight bash >}}
$ pug -w -P -p . -o ../html *.pug
{{< / highlight >}}

* .

#### Filter

You can compile source assets directly into `pug`.

{{< highlight bash >}}
$ npm i -g jstransformer-stylus
$ npm i -g jstransformer-coffee-script
{{< / highlight >}}

Now you can use your `stylus` and `coffescript` directly.

* [gitlab.com/.../alert-include-source.pug][03-alert-source-pug].

{{< highlight haskell >}}
doctype html
html(lang="en")

  head
    meta(charset='utf-8')
    title Example Template
    style 
      include:stylus ../css/style.styl
    script      
      include:coffee-script(bare=true) ../js/script.coffee
    script(src='../js/live.js')

  body
    include partials/body.pug
{{< / highlight >}}

![Jade/Pug: Include Source][31-vim-include-source]

#### Complete Build

The build result can be shown,
in `html` directory inside tree, as below figure:

![Jade/Pug: Tree Files][31-tree-files-complete]

#### Finally

This article is intended a brief overview for beginner.
From this starting point, you can step on to other templating language.
I won't go deep with `Pug` in this article series.
We will go deep later with `Pug` in other article series.

-- -- --

### What's Next?

Consider continue reading [ [Tools - CSS - Stylus][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}frontend/2020/04/08/tools-css-stylus/

[03-body-pug]:              {{< tutor-css-tools >}}/html-basic/03-template/pug/partials/body.pug
[03-alert-pug]:             {{< tutor-css-tools >}}/html-basic/03-template/pug/alert.pug
[03-alert-html]:            {{< tutor-css-tools >}}/html-basic/03-template/html/alert.html
[03-alert-include-pug]:     {{< tutor-css-tools >}}/html-basic/03-template/pug/alert-include.pug
[03-alert-source-pug]:      {{< tutor-css-tools >}}/html-basic/03-template/pug/alert-include-source.pug

[31-vim-pug]:               {{< assets-frontend >}}/2020/04/31-vim-pug.png
[31-vim-html]:              {{< assets-frontend >}}/2020/04/31-vim-html.png
[31-vim-html-prettier]:     {{< assets-frontend >}}/2020/04/31-vim-html-prettier.png
[31-cli-watch]:             {{< assets-frontend >}}/2020/04/31-cli-watch.png
[31-vim-include-source]:    {{< assets-frontend >}}/2020/04/31-vim-include-source.png
[31-tree-directories]:      {{< assets-frontend >}}/2020/04/31-tree-directories.png
[31-tree-files-complete]:   {{< assets-frontend >}}/2020/04/31-tree-files-complete.png
[31-tree-files-current]:    {{< assets-frontend >}}/2020/04/31-tree-files-current.png

[source-example]:           {{< tutor-css-tools >}}/html-basic/03-template/
