---
type   : post
title  : "Tools - CSS - Stylus"
date   : 2020-04-08T09:17:35+07:00
slug   : tools-css-stylus
categories: [frontend]
tags      : [css, stylus]
keywords  : [template, stylus]
author : epsi
opengraph:
  image: assets-frontend/2020/04/32-vim-stylus.png

toc    : "toc-2020-04-html"

excerpt:
  An example of Stylus, CSS preprocessor for beginner.

---

### Preface

> Goal: An example of Stylus, CSS preprocessor for beginner.

Here, I represent you,a cool template stack: `Pug+Stylus+Coffeescript`.

1. HTML: Pug
2. CSS: Stylus
3. Javascript: Coffeescript

#### Source Examples

You can obtain source examples here:

* [html-basic/03-template][source-example]

Let's get it on. Start with a CSS preprocessor called `Stylus`.

-- -- --

### Stylus

#### Official Documentation

* [stylus-lang.com](https://stylus-lang.com/)

#### Install

{{< highlight bash >}}
$ npm i -g stylus
{{< / highlight >}}

#### Simple Source

Consider this indented document styles as below:

* [gitlab.com/.../style.styl][03-style-styl].

{{< highlight sass >}}
.red
  color: #fff !important
  background-color: #f44336 !important

.yellow
  color: #000 !important
  background-color: #ffeb3b !important

.green
  color: #fff !important
  background-color: #4CAF50 !important

.blue
  color: #fff !important
  background-color: #2196F3 !important
{{< / highlight >}}

It is very similar with CSS,
but without curly braces, nor semicolon.

![Stylus: Stylus Source Code][32-vim-stylus]

#### Command Line

Now you can run this command:

{{< highlight bash >}}
$ stylus -w css/style.styl -o css/style.css
  watching /home/epsi/.npm-global/lib64/node_modules/stylus/lib/functions/index.styl
  compiled css/style.css
  watching css/style.styl
{{< / highlight >}}

![Stylus: Stylus Command Line][32-cli-stylus]

And the result would be a simple CSS code as below

* [gitlab.com/.../style.css][03-style-css].

{{< highlight css >}}
.red {
  color: #fff !important;
  background-color: #f44336 !important;
}
.yellow {
  color: #000 !important;
  background-color: #ffeb3b !important;
}
.green {
  color: #fff !important;
  background-color: #4caf50 !important;
}
.blue {
  color: #fff !important;
  background-color: #2196f3 !important;
}
{{< / highlight >}}

![Stylus: CSS result][32-vim-css]

#### Finally

This article is intended a brief overview for beginner.
From this, you can step on to other preprocessor.
I won't go deep with `Stylus` in this article series.
I have another article series, contain `SASS`, `LESS`, and `PostCSS`.

-- -- --

### Internal Stylus in Pug

This topic has been discussed in previous article.

* [Tools - HTML - Pug][local-pug]

-- -- --

### What's Next?

Consider continue reading [ [Tools - Javascript - Coffescript][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[03-style-css]:             {{< tutor-css-tools >}}/html-basic/03-template/css/style.css
[03-style-styl]:            {{< tutor-css-tools >}}/html-basic/03-template/css/style.styl

[local-whats-next]:         {{< baseurl >}}frontend/2020/04/09/tools-javascript-coffeescript/
[local-pug]:                {{< baseurl >}}frontend/2020/04/07/tools-html-pug/

[32-vim-stylus]:            {{< assets-frontend >}}/2020/04/32-vim-stylus.png
[32-cli-stylus]:            {{< assets-frontend >}}/2020/04/32-cli-stylus.png
[32-vim-css]:               {{< assets-frontend >}}/2020/04/32-vim-css.png

[source-example]:           {{< tutor-css-tools >}}/html-basic/03-template/
