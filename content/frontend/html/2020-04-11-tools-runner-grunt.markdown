---
type   : post
title  : "Task Runner - Grunt"
date   : 2020-04-11T09:17:35+07:00
slug   : tools-task-runner-grunt
categories: [frontend]
tags      : [grunt]
keywords  : [task runner]
author : epsi
opengraph:
  image: assets-frontend/2020/04/34-vim-gruntfile-js.png

toc    : "toc-2020-04-html"

excerpt:
  Put the whole stuff together using grunt task runner.

---

### Preface

> Goal: Put the whole stuff together using grunt task runner.

Have you ever got tired of saving, and running render command,
and then go back to text editor, and doing this over and over again?
This is why we need automation.

`pug` has built in tools to run this automation, and so does other tools.
But when in comes to work with many tools, this won't be sufficient.
Imagine you have three tools, and you have to run,
three different tools, in three different terminal.
This would be exhausting.

As your project grown, you might need other tools,
you can utilize task runner such as `gulp` or `grunt`.

#### Source Examples

You can obtain source examples here:

* [html-basic/03-template][source-example]

#### Automation

> Grunt is simply an automation.

Suppose that you want to build a website without framework,
nor SSG, nor fancy backend using `Pug+Stylus+Coffeescript` stack.
`grunt` can watch each component, and compile into vanilla.

* Generate `HTML` from `Pug`.

* Render `CSS` from `Stylus`.

* Compile `Javascript` from `Coffeescript`

![Grunt: Stylus + Coffeescript + Pug][34-vim-stack]

In short, `grunt` can watch the whole stack at once.

#### Downside

> Grunt is an old tool.

The `task runner` is belong to the past
for conservative web development.
Use `bundler` for modern web development instead,
such as `webpack`, `roller` or `parcel`.
Both `task runner` and `bundler` might takes some configuration effort.

If you desire vanilla, `grunt` is still your choice.

-- -- --

### Grunt

> Tips: Do not get initimidated by long config, once you understans, you can reuse in other project.

Here I represent an automation example on how to build `Gruntfile.js`.

### Official Documentation

* [gruntjs.com/](https://gruntjs.com/)

#### Install

{{< highlight bash >}}
$ npm install -g grunt
{{< / highlight >}}

Since in this project, we are dealing with `Pug+Stylus+Coffeescript`,
we need to install each `contrib`.
We also need to the `contrib-watch`.

You can also install locally per project, instead of global install.

{{< highlight bash >}}
$ npm install grunt --save-dev
$ npm install grunt-contrib-pug --save-dev
$ npm install grunt-contrib-stylus --save-dev
$ npm install grunt-contrib-coffee --save-dev
$ npm install grunt-contrib-watch --save-dev
{{< / highlight >}}

Or in package.json

* [gitlab.com/.../package.json][03-package-json]

{{< highlight javascript >}}
{
  ...
  "devDependencies": {
    "grunt": "^1.0.1",
    "grunt-contrib-pug": "^2.0.0",
    "grunt-contrib-stylus": "^1.2.0",
    "grunt-contrib-coffee": "^2.1.0",
    "grunt-contrib-watch": "^1.1.0"
  }
}
{{< / highlight >}}

And do `$ npm install` as a starter before you begin your project.

#### Gruntfile.js

You also need to a configuration file named `Gruntfile.js`.

{{< highlight bash >}}
$ touch Gruntfile.js
{{< / highlight >}}

As an overview,
the `grunt` configuration skeleton,
for our little project,
is shown as below:

{{< highlight javascript >}}
module.exports = function(grunt) {
  // configure the tasks
  let config = {

    // Pug
    pug: {
      ...
    },

    // Stylus
    stylus: {
      ...
    },

    // Coffeescript
    coffee: {
      ...
    },

    //  Watch Files
    watch: {
      pug: {
        ...
      },
      stylus: {
        ...
      },
      coffee: {
        ...
      }
    }

  };

  grunt.initConfig(config);

  // load the tasks
  ...

  // define the tasks
  grunt.registerTask('default', [
    'pug', 'stylus', 'coffee', 'watch'
  ] );

};
{{< / highlight >}}

Open your favorite text editor and start writing configuration.

-- -- --

### Pug Case

Consider our last `alert.pug`.
We are going to compile it using `grunt`.

* [gitlab.com/.../Gruntfile.js][03-gruntfile-js]

#### One Pug to One HTML

Have a look at using this configuration below:

{{< highlight javascript >}}
module.exports = function(grunt) {
  // configure the tasks
  let config = {

    // Pug
    pug: {
      compile: {
        options: {
          pretty: true
        },
        files: {
          'html/alert.html': 'pug/alert.pug' // 1:1 compile
        }
      }
    }
  };

  grunt.initConfig(config);

  // load the tasks
  grunt.loadNpmTasks('grunt-contrib-pug');

  // define the tasks
  grunt.registerTask('default', [
    'pug'
  ] );
};
{{< / highlight >}}

With this configuration, we can run:

{{< highlight bash >}}
$ grunt pug
{{< / highlight >}}

Or simply the use the `default` task

{{< highlight bash >}}
$ grunt
{{< / highlight >}}

![Grunt: One to One Pug Render][34-grunt-pug-one]

This will compile `pug/alert.pug` to `pug/alert.html`.

#### Filters

How about `pug/alert-include-source.pug`
to `pug/alert-include-source.html`?
Allright, this should be as simple as changing line below:

{{< highlight javascript >}}
  files: {
    'html/alert-include-source.html': 'pug/alert-include-source.pug'
  }
{{< / highlight >}}

Let's see what's happened!

![Grunt: Pug Filter Error][34-grunt-pug-filter-error]

To solve the situation we just need to to add both filters:
* to render `stylus` and,
* to compile `coffescript`.

{{< highlight javascript >}}
  options: {
    pretty: true,
    filters: {
      'stylus': function(block) {
       return require('stylus').render(block);
      },
     'coffee-script': function(block) {
        return require('coffeescript').compile(block);
      },
    },
  },
  files: {
   'html/alertinclude-source.html': 'pug/alert-include-source.pug'
  }
{{< / highlight >}}

Now the `grunt` should running fine.

#### Compile Many Pug at Once

What good is automation if we cannot run many task at once?
Doing this is simply as changing `files` configuration as below:

{{< highlight javascript >}}
  files: [ {
    cwd: "pug",
    src: "*.pug",
    dest: "html",
    expand: true,
    ext: ".html"
  } ]
{{< / highlight >}}

This will compile `pug/*.pug` into its representative in `html/*.html`.

![Grunt: Compile The Whole Pug Files][34-grunt-pug-many]

As you can see in figure above, now the grunt compile three files.

#### Watch

This the most automation task from `grunt`.
The watch configuration is simple:

{{< highlight javascript >}}
    //  Watch Files
    watch: {
      pug: {
        files: ['pug/**/*'],
        tasks: ['pug'],
        options: {
          interrupt: false,
          spawn: false
        }
      }
{{< / highlight >}}

To complete the code, is to simply alter the configuration as below:

{{< highlight javascript >}}
module.exports = function(grunt) {
  // configure the tasks
  let config = {

    // Pug
    pug: {
      ...
    },
    
    //  Watch Files
    watch: {
      ...
    }
  };

  grunt.initConfig(config);

  // load the tasks
  grunt.loadNpmTasks('grunt-contrib-pug');
  grunt.loadNpmTasks('grunt-contrib-watch');  

  // define the tasks
  grunt.registerTask('default', [
    'pug', 'watch'
  ] );
};
{{< / highlight >}}

Now the default task will run `pug task`, then run `watch task`.

![Grunt: Watch Pug][34-grunt-watch]

Alter one, or two `pug` files, to see the `watch task` result.

-- -- --

### Complete Case

As I said earlier, we use this stack `Pug+Stylus+Coffeescript`.
So these stack should be completely covered by `Gruntfile.js` configuration.

#### Stylus

The stylus part is simply one file `style.styl`, so we just need this:

{{< highlight javascript >}}
    // Stylus
    stylus: {
      compile: {
        options: {
          compress: false
        },
        files: {
          'css/style.css': 'css/style.styl' // 1:1 compile
        }
      }
    },
{{< / highlight >}}

Do not forget to watch

{{< highlight javascript >}}
    //  Watch Files
    watch: {
      stylus: {
        files: ['css/style.styl'],
        tasks: ['stylus']
      }
    }
{{< / highlight >}}

And load task.

{{< highlight javascript >}}
  grunt.loadNpmTasks('grunt-contrib-stylus');
{{< / highlight >}}

#### Coffeescript

The coffeescript part is also simply one file `script.coffee`.

{{< highlight javascript >}}
    // Coffeescript
    coffee: {
      compile: {
        files: {
          'js/script.js': 'js/script.coffee' // 1:1 compile
        }
      }
    },
{{< / highlight >}}

Also, do not forget to watch

{{< highlight javascript >}}
    //  Watch Files
    watch: {
      coffee: {
        files: ['js/script.coffee'],
        tasks: ['coffee']
      }
    }
{{< / highlight >}}

And also load task.

{{< highlight javascript >}}
  grunt.loadNpmTasks('grunt-contrib-coffee');
{{< / highlight >}}

#### Complete Configuration

Finally we need to register all tasks.

{{< highlight javascript >}}
  // define the tasks
  grunt.registerTask('default', [
    'pug', 'stylus', 'coffee', 'watch'
  ] );
{{< / highlight >}}

![Grunt: Watch All: Pug + Styles + Coffeescript][34-grunt-watch-all]

#### Summary

As a conclusion,
the complete configuration code is shown as below:

{{< highlight javascript >}}
module.exports = function(grunt) {
  // configure the tasks
  let config = {

    // Pug
    pug: {
      compile: {
        options: {
          data: {
            debug: false
          },
          pretty: true,
          filters: {
            'stylus': function(block) {
              return require('stylus').render(block);
            },
            'coffee-script': function(block) {
              return require('coffeescript').compile(block);
            },
          },
        },
        files: [ {
          cwd: "pug",
          src: "*.pug",
          dest: "html",
          expand: true,
          ext: ".html"
        } ]
      }
    },

    // Stylus
    stylus: {
      compile: {
        options: {
          compress: false
        },
        files: {
          'css/style.css': 'css/style.styl' // 1:1 compile
        }
      }
    },

    // Coffeescript
    coffee: {
      compile: {
        files: {
          'js/script.js': 'js/script.coffee' // 1:1 compile
        }
      }
    },

    //  Watch Files
    watch: {
      pug: {
        files: ['pug/**/*'],
        tasks: ['pug'],
        options: {
          interrupt: false,
          spawn: false
        }
      },
      stylus: {
        files: ['css/style.styl'],
        tasks: ['stylus']
      },
      coffee: {
        files: ['js/script.coffee'],
        tasks: ['coffee']
      }
    }

  };

  grunt.initConfig(config);

  // load the tasks
  grunt.loadNpmTasks('grunt-contrib-pug');
  grunt.loadNpmTasks('grunt-contrib-stylus');
  grunt.loadNpmTasks('grunt-contrib-coffee');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // define the tasks
  grunt.registerTask('default', [
    'pug', 'stylus', 'coffee', 'watch'
  ] );

};
{{< / highlight >}}

![Grunt: Vim Gruntfile.js][34-vim-gruntfile-js]

-- -- --

### What's Next?

Consider continue reading [ [Bundler - Webpack - Part One][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}frontend/2020/04/13/tools-bundler-webpack-01/

[03-package-json]:          {{< tutor-css-tools >}}/html-basic/03-template/package.json
[03-gruntfile-js]:          {{< tutor-css-tools >}}/html-basic/03-template/Gruntfile.js

[34-grunt-pug-one]:         {{< assets-frontend >}}/2020/04/34-grunt-pug-one-to-one.png
[34-grunt-pug-filter-error]:{{< assets-frontend >}}/2020/04/34-grunt-pug-filter-error.png
[34-grunt-pug-many]:        {{< assets-frontend >}}/2020/04/34-grunt-pug-many-files.png
[34-grunt-watch]:           {{< assets-frontend >}}/2020/04/34-grunt-pug-watch.png
[34-grunt-watch-all]:       {{< assets-frontend >}}/2020/04/34-grunt-pug-watch-all.png
[34-vim-gruntfile-js]:      {{< assets-frontend >}}/2020/04/34-vim-gruntfile-js.png
[34-vim-stack]:             {{< assets-frontend >}}/2020/04/34-vim-stack.png
[source-example]:           {{< tutor-css-tools >}}/html-basic/03-template/
