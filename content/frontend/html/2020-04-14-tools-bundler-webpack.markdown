---
type   : post
title  : "Bundler - Webpack - Part Two"
date   : 2020-04-14T09:17:35+07:00
slug   : tools-bundler-webpack-02
categories: [frontend]
tags      : [webpack]
keywords  : [bundler]
author : epsi
opengraph:
  image: assets-frontend/2020/04/49-cli-webpack.png

toc    : "toc-2020-04-html"

excerpt:
  Put the whole stuff together using webpack bundler.
  Configuration Topic | HTML Template, Pug, CSS, Stylus.

---

### Preface

> Goal: Put the whole stuff together using webpack bundler.

#### Source Examples

You can obtain source examples here:

* [html-basic/04-webpack][source-example]

-- -- --

### 4: Template - HTML

#### Objective

> Generate `dist/index.html`, from `html/index.html` with injected assets.

#### Directory Tree

Additional file: `html/index.html`.

{{< highlight bash >}}
.
├── css
│   └── style.css
├── dist
│   ├── bundle.js
│   ├── index.html
│   └── style.js
├── html
│   └── index.html
├── js
│   ├── live.js
│   └── script.js
├── package.json
└── webpack.config.js
{{< / highlight >}}

The `webpack` should generate three files:

* `dist/bundle.js`, and
* `dist/style.js`, and
* `dist/index.html`.

#### HTML View

The `html/index.html` does not contain either `js/script.js`,
nor `css/style.css`.

* [gitlab.com/.../html/index.html][04-html-index-html]

{{< highlight html >}}
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Example Template</title>
    <script src="../js/live.js"></script>
  </head>
  <body>
    ...
  </body>
</html>
{{< / highlight >}}

#### package.json

Additional node module `html-webpack-plugin`.

* [gitlab.com/.../package.json][04-package-json].

{{< highlight javascript >}}
  "devDependencies": {
    "webpack": "^4.43.0",
    "webpack-cli": "^3.3.11",
    "css-loader": "^3.5.3",
    "style-loader": "^1.2.1",
    "html-webpack-plugin": "^4.3.0"
  },
{{< / highlight >}}

#### webpack.config.js

* [gitlab.com/.../webpack.config.js][04-webpack-config-js].

{{< highlight javascript >}}
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  mode: 'development',
  entry: {
    bundle: "./js/script.js",
    style: "./css/style.css"
  },
  output: {
    filename: "[name].js",
    path: path.resolve(__dirname, 'dist'),
  },
  module: {
    rules: [
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({template: './html/index.html'})
  ]
};
{{< / highlight >}}

#### Command Line Interface

Just run the `webpack` in terminal

{{< highlight bash >}}
$ webpack
Hash: 20348db9915d290852a2
Version: webpack 4.43.0
Time: 1369ms
Built at: 05/09/2020 1:24:37 AM
     Asset      Size  Chunks             Chunk Names
 bundle.js  4.29 KiB  bundle  [emitted]  bundle
index.html  1.33 KiB          [emitted]  
  style.js  16.9 KiB   style  [emitted]  style
Entrypoint bundle = bundle.js
Entrypoint style = style.js
[./css/style.css] 519 bytes {style} [built]
[./js/script.js] 515 bytes {bundle} [built]
[./node_modules/css-loader/dist/cjs.js!./css/style.css] 1.44 KiB {style} [built]
    + 2 hidden modules
...
{{< / highlight >}}

![Webpack: Generate HTML: Running Webpack in Terminal][44-cli-webpack]

#### View in Browser

Just check if everything goes right.

Also check the source code with `Ctrl+U`,
the assets should be somewhere below the documents.

* [gitlab.com/.../dist/index.html][04-dist-index-html]

{{< highlight html >}}
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Example Template</title>
    <script src="../js/live.js"></script>
  </head>
  <body>
    <div class="container">
      ...
    </div>
  <script src="bundle.js"></script><script src="style.js"></script></body>
</html>
{{< / highlight >}}

#### Plugin in Configuration

We utilize `html-webpack-plugin`.

{{< highlight javascript >}}
const HtmlWebpackPlugin = require('html-webpack-plugin');
{{< / highlight >}}

And use the plugin in configuration:

{{< highlight javascript >}}
  plugins: [
    new HtmlWebpackPlugin({template: './html/index.html'})
  ]
{{< / highlight >}}

I know, it looks magical.

-- -- --

### 5: Template - Pug

#### Objective

> Generate `dist/index.html`, from `pug/alert.pug` with injected assets.

#### Directory Tree

Removed file: `html/index.html`.

Additional file: `pug/alert.pug` and `pug/partials/body.pug`.

{{< highlight bash >}}
.
├── css
│   └── style.css
├── dist
│   ├── bundle.js
│   ├── index.html
│   └── style.js
├── js
│   ├── live.js
│   └── script.js
├── pug
│   ├── alert.pug
│   └── partials
│       └── body.pug
├── package.json
└── webpack.config.js
{{< / highlight >}}

#### Pug View

The `pug/alert.pug` is shown as below:

* [gitlab.com/.../pug/index.pug][05-pug-index-pug]

{{< highlight haskell >}}
doctype html
html(lang="en")

  head
    meta(charset='utf-8')
    title Example Template
    script(src='../js/live.js')

  body
    include partials/body.pug
{{< / highlight >}}

And the `pug/partials/body.pug` is shown as below:

{{< highlight haskell >}}
.container
  h2 Relationship Alerts
  p To close the alerts, click on the X in the upper right corner:
  
  ...
{{< / highlight >}}

#### package.json

Additional node module `pug` and `pug-loader`.

* [gitlab.com/.../package.json][05-package-json].

{{< highlight javascript >}}
  "devDependencies": {
    "webpack": "^4.43.0",
    "webpack-cli": "^3.3.11",
    "css-loader": "^3.5.3",
    "style-loader": "^1.2.1",
    "html-webpack-plugin": "^4.3.0",
    "pug": "^2.0.4",
    "pug-loader": "^2.4.0"
  },
{{< / highlight >}}

#### webpack.config.js

* [gitlab.com/.../webpack.config.js][05-webpack-config-js].

{{< highlight javascript >}}
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  mode: 'development',
  entry: {
    bundle: "./js/script.js",
    style: "./css/style.css"
  },
  output: {
    filename: "[name].js",
    path: path.resolve(__dirname, 'dist'),
  },
  module: {
    rules: [
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.pug$/i,
        use: [
          {
            loader: 'pug-loader',
            options: {
              pretty: true
            }
          }
        ],
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({template: './pug/alert.pug'})
  ]
};
{{< / highlight >}}

#### Command Line Interface

Just run the `webpack` in terminal

{{< highlight bash >}}
$ webpack
Hash: 199cfdd8d2bf018d5f42
Version: webpack 4.43.0
Time: 950ms
Built at: 05/09/2020 1:35:34 AM
     Asset      Size  Chunks             Chunk Names
 bundle.js  4.29 KiB  bundle  [emitted]  bundle
index.html  1.27 KiB          [emitted]  
  style.js  16.9 KiB   style  [emitted]  style
Entrypoint bundle = bundle.js
Entrypoint style = style.js
[./css/style.css] 519 bytes {style} [built]
[./js/script.js] 515 bytes {bundle} [built]
[./node_modules/css-loader/dist/cjs.js!./css/style.css] 1.44 KiB {style} [built]
    + 2 hidden modules
...
{{< / highlight >}}

#### View in Browser

Change the content of `pug` files, run `webpack` again,
and see if there is any changes in `index.html`.

* [gitlab.com/.../dist/index.html][05-dist-index-html]

#### Change in Configuration

The plugin, has been altered into this:

{{< highlight javascript >}}
  plugins: [
    new HtmlWebpackPlugin({template: './pug/alert.pug'})
  ]
{{< / highlight >}}

Remember that the example source name is `alert.pug`, not `index.pug`.
And the generated file is `index.html`, not `alert.html`.
You can figure it out yourself,
how to translate from `alert.pug` to `alert.html`.

#### Loader in Configuration

Also you need rules in modules that use `pug-loader`.

{{< highlight javascript >}}
        test: /\.pug$/i,
        use: [
          {
            loader: 'pug-loader',
            options: {
              pretty: true
            }
          }
        ],
{{< / highlight >}}

-- -- --

### 6: Stylesheet - Extract CSS

Consider go back for a while to split assets.
Now we are going to extract the assets from `dist/style.js`,
into `dist/style.css`.

#### Objective

> Extract stylesheet from `dist/style.js` to `dist/style.css`

#### Directory Tree

Again, we use the manually created `dist/example.html`.

No additional file.
The `dist/style.css` will be generated.

{{< highlight bash >}}
.
├── css
│   └── style.css
├── dist
│   ├── bundle.js
│   ├── example.html
│   ├── style.css
│   └── style.js
├── js
│   ├── live.js
│   └── script.js
├── package.json
└── webpack.config.js
{{< / highlight >}}

#### package.json

Required node module: `mini-css-extract-plugin`.

* [gitlab.com/.../package.json][06-package-json].

{{< highlight javascript >}}
  "devDependencies": {
    "webpack": "^4.43.0",
    "webpack-cli": "^3.3.11",
    "css-loader": "^3.5.3",
    "mini-css-extract-plugin": "^0.9.0"
  },
{{< / highlight >}}

#### webpack.config.js

* [gitlab.com/.../webpack.config.js][06-webpack-config-js].

{{< highlight javascript >}}
const
  path = require('path'),
  MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
  mode: 'development',
  entry: {
    bundle: "./js/script.js",
    style: "./css/style.css"
  },
  output: {
    filename: "[name].js",
    path: path.resolve(__dirname, 'dist'),
  },
  module: {
    rules: [
      {
        test: /\.css$/i,
        use: [MiniCssExtractPlugin.loader, 'css-loader'],
      },
    ],
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: "[name].css"
    })
  ],
};
{{< / highlight >}}

#### Command Line Interface

Just run the `webpack` in terminal

{{< highlight bash >}}
$ webpack
Hash: ffe5e4c29cb5de903e63
Version: webpack 4.43.0
Time: 1203ms
Built at: 05/09/2020 1:45:26 AM
    Asset      Size  Chunks             Chunk Names
bundle.js  4.29 KiB  bundle  [emitted]  bundle
style.css  1.15 KiB   style  [emitted]  style
 style.js  3.83 KiB   style  [emitted]  style
Entrypoint bundle = bundle.js
Entrypoint style = style.css style.js
[./css/style.css] 39 bytes {style} [built]
[./js/script.js] 515 bytes {bundle} [built]
    + 1 hidden module
{{< / highlight >}}

![Webpack: Extract Stylesheet: Running Webpack in Terminal][46-cli-webpack]

#### View in Browser

Just check, if the stylesheet loaded.

* [gitlab.com/.../dist/example.html][06-dist-example-html]:

* [gitlab.com/.../dist/style.css][06-dist-style-css]

#### Plugin in Configuration

We utilize `MiniCssExtractPlugin`.

{{< highlight javascript >}}
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
{{< / highlight >}}

And use it in configuration:

{{< highlight javascript >}}
  plugins: [
    new MiniCssExtractPlugin({
      filename: "[name].css"
    })
  ],
{{< / highlight >}}

Nothing magical, here.
It just use `[name].css`, to get the right filename.

#### Loader in Configuration

Beware of this loader.
The rules utilize `MiniCssExtractPlugin.loader` instead of `style-loader`.
And still using `css-loader`.

{{< highlight javascript >}}
  module: {
    rules: [
      {
        test: /\.css$/i,
        use: [MiniCssExtractPlugin.loader, 'css-loader'],
      },
    ],
  },
{{< / highlight >}}

-- -- --

### 7: Stylesheet - Render Stylus

#### Objective

> Render from `css/stylus.styl`, then extract to `dist/style.css`

#### Directory Tree

Still using the manually created `dist/example.html`.

Additional file: `css/style.styl`. Removed file: `css/style.css`.

From `css/style.styl`, the `dist/style.css` will be generated.

{{< highlight bash >}}
.
├── css
│   └── style.styl
├── dist
│   ├── bundle.js
│   ├── example.html
│   ├── style.css
│   └── style.js
├── js
│   ├── live.js
│   └── script.js
├── package.json
└── webpack.config.js
{{< / highlight >}}

#### Stylesheet

* [gitlab.com/.../css/style.styl][07-css-style-styl]

{{< highlight sass >}}
...
.red
  color: #fff !important
  background-color: #f44336 !important

.yellow
  color: #000 !important
  background-color: #ffeb3b !important
...
{{< / highlight >}}

#### package.json

Additional node module `stylus` and `stylus-loader`.

* [gitlab.com/.../package.json][07-package-json].

{{< highlight javascript >}}
  "devDependencies": {
    "webpack": "^4.43.0",
    "webpack-cli": "^3.3.11",
    "css-loader": "^3.5.3",
    "mini-css-extract-plugin": "^0.9.0",
    "stylus": "^0.54.7",
    "stylus-loader": "^3.0.2"
  },
{{< / highlight >}}

#### webpack.config.js

* [gitlab.com/.../webpack.config.js][07-webpack-config-js].

{{< highlight javascript >}}
const
  path = require('path'),
  MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
  mode: 'development',
  entry: {
    bundle: "./js/script.js",
    style: "./css/style.styl"
  },
  output: {
    filename: "[name].js",
    path: path.resolve(__dirname, 'dist'),
  },
  module: {
    rules: [
      {
        test: /\.styl$/i,
        use: [
          MiniCssExtractPlugin.loader, 
          'css-loader', 
          'stylus-loader'
        ],
      },
    ],
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: "[name].css"
    })
  ],
  
};
{{< / highlight >}}

#### Command Line Interface

Just run the `webpack` in terminal

{{< highlight bash >}}
$ webpack
Hash: f9b55bb4cf2c2b2cd2bc
Version: webpack 4.43.0
Time: 2467ms
Built at: 05/09/2020 2:08:07 AM
    Asset      Size  Chunks             Chunk Names
bundle.js  4.29 KiB  bundle  [emitted]  bundle
style.css  1.15 KiB   style  [emitted]  style
 style.js  3.84 KiB   style  [emitted]  style
Entrypoint bundle = bundle.js
Entrypoint style = style.css style.js
[./css/style.styl] 39 bytes {style} [built]
[./js/script.js] 515 bytes {bundle} [built]
    + 1 hidden module
{{< / highlight >}}

![Webpack: Render Stylus: Running Webpack in Terminal][47-cli-webpack]

#### View in Browser

Just check, if the stylesheet loaded.

Change the stylesheet in `css/style.styl` a bit,
and see if the change reflected in genereted `dist/style.css`.

* [gitlab.com/.../dist/style.css][07-dist-style-css]

#### Entry Point in Configuration

The entry point, has been altered into this:

{{< highlight javascript >}}
  entry: {
    bundle: "./js/script.js",
    style: "./css/style.styl"
  },
{{< / highlight >}}

instead of this:


{{< highlight javascript >}}
  entry: {
    bundle: "./js/script.js",
    style: "./css/style.css"
  },
{{< / highlight >}}

#### Loader in Configuration

Also we have three loader in `styl` rules:

{{< highlight javascript >}}
  module: {
    rules: [
      {
        test: /\.styl$/i,
        use: [
          MiniCssExtractPlugin.loader, 
          'css-loader', 
          'stylus-loader'
        ],
      },
    ],
  },
{{< / highlight >}}

-- -- --

### What's Next?

Consider continue reading [ [Bundler - Webpack - Part Three][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}frontend/2020/04/15/tools-bundler-webpack-03/

[44-cli-webpack]:           {{< assets-frontend >}}/2020/04/44-cli-webpack.png
[46-cli-webpack]:           {{< assets-frontend >}}/2020/04/46-cli-webpack.png
[47-cli-webpack]:           {{< assets-frontend >}}/2020/04/47-cli-webpack.png

[source-example]:           {{< tutor-css-tools >}}/html-basic/04-webpack/

[04-package-json]:          {{< tutor-css-tools >}}/html-basic/04-webpack/11-html/package.json
[04-webpack-config-js]:     {{< tutor-css-tools >}}/html-basic/04-webpack/11-html/webpack.config.js
[04-html-index-html]:       {{< tutor-css-tools >}}/html-basic/04-webpack/11-html/html/index.html
[04-dist-index-html]:       {{< tutor-css-tools >}}/html-basic/04-webpack/11-html/dist/index.html

[05-package-json]:          {{< tutor-css-tools >}}/html-basic/04-webpack/12-pug/package.json
[05-webpack-config-js]:     {{< tutor-css-tools >}}/html-basic/04-webpack/12-pug/webpack.config.js
[05-pug-alert-pug]:         {{< tutor-css-tools >}}/html-basic/04-webpack/12-pug/pug/alert.pug
[05-dist-index-html]:       {{< tutor-css-tools >}}/html-basic/04-webpack/12-pug/dist/index.html

[06-package-json]:          {{< tutor-css-tools >}}/html-basic/04-webpack/21-css/package.json
[06-webpack-config-js]:     {{< tutor-css-tools >}}/html-basic/04-webpack/21-css/webpack.config.js
[06-dist-example-html]:     {{< tutor-css-tools >}}/html-basic/04-webpack/21-css/dist/example.html
[06-css-style-css]:         {{< tutor-css-tools >}}/html-basic/04-webpack/21-css/css/style.css
[06-dist-style-css]:        {{< tutor-css-tools >}}/html-basic/04-webpack/21-css/dist/style.css

[07-package-json]:          {{< tutor-css-tools >}}/html-basic/04-webpack/22-stylus/package.json
[07-webpack-config-js]:     {{< tutor-css-tools >}}/html-basic/04-webpack/22-stylus/webpack.config.js
[07-dist-example-html]:     {{< tutor-css-tools >}}/html-basic/04-webpack/22-stylus/dist/example.html
[07-css-style-styl]:        {{< tutor-css-tools >}}/html-basic/04-webpack/22-stylus/css/style.styl
[07-dist-style-css]:        {{< tutor-css-tools >}}/html-basic/04-webpack/22-stylus/dist/style.css
