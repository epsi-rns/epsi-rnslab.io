---
type   : post
title  : "Bundler - Webpack - Part Three"
date   : 2020-04-15T09:17:35+07:00
slug   : tools-bundler-webpack-03
categories: [frontend]
tags      : [webpack]
keywords  : [bundler]
author : epsi
opengraph:
  image: assets-frontend/2020/04/49-cli-webpack.png

toc    : "toc-2020-04-html"

excerpt:
  Put the whole stuff together using webpack bundler.
  Configuration Topic | Coffescript, Complete Configuration.

---

### Preface

> Goal: Put the whole stuff together using webpack bundler.

#### Source Examples

You can obtain source examples here:

* [html-basic/04-webpack][source-example]

-- -- --

### 8: Javascript - Compile Coffeescript

Again, consider go back for a while to split assets.
Now we are going to bundle a coffeescript assets.

#### Objective

> Bundle Javascript to `dist/bundle.js` from `js/script.coffee`

#### Directory Tree

Using `dist/example.html` as usual.

Additional file: `js/script.coffee`.

{{< highlight bash >}}
.
├── css
│   └── style.css
├── dist
│   ├── bundle.js
│   └── example.html
├── js
│   ├── live.js
│   └── script.coffee
├── package.json
└── webpack.config.js
{{< / highlight >}}

#### HTML View

The `html/index.html` contain the `dist/bundle.js`.

* [gitlab.com/.../dist/example.html][08-dist-example-html]

{{< highlight html >}}
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Example Webpack</title>
    <script src="./bundle.js"></script>
    <script src="../js/live.js"></script>
  </head>
  <body>
    ...
  </body>
</html>
{{< / highlight >}}

#### Coffeescript

* [gitlab.com/.../jss/script.coffee][08-js-script-coffee]

{{< highlight coffeescript >}}
document.addEventListener 'DOMContentLoaded', (event) ->
  alertButtons = document.getElementsByClassName('dismissable')
  i = 0
  while i < alertButtons.length

    alertButtons[i].onclick = ->
      @parentElement.style.display = 'none'
      console.log 'Close Button. Element ' + @parentElement.id + ' dismissed'
      false

    i++
  return
{{< / highlight >}}

#### package.json

Additional node module `coffee-script` and `coffee-loader`.

* [gitlab.com/.../package.json][08-package-json].

{{< highlight javascript >}}
  "devDependencies": {
    "webpack": "^4.43.0",
    "webpack-cli": "^3.3.11",
    "css-loader": "^3.5.3",
    "style-loader": "^1.2.1",
    "coffee-script": "^1.12.7",
    "coffee-loader": "^0.7.3"
  },
{{< / highlight >}}

#### webpack.config.js

* [gitlab.com/.../webpack.config.js][08-webpack-config-js].

{{< highlight javascript >}}
const path = require('path');

module.exports = {
  mode: 'development',
  entry: {
    bundle: [
      "./js/script.coffee",
      "./css/style.css",
    ],
  },
  output: {
    filename: "[name].js",
    path: path.resolve(__dirname, 'dist'),
  },
  module: {
    rules: [
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.coffee$/,
        use: [ 'coffee-loader' ]
      }
    ],
  }  
};
{{< / highlight >}}

#### Command Line Interface

Just run the `webpack` in terminal

{{< highlight bash >}}
$ webpack
Hash: b7252a974bbe17042669
Version: webpack 4.43.0
Time: 1705ms
Built at: 05/09/2020 2:17:19 AM
    Asset      Size  Chunks             Chunk Names
bundle.js  18.1 KiB  bundle  [emitted]  bundle
Entrypoint bundle = bundle.js
[0] multi ./js/script.coffee ./css/style.css 40 bytes {bundle} [built]
[./css/style.css] 519 bytes {bundle} [built]
[./js/script.coffee] 417 bytes {bundle} [built]
[./node_modules/css-loader/dist/cjs.js!./css/style.css] 1.44 KiB {bundle} [built]
    + 2 hidden modules
{{< / highlight >}}

![Webpack: Compile Coffeescript: Running Webpack in Terminal][48-cli-webpack]

There is only `bundle.js` generated.

#### View in Browser

Check again, if the dismissable button works.

* [gitlab.com/.../dist/bundle.js][08-dist-bundle-js]

#### Entry Point in Configuration

The entry point, has been altered into this:

{{< highlight javascript >}}
    bundle: [
      "./js/script.coffee",
      "./css/style.css",
    ],
{{< / highlight >}}

instead of this:

{{< highlight javascript >}}
    bundle: [
      "./js/script.js",
      "./css/style.css",
    ],
{{< / highlight >}}

#### Loader in Configuration

Also we have this simple `coffee-loader` rules:

{{< highlight javascript >}}
      {
        test: /\.coffee$/,
        use: [ 'coffee-loader' ]
      }
{{< / highlight >}}

-- -- --

### 9: Tools: Dev Server

Webpack is equipped by a web server.
Instead of browsersync or other simple webserver,
you may use `webpack-dev-server`.

#### Objective

> Serve Development Site, along with livereload.js.

#### Official Documentation

You should read this first.

* [DevServer](https://webpack.js.org/configuration/dev-server/)

#### Directory Tree

We are going to use, our last `4: Template - HTML` example.
But this time, without `live.js` in `html/index.html`.

Removed file: `js/live.js`.

{{< highlight bash >}}
.
├── css
│   └── style.css
├── dist
│   ├── bundle.js
│   ├── index.html
│   └── style.js
├── html
│   └── index.html
├── js
│   └── script.js
├── package.json
└── webpack.config.js
{{< / highlight >}}

#### package.json

Additional node module `coffee-script` and `coffee-loader`.

* [gitlab.com/.../package.json][08-package-json].

{{< highlight javascript >}}
  "devDependencies": {
    "webpack": "^4.43.0",
    "webpack-cli": "^3.3.11",
    "webpack-dev-server": "^3.11.0"
  },
{{< / highlight >}}

#### webpack.config.js

* [gitlab.com/.../webpack.config.js][09-webpack-config-js].

{{< highlight javascript >}}
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin'); //installed via npm

module.exports = {
  mode: 'development',
  entry: {
    bundle: "./js/script.js",
    style: "./css/style.css"
  },
  output: {
    filename: "[name].js",
    path: path.resolve(__dirname, 'dist'),
  },
  module: {
    rules: [
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({template: './html/index.html'})
  ],
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    port: 9000
  }
};
{{< / highlight >}}

#### Command Line Interface

Just run the `webpack-dev-server` in terminal

{{< highlight bash >}}
$ webpack-dev-server
ℹ ｢wds｣: Project is running at http://localhost:9000/
ℹ ｢wds｣: webpack output is served from /
ℹ ｢wds｣: Content not from webpack is served from /home/epsi/Documents/html-basic/04-webpack/51-all/dist
ℹ ｢wdm｣: Hash: 6edb011f8c5c512539bb
Version: webpack 4.43.0
Time: 7674ms
{{< / highlight >}}

![Webpack Dev Server: Running in Terminal][49-cli-webdevserv]

#### Livereload

With webpack, we can utilize `livereload.js` instead of `live.js`.
While `live.js` attach `javascript` in source code,
`livereload` utilize `websocket protocol`.

You can have a look of what is happening on the inside,
by using inspect element in your browser.

![LiveReload: Websocket in Inspect Element][49-websocket]

You should find `websocket` stuff over there.

-- -- --

### 10: Putting All Stuff Together

As a summmary, we are going to complete this example,
by put a bunch of configuration into one.

#### Objective

> Putting Together, Pug + Stylus + Coffeescript

#### Directory Tree

This should be self explanatory.
A bunch of code, in a blender.

{{< highlight bash >}}
.
├── css
│   └── style.styl
├── dist
│   ├── bundle.js
│   ├── index.html
│   ├── style.css
│   └── style.js
├── js
│   └── script.coffee
├── pug
│   ├── alert.pug
│   └── partials
│       └── body.pug
├── package.json
└── webpack.config.js
{{< / highlight >}}

![Webpack: Nerdtree configuration][41-nerdtree-webpack]

#### package.json

Whoaaa, what a long dependency. We finally made it!

* [gitlab.com/.../package.json][09-package-json].

{{< highlight javascript >}}
  "devDependencies": {
    "webpack": "^4.43.0",
    "webpack-cli": "^3.3.11",
    "webpack-dev-server": "^3.11.0",
    "html-webpack-plugin": "^4.3.0",
    "mini-css-extract-plugin": "^0.9.0",
    "css-loader": "^3.5.3",
    "style-loader": "^1.2.1",
    "pug": "^2.0.4",
    "pug-loader": "^2.4.0",
    "stylus": "^0.54.7",
    "stylus-loader": "^3.0.2",
    "coffee-script": "^1.12.7",
    "coffee-loader": "^0.7.3"
  },
{{< / highlight >}}

#### webpack.config.js

> Tips: Do not get initimidated by long config, once you understand, you can reuse in other project.

And you have a cool configuration too.

* [gitlab.com/.../webpack.config.js][09-webpack-config-js].

{{< highlight javascript >}}
const
  path = require('path'),
  HtmlWebpackPlugin = require('html-webpack-plugin'),
  MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  mode: 'development',
  entry: {
    bundle: "./js/script.coffee",
    style: "./css/style.styl"
  },
  output: {
    filename: "[name].js",
    path: path.resolve(__dirname, 'dist'),
  },
  module: {
    rules: [
      {
        test: /\.pug$/i,
        use: [
          {
            loader: 'pug-loader',
            options: {
              pretty: true
            }
          }
        ],
      },
      {
        test: /\.styl$/i,
        use: [
          MiniCssExtractPlugin.loader, 
          'css-loader', 
          'stylus-loader'],
      },
      {
        test: /\.coffee$/,
        use: [ 'coffee-loader' ]
      }
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "./pug/alert.pug"
    }),
    new MiniCssExtractPlugin({
      filename: "[name].css"
    })
  ],
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    port: 9000
  }
};
{{< / highlight >}}

#### Command Line Interface

Just run the `webpack` in terminal

{{< highlight bash >}}
$ webpack
Hash: 9f11c9d00cf928503b36
Version: webpack 4.43.0
Time: 5976ms
Built at: 05/09/2020 2:25:36 AM
     Asset      Size  Chunks             Chunk Names
 bundle.js  4.21 KiB  bundle  [emitted]  bundle
index.html  1.31 KiB          [emitted]  
 style.css  1.15 KiB   style  [emitted]  style
  style.js  3.84 KiB   style  [emitted]  style
Entrypoint bundle = bundle.js
Entrypoint style = style.css style.js
[./css/style.styl] 39 bytes {style} [built]
[./js/script.coffee] 417 bytes {bundle} [built]
    + 1 hidden module

{{< / highlight >}}

![Webpack: Putting it all together: Running Webpack in Terminal][49-cli-webpack]

#### Directory Tree Result

You can see the `dist` directory content here:

![Webpack: All: Directory Tree][49-directory-tree]

#### View in Browser

It is a good time to check again, if everything works fine in browser.

* [gitlab.com/.../dist/index.html][09-dist-index-html]

#### Change in Configuration

The `webpack` configuration is pretty self-explanatory.
All has been discussed, step by step.

I think that's all about `webpack`.
Now you are ready to generate your static site,
without any framework, nor SSG, nor backend.

-- -- --

### What's Next?

Consider continue reading [ [Bundler - Rollup - Part One][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}frontend/2020/04/17/tools-bundler-rollup-01/

[41-nerdtree-webpack]:      {{< assets-frontend >}}/2020/04/41-nerdtree-webpack.png
[48-cli-webpack]:           {{< assets-frontend >}}/2020/04/48-cli-webpack.png
[49-cli-webdevserv]:        {{< assets-frontend >}}/2020/04/49-cli-webdevserver.png
[49-websocket]:             {{< assets-frontend >}}/2020/04/49-livereload-network.png
[49-cli-webpack]:           {{< assets-frontend >}}/2020/04/49-cli-webpack.png
[49-directory-tree]:        {{< assets-frontend >}}/2020/04/49-directory-tree.png

[source-example]:           {{< tutor-css-tools >}}/html-basic/04-webpack/

[08-package-json]:          {{< tutor-css-tools >}}/html-basic/04-webpack/31-coffee/package.json
[08-webpack-config-js]:     {{< tutor-css-tools >}}/html-basic/04-webpack/31-coffee/webpack.config.js
[08-dist-example-html]:     {{< tutor-css-tools >}}/html-basic/04-webpack/31-coffee/dist/example.html
[08-js-script-coffee]:      {{< tutor-css-tools >}}/html-basic/04-webpack/31-coffee/js/script.coffee
[08-dist-bundle-js]:        {{< tutor-css-tools >}}/html-basic/04-webpack/31-coffee/dist/bundle.js

[09-package-json]:          {{< tutor-css-tools >}}/html-basic/04-webpack/41-server/package.json
[09-webpack-config-js]:     {{< tutor-css-tools >}}/html-basic/04-webpack/41-server/webpack.config.js
[09-dist-index-html]:       {{< tutor-css-tools >}}/html-basic/04-webpack/41-server/html/index.html

[10-package-json]:          {{< tutor-css-tools >}}/html-basic/04-webpack/51-all/package.json
[10-webpack-config-js]:     {{< tutor-css-tools >}}/html-basic/04-webpack/51-all/webpack.config.js
[10-dist-index-html]:       {{< tutor-css-tools >}}/html-basic/04-webpack/51-all/dist/index.html
