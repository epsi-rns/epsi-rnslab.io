---
type   : post
title  : "Bundler - Rollup - Part Two"
date   : 2020-04-18T09:17:35+07:00
slug   : tools-bundler-rollup-02
categories: [frontend]
tags      : [rollup]
keywords  : [bundler]
author : epsi
opengraph:
  image: assets-frontend/2020/04/58-cli-rollup-watch.png

toc    : "toc-2020-04-html"

excerpt:
  Put the whole stuff together using rollup bundler.
  Configuration Topic | Stylus, Coffescript, Dev Server, Complete Configuration.

---

### Preface

> Goal: Put the whole stuff together using rollup bundler.

-- -- --

### 5: Template - Pug - Skipped

I found the `rollup-plugin-pug` works well.
It can render a `pug` file into html,
and put the `html` into `build/js/script.js`.
But I still do not know,
how to extract the string into external html file.

#### Objective

> Generate `build/index.html`, from `src/pug/alert.pug`.

#### Result

Since I failed. I skip this part.
But don't worry, the rest of this article works well.
And I think you can continue safely.

-- -- --

### 6: Stylesheet - Render Stylus

#### Objective

> Generate output from the `src/css/style.styl` stylesheet into `build/css/style.css`.

#### Directory Tree

* Additional artefact: `src/css/style.styl`.

* Removed artefact: `src/css/style.css`.

{{< highlight bash >}}
├── build
│   ├── css
│   │   └── style.css
│   ├── index.html
│   └── js
│       └── script.js
├── package.json
├── rollup.config.js
└── src
    ├── css
    │   └── style.styl
    ├── entry.js
    ├── html
    │   └── alert.html
    └── js
        └── script.js
{{< / highlight >}}

#### Example Stylus

Th example `src/css/style.js` contain this file:

* [gitlab.com/.../src/css/style.styl][06-src-css-style-styl]

{{< highlight sass >}}
...
.red
  color: #fff !important
  background-color: #f44336 !important

.yellow
  color: #000 !important
  background-color: #ffeb3b !important
...
{{< / highlight >}}

#### package.json

Additional rollup plugin `rollup-plugin-stylus-compiler`.

* [gitlab.com/.../package.json][06-package-json]

Do not forget to install `stylus` first.

#### rollup.config.js

The configuration has only one entry point: the `src/entry.js`.

* [gitlab.com/.../rollup.config.js][06-rollup-config-js].

{{< highlight javascript >}}
import html from 'rollup-plugin-generate-html-template';
import css from 'rollup-plugin-css-only';
import stylus from 'rollup-plugin-stylus-compiler';

export default {
  input: 'src/entry.js',
  output: { 
    file: 'build/js/script.js',
    format: 'iife'
  },
  plugins: [
    stylus(),
    css({ 
      output: 'build/css/style.css' 
    }),
    html({
      template: 'src/html/alert.html',
      target: 'build/index.html',
      inject: 'head',
      externals: [
        { type: 'js', file: "js/script.js", pos: 'before' }
      ]
    })
  ]
}
{{< / highlight >}}

#### entry.js

To make life easier, I make a file named `entry,js`,
with content as below code:

* [gitlab.com/.../src/entry.js][06-src-entry-js]

{{< highlight javascript >}}
import './js/script.js'
import './css/style.styl'
{{< / highlight >}}

You can name the file anything you like,
as long as it is javascript.

The `rollup-plugin-stylus-compiler` enable the javacript,
to understand the `stylus` file format to be imported.

#### Command Line Interface

Just run the `rollup` in terminal

{{< highlight bash >}}
rollup v2.9.0
bundles src/entry.js → build/js/script.js...
created build/js/script.js in 156ms

[2020-05-12 02:44:44] waiting for changes..
{{< / highlight >}}

Again, still the very same output.

#### Result

The `rollup` generate `build/css/style.css`,
that rendered from `src/css/style.styl`.

* [gitlab.com/.../build/css/style.css][62-build-css-style-css]

#### Plugin in Configuration

Based on `rollup-plugin-stylus-compiler`.

{{< highlight javascript >}}
  plugins: [
    stylus(),
    ...
  ]
{{< / highlight >}}

-- -- --

### 7: Javascript - Compile Coffeescript

#### Objective

> Bundle Javascript from `src/js/script.coffee` to `build/js/script.js`.

#### Directory Tree

* Additional artefact: `src/js/script.coffee`.

* Removed artefact: `src/jss/script.js`.

{{< highlight bash >}}
├── build
│   ├── css
│   │   └── style.css
│   ├── index.html
│   └── js
│       └── script.js
├── package.json
├── rollup.config.js
└── src
    ├── css
    │   └── style.css
    ├── entry.js
    ├── html
    │   └── alert.html
    └── js
        └── script.coffee
{{< / highlight >}}

#### Example Coffeescript

Th example `src/css/style.js` contain this file:

* [gitlab.com/.../src/js/script.cofee][07-src-js-script-coffee]

{{< highlight python >}}
document.addEventListener 'DOMContentLoaded', (event) ->
  alertButtons = document.getElementsByClassName('dismissable')
  i = 0
  while i < alertButtons.length

    alertButtons[i].onclick = ->
      @parentElement.style.display = 'none'
      console.log 'Close Button. Element ' + @parentElement.id + ' dismissed'
      false

    i++
  return
{{< / highlight >}}

#### package.json

Additional rollup plugin `rollup-plugin-coffee-script`.

* [gitlab.com/.../package.json][07-package-json]

Do not forget to install `coffeescript` first.

#### rollup.config.js

The configuration has only one entry point: the `src/entry.js`.

* [gitlab.com/.../rollup.config.js][07-rollup-config-js].

{{< highlight javascript >}}
import html from 'rollup-plugin-generate-html-template';
import css from 'rollup-plugin-css-only';
import coffeescript from 'rollup-plugin-coffee-script';

export default {
  input: 'src/entry.js',
  output: { 
    file: 'build/js/script.js',
    format: 'iife'
  },
  plugins: [
    coffeescript(),
    css({ 
      output: 'build/css/style.css' 
    }),
    html({
      template: 'src/html/alert.html',
      target: 'build/index.html',
      inject: 'head',
      externals: [
        { type: 'js', file: "js/script.js", pos: 'before' }
      ]
    })
  ]
}
{{< / highlight >}}

#### entry.js

To make life easier, I make a file named `entry,js`,
with content as below code:

* [gitlab.com/.../src/entry.js][07-src-entry-js]

{{< highlight javascript >}}
import './js/script.coffee'
import './css/style.css'
{{< / highlight >}}

You can name the file anything you like,
as long as it is javascript.

The `rollup-plugin-coffee-script` enable the javacript,
to understand the `coffeescript` file format to be imported.

#### Command Line Interface

Just run the `rollup` in terminal

{{< highlight bash >}}
rollup v2.9.0
bundles src/entry.js → build/js/script.js...
created build/js/script.js in 114ms

[2020-05-12 02:55:02] waiting for changes...
{{< / highlight >}}

Over and over, still the very same output.

#### Result

The `rollup` generate `build/js/script.js`,
that compiled from `src/jss/script.coffee`.

* [gitlab.com/.../build/js/script.js][07-build-js-script-js]

#### Plugin in Configuration

Based on `rollup-plugin-coffee-script`.

{{< highlight javascript >}}
  plugins: [
    coffeescript(),
    ...
  ]
{{< / highlight >}}

-- -- --

### 8: Tools: Dev Server

Rollup is equipped by a web server.
Instead of browsersync or other simple webserver,
you may use `rollup-plugin-serve`.

#### Objective

> Serve Development Site, along with livereload.js.

#### Directory Tree

We are going to use, our last `3: Template - HTML` example.

{{< highlight bash >}}
├── build
│   ├── css
│   │   └── style.css
│   ├── index.html
│   └── js
│       └── script.js
├── package.json
├── rollup.config.js
└── src
    ├── css
    │   └── style.css
    ├── entry.js
    ├── html
    │   └── alert.html
    └── js
        └── script.js
{{< / highlight >}}

#### package.json

Additional rollup plugin `rollup-plugin-serve`.
And optionally plugin `rollup-plugin-livereload`.

* [gitlab.com/.../package.json][08-package-json]

#### rollup.config.js

The configuration has only one entry point: the `src/entry.js`.

* [gitlab.com/.../rollup.config.js][08-rollup-config-js].

{{< highlight javascript >}}
import html from 'rollup-plugin-generate-html-template';
import css from 'rollup-plugin-css-only';
import serve from 'rollup-plugin-serve';
import livereload from 'rollup-plugin-livereload'

export default {
  input: 'src/entry.js',
  output: { 
    file: 'build/js/script.js',
    format: 'iife'
  },
  plugins: [
    css({ 
      output: 'build/css/style.css' 
    }),
    html({
      template: 'src/html/alert.html',
      target: 'build/index.html',
      inject: 'head',
      externals: [
        { type: 'js', file: "js/script.js", pos: 'before' },
        { type: 'css', file: "css/style.css", pos: 'before' }
      ]
    }),
    serve('build'),
    livereload({
      watch: ['dist', 'src']
    })
  ]
}
{{< / highlight >}}

#### Command Line Interface

Just run the `rollup` in terminal

{{< highlight bash >}}
rollup v2.9.0
bundles src/entry.js → build/js/script.js...
http://localhost:10001 -> /home/epsi/Documents/html-basic/05-rollup/08-server/build
LiveReload enabled
created build/js/script.js in 144ms

[2020-05-12 03:56:43] waiting for changes...
{{< / highlight >}}

Finally, comes up something different.

![Rollup: Stylesheet: Running Rollpack with Serve and LiveReload][58-cli-rollup]

#### Plugin in Configuration

Add both plugin:

{{< highlight javascript >}}
import serve from 'rollup-plugin-serve';
import livereload from 'rollup-plugin-livereload'
{{< / highlight >}}

And configure:

{{< highlight javascript >}}
  plugins: [
    ...
    serve('build'),
    livereload({
      watch: ['dist', 'src']
    })
  ]
{{< / highlight >}}

-- -- --

### 9: Putting All Stuff Together

#### Objective

> Putting Together, HTML + Stylus + Coffeescript

#### Directory Tree

{{< highlight bash >}}
├── build
│   ├── css
│   │   └── style.css
│   ├── index.html
│   └── js
│       └── script.js
├── package.json
├── rollup.config.js
└── src
    ├── css
    │   └── style.styl
    ├── entry.js
    ├── html
    │   └── alert.html
    └── js
        └── script.coffee
{{< / highlight >}}

![Rollup: Nerdtree configuration][51-nerdtree-rollup]

#### package.json

We finally made this long dependency

* [gitlab.com/.../package.json][09-package-json]

{{< highlight javascript >}}
  "devDependencies": {
    "rollup": "^2.9.0",
    "rollup-plugin-css-only": "^2.0.0",
    "rollup-plugin-generate-html-template": "^1.6.1",
    "rollup-plugin-livereload": "^1.3.0",
    "rollup-plugin-serve": "^1.0.1",
    "coffeescript": "^2.5.1",
    "rollup-plugin-coffee-script": "^2.0.0",
    "rollup-plugin-stylus-compiler": "^1.0.1"
  }
{{< / highlight >}}

#### rollup.config.js

Still using the only one entry point: the `src/entry.js`.

* [gitlab.com/.../rollup.config.js][09-rollup-config-js].

{{< highlight javascript >}}
import serve from 'rollup-plugin-serve';
import livereload from 'rollup-plugin-livereload'

import html from 'rollup-plugin-generate-html-template';
import css from 'rollup-plugin-css-only';

import stylus from 'rollup-plugin-stylus-compiler';
import coffeescript from 'rollup-plugin-coffee-script';

export default {
  input: 'src/entry.js',
  output: { 
    file: 'build/js/script.js',
    format: 'iife'
  },
  plugins: [
    stylus(),
    coffeescript(),
    css({ 
      output: 'build/css/style.css' 
    }),
    html({
      template: 'src/html/alert.html',
      target: 'build/index.html',
      inject: 'head',
      externals: [
        { type: 'js', file: "js/script.js", pos: 'before' }
      ]
    }),
    serve('build'),
    livereload({
      watch: ['dist', 'src']
    })
  ]
}
{{< / highlight >}}

#### Command Line Interface

Just run the `rollup` in terminal

{{< highlight bash >}}
rollup v2.9.0
bundles src/entry.js → build/js/script.js...
http://localhost:10001 -> /home/epsi/Documents/html-basic/05-rollup/09-all/build
LiveReload enabled
created build/js/script.js in 317ms

[2020-05-12 04:17:38] waiting for changes...
{{< / highlight >}}

The same as previous screenshot.

#### Directory Tree Result

You can see the `build` directory content here:

![Rollup: All: Directory Tree][59-directory-tree]

#### View in Browser

It is a good time to check again, if everything works fine in browser.

* [gitlab.com/.../build/index.html][09-build-index-html]

#### Change in Configuration

The `rollup` configuration is pretty self-explanatory.
All has been discussed, step by step.

I think that's all about `rollup`.
Now you are ready to generate your static site,
without any framework, nor SSG, nor backend.

-- -- --

### What's Next?

Consider continue reading [ [Bundler - Parcel][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}frontend/2020/04/19/tools-bundler-parcel/

[51-nerdtree-rollup]:       {{< assets-frontend >}}/2020/04/51-nerdtree-rollup.png
[58-cli-rollup]:            {{< assets-frontend >}}/2020/04/58-cli-rollup-watch.png
[59-directory-tree]:        {{< assets-frontend >}}/2020/04/59-directory-tree.png

[06-package-json]:          {{< tutor-css-tools >}}/html-basic/05-rollup/06-stylus/package.json
[06-rollup-config-js]:      {{< tutor-css-tools >}}/html-basic/05-rollup/06-stylus/rollup.config.js
[06-src-entry-js]:          {{< tutor-css-tools >}}/html-basic/05-rollup/06-stylus/src/entry.js
[06-src-css-style-styl]:    {{< tutor-css-tools >}}/html-basic/05-rollup/06-stylus/src/css/style.styl
[62-build-css-style-css]:   {{< tutor-css-tools >}}/html-basic/05-rollup/06-stylus/build/css/style.css

[07-package-json]:          {{< tutor-css-tools >}}/html-basic/05-rollup/07-coffee/package.json
[07-rollup-config-js]:      {{< tutor-css-tools >}}/html-basic/05-rollup/07-coffee/rollup.config.js
[07-src-entry-js]:          {{< tutor-css-tools >}}/html-basic/05-rollup/07-coffee/src/entry.js
[07-src-js-script-coffee]:  {{< tutor-css-tools >}}/html-basic/05-rollup/07-coffee/src/js/script.coffee
[07-build-js-script-js]:    {{< tutor-css-tools >}}/html-basic/05-rollup/07-coffee/build/js/script.js

[08-package-json]:          {{< tutor-css-tools >}}/html-basic/05-rollup/08-server/package.json
[08-rollup-config-js]:      {{< tutor-css-tools >}}/html-basic/05-rollup/08-server/rollup.config.js

[09-package-json]:          {{< tutor-css-tools >}}/html-basic/05-rollup/09-all/package.json
[09-rollup-config-js]:      {{< tutor-css-tools >}}/html-basic/05-rollup/09-all/rollup.config.js
[09-build-index-html]:      {{< tutor-css-tools >}}/html-basic/05-rollup/09-all/build/index.html
