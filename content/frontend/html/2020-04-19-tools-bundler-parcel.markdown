---
type   : post
title  : "Bundler - Parcel"
date   : 2020-04-19T09:17:35+07:00
slug   : tools-bundler-parcel
categories: [frontend]
tags      : [parcel]
keywords  : [bundler]
author : epsi
opengraph:
  image: assets-frontend/2020/04/62-build-alert-html.png

toc    : "toc-2020-04-html"

excerpt:
  Put the whole stuff together using parcel bundler.
  Case | Pug, Stylus, Coffescript.

---

### Preface

> Goal: Put the whole stuff together using parcel bundler.

We have already discussed a `webpack` example,
and then `rollup` example, both targeting modern web development.
There is also a popular bundler called `parcel`.

In contrast with long config `webpack`,
this `parcel` thing can have zero configuration.

#### Source Examples

You can obtain source examples here:

* [html-basic/06-parcel][source-example]

-- -- --

#### Example: In a Nutshell

> Parcel is also a bundler, without complex configuration

#### How does it looks like?

Supposed that you have these source assets:

{{< highlight bash >}}
src
├── assets.js
├── css
│   ├── button.css
│   ├── color.css
│   └── main.css
├── html
│   └── alert.html
└── js
    └── script.js
{{< / highlight >}}

All the sources, including `html`, `css`, and `js`,
will be bundled into one javascript, one stylesheet,
and one html document.

{{< highlight bash >}}
dist
├── index.html
├── assets.2158d4e5.css
└── assets.2158d4e5.js
{{< / highlight >}}

Very similar with webpack ,and rollpack.

I hope this explains 🙏🏽.

#### Official Documentation

You should read this first.

* [Getting Started](https://parceljs.org/getting_started.html)

This article is useless if you never touch the official documentation.

#### Difference

`parcel` enable zero configuration.
Command line configuration can be stored in `package.json`.

#### Learn by Example

This article use previous example,
converted to `parcel` configuration,
explained step by step.

-- -- --

#### 1: Plain: HTML + CSS + JS

Consider start with simple assets.

#### Objective

> First time parcel.

#### Directory Tree

Consider this source directory below:

{{< highlight bash >}}
└── src
    ├── css
    │   └── style.css
    ├── html
    │   └── index.html
    └── js
        └── script.js
{{< / highlight >}}

#### Example Assets

The assets are:

* [gitlab.com/.../src/js/script.js][01-src-js-script-js]

* [gitlab.com/.../src/css/style.css][01-src-css-style-css]

* [gitlab.com/.../src/html/index.html][01-src-html-index-html]

While the `src/html/index.html` contain this header:

{{< highlight html >}}
  <head>
    <meta charset="utf-8">
    <title>Example Plain</title>
    <link href="../css/style.css" rel="stylesheet" type="text/css">
    <script src="../js/script.js"></script>
  </head>
{{< / highlight >}}

#### package.json

We can setup `package.json` as below:

* [gitlab.com/.../package.json][01-package-json].

{{< highlight javascript >}}
  "scripts": {
    "dev": "parcel src/html/index.html",
    "build": "parcel build src/html/index.html"
  },
  "devDependencies": {
    "parcel-bundler": "^1.12.4"
  }
{{< / highlight >}}

#### Command Line Interface

`parcel` utilize command line argument,
instead of configuration.
Therefore, long command line is pretty common.

You can either use `parcel` command for development,
and examine the result right away in `localhost:1234`.
You can also `npm run dev` as set in `package.json`.

{{< highlight bash >}}
❯ parcel src/html/index.html
Server running at http://localhost:1234 
✨  Built in 2.71s.
{{< / highlight >}}

![CLI: parcel][61-cli-parcel]

Or you can either use `parcel build`,
or `npm run build` command for production,
and examine the result using you own web server.

{{< highlight bash >}}
❯ parcel build src/html/index.html
✨  Built in 779ms.

dist/style.ffae417e.css.map    1.84 KB     7ms
dist/script.9ce76c64.js        1.39 KB    19ms
dist/index.html                1.21 KB     6ms
dist/style.ffae417e.css          994 B    13ms
dist/script.9ce76c64.js.map      924 B     2ms
{{< / highlight >}}

![CLI: parcel build][61-cli-parcel-build]

Personally, I like to use `parcel build` command in development,
so I can examine all generated files.

#### View in Browser

You can just open the `localhost:1234` in browser.
You will see something similar like this below:

![Parcel: ][61-index-html]

#### Example HTML Output

While the `dist/index.html` contain this header:

{{< highlight html >}}
  <head>
    <meta charset="utf-8">
    <title>Example Bundle</title>
    <script src="/assets.2158d4e5.js"></script>
  <link rel="stylesheet" href="/assets.2158d4e5.css"></head>
{{< / highlight >}}

Transformed from `src/html/index.html` above.

-- -- --

#### 2: Bundle

How about bundling two or more stylesheet together?

#### Objective

> A bundle example.

#### Directory Tree

Consider this source directory below:

{{< highlight bash >}}
└── src
    ├── assets.js
    ├── css
    │   ├── button.css
    │   ├── color.css
    │   └── main.css
    ├── html
    │   └── alert.html
    └── js
        └── script.js
{{< / highlight >}}

These has three stylesheets.

#### package.json

* [gitlab.com/.../package.json][02-package-json].

{{< highlight javascript >}}
  "scripts": {
    "dev": "parcel src/html/alert.html --out-file index.html",
    "build": "parcel build src/html/alert.html --no-source-maps --out-file index.html"
  },
  "devDependencies": {
    "parcel-bundler": "^1.12.4"
  }
{{< / highlight >}}

#### Example of Separated Assets

Consider `src/html/index.html` contain this file:

* [gitlab.com/.../src/html/index.html][02-src-html-index-html]

{{< highlight html >}}
  <head>
    <meta charset="utf-8">
    <title>Example Bundle</title>
    <link href="../css/main.css"   rel="stylesheet" type="text/css">
    <link href="../css/color.css"  rel="stylesheet" type="text/css">
    <link href="../css/button.css" rel="stylesheet" type="text/css">
    <script src="../js/script.js"></script>
  </head>
{{< / highlight >}}

If we do this `parcel` command as usual,
we would have this complex result as below:

{{< highlight bash >}}
❯ parcel build src/html/index.html
✨  Built in 8.00s.

dist/script.9ce76c64.js         1.39 KB    727ms
dist/index.html                 1.34 KB    1.57s
dist/button.6de34fe4.css.map    1.07 KB     21ms
dist/script.9ce76c64.js.map       924 B      4ms
dist/color.98f5d4f3.css.map       566 B     21ms
dist/button.6de34fe4.css          546 B    6.30s
dist/main.ae68bab5.css.map        502 B     21ms
dist/color.98f5d4f3.css           290 B    6.30s
dist/main.ae68bab5.css            254 B    6.30s
{{< / highlight >}}

![Parcel: ][62-build-index-html]

What a mess ??

#### Example of Bundled Assets

Now consider `src/html/alert.html` contain this file:

* [gitlab.com/.../src/html/alert.html][02-src-html-alert-html]

{{< highlight html >}}
  <head>
    <meta charset="utf-8">
    <title>Example Bundle</title>
    <script src="../assets.js"></script>
  </head>
{{< / highlight >}}

In which the `assets.js` only consists of imported statement as below:

* [gitlab.com/.../src/assets.js][02-src-assets-js]

{{< highlight javascript >}}
import './js/script.js'
import './css/main.css'
import './css/button.css'
import './css/color.css'
{{< / highlight >}}

Let's do this `parcel` command again, with a few argument adjustment:

{{< highlight bash >}}
❯ parcel build src/html/alert.html --no-source-maps --out-file index.html
✨  Built in 3.74s.

dist/assets.20f97c1e.js     1.65 KB    2.47s
dist/index.html              1.2 KB    1.14s
dist/assets.24105bc7.css      946 B    2.01s
{{< / highlight >}}

![Parcel: ][62-build-alert-html]

Wow...!

#### Example HTML Output

Consider have a look at our final result. The `dist/index.html` header:

{{< highlight html >}}
  <head>
    <meta charset="utf-8">
    <title>Example Plain</title>
    <link href="/style.d1edd1db.css" rel="stylesheet" type="text/css">
    <script src="/script.f5aa2ae4.js"></script>
  <script src="/style.d1edd1db.js"></script></head>
{{< / highlight >}}

Transformed from `src/html/alert.html` above.

-- -- --

#### 3: Stack: Pug + Stylus + Coffeescript

What if I do not use standard assets?
Instead of `html+css+js`,
I can use other stack such as `pug+stylus+coffeescript`.

#### Objective

> A complete stack example

#### Directory Tree

Consider these sources below:

The first one is pretty basic, using `pug+css+js`.

{{< highlight bash >}}
└── src
    ├── css
    │   └── style.css
    ├── js
    │   └── script.js
    └── pug
        ├── index.pug
        └── partials
            └── body.pug
{{< / highlight >}}

And the second one is enhanced, using `pug+stylus+coffeescript`.

{{< highlight bash >}}
└── src
    ├── css
    │   └── style.styl
    ├── js
    │   └── script.coffee
    └── pug
        ├── alert.pug
        └── partials
            └── body.pug
{{< / highlight >}}

![Parcel: Nerdtree Assets][61-nerdtree-parcel]

#### package.json

Consider adjust the script command as needed.

* [gitlab.com/.../package.json][03-package-json].

{{< highlight javascript >}}
  "scripts": {
    "dev": "parcel src/pug/alert.pug --out-file index.html",
    "build": "parcel build src/pug/alert.pug --no-source-maps --out-file index.html"
  },
  "devDependencies": {
    "parcel-bundler": "^1.12.4",
    "pug": "^2.0.4",
    "stylus": "^0.54.7",
    "coffeescript": "^2.5.1"
  }
{{< / highlight >}}

Notice a few additional dependencies.

#### Example of Basic Stack

Consider this `src/pug/index.pug` below:

* [gitlab.com/.../src/pug/index.pug][03-src-pug-index-pug]

{{< highlight haskell >}}
doctype html
html(lang="en")

  head
    meta(charset='utf-8')
    title Example Stack
    link(href='../css/style.css', rel='stylesheet', type='text/css')
    script(src='../js/script.js')

  body
    include partials/body.pug
{{< / highlight >}}

Consider running the `parcel` command:

{{< highlight bash >}}
❯ parcel build src/pug/index.pug --no-source-maps
✨  Built in 11.05s.

dist/script.ba062b13.js    1.34 KB    291ms
dist/index.html            1.19 KB    2.17s
dist/style.150d1f3d.css      946 B    8.75s
{{< / highlight >}}

`pug` successfully build.

#### Example of Complete Stack

Consider this `src/pug/alert.pug`,
using `stylus` and `coffeescript` as below document:

* [gitlab.com/.../src/pug/alert.pug][03-src-pug-alert-pug]

{{< highlight haskell >}}
doctype html
html(lang="en")

  head
    meta(charset='utf-8')
    title Example Stack
    link(href='../css/style.styl', rel='stylesheet', type='text/css')
    script(src='../js/script.coffee')

  body
    include partials/body.pug
{{< / highlight >}}

Consider running the `parcel` command.
Note that you can always use `npm run build` shorthand,
instead of this long command below:

{{< highlight bash >}}
❯ parcel build src/pug/alert.pug --no-source-maps --out-file index.html
✨  Built in 5.53s.

dist/script.78bb67b2.js    1.37 KB    4.51s
dist/index.html            1.19 KB     24ms
dist/style.150d1f3d.css      946 B    5.37s
{{< / highlight >}}

`pug+stylus+coffeescript` also successfully build.

#### Example HTML Output

Consider have a look at our final result. The `dist/index.html` header.

{{< highlight html >}}
❯ cat dist/index.html | prettier --parser html
...
  <head>
    <meta charset="utf-8" />
    <title>Example Stack</title>
    <link href="/style.0cb376f7.css" rel="stylesheet" type="text/css" />
    <script src="/script.8529ffc2.js"></script>
    <script src="/style.0cb376f7.js"></script>
  </head>
...
{{< / highlight >}}

Transformed from `src/pug/alert.pug` above.

#### Conclusion

It is so easy to use `parcel` for simple task.

-- -- --

### What's Next?

All is done by now!

thankyou for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}frontend/2020/04/01/html-overview/

[source-example]:           {{< tutor-css-tools >}}/html-basic/06-parcel/

[61-nerdtree-parcel]:       {{< assets-frontend >}}/2020/04/61-nerdtree-parcel.png
[61-cli-parcel]:            {{< assets-frontend >}}/2020/04/61-cli-parcel.png
[61-cli-parcel-build]:      {{< assets-frontend >}}/2020/04/61-cli-parcel-build.png
[61-index-html]:            {{< assets-frontend >}}/2020/04/61-browser.png
[62-build-index-html]:      {{< assets-frontend >}}/2020/04/62-build-index-html.png
[62-build-alert-html]:      {{< assets-frontend >}}/2020/04/62-build-alert-html.png

[01-package-json]:          {{< tutor-css-tools >}}/html-basic/06-parcel/01-plain/package.json
[01-src-js-script-js]:      {{< tutor-css-tools >}}/html-basic/06-parcel/01-plain/src/js/script.js
[01-src-css-style-css]:     {{< tutor-css-tools >}}/html-basic/06-parcel/01-plain/src/css/style.css
[01-src-html-index-html]:   {{< tutor-css-tools >}}/html-basic/06-parcel/01-plain/src/html/index.html

[02-package-json]:          {{< tutor-css-tools >}}/html-basic/06-parcel/02-bundle/package.json
[02-src-html-alert-html]:   {{< tutor-css-tools >}}/html-basic/06-parcel/02-bundle/src/html/alert.html
[02-src-html-index-html]:   {{< tutor-css-tools >}}/html-basic/06-parcel/02-bundle/src/html/index.html
[02-src-assets-js]:         {{< tutor-css-tools >}}/html-basic/06-parcel/02-bundle/src/assets.js

[03-package-json]:          {{< tutor-css-tools >}}/html-basic/06-parcel/03-stack/package.json
[03-src-pug-index-pug]:     {{< tutor-css-tools >}}/html-basic/06-parcel/03-stack/src/pug/index.pug
[03-src-pug-alert-pug]:     {{< tutor-css-tools >}}/html-basic/06-parcel/03-stack/src/pug/alert.pug
