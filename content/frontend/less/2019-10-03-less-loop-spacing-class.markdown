---
type   : post
title  : "Less - Loop - Spacing Class"
date   : 2019-10-03T09:17:35+07:00
slug   : less-loop-spacing-class
categories: [frontend]
tags      : [less]
keywords  : [loop, spacing, margin, padding]
author : epsi
opengraph:
  image: assets-frontend/2019/10/lessjs-loop-script.png

excerpt:
  Step by step demonstration of lessjs loop capability
  to generate margin and padding classes.

toc    : "toc-2019-10-css-tools"

---

### Preface

> Goal: Generate custom spacing classes step by step,
  utilizing lessjs recursive capability to achieve loop.

Even when utilizing CSS Framework,
theme making require to create custom CSS.
One of my need is spacing class helper,
which is not available in Semantic UI.
This spacing class can be made utilizing `LessJS`.

![lessjs: script][image-ss-less]

These guidance will save you a lot of coding time.
And also tons of typing.

#### Reading

All code below are ported from the previous SASS article:

* [SASS - Loop - Spacing Class][sass-loop]

#### CSS Preprocessor

SASS/SCSS is one alternative among CSS preprocessor family.

![Illustration: CSS Preprocessor ][illust-prepro]

-- -- --

### 1: Simple Loop with Recursive

Imagine that we need a sequence of classes
to handle <code>margin</code>,
similar like CSS below:

{{< highlight css >}}
.m-1 {
  margin: 1px;
}
{{< / highlight >}}

This can be achieved with this recursive function

{{< highlight sass >}}
.loop(@cursor) when (@cursor > 0) {
  .loop((@cursor - 1)); // next iteration
  ...
}
{{< / highlight >}}

#### CSS Class Naming

We want to achieve this class naming

* <code>.m-@{number}</code>

#### LESS Source

For comfort reason,
we also need to define intial value for the loop.

* <code>/spacing-less/less/01-for.scss</code>
  : [gitlab.com/.../spacing-less/less/01-for.scss][tutor-less-01].

{{< highlight sass >}}
.loop(@cursor) when (@cursor > 0) {
  // next iteration
  .loop((@cursor - 1));

  // code for each iteration
  .m-@{cursor}{
    margin: (1px * @cursor); 
  }
}

// launch the loop
.loop(3);
{{< / highlight >}}

#### Variable Interpolation

Inside the loop we have this <code>@{...}</code> code.
This is variable interpolation.

* <http://lesscss.org/functions/#list-functions>

We use variable interpolation,
when we want to extract value into the css.

#### CSS Result

The <code>@cursor</code> variable takes care the rest,
so now we have this result below:

* <code>/spacing-less/css/01-loop.scss</code>
  : [gitlab.com/.../spacing-less/css/01-loop.css][tutor-css-01].

{{< highlight css >}}
.m-1 {
  margin: 1px;
}
.m-2 {
  margin: 2px;
}
.m-3 {
  margin: 3px;
}
{{< / highlight >}}

#### Limitation

The issue with <code>.loop(@cursor)</code> is we cannot use interval.
We can have this <code>[1, 2, 3, 4, 5]</code> sequence.
But we cannot have this <code>[5, 10, 15, 20, 25]</code> sequence.

-- -- --

### 2: More Loop Tricks

To solve <code>[5, 10, 15, 20, 25]</code> sequence,
we can utilize <code>.foo(@number)</code> function,
inside <code>.loop(@cursor)</code> recursive function.
The <code>.foo(@number)</code> does the actual printing.

{{< highlight sass >}}
.loop(@cursor) when (@cursor > 0) {
  // next iteration
  .loop((@cursor - 1));

  // actual printing
  .foo((@cursor * 5))
}
{{< / highlight >}}

#### CSS Class Naming

We still want to achieve this class naming

* <code>.m-@{number}</code>

#### LESS Source

Consider this <code>.loop(@cursor)</code> loop below:

* <code>/spacing-less/less/02-while.scss</code>
  : [gitlab.com/.../spacing-less/less/02-loop.scss][tutor-less-02].

{{< highlight sass >}}
.foo(@number) {
  .m-@{number}{
    margin: (1px * @number); 
  }
}

.loop(@cursor) when (@cursor > 0) {
  // next iteration
  .loop((@cursor - 1));

  // actual printing
  .foo((@cursor * 5))
}

// launch the loop
.loop(5);
{{< / highlight >}}

#### CSS Result

* <code>/spacing-less/css/02-while.scss</code>
  : [gitlab.com/.../spacing-less/css/02-loop.css][tutor-css-02].

{{< highlight css >}}
.m-5 {
  margin: 5px;
}
.m-10 {
  margin: 10px;
}
.m-15 {
  margin: 15px;
}
.m-20 {
  margin: 20px;
}
.m-25 {
  margin: 25px;
}
{{< / highlight >}}

-- -- --

### 3: Each

Before facing complex situation, consider a simple example.

#### Challenge

Now we have challenge that <code>margin</code> property
has these variants:

* <code>margin</code>,

* <code>margin-top</code>,

* <code>margin-bottom</code>,

* <code>margin-left</code>,

* <code>margin-right</code>

For simplicity reason,
I exclude the original <code>margin</code> variant.

#### CSS Class Naming

We want to achieve this class naming

* <code>.m-@{side}-5</code>

#### List Declaration

Less variable can handle a list.

{{< highlight sass >}}
@sides: top, bottom, left, right;
{{< / highlight >}}

#### Accessing List

This list can be accessed by using <code>@each</code> iterator.

{{< highlight sass >}}
each(@sides, {
  ...@{value}...
});
{{< / highlight >}}

#### LESS Source

Consider accessing <code>@sides</code> by this example below:

* <code>/spacing-less/less/03-each.scss</code>
  : [gitlab.com/.../spacing-less/less/03-each.scss][tutor-less-03].

{{< highlight sass >}}
// property
@sides: top, bottom, left, right;

each(@sides, {
  .m-@{value}-5 {
    margin-@{value}: 5px;
  }
});
{{< / highlight >}}

#### CSS Result

Now we have this:

* <code>/spacing-less/css/03-each.scss</code>
  : [gitlab.com/.../spacing-less/css/03-each.css][tutor-css-03].

{{< highlight css >}}
.m-top-5 {
  margin-top: 5px;
}
.m-bottom-5 {
  margin-bottom: 5px;
}
.m-left-5 {
  margin-left: 5px;
}
.m-right-5 {
  margin-right: 5px;
}
{{< / highlight >}}

This is ugly, because we want <code>.m-t-5</code>,
instead of </code>.m-top-5</code>.

-- -- --

### 4: Each with Pairs

Less has solution for this issue.

#### CSS Class Naming

We want to achieve this class naming

* <code>.m-@{abbreviation}-5</code>

#### List Declaration

Luckily, less allow us to have pairs in list.
Now we have CSS sub-property as below:

{{< highlight sass >}}
@sides: {top: t; bottom: b; left: l; right: r}
{{< / highlight >}}

#### LESS Source

Consider rewrite previous code.

* <code>/spacing-less/less/04-each-pairs.scss</code>
  : [gitlab.com/.../spacing-less/less/04-each-pairs.scss][tutor-less-04].

{{< highlight sass >}}
// property: abbreviation
@sides: {top: t; bottom: b; left: l; right: r}

each(@sides, {
  .m-@{value}-5 {
    margin-@{key}: 5px;
  }
})
{{< / highlight >}}

#### CSS Result

Now we have what wee need.

* <code>/spacing-less/css/04-each-pairs.scss</code>
  : [gitlab.com/.../spacing-less/css/04-each-pairs.css][tutor-css-04].

{{< highlight css >}}
.m-t-5 {
  margin-top: 5px;
}
.m-b-5 {
  margin-bottom: 5px;
}
.m-l-5 {
  margin-left: 5px;
}
.m-r-5 {
  margin-right: 5px;
}
{{< / highlight >}}

Notice that I use <code>.m-t-5</code>, instead of </code>.mt-5</code>,
to differ from bootstrap spacing classes.

-- -- --

### 5: Each in Loop

Consider leverage to a more complex situation.
We need a sequence of each classes.

#### CSS Class Naming

We want to achieve this class naming

* <code>.m-@{subname}-@{number}</code>

#### Skeleton

We put <code>each</code> iterator inside <code>.foo</code> function.

{{< highlight sass >}}
.foo(@number) {
  each(@sides, {
    ...
  })
}
{{< / highlight >}}

#### LESS Source

* <code>/spacing-less/less/05-loop-each-pairs.scss</code>
  : [gitlab.com/.../spacing-less/less/05-loop-each-pairs.scss][tutor-less-05].

{{< highlight sass >}}
// sub-property: abbreviation
@sides: {top: t; bottom: b; left: l; right: r}

.foo(@number) {
  each(@sides, {
    .m-@{value}-@{number} {
      margin-@{key}: (1px * @number);
    }
  })
}

.loop(@cursor) when (@cursor > 0) {
  // next iteration
  .loop((@cursor - 1));

  // actual printing
  .foo((@cursor * 5))
}

// launch the loop
.loop(5);
{{< / highlight >}}

#### CSS Result

* <code>/spacing-less/css/05-loop-each-pairs.css</code>
  : [gitlab.com/.../spacing-less/css/05-loop-each-pairs.css][tutor-css-05].

{{< highlight css >}}
.m-t-5 {
  margin-top: 5px;
}
.m-b-5 {
  margin-bottom: 5px;
}
.m-l-5 {
  margin-left: 5px;
}
.m-r-5 {
  margin-right: 5px;
}
.m-t-10 {
  margin-top: 10px;
}
.m-b-10 {
  margin-bottom: 10px;
}
.m-l-10 {
  margin-left: 10px;
}
.m-r-10 {
  margin-right: 10px;
}
.m-t-15 {
  margin-top: 15px;
}
.m-b-15 {
  margin-bottom: 15px;
}
.m-l-15 {
  margin-left: 15px;
}
.m-r-15 {
  margin-right: 15px;
}
.m-t-20 {
  margin-top: 20px;
}
.m-b-20 {
  margin-bottom: 20px;
}
.m-l-20 {
  margin-left: 20px;
}
.m-r-20 {
  margin-right: 20px;
}
.m-t-25 {
  margin-top: 25px;
}
.m-b-25 {
  margin-bottom: 25px;
}
.m-l-25 {
  margin-left: 25px;
}
.m-r-25 {
  margin-right: 25px;
}
{{< / highlight >}}

-- -- --

### 6: Nested Each in Loop

#### CSS Class Naming

We want to achieve this class naming

* <code>.@{name}-@{subname}-@{number}</code>

#### List Declaration

We define CSS property list:

* property: margin and padding

* each has sub-property: top, bottom, left, right.

{{< highlight sass >}}
@sides: {top: t; bottom: b; left: l; right: r}
@properties: {margin: m; padding: p}
{{< / highlight >}}

#### Skeleton

We put both <code>each</code> iterator,
inside <code>.loop(@cursor)</code> loop.
But remember that we have outer loop and inner loop.
The inner loop lies inside <code>.foo(@number, @prop, @abbr)</code>.

{{< highlight sass >}}
.foo(@number, @prop, @abbr) {
  each(@sides, {
    ...
  })
}

.loop(@cursor) when (@cursor > 0) {
  ...
  
  each(@properties, {
    .foo((@cursor * 5), @key, @value)
  })  
}
{{< / highlight >}}

#### LESS Source

* <code>/spacing-less/less/06-loop-each-pairs.scss</code>
  : [gitlab.com/.../spacing-less/less/06-loop-each-pairs.scss][tutor-less-06].

{{< highlight sass >}}
// sub-property: abbreviation
@sides: {top: t; bottom: b; left: l; right: r}
@properties: {margin: m; padding: p}

.foo(@number, @prop, @abbr) {
  each(@sides, {
    .@{abbr}-@{value}-@{number} {
      @{prop}-@{key}: (1px * @number);
    }
  })
}

.loop(@cursor) when (@cursor > 0) {
  // next iteration
  .loop((@cursor - 1));
  
  each(@properties, {
    // actual printing
    .foo((@cursor * 5), @key, @value)
  })  
}

// launch the loop
.loop(5);
{{< / highlight >}}

#### CSS Result

Now we have both margin and padding.

* <code>/spacing-less/css/06-loop-each-pairs.css</code>
  : [gitlab.com/.../spacing-less/css/06-loop-each-pairs.css][tutor-css-06].

{{< highlight css >}}
.m-t-5 {
  margin-top: 5px;
}
.m-b-5 {
  margin-bottom: 5px;
}
.m-l-5 {
  margin-left: 5px;
}
.m-r-5 {
  margin-right: 5px;
}
.p-t-5 {
  padding-top: 5px;
}
.p-b-5 {
  padding-bottom: 5px;
}
.p-l-5 {
  padding-left: 5px;
}
.p-r-5 {
  padding-right: 5px;
}
.m-t-10 {
  margin-top: 10px;
}
.m-b-10 {
  margin-bottom: 10px;
}
.m-l-10 {
  margin-left: 10px;
}
.m-r-10 {
  margin-right: 10px;
}
.p-t-10 {
  padding-top: 10px;
}
.p-b-10 {
  padding-bottom: 10px;
}
.p-l-10 {
  padding-left: 10px;
}
.p-r-10 {
  padding-right: 10px;
}
.m-t-15 {
  margin-top: 15px;
}
.m-b-15 {
  margin-bottom: 15px;
}
.m-l-15 {
  margin-left: 15px;
}
.m-r-15 {
  margin-right: 15px;
}
.p-t-15 {
  padding-top: 15px;
}
.p-b-15 {
  padding-bottom: 15px;
}
.p-l-15 {
  padding-left: 15px;
}
.p-r-15 {
  padding-right: 15px;
}
.m-t-20 {
  margin-top: 20px;
}
.m-b-20 {
  margin-bottom: 20px;
}
.m-l-20 {
  margin-left: 20px;
}
.m-r-20 {
  margin-right: 20px;
}
.p-t-20 {
  padding-top: 20px;
}
.p-b-20 {
  padding-bottom: 20px;
}
.p-l-20 {
  padding-left: 20px;
}
.p-r-20 {
  padding-right: 20px;
}
.m-t-25 {
  margin-top: 25px;
}
.m-b-25 {
  margin-bottom: 25px;
}
.m-l-25 {
  margin-left: 25px;
}
.m-r-25 {
  margin-right: 25px;
}
.p-t-25 {
  padding-top: 25px;
}
.p-b-25 {
  padding-bottom: 25px;
}
.p-l-25 {
  padding-left: 25px;
}
.p-r-25 {
  padding-right: 25px;
}
{{< / highlight >}}

-- -- --

### 7: Final

Now comes the final result.

#### CSS Class Naming

We want to achieve both class naming

* <code>.@{name}-@{number}</code>

* <code>.@{name}-@{subname}-@{number}</code>

#### Skeleton

We put more stuff in <code>.foo(@number, @prop, @abbr)</code> function.

{{< highlight sass >}}
.foo(@number, @prop, @abbr) {
  .@{abbr}-@{number} {
    @{prop}: (1px * @number) !important;
  }

  each(@sides, {
    .@{abbr}-@{value}-@{number} {
      @{prop}-@{key}: (1px * @number) !important;
    }
  })
}
{{< / highlight >}}

#### LESS Source

* <code>/spacing-less/less/07-final.scss</code>
  : [gitlab.com/.../spacing-less/less/07-final.scss][tutor-less-07].

{{< highlight sass >}}
// sub-property: abbreviation
@properties: {margin: m; padding: p}
@sides: {top: t; bottom: b; left: l; right: r}

.foo(@number, @prop, @abbr) {
  .@{abbr}-@{number} {
    @{prop}: (1px * @number) !important;
  }

  each(@sides, {
    .@{abbr}-@{value}-@{number} {
      @{prop}-@{key}: (1px * @number) !important;
    }
  })
}

.loop(@cursor) when (@cursor > 0) {
  // next iteration
  .loop((@cursor - 1));
  
  each(@properties, {
    // actual printing
    .foo((@cursor * 5), @key, @value)
  })
  
}

// launch the loop
.loop(5);
{{< / highlight >}}

Notice that I also put <code>!important</code> in css value.

#### CSS Result

* <code>/spacing-less/css/07-final.css</code>
  : [gitlab.com/.../spacing-less/css/07-final.css][tutor-css-07].

{{< highlight css >}}
.m-5 {
  margin: 5px !important;
}
.m-t-5 {
  margin-top: 5px !important;
}
.m-b-5 {
  margin-bottom: 5px !important;
}
.m-l-5 {
  margin-left: 5px !important;
}
.m-r-5 {
  margin-right: 5px !important;
}
.p-5 {
  padding: 5px !important;
}
.p-t-5 {
  padding-top: 5px !important;
}
.p-b-5 {
  padding-bottom: 5px !important;
}
.p-l-5 {
  padding-left: 5px !important;
}
.p-r-5 {
  padding-right: 5px !important;
}
.m-10 {
  margin: 10px !important;
}
.m-t-10 {
  margin-top: 10px !important;
}
.m-b-10 {
  margin-bottom: 10px !important;
}
.m-l-10 {
  margin-left: 10px !important;
}
.m-r-10 {
  margin-right: 10px !important;
}
.p-10 {
  padding: 10px !important;
}
.p-t-10 {
  padding-top: 10px !important;
}
.p-b-10 {
  padding-bottom: 10px !important;
}
.p-l-10 {
  padding-left: 10px !important;
}
.p-r-10 {
  padding-right: 10px !important;
}
.m-15 {
  margin: 15px !important;
}
.m-t-15 {
  margin-top: 15px !important;
}
.m-b-15 {
  margin-bottom: 15px !important;
}
.m-l-15 {
  margin-left: 15px !important;
}
.m-r-15 {
  margin-right: 15px !important;
}
.p-15 {
  padding: 15px !important;
}
.p-t-15 {
  padding-top: 15px !important;
}
.p-b-15 {
  padding-bottom: 15px !important;
}
.p-l-15 {
  padding-left: 15px !important;
}
.p-r-15 {
  padding-right: 15px !important;
}
.m-20 {
  margin: 20px !important;
}
.m-t-20 {
  margin-top: 20px !important;
}
.m-b-20 {
  margin-bottom: 20px !important;
}
.m-l-20 {
  margin-left: 20px !important;
}
.m-r-20 {
  margin-right: 20px !important;
}
.p-20 {
  padding: 20px !important;
}
.p-t-20 {
  padding-top: 20px !important;
}
.p-b-20 {
  padding-bottom: 20px !important;
}
.p-l-20 {
  padding-left: 20px !important;
}
.p-r-20 {
  padding-right: 20px !important;
}
.m-25 {
  margin: 25px !important;
}
.m-t-25 {
  margin-top: 25px !important;
}
.m-b-25 {
  margin-bottom: 25px !important;
}
.m-l-25 {
  margin-left: 25px !important;
}
.m-r-25 {
  margin-right: 25px !important;
}
.p-25 {
  padding: 25px !important;
}
.p-t-25 {
  padding-top: 25px !important;
}
.p-b-25 {
  padding-bottom: 25px !important;
}
.p-l-25 {
  padding-left: 25px !important;
}
.p-r-25 {
  padding-right: 25px !important;
}
{{< / highlight >}}

Now you can use this spacing less in your project.

-- -- --

### 8: Update: XY

For practical reason, I have to update this article.

* Start form zero <code>.0</code>, instead of <code>5</code>.

* Consider X, and Y just like bootstrap.

{{< highlight css >}}
.m-y-5 {
  margin-top: 5px !important;
  margin-bottom: 5px !important;
}

.m-x-5 {
  margin-left: 5px !important;
  margin-right: 5px !important;
}
{{< / highlight >}}

The drawback is, I have bigger stylesheet size.

#### List Declaration

Luckily, less allow us to have pairs in list.
Now we have CSS sub-property as below:

{{< highlight sass >}}
@properties: {margin: m; padding: p}
@sides: {top: t; bottom: b; left: l; right: r}
@sidesy: top, bottom;
@sidesx: left, right;
{{< / highlight >}}

And the <code>foo</code> function is a little bit different:

{{< highlight sass >}}
.foo(@number, @prop, @abbr) {
  .@{abbr}-@{number} {
    @{prop}: (1px * @number) !important;
  }

  each(@sides, {
    .@{abbr}-@{value}-@{number} {
      @{prop}-@{key}: (1px * @number) !important;
    }
  })

  .@{abbr}-y-@{number} {
    each(@sidesy, {
      @{prop}-@{value}: (1px * @number) !important;
    })
  }

  .@{abbr}-x-@{number} {
    each(@sidesx, {
      @{prop}-@{value}: (1px * @number) !important;
    })
  }
}
{{< / highlight >}}

#### LESS Source

As a conclusion, this is the complete code.

* <code>/spacing-less/less/08-final-xy.less</code>
  : [gitlab.com/.../spacing-less/less/08-final-xy.less][tutor-less-08].

{{< highlight sass >}}
// sub-property: abbreviation
@properties: {margin: m; padding: p}
@sides: {top: t; bottom: b; left: l; right: r}
@sidesy: top, bottom;
@sidesx: left, right;

.foo(@number, @prop, @abbr) {
  .@{abbr}-@{number} {
    @{prop}: (1px * @number) !important;
  }

  each(@sides, {
    .@{abbr}-@{value}-@{number} {
      @{prop}-@{key}: (1px * @number) !important;
    }
  })

  .@{abbr}-y-@{number} {
    each(@sidesy, {
      @{prop}-@{value}: (1px * @number) !important;
    })
  }

  .@{abbr}-x-@{number} {
    each(@sidesx, {
      @{prop}-@{value}: (1px * @number) !important;
    })
  }
}

.loop(@cursor) when (@cursor > 0) {
  // next iteration
  .loop((@cursor - 1));
  
  each(@properties, {
    // actual printing
    .foo((@cursor * 5 -  5), @key, @value)
  })
  
}

// launch the loop
.loop(6);
{{< / highlight >}}

-- -- --

### What is Next ?

After this `loop`, there is also `conditional` article, that you might need to read.
Consider continue reading [ [Less - Conditional - Color Class][local-whats-next] ].

Thank you for reading.

-- -- --

### Conclusion

__These spacing classes is ready to serve__.

![lessjs: output][image-ss-css]

What do you think ?

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2019/10/04/less-conditional-color-class/

[sass-loop]:    {{< baseurl >}}frontend/2019/06/21/sass-loop-spacing-class/

[illust-prepro]:    {{< assets-frontend >}}/2019/10/css-preprocessors.png

[image-ss-less]:    {{< assets-frontend >}}/2019/10/lessjs-loop-script.png
[image-ss-css]:     {{< assets-frontend >}}/2019/10/lessjs-loop-output.png

[tutor-less-01]:    {{< tutor-css-tools >}}/spacing-less/less/01-loop.less
[tutor-less-02]:    {{< tutor-css-tools >}}/spacing-less/less/02-loop.less
[tutor-less-03]:    {{< tutor-css-tools >}}/spacing-less/less/03-each.less
[tutor-less-04]:    {{< tutor-css-tools >}}/spacing-less/less/04-each-pairs.less
[tutor-less-05]:    {{< tutor-css-tools >}}/spacing-less/less/05-loop-each-pairs.less
[tutor-less-06]:    {{< tutor-css-tools >}}/spacing-less/less/06-loop-each-pairs.less
[tutor-less-07]:    {{< tutor-css-tools >}}/spacing-less/less/07-final.less
[tutor-less-08]:    {{< tutor-css-tools >}}/spacing-less/less/08-final-xy.less

[tutor-css-01]:     {{< tutor-css-tools >}}/spacing-less/css/01-loop.css
[tutor-css-02]:     {{< tutor-css-tools >}}/spacing-less/css/02-loop.css
[tutor-css-03]:     {{< tutor-css-tools >}}/spacing-less/css/03-each.css
[tutor-css-04]:     {{< tutor-css-tools >}}/spacing-less/css/04-each-pairs.css
[tutor-css-05]:     {{< tutor-css-tools >}}/spacing-less/css/05-loop-each-pairs.css
[tutor-css-06]:     {{< tutor-css-tools >}}/spacing-less/css/06-loop-each-pairs.css
[tutor-css-07]:     {{< tutor-css-tools >}}/spacing-less/css/07-final.css
[tutor-css-08]:     {{< tutor-css-tools >}}/spacing-less/css/08-final-xy.css
