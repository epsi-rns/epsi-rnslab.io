---
type   : post
title  : "PostCSS - Configuration"
date   : 2019-10-10T09:17:35+07:00
slug   : postcss-configuration
categories: [frontend]
tags      : [postcss]
keywords  : [loop, spacing, margin, padding]
author : epsi
opengraph:
  image: assets-frontend/2019/10/postcss-watch-verbose.png

excerpt:
  Example postcss configuration to generate margin and padding classes.

toc    : "toc-2019-10-css-tools"

---

### Preface

> Goal: Configure postcss for use with custom spacing classes.

There are many way to configure PostCSS,

* Using `postcss.config.js`

* Using Task Runner such as Grunt or Gulp

* Using bundler, such as `postcss.config.js` along with `webpack.config.js`.

#### Reading

All code below are ported from the previous SASS article:

* [SASS - Loop - Spacing Class][sass-loop]

-- -- --

### 1: PostCSS Configuration

There are many way to configure PostCSS.
The simple one is using `postcss.config.js`.

#### When to Use

This assuming that you do not work with any other tools.
If you work with other tools you might consider to use `gulp` or `grunt`.
Or even `postcss.config.js` along with `webpack.config.js`.

#### Requirement

Requirement may vary, for each case.
Some developer might utilize `PreCSS`,
while some other developer might prefer `SugarSS`.
And so does detail feature for each project might be different.

For the next loop spacing class case, `PreCSS` is all you need.
Also for three good reasons I added some feature:

* prettier or prettify: to tidy the output CSS,

* postcss-strip-inline-comments: to enable inline comment style like in SASS, and

* postcss-each: complex each with mutiple variable.

#### Example

For this tutorial, I simply use this configuration below:

* [gitlab.com/.../spacing-postcss/postcss.config.js][tutor-config].

{{< highlight javascript >}}
module.exports = {
  syntax: 'postcss-scss',
  plugins: [
    require('postcss-strip-inline-comments'),
    require('postcss-each'),
    require('precss'),
    require('postcss-prettify'),
  ],
}
{{< / highlight >}}

#### Node Package

For this to work, we need `package.json`:

* [gitlab.com/.../spacing-postcss/package.json][tutor-config-package].

{{< highlight javascript >}}
{
  ...
  "devDependencies": {
    "postcss": "^7.0.29",
    "postcss-cli": "^7.1.1",
    "postcss-each": "^0.10.0",
    "postcss-prettify": "^0.3.4",
    "postcss-scss": "^2.0.0",
    "postcss-strip-inline-comments": "^0.1.5",
    "precss": "^4.0.0"
  }
}
{{< / highlight >}}

Do not forget to install with NPM.

{{< highlight bash >}}
$ npm install
{{< / highlight >}}

#### Command Line Interface

Now you can simply run this command:

{{< highlight bash >}}
$ postcss -d build src/00-comment.css
{{< / highlight >}}

Or watch in verbose.

{{< highlight bash >}}
$ postcss -w --verbose -d build src/**/*.css
Processing src/00-comment.css...
Processing src/01-for.css...
Processing src/02-for-by.css...
Processing src/03-each.css...
Processing src/04-each-pairs.css...
Processing src/05-for-each-pairs.css...
Processing src/06-for-nested-each-pairs.css...
Processing src/07-final.css...
Processing src/08-final-xy.css...
Finished src/00-comment.css in 1.5 s
Finished src/01-for.css in 1.51 s
Finished src/02-for-by.css in 1.51 s
Finished src/04-each-pairs.css in 1.51 s
Finished src/05-for-each-pairs.css in 1.51 s
Finished src/03-each.css in 1.51 s
Finished src/06-for-nested-each-pairs.css in 1.51 s
Finished src/07-final.css in 1.51 s
Finished src/08-final-xy.css in 1.51 s

Waiting for file changes...
{{< / highlight >}}

![PostCSS: Watch in Verbose][image-ss-watch]

-- -- --

### 2: Alternative: Gulp

There are many way to configure PostCSS,
alternatively you can use`gulp`.
The basic configuration is as below:

#### Basic Gulp

`PreCSS` is all you need.

{{< highlight javascript >}}
'use strict';

var gulp = require('gulp');

gulp.task('css', () => {
  const postcss    = require('gulp-postcss')

  return gulp.src('src/**/*.css')
    .pipe( postcss([ require('precss') ]) )
    .pipe( gulp.dest('dest/') )
})

gulp.task('default', gulp.series('css'));
{{< / highlight >}}

#### Complete Gulp

The code grow as below:

* [gitlab.com/.../spacing-postcss/gulp.js][tutor-gulp].

{{< highlight javascript >}}
'use strict';

var gulp = require('gulp');

gulp.task('css', () => {
  const postcss    = require('gulp-postcss')
  const prettier   = require('gulp-prettier')
  const scss       = require('postcss-scss');

  return gulp.src('src/**/*.css')
    .pipe( postcss([ 
        require('postcss-strip-inline-comments'), 
        require('postcss-each'),
        require('precss')
      ], {syntax: scss}) 
    )
    .pipe( prettier() )
    .pipe( gulp.dest('dest/') )
})

gulp.task('default', gulp.series('css'));
{{< / highlight >}}

#### Node Package for Gulp

For this to work, we need `package.json`:

* [gitlab.com/.../spacing-postcss/package-example-gulp.json][tutor-gulp-package].

{{< highlight javascript >}}
{
  ...
  "devDependencies": {
    "gulp": "^4.0.2",
    "gulp-postcss": "^8.0.0",
    "gulp-prettier": "^2.3.0",
    "precss": "^4.0.0",
    "postcss-scss": "^2.0.0",
    "postcss-strip-inline-comments": "^0.1.5",
    "postcss-each": "^0.10.0"
  }
}
{{< / highlight >}}

Now you can install with NPM.

{{< highlight bash >}}
$ npm install
{{< / highlight >}}

#### Run Gulp, Run!

Now run the `gulp`:

{{< highlight bash >}}
$ gulp
[01:27:27] Using gulpfile ~/Documents/postcss/gulpfile.js
[01:27:27] Starting 'default'...
[01:27:27] Starting 'css'...
[01:27:34] Finished 'css' after 6.89 s
[01:27:34] Finished 'default' after 6.89 s
{{< / highlight >}}

![Gulp: running PostCSS][image-ss-gulp]

-- -- --

### 3: Alternative: Grunt

Using Grunt with postcss is a little bit tricky,
because post css in grunt will overwrite the original files

Luckily there is a cool complete answer in stackoverflow.

* [How to use autoprefixer with postcss and Grunt on more than one file?][grunt-postcss-copy]

#### Basic Grunt

Consider this my first attempt of `Gruntfile.js` configuration:

{{< highlight javascript >}}
module.exports = function(grunt) {
  // configure the tasks
  let config = {

    postcss: {
      options: {
        map: true, // inline sourcemaps
        syntax: require('postcss-scss'),
        processors: [
          require('postcss-strip-inline-comments'),
          require('postcss-each'),
          require('precss'),
          require('postcss-prettify'),
        ]
      },
      dist: {
        src: 'src/*.css'
      }
    }
  };

  grunt.initConfig(config);

  // load the tasks
  grunt.loadNpmTasks('grunt-postcss');

  // define the tasks
  grunt.registerTask('default', [
    'postcss'
  ] );

};
{{< / highlight >}}

With this configuration, all files in `src/*.css`.

#### Complete Grunt

After reading stackoverflow, I've got this working workaround.

* [gitlab.com/.../spacing-postcss/Gruntfile.js][tutor-gruntfile-js]

{{< highlight javascript >}}
module.exports = function(grunt) {
  // configure the tasks
  let config = {

    postcss: {
      options: {
        map: true, // inline sourcemaps
        syntax: require('postcss-scss'),
        processors: [
          require('postcss-strip-inline-comments'),
          require('postcss-each'),
          require('precss'),
          require('postcss-prettify'),
        ]
      },
      dist: {
        files: [
          {
            src: 'dest/*.css'
          }
        ]
      }
    },

    copy: {
      postcss: {
        files: [
          {
            src: 'src/*.css',
            dest: './dest/',
            expand: true,
            flatten: true
          }
        ]
      }
    },

    watch: {
      postcss: {
        files: ['src/*'],
        tasks: ['postcss'],
        options: {
          interrupt: false,
          spawn: false
        }
      },
    }

  };

  grunt.initConfig(config);

  // load the tasks
  grunt.loadNpmTasks('grunt-postcss');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('css', ['copy:postcss', 'postcss']);

  // define the tasks
  grunt.registerTask('default', [
    'css', 'watch'
  ] );

};
{{< / highlight >}}

#### Node Package for Grunt

For this to work, we need `package.json`:

* [gitlab.com/.../spacing-postcss/package-example-grunt.json][tutor-grunt-package].

{{< highlight javascript >}}
{
  ...
  "devDependencies": {
    "grunt": "^1.0.1",
    "grunt-contrib-copy": "^1.0.0",
    "grunt-contrib-watch": "^1.1.0",
    "grunt-postcss": "^0.9.0",
    "postcss-each": "^0.10.0",
    "postcss-prettify": "^0.3.4",
    "postcss-scss": "^2.0.0",
    "postcss-strip-inline-comments": "^0.1.5",
    "precss": "^4.0.0"
  }
}
{{< / highlight >}}

Now you can install with NPM.

{{< highlight bash >}}
$ npm install
{{< / highlight >}}

#### Run Grunt, Run!

Now run the `grunt`:

{{< highlight bash >}}
$ grunt
Running "copy:postcss" (copy) task
Copied 9 files

Running "postcss:dist" (postcss) task
>> 9 processed stylesheets created.

Running "watch" task
Waiting...
{{< / highlight >}}

![Grunt: running PostCSS][image-ss-grunt]

-- -- --

### What is Next ?

After this `configuration`, it is a good time to examine `postcss` in action.
Consider continue reading [ [PostCSS - Loop - Spacing Class][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2019/10/11/postcss-loop-spacing-class/

[grunt-postcss-copy]:   https://stackoverflow.com/questions/36376645/how-to-use-autoprefixer-with-postcss-and-grunt-on-more-than-one-file#36442427

[sass-loop]:    {{< baseurl >}}frontend/2019/06/21/sass-loop-spacing-class/

[image-ss-postcss]: {{< assets-frontend >}}/2019/10/postcss-for-nested-script.png

[image-ss-gulp]:    {{< assets-frontend >}}/2019/10/postcss-gulp.png
[image-ss-grunt]:   {{< assets-frontend >}}/2019/10/postcss-grunt.png
[image-ss-watch]:   {{< assets-frontend >}}/2019/10/postcss-watch-verbose.png

[tutor-config]:         {{< tutor-css-tools >}}/spacing-postcss/postcss.config.js
[tutor-config-package]: {{< tutor-css-tools >}}/spacing-postcss/package.json
[tutor-gulp]:           {{< tutor-css-tools >}}/spacing-postcss/gulpfile.js
[tutor-gulp-package]:   {{< tutor-css-tools >}}/spacing-postcss/package-example-gulp.json
[tutor-gruntfile-js]:   {{< tutor-css-tools >}}/spacing-postcss/Gruntfile.js
[tutor-grunt-package]:  {{< tutor-css-tools >}}/spacing-postcss/package-example-grunt.json

