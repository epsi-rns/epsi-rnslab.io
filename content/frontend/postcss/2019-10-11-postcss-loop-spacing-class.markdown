---
type   : post
title  : "PostCSS - Loop - Spacing Class"
date   : 2019-10-11T09:17:35+07:00
slug   : postcss-loop-spacing-class
categories: [frontend]
tags      : [postcss]
keywords  : [loop, spacing, margin, padding]
author : epsi
opengraph:
  image: assets-frontend/2019/10/postcss-for-nested-script.png

excerpt:
  Step by step demonstration of postcss loop capability
  to generate margin and padding classes.

toc    : "toc-2019-10-css-tools"

---

### Preface

> Goal: Generate custom spacing classes step by step,
  utilizing postcss loop capability.

Even when utilizing CSS Framework,
theme making require to create custom CSS.
One of my need is spacing class, that is not available
in neither Bulma nor Materialize.
This spacing class can be made utilizing `PostCSS`,
or to be exact, using PreCSS `plugin`.

![PostCSS: script][image-ss-postcss]

These guidance will save you a lot of coding time.
And also tons of typing.

#### Reading

All code below are ported from the previous SASS article:

* [SASS - Loop - Spacing Class][sass-loop]

#### CSS Preprocessor

SASS/SCSS is one alternative among CSS preprocessor family.

![Illustration: CSS Preprocessor ][illust-prepro]

-- -- --

### Configuration

This assume you have read the previous articles.
We are using this configuration:

* [gitlab.com/.../spacing-postcss/postcss.config.js][tutor-config].

{{< highlight javascript >}}
module.exports = {
  syntax: 'postcss-scss',
  plugins: [
    require('postcss-strip-inline-comments'),
    require('postcss-each'),
    require('precss'),
    require('postcss-prettify'),
  ],
}
{{< / highlight >}}

If you haven't read the configuration guidance yet,
I suggest you to take one step behind to this article:

* [PostCSS - Configuration][local-whats-behind]

-- -- --

### 0: Comment

Consider test our `postcss` with `gulp` to remove this simple comment.

#### PostCSS Source

* [gitlab.com/.../spacing-postcss/src/00-comment.css][tutor-postcss-01].

{{< highlight sass >}}
// This line will be removed

.m-1 {
  margin: 1px;
}
{{< / highlight >}}

#### CSS Result

The result would be:

* [gitlab.com/.../spacing-less/css/00-comment.css][tutor-css-01].

{{< highlight sass >}}
.m-1 {
  margin: 1px;
}
{{< / highlight >}}

-- -- --

### 1: @for

Imagine that we need a sequence of classes
to handle <code>margin</code>,
similar like CSS below:

{{< highlight css >}}
.m-1 {
  margin: 1px;
}
{{< / highlight >}}

This can be achieved with this loop constructor

{{< highlight sass >}}
@for ... from ... to ... {
  ...
}
{{< / highlight >}}

#### CSS Class Naming

We want to achieve this class naming

* <code>.m-$(number)</code>

#### PostCSS Source

For comfort reason,
we also need to define intial value for the loop.

* [gitlab.com/.../spacing-postcss/src/01-for.css][tutor-postcss-01].

{{< highlight sass >}}
// variable initialization

$loop-begin: 1;
$loop-stop:  3;

// loop

@for $cursor from $loop-begin to $loop-stop {
  .m-$(cursor) {
    margin: $(cursor)px;
  }
}
{{< / highlight >}}

#### Variable Interpolation

Inside the loop we have this <code>$(...)</code> code.
This is variable interpolation.

We use variable interpolation,
when we want to extract value into the css.

#### CSS Result

The <code>$(cursor)</code> variable takes care the rest,
so now we have this result below:

* [gitlab.com/.../spacing-postcss/dest/01-for.css][tutor-css-01].

{{< highlight css >}}
.m-1 {
  margin: 1px;
}
.m-2 {
  margin: 2px;
}
.m-3 {
  margin: 3px;
}
{{< / highlight >}}

#### Limitation

The issue with <code>@for</code> is we cannot use interval.
We can have this <code>[1, 2, 3, 4, 5]</code> sequence.
But we cannot have this <code>[5, 10, 15, 20, 25]</code> sequence.

-- -- --

### 2: @for by Interval

To solve <code>[5, 10, 15, 20, 25]</code> sequence,
we can utilize <code>@for ... by</code>.

{{< highlight sass >}}
@for ... from ... to ... by ... {
  ...
}
{{< / highlight >}}

#### CSS Class Naming

We still want to achieve this class naming

* <code>.m-$(number)</code>

#### PostCSS Source

Consider this <code>@for</code> loop below:

* [gitlab.com/.../spacing-postcss/src/02-for-by.css][tutor-postcss-02].

{{< highlight sass >}}
// variable initialization

$loop-begin: 5;
$loop-stop: 25;
$interval:   5;

// loop

@for $cursor from $loop-begin
to $loop-stop by $interval {
  .m-$(cursor) {
    margin: $(cursor)px;
  }
}
{{< / highlight >}}

#### CSS Result

* [gitlab.com/.../spacing-postcss/dest/02-for-by.css][tutor-css-02].

{{< highlight css >}}
.m-5 {
  margin: 5px;
}
.m-10 {
  margin: 10px;
}
.m-15 {
  margin: 15px;
}
.m-20 {
  margin: 20px;
}
.m-25 {
  margin: 25px;
}
{{< / highlight >}}

-- -- --

### 3: @each

Before facing complex situation, consider a simple example.

#### Challenge

Now we have challenge that <code>margin</code> property
has these variants:

* <code>margin</code>,

* <code>margin-top</code>,

* <code>margin-bottom</code>,

* <code>margin-left</code>,

* <code>margin-right</code>

For simplicity reason,
I exclude the original <code>margin</code> variant.

#### CSS Class Naming

We want to achieve this class naming

* <code>.m-$(side)-5</code>

#### List Declaration

PostCSS can handle a list.

{{< highlight sass >}}
(top, bottom, left, right)
{{< / highlight >}}

#### Accessing List

This list can be accessed by using <code>@each</code> iterator.

{{< highlight sass >}}
@each ... in ... {
  ...
}
{{< / highlight >}}

#### PostCSS Source

Consider accessing <code>$sides</code> by this example below:

* [gitlab.com/.../spacing-postcss/src/03-each.css][tutor-postcss-03].

{{< highlight sass >}}
@each $side in (top, bottom, left, right) {
  .m-$(side)-5 {
    margin-$(side): 5px;
  }
}
{{< / highlight >}}

#### CSS Result

Now we have this:

* [gitlab.com/.../spacing-postcss/dest/03-each.css][tutor-css-03].

{{< highlight css >}}
.m-top-5 {
  margin-top: 5px;
}
.m-bottom-5 {
  margin-bottom: 5px;
}
.m-left-5 {
  margin-left: 5px;
}
.m-right-5 {
  margin-right: 5px;
}
{{< / highlight >}}

This is ugly, because we want <code>.m-t-5</code>,
instead of </code>.m-top-5</code>.

-- -- --

### 4: @each with Pairs

PostCSS has solution for this issue.

#### CSS Class Naming

We want to achieve this class naming

* <code>.m-$(abbreviation)-5</code>

#### List Declaration

Luckily, PostCSS allow us to have pairs in list.
Now we have property as pairs as shown below:

{{< highlight sass >}}
(top, bottom, left, right), (t, b, l, r)
{{< / highlight >}}

#### PostCSS Source

Consider rewrite previous code.

* [gitlab.com/.../spacing-postcss/src/04-each-pairs.css][tutor-postcss-04].

{{< highlight sass >}}
// sides : properties, abbreviation;

@each $side, $name 
in (top, bottom, left, right), (t, b, l, r) {
  .m-$(name)-5 {
    margin-$(side): 5px;
  }
}
{{< / highlight >}}

#### CSS Result

Now we have what wee need.

* [gitlab.com/.../spacing-postcss/dest/04-each-pairs.css][tutor-css-04].

{{< highlight css >}}
.m-t-5 {
  margin-top: 5px;
}
.m-b-5 {
  margin-bottom: 5px;
}
.m-l-5 {
  margin-left: 5px;
}
.m-r-5 {
  margin-right: 5px;
}

{{< / highlight >}}

Notice that I use <code>.m-t-5</code>, instead of </code>.mt-5</code>,
to differ from bootstrap spacing classes.

-- -- --

### 5: @for @each

Consider leverage to a more complex situation.
We need a sequence of each classes.

#### CSS Class Naming

We want to achieve this class naming

* <code>.m-$(subname)-$($number)</code>

#### Skeleton

We put <code>@each</code> iterator inside <code>@for</code> loop.

{{< highlight sass >}}
@for $cursor from $loop-begin
to $loop-stop by $interval {

  @each $side, $name
  in (top, bottom, left, right), (t, b, l, r) {
    ...
  }
}
{{< / highlight >}}

#### PostCSS Source

* [gitlab.com/.../spacing-postcss/src/05-for-each-pairs.css][tutor-postcss-05].

{{< highlight sass >}}
// variable initialization

$loop-begin: 5;
$loop-stop: 25;
$interval:   5;

@for $cursor from $loop-begin
to $loop-stop by $interval {
  
  @each $side, $name
  // sides: properties, abbreviation;
  in (top, bottom, left, right), (t, b, l, r) {
    .m-$(name)-$(cursor) {
      margin-$(side): $(cursor)px;
    }
  }
}
{{< / highlight >}}

#### CSS Result

* [gitlab.com/.../spacing-postcss/dest/05-for-each-pairs.css][tutor-css-05].

{{< highlight css >}}
.m-t-5 {
  margin-top: 5px;
}
.m-b-5 {
  margin-bottom: 5px;
}
.m-l-5 {
  margin-left: 5px;
}
.m-r-5 {
  margin-right: 5px;
}
.m-t-10 {
  margin-top: 10px;
}
.m-b-10 {
  margin-bottom: 10px;
}
.m-l-10 {
  margin-left: 10px;
}
.m-r-10 {
  margin-right: 10px;
}
.m-t-15 {
  margin-top: 15px;
}
.m-b-15 {
  margin-bottom: 15px;
}
.m-l-15 {
  margin-left: 15px;
}
.m-r-15 {
  margin-right: 15px;
}
.m-t-20 {
  margin-top: 20px;
}
.m-b-20 {
  margin-bottom: 20px;
}
.m-l-20 {
  margin-left: 20px;
}
.m-r-20 {
  margin-right: 20px;
}
.m-t-25 {
  margin-top: 25px;
}
.m-b-25 {
  margin-bottom: 25px;
}
.m-l-25 {
  margin-left: 25px;
}
.m-r-25 {
  margin-right: 25px;
}
{{< / highlight >}}

-- -- --

### 6: Nested @for

#### CSS Class Naming

We want to achieve this class naming

* <code>.$(name)-$(subname)-$(number)</code>

#### List Declaration

We define CSS property list:

* property: margin and padding

{{< highlight sass >}}
(margin, padding), (m, p)
{{< / highlight >}}

* each has sub-property: top, bottom, left, right.

{{< highlight sass >}}
(top, bottom, left, right), (t, b, l, r)
{{< / highlight >}}

#### Skeleton

We put both <code>@each</code> iterator,
inside <code>@for</code> loop.

{{< highlight sass >}}
@for $cursor from $loop-begin
to $loop-stop by $interval {

  @each $prop, $name
  in (margin, padding), (m, p) {  
    @each $side, $subname
    in (top, bottom, left, right), (t, b, l, r) {
      ...
    }
  }
}
{{< / highlight >}}

#### PostCSS Source

* [gitlab.com/.../spacing-postcss/src/06-for-nested-each-pairs.css][tutor-postcss-06].

{{< highlight sass >}}
// variable initialization

$loop-begin: 5;
$loop-stop: 25;
$interval:   5;

@for $cursor from $loop-begin
to $loop-stop by $interval {

  @each $prop, $name
  in (margin, padding), (m, p) {  
    @each $side, $subname
    in (top, bottom, left, right), (t, b, l, r) {
      .$(name)-$(subname)-$(cursor) {
        $(prop)-$(side): $(cursor)px;
      }
    }
  }
}
{{< / highlight >}}

#### CSS Result

Now we have both margin and padding.

* [gitlab.com/.../spacing-postcss/dest/06-for-nested-each-pairs.css][tutor-css-06].

{{< highlight css >}}
.m-t-5 {
  margin-top: 5px;
}
.m-b-5 {
  margin-bottom: 5px;
}
.m-l-5 {
  margin-left: 5px;
}
.m-r-5 {
  margin-right: 5px;
}
.p-t-5 {
  padding-top: 5px;
}
.p-b-5 {
  padding-bottom: 5px;
}
.p-l-5 {
  padding-left: 5px;
}
.p-r-5 {
  padding-right: 5px;
}
.m-t-10 {
  margin-top: 10px;
}
.m-b-10 {
  margin-bottom: 10px;
}
.m-l-10 {
  margin-left: 10px;
}
.m-r-10 {
  margin-right: 10px;
}
.p-t-10 {
  padding-top: 10px;
}
.p-b-10 {
  padding-bottom: 10px;
}
.p-l-10 {
  padding-left: 10px;
}
.p-r-10 {
  padding-right: 10px;
}
.m-t-15 {
  margin-top: 15px;
}
.m-b-15 {
  margin-bottom: 15px;
}
.m-l-15 {
  margin-left: 15px;
}
.m-r-15 {
  margin-right: 15px;
}
.p-t-15 {
  padding-top: 15px;
}
.p-b-15 {
  padding-bottom: 15px;
}
.p-l-15 {
  padding-left: 15px;
}
.p-r-15 {
  padding-right: 15px;
}
.m-t-20 {
  margin-top: 20px;
}
.m-b-20 {
  margin-bottom: 20px;
}
.m-l-20 {
  margin-left: 20px;
}
.m-r-20 {
  margin-right: 20px;
}
.p-t-20 {
  padding-top: 20px;
}
.p-b-20 {
  padding-bottom: 20px;
}
.p-l-20 {
  padding-left: 20px;
}
.p-r-20 {
  padding-right: 20px;
}
.m-t-25 {
  margin-top: 25px;
}
.m-b-25 {
  margin-bottom: 25px;
}
.m-l-25 {
  margin-left: 25px;
}
.m-r-25 {
  margin-right: 25px;
}
.p-t-25 {
  padding-top: 25px;
}
.p-b-25 {
  padding-bottom: 25px;
}
.p-l-25 {
  padding-left: 25px;
}
.p-r-25 {
  padding-right: 25px;
}
{{< / highlight >}}

-- -- --

### 7: Final

Now comes the final result.

#### CSS Class Naming

We want to achieve both class naming

* <code>.$(name)-$(number)</code>

* <code>.$(name)-$(subname)-$(number)</code>

#### Skeleton

We put both <code>@each</code> iterator,
inside <code>@for</code> loop.

{{< highlight sass >}}
@for $cursor from $loop-begin
to $loop-stop by $interval {

  @each $prop, $name
  in (margin, padding), (m, p) {  
    ...

    @each $side, $subname
    in (top, bottom, left, right), (t, b, l, r) {
      ...
    }
  }
}
{{< / highlight >}}

#### PostCSS Source

* [gitlab.com/.../spacing-postcss/src/07-final.css][tutor-postcss-07].

{{< highlight sass >}}
// variable initialization

$loop-begin: 5;
$loop-stop: 25;
$interval:   5;

@for $cursor from $loop-begin
to $loop-stop by $interval {

  @each $prop, $name
  in (margin, padding), (m, p) {  
    .$(name)-$(cursor) {
      $(prop): $(cursor)px !important;
    }

    @each $side, $subname
    in (top, bottom, left, right), (t, b, l, r) {
      .$(name)-$(subname)-$(cursor) {
        $(prop)-$(side): $(cursor)px !important;
      }
    }
  }
}
{{< / highlight >}}

Notice that I also put <code>!important</code> in css value.

#### CSS Result

* [gitlab.com/.../spacing-postcss/dest/07-final.css][tutor-css-07].

{{< highlight css >}}
.m-5 {
  margin: 5px !important;
}
.m-t-5 {
  margin-top: 5px !important;
}
.m-b-5 {
  margin-bottom: 5px !important;
}
.m-l-5 {
  margin-left: 5px !important;
}
.m-r-5 {
  margin-right: 5px !important;
}
.p-5 {
  padding: 5px !important;
}
.p-t-5 {
  padding-top: 5px !important;
}
.p-b-5 {
  padding-bottom: 5px !important;
}
.p-l-5 {
  padding-left: 5px !important;
}
.p-r-5 {
  padding-right: 5px !important;
}
.m-10 {
  margin: 10px !important;
}
.m-t-10 {
  margin-top: 10px !important;
}
.m-b-10 {
  margin-bottom: 10px !important;
}
.m-l-10 {
  margin-left: 10px !important;
}
.m-r-10 {
  margin-right: 10px !important;
}
.p-10 {
  padding: 10px !important;
}
.p-t-10 {
  padding-top: 10px !important;
}
.p-b-10 {
  padding-bottom: 10px !important;
}
.p-l-10 {
  padding-left: 10px !important;
}
.p-r-10 {
  padding-right: 10px !important;
}
.m-15 {
  margin: 15px !important;
}
.m-t-15 {
  margin-top: 15px !important;
}
.m-b-15 {
  margin-bottom: 15px !important;
}
.m-l-15 {
  margin-left: 15px !important;
}
.m-r-15 {
  margin-right: 15px !important;
}
.p-15 {
  padding: 15px !important;
}
.p-t-15 {
  padding-top: 15px !important;
}
.p-b-15 {
  padding-bottom: 15px !important;
}
.p-l-15 {
  padding-left: 15px !important;
}
.p-r-15 {
  padding-right: 15px !important;
}
.m-20 {
  margin: 20px !important;
}
.m-t-20 {
  margin-top: 20px !important;
}
.m-b-20 {
  margin-bottom: 20px !important;
}
.m-l-20 {
  margin-left: 20px !important;
}
.m-r-20 {
  margin-right: 20px !important;
}
.p-20 {
  padding: 20px !important;
}
.p-t-20 {
  padding-top: 20px !important;
}
.p-b-20 {
  padding-bottom: 20px !important;
}
.p-l-20 {
  padding-left: 20px !important;
}
.p-r-20 {
  padding-right: 20px !important;
}
.m-25 {
  margin: 25px !important;
}
.m-t-25 {
  margin-top: 25px !important;
}
.m-b-25 {
  margin-bottom: 25px !important;
}
.m-l-25 {
  margin-left: 25px !important;
}
.m-r-25 {
  margin-right: 25px !important;
}
.p-25 {
  padding: 25px !important;
}
.p-t-25 {
  padding-top: 25px !important;
}
.p-b-25 {
  padding-bottom: 25px !important;
}
.p-l-25 {
  padding-left: 25px !important;
}
.p-r-25 {
  padding-right: 25px !important;
}
{{< / highlight >}}

Now you can use this spacing sass in your project.

-- -- --

### 8: Update: XY

For practical reason, I have to update this article.

* Start form zero <code>.0</code>, instead of <code>5</code>.

* Consider X, and Y just like bootstrap.

{{< highlight css >}}
.m-y-5 {
  margin-top: 5px !important;
  margin-bottom: 5px !important;
}

.m-x-5 {
  margin-left: 5px !important;
  margin-right: 5px !important;
}
{{< / highlight >}}

The drawback is, I have bigger stylesheet size.

#### List Declaration

Luckily, sass allow us to have pairs in list.
Now we have CSS sub-property as below:

{{< highlight sass >}}
(margin, padding), (m, p)
{{< / highlight >}}

and

{{< highlight sass >}}
(top, bottom, left, right), (t, b, l, r)
{{< / highlight >}}

and both below

{{< highlight sass >}}
(top, bottom)
(left, right)
{{< / highlight >}}

And the loop is a little bit different:

{{< highlight sass >}}
@for $cursor from $loop-begin
to $loop-stop by $interval {
  @each $prop, $name
  in (margin, padding), (m, p) { 
    ...

    @each $side, $subname
    in (top, bottom, left, right), (t, b, l, r) {
      ...
    }
    .$(name)-y-$(cursor) {
      @each $side in (top, bottom) {
        ...
      }
    }
    .$(name)-x-$(cursor) {
      @each $side in (left, right) {
        ...
      }
    }
  }
}
{{< / highlight >}}

#### PostCSS Source

* [gitlab.com/.../spacing-postcss/src/08-final-xy.sass][tutor-postcss-08].

{{< highlight sass >}}
// variable initialization

$loop-begin: 0;
$loop-stop: 25;
$interval:   5;

@for $cursor from $loop-begin
to $loop-stop by $interval {

  @each $prop, $name
  in (margin, padding), (m, p) {  
    .$(name)-$(cursor) {
      $(prop): $(cursor)px !important;
    }

    @each $side, $subname
    in (top, bottom, left, right), (t, b, l, r) {
      .$(name)-$(subname)-$(cursor) {
        $(prop)-$(side): $(cursor)px !important;
      }
    }
    .$(name)-y-$(cursor) {
      @each $side in (top, bottom) {
        $(prop)-$(side): $(cursor)px !important;
      }
    }
    .$(name)-x-$(cursor) {
      @each $side in (left, right) {
        $(prop)-$(side): $(cursor)px !important;
      }
    }

  }
}
{{< / highlight >}}

#### CSS Result

* [gitlab.com/.../spacing-postcss/dest/08-final-xy.css][tutor-css-08].

__These spacing classes is ready to serve__.

![PostCSS: output][image-ss-css]

-- -- --

### What is Next ?

After this `configuration`, it is a good time to examine `postcss` in action.
Consider continue reading [ [PostCSS - Loop - SugarSS][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2019/10/12/postcss-loop-sugarss/
[local-whats-behind]:   {{< baseurl >}}frontend/2019/10/10/postcss-configuration/

[jmaczan]: https://github.com/jmaczan/bulma-helpers/blob/master/sass/helpers/spacing/margin-padding.sass

[illust-prepro]:    {{< assets-frontend >}}/2019/10/css-preprocessors.png

[sass-loop]:    {{< baseurl >}}frontend/2019/06/21/sass-loop-spacing-class/

[image-ss-postcss]: {{< assets-frontend >}}/2019/10/postcss-for-nested-script.png
[image-ss-css]:     {{< assets-frontend >}}/2019/10/postcss-for-nested-output.png

[tutor-config]:     {{< tutor-css-tools >}}/spacing-postcss/postcss.config.js

[tutor-postcss-00]: {{< tutor-css-tools >}}/spacing-postcss/src/00-comment.css
[tutor-postcss-01]: {{< tutor-css-tools >}}/spacing-postcss/src/01-for.css
[tutor-postcss-02]: {{< tutor-css-tools >}}/spacing-postcss/src/02-for-by.css
[tutor-postcss-03]: {{< tutor-css-tools >}}/spacing-postcss/src/03-each.css
[tutor-postcss-04]: {{< tutor-css-tools >}}/spacing-postcss/src/04-each-pairs.css
[tutor-postcss-05]: {{< tutor-css-tools >}}/spacing-postcss/src/05-for-each-pairs.css
[tutor-postcss-06]: {{< tutor-css-tools >}}/spacing-postcss/src/06-for-nested-each-pairs.css
[tutor-postcss-07]: {{< tutor-css-tools >}}/spacing-postcss/src/07-final.css
[tutor-postcss-08]: {{< tutor-css-tools >}}/spacing-postcss/src/08-final-xy.css

[tutor-css-00]:     {{< tutor-css-tools >}}/spacing-postcss/dest/00-comment.css
[tutor-css-01]:     {{< tutor-css-tools >}}/spacing-postcss/dest/01-for.css
[tutor-css-02]:     {{< tutor-css-tools >}}/spacing-postcss/dest/02-for-by.css
[tutor-css-03]:     {{< tutor-css-tools >}}/spacing-postcss/dest/03-each.css
[tutor-css-04]:     {{< tutor-css-tools >}}/spacing-postcss/dest/04-each-pairs.css
[tutor-css-05]:     {{< tutor-css-tools >}}/spacing-postcss/dest/05-for-each-pairs.css
[tutor-css-06]:     {{< tutor-css-tools >}}/spacing-postcss/dest/06-for-nested-each-pairs.css
[tutor-css-07]:     {{< tutor-css-tools >}}/spacing-postcss/dest/07-final.css
[tutor-css-08]:     {{< tutor-css-tools >}}/spacing-postcss/dest/08-final-xy.css
