---
type   : post
title  : "PostCSS - Loop - SugarSS"
date   : 2019-10-12T09:17:35+07:00
slug   : postcss-loop-sugarss
categories: [frontend]
tags      : [postcss]
keywords  : [loop, spacing, margin, padding]
author : epsi
opengraph:
  image: assets-frontend/2019/10/postcss-sugarss-loop.png

excerpt:
  Enable variables, and loop in SugarSS.
  Using SugarSS along with PreCSS.

toc    : "toc-2019-10-css-tools"

---

### Preface

Now a tricky question, about custom tailored preprocessor.

> What if, I prefer indentation style instead of bracket style?

PostCSS has a `SugarSS` parser that has a nice indentation style,
just like `SASS` or `Stylus`.
The issue is, `SugarSS` does not have any capability as good as `PreCSS`,
such as `$variables`, `@for`, `@each` and `@if`.

The beauty of `postcss` is that you can mix tools together.
Using `SugarSS` along with `PreCSS`,
enable indentation style from `SugarSS`,
while at the same time preserve feature from `PreCSS`.

-- -- --

### Configuration

We are using this configuration:

{{< highlight javascript >}}
module.exports = {
  parser: 'sugarss',
  plugins: [
    require('postcss-strip-inline-comments'),
    require('postcss-each'),
    require('precss'),
    require('postcss-prettify'),
  ],
}
{{< / highlight >}}

If you haven't read the configuration guidance yet,
I suggest you to take one step behind to this article:

* [PostCSS - Configuration][local-whats-behind]

Instead of 

{{< highlight javascript >}}
  syntax: 'postcss-scss',
{{< / highlight >}}

We use

{{< highlight javascript >}}
  parser: 'sugarss',
{{< / highlight >}}

-- -- --

### Precaution

> You cannot use multiline in the syntax.

Since this mix approach is more like a workaround than best practice,
there is a drawback.

Consider this example below:

#### PreCSS Source

Consider this <code>@for</code> loop below:

* [gitlab.com/.../spacing-postcss/src/02-for-by.css][tutor-postcss-02].

{{< highlight sass >}}
// variable initialization

$loop-begin: 5;
$loop-stop: 25;
$interval:   5;

// loop

@for $cursor from $loop-begin
to $loop-stop by $interval {
  .m-$(cursor) {
    margin: $(cursor)px;
  }
}
{{< / highlight >}}

#### SugarSS Source

The correct translation by removing semicolons and brackets is shown as below:

* [gitlab.com/.../spacing-postcss/sugar/02-for-by.css][tutor-sugarss-02].

{{< highlight sass >}}
// variable initialization

$loop-begin: 5
$loop-stop: 25
$interval:   5

// loop

@for $cursor from $loop-begin to $loop-stop by $interval
  .m-$(cursor)
    margin: $(cursor)px
{{< / highlight >}}

#### The Difference

You should write the whole statement, as oneliner as below:

{{< highlight sass >}}
@for $cursor from $loop-begin to $loop-stop by $interval
    .m-$(cursor)
    margin: $(cursor)px
{{< / highlight >}}

And this one below would be fail:

{{< highlight sass >}}
@for $cursor from $loop-begin
to $loop-stop by $interval
  .m-$(cursor)
    margin: $(cursor)px
{{< / highlight >}}

Because it will be translated as

{{< highlight haskell >}}
@for $cursor from $loop-begin;
to $loop-stop by $interval {
  .m-$(cursor) {
    margin: $(cursor)px;
  }
}
{{< / highlight >}}

Note that semicolon after the word `begin`.
This little guy will break your loop statement.

-- -- --

### Complete Example

How about complete example?
The final spacing loop classes.

#### SugarSS Source

* [gitlab.com/.../spacing-postcss/sugar/08-final-xy.sass][tutor-sugarss-08].

{{< highlight sass >}}
// variable initialization

$loop-begin: 0
$loop-stop: 25
$interval:   5

@for $cursor from $loop-begin to $loop-stop by $interval

  @each $prop, $name in (margin, padding), (m, p)
    .$(name)-$(cursor)
      $(prop): $(cursor)px !important

    @each $side, $subname in (top, bottom, left, right), (t, b, l, r)
      .$(name)-$(subname)-$(cursor)
        $(prop)-$(side): $(cursor)px !important

    .$(name)-y-$(cursor)
      @each $side in (top, bottom)
        $(prop)-$(side): $(cursor)px !important

    .$(name)-x-$(cursor)
      @each $side in (left, right)
        $(prop)-$(side): $(cursor)px !important

{{< / highlight >}}

![PostCSS: output][image-ss-sugarss]

#### CSS Result

This will result, about the same output as previous `precss` example.

* [gitlab.com/.../spacing-postcss/dest/08-final-xy.css][tutor-css-08].

-- -- --

### Conclusion

__These spacing classes is ready to serve__.

What do you think ?

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-behind]:   {{< baseurl >}}frontend/2019/10/10/postcss-configuration/

[tutor-postcss-02]: {{< tutor-css-tools >}}/spacing-postcss/src/02-for-by.css
[tutor-sugarss-02]: {{< tutor-css-tools >}}/spacing-postcss/sugar/02-for-by.css
[tutor-sugarss-08]: {{< tutor-css-tools >}}/spacing-postcss/sugar/08-final-xy.css
[tutor-css-08]:     {{< tutor-css-tools >}}/spacing-postcss/dest/08-final-xy.css

[image-ss-sugarss]: {{< assets-frontend >}}/2019/10/postcss-sugarss-loop.png

