---
type   : post
title  : "Sass - Common Implementation"
date   : 2019-01-13T09:17:35+07:00
slug   : sass-common-compiler
categories: [ssg, frontend]
tags      : [hugo, cli]
keywords  : [command line interface, ruby sass, dart sass, node sass]
author : epsi
opengraph:
  image: assets/site/images/topics/sass.png

excerpt:
  Transform Sass into CSS using Sass compiler.
  At least there are three common implementation.

toc    : "toc-2019-01-sass-watch"

---

### Preface

> Goal: Transform Sass into CSS using Sass compiler.

There are a few options to compile Sass.

*	Common Compiler: ruby-sass, dart-sass, and node-sass.

*	LibSass Wrapper: SassC, sass.cr, go-libsass, jsass, sass.js, lua-sass, libsass-net, node-sass, CSS::Sass, SassPHP, libsass-python, sassc-ruby, sass_rs, and Sass-Scala.

*	Task Runner: Gulp, Grunt. And maybe also these: Brunch, Broccoli, WebPack, and Parcel.

#### Reading

This article only explain common compiler.
These three has been mentioned in official documentation.

* [sass-lang.com](http://sass-lang.com/)

#### Table of Content

* Preface: Table of Content

* Prepare

* 1: deprecated ruby-sass

* 2: dart-sass

* 3: node-sass

* What is Next ?

-- -- --

### Prepare

We need an example of Sass.
Any example is okay.
You can use any.

#### Example Sass

I already have a Sass project
You can just <code>git clone</code> the repository to get that Sass case.

* [gitlab.com demo-hugo/](https://gitlab.com/epsi-rns/demo-hugo/tree/master/themes/oriclone)

{{< highlight bash >}}
$ git clone https://epsi-rns.gitlab.io/demo-hugo/
{{< / highlight >}}

![sass case: example in tree][image-ss-tree-sass]

-- -- --

### 1: deprecated ruby-sass

Ruby Sass has been officialy deprecated.
It also very common.
There are already tons of tutorials.
So I won't talk about it know more.

#### Reading

* [sass-lang.com/ruby-sass](http://sass-lang.com/ruby-sass)

#### Example in Other Reference

I have been explain ruby-sass a few times in this blog.
As part of Bootstrap, part of Jekyll, and part of Hugo.

* [Hugo - Bootstrap - SASS][local-hugo-sass]

#### Command

I have a need to show the command,
because each sass compiler implementation might have a few differences.

{{< highlight bash >}}
$ sass --watch -I sass sass/themes/oriclone:static/assets/css --style compressed --sourcemap=none
{{< / highlight >}}

-- -- --

### 2: dart-sass

The deprecated ruby-sass has been replaced with dart-sass.

#### Reading

* [sass-lang.com/dart-sass](http://sass-lang.com/dart-sass)

#### Installing

The issue with dart is, this good language is not friendly to linux distribution,
other than apt-based. Fortunately, installing dart is easy.
All you need is download, extract and voila, it works.

* [github.com dart-sass 1.16.1](https://github.com/sass/dart-sass/releases/tag/1.16.1)

#### in Windows

Installing Dart in Windows is even easier using Chocolatey.

![Windows cmder: choco install sass][image-ss-choco-install-sass]

#### Command

Dart-sass options is a little bit different than the ruby-sass counterpart.

I put my downloaded archive in <code>/media/Works/</code>.
Your place might be different.

{{< highlight bash >}}
$ /media/Works/dart-sass/sass --watch -I sass sass/themes/oriclone:static/assets/css --style=compressed --no-source-map
{{< / highlight >}}

![dart-sass: in action][image-ss-dart-sass]

For easy of use, you can make alias,
in <code>.bashrc</code> or <code>.zshrc</code>.

{{< highlight bash >}}
alias dart-sass=/media/Works/dart-sass/sass
{{< / highlight >}}

-- -- --

### 3: node-sass

node-sass is very common, but if you are a <code>NodeJS</code> beginner,
you should be aware about setting up <code>NPM</code> environment.

#### Reading

* [sass-lang.com/install](http://sass-lang.com/install)

#### Setting-up NPM Environment

> No need any sudo!

As a beginner of NodeJS in linux,
I hit permission issue a few times,
until I find this good article:

* [Resolving EACCES permissions errors when installing packages globally][resolving-eaccess]

It is basically, just setup global path for regular linux user.

![node npm: setup global path][image-ss-npm-path]

Do not forget to add to <code>.bashrc</code> or <code>.zshrc</code>:

{{< highlight bash >}}
export PATH="$PATH:$HOME/.npm-global/bin"
{{< / highlight >}}

#### Installing

Now you can just do install globally,
without any need of <code>sudo</code> command.

{{< highlight bash >}}
$ npm install -g node-sass
{{< / highlight >}}

![npm: install node-sass][image-ss-install-node-sass]

#### Command

Now you can use this <code>node-sass</code> in your command line.
Node-sass options is also a little bit different than the ruby-sass counterpart.

{{< highlight bash >}}
$ node-sass --watch --include-path sass sass/themes/oriclone --output static/assets/css --output-style compressed --omit-source-map-url
{{< / highlight >}}

![node-sass: in action][image-ss-node-sass]

#### Using Script

I have got this remarkable idea from Bulma Documentation:

* [Bulma Customize with Node Sass][with-node-sass]

We can actually shorten the script,
using <code>packages.json</code> by adding this line below

{{< highlight json >}}
  "scripts": {
    "css-build": "node-sass --include-path sass sass/themes/oriclone --output static/assets/css",
    "css-watch": "npm run css-build -- --watch --output-style compressed --omit-source-map-url",
    "start": "npm run css-watch"
  },
{{< / highlight >}}

It is really up to you, how you configure the script.
Everyone have their on use case, and require different behaviour.

![node-sass: in action][image-ss-npm-run-css-watch]

It is simply shorter.

-- -- --

### What is Next ?

The issue is, the command is long.
We can solve this later using task runner,
such as gulp or grunt, and a little introduction to webpack.

There is other article, that you might need to read.
Consider continue reading [ [Sass - Task Runner][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2019/01/15/sass-task-runner/
[local-hugo-sass]:      {{< baseurl >}}ssg/2018/09/07/hugo-bootstrap-sass/

[resolving-eaccess]:    https://docs.npmjs.com/resolving-eacces-permissions-errors-when-installing-packages-globally
[with-node-sass]:       https://bulma.io/documentation/customize/with-node-sass/

[image-ss-dart-sass]:   {{< assets-frontend >}}/2019/01/fedora-dart-sass.png
[image-ss-node-sass]:   {{< assets-frontend >}}/2019/01/fedora-node-sass-watch.png
[image-ss-tree-sass]:   {{< assets-frontend >}}/2019/01/sass-oriclone-tree.png
[image-ss-npm-path]:    {{< assets-frontend >}}/2019/01/fedora-npm-global-setup-path.png

[image-ss-choco-install-sass]:  {{< assets-frontend >}}/2019/01/windows-03-choco-install-sass.png
[image-ss-install-node-sass]:   {{< assets-frontend >}}/2019/01/fedora-npm-install-node-sass.png
[image-ss-npm-run-css-watch]:   {{< assets-frontend >}}/2019/01/fedora-npm-run-css-watch.png
