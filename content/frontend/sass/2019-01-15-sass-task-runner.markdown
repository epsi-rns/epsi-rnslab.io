---
type   : post
title  : "Sass - Task Runner"
date   : 2019-01-15T09:17:35+07:00
slug   : sass-task-runner
categories: [ssg, frontend]
tags      : [hugo, cli]
keywords  : [command line interface, sass, task runner, gulp, grunt, webpack]
author : epsi
opengraph:
  image: assets/site/images/topics/sass.png

excerpt:
  Transform Sass into CSS
  using Task Runner such as Gulp and Grunt.
  And bundler such as Webpack.

toc    : "toc-2019-01-sass-watch"

---

### Preface

> Goal: Transform Sass into CSS using Task Runner.

#### Prepare Example Sass

I'm still using Sass project from the previous example.
The <code>oriclone</code> theme, from <code>demo hugo</code>.

#### Table of Content

* Preface: Table of Content

* 1: Gulp

* 2: Grunt

* 3: Webpack

* What is Next ?

-- -- --

### 1: Gulp

Gulp is NodeJS package.
You need <code>gulp</code> package, <code>gulp-sass</code>, and <code>node-sass</code>.

#### Reading

The official Site is

* [gulpjs.com](https://gulpjs.com/docs/en/getting-started/quick-start)

#### Install

You might do differently using <code>--save-dev</code>.

{{< highlight bash >}}
$ npm install -g gulp
$ npm install -g gulp-sass
{{< / highlight >}}

![npm: install gulp][image-ss-install-gulp]

and

![npm: install gulp-sass][image-ss-install-gulp-sass]

Note that we have already install <code>node-sass</code>,
in previous article.

#### Config

I have made a <code>gulpfile.js</code> to handle only Sass compilation to CSS.

{{< highlight javascript >}}
'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var sassOptions = {
    includePaths: ['sass'],
    outputStyle: 'compressed'
  };

sass.compiler = require('node-sass');

//compile
gulp.task('sass', function () {
  return gulp.src('sass/themes/oriclone/*.scss')
    .pipe(sass(sassOptions).on("error", sass.logError))
    .pipe(gulp.dest('static/assets/css'));
});

gulp.task('sass:watch', function () {
  gulp.watch('sass/themes/oriclone/*.scss', gulp.series('sass'));
});
{{< / highlight >}}

![gulp: config gulpfile.js][image-ss-vim-gulpfile]

#### Issue with Gulp

If you run gulp, gulp will complain about local gulp something.

{{< highlight bash >}}
$ gulp watch:sass
Local gulp not found in ...
{{< / highlight >}}

This can be solved with <code>npm link</code>.

{{< highlight bash >}}
$ npm link node-sass gulp gulp-sass
/media/Works/sites/oriclone/node_modules/gulp-sass -> /home/epsi/.npm-global/lib/node_modules/gulp-sass
/media/Works/sites/oriclone/node_modules/gulp -> /home/epsi/.npm-global/lib/node_modules/gulp
/media/Works/sites/oriclone/node_modules/node-sass -> /home/epsi/.npm-global/lib/node_modules/node-sass
{{< / highlight >}}

![npm: link packages][image-ss-npm-link-package]

Now the gulp will run just fine.

#### Command

Command argument in Gulp, is following your config.

{{< highlight bash >}}
$ gulp watch:sass
{{< / highlight >}}

Consider change any of the sass file,
then save to trigger the watch action in gulp.

![gulp: watch in action][image-ss-gulp-watch]

-- -- --

### 2: Grunt

Grunt is NodeJS package.
You need <code>grunt</code> package, and optionally <code>node-sass</code>.

#### Reading

The official Site is

* [gruntjs.com](https://gruntjs.com/getting-started)

#### Install

You might do differently using <code>--save-dev</code>.

{{< highlight bash >}}
$ npm install -g grunt
{{< / highlight >}}

![npm: install grunt][image-ss-install-grunt]

#### Config

I have made a <code>Gruntfile.js</code> to handle only Sass compilation to CSS.

{{< highlight javascript >}}
module.exports = function(grunt) {

  grunt.initConfig({
    sass: {                              // Task
      dist: {                            // Target
        options: {                       // Target options
          style: 'compressed',
          loadPath: ['sass'],
          sourcemap: 'none'
        },
        files: [{
          expand: true,
          cwd: 'sass/themes/oriclone',
          src: ['*.scss'],
          dest: 'static/assets/css',
          ext: '.css'
        }]
      }
    },
    watch: {
      css: {
        files: 'sass/themes/oriclone/*.scss',
        tasks: ['sass']
      }
    }
  });
 
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
 
  grunt.registerTask('default', ['watch', 'sass']);

};
{{< / highlight >}}

![grunt: config Gruntfile.js][image-ss-vim-gruntfile]

#### Command

Command argument in Grunt, is following your config.

{{< highlight bash >}}
$ grunt watch
{{< / highlight >}}

Consider change any of the sass file,
then save to trigger the watch action in grunt.

![grunt: watch in action][image-ss-grunt-watch]

-- -- --

### 3: Webpack

Webpack is a bundler. 
Not just a task runner.

#### Reading

The official Site is

* [webpack.js.org/](https://webpack.js.org/)

#### Install

Using <code>--save-dev</code>, in project directory.

{{< highlight bash >}}
$ npm install -D node-sass webpack webpack-cli extract-text-webpack-plugin@next css-loader sass-loader style-loader url-loader file-loader
{{< / highlight >}}

![npm: install webpack: top][image-ss-npm-webpack-top]

And after long lines and processing time, we will have:

![npm: install webpack: bottom][image-ss-npm-webpack-bottom]

#### Bundler vs Task Runner

We just cannot expect bundler to behave like task runner,
because of different concept they have.

In my example, I move the image assets, into source directory.
And bundler will generate image assets required in destination path.
And will not generate unneeded image assets.

#### Config

I have made a <code>webpack.config.js</code>,
to handle Sass compilation to CSS.

{{< highlight javascript >}}
const path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
  entry: {
    'bootstrap.css': './sass/themes/oriclone/bootstrap.scss',
    'fontawesome.css': './sass/themes/oriclone/fontawesome.scss',
    'main.css': './sass/themes/oriclone/main.scss'
  },
  output: {
	path: path.resolve(__dirname, 'static/assets')
  },
  module: {
    rules: [{
      test    : /\.(png|jpg|svg|gif)$/,
      use: [{
        loader: 'file-loader',
        options: { 
          name: 'images/[name].[ext]'
        } 
      }]
    },{
      test: /\.scss$/,
      use: ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: [
          { loader: 'css-loader' },
          { loader: 'sass-loader',
            options: {
              includePaths: ["sass"]
            }
          },
        ]
      })
    }]
  },
  plugins: [
    new ExtractTextPlugin('css/[name]'),
  ]
};
{{< / highlight >}}

![webpack: config webpack.config.js][image-ss-vim-webpack]

#### Command

Running webpack command in Webpack.

{{< highlight bash >}}
$ webpack --mode production
{{< / highlight >}}

![webpack: build in action][image-ss-webpack-production]

I must admit, I'm a n00b, where it comes to webpack.
I do not know much about how to configure webpack in daily life.

-- -- --

### What is Next ?

There are other method as well using Builder such as Brunch or Broccolli.
And using bundler such as Parcel. But I think it is enough for now.

There is other article, that you might need to read.
Consider continue reading [ [Windows - Chocolatey - Sass][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2019/01/23/windows-choco-sass/

[resolving-eaccess]:    https://docs.npmjs.com/resolving-eacces-permissions-errors-when-installing-packages-globally

[image-ss-grunt-watch]: {{< assets-frontend >}}/2019/01/fedora-grunt-sass-watch.png
[image-ss-gulp-watch]:  {{< assets-frontend >}}/2019/01/fedora-gulp-sass-watch.png

[image-ss-install-grunt]:       {{< assets-frontend >}}/2019/01/fedora-npm-install-grunt.png
[image-ss-install-gulp]:        {{< assets-frontend >}}/2019/01/fedora-npm-install-gulp.png
[image-ss-install-gulp-sass]:   {{< assets-frontend >}}/2019/01/fedora-npm-install-gulp-sass.png

[image-ss-npm-link-package]:    {{< assets-frontend >}}/2019/01/fedora-npm-link-gulp-sass.png

[image-ss-vim-gruntfile]:       {{< assets-frontend >}}/2019/01/fedora-vim-gruntfile.png
[image-ss-vim-gulpfile]:        {{< assets-frontend >}}/2019/01/fedora-vim-gulpfile.png
[image-ss-vim-webpack]:         {{< assets-frontend >}}/2019/01/fedora-vim-webpack-config.png

[image-ss-npm-webpack-bottom]:  {{< assets-frontend >}}/2019/01/fedora-npm-install-webpack-and-friends-bottom.png
[image-ss-npm-webpack-top]:     {{< assets-frontend >}}/2019/01/fedora-npm-install-webpack-and-friends-top.png

[image-ss-webpack-production]:  {{< assets-frontend >}}/2019/01/fedora-webpack-mode-production.png
