---
type   : post
title  : "Windows - Chocolatey - Sass"
date   : 2019-01-23T09:17:35+07:00
slug   : windows-choco-sass
categories: [ssg, frontend]
tags      : [hugo, cli]
keywords  : [command line interface, windows, chocolatey, sass]
author : epsi
opengraph:
  image: assets/site/images/topics/sass.png

toc    : "toc-2019-01-windows-cli"

excerpt:
  Windows can have powerful command line environment using
  chocolatey package manager and cmder terminal.
  Sass can make use of these tools,
  to make you feels like home.
  Mimicing linux environment.

---

### Preface

> Goal: Using comfortable CLI environment in Windows for Sass.

#### Table of Content

* Preface: Table of Content

* 1: Windows: Setting up CLI Environment

* 2: Sass: Install

* 3: Sass: Options

* What is Next ?

-- -- --

### 1: Windows: Setting up CLI Environment

I have written the basic setup in separate article in my other blog.
You should read this before you begin:

*	[Windows: Setting up CLI Environment][github-windows-cli]

-- -- --

### 2: Sass: Install

Installing is pretty easy.

{{< highlight bash >}}
$ choco install sass
{{< / highlight >}}

![Windows cmder: choco install sass][image-ss-choco-install-sass]

I'm using Sass that is built on top of dart.
As alternative, you can use <code>node-sass</code>.
But unfortunately, at the moment, I cannot find it in choco package list.

You might experience this issue as below, on first time running.

{{< highlight bash >}}
$ sass
'dart.exe' is not recognized as an internal or external command,
operable program or batch file.
{{< / highlight >}}

![Windows cmder: sass dart issue][image-ss-cmder-dart-issue]

Just close the terminal,
and open sass in new terminal. 
Sass will be running just fine.

![Windows cmder: sass options][image-ss-cmder-sass-options]

-- -- --

### 3: Sass: Options

Dart-sass options is a little bit different than the ruby-sass counterpart.

#### Example Sass

We need an example of Sass.
any example is okay.
You can use any.

{{< highlight bash >}}
$ git clone https://epsi-rns.gitlab.io/demo-hugo/
{{< / highlight >}}

![Windows cmder: git clone][image-ss-cmder-git-clone]

#### in Action

Now you can try Sass as usual, in windows <code>cmder</code>.

{{< highlight bash >}}
$ cd D:\cli\demo-hugo\themes\oriclone\
$ sass --watch -I sass  sass/themes/oriclone:static/assets/css --style=compressed --no-source-map
Sass is watching for changes. Press Ctrl-C to stop.
{{< / highlight >}}

![Windows cmder: sass in action][image-ss-cmder-sass-action]

For our comparation.
Our ruby counterpart is using options as below

{{< highlight bash >}}
$ sass --watch -I sass  sass/themes/oriclone:static/assets/css --style compressed --sourcemap=none
{{< / highlight >}}

-- -- --

### What is Next ?

There is other article, that you might need to read.
Consider continue reading [ [Windows - Chocolatey - Hugo][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2019/01/21/windows-choco-hugo/
[github-windows-cli]:       https://epsi-rns.github.io/system/2019/01/21/windows-cli-environment.html

[image-ss-cmder-git-clone]:     {{< assets-frontend >}}/2019/01/windows-02-cmder-git-clone-demo.png
[image-ss-choco-install-sass]:  {{< assets-frontend >}}/2019/01/windows-03-choco-install-sass.png
[image-ss-cmder-sass-action]:   {{< assets-frontend >}}/2019/01/windows-03-cmder-sass-action.png
[image-ss-cmder-dart-issue]:    {{< assets-frontend >}}/2019/01/windows-03-cmder-sass-dart-issue.png
[image-ss-cmder-sass-options]:  {{< assets-frontend >}}/2019/01/windows-03-cmder-sass-options.png
