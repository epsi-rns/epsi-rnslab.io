---
type   : post
title  : "Bezier SVG - Introduction"
date   : 2022-01-11T09:17:35+07:00
slug   : bezier-svg-01
categories: [frontend]
tags      : [svg]
keywords  : [bezier, path, mdn]
author : epsi
opengraph:
  image: assets/posts/frontend/2022/01-bezier/01-path-svg-on-browser-with-mouse.png

toc    : "toc-2022-02-animation"

excerpt:
  Examining Bezier Path form Official MDN.

---

### Preface

> Goal: Examining Bezier Path form Official MDN.

Call me stupid.
Bu I really have difficulties,
on understanding bezier line.
The fact that SVG has some potential challenge me,
to learn from the very bottom.

#### MDN Documents

Luckily there is this very good official MDN explanation.
However, I still need to examine deeper,
with my own modification.

* [Scalable Vector Graphics: Paths][mdn-docs]

#### Path Tag

> Code and Curves!

So we have this bezier line feature in HTML.
1. What is it look like?
2. How exactly do we have to write?
3. What do we want to achieve?

![SVG: Codes and Curves][codes-and-curves]

You can see there are nine lines,
in background figure in browser above.
You can spot the fifth line in ViM editor above.

#### Codepen Demo

After a while, I can make my own simple 2D animation.
Not as advance as drawing 3D vortex,
but enough for beginner like me.

* [SVG Bezier Curves Animation][codepen]

{{< embed-video width="480" height="580"
    src="assets/posts/frontend/2022/01-bezier/33-output-tall.mp4" >}}

It is still CPU expensive.
But hey, this is my first codepen.
Maybe I could be better someday.

#### Where do we start?

> From simple!

I promise you, the basic part will be boring.
But don't worry, we will get our animation,
after we examine the MDN example.

-- -- --

### Common Page Setup

#### Viewbox

The original code from MDN is as below:

{{< highlight html >}}
<svg width="190" height="160" xmlns="http://www.w3.org/2000/svg">
  ...
</svg>
{{< / highlight >}}

To make this fitted in our browser width we can rewrite as below:

{{< highlight html >}}
<svg viewbox="0 0 190 160"
     xmlns="http://www.w3.org/2000/svg">
  ...
</svg>
{{< / highlight >}}

I also add rectangle, to make the axes clear.

{{< highlight html >}}
<svg viewbox="0 0 190 160"
     xmlns="http://www.w3.org/2000/svg">
  <rect x="0" y="0" width="190" height="160"
        stroke="black" fill="transparent"/>
  ...
</svg>
{{< / highlight >}}

We need to compare the result with X axes, and Y axes.
This is the purpose of rectangle.

#### Stylesheet: Bezier Area

We need to setup simple stylesheet, for the box area.

* [gitlab.com/.../10-bezier.css][css-10-bezier]

{{< highlight css >}}
.box {
  stroke: gray;
  stroke-width: 0.5px;
  fill  : #64B5F6;
}
.box.small {
  width : 40px;
  height: 20px;
}
.box.big {
  width : 200px;
  height: 200px;
}
{{< / highlight >}}

You might recognize a few strange properties.
This is not HTML properties, but rather SVG properties.
And sure, it can be styled in CSS as well.

#### Stylesheet: Helper Line

And also for the helper line.

{{< highlight css >}}
.line  { stroke-width: 0.5px; }
.line.blue  { stroke: blue; }
.line.green { stroke: green; }
.line.red   { stroke: red; }
{{< / highlight >}}

#### Complete Page

Our common complete page is as below.

* [gitlab.com/.../12-bezier-a.html][html-12-bezier-a]

{{< highlight html >}}
<!DOCTYPE html>
<html lang="en">

<head>
  <title>Bezier Curves</title>
  <link rel="stylesheet"
        href="10-bezier.css"/>
</head>

<body>

<svg viewbox="0 0 190 160"
     stroke="black" fill="transparent">
  <rect x="0" y="0" width="190" height="160"/>
  ...
</svg>

</body>
</html>

{{< / highlight >}}

We are going to add/remove stylesheet or javascript,
as needed later. Nothing special.

-- -- --

### What is Next ?

This should be enough for starting point.
We are going to continue with cubic bezier curves.

Consider continue reading [ [Bezier SVG - MDN Path - Cubic][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2022/01/13/bezier-svg-02/


[mdn-docs]: https://developer.mozilla.org/en-US/docs/Web/SVG/Tutorial/Paths
[codepen]:  https://codepen.io/epsi-rns/pen/gOXewQo

[codes-and-curves]:     {{< baseurl >}}assets/posts/frontend/2022/01-bezier/01-path-svg-on-browser-with-mouse.png

[html-12-bezier-a]:     {{< tutor-css-tools >}}/bezier/12-bezier-a.html

[css-10-bezier]:        {{< tutor-css-tools >}}/bezier/10-bezier.css

[16-mdn]:   {{< baseurl >}}assets/posts/frontend/2022/01-bezier/16-mdn.png
[17-mdn]:   {{< baseurl >}}assets/posts/frontend/2022/01-bezier/17-mdn.png
[18-mdn]:   {{< baseurl >}}assets/posts/frontend/2022/01-bezier/18-mdn.png

[html-16-bezier]:       {{< tutor-css-tools >}}/bezier/16-bezier.html
[html-17-bezier]:       {{< tutor-css-tools >}}/bezier/17-bezier.html
[html-18-bezier]:       {{< tutor-css-tools >}}/bezier/18-bezier.html
