---
type   : post
title  : "Bezier SVG - MDN Path - Cubic"
date   : 2022-01-13T09:17:35+07:00
slug   : bezier-svg-02
categories: [frontend]
tags      : [svg, css]
keywords  : [bezier, path, mdn]
author : epsi
opengraph:
  image: assets/posts/frontend/2022/01-bezier/12-mdn-c.png

toc    : "toc-2022-02-animation"

excerpt:
  Examining Bezier Path form Official MDN.

---

### Preface

> Goal: Examining Bezier Path form Official MDN.

#### MDN Documents

This article is based on:

* [Scalable Vector Graphics: Paths][mdn-docs]

The right place to start to learn is,
from the manual book.

-- -- --

### Cubic Bezier Curve

#### Original Code

The original code from MDN is as below:

{{< highlight html >}}
  <path d="M 10 10 C 20 20, 40 20, 50 10" stroke="black" fill="transparent"/>
  <path d="M 70 10 C 70 20, 110 20, 110 10" stroke="black" fill="transparent"/>
  <path d="M 130 10 C 120 20, 180 20, 170 10" stroke="black" fill="transparent"/>
{{< / highlight >}}

There are actually nine lines,
bu I write only three for simplicity.

#### Indented Code

> Proper Indentation for Learning Purpose

We can write down the code form MDN above without any changes,
but with clearer indentation.

* [gitlab.com/.../12-bezier-a.html][html-12-bezier-a]

{{< highlight html >}}
  <rect x="10" y="10" width="40" height="20"
        stroke="gray" fill="transparent"/>
  <path d="M 10 10
           C 20 20, 40 20, 50 10"
        stroke="black" fill="transparent"/>
  <path d="M 70 10
           C 70 20, 110 20, 110 10"
        stroke="black" fill="transparent"/>
  <path d="M 130 10
           C 120 20, 180 20, 170 10"
        stroke="black" fill="transparent"/>
{{< / highlight >}}

You can see the result below:

![SVG MDN: Cubic Bezier Curve: Original][12-mdn-a]

We can examine better with proper indentation.

* Starting Point (Move): `M 10 10`.
* Cubic `C 20 20, 40 20, 50 10`.

#### Using Relative Axes with Grouping

So what is it anyway with that small rectangle?
Well, it is going to be easier to use relative axes,
than absolute one.

We can do it by grouping the object together,
such as this example code below:

* [gitlab.com/.../12-bezier-b.html][html-12-bezier-b]

{{< highlight html >}}
  <g transform="translate(10,10)" fill="#E3F2FD">
      <rect x="0" y="0" width="40" height="20"
            stroke="gray"/>
      <path d="M 0  0 C 10 10, 30 10, 40 0"/>
  </g>
{{< / highlight >}}

Since we have three lines with the same Y axes.
We can group them by Y axes first,
then grouping by X axes,
then we can have the relative coordinate.

{{< highlight html >}}
  <g transform="translate(0,10)" fill="#E3F2FD">
    <g transform="translate(10,0)">
      <rect x="0" y="0" width="40" height="20"
            stroke="gray"/>
      <path d="M 0  0 C 10 10, 30 10, 40 0"/>
    </g>
    <g transform="translate(70,0)">
      <path d="M 0 0 C 0 10, 40 10, 40 0"/></g>
    <g transform="translate(130,0)">
      <path d="M 0 0 C -10 10, 50 10, 40 0"/></g>
  </g>
{{< / highlight >}}

At last, I add fill color,
so you can understand that path is,
rather an area than just line.

![SVG MDN: Cubic Bezier Curve: Relative Axes][12-mdn-b]

Please do not stop with the figure result.
Our goal is to examine the coordinate in the code,
rather than enjoy the preview.

#### Line Direction Helper

So again what is it with the curve?
What is happening behind the scene?

We can write down some line,
based on the relative point above,
to get better understanding.

This time with help of stylesheet.
For example the first curve.

* [gitlab.com/.../12-bezier-c.html][html-12-bezier-c]

{{< highlight html >}}
  <g transform="translate(0,10)" fill="#E3F2FD">
    <g transform="translate(10,0)">
      <rect x="0" y="0" class="box small"/>
      <path d="M  0  0 C 10 10, 30 10, 40 0"/>
      <path d="M  0  0 L 10 10" class="line blue"/>
      <path d="M 10 10 L 30 10" class="line blue"/>
      <path d="M 40  0 L 30 10" class="line blue"/>
    </g>
    ...
  </g>
{{< / highlight >}}

The second curve.

{{< highlight html >}}
  <g transform="translate(0,10)" fill="#E3F2FD">
    ...
    <g transform="translate(70,0)">
      <path d="M  0  0 C  0 10, 40 10, 40 0"/>
      <path d="M  0  0 L  0 10" class="line green"/>
      <path d="M  0 10 L 40 10" class="line green"/>
      <path d="M 40  0 L 40 10" class="line green"/>
    </g>
    ...
  </g>
{{< / highlight >}}

And finally the third curve.

{{< highlight html >}}
  <g transform="translate(0,10)" fill="#E3F2FD">
    ...
    <g transform="translate(130,0)">
      <path d="M   0  0 C -10 10, 50 10, 40 0"/>
      <path d="M   0  0 L -10 10" class="line red"/>
      <path d="M -10 10 L  50 10" class="line red"/>
      <path d="M  40  0 L  50 10" class="line red"/>
    </g>
  </g>
{{< / highlight >}}

With the result as below figure:

![SVG MDN: Cubic Bezier Curve: Line Direction][12-mdn-c]

With this helper line I can examine,
the relationship between the curve and the line.

-- -- --

### Stringed Cubic Bezier Curve

#### Original Code

The original code from MDN is as below:

{{< highlight html >}}
  <path d="M 10 80 C 40 10, 65 10, 95 80 S 150 150, 180 80" stroke="black" fill="transparent"/>
{{< / highlight >}}

There is only one line.
But multiple points of direction that we should examine about.

#### Indented Code

With proper indentation and rectangle border,
we can write down as below code:

* [gitlab.com/.../13-bezier-a.html][html-13-bezier-a]

{{< highlight html >}}
  <svg xmlns="http://www.w3.org/2000/svg"
       viewbox="0 0 190 160">
    <rect x="0" y="0" width="190" height="160"
          stroke="gray" fill="transparent"/>
    <path d="M 10 80 
             C 40 10, 65 10, 95 80
             S 150 150, 180 80"
          stroke="black" fill="transparent"/>
  </svg>
{{< / highlight >}}

You can see the result below:

![SVG MDN: Stringed Cubic Bezier Curve: Original][13-mdn-a]

We can examine better with proper indentation.

* Move: `M 10 80`.
* Cubic `C 40 10, 65 10, 95 80`.
* Stringed `S 150 150, 180 80`.

#### Redefine Coordinate

It is not easy to examine a curve with dimension 190x160 area.
How about moving the *viewbox* to fit 200x200 area?
And put the X axis right in the middle coordinate.

{{< highlight html >}}
  <svg xmlns="http://www.w3.org/2000/svg"
        viewbox="0 0 200 200">
    ...
  </svg>
{{< / highlight >}}

The middle is at 100 height.
This way we can examine easier
The code above can be rewritten as below code:

* [gitlab.com/.../13-bezier-b.html][html-13-bezier-b]

{{< highlight html >}}
  <svg xmlns="http://www.w3.org/2000/svg"
        viewbox="0 0 200 200">
    <rect x="0" y="0" width="200" height="200"
          stroke="gray" fill="transparent"/>
    <path d="M   0 100
             C  50  30,  65  30, 95 100
             S 150 170, 200 100"
          stroke="black" fill="transparent"/>
  </svg>
{{< / highlight >}}

Instead of starting from 10 to 180,
we start from 0 (zero) to 200.

![SVG MDN: Stringed Cubic Bezier Curve: Redefining Coordinates][13-mdn-b]

Our goal is still to examine the coordinate in the code.

#### Line Direction Helper

So again what is it with the curve?
What is exactly going on behind the scene?

Consider this code below:

* [gitlab.com/.../13-bezier-c.html][html-13-bezier-c]

{{< highlight html >}}
  <svg xmlns="http://www.w3.org/2000/svg"
        viewbox="0 0 200 200">
    <rect x="0" y="0" class="box big"/>
    <path d="M 00 100
             C 50 30, 65 30, 95 100
             S 150 170, 200 100"
          stroke="black" fill="transparent"/>
    <path d="M    0 100 L  50  30" class="line blue"/>
    <path d="M   50  30 L  65  30" class="line blue"/>
    <path d="M   65  30 L  95 100" class="line blue"/>
  </svg>
{{< / highlight >}}

> It looks like something is missing.

Since the original point from stringed cannot explain what happened,
I manually add the green line as below:

{{< highlight html >}}
    <path d="M    0 100 L  50  30" class="line blue"/>
    <path d="M   50  30 L  65  30" class="line blue"/>
    <path d="M   65  30 L  95 100" class="line blue"/>

    <path d="M   95 100 L 125 170" class="line green"/>

    <path d="M   95 100 L 150 170" class="line blue"/>
    <path d="M  150 170 L 200 100" class="line blue"/>
{{< / highlight >}}

With the result as below figure:

![SVG MDN: Stringed Cubic Bezier Curve: Line Direction][13-mdn-c]

I know, string can be rather confusing.
But at least we can have the big picture,
to get the relationship between the curve and the line.

-- -- --

### What is Next ?

After cubic curves,
we are going to continue with quadratic curves.

Consider continue reading [ [Bezier SVG - MDN Path - Quadratic][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2022/01/15/bezier-svg-03/

[mdn-docs]: https://developer.mozilla.org/en-US/docs/Web/SVG/Tutorial/Paths

[12-mdn-a]: {{< baseurl >}}assets/posts/frontend/2022/01-bezier/12-mdn-a.png
[12-mdn-b]: {{< baseurl >}}assets/posts/frontend/2022/01-bezier/12-mdn-b.png
[12-mdn-c]: {{< baseurl >}}assets/posts/frontend/2022/01-bezier/12-mdn-c.png
[13-mdn-a]: {{< baseurl >}}assets/posts/frontend/2022/01-bezier/13-mdn-a.png
[13-mdn-b]: {{< baseurl >}}assets/posts/frontend/2022/01-bezier/13-mdn-b.png
[13-mdn-c]: {{< baseurl >}}assets/posts/frontend/2022/01-bezier/13-mdn-c.png

[image-site-desc]:      {{< baseurl >}}assets/posts/frontend/2022/02-observer/01-sitedesc-sfme.png
[png-blue-sfme]:        {{< tutor-css-tools >}}/bezier/blue-sfme.png

[html-12-bezier-a]:     {{< tutor-css-tools >}}/bezier/12-bezier-a.html
[html-12-bezier-b]:     {{< tutor-css-tools >}}/bezier/12-bezier-b.html
[html-12-bezier-c]:     {{< tutor-css-tools >}}/bezier/12-bezier-c.html
[html-13-bezier-a]:     {{< tutor-css-tools >}}/bezier/13-bezier-a.html
[html-13-bezier-b]:     {{< tutor-css-tools >}}/bezier/13-bezier-b.html
[html-13-bezier-c]:     {{< tutor-css-tools >}}/bezier/13-bezier-c.html
