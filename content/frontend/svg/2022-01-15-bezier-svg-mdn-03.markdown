---
type   : post
title  : "Bezier SVG - MDN Path - Quadratic"
date   : 2022-01-15T09:17:35+07:00
slug   : bezier-svg-03
categories: [frontend]
tags      : [svg, css]
keywords  : [bezier, path, mdn]
author : epsi
opengraph:
  image: assets/posts/frontend/2022/01-bezier/15-mdn-c.png

toc    : "toc-2022-02-animation"

excerpt:
  Examining Bezier Path form Official MDN.

---

### Preface

> Goal: Examining Bezier Path form Official MDN.

#### MDN Documents

This article is based on:

* [Scalable Vector Graphics: Paths][mdn-docs]

The right place to start to learn is,
from the manual book.

-- -- --

### Quadratic Bezier Curve

#### Original Code

The original code from MDN is as below:

{{< highlight html >}}
  <path d="M 10 80 Q 95 10 180 80" stroke="black" fill="transparent"/>
{{< / highlight >}}

There are actually nine lines,
bu I write only three for simplicity.

#### Indented Code

> Proper Indentation for Learning Purpose

We can write down the code form MDN above without any changes,
but with clearer indentation.

* [gitlab.com/.../14-bezier-a.html][html-14-bezier-a]

{{< highlight html >}}
<svg xmlns="http://www.w3.org/2000/svg"
     viewbox="0 0 190 160">
  <rect x="0" y="0" width="190" height="160"
        stroke="gray" fill="transparent"/>
  <path d="M 10 80
           Q 95 10 180 80"
        stroke="black" fill="transparent"/>
</svg>
{{< / highlight >}}

You can see the result below:

![SVG MDN: Quadratic Bezier Curve: Original][14-mdn-a]

We can examine better with proper indentation.

* Move: `M 10 80`.
* Quadratic `Q 95 10 180 80`.

#### Redefine Coordinate

Just like previous example,
it is not easy to examine a curve with dimension 190x160 area.
How about moving the *viewbox* to fit 200x200 area?
And put the X axis right in the middle coordinate.

* [gitlab.com/.../14-bezier-b.html][html-14-bezier-b]

{{< highlight html >}}
<svg xmlns="http://www.w3.org/2000/svg"
     viewbox="0 0 200 200">
  ...
</svg>
{{< / highlight >}}

Again, the middle is at 100 height.
This way we can examine easier
The code above can be rewritten as below code:

{{< highlight html >}}
<svg xmlns="http://www.w3.org/2000/svg"
     viewbox="0 0 200 200">
  <rect x="0" y="0" width="200" height="200"
        stroke="gray" fill="transparent"/>
  <path d="M 0 100
           Q 100 20 200 100"
        stroke="black" fill="transparent"/>
</svg>
{{< / highlight >}}

Instead of starting from 10 to 180,
we start from 0 (zero) to 200.

![SVG MDN: Quadratic Bezier Curve: Redefining Coordinates][14-mdn-b]

Our goal is still to examine the coordinate in the code.

#### Line Direction Helper

For the third time.
What is it with the curve?
What is happening behind the scene?

* [gitlab.com/.../14-bezier-c.html][html-14-bezier-c]

{{< highlight html >}}
<svg xmlns="http://www.w3.org/2000/svg"
     viewbox="0 0 200 200">
  <rect x="0" y="0" class="box big"/>
  <path d="M 0 100
           Q 100 20 200 100"
        stroke="black" fill="transparent"/>
  <path d="M   0 100 L 100  20" class="line blue"/>
  <path d="M 100  20 L 200 100" class="line blue"/>
</svg>
{{< / highlight >}}

With the result as below figure:

![SVG MDN: Quadratic Bezier Curve: Line Direction][14-mdn-c]

There should be no problem to understand,
the relationship between the curve and the line.

-- -- --

### Stringed Quadratic Bezier Curve

> Using `T` instead of `S`.

#### Original Code

The original code from MDN is as below:

{{< highlight html >}}
 <path d="M 10 80 Q 52.5 10, 95 80 T 180 80" stroke="black" fill="transparent"/>
{{< / highlight >}}

There is only one bezier line, with multiple points.

#### Indented Code

With proper indentation and rectangle border,
we can write down as below code:

* [gitlab.com/.../15-bezier-a.html][html-15-bezier-a]

{{< highlight html >}}
<svg xmlns="http://www.w3.org/2000/svg"
     viewbox="0 0 190 160">
  <path d="M 10 80 
           Q 52.5 10, 95 80
           T 180 80"
        stroke="black" fill="transparent"/>
</svg>
{{< / highlight >}}

You can see the result below:

![SVG MDN: Stringed Quadratic Bezier Curve: Original][15-mdn-a]

We can examine better with proper indentation.

* Move: `M 10 80`.
* Quadratic `Q 52.5 10, 95 80`.
* Stringed `T 180 80`.

#### Redefine Coordinate

For the third time,
it is not easy to examine a curve with dimension 190x160 area.
How about moving the *viewbox* to fit 200x200 area?
And put the X axis right in the middle coordinate.

The middle is at 100 height.
This way we can examine easier
The code above can be rewritten as below code:

* [gitlab.com/.../15-bezier-b.html][html-15-bezier-b]

{{< highlight html >}}
<svg xmlns="http://www.w3.org/2000/svg"
     viewbox="0 0 200 200">
  <rect x="0" y="0"
        width="200" height="200"
        stroke="gray" fill="transparent"/>
  <path d="M   0 100 
           Q  50  20, 100 100
           T 200 100"
        stroke="black" fill="transparent"/>
</svg>
{{< / highlight >}}

Instead of starting from 10 to 180,
we start from 0 (zero) to 200.

![SVG MDN: Stringed Quadratic Bezier Curve: Redefining Coordinates][15-mdn-b]

Remember our goal.
To examine the coordinate in the code, instead just displaying result.

#### Line Direction Helper

For the fourth time.
What is it with the curve?
What is happening behind the scene?

Consider this code below:

* [gitlab.com/.../15-bezier-c.html][html-15-bezier-c]

{{< highlight html >}}
<svg xmlns="http://www.w3.org/2000/svg"
     viewbox="0 0 200 200">
  <rect x="0" y="0" class="box big"/>
  <path d="M   0 100 
           Q  50   0, 100 100
           T 200 100"
        stroke="black" fill="transparent"/>
  <path d="M   0 100 L  50   0" class="line blue"/>
  <path d="M  50   0 L 100 100" class="line blue"/>
</svg>
{{< / highlight >}}

> Whoops, it looks like something is missing, again.

Since the original point from stringed cannot explain what happened,
I manually add the green line as below:

{{< highlight html >}}
  <path d="M   0 100 L  50   0" class="line blue"/>
  <path d="M  50   0 L 100 100" class="line blue"/>
  <path d="M 100 100 L 150 200" class="line green"/>
  <path d="M 100 100 L 200 100" class="line blue"/>
{{< / highlight >}}

With the result as below figure:

![SVG MDN: Stringed Quadratic Bezier Curve: Line Direction][15-mdn-c]

I must admit I'm still confused.
But I can use the Bezier now.

-- -- --

### What is Next ?

We are ready to apply our knowledge,
and pour it into a web page.

Consider continue reading [ [Bezier SVG - Generating DOM][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2022/01/17/bezier-svg-04/

[mdn-docs]: https://developer.mozilla.org/en-US/docs/Web/SVG/Tutorial/Paths

[14-mdn-a]: {{< baseurl >}}assets/posts/frontend/2022/01-bezier/14-mdn-a.png
[14-mdn-b]: {{< baseurl >}}assets/posts/frontend/2022/01-bezier/14-mdn-b.png
[14-mdn-c]: {{< baseurl >}}assets/posts/frontend/2022/01-bezier/14-mdn-c.png
[15-mdn-a]: {{< baseurl >}}assets/posts/frontend/2022/01-bezier/15-mdn-a.png
[15-mdn-b]: {{< baseurl >}}assets/posts/frontend/2022/01-bezier/15-mdn-b.png
[15-mdn-c]: {{< baseurl >}}assets/posts/frontend/2022/01-bezier/15-mdn-c.png

[html-14-bezier-a]:     {{< tutor-css-tools >}}/bezier/14-bezier-a.html
[html-14-bezier-b]:     {{< tutor-css-tools >}}/bezier/14-bezier-b.html
[html-14-bezier-c]:     {{< tutor-css-tools >}}/bezier/14-bezier-c.html
[html-15-bezier-a]:     {{< tutor-css-tools >}}/bezier/15-bezier-a.html
[html-15-bezier-b]:     {{< tutor-css-tools >}}/bezier/15-bezier-b.html
[html-15-bezier-c]:     {{< tutor-css-tools >}}/bezier/15-bezier-c.html
