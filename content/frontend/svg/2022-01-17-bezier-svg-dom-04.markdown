---
type   : post
title  : "Bezier SVG - Generating Single Path"
short  : "Bezier SVG - Single Path"
date   : 2022-01-17T09:17:35+07:00
slug   : bezier-svg-04
categories: [frontend]
tags      : [svg, css, javascript]
keywords  : [bezier, path, dom]
author : epsi
opengraph:
  image: assets/posts/frontend/2022/01-bezier/23-path.png

toc    : "toc-2022-02-animation"

excerpt:
  Generating SVG in DOM using Javascript.

---

### Preface

> Goal: Generating SVG in DOM using Javascript.

-- -- --

### Single Bezier Area

We can reuse our last MDN sample,
and generate our own simple example.

#### Viewbox

The dimension size is 200 width x 200 height.

* [gitlab.com/.../21-path.html][html-21-path]

{{< highlight html >}}
<svg xmlns="http://www.w3.org/2000/svg"
     viewbox="0 0 200 200">
  <rect x="0" y="0"
        width="200" height="200"
        stroke="gray" fill="transparent"/>
  ...
</svg>
{{< / highlight >}}

#### Path: Line

We need a temporary line as a guideline.

{{< highlight html >}}
  <path d="M   0  90
           C  50  80,  50  80, 100 100
           S 150 120, 200 110"
        stroke="green" fill="transparent"/>
{{< / highlight >}}

#### Path: Area

This is what we want to achieve.
The bezier is slightly different,
from the line above for aestethic purpose,
by giving variant, shift point by 10 or else.
It all depends on your creativity.

{{< highlight html >}}
  <path d="M   0  90
           C  50  80,  70  80, 120 100
           S 170 130, 200 120
           L 200, 0
           L   0, 0
           Z   0 90"
          stroke="black" fill="#2196F3"/>
{{< / highlight >}}

We can examine the bezier line.

* Move: `M   0  90`.
* Quadratic `C  50  80,  70  80, 120 100`.
* Stringed `S 170 130, 200 120`.

Then create the border area to fit the border rectangle.

* Line: `L 200, 0`.
* Line: `L   0, 0`.
* Close: `Z   0 90`.

#### Preview

![SVG Bezier: Single Bezier Area][21-path]

Try to resize the size of your browser.

-- -- --

### Multiple Bezier Area

We can reuse our own simple example,
and generate more complex painting.

#### Material Color

I'm going to use material color with 10 scale gradation.
This means we can have ten bezier line,
starting from zero (0) to nine (9).

The color choice is yours to decide.
For this example, I choose blue.

#### Stylesheet

We can make our life easier with stylesheet.

{{< highlight css >}}
.box {
  stroke: gray;
  stroke-width: none;
  fill  : white;
  width : 200px;
  height: 200px;
}

.area {
  stroke: none;
  stroke-width: none;
}

.line  {
  stroke-width: 0.5px;
  fill  : transparent;
}
.line.blue  { stroke: blue; }
.line.green { stroke: green; }
.line.red   { stroke: red; }
{{< / highlight >}}

Note that this is inline stylesheet.
We haven't separate into its own file yet.

#### Viewbox

Now using class, from stylesheet.

* [gitlab.com/.../22-path.html][html-22-path]

{{< highlight html >}}
<svg xmlns="http://www.w3.org/2000/svg"
     viewbox="0 0 200 200">
  <rect x="0" y="0"
        class="box"/>
  ...
</svg>
{{< / highlight >}}

#### Path: Area and Line

There are so many areas,
I start from only one area in the middle:

{{< highlight html >}}
      <!-- material blue 05 -->
      <path d="M   0  70
               C  50  60,  70  60, 120 80
               S 170 110, 200 100
               L 200,  0 L 0, 0 Z  0  70"
            class="area" fill="#2196F3"/>
      <path d="M   0  70
               C  50  60,  50 60, 100 80
               S 150 100, 200 90"
            class="line green"/>
{{< / highlight >}}

#### Preview

Try to resize the size of your browser.

![SVG Bezier: Multiple Bezier Area][22-path]

You can see, that the area and the line is slightly different.
We are going to have more variant, to make this more artistic.

#### Path: Starting Point

I shift our guidance to the first line.

{{< highlight html >}}
  <!-- material blue 00 -->
  <path d="M   0 190
           C  50 180,  70 180, 120 200
           S 170 230, 200 320
           L 200,  0 L 0, 0 Z  0  90"
        class="area" fill="#E3F2FD"/>
  <path d="M   0 190
           C  50 180,  50 180, 100 200
           S 150 220, 200 210"
        class="line green"/>
{{< / highlight >}}

I have experience difficulties while debugging.
With starting point as guidance, this helps a lot.

The complete source code given in repository.

-- -- --

### SVG as Background

This can be achieved by simply using `z-index`.
And put the content above the SVG background.

#### HTML: SVG Container

From now on, we need SVG container.
Consider named the ID as `svg_container`.

* [gitlab.com/.../23-path.html][html-23-path]

{{< highlight html >}}
  <div id="svg_container">
    <svg xmlns="http://www.w3.org/2000/svg"
         viewbox="0 0 200 160">
      <rect x="0" y="0"
            class="box"/>
      ...
    </svg>
  </div>
{{< / highlight >}}

#### Stylesheet: SVG Container

{{< highlight css >}}
  #svg_container {
    position: absolute;
    top: 0;
    right: 0;
    left: 0;
    height: 100%;
    overflow: hidden;
    z-index: -1;
  }
{{< / highlight >}}

#### Stylesheet: Hiding Temporary Guidelines

You can switch, turned in on or off.
Anytime you like.

{{< highlight css >}}
  .line.blue  { stroke: blue; }
  .line.green { stroke: green; stroke-width: 0px; }
  .line.red   { stroke: red; }
{{< / highlight >}}

#### HTML: The Content

Consider put simple content.

{{< highlight html >}}
<body>
  <div id="svg_container">
    ...
  </div>

  <div id="sitedesc_image"></div>
  <p style="text-align: center;">
    Paragraph Test on SVG Background</p>
</body>
{{< / highlight >}}

#### Stylesheet: Header Image

{{< highlight css >}}
  #sitedesc_image {
    width: 500px;
    height:100px;
    max-width: 100%;
    background: transparent url(../blue-sfme.png) no-repeat left top;
    background-size: 100% auto;
    float: none;
    margin-top: 0;
    margin-bottom: 10px;
    margin-right: auto;
    margin-left: auto;
  }
{{< / highlight >}}

#### SVG: Path

It is very similar from previous path tag.
We need nine bezier area.
Consider this one example below:

{{< highlight html >}}
      <!-- material blue 05 -->
      <path d="M   0  70
               C  50  60,  70  60, 120 80
               S 170 110, 200 100
               L 200,  0 L 0, 0 Z  0  70"
            class="area" fill="#2196F3"/>
      <path d="M   0  70
               C  50  60,  50 60, 100 80
               S 150 100, 200 90"
            class="line green"/>
{{< / highlight >}}

#### Preview

Enjoy the preview.

![SVG Bezier: SVG as Background][23-path]

-- -- --

#### Javascript Automation: Single Bezier

Writing bezier line from point to point is,
no just a tedious task, but also error prone.
Luckily we can generate the DOM with javascript.

Consider start from one line.

#### HTML: Head

Before we begin, consider tidy our document.
Refactor into different stylesheet file.

* [gitlab.com/.../24-path.html][html-24-path]
* [gitlab.com/.../24-bezier.css][css-24-bezier]
* [gitlab.com/.../24-style.css][css-24-style]

{{< highlight html >}}
<head>
  <title>Bezier Curves</title>
  <link rel="stylesheet"
        href="24-bezier.css"/>
  <link rel="stylesheet"
        href="24-style.css"/>
</head>
{{< / highlight >}}

#### HTML: Body

Also prepare the HTML body.

{{< highlight html >}}
<body>

  <div id="svg_container">
    <svg xmlns="http://www.w3.org/2000/svg"
         id="svg_root" viewbox="0 0 200 160">
    </svg>
  </div>

  <div id="sitedesc_image"></div>
  <p style="text-align: center;">
    Paragraph Test on SVG Background</p>

  <script type="text/javascript">
    ...
  </script>

</body>
{{< / highlight >}}

Instead pouring a bunch of path tags,
we simplfy our process by using javascript.
The SVG path can be drawan inside `svg_root` element.

#### Javascript: Path Element

Beware that we require to use `.createElementNS`,
to create SVG path element.

{{< highlight javascript >}}
    const svg_root  = document.getElementById("svg_root");

    line = document
      .createElementNS('http://www.w3.org/2000/svg',"path"); 
    ...

     svg_root.appendChild(line);
{{< / highlight >}}

#### Javascript: Setting Attribute

And the rest is just setting attribute.

{{< highlight javascript >}}
    // material blue 05
    line = document
      .createElementNS('http://www.w3.org/2000/svg',"path"); 
    line.setAttribute("class", "area");
    line.setAttribute("fill", "#2196F3");
    line.setAttribute("d",
              `M   0  70
               C  50  60,  70  60, 120 80
               S 170 110, 200 100
               L 200,  0 L 0, 0 Z  0  70`);
{{< / highlight >}}

> Simple right?

Do not fogert to check the result in inspect element.

#### Preview

Enjoy the preview.

![SVG Bezier: Javascript Automation: Single Bezier][24-path]

-- -- --

### What is Next ?

We can continue to paint multiple bezier line with native javascript.

Consider continue reading [ [Bezier SVG - Generating Multiple Path][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2022/01/19/bezier-svg-05/

[21-path]:  {{< baseurl >}}assets/posts/frontend/2022/01-bezier/21-path.png
[22-path]:  {{< baseurl >}}assets/posts/frontend/2022/01-bezier/22-path.png
[23-path]:  {{< baseurl >}}assets/posts/frontend/2022/01-bezier/23-path.png
[24-path]:  {{< baseurl >}}assets/posts/frontend/2022/01-bezier/24-path.png

[html-21-path]:         {{< tutor-css-tools >}}/bezier/21-path.html
[html-22-path]:         {{< tutor-css-tools >}}/bezier/22-path.html
[html-23-path]:         {{< tutor-css-tools >}}/bezier/23-path.html
[html-24-path]:         {{< tutor-css-tools >}}/bezier/24-path.html

[css-24-bezier]:        {{< tutor-css-tools >}}/bezier/24-bezier.css
[css-24-style]:         {{< tutor-css-tools >}}/bezier/24-style.css
