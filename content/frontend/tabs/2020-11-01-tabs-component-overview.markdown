---
type   : post
title  : "Tabs - Component Overview"
date   : 2020-11-01T09:17:35+07:00
slug   : tabs-component-overview
categories: [frontend]
tags      : [html]
keywords  : [overview]
author : epsi
opengraph:
  image: assets/posts/frontend/2020/11-tabs/00-tabs-mockup-mobile.png

toc    : "toc-2020-11-component-tabs"

excerpt:
  An overview about this tabs component project.

---

### Goal

> Goal: An overview about this tabs component project.

My aim is to give ready to use examples.

#### Source Examples

You can obtain source examples here:

* [html-preprocessor/components/01-tabs][source-example]

### Towards Modern Web Development

> Go ahead jump!

I have been thinking lately,
on how to explain web development for beginner, in a faster fashioned.
And I decide to go straight away
from basic stuff (html+css+js)
to `tailwind` and `alpine.js` or `vue.js`.
This way, beginner can step into modern framework,
without the burden of reading too many stuff.

To be exact these are the steps:

1. CSS: `Plain CSS`, then `Tailwind CSS`.
   Skipping the whole `CSS preprocessor` stuff.

2. Javascript: `oldschool inline`, then `unobtrusive`,
   then `jquery`, then `alpine.js`.

3. Javascript: Other modern web framework such as: `vue.js`.

### Custom HTML component

> Be Brave!

At the same time,
I have seen beginner copy-paste stuff from the internet,
without knowing how it works, especially CSS Framework users.
Of course using ready to use component is easier.
It works well, but beginner will quickly stuck,
by the limitation of one particular technology.
And a moment later beginner can realize,
that their skill goes to nowhere.

The thing is, anyone can make their own custom component.
All you need to do is be brave.
Just start make your own custom component.
This article will show you how the proces can be done,
from scratch to complete.

This likely require imagination in the first place,
than ended up with technical skill.
No need to be perfect, since you can enhance later anyway.
Once you start, it is going to be just a regular coding process.
It takes time to finish, but nothing scary.
The hardest part is to start.

Howver, you never know if you never try.

### Step By Step.

> But how exactly?

I arrange this article series as below:

1. CSS: Basic layout with `Flex`.
2. CSS: Two Layouts for Tabs Component.
3. JS:  `Plain Javascript`.
4. CSS: `Tailwind CSS` (extracted).
5. JS:  `jQuery` (obsolete).
6. JS:  `Alpine.js` (inline), and (extracted).
7. JS:  `Vue.js` (components).
8. JS:  `Vue CLI` (components).

### Two Tabs Components

> Why not both?

I made two tabs components.

1. Simple Tabs
2. Enhanced Tabs

For each step, I start with simple tabs,
so reader can understand how it works.
Then I continue with enhanced tabs,
so you can adapt the basicknowledge,
to real live components making situation.

### Responsive Preview

Mobile Mockup for Enhanced Tabs:

![Tabs Component: Mobile Preview][00-preview-mobile]

Desktop Mockup for Enhanced Tabs:

![Tabs Component: Tablet Preview][00-preview-tablet]

I'm using `mobile first` to manage responsive in stylesheet.

### Why Tailwind?

> It looks cool.

First, `tailwind` is suitable for working with modern web development.

In my own opinion, is from productivity perspective.
Maintaining stylesheet rules is nightmare,
that is why we need classes.
Maintaining classes is also nightmare,
that is why we need `css preprocessor`.
Tailwind will significantly reduce,
the use of `css preprocessor`.

We do not really need to use `tailwind` to make a component.
This is just silly to force to use tailwind for very simple project.
But you are going to need `tailwind`,
to work with other modern web development tools.
So I guess `tailwind` knowledge is a must these days.

### Why Alpine.js?

> Suitable for beginner.

* First, `alpine.js` is direct replacement of the obsolete `jQuery`.
  This is why I put a `jQuery` article as an example,
  so that you can compare between the two.

* Second, alpine is lighter than `Vue` or else.
  `alpine.js` does not try to do backend stuff,
  so beginner does not have the burden to learn too far.
  But there might be limitations as trade-off.

* At last, bridging.
  if you understand `alpine.js`,
  then you can turn the code to modern web development.
  Such as porting to `Vue` or else.

Instead of programming line by line,
you just need to declare, what to do with the DOM.

All this conventional frontend frameworks, are
running without the needs of bundle.
This means easy to setup.
This journey stop with `vue.js` that also can be served via CDN only.
Then we continue to step in, to mdern web framework using NPM.

### Modern Framework

> Modern means: Use bundler 😀.

I intent to write more, such as `vue.js`.
I do it for myself, so I can compare each framework easily.

I started with `vue.js`,
because it is easy to port from `alpine.js` to `vue.js`
And it is easy to port from `vue.js` to `Vue CLI`.
And later with bundle in mind,
]you can port to other modern framework as well.

### Tools

> Beginner Friendly

To avoid many too setup,
I am intentionally use plain HTML.
This is a setup free situation.
Beginner can just open the file from file manager.
I believe this is easier for beginner.

Except for `Tailwind CSS`.
This is require careful setup.

Optionally, I have setup `browser-sync` for convenience reason.
So you can open the files right away from browser.
This reduce the need to repetitively go back and forth,
from file manager to browser.

{{< highlight bash >}}
$ browser-sync start --server --directory
{{< / highlight >}}

![Tabs Component: browser-sync][00-browser-sync]

### Directory Arrangement

I arrange the directoy as below:

{{< highlight bash >}}
❯ tree -d -L 1
.
├── css
├── js
└── tailwind
{{< / highlight >}}

All plain `html` files are in the root of this working directory.

### Credits

> Inspired By!

I began this making `HTML` component,
after reading `how to` section from `w3schools`.
But this good tutorial seems to be made a decade ago,
so I decide to make the component from the scratch,
without even copy paste the code from `w3schools`.

However if you wish to know the original component,
you can read here first.

* [How To - Tab Headers][howto-01]

* [How To - Full Page Tabs][howto-02]

### License Issue

> Freedom

Frankly speaking,
making my own custom component gives me freedom, while blogging.
I do not need to care anymore,
about giving any credits to any third party.
Because I did it myself.

#### Javascript/Typescript in CLI

This blog is mainly talking about web development.
For `javascript` or `typescript` in CLI,
you might consider to have a look at my other blog,
for example this article series:

* [Typescript - Small Step into Typed][playing-with-records]

-- -- --

### What's Next?

Before we begin, we need to build a basic HTML layout.

Consider continue reading [ [Tabs - CSS - Flex Layout][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}frontend/2020/11/02/tabs-css-flex-layout/
[playing-with-records]:     https://epsi.bitbucket.io/lambda/2020/11/03/playing-with-records-typescript/

[source-example]:           {{< tutor-css-tools >}}/components/01-tabs/

[00-preview-tablet]:    {{< baseurl >}}assets/posts/frontend/2020/11-tabs/00-tabs-mockup-tablet.png
[00-preview-mobile]:    {{< baseurl >}}assets/posts/frontend/2020/11-tabs/00-tabs-mockup-mobile.png
[00-browser-sync]:      {{< baseurl >}}assets/posts/frontend/2020/11-tabs/00-browser-sync.png

[howto-01]: https://www.w3schools.com/howto/howto_js_tab_header.asp
[howto-02]: https://www.w3schools.com/howto/howto_js_full_page_tabs.asp
