---
type   : post
title  : "Tabs - CSS - Flex Layout"
date   : 2020-11-02T09:17:35+07:00
slug   : tabs-css-flex-layout
categories: [frontend]
tags      : [html, css]
keywords  : [flex]
author : epsi
opengraph:
  image: assets/posts/frontend/2020/11-tabs/01-tabs-basic-mobile.png

toc    : "toc-2020-11-component-tabs"

excerpt:
  Creating basic layout from scratch using Flex.

---

### Preface

> Goal: Creating basic layout from scratch using Flex.

Almost every component start with layout.

-- -- --

### Very Simple Flex

> Let's practice!

Consider start with a very simple flex structure.

#### Reading Reference

All is explained here:

* <https://css-tricks.com/snippets/css/a-guide-to-flexbox/>

I don't think that I need to explain more.

#### Document: HTML Body

* [gitlab.com/.../01a-css-flex.html][html-01a-flex]

{{< highlight html >}}
  <main class="tabs">
    <div class="tab-headers">
      Header
    </div>

    <div class="tab-contents">
      Contents
    </div>
  </main>
{{< / highlight >}}

#### Stylesheet: CSS

The stylesheet would be self explanatory.

* [gitlab.com/.../css/01-flex-layout.css][css-01-flexlayout]

{{< highlight css >}}
.tabs {
  display: flex;
  flex-direction: row;
}

.tabs * {
  box-sizing: border-box;
  padding: 1rem;
}

.tab-headers {
  flex-basis: 30%;
  background-color: #bdf;
}

.tab-contents {
  flex-basis: 70%;
  background-color: #6bf;
}
{{< / highlight >}}


#### Document: HTML Head

Do not forget to put the stylesheet between the `<head>...</head>`.

{{< highlight html >}}
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Tabs - Flex Layout</title>
  <link rel="stylesheet" type="text/css"href="../css/01-flex-layout.css">
{{< / highlight >}}

#### Preview

The output in browser would be similar to this figure below:

![Tabs Component: Flex Layout][01-flex-layout]

-- -- --

### Basic Layout

Still using flex, consider continue to basic structure.

#### Document: HTML Body

* [gitlab.com/.../01b-css-inner.html][html-01b-inner]

{{< highlight html >}}
  <main class="tabs">
    <div class="tab-headers">
      <div>One</div>
      <div>Two</div>
      <div>Three</div>
      <div>Four</div>
    </div>

    <div class="tab-contents">
      <div>
        <h3>Lorem Ipsum</h3>
        <p>Lorem ipsum dolor sit amet,
          consectetur adipiscing elit.
          Quisque in faucibus magna.</p>
      </div>
    </div>
  </main>
{{< / highlight >}}

It is still simple.
I just add a few content.

#### Stylesheet: CSS: Responsive

> Change the flow direction from column to row.

The stylesheet would be as below:

* [gitlab.com/.../css/01-basic-layout.css][css-01-basic-layout]

{{< highlight css >}}
/* Main Layout: Mobile First */

.tabs {
  display: flex;
  flex-direction: column;
}

.tabs * {
  box-sizing: border-box;
}

@media (min-width: 768px) {
  .tabs {
    flex-direction: row;
  }
  
  .tabs>* {
    min-height: 20rem;
  }
}
{{< / highlight >}}

Form the very start,
I use `mobile first` as responsive rule.
That is why I use `@media (min-width: …) {…}`,
instead of `@media (max-width: …) {…}`.

I also add `border-box` to make the box size consistent.

#### Stylesheet: CSS: Inner Layout

We begin the simple structure with inner layout and outer layout:

* Outer: `.tab-headers` and `.tab-contents`.

* Inner: `.tab-headers div` and `.tab-contents div`.

{{< highlight css >}}
/* Inner and Outer Layout */

.tab-headers {
  flex-basis: 30%;
}

.tab-headers div {
  padding: 1rem;
  height:  4rem;
  display: flex;
  align-items: center;
}

.tab-contents {
  flex-basis: 70%;
}

.tab-contents div {
  padding: 1rem;
  height:  100%;
}
{{< / highlight >}}

* [gitlab.com/.../css/01-basic-layout.css][css-01-basic-layout]

#### Stylesheet: CSS: Border

> You don't need this one below.

For convenience while debugging,
I usually use different color.

Sometimes I also add border,
while examining box model.

{{< highlight css >}}
/* Border helper to distinct element */

.tabs div {
  border: 1px dashed grey;
  background-color: #bdf;
}

.tabs div div {
  background-color: #6bf;
}
{{< / highlight >}}

* .

Once we get the layout right,
we can safely remove this additional stuff.

#### Document: HTML Head

Put the stylesheet between the `<head>...</head>`.

{{< highlight html >}}
  <meta name="viewport"
        content="width=device-width, initial-scale=1">
  <title>Tabs - Inner Layout</title>
  <link rel="stylesheet" type="text/css"
        href="../css/01-basic-layout.css">
{{< / highlight >}}

#### Preview

Now we have two kind of previews,
based on responsiveness.

> Mobile First

![Tabs Component: Basic Layout: Mobile][01-basic-mobile]

> Tablet

![Tabs Component: Basic Layout: Tablet][01-basic-tablet]

As you can see with this distinct color,
the tabs on the left pane has two layers.

The right pane, is actually has two layers,
but the inner layers occupied all the height.
We need two layers for content,
because we need different inner content for each tabs.

-- -- --

### Tailwind

Since we are going to use `Tailwind CSS`,
I'm going to introduce from the very start.

#### Stylesheet: Tailwind

> Significantly reduce lines of code.

Do not bother will the `tailwind` setup,
I will explain later.
All you  need to know that,
we can represent all the previous stylesheet,
to other format.

* [gitlab.com/.../tailwind/01-tailwind.css][css-01-tailwind]

{{< highlight css >}}
/* Main Layout: Mobile First */
.tabs        { @apply flex flex-col; }
.tabs *      { @apply box-border; }

@screen md {
  .tabs      { @apply flex-row; }  
  .tabs>*    { min-height: 20rem; }
}

/* Inner and Outer Layout */
.tab-headers        { flex-basis: 30%; }
.tab-headers div    { @apply p-4 h-16 flex items-center; }
.tab-contents       { flex-basis: 70%; }
.tab-contents div   { @apply p-4 h-full; }

/* Border helper to distinct element */

.tabs div       { border: 1px dashed grey; }
.tabs div       { @apply bg-blue-300; }
.tabs div div   { @apply bg-blue-500; }
{{< / highlight >}}

As you can see, not everything can be **ported** to `tailwind`,
some rulse are still using `native css`.

* .

This is just an introduction,
so you can skip this `tailwind` stuff.
We will discuss about it later.

#### Document: HTML Head

The `tailwind` build result is provided in repository.
Do not forget to change the stylesheet between the `<head>...</head>`.

{{< highlight html >}}
  <meta name="viewport"
        content="width=device-width, initial-scale=1">
  <title>Tabs - Inner Layout</title>
  <link rel="stylesheet" type="text/css"
        href="../css/01-tailwind-basic.css">
{{< / highlight >}}

The preview result is exactly the same.

-- -- --

### Inspect Elements

> Whenever you have problems.

For any debugging issue, you can check first with inspect elements.

![Tabs Component: Inspect elements][01-inspect-element]

Sure, that most people have already know about this feature.
I just realize that some beginner,
always forget to use this powerful tools.

-- -- --

### What's Next?

Consider continue reading [ [Tabs - CSS - Mockup Layout][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2020/11/03/tabs-css-mockup-layout/

[01-flex-layout]:       {{< baseurl >}}assets/posts/frontend/2020/11-tabs/01-tabs-flex-layout.png
[01-basic-tablet]:      {{< baseurl >}}assets/posts/frontend/2020/11-tabs/01-tabs-basic-tablet.png
[01-basic-mobile]:      {{< baseurl >}}assets/posts/frontend/2020/11-tabs/01-tabs-basic-mobile.png
[01-inspect-element]:   {{< baseurl >}}assets/posts/frontend/2020/11-tabs/01-flex-inspect-element.png

[html-01a-flex]:        {{< tutor-css-tools >}}/components/01-tabs/01-layout/01a-css-flex.html
[html-01b-inner]:       {{< tutor-css-tools >}}/components/01-tabs/01-layout/01b-css-inner.html

[css-01-flexlayout]:    {{< tutor-css-tools >}}/components/01-tabs/css/01-flex-layout.css
[css-01-basic-layout]:  {{< tutor-css-tools >}}/components/01-tabs/css/01-basic-layout.css
[css-01-tailwind]:      {{< tutor-css-tools >}}/components/01-tabs/tailwind/01-tailwind.css
