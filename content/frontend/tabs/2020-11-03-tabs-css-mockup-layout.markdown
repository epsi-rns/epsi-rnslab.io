---
type   : post
title  : "Tabs - CSS - Mockup Layout"
date   : 2020-11-03T09:17:35+07:00
slug   : tabs-css-mockup-layout
categories: [frontend]
tags      : [html, css]
keywords  : [mockup]
author : epsi
opengraph:
  image: assets/posts/frontend/2020/11-tabs/02-tabs-enhanced-mobile.png

toc    : "toc-2020-11-component-tabs"

excerpt:
  Making a ready mockup layout.
  A tabs component without javascript interactivity.

---

### Preface

> Goal: Making a ready mockup layout.

This is basically just,
a tabs component without javascript interactivity.

#### Examples

For tutorial purpose we need two layouts:

1. Simple layout, to explain javascript in the next capter.

2. Enhance layout, as real live example.

It is not a good idea to explain basic javascript,
with complex `html` layout along with their style.
I keep the explanation simple, and apply later for enhanced tabs.

-- -- --

### Simple Tabs

Consider start with a simple tabs component.

#### Document: HTML Body

* [gitlab.com/.../02a-css-simple.html][html-02a-simple]

{{< highlight html >}}
  <main class="tabs">
    <div class="tab-headers">
      <div class="bg-gray">
          Home</div>
      <div class="active bg-orange">
          Team</div>
      <div class="bg-gray">
          Contact</div>
      <div class="bg-gray">
          About</div>
    </div>

    <div class="tab-contents">
      <div class="bg-gray">
        <h3>Lorem Ipsum</h3>
        <p>Lorem ipsum dolor sit amet,
           consectetur adipiscing elit.
           Quisque in faucibus magna.</p>
      </div>
    </div>
  </main>
{{< / highlight >}}

#### Stylesheet: CSS: Page

Consider add page rules to enhance the looks.

{{< highlight css >}}
body { font-family: Arial, Helvetica, sans-serif; }

section h1 {
  margin: 1rem;
}
{{< / highlight >}}

#### Stylesheet: CSS: Basic Layout

I add the `max-width` and change the `min-height`.

* [gitlab.com/.../css/02-simple-layout.css][css-02-simple-layout]

{{< highlight css >}}
.tabs {
  display: flex;
  flex-direction: column;
  max-width: 1024px;
  border: 1px solid #ccc;
}

.tabs * {
  box-sizing: border-box;
}

@media (min-width: 768px) {
  .tabs {
    flex-direction: row;
  }
  
  .tabs>* {
    min-height: 12rem;
  }
}
{{< / highlight >}}

#### Stylesheet: Inner Layout

The stylesheet would be self explanatory.

* [gitlab.com/.../css/02-simple-layout.css][css-02-simple-layout]

{{< highlight css >}}
.tab-headers {
  flex-basis: 30%;
}

.tab-headers div {
  padding: 1rem;
  height:  4rem;
  display: flex;
  align-items: center;

  cursor: pointer;
  transition: 0.3s;
  overflow: hidden;

  color: white;
  font-size: 1rem; 
}

.tab-contents {
  flex-basis: 70%;
}

.tab-contents div {
  padding: 1rem;
  height:  100%;

  color: white;
  font-size: 1rem;
}
{{< / highlight >}}

Just padding and stuff.

#### Stylesheet: CSS

Consider to enhance the looks with colors.
We do not need to put all possible colors.
Just four colors as we need only four tabs in mockup design.

* [gitlab.com/.../css/02-simple-layout.css][css-02-simple-layout]

{{< highlight css >}}
.tab-headers div:hover {
  background-color: #777;
}

.bg-white  { background-color: white; }
.bg-gray   { background-color: #555; }
.bg-orange { background-color: orange; }
.bg-teal   { background-color: teal;  }
.bg-blue   { background-color: blue; }
.bg-red    { background-color: red; }
{{< / highlight >}}

Since we are going to use `tailwind` later,
we can use other colors with the same namespace,
such as `bg-gray-700`, `blue-500` and so on.

{{< highlight css >}}
.bg-gray-700   { background-color: #616161; }
.bg-orange-500 { background-color: #ff9800; }
.bg-teal-500   { background-color: #009688;  }
.bg-blue-500   { background-color: #2196f3; }
.bg-red-500    { background-color: #f44336; }
{{< / highlight >}}

I get this color from google material color.
This is just a temporary solution to make the mockup works.
We will get rid all these colors once we get into `tailwind`.

#### Document: HTML Head

Now we have this below between the `<head>...</head>`.

{{< highlight html >}}
  <title>Simple Tabs - Oldschool Javascript</title>
  <link rel="stylesheet" type="text/css"
        href="../css/02-simple-layout.css">
{{< / highlight >}}

#### Preview

Test the responsiveness in your browser

> Mobile First, with hover on about tab.

![Tabs Component: Simple Layout: Mobile][02-simple-mobile]

> Tablet

![Tabs Component: Simple Layout: Tablet][02-simple-tablet]

We will add javascript interactivity later.

-- -- --

### Enhanced Layout

Based on simple structure above,
consider continue to enhanced structure.

#### Document: HTML Head

Since the stylesheet become complex,
we need to separate each stylesheet in separate files.

* [gitlab.com/.../02b-css-nested-inner.html][html-02b-nested-inner]

{{< highlight html >}}
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Tabs - Nested Inner Layout</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="../css/02-enhanced-layout.css">
  <link rel="stylesheet" type="text/css" href="../css/02-background-colors.css">
  <link rel="stylesheet" type="text/css" href="../css/02-border-radius.css">
</head>
{{< / highlight >}}

I also `FontAwesome` for coolness reason.

#### Document: HTML Body

Now we have three level elements inside `<main>`:

* Tab Headers
  1. Outer: `<div class="tab-headers">`
  2. Inner: `<div class="bg-gray">`
  3. 3rd Level: `<div>...Home</div>`

* Tab Contents
  1. Outer: `<div class="tab-contents">`
  2. Inner: `<div class="bg-orange">`
  3. 3rd Level: `<div>...</div>`


{{< highlight html >}}
  <main class="tabs">
    <div class="tab-headers">
      <div class="bg-gray">
        <div>
          <i class="fa fa-home"></i>&nbsp;
          Home</div></div>
      <div class="active bg-orange">
        <div class="bg-white">
          <i class="fa fa-globe"></i>&nbsp;
          Team</div></div>
      <div class="bg-gray">
        <div>
          <i class="fa fa-envelope"></i>&nbsp;
          Contact</div></div>
      <div class="bg-gray">
        <div>
          <i class="fa fa-address-card"></i>&nbsp;
          About</div></div>
    </div>

    <div class="tab-contents">
      <div class="bg-orange">
        <div class="tab-content bg-white">
          <h3>Lorem Ipsum</h3>
          <p>Lorem ipsum dolor sit amet,
             consectetur adipiscing elit.
             Quisque in faucibus magna.</p>
        </div>
      </div>
    </div>
  </main>
{{< / highlight >}}

* [gitlab.com/.../02b-css-nested-inner.html][html-02b-nested-inner]

#### Stylesheet: CSS: Responsive

This is exactly the same as the `simple` mockup.

#### Stylesheet: CSS: Inner Layout

This is also the same with the `simple` mockup.
All I did is just adding third level elements

* `.tab-headers div div`, and

* `.tab-content`

Beware of the direct element `.tab-contents > div`.
So we do not messed up with `.tab-contents div .tab-content`.

* [gitlab.com/.../css/02-enhanced-layout.css][css-02-styled-layout]

{{< highlight css >}}
/* Inner and Outer Layout */

.tab-headers {
  flex-basis: 30%;
}

.tab-headers div {
  padding-left:  1rem;
  padding-right: 1rem;
  height: 4rem;
  display: flex;
  align-items: center;

  cursor: pointer;
  transition: 0.3s;
  overflow: hidden;

  color: white;
  font-size: 1rem; 
}

.tab-headers div div {
  width: 100%;
}

.tab-contents {
  flex-basis: 70%;
}

.tab-contents > div {
  padding-left:  1rem;
  padding-right: 1rem;
  height: 100%;

  color: black;
  font-size: 1rem;
}

.tab-content {
  padding: 1rem;
  height: 100%;
}
{{< / highlight >}}

* .

And also pay attention to the responsive effect.

{{< highlight css >}}
.tab-headers div {
  padding-left:  1rem;
  padding-right: 1rem;
}

.tab-contents > div {
  padding-left:  1rem;
  padding-right: 1rem;
}

@media (min-width: 768px) {
  .tab-headers div {
    padding-left:  1rem;
    padding-right: 0rem;
  }

  .tab-contents > div {
  padding-left:  0;
  padding-right: 1rem;
  }
}
{{< / highlight >}}

* .

#### Stylesheet: CSS: Background Color

Now we can set the rules,
related to background color.

* [gitlab.com/.../css/02-background-colors.css][css-02-bg-colors]

{{< highlight css >}}
.tab-headers > div:hover {
  background-color: #777;
}

.tab-headers div.active:hover {
  background-color: #555;
}

.tab-headers div.active div:hover {
  background-color: #eee;
}

.tab-contents div div:hover {
  background-color: #eee;
}

/* Add some decoration */

.bg-white  { background-color: white; }
.bg-gray   { background-color: #555; }
.bg-orange { background-color: orange; }
.bg-teal   { background-color: teal;  }
.bg-blue   { background-color: blue; }
.bg-red    { background-color: red; }

.tab-headers .bg-white {
  color: black;
}
{{< / highlight >}}

* .

#### Stylesheet: CSS: Border Radius

If you want, you can also add border radius.
Or you can omit this completely.

The explanation is beyond this tutorial.
You can read the reference somewhere else.

* [gitlab.com/.../css/02-border-radius.css][css-02-border-radius]

{{< highlight css >}}

/* Shadow */

.tabs {
  box-shadow: 0.25rem 0.25rem 0.25rem #ccc;
}

/* Responsive border-radius */

.tabs {
  border-radius: 0.5rem;
}

.tab-headers {
  border-radius: 0.5rem 0.5rem;
}

…

@media (min-width: 640px) {
  .tab-headers {
    border-radius: 0.5rem 0 0 0.5rem;
  }

  …
}
{{< / highlight >}}

* .

The thing is, in this case,
border radius rules become complex in responsive design.

#### Preview

Test the responsiveness in your browser

> Mobile First, with hover on about tab.

![Tabs Component: Enhanced Layout: Mobile][02-enhanced-mobile]

> Tablet

![Tabs Component: Enhanced Layout: Tablet][02-enhanced-tablet]

We are done with the mockup.

With your imagination
you can make other mockup from scratch as you need.

-- -- --

### What's Next?

Consider continue reading [ [Tabs - JS - Plain Oldschool][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2020/11/04/tabs-js-plain-oldschool/

[02-enhanced-tablet]:   {{< baseurl >}}assets/posts/frontend/2020/11-tabs/02-tabs-enhanced-tablet.png
[02-enhanced-mobile]:   {{< baseurl >}}assets/posts/frontend/2020/11-tabs/02-tabs-enhanced-mobile.png
[02-simple-tablet]:     {{< baseurl >}}assets/posts/frontend/2020/11-tabs/02-tabs-simple-tablet.png
[02-simple-mobile]:     {{< baseurl >}}assets/posts/frontend/2020/11-tabs/02-tabs-simple-mobile.png

[html-02a-simple]:      {{< tutor-css-tools >}}/components/01-tabs/02-mockup/02a-css-simple.html
[html-02b-nested-inner]:{{< tutor-css-tools >}}/components/01-tabs/02-mockup/02b-css-nested-inner.html

[css-02-bg-colors]:     {{< tutor-css-tools >}}/components/01-tabs/css/02-background-colors.css
[css-02-border-radius]: {{< tutor-css-tools >}}/components/01-tabs/css/02-border-radius.css
[css-02-simple-layout]: {{< tutor-css-tools >}}/components/01-tabs/css/02-simple-layout.css
[css-02-styled-layout]: {{< tutor-css-tools >}}/components/01-tabs/css/02-enhanced-layout.css
