---
type   : post
title  : "Tabs - JS - Plain Oldschool"
date   : 2020-11-04T09:17:35+07:00
slug   : tabs-js-plain-oldschool
categories: [frontend]
tags      : [html, javascript]
keywords  : [onclick event, plain, inline]
author : epsi
opengraph:
  image: assets/posts/frontend/2020/11-tabs/03-tabs-simple-oldschool-console-log.png

toc    : "toc-2020-11-component-tabs"

excerpt:
  Adding javascript interactivity.
  Exploring DOM with inline javascript.

---

### Preface

> Goal: Adding javascript interactivity. Exploring DOM.

This is basically just,
a tabs component with javascript interactivity.

#### Examples

For tutorial purpose we need three layouts:

1. Simple layout, to example of oldschool javascript.

2. Simple layout, to explain unobtrusive javascript.

3. Enhance layout, as real live example.

This article cover the first part only.
We will continue the other two parts later.

-- -- --

### Simple Tabs Inline: Structure

Consider start with the oldschool inline javascript.

The most used interactivity is the `onclick` event.
There are already tons of example in the internet.

#### Document: HTML Head

We can reuse previous stylesheet between the `<head>...</head>`.

{{< highlight html >}}
    <link rel="stylesheet" type="text/css"
        href="../css/02-simple-layout.css">
{{< / highlight >}}

#### Document: HTML Body

The typical oldschool javascript has this inline `onclick` event.

* [gitlab.com/.../03a-js-oldschool.html][html-03a-oldschool]

{{< highlight html >}}
  <main class="tabs">
    <div class="tab-headers">
      <div data-target="home"  data-color="bg-blue-500"
           onclick="activateTab(event)">Home</div>
      <div data-target="team"  data-color="bg-teal-500"
           onclick="activateTab(event)"
           class="active">Team</div>
      <div data-target="news"  data-color="bg-red-500"
           onclick="activateTab(event)">News</div>
      <div data-target="about" data-color="bg-orange-500"
           onclick="activateTab(event)">About</div>
    </div>

    <div class="tab-spacer"></div>

    <div class="tab-contents">
      <div class="tab-content bg-gray-700">
        <h3>Home</h3>
        <p>Lorem ipsum dolor sit amet,
           consectetur adipiscing elit.
           Quisque in faucibus magna.</p>
      </div>
    </div>
  </main>

  <script src="../js/custom-oldschool.js"></script>
{{< / highlight >}}

The javascript should be added at the end of the document,
instead of between the `<head>...</head>`.

#### DOM Element: Tab Header

{{< highlight html >}}
      <div data-target="home"  data-color="bg-blue-500"
           onclick="activateTab(event)">Home</div>
{{< / highlight >}}

There are two things here

1. You can spot this `onclick` event:

   * `onclick="activateTab(event)"`

2. To avoid long code,
   I also utilize not so oldschool html standard,
   the `data-` attribute:

   * `data-target="home" data-color="bg-blue-500"`

#### DOM Element: Tab Content

I intentionally use only one content,
as a start, for the sake of simplicity.

{{< highlight html >}}
    <div class="tab-contents">
      <div class="tab-content bg-gray-700">
        <h3>Home</h3>
        <p>Lorem ipsum dolor sit amet,
           consectetur adipiscing elit.
           Quisque in faucibus magna.</p>
      </div>
    </div>
{{< / highlight >}}

We will add complete content later.
This should have four contents,
the same as the number of header tabs.

-- -- --

### Simple Tabs Inline: Javascript

Once we are done with `html` structure,
we can add explore DOM (domain object model),
and add any related event.

#### Javascript: Getting Element from Event

The javascript should contain the `activateTab(event)` function:

{{< highlight javascript >}}
function activateTab(event) {
  var targetHeader = event.srcElement;
  var targetName = targetHeader.dataset.target;
  var colorClass = targetHeader.dataset.color;

  console.log(targetName);
  console.log(colorClass);
}
{{< / highlight >}}

You can test the value with `inspect element`.

![Tabs Component: Simple Layout: Console Log][04-console-log]

If you click the white `home` element, this will show:

{{< highlight html >}}
home
bg-blue-500
{{< / highlight >}}

#### Javascript: Complete

I won't explain the javascript one by one here,
as you can read in other reference.
We still have many things to go, from `jquery` to `alpine.js`.

So here it is the script:

* [gitlab.com/.../js/custom-oldschool.js][js-custom-oldschool]

{{< highlight javascript >}}
var tabHeaders  = document.querySelector('.tab-headers');
var tabContents = document.querySelector('.tab-contents');

function activateTab(event) {
  var targetHeader = event.srcElement;
  var targetName = targetHeader.dataset.target;
  var colorClass = targetHeader.dataset.color;

  // Set all to default setting
  Array.from(tabHeaders.children)
    .forEach(function(tabHeader) {
      tabHeader.classList.remove("active");
      tabHeader.classList.remove(tabHeader.dataset.color);
      tabHeader.classList.add("bg-gray-700");
    });
  // Except the chosen one
  targetHeader.classList.add("active");
  targetHeader.classList.remove("bg-gray-700");
  targetHeader.classList.add(colorClass);
}

tabHeaders.querySelector('.active').click();
{{< / highlight >}}

#### Javascript: Default Tab

> Initialize

Selecting default tab can be done by trigger the click event
with element containing `active` class.

{{< highlight javascript >}}
var tabHeaders  = document.querySelector('.tab-headers');
tabHeaders.querySelector('.active').click();
{{< / highlight >}}

This will change the state of the DOM elements from:

{{< highlight html >}}
    <div class="tab-headers">
      <div>Home</div>
      <div class="active">Team</div>
      <div>News</div>
      <div>About</div>
    </div>
{{< / highlight >}}

To new state as below:

{{< highlight html >}}
    <div class="tab-headers">
      <div class="bg-gray-700">Home</div>
      <div class="active bg-teal-500">Team</div>
      <div class="bg-gray-700">News</div>
      <div class="bg-gray-700">About</div>
    </div>
{{< / highlight >}}

#### Javascript: Click Event

And if you click `home` tabs,
the DOM state will be altered to:

{{< highlight html >}}
    <div class="tab-headers">
      <div class="active bg-blue-500">Home</div>
      <div class="bg-gray-700">Team</div>
      <div class="bg-gray-700">News</div>
      <div class="bg-gray-700">About</div>
    </div>
{{< / highlight >}}

![Tabs Component: Simple Layout: Console Log][04-inspect-rules]

You can see all in inspect element.

-- -- --

### How does it works?

It can be done with these two steps.

#### 1: Reset all tabs to default setting.

{{< highlight javascript >}}
  Array.from(tabHeaders.children)
    .forEach(function(tabHeader) {
      tabHeader.classList.remove("active");
      tabHeader.classList.remove(tabHeader.dataset.color);
      tabHeader.classList.add("bg-gray-700");
    });
{{< / highlight >}}

This will result in:

{{< highlight html >}}
      <div class="bg-gray-700">Home</div>
      <div class="bg-gray-700">Team</div>
      <div class="bg-gray-700">News</div>
      <div class="bg-gray-700">About</div>
{{< / highlight >}}

#### 2: Set class for selected tab

Set the class state for the chosen one:

{{< highlight javascript >}}
  targetHeader.classList.add("active");
  targetHeader.classList.remove("bg-gray-700");
  targetHeader.classList.add(colorClass);
{{< / highlight >}}

This will result in:

{{< highlight javascript >}}
      <div class="active bg-blue-500">Home</div>
{{< / highlight >}}

#### Preview

We still doesn't touch the right content.
We will discuss this in the next article.

![Tabs Component: Simple Layout: Oldschool][04-simple-tablet]

Once you understand how it works,
you can apply to other event as well.
You can event create your own custom event.

-- -- --

### What's Next?

Consider continue reading [ [Tabs - JS - Plain Unobtrusive][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2020/11/05/tabs-js-plain-unobtrusive/

[04-console-log]:       {{< baseurl >}}assets/posts/frontend/2020/11-tabs/03-tabs-simple-oldschool-console-log.png
[04-inspect-rules]:     {{< baseurl >}}assets/posts/frontend/2020/11-tabs/03-tabs-simple-oldschool-rules.png
[04-simple-tablet]:     {{< baseurl >}}assets/posts/frontend/2020/11-tabs/03-tabs-simple-oldschool-tablet.png

[html-03a-oldschool]:   {{< tutor-css-tools >}}/components/01-tabs/03-plain/03a-js-oldschool.html
[js-custom-oldschool]:  {{< tutor-css-tools >}}/components/01-tabs/js/custom-oldschool.js
