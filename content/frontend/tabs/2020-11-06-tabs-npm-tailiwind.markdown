---
type   : post
title  : "Tabs - NPM - Tailwind Build"
date   : 2020-11-06T09:17:35+07:00
slug   : tabs-npm-tailwind
categories: [frontend]
tags      : [html, css]
keywords  : [npm install, package.json, postcss, configuration]
author : epsi
opengraph:
  image: assets/posts/frontend/2020/11-tabs/06-npm-install-loglevel.png

toc    : "toc-2020-11-component-tabs"

excerpt:
  Getting a Proper Tailwind Configuration

---

### Preface

> Goal: Getting a Proper Tailwind Configuration

For learning purpose only.

#### Reading Reference

* <https://tailwindcss.com/>

-- -- --

### Basic Tailwind Command

I won't give another install tutorials.
There is already tons of articles from the internet.

However, this is my working setup.

#### package.json

For daily development you can use `package.json` instead:

* [gitlab.com/.../package.json][config-package-json]

{{< highlight javascript >}}
{
  …
  "devDependencies": {
    "autoprefixer": "^10.0.2",
    "postcss": "^8.1.7",
    "postcss-cli": "^8.3.0",
    "tailwindcss": "^2.0.1"
  }
}
{{< / highlight >}}

You should do this first

{{< highlight bash >}}
$ npm install --loglevel=error
{{< / highlight >}}

![Tabs Component: Install Tailwind: package.json][04-package-json]

#### Simple Build

Tailwind CSS 1.9 can run perfectly from command line,
using global install.

{{< highlight bash >}}
$ tailwindcss build tailwind.css -o styles.css
{{< / highlight >}}

But since Tailwind CSS 2.0.
I have to wrapped it in `npm` force it to use local `node_modules`.

{{< highlight javascript >}}
{
  …
  "scripts": {
    "build:tailwind": "tailwindcss build tailwind.css -o styles.css",
  }
}
{{< / highlight >}}

Now I can run this without an issue.

{{< highlight bash >}}
$ npm run --silent build:tailwind

   tailwindcss 2.0.1
  
   🚀 Building: tailwind.css
  
   ✅ Finished in 10 s
   📦 Size: 12.08KB
   💾 Saved to styles.css
{{< / highlight >}}

![Tabs Component: NPM Run Build Tailwind][04-build-tailwind]

In order to run this you need to have `tailwind.config.js`.

#### tailwind.config.js

A simple working `tailwind` configuration would be as below:

* [gitlab.com/.../tailwind/tailwind.config.js][config-tailwind]

{{< highlight javascript >}}
module.exports = {
  future: {
    purgeLayersByDefault: true,
  },
  purge: {
    enabled: true,
    content: ['./*.html'],
  },
  theme: {
    extend: {},
  },
  variants: {},
  plugins: [],
}
{{< / highlight >}}

[]

-- -- --

### PostCSS

There is another way though.
I like to use `PostCSS` to **minify** the `tailwind` result.

#### package.json

* [gitlab.com/.../package.json][config-package-json]

{{< highlight javascript >}}
{
  …
  "devDependencies": {
    "autoprefixer": "^10.0.2",
    "cssnano": "^4.1.10",
    "postcss": "^8.1.7",
    "postcss-cli": "^8.3.0",
    "tailwindcss": "^2.0.1"
  }
}
{{< / highlight >}}

#### postcss.config.js

A simple working `PostCSS` configuration would be as below:

* [gitlab.com/.../tailwind/postcss.config.js][config-postcss]

{{< highlight javascript >}}
const cssnano = require('cssnano')

module.exports = {
  plugins: {
    tailwindcss: {},
    autoprefixer: {},
    cssnano: { preset: 'default' }
  }
}
{{< / highlight >}}

#### Using PostCSS

{{< highlight bash >}}
❯ postcss --verbose tailwind.css -o styles.css
Processing tailwind.css...
Finished tailwind.css in 14 s
~/Doc/t/c/01-tabs
{{< / highlight >}}

![Tabs Component: Using PostCSS to Process Tailwind][04-postcss-run]

-- -- --

### Watch

There are at least two options for this

1. NPM Nodemon
   * <https://nodemon.io/>

2. NPM Onchange
   * <https://github.com/Qard/onchange>

#### package.json

Consider add dependency:

* [gitlab.com/.../package.json][config-package-json]

{{< highlight javascript >}}
{
  …
  "devDependencies": {
    "autoprefixer": "^10.0.2",
    "cssnano": "^4.1.10",
    "nodemon": "^2.0.6",
    "onchange": "^7.1.0",
    "postcss": "^8.1.7",
    "postcss-cli": "^8.3.0",
    "tailwindcss": "^2.0.1"
  }
}
{{< / highlight >}}

Do not forget to run `npm install`.

#### 1: NPM Nodemon

{{< highlight bash >}}
$ nodemon --watch tailwind.css --exec 'npm run --silent build:tailwind'
{{< / highlight >}}

![Tabs Component: Nodemon Watch Tailwind][04-nodemon-watch]

For convenience you can build script in `package.json` as below:

{{< highlight javascript >}}
{
  …
  "scripts": {
    "build:tailwind": "tailwindcss build tailwind.css -o styles.css",
    "nodemon:tailwind": "nodemon --exec 'npm run build:tailwind'",
    "nodemon:watch": "npm run nodemon:tailwind -- --watch ./tailwind/tailwind.css"
  }
}
{{< / highlight >}}

#### 2: NPM Onchange

{{< highlight bash >}}
$ onchange tailwind.css -i -- npm run  --silent build:tailwind
{{< / highlight >}}

![Tabs Component: Onchange Watch Tailwind][04-onchange-tailwind]

For convenience you can build script in `package.json` as below:

{{< highlight javascript >}}
{
  …
  "scripts": {
    "build:tailwind": "tailwindcss build tailwind.css -o styles.css",
    "watch:tailwind": "onchange tailwind.css -i -- npm run  --silent build:tailwind",
  }
}
{{< / highlight >}}

-- -- --

### Multiple Tailiwind Stylesheet

This article require three kind of `tailwind` build,
all in `tailwind` directory
I arranged the script as below:

* [gitlab.com/.../package.json][config-package-json]

{{< highlight javascript >}}
{
  …
  "scripts": {
    "build:tailwind:01": "tailwindcss build ./tailwind/01-tailwind.css -c ./tailwind/tailwind.config.js -o ./css/01-tailwind-basic.css",
    "watch:tailwind:01": "onchange './tailwind/01-tailwind.css' -i -- npm run build:tailwind:01",

    "build:tailwind:03": "tailwindcss build ./tailwind/03-tailwind.css -c ./tailwind/tailwind.config.js -o ./css/03-tailwind-simple.css",
    "watch:tailwind:03": "onchange './tailwind/03-tailwind.css' -i -- npm run build:tailwind:03",

    "build:tailwind:04": "tailwindcss build ./tailwind/04-tailwind.css -c ./tailwind/tailwind.config.js -o ./css/04-tailwind-enhanced.css",
    "watch:tailwind:04": "onchange './tailwind/04-tailwind.css' -i -- npm run build:tailwind:04",
  }
}
{{< / highlight >}}

I have limitation on NPM knowledge.
Maybe someday I'll get a better idea.

-- -- --

### What's Next?

After install, why don't we moved on to examples?

Consider continue reading [ [Tabs - CSS - Tailwind CSS][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2020/11/07/tabs-css-tailwind/

[04-package-json]:      {{< baseurl >}}assets/posts/frontend/2020/11-tabs/04-npm-install-loglevel.png
[04-build-tailwind]:    {{< baseurl >}}assets/posts/frontend/2020/11-tabs/04-npm-build-tailwind.png
[04-postcss-run]:       {{< baseurl >}}assets/posts/frontend/2020/11-tabs/04-npm-postcss-verbose.png
[04-nodemon-watch]:     {{< baseurl >}}assets/posts/frontend/2020/11-tabs/04-nodemon-watch.png
[04-onchange-tailwind]: {{< baseurl >}}assets/posts/frontend/2020/11-tabs/04-onchange-tailwind.png

[config-package-json]:  {{< tutor-css-tools >}}/components/01-tabs/package.json
[config-postcss]:       {{< tutor-css-tools >}}/components/01-tabs/tailwind/postcss.config.js
[config-tailwind]:      {{< tutor-css-tools >}}/components/01-tabs/tailwind/tailwind.config.js
