---
type   : post
title  : "Tabs - CSS - Tailwind Build"
date   : 2020-11-07T09:17:35+07:00
slug   : tabs-css-tailwind
categories: [frontend]
tags      : [html, css]
keywords  : [npm install, package.json, postcss, configuration]
author : epsi
opengraph:
  image: assets/posts/frontend/2020/11-tabs/07-tabs-vim-panes.png

toc    : "toc-2020-11-component-tabs"

excerpt:
  Porting stylesheet to Tailwind CSS.

---

### Preface

> Goal: Porting stylesheet to Tailwind CSS.

#### Examples

This article series require three `tailwind` stylesheet.

* 01-tailwind.css

* 03-tailwind.css

* 04-tailwind.css

{{< highlight bash >}}
$ tree tailwind
tailwind
├── 01-tailwind.css
├── 03-tailwind.css
├── 04-tailwind.css
├── postcss.config.js
└── tailwind.config.js

0 directories, 5 files
{{< / highlight >}}

![Tabs Component: Tree Directory: Tailwind][04-tree-tailwind]

Note that the `package.json` is in the root directory.

-- -- --

### 01-tailwind.css

This is the simple one.

#### Configuration

{{< highlight javascript >}}
module.exports = {
  future: {
    purgeLayersByDefault: true,
  },
  purge: {
    enabled: true,
    content: ['./*.html'],
  },
  theme: {
    extend: {},
  },
  variants: {},
  plugins: [],
}
{{< / highlight >}}

[]

#### Stylesheet

* [gitlab.com/.../tailwind/01-tailwind.css][css-01-tailwind]

{{< highlight css >}}
@tailwind base;
@tailwind components;
@tailwind utilities;

/* Main Layout: Mobile First */
.tabs        { @apply flex flex-col; }
.tabs *      { @apply box-border; }

@screen md {
  .tabs      { @apply flex-row; }  
  .tabs>*    { min-height: 20rem; }
}

/* Inner and Outer Layout */
.tab-headers        { flex-basis: 30%; }
.tab-headers div    { @apply p-4 h-16 flex items-center; }
.tab-contents       { flex-basis: 70%; }
.tab-contents div   { @apply p-4 h-full; }

/* Border helper to distinct element */

.tabs div       { border: 1px dashed grey; }
.tabs div       { @apply bg-blue-300; }
.tabs div div   { @apply bg-blue-500; }
{{< / highlight >}}

* .

### 03-tailwind.css

This configuration change is optional.

#### Configuration

* [gitlab.com/.../tailwind/tailwind.config.js][config-tailwind]

{{< highlight javascript >}}
const colors = require('tailwindcss/colors')

module.exports = {
  important: false,
  future: {
    purgeLayersByDefault: true,
  },
  purge: {
    enabled: true,
    content: ['./*.html'],
  },
  theme: {
    extend: {},
    colors: {
      black: '#000',
      white: '#fff',
      gray: colors.trueGray,
      red: colors.rose,
      teal: colors.teal,
      blue: colors.blue,
      orange: colors.orange
    }
  },
  variants: {},
  plugins: [],
}
{{< / highlight >}}

[]

#### Stylesheet

* [gitlab.com/.../tailwind/03-tailwind.css][css-03-tailwind]

{{< highlight css >}}
@tailwind base;

@layer base {
  h1  { @apply text-4xl; }
  h2  { @apply text-3xl; }
  h3  { @apply text-2xl; }
}

@tailwind components;
@tailwind utilities;

body  { @apply font-sans m-4; }
section h1 { @apply m-4; }

/* Main Layout: Mobile First */

.tabs {
  @apply flex flex-col max-w-5xl
    border border-solid border-gray-200;
}

.tabs *      { @apply box-border; }

@screen md {
  .tabs      { @apply flex-row; }  
  .tabs>*    { min-height: 12rem; }
}

/* Inner and Outer Layout */

.tab-spacer  { @apply h-5 bg-gray-700; }

.tab-headers { flex-basis: 30%; }

.tab-headers div { 
  @apply p-4 h-16 flex items-center cursor-pointer
    duration-300 overflow-hidden text-white text-base;
}
.tab-contents       { flex-basis: 70%; }
.tab-contents div   { @apply p-4 h-full text-white text-base; }

/* Add some decoration */
.tab-headers div:hover { @apply bg-gray-500; }
{{< / highlight >}}

* .

#### Document: HTML Head

Now I can rewrite the HTML Head,
of the previous simple unobtrusive document:

{{< highlight html >}}
<head>
  <meta name="viewport"
        content="width=device-width, initial-scale=1">
  <title>Simple Tabs - Oldschool Javascript</title>
  <link rel="stylesheet" type="text/css" 
        href="../css/03-tailwind-simple.css">
  <script src="../js/custom/unobtrusive-simple.js"></script>
</head>
{{< / highlight >}}

### 04-tailwind.css

#### Configuration

The same as previous.

#### Stylesheet

* [gitlab.com/.../tailwind/04-tailwind.css][css-04-tailwind]

{{< highlight css >}}
@tailwind base;

@layer base {
  h1  { @apply text-4xl; }
  h2  { @apply text-3xl; }
  h3  { @apply text-2xl; }
}

@tailwind components;
@tailwind utilities;

@layer utilities { .flex-30 { flex-basis: 30% } }
@layer utilities { .flex-70 { flex-basis: 70% } }

body  { @apply font-sans m-4; }
section h1 { @apply m-4; }

/* Main Layout: Mobile First */

.tabs {
  @apply flex flex-col max-w-5xl
    border border-solid border-gray-200;
}

.tabs *      { @apply box-border; }

@screen md {
  .tabs      { @apply flex-row; }  
  .tabs>*    { min-height: 12rem; }
}

/* Inner and Outer Layout */

.tab-spacer  { @apply h-5 bg-gray-700; }

.tab-headers { @apply flex-30; }

.tab-headers div { 
  @apply pl-4 pr-4 h-16 flex items-center cursor-pointer
    duration-300 overflow-hidden text-white text-base;
}

.tab-headers div div    { @apply w-full; }
.tab-headers div:hover  { @apply bg-gray-500; }
.tab-headers div.active:hover       { @apply bg-gray-700; }
.tab-headers div.active div:hover   { @apply bg-gray-100; }

.tab-contents       { @apply flex-70; }
.tab-contents>div   { @apply pl-4 pr-4 h-full text-black text-base; }
.tab-content        { @apply p-4 h-full; }

@screen md {
  .tab-spacer       { @apply hidden; }
  .tab-headers div  { @apply pl-4 pr-0; }
  .tab-contents>div { @apply pl-0 pr-4; }
}

/* Color Decorations */
.tab-headers>div:hover              { @apply bg-gray-500; }
.tab-headers div.active:hover       { @apply bg-gray-700; }
.tab-headers div.active div:hover   { @apply bg-gray-300; }
.tab-content                        { @apply bg-white; }
.tab-contents div div.is-hovered    { @apply bg-gray-300; }
.tab-headers .bg-white { @apply text-black; }
.dummy-pallete { @apply hidden; }
{{< / highlight >}}

* .

Note that I added `@layer utilities` as below:


{{< highlight css >}}
@layer utilities { .flex-30 { flex-basis: 30% } }
@layer utilities { .flex-70 { flex-basis: 70% } }
{{< / highlight >}}

So I can write 

{{< highlight css >}}
.tab-headers    { @apply flex-30; }
.tab-contents   { @apply flex-70; }
{{< / highlight >}}

There is nothing special about this article.
After all, it is just porting,
from `css rules`, to `tailwind utility class`.

![Tabs Component: Tailwind Side by Side in ViM Panes][04-tabs-vim-panes]

#### Document: HTML Head

Now I can also rewrite the HTML Head,
of the previous unobtrusive document:

* [gitlab.com/.../tailwind/04-css-tailwind.html][html-04-tailwind-css]

{{< highlight html >}}
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Tabs - Tailwind CSS</title>
  <link rel="stylesheet" type="text/css" href="../css/04-tailwind-enhanced.css">
  <link rel="stylesheet" type="text/css" href="../css/02-border-radius.css">
  <script src="../js/custom/unobtrusive-enhanced.js"></script>
</head>
{{< / highlight >}}

-- -- --

### What's Next?

After `stylesheet`, Let's go back to `javascript`.

Consider continue reading [ [Tabs - JS - jQuery (obsolete)][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2020/11/08/tabs-js-jquery-cash/

[04-tree-tailwind]:     {{< baseurl >}}assets/posts/frontend/2020/11-tabs/04-tree-tailwind.png
[04-tabs-vim-panes]:    {{< baseurl >}}assets/posts/frontend/2020/11-tabs/04-tabs-vim-panes.png

[config-tailwind]:      {{< tutor-css-tools >}}/components/01-tabs/tailwind/tailwind.config.js

[html-04-tailwind-css]: {{< tutor-css-tools >}}/components/01-tabs/04-tailwind/04-css-tailwind.html
[css-01-tailwind]:      {{< tutor-css-tools >}}/components/01-tabs/tailwind/01-tailwind.css
[css-03-tailwind]:      {{< tutor-css-tools >}}/components/01-tabs/tailwind/03-tailwind.css
[css-04-tailwind]:      {{< tutor-css-tools >}}/components/01-tabs/tailwind/04-tailwind.css
