---
type   : post
title  : "Tabs - JS - Alpine Extracted"
date   : 2020-11-10T09:17:35+07:00
slug   : tabs-js-alpine-extracted
categories: [frontend]
tags      : [html, javascript]
keywords  : [onclick event, alpine.js]
author : epsi
opengraph:
  image: assets/posts/frontend/2020/11-tabs/00-tabs-mockup-mobile.png

toc    : "toc-2020-11-component-tabs"

excerpt:
  Moving Alpine.js's attribute using `x-spread`.

---

### Preface

> Goal: Moving Alpine.js's attribute using `x-spread`.

Writing `Alpine.js` is like writing configuration,
as opposed with writing logic in coding.

#### Article Steps

The `alpine.js` is divided into two parts.

* Inline Alpine.js, then

* Extracted Alpine.js.

The first one has already discussed in previous article.
Now all we need is, to extract from the element.

#### Examples

For tutorial purpose we need two layouts:

1. Simple layout, to explain jQuery.

2. Enhance layout, as real live example.

#### Preview

![Tabs Component: Enhanced Layout: Alpine][00-preview-mobile]

-- -- --

### Simple Tabs : Structure

#### Document: HTML Head

We can reuse previous `tailwind` stylesheet.

{{< highlight html >}}
  <title>Simple Tabs - Alpine.js (extracted)</title>
  <link rel="stylesheet" type="text/css" 
        href="../css/03-tailwind-simple.css">
  <script src="../js/vendors/alpine.min.js"></script>
  <script src="../js/custom/alpine-simple.js"></script>
{{< / highlight >}}

Yes, we are going to move stuff to an external javascript.

#### Document: HTML Body: Tab Headers

Now it is as simple as:

* [gitlab.com/.../06e-js-alpine-simple-spreads.html][html-06e-alpine-ex-s]

{{< highlight html >}}
  <main class="tabs" x-data="tabs()">
    <div class="tab-headers">
      <div x-spread="header_home">Home</div>
      <div x-spread="header_team">Team</div>
      <div x-spread="header_news">News</div>
      <div x-spread="header_about">About</div>
    </div>
      …
    </div>

    …
  </main>
{{< / highlight >}}

It looks tidy right 🙂?

#### How Does It Works?

We are going to initialize stuff with `x-data=tabs()`.
All the configuration, bundled in this `tabs()`function.

* [gitlab.com/.../js/custom/alpine-simple.js][js-custom-alpine-s]

{{< highlight javascript >}}
function tabs() {
  return {
    tab: 'news',
    // … more configuration here
  }
}
{{< / highlight >}}

#### Javascript: Handling Headers Event

The `x-spread` manage all the attributes.

{{< highlight html >}}
      <div x-spread="header_home">Home</div>
{{< / highlight >}}

So we can have the details somewhere else,
such as below code.

{{< highlight javascript >}}
function tabs() {
  return {
    tab: 'news',
    header_home: {
      ['@click']()      { this.tab = 'home' },
      [':class']()      { 
        return (this.tab == 'home') ? 'active bg-blue-500' : 'bg-gray-700';
      }
    },
    // … more configuration here
  }
}
{{< / highlight >}}

#### The Issue with Alpine

We still have to write down the declaration,
for each element such as below:

* [gitlab.com/.../js/custom/alpine-simple.js][js-custom-alpine-s]

{{< highlight javascript >}}
function tabs() {
  return {
    tab: 'news',
    header_home: {
      ['@click']()      { this.tab = 'home' },
      [':class']()      { 
        return (this.tab == 'home') ? 'active bg-blue-500' : 'bg-gray-700';
      }
    },
    header_team: {
      ['@click']()      { this.tab = 'team' },
      [':class']()      { 
        return (this.tab == 'team') ? 'active bg-teal-500' : 'bg-gray-700';
      }
    },
    header_news: {
      ['@click']()      { this.tab = 'news' },
      [':class']()      { 
        return (this.tab == 'news') ? 'active bg-red-500' : 'bg-gray-700';
      }
    },
    header_about: {
      ['@click']()      { this.tab = 'about' },
      [':class']()      { 
        return (this.tab == 'about') ? 'active bg-orange-500' : 'bg-gray-700';
      }
    },
    // … more configuration here
  }
}
{{< / highlight >}}

Or maybe it is just me.
I just still don't know how to do it yet.

#### Document: HTML Body: Tab Content

The same writing style,
also applied the tab contents.

* [gitlab.com/.../06e-js-alpine-simple-spreads.html][html-06e-alpine-ex-s]

{{< highlight html >}}
    <div class="tab-contents">
      <div x-spread="content_home">
        <h3>Home</h3>
        <p>Lorem ipsum dolor sit amet,
           consectetur adipiscing elit.
           Quisque in faucibus magna.</p>
      </div>
      <div x-spread="content_team">
        <h3>Team</h3>
        <p>Nulla luctus nisl in venenatis vestibulum.
           Nam consectetur blandit consectetur.</p>
      </div>
      <div x-spread="content_news">
        <h3>News</h3>
        <p>Phasellus vel tempus mauris,
           quis mattis leo.
           Mauris quis velit enim.</p>
      </div>
      <div x-spread="content_about">
        <h3>About</h3>
        <p>Interdum et malesuada fames ac ante
           ipsum primis in faucibus.
           Nulla vulputate tortor turpis,
           at eleifend eros bibendum quis.</p>
      </div>
    </div>
{{< / highlight >}}

#### Javascript: Handling Headers Event to Tab Content

The `x-spread` manage all the attributes.
Consider this example below, for `home` tab:

{{< highlight html >}}
  <main class="tabs" x-data="tabs()">
    <div class="tab-headers">
      <div x-spread="header_home">Home</div>
      …
    </div>

    <div class="tab-spacer"></div>

    <div class="tab-contents">
      <div x-spread="content_home">
        <h3>Home</h3>
        <p>Lorem ipsum dolor sit amet,
           consectetur adipiscing elit.
           Quisque in faucibus magna.</p>
      </div>
      …
    </div>
  </main>
{{< / highlight >}}

The same extracted attributes,
also applied to the tab contents.

* [gitlab.com/.../js/custom/alpine-simple.js][js-custom-alpine-s]

{{< highlight javascript >}}
function tabs() {
  return {
    tab: 'news',
    header_home: {
      ['@click']()      { this.tab = 'home' },
      [':class']()      { 
        return (this.tab == 'home') ? 'active bg-blue-500' : 'bg-gray-700';
      }
    },
    // … more configuration here
    content_home: {
      ['x-show']() { return (this.tab == 'home'); },
      [':class']() { return (this.tab == 'home') ? 'bg-blue-500' : ''; }
    },
    // … more configuration here
  }
}
{{< / highlight >}}

Again, I know it is long.
But that is all.

Your code should be working by now.

#### Preview

The same as previous example.

#### The Riddle: Issue with Alpine!

> Can we make this simpler?

Unfortunately I failed to combine the `x-spread` with `x-for`.
Perhaps I'm asking too much.
Or maybe I just do not know how to do it yet.

-- -- --

### Enhanced Tabs: Structure

How about real live component?
Let's the fun begin.

Remember that we only add these two feature:

* `hover` and,

* `bg-white`.

#### Document: HTML Head

We still can reuse previous stylesheet.

{{< highlight html >}}
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Tabs - Alpine.js (extracted)</title>
  <link rel="stylesheet" type="text/css" href="../css/04-tailwind-enhanced.css">
  <link rel="stylesheet" type="text/css" href="../css/02-border-radius.css">
  <script src="../js/vendors/alpine.min.js"></script>
  <script src="../js/custom/alpine-enhanced.js"></script>
{{< / highlight >}}

#### Document: HTML Body: Initialization

* [gitlab.com/.../06f-js-alpine-enhanced-spreads.html][html-06f-alpine-ex]

{{< highlight html >}}
  <main class="tabs" x-data="tabs()">
    /* .. elements here … */
  </main>
{{< / highlight >}}

* .

#### How Does It Works?

The same as the simple example above.
But with `hover` variable.
We are going to initialize stuff with `x-data=tabs()`.
All the configuration, bundled in this `tabs()`function.

* [gitlab.com/.../js/custom/alpine-enhanced.js][js-custom-alpine]

{{< highlight javascript >}}
function tabs() {
  return {
    tab: 'news',    
    hover: false,
    // … more configuration here
  }
}
{{< / highlight >}}

#### Document: HTML Body: Tab Headers

* [gitlab.com/.../06f-js-alpine-enhanced-spreads.html][html-06f-alpine-ex]

We have the third level `div`.

{{< highlight html >}}
    <div class="tab-headers">
    <div class="tab-headers">
      <div x-spread="header_home">
        <div x-spread="header_inner_home">Home</div>
      </div>
      <div x-spread="header_team">
        <div x-spread="header_inner_team">Team</div>
      </div>
      <div x-spread="header_news">
        <div <div x-spread="header_inner_news">News</div>
      </div>
      <div x-spread="header_about">
        <div <div x-spread="header_inner_about">About</div>
      </div>
    </div>
      …
    </div>
{{< / highlight >}}

Now I feel good. The `html` source code looks tidy.

#### Javascript: Handling Headers Event

The `x-spread` manage all the attributes.

{{< highlight html >}}
    <div class="tab-contents">
      <div x-spread="content_home">
        <div class="tab-content" x-spread="content_inner_home">
          <h3>Home</h3>
          …
{{< / highlight >}}

So we can have the details somewhere else, such as below code,
along with the `x-on:mouseenter` and `x-on:mouseleave` event.

* [gitlab.com/.../js/custom/alpine-enhanced.js][js-custom-alpine]

{{< highlight javascript >}}
function tabs() {
  return {
    tab: 'news',
    hover: false,
    header_home: {
      ['@click']()      { this.tab = 'home'; this.hover = this.tab; },
      ['@mouseenter']() { this.hover = (this.tab == 'home') ? this.tab : false; },
      ['@mouseleave']() { this.hover = false },
      [':class']()      { 
        return (this.tab == 'home') ? 'active bg-blue-500' : 'bg-gray-700';
      }
    },
    // … more configuration here
  }
}
{{< / highlight >}}

#### Document: HTML Body: Tab Content

We also have the third level `div` here.
The same writing style, also applied the tab contents.

* [gitlab.com/.../06f-js-alpine-enhanced-spreads.html][html-06f-alpine-ex]

{{< highlight html >}}
    <div class="tab-contents">
      <div x-spread="content_home">
        <div class="tab-content" x-spread="content_inner_home">
          <h3>Home</h3>
          <p>Lorem ipsum dolor sit amet,
             consectetur adipiscing elit.
             Quisque in faucibus magna.</p>
        </div>
      </div>
      …
    </div>
{{< / highlight >}}

#### Javascript: Handling Headers Event to Tab Content

The `x-spread` manage all the attributes.
Consider this example below, for `home` tab:

{{< highlight html >}}
  <main class="tabs" x-data="tabs()">
    <div class="tab-headers">
      <div x-spread="header_home">
        <div x-spread="header_inner_home">Home</div>
      </div>
      …
    </div>

    <div class="tab-spacer"></div>

    <div class="tab-contents">
      <div x-spread="content_home">
        <div class="tab-content" x-spread="content_inner_home">
          <h3>Home</h3>
          <p>Lorem ipsum dolor sit amet,
             consectetur adipiscing elit.
             Quisque in faucibus magna.</p>
        </div>
      </div>
    </div>
  </main>
{{< / highlight >}}

The same extracted attributes,
also applied to the tab contents.

* [gitlab.com/.../js/custom/alpine-enhanced.js][js-custom-alpine]

{{< highlight javascript >}}
function tabs() {
  return {
    tab: 'news',
    hover: false,
    header_home: {
      ['@click']()      { this.tab = 'home'; this.hover = this.tab; },
      ['@mouseenter']() { this.hover = (this.tab == 'home') ? this.tab : false; },
      ['@mouseleave']() { this.hover = false },
      [':class']()      { 
        return (this.tab == 'home') ? 'active bg-blue-500' : 'bg-gray-700';
      }
    },
    // … more configuration here
    header_inner_home: {
      [':class']() { return (this.tab == 'home') ? 'bg-white' : ''; }
    },
    // … more configuration here
    content_home: {
      ['x-show']() { return (this.tab == 'home'); },
      [':class']() { return (this.tab == 'home') ? 'bg-blue-500' : ''; }
    },
    // … more configuration here
    content_inner_home: {
      [':class']() { return (this.hover == 'home') ? 'is-hovered' : ''; }
    },
    // … more configuration here
  }
}
{{< / highlight >}}

You can examine yourself, how synching `hover` implemented here:

{{< highlight javascript >}}
function tabs() {
  return {
    tab: 'news',
    hover: false,
    header_home: {
      ['@click']()      { this.tab = 'home'; this.hover = this.tab; },
      ['@mouseenter']() { this.hover = (this.tab == 'home') ? this.tab : false; },
      ['@mouseleave']() { this.hover = false }
      }
    },
    // … more configuration here
    content_inner_home: {
      [':class']() { return (this.hover == 'home') ? 'is-hovered' : ''; }
    },
    // … more configuration here
  }
}
{{< / highlight >}}

I think that's all.

#### Preview

The same as previous example.

#### Complete

If you want, I also provide a complete version with `FontAwesome`.

* [gitlab.com/.../06g-complete.html][html-06g-alpine-fa]

But, this won't be my final journey.
I still want to explore modern javascript framework as well.

-- -- --

### What's Next?

This lightweight `alpine.js` can be leveraged to `vue.js`.

Consider continue reading [ [Tabs - JS - Vue.js (component)][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2020/11/10/tabs-js-alpine-extracted/

[koding-beneran]: https://www.youtube.com/channel/UCDe6vtIp8MqXqw_d4RmBX5g

[00-preview-mobile]:    {{< baseurl >}}assets/posts/frontend/2020/11-tabs/00-tabs-mockup-mobile.png

[html-06e-alpine-ex-s]: {{< tutor-css-tools >}}/components/01-tabs/06-alpine/06e-js-alpine-simple-spreads.html
[html-06f-alpine-ex]:   {{< tutor-css-tools >}}/components/01-tabs/06-alpine/06f-js-alpine-enhanced-spreads.html

[js-custom-alpine]:     {{< tutor-css-tools >}}/components/01-tabs/js/custom/alpine-enhanced.js
[js-custom-alpine-s]:   {{< tutor-css-tools >}}/components/01-tabs/js/custom/alpine-simple.js

[html-06g-alpine-fa]:   {{< tutor-css-tools >}}/components/01-tabs/06-alpine/06g-complete.html
