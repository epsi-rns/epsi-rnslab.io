---
type   : post
title  : "Tabs - JS - Vue Router"
date   : 2020-11-13T09:17:35+07:00
slug   : tabs-js-vue-router
categories: [frontend]
tags      : [html, vue]
keywords  : [application, router, vue.js]
author : epsi
opengraph:
  image: assets/posts/frontend/2020/11-vue/08-vue-router-qb-aloha-maui.png

toc    : "toc-2020-11-component-tabs"

excerpt:
  Preparing Vue Application, using Vue router.

---

### Preface

> Goal: Preparing Vue Application, using Vue router.

Welcome to modern web framework.
Oh.. I mean NPM bundle.

#### Managing Example Links

We are going to step into Vue component.
Just like usual I use step by step explanation.
This means four or five Vue components,
from mockup, then simple, then enhanced.
It is going to be tidy if we can put them all in one application.
Each components managed by different links.

To manage all these links, we require router.
This is where this article comes in place,
before we step into the real components in the next article.

You can also consider this article as introducing to vue application.

#### Reading Reference:

Official Site

* <https://cli.vuejs.org/>

-- -- --

### The Terminal Command

#### Install

As you might have seen in the example,
you should install with NPM first.

{{< highlight bash >}}
❯ npm install -g @vue/cli
{{< / highlight >}}

![Vue2 App: NPM: Vue CLI Install][08-cli-install]

#### Create App

We should start with creating application

{{< highlight bash >}}
$ vue create vue-hello
{{< / highlight >}}

And you will have a chance,
to get a preconfigured setup,
as figure above.

{{< highlight bash >}}
Vue CLI v4.5.9
? Please pick a preset: (Use arrow keys)
❯ Default ([Vue 2] babel, eslint) 
  Default (Vue 3 Preview) ([Vue 3] babel, eslint) 
  Manually select features 
{{< / highlight >}}

![Vue2 App: CLI: Vue Create App][08-vue2-create-opt]

Just continue until its done

{{< highlight bash >}}
🚀  Invoking generators...
📦  Installing additional dependencies...

...

⚓  Running completion hooks...

📄  Generating README.md...

🎉  Successfully created project vue-hello.
👉  Get started with the following commands:
{{< / highlight >}}

![Vue2 App: CLI: Vue Create App][08-vue2-create-fin]

#### Running for The First Time

You can safely enter the directory

{{< highlight bash >}}
$ cd vue-hello
{{< / highlight >}}

And run the `npm` script.

{{< highlight bash >}}
$ npm run serve
{{< / highlight >}}

After a few moment, there will a message in your terminal.

{{< highlight bash >}}
 DONE  Compiled successfully in 3433ms      3:35:08 PM


  App running at:
  - Local:   http://localhost:8080/ 
  - Network: http://192.168.1.152:8080/

  Note that the development build is not optimized.
  To create a production build, run npm run build.
{{< / highlight >}}

![Vue2 App: CLI: npm run serve][08-vue2-npm-run-serve]

#### Preview

Consider open your browser, to see result.

![Vue2 App: CLI: Default Page][08-vue2-qb-default]

The typical default `vue` page  can be shown as above figure.
The `title` in browser, follow the setting in `package.json`.

#### CLI Service

I once have difficulties with `cli-service`.
My workaround is, by also installing this locally, if required.

{{< highlight bash >}}
❯ npm i -s @vue/cli-service-global
+ @vue/cli-service-global@4.5.9
updated 1 package and audited 1380 packages in 29.084s
{{< / highlight >}}

![Vue2 App: NPM: Vue Service Install][08-service-install]

-- -- --

### The Default Vue2 Hello

#### App.vue

You should find these structure in `src/App.vue`

{{< highlight bash >}}
<template>
  <div id="app">
    …
  </div>
</template>

<script>
...
</script>

<style>
...
</style>
{{< / highlight >}}

You can learn about this structure somewhere else.
There are tons of basic tutorial in the internet.

#### main.js

You may spot the `src/main.js` file as below.

{{< highlight javascript >}}
import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
{{< / highlight >}}

What is this cryptic `render: h => h(App)` anyway 🤔?
It is actually short version of code below.

{{< highlight javascript >}}
new Vue({
  el: '#app', // side effect with eslint
  render: function (createElement) {
    return createElement(App);
  }
})
{{< / highlight >}}

You might also find old tutorial as below:

{{< highlight javascript >}}
new Vue({
  el: '#app',
  components: { App },
  template: '<App/>'
})
{{< / highlight >}}

This style has been deprecated,
and might confused people learning from ancient codes.
You should use `render: h => h(App)` instead.

#### HelloWorld.app

And at last, `src/components/HelloWorld.vue`
With basic `Vue` component structure.

{{< highlight bash >}}
<template>
  <div class="hello">
    …
  </div>
</template>

<script>
...
</script>

<style>
...
</style>
{{< / highlight >}}

-- -- --

### Using Vue2 Router

If you want to have muliple pages in single application.
You can utilize `router` to manage the application.

#### Reading Reference:

Official Site

* <https://router.vuejs.org/>

#### Credits

As a beginner, I once confused about this router.
But hey, the community is helpful.
I got help from this kind guy.

* <https://medium.com/@MFaridZia>

#### Install

As usual, we need to install the dependency first, before we can us it.

{{< highlight bash >}}
$ npm i -s -D vue-router
+ vue-router@3.4.9
added 1 package from 1 contributor and audited 1305 packages in 29.198s
{{< / highlight >}}

![Vue2 App: NPM: Vue2 Router Install][08-vue2-router-inst]

#### Directory Structure

Our directory structure should represent something similar as below:

{{< highlight bash >}}
$ tree -I node_modules
.
├── babel.config.js
├── package.json
├── package-lock.json
├── public
│   ├── favicon.ico
│   └── index.html
├── README.md
└── src
    ├── App.vue
    ├── assets
    │   └── logo.png
    ├── components
    │   └── HelloWorld.vue
    ├── main.js
    └── router
        └── index.js

5 directories, 11 files
{{< / highlight >}}

![Vue2 App: Router: General Tree Structure][08-vue2-tree-general]

We should create the `router/index.js` manually,
this will be explained later.

#### HelloWorld.vue

Consider we start from the component first.
I make this very simple.

* [gitlab.com/.../src/components/HelloWorld.vue][vue2-hello-world]

{{< highlight html >}}
<template>
  <div>
    <p><strong>{{ msg }}</strong></p>
  </div>
</template>

<script>
export default {
  name: 'HelloEarth',
  props: {
    msg: { type: String, default: 'Hello Earth!' }
  }
}
</script>
{{< / highlight >}}

#### index.js

After we are done with component,
we can continue to configure our very first router.

* `src/router/index.js`

{{< highlight javascript >}}
import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'default',
      component: HelloWorld,
      meta: { title: 'Hello' }
    }
  ]
})
{{< / highlight >}}

#### main.js

And then add it into `src/main.js` file as below.

{{< highlight javascript >}}
import Vue from 'vue'
import App from './App.vue'
import router from './router'

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
{{< / highlight >}}

We are not finished yet.

#### App.vue

At last, alter the landing page in `src/App.vue`.

{{< highlight html >}}
<template>
  <div id="app">
    <router-link to="hello">Hello</router-link>
    <router-view></router-view>
  </div>
</template>

<script>
export default {
  name: 'App'
}
</script>

<style>
body { font-family: Arial, Helvetica, sans-serif; }
#app { margin: 1rem; }
a    { padding-right: 1rem; }
</style>
{{< / highlight >}}

#### Preview

Consider open your browser, to see result.

![Vue2 App: Preview: Simple Router Page][08-vue2-qb-simple]

The very simple page of `vue` router example,
can be shown as above figure.

-- -- --

### Vue2 Multiple Component

We can leverage the basic example above with more component.

#### Directory Structure

{{< highlight bash >}}
$ tree src
src
├── App.vue
├── assets
├── components
│   ├── AlohaWorld.vue
│   ├── HelloWorld.vue
│   ├── TitleHeading.vue
│   └── YelloWorld.vue
├── main.js
└── router
    └── index.js

3 directories, 7 files
{{< / highlight >}}

Note that we have a `TitleHeading` component in above tree.

![Vue2 App: Router: Router Tree Structure][08-vue2-tree-router]

#### Greeting Component

The HelloWorld is remain intact.

* [gitlab.com/.../src/components/HelloWorld.vue][vue2-hello-world]

We have two more components:

* [gitlab.com/.../src/components/AlohaWorld.vue][vue2-aloha-world]

{{< highlight html >}}
<template>
  <div>
    <p><strong>{{ msg }}</strong></p>
  </div>
</template>

<script>
export default {
  name: 'AlohaMaui',
  props: {
    msg: { type: String, default: 'Aloha Maui!' }
  }
}
</script>
{{< / highlight >}}

* [gitlab.com/.../src/components/YelloWorld.vue][vue2-yello-world]

{{< highlight html >}}
<template>
  <div>
     <p><strong>{{ msg }}</strong></p>
  </div>
</template>

<script>
export default {
  name: 'YelloWorld',
  props: {
    msg: { type: String, default: 'Yellow World!' }
  }
}
</script>
{{< / highlight >}}

#### index.js

Now we can seriously make a real router:

* [gitlab.com/.../src/router/index.js][vue2-index-js]

{{< highlight javascript >}}
import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import AlohaWorld from '@/components/AlohaWorld'
import YelloWorld from '@/components/YelloWorld'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'default',
      component: YelloWorld,
      meta: { title: 'Yellow' }
    },
    {
      path: '/hello',
      name: 'Hello',
      component: HelloWorld,
      meta: { title: 'Hello' }
    },
    {
      path: '/aloha',
      name: 'Aloha',
      component: AlohaWorld,
      meta: { title: 'Aloha' }
    },
    {
      path: '/yellow',
      name: 'Yellow',
      component: YelloWorld,
      meta: { title: 'Yellow' }
    }
  ]
})
{{< / highlight >}}

But what is this additional `meta title` anyway 🤔?

#### Heading Component

This `TitleHeading` is using this `$route.meta.title` variable. 

* [gitlab.com/.../src/components/TitleHeading.vue][vue2-title-heading]

{{< highlight html >}}
<template>
  <section>
    <h1>{{ $route.meta.title }}</h1>
  </section>
</template>

<script>
export default {
  name: 'TitleHeading'
}
</script>
{{< / highlight >}}

This way, we can set different title,
based on page setting in `route` configuration.

#### App.vue

We should gather all these components above in `src/App.vue`.

* [gitlab.com/.../src/App.vue][vue2-app-vue]

{{< highlight html >}}
<template>
  <div id="app">
    <TitleHeading/>

    <router-link to="hello">Hello</router-link>
    <router-link to="aloha">Aloha</router-link>
    <router-link to="yellow">Yellow</router-link>

    <router-view></router-view>

    <p>I love myself as well.</p>
  </div>
</template>

<script>
import TitleHeading from './components/TitleHeading.vue'

export default {
  name: 'App',
  components: {
    TitleHeading
  }
}
</script>

<style>
body { font-family: Arial, Helvetica, sans-serif; }
#app { margin: 1rem; }
a    { padding-right: 1rem; color: #00796b; }
</style>
{{< / highlight >}}

Consider also have a nice link color in stylesheet.

#### Finishing with main.js

We want the `title` to be also dynamically changed right?
Then append these additional code into `src/main.js` file as below.

* [gitlab.com/.../src/main.js][vue2-main-js]

{{< highlight javascript >}}
import Vue from 'vue'
import App from './App.vue'
import router from './router'

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
  computed: {
    pageTitle: function() {
      return this.$route.meta.title;
    }
  },
  created () {
    document.title = this.$route.meta.title;
  },
  watch: {
    $route(to) {
      document.title = to.meta.title;
    },
  }
}).$mount('#app')
{{< / highlight >}} 

#### Preview

Consider open your browser, to see result.

![Vue2 App: Preview: Router with Different Page][08-vue2-qb-aloha-maui]

The multi component of `vue` router example in a single page,
can be shown as above figure.

Just click each link, and the content would be changed instantly.

![Vue2 App: Preview: Router with Different Page][08-vue2-qb-yellow-wld]

-- -- --

### Using Vue3 Router

`Vue3` had released in late 2020.
You can also utilize `Vue3`.
It is very similar with `Vue2`.

#### Reading Reference:

Official Site

* <https://next.router.vuejs.org/>

#### Install

As usual, we need to install the dependency first, before we can us it.

{{< highlight bash >}}
$ npm i -s -D vue-router@next
+ vue-router@4.0.0-rc.6
updated 1 package and audited 1327 packages in 30.097s
{{< / highlight >}}

![Vue3 App: NPM: Vue Router Install][08-vue3-router-inst]

#### Reusable Component

We don't need three component to do the same task.
We can utilize this single `HelloWorld.vue` component only,
and throw away the other two components.

__you wasted me up, and dumped me from the mountain__
__you wasted me up, and throw me from to the sea__

#### Directory Structure

{{< highlight bash >}}
$ tree src
src
├── App.vue
├── assets
├── components
│   ├── HelloWorld.vue
│   └── TitleHeading.vue
├── main.js
└── router
    └── index.js

3 directories, 5 files
{{< / highlight >}}

![Vue3 App: Router: Router Tree Structure][08-vue3-tree-router]

#### App.vue

We should gather all these components above in `src/App.vue`.

* [gitlab.com/.../src/App.vue][vue3-app-vue]

It is exactly the same as the previous `Vue2` version.

#### main.js

The `main.js` has a new syntax, as shown below:

* [gitlab.com/.../src/main.js][vue3-main-js]

{{< highlight javascript >}}
import { createApp, reactive } from 'vue'
import App from './App.vue'
import router from './router'

const Title = reactive({
  computed: {
    pageTitle: function() {
      return this.$route.meta.title;
    }
  },
  created () {
    document.title = this.$route.meta.title;
  },
  watch: {
    $route(to) {
      document.title = to.meta.title;
    },
  }
})

createApp(App)
  .use(router)
  .mixin(Title)
  .mount('#app')
{{< / highlight >}}

In `Vue3` we have chain functions: `use`, `mixin`, and `mount`.

#### Heading Component

The `TitleHeading` is remain intact.
It is exactly the same as previous `Vue2` code.

* [gitlab.com/.../src/components/TitleHeading.vue][vue3-title-heading]

#### HelloWorld.vue

Is there any changes here?

{{< highlight html >}}
<template>
  <p><strong>{{ msg }}</strong></p>
</template>

<script>
export default {
  name: 'HelloEarth',
  props: {
    msg: { type: String, default: 'Hello Earth!' }
  }
}
</script>
{{< / highlight >}}

Yes, the previous `Vue2` version has more `div`:

{{< highlight html >}}
<template>
  <div>
    <p><strong>{{ msg }}</strong></p>
  </div>
</template>
{{< / highlight >}}

#### index.js

Finnaly the router itself.
We can adapt the previous code to `Vue3` writing style.

* [gitlab.com/.../src/router/index.js][vue3-index-js]

{{< highlight javascript >}}
import { createWebHistory, createRouter } from "vue-router";
import HelloWorld from '@/components/HelloWorld'

const routes = [
  {
    path: '/',
    name: 'default',
    component: HelloWorld,
    meta: { title: 'Yellow' },
    props: { msg: 'How Are You?' }
  },
  {
    path: '/hello',
    name: 'Hello',
    component: HelloWorld,
    meta: { title: 'Hello' },
    props: { msg: 'Hello Earth!' }
  },
  {
    path: '/aloha',
    name: 'Aloha',
    component: HelloWorld,
    meta: { title: 'Aloha' },
    props: { msg: 'Aloha Maui!' }
  },
  {
    path: '/yellow',
    name: 'Yellow',
    component: HelloWorld,
    meta: { title: 'Yellow' },
    props: { msg: 'Yellow World!' }
  }
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
{{< / highlight >}}

As you can see in above `routes`,
they all use the same `HelloWorld` component.

> Simpler right?

#### Preview

It is exactly the same as previous.

![Vue3 App: Preview: Router with Different Page][08-vue2-qb-yellow-wld]

-- -- --

### Vue Mixin

This is a `Vue2` feature that I can use in `Vue3`.

In the next article I require three components,
all with the same template structure, but different behaviour.
How am I going to achieve this in `Vue`?

`mixin` comes to rescue We can go further from previous example.

#### TemplateAbstract.vue

First we have to move the template to other components.

* [gitlab.com/.../src/components/TemplateAbstract.vue][vue3-tmpl-abstract]

{{< highlight html >}}
<template>
  <p><strong>{{ msg }}</strong></p>
</template>
{{< / highlight >}}

This component contain the structure, only `<template>`.
The real world template structure,
might be longer, with a few line of tags.

#### HelloWorld.vue

Then use it somewhere, with `mixin`.

* [gitlab.com/.../src/components/HelloWorld.vue][vue3-hello-world]

{{< highlight html >}}
<script>
import TemplateAbstract from '@/components/TemplateAbstract'

export default {
  mixins:[TemplateAbstract],
  name: 'HelloEarth',
  props: {
    msg: { type: String, default: 'Hello Earth!' }
  }
}
</script>
{{< / highlight >}}

This component contain behaviour only, in `<script>`.
The real world script behaviour,
may vary between different components.

#### Preview

It is exactly the same as previous.
There is no need for more figure here.

> And we are done.

-- -- --

### What's Next?

It is a good time to go back to our very tabs component.
But this time we will represent in `Vue CLI` fashioned.

Consider continue reading [ [Tabs - JS - Vue2 Application][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2020/11/14/tabs-js-vue2-application/

[vue2-app-vue]:         {{< tutor-css-tools >}}/components/01-tabs/08-vue/vue2-router-overview/src/App.vue
[vue2-main-js]:         {{< tutor-css-tools >}}/components/01-tabs/08-vue/vue2-router-overview/src/main.js
[vue2-index-js]:        {{< tutor-css-tools >}}/components/01-tabs/08-vue/vue2-router-overview/src/router/index.js

[vue2-aloha-world]:     {{< tutor-css-tools >}}/components/01-tabs/08-vue/vue2-router-overview/src/components/AlohaWorld.vue
[vue2-hello-world]:     {{< tutor-css-tools >}}/components/01-tabs/08-vue/vue2-router-overview/src/components/HelloWorld.vue
[vue2-yello-world]:     {{< tutor-css-tools >}}/components/01-tabs/08-vue/vue2-router-overview/src/components/YelloWorld.vue
[vue2-title-heading]:   {{< tutor-css-tools >}}/components/01-tabs/08-vue/vue2-router-overview/src/components/TitleHeading.vue

[vue3-app-vue]:         {{< tutor-css-tools >}}/components/01-tabs/08-vue/vue3-router-overview/src/App.vue
[vue3-main-js]:         {{< tutor-css-tools >}}/components/01-tabs/08-vue/vue3-router-overview/src/main.js
[vue3-index-js]:        {{< tutor-css-tools >}}/components/01-tabs/08-vue/vue3-router-overview/src/router/index.js
[vue3-title-heading]:   {{< tutor-css-tools >}}/components/01-tabs/08-vue/vue2-router-overview/src/components/TitleHeading.vue
[vue3-tmpl-abstract]:   {{< tutor-css-tools >}}/components/01-tabs/08-vue/vue3-router-overview/src/components/TemplateAbstract.vue
[vue3-hello-world]:     {{< tutor-css-tools >}}/components/01-tabs/08-vue/vue3-router-overview/src/components/HelloWorld.vue

[08-cli-install]:       {{< baseurl >}}assets/posts/frontend/2020/11-vue/08-vue2-cli-install.png
[08-service-install]:   {{< baseurl >}}assets/posts/frontend/2020/11-vue/08-vue2-service-install.png
[08-vue2-router-inst]:  {{< baseurl >}}assets/posts/frontend/2020/11-vue/08-vue2-router-install.png
[08-vue2-create-opt]:   {{< baseurl >}}assets/posts/frontend/2020/11-vue/08-vue2-create-options.png
[08-vue2-create-fin]:   {{< baseurl >}}assets/posts/frontend/2020/11-vue/08-vue2-create-finished.png
[08-vue2-npm-run-serve]:{{< baseurl >}}assets/posts/frontend/2020/11-vue/08-npm-run-serve.png
[08-vue2-qb-default]:   {{< baseurl >}}assets/posts/frontend/2020/11-vue/08-vue2-router-qb-default-hello.png
[08-vue2-tree-general]: {{< baseurl >}}assets/posts/frontend/2020/11-vue/08-vue2-tree-general-structure.png
[08-vue2-qb-simple]:    {{< baseurl >}}assets/posts/frontend/2020/11-vue/08-vue2-router-qb-simple.png
[08-vue2-tree-router]:  {{< baseurl >}}assets/posts/frontend/2020/11-vue/08-vue2-tree-router-structure.png
[08-vue2-qb-aloha-maui]:{{< baseurl >}}assets/posts/frontend/2020/11-vue/08-vue2-router-qb-aloha-maui.png
[08-vue2-qb-yellow-wld]:{{< baseurl >}}assets/posts/frontend/2020/11-vue/08-vue2-router-qb-yellow-world.png

[08-vue3-router-inst]:  {{< baseurl >}}assets/posts/frontend/2020/11-vue/08-vue3-router-install.png
[08-vue3-tree-router]:  {{< baseurl >}}assets/posts/frontend/2020/11-vue/08-vue3-tree-router-structure.png
