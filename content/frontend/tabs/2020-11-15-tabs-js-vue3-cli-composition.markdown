---
type   : post
title  : "Tabs - JS - Vue3 Composition"
date   : 2020-11-15T09:17:35+07:00
slug   : tabs-js-vue3-composition
categories: [frontend]
tags      : [html, vue]
keywords  : [application, component, vue.js]
author : epsi
opengraph:
  image: assets/posts/frontend/2020/11-vue/08-tabs3-qb-preview-mobile.png

toc    : "toc-2020-11-component-tabs"

excerpt:
  Porting to Vue3 using different pattern.
  Solving this.$children removal issue.

---

### Preface

> Goal: Porting to Vue3 using different pattern.
> Solving this.$children removal issue.

In order to select tab in click event,
all the children data should be available in all child components,
This is why I need to pass all tabs data in parent component,
along with the children data, to all child components.

I assume you have read the previous article,
the Vue component, and Vue router, and Vue2 application.

-- -- --

### Pattern Changes

> From the `vue2` tabs to `vue3` tabs.

We are going to overhaul the previous pattern,
with these changes:

1. Using composition API instead of options API.
   Following the newly released `vue3`.

2. Using javascript data instead of template slot.
   Using `provide` and `inject` mechanism, instead of `$children`.

#### The $children Removal

> There must be another way 🙂.

As you might already know, the `$children` API,
has been deprecated in `vue3`.
The issue is my `vue2` tabs component,
rely heavily with `this.$children`.
I spent two days, trying to find out,
how to emulate `this.$children`, and find no luck.
I started to think that I might be trapped in `XY problem`.

Then I suddenly realize that I can achieve similar result,
if I could change entirely, the way I write this component pattern.


#### The Vue2 Tabs

Data is provided as `html` template, in a component.
This way is comfortable when we have a lot of `html` tags.

{{< highlight html >}}
<template>
   <tabs>
      <tab name="home" title="Home" color="bg-blue-500">
        <h3>Home</h3>
        <p>Lorem ipsum dolor sit amet,
           consectetur adipiscing elit.
           Quisque in faucibus magna.</p>
      </tab>
      …
  </tabs>
</template>
{{< / highlight >}}

We can just easily passing the `html` data through `slots`.

#### The Vue3 Tabs

Data is provided in `javascript` array, in external file.
This way, we can programatically passing data, between component.
By using basic types means, an easy way without black magic.

* [html-preprocessor/.../src/tabsArray.js][src-tabs-array-js]

{{< highlight javascript >}}
let tabsArray = [
  { name: 'home', title: 'Home', color: 'bg-blue-500',
    text: `Lorem ipsum dolor sit amet,
      consectetur adipiscing elit.
      Quisque in faucibus magna.` },
  { name: 'team', title: 'Team', color: 'bg-teal-500',
    text: `Nulla luctus nisl in venenatis vestibulum.
      Nam consectetur blandit consectetur.` },
  { name: 'news', title: 'News', color: 'bg-red-500',
    text: `Phasellus vel tempus mauris,
      quis mattis leo.
      Mauris quis velit enim.` },
  { name: 'about', title: 'About', color: 'bg-orange-500',
    text: `Interdum et malesuada fames ac ante
      ipsum primis in faucibus.
      Nulla vulputate tortor turpis,
      at eleifend eros bibendum quis.` }
];

export { tabsArray };
{{< / highlight >}}

This new component might not be the best practice,
but the new component works well.
This should be enough for me.

-- -- --

### Application: The Vue3 Tabs

Before we discuss about component,
consider refresh our memory,
to get the big picture onhow this application works.

#### Source Examples

You can obtain source examples here:

* [html-preprocessor/components/01-tabs/08-vue/vue3-tabs/][source-example]

#### Preview

You can test in your favorite browser:

![Vue App: Component Preview: Enhanced: Mobile][08-tabs-qb-mobile]

#### Components

Step by step component is named as below:

1. MainMockup: Basic (limited) interactivity.
2. MainSimple: Fully working simple tabs.
3. MainEnhanced: Fully working enhanced tabs.

You are going to see how elegant `vue3` composition,
in solving issue, compared with `vue2` counterpart.
The `provide` and `inject` mechanism is already happened in `vue2`,
and it is getting better in `vue3`.

#### Directory Structure

> Consider to get organized.

I have refactor each component into 

1. `Main`,
2. `Tabs`,
3. `TabHeader`,
4. `TabContent`

{{< highlight bash >}}
$ tree src
src
├── App.vue
├── assets
│   └── css
│       ├── background-colors.css
│       ├── border-radius.css
│       ├── enhanced-layout.css
│       └── simple-layout.css
├── components
│   ├── enhanced
│   │   ├── MainEnhanced.vue
│   │   ├── TabContentEnhanced.vue
│   │   ├── TabHeaderEnhanced.vue
│   │   └── TabsEnhanced.vue
│   ├── mockup
│   │   ├── MainMockup.vue
│   │   ├── TabContentMockup.vue
│   │   ├── TabHeaderMockup.vue
│   │   └── TabsMockup.vue
│   ├── simple
│   │   ├── MainSimple.vue
│   │   ├── TabContentSimple.vue
│   │   ├── TabHeaderSimple.vue
│   │   └── TabsSimple.vue
│   └── TitleHeading.vue
├── main.js
├── router
│   └── index.js
└── tabsArray.js

7 directories, 21 files
{{< / highlight >}}

![Vue Tabs App: App Tree Structure][08-tabs-tree-app]

We do not really need `Main*.vue`,
and we can instead use `Tabs*.vue` directly.
My intention is explaining an alternative implementation,
without using `this.$children`.
So we can compare with original code.

#### main.js

The `main.js` is exactly the same as previous `vue3` router example.

* [gitlab.com/.../src/main.js][src-main-js]

{{< highlight javascript >}}
import { createApp, reactive } from 'vue'
import App from './App.vue'
import router from './router'

const Title = reactive({
  computed: {
    pageTitle: function() {
      return this.$route.meta.title;
    }
  },
  created () {
    document.title = this.$route.meta.title;
  },
  watch: {
    $route(to) {
      document.title = to.meta.title;
    },
  }
})

createApp(App)
  .use(router)
  .mixin(Title)
  .mount('#app')
{{< / highlight >}}

#### App.vue

We should gather all these three components above in `src/App.vue`.

* [gitlab.com/.../src/App.vue][src-app-vue]

{{< highlight html >}}
<template>
  <div id="app">
    <section class="link-nav">
      <router-link to="simple-mockup">Simple Mockup</router-link>
      <router-link to="simple-tabs"  >Simple Tabs</router-link>
      <router-link to="enhanced-tabs">Enhanced Tabs</router-link>
    </section>

    <TitleHeading/>

    <router-view></router-view>
  </div>
</template>

<script>
import TitleHeading from './components/TitleHeading.vue'

export default {
  name: 'App',
  components: {
    TitleHeading
  }
}
</script>

<style>
body { font-family: Arial, Helvetica, sans-serif; }
#app { margin: 1rem; }
section.link-nav { margin-bottom: 1rem; }
a { padding-right: 1rem; color: #00796b; }
</style>
{{< / highlight >}}

#### Router: index.js

And the index is also similar.

* [gitlab.com/.../src/router/index.js][src-index-js]

{{< highlight javascript >}}
import { createWebHistory, createRouter } from "vue-router";
import MainMockup from   '@/components/mockup/MainMockup'
import MainSimple from   '@/components/simple/MainSimple'
import MainEnhanced from '@/components/enhanced/MainEnhanced'

const routes = [
  {
    path: '/',
    name: 'default',
    component: MainEnhanced,
    meta: { title: 'Default Page' }
  },
  {
    path: '/simple-mockup',
    name: 'MainMockup',
    component: MainMockup,
    meta: { title: 'Simple Tabs - Mockup' }
  },
  {
    path: '/simple-tabs',
    name: 'MainSimple',
    component: MainSimple,
    meta: { title: 'Simple Tabs - Component' }
  },
  {
    path: '/enhanced-tabs',
    name: 'MainEnhanced',
    component: MainEnhanced,
    meta: { title: 'Enhanced Tabs - Component' }
  }
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
{{< / highlight >}}

#### Heading Component

The `TitleHeading.vue` component is,
exactly the same as previous router example.

* [gitlab.com/.../src/components/TitleHeading.vue][src-title-heading-vue]

{{< highlight html >}}
<template>
  <section>
    <h1>{{ $route.meta.title }}</h1>
  </section>
</template>

<script>
export default {
  name: 'TitleHeading'
}
</script>
{{< / highlight >}}

I think we are done with the application setup.
We are ready for the element explanation.

-- -- --

### Component: MainMockup

#### Main Component: Complete

Without using `mixin`, we can have the main component as below:

* [gitlab.com/.../src/components/mockup/MainMockup.vue][src-main-mockup-vue]

{{< highlight javascript >}}
<template>
  <tabs />
</template>

<script>
import { provide } from 'vue'
import { tabsArray } from '@/tabsArray.js'
import tabs from '@/components/mockup/TabsMockup.vue'

export default {
  name: 'MainMockup',
  components: { tabs },
  setup() {
    provide('tabsArray', tabsArray)
  }
}
</script>
{{< / highlight >}}

#### Main Component: Passing Data

The template is simply without data:

{{< highlight html >}}
<template>
  <tabs />
</template>
{{< / highlight >}}

The data passed using `provide` mechanism as below:

{{< highlight javascript >}}
  setup() {
    provide('tabsArray', tabsArray)
  }
{{< / highlight >}}

Since the data is reusable between three components,
It is better to put the source in external file.

{{< highlight javascript >}}
import { tabsArray } from '@/tabsArray.js'
{{< / highlight >}}

Of course you can embed the data inside the component instead.

#### Tabs Component: The Container

This component do all the horse works.

* [gitlab.com/.../src/components/mockup/TabsMockup.vue][src-tabs-mockup-vue]

{{< highlight javascript >}}
<template>
  …
</template>

<script>
import { inject } from 'vue'
import tabheader  from '@/components/mockup/TabHeaderMockup.vue'
import tabcontent from '@/components/mockup/TabContentMockup.vue'

export default {
  name: 'TabsMockup',
  components: { tabheader, tabcontent },
  setup() {
    const tabs = inject('tabsArray')
    return { tabs }
  }
}
</script>

<style scoped>
  @import '../../assets/css/simple-layout.css';
</style>
{{< / highlight >}}

#### Tabs Component: Getting Data from The Parent

We can utilize the provided data in child component.

{{< highlight javascript >}}
  setup() {
    const tabs = inject('tabsArray')
    return { tabs }
  }
{{< / highlight >}}

You can spot the difference between examples.
In contrast with the previous `vue2` tabs mechanism, as below:

{{< highlight javascript >}}
  data() {
    return {
      tabs: this.$children
    }
  }
{{< / highlight >}}

#### Tabs Component: The Template

We have two separated `v-for` running in two different components.
Feel free to bind each `props` in element for each iteration.

* `tabheader`,

* `tabcontent`.

{{< highlight html >}}
<template>
  <main class="tabs">
    <div class="tab-headers">
      <tabheader
        v-for="tab in tabs" v-bind:key="tab.name"
        :title="tab.title" :color="tab.color"
        :tabname="tab.name"
      />
    </div>

    <div class="tab-spacer"></div>

    <div class="tab-contents">
      <tabcontent
        v-for="tab in tabs" v-bind:key="tab.name"
        :text="tab.text" :title="tab.title" :color="tab.color"
        :tabname="tab.name"
      />
    </div>
  </main>
</template>
{{< / highlight >}}

I utilize `:tabname="tab.name"` instead of `:name="tab.name"`,
so this identifier does not mistakenly collide,
with `this.name` (component name).

#### Tab Header Component

We do not have any javascript interactivity yet.
This is just contain basic `props` definition.

* [gitlab.com/.../src/components/mockup/TabHeaderMockup.vue][src-header-mockup-vue]

{{< highlight javascript >}}
<template>
  <div class="bg-gray-700">{{ title }}</div>
</template>

<script>
export default {
  name: 'TabHeaderMockup',
  props: {
    tabname: { type: String, required: true },
    title:   { type: String, required: true },
    color:   { type: String, required: true }
  }
}
</script>
{{< / highlight >}}

You can turn the tab into colourful tab header,
by using `v-bind` as below:

{{< highlight javascript >}}
<template>
  <div v-bind:class="color">{{ title }}</div>
</template>
{{< / highlight >}}

#### Tab Content Component

For this mockup, we only show `team` content.

* [gitlab.com/.../src/components/mockup/TabContentMockup.vue][src-content-mockup-vue]

{{< highlight javascript >}}
<template>
  <div v-bind:class="color"
       v-show="this.tabname == 'team'">
    <h3>{{ title }}</h3>
    <p>{{ text }}</p>
  </div>
</template>

<script>
export default {
  name: 'TabContentMockup',
  props: {
    tabname:  { type: String, required: true },
    title:    { type: String, required: true },
    text:     { type: String, required: true },
    color:    { type: String, required: true }
  }
}
</script>
{{< / highlight >}}

#### Preview

You can enjoy testing in your favorite browser:

![Vue App: Component Preview: Mockup: Tablet][08-tabs-qb-mockup]

-- -- --

### Component: MainSimple

#### Main Component

Very similar with previous mockup,
we only change the component name.

* [gitlab.com/.../src/components/simple/MainSimple.vue][src-main-simple-vue]

{{< highlight javascript >}}
import tabs from '@/components/simple/TabsSimple.vue'
{{< / highlight >}}

#### Tabs Component: The Template

Nothing is changed.
It is exactly the same with previous mockup template.

This component also use the same stylesheet.

#### Tabs Component: The Script

> Manage selected tab.

Since we want to manage selected tab, for both header and content,
we need to `provide` the variable, for both child components.
We also need this variable to be `reactive` too, based on click event.

* [gitlab.com/.../src/components/simple/TabsSimple.vue][src-tabs-simple-vue]

{{< highlight javascript >}}
import { inject } from 'vue'
import { provide, ref } from 'vue'
import tabheader  from '@/components/simple/TabHeaderSimple.vue'
import tabcontent from '@/components/simple/TabContentSimple.vue'

export default {
  name: 'TabsSimple',
  components: { tabheader, tabcontent },
  setup() {
    const tabs = inject('tabsArray')

    const tabSelected = ref('team')
    provide('tabSelected', tabSelected)

    return { tabs }
  }
}
{{< / highlight >}}

You can spot the improvement in above script.

#### Tab Header Component

We setup the javascript interactivity for this tab header component.

* [gitlab.com/.../src/components/simple/TabHeaderSimple.vue][src-header-simple-vue]

{{< highlight javascript >}}
<template>
  <div 
    v-on:click="selectTabByName(this.tabname)"
    v-bind:class="[activeClass(), colorClass()]"
   >{{ title }}</div>
</template>

<script>
import { inject } from 'vue'

export default {
  name: 'TabHeaderSimple',
  props: {
    tabname: { type: String, required: true },
    title:   { type: String, required: true },
    color:   { type: String, required: true }
  },
  setup() {
    const tabSelected = inject('tabSelected')
    return { tabSelected }
  },
  mounted() {
    this.selectTabByName(this.tabSelected);
  },
  methods: {
    selectTabByName(tabName) {
      this.tabSelected = tabName
    },
    activeClass : function () {
      return this.tabname == this.tabSelected ? 'active' : '';
    },
    colorClass  : function () {
      return this.tabname == this.tabSelected ?
               this.color : 'bg-gray-700';
    }
  }
}
</script>
{{< / highlight >}}

#### Tab Header Component: Selecting Tab

> How does it works?

It is similar wih previous `vue2` example,
but this time using `tabSelected` with `provide/inject` mechanism.
The `tabname` has been bound using `v-for`.

{{< highlight javascript >}}
    v-on:click="selectTabByName(this.tabname)"
{{< / highlight >}}

The methods is just oneliner.

{{< highlight javascript >}}
  setup() {
    const tabSelected = inject('tabSelected')
    return { tabSelected }
  },
  methods: {
    selectTabByName(tabName) {
      this.tabSelected = tabName
    }
  }
{{< / highlight >}}

With this script we also initialize the default tab using `mounted`.

{{< highlight javascript >}}
  mounted() {
    this.selectTabByName(this.tabSelected);
  },
{{< / highlight >}}

#### Tab Header Component: Binding Color

We are no longer rely on `tab` parameter anymore,
as we did with `vue2` examples.

{{< highlight javascript >}}
v-bind:class="[activeClass(tab), colorClass(tab)]"
{{< / highlight >}}

But instead it is simpler.

{{< highlight javascript >}}
<template>
  <div 
    v-on:click="selectTabByName(this.tabname)"
    v-bind:class="[activeClass(), colorClass()]"
   >{{ title }}</div>
</template>
{{< / highlight >}}

This can be happened,
because we already have `tabname` bound to each tab.

{{< highlight javascript >}}
  methods: {
    activeClass : function () {
      return this.tabname == this.tabSelected ? 'active' : '';
    },
    colorClass  : function () {
      return this.tabname == this.tabSelected ?
               this.color : 'bg-gray-700';
    }
  }
{{< / highlight >}}

#### Tab Content Component

We also have a few improvement here:

* [gitlab.com/.../src/components/simple/TabContentSimple.vue][src-content-simple-vue]

{{< highlight javascript >}}
<template>
  <div v-bind:class="this.color" 
       v-show="this.tabname == this.tabSelected">
    <h3>{{ title }}</h3>
    <p>{{ text }}</p>
  </div>
</template>

<script>
import { inject } from 'vue'

export default {
  name: 'TabContentMockup',
  props: {
    tabname:  { type: String, required: true },
    title:    { type: String, required: true },
    text:     { type: String, required: true },
    color:    { type: String, required: true }
  },
  setup() {
    const tabSelected = inject('tabSelected')   
    return { tabSelected }
  }
}
</script>
{{< / highlight >}}

We can choose which tab shown by already bound `tabname`.

{{< highlight html >}}
  <div v-bind:class="this.color" 
       v-show="this.tabname == this.tabSelected">
    …
  </div>
{{< / highlight >}}

The `tabSelected` is also using `provide/inject` mechanism.

{{< highlight javascript >}}
  setup() {
    const tabSelected = inject('tabSelected')
    return { tabSelected }
  }
{{< / highlight >}}

#### Preview

Again, you can enjoy testing in your favorite browser:

![Vue App: Component Preview: Simple: Tablet][08-tabs-qb-simple]

-- -- --

### Component: MainEnhanced

This is our fully functional tabs component.

#### Main Component

Very similar with previous mockup,
we only change the component name.

* [gitlab.com/.../src/components/enhanced/enhancedSimple.vue][src-main-enhanced-vue]

{{< highlight javascript >}}
import tabs from '@/components/enhanced/TabsEnhanced.vue'
{{< / highlight >}}

#### Tabs Component: The Template

We have different class name using `-enh`,
to avoid stylesheet collision.

{{< highlight html >}}
<template>
  <main class="tabs-enh">
    <div class="tab-enh-headers">
      <tabheader
        v-for="tab in tabs" v-bind:key="tab.name"
        :title="tab.title" :color="tab.color"
        :tabname="tab.name"
      />
    </div>

    <div class="tab-enh-spacer"></div>

    <div class="tab-enh-contents">
      <tabcontent
        v-for="tab in tabs" v-bind:key="tab.name"
        :text="tab.text" :title="tab.title" :color="tab.color"
        :tabname="tab.name"
      />
    </div>
  </main>
</template>
{{< / highlight >}}

The rest is the same.
No difference.

#### Tabs Component: The Stylesheet

This component use different stylesheet.

{{< highlight css >}}
  @import '../../assets/css/enhanced-layout.css';
  @import '../../assets/css/background-colors.css';
  @import '../../assets/css/border-radius.css';
{{< / highlight >}}

#### Tabs Component: The Script

> Manage selected tab.

Since we want to also manage hovered tab,
we also need to `provide` the `tabHovered` variable.
We also need this variable to be `reactive` too,
based on mouse event.

* [gitlab.com/.../src/components/enhanced/TabsEnhanced.vue][src-tabs-enhanced-vue]

{{< highlight javascript >}}
import { inject } from 'vue'
import { provide, ref } from 'vue'
import tabheader  from '@/components/enhanced/TabHeaderEnhanced.vue'
import tabcontent from '@/components/enhanced/TabContentEnhanced.vue'

export default {
  name: 'TabsEnhanced',
  components: { tabheader, tabcontent },
  setup() {
    const tabs = inject('tabsArray')

    const tabSelected = ref('team')
    const tabHovered  = ref('')
    provide('tabSelected', tabSelected)
    provide('tabHovered',  tabHovered)

    return { tabs }
  }
}
{{< / highlight >}}

You can spot both `tabSelected` and `tabHovered` in above script.
Both contain the name of the tab as `string`,
instead of just `boolean` value.

There is no need to return value, for `provide`.
But we need to return value, for `inject` in both child component.

#### Tab Header Component

We setup more javascript interactivity for this tab header component.

* [gitlab.com/.../src/components/enhanced/TabHeaderEnhanced.vue][src-header-enhanced-vue]

{{< highlight javascript >}}
<template>
  <div 
    v-on:click="selectTabByName(this.tabname)"
    v-on:mouseenter="this.tabHovered = this.tabname"
    v-on:mouseleave="this.tabHovered = ''"
    v-bind:class="[activeClass(), colorClass()]">
    <div
      v-bind:class="{ 'bg-white' : this.tabname == this.tabSelected }"
     >{{ title }}</div>
  </div>
</template>

<script>
import { inject } from 'vue'

export default {
  name: 'TabHeaderSimple',
  props: {
    tabname: { type: String, required: true },
    title:   { type: String, required: true },
    color:   { type: String, required: true }
  },
  setup() {
    const tabSelected = inject('tabSelected')
    const tabHovered  = inject('tabHovered')
    return { tabSelected, tabHovered }
  },
  mounted() {
    this.selectTabByName(this.tabSelected);
  },
  methods: {
    selectTabByName(tabName) {
      this.tabSelected = tabName
    },
    activeClass : function () {
      return this.tabname == this.tabSelected ? 'active' : '';
    },
    colorClass  : function () {
      return this.tabname == this.tabSelected ?
               this.color : 'bg-gray-700';
    }
  }
}
</script>
{{< / highlight >}}

#### Tab Header Component: Template

We have more `<div>`:

{{< highlight html >}}
<template>
  <div …>
    <div
      v-bind:class="{ 'bg-white' : this.tabname == this.tabSelected }"
     >{{ title }}</div>
  </div>
</template>
{{< / highlight >}}

#### Tab Header Component: Synching Hover

> How does it works?

It is similar wih previous `vue2` example,
but this time using `tabHovered` with `provide/inject` mechanism.
The `tabname` has been bound using `v-for`.

{{< highlight javascript >}}
    v-on:mouseenter="this.tabHovered = this.tabname"
    v-on:mouseleave="this.tabHovered = ''"
{{< / highlight >}}

And in script we also initialize the default tab using `mounted`.

{{< highlight javascript >}}
  setup() {
    const tabSelected = inject('tabSelected')
    const tabHovered  = inject('tabHovered')
    return { tabSelected, tabHovered }
  }
{{< / highlight >}}

#### Tab Content Component

We also have a few improvement here:

* [gitlab.com/.../src/components/enhanced/TabContentEnhanced.vue][src-content-enhanced-vue]

{{< highlight javascript >}}
<template>
  <div v-bind:class="this.color" 
       v-show="this.tabname == this.tabSelected">
    <div class="tab-enh-content"
         v-bind:class="{ 'is-hovered' : this.tabHovered == this.tabname}">
      <h3>{{ title }}</h3>
      <p>{{ text }}</p>
    </div>
  </div>
</template>

<script>
import { inject } from 'vue'

export default {
  name: 'TabContentMockup',
  props: {
    tabname:  { type: String, required: true },
    title:    { type: String, required: true },
    text:     { type: String, required: true },
    color:    { type: String, required: true }
  },
  setup() {
    const tabSelected = inject('tabSelected')
    const tabHovered  = inject('tabHovered')
    return { tabSelected, tabHovered }
  }
}
</script>
{{< / highlight >}}

#### Tab content Component: Template

We have more `<div>`:

{{< highlight html >}}
<template>
  <div …>
    <div class="tab-enh-content" …>
       …
    </div>
  </div>
</template>
{{< / highlight >}}

#### Tab Content Component: Synching Hover

> How does it works?

We can alter the color of the hovered tab header using bound `tabname`.

{{< highlight html >}}
    <div class="tab-enh-content"
         v-bind:class="{ 'is-hovered' : this.tabHovered == this.tabname}">
      <h3>{{ title }}</h3>
      <p>{{ text }}</p>
    </div>
{{< / highlight >}}

The `tabHovered` is also using `provide/inject` mechanism.

{{< highlight javascript >}}
  setup() {
    const tabSelected = inject('tabSelected')
    const tabHovered  = inject('tabHovered')
    return { tabSelected, tabHovered }
  }
{{< / highlight >}}

#### Preview

At last, our fully functional, tabs component in browser.

![Vue App: Component Preview: Enhanced: Tablet][08-tabs-qb-enhanced]

I must admit that my `vue` knowledge is still limited.
I'm open to other possibity,
and also other issue comes with this component pattern.

> I still have so much to learn.

-- -- --

### What's Next?

Later.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}frontend/2020/11/14/tabs-js-vue-app/

[08-tabs-tree-app]:     {{< baseurl >}}assets/posts/frontend/2020/11-vue/08-tabs3-tree-app-structure.png
[08-tabs-qb-mobile]:    {{< baseurl >}}assets/posts/frontend/2020/11-vue/08-tabs3-qb-preview-mobile.png
[08-tabs-qb-mockup]:    {{< baseurl >}}assets/posts/frontend/2020/11-vue/08-tabs3-qb-preview-mockup.png
[08-tabs-qb-simple]:    {{< baseurl >}}assets/posts/frontend/2020/11-vue/08-tabs3-qb-preview-simple.png
[08-tabs-qb-enhanced]:  {{< baseurl >}}assets/posts/frontend/2020/11-vue/08-tabs3-qb-preview-enhanced.png

[source-example]:       {{< tutor-css-tools >}}/components/01-tabs/08-vue/vue3-tabs/
[src-app-vue]:          {{< tutor-css-tools >}}/components/01-tabs/08-vue/vue3-tabs/src/App.vue
[src-main-js]:          {{< tutor-css-tools >}}/components/01-tabs/08-vue/vue3-tabs/src/main.js
[src-tabs-array-js]:    {{< tutor-css-tools >}}/components/01-tabs/08-vue/vue3-tabs/src/tabsArray.js
[src-index-js]:         {{< tutor-css-tools >}}/components/01-tabs/08-vue/vue3-tabs/src/router/index.js

[src-title-heading-vue]:{{< tutor-css-tools >}}/components/01-tabs/08-vue/vue3-tabs/src/components/TitleHeading.vue

[src-main-mockup-vue]:      {{< tutor-css-tools >}}/components/01-tabs/08-vue/vue3-tabs/src/components/mockup/MainMockup.vue
[src-tabs-mockup-vue]:      {{< tutor-css-tools >}}/components/01-tabs/08-vue/vue3-tabs/src/components/mockup/TabsMockup.vue
[src-header-mockup-vue]:    {{< tutor-css-tools >}}/components/01-tabs/08-vue/vue3-tabs/src/components/mockup/TabHeaderMockup.vue
[src-content-mockup-vue]:   {{< tutor-css-tools >}}/components/01-tabs/08-vue/vue3-tabs/src/components/mockup/TabContentMockup.vue

[src-main-simple-vue]:      {{< tutor-css-tools >}}/components/01-tabs/08-vue/vue3-tabs/src/components/simple/MainSimple.vue
[src-tabs-simple-vue]:      {{< tutor-css-tools >}}/components/01-tabs/08-vue/vue3-tabs/src/components/simple/TabsSimple.vue
[src-header-simple-vue]:    {{< tutor-css-tools >}}/components/01-tabs/08-vue/vue3-tabs/src/components/simple/TabHeaderSimple.vue
[src-content-simple-vue]:   {{< tutor-css-tools >}}/components/01-tabs/08-vue/vue3-tabs/src/components/simple/TabContentSimple.vue

[src-main-enhanced-vue]:    {{< tutor-css-tools >}}/components/01-tabs/08-vue/vue3-tabs/src/components/enhanced/MainEnhanced.vue
[src-tabs-enhanced-vue]:    {{< tutor-css-tools >}}/components/01-tabs/08-vue/vue3-tabs/src/components/enhanced/TabsEnhanced.vue
[src-header-enhanced-vue]:  {{< tutor-css-tools >}}/components/01-tabs/08-vue/vue3-tabs/src/components/enhanced/TabHeaderEnhanced.vue
[src-content-enhanced-vue]: {{< tutor-css-tools >}}/components/01-tabs/08-vue/vue3-tabs/src/components/enhanced/TabContentEnhanced.vue
