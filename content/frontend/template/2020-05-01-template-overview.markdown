---
type   : post
title  : "Template - Overview"
date   : 2020-05-01T09:17:35+07:00
slug   : template-overview
categories: [frontend]
tags      : [html]
keywords  : [overview]
author : epsi
opengraph:
  image: assets-frontend/2020/05/01-html-hyperlink.png

toc    : "toc-2020-05-template"

excerpt:
  An overview about HTML Preprocessor.

---

### Preface

> Goal: An overview about HTML Preprocessor.

Last month, I wrote a few articles about HTML,
from basic stuff until bundler such as webpack, roller, and parcel.
The big gap is the template engine aka HTML preprocessor.
So I decide to make an article series dedicated to HTML preprocessor.

`Template Engine` in `MVC` context is the view parts.
This means, **frontend** can alter views without messing with **backend**.
However `templating engine` is also a fundamental part of learning **backend**.

![The Process of Learning Plain HTML Step by Step][learning-html-step]

I choose the term `HTML preprocessor`,
because the word `template` might refer to somethine else,
such as `themes`, or `boilerplate`, or `example code`.

#### Which Template ?

> My aim is to give ready to use examples.

We have to pick what example to provide.
There are so many template,
There is no use to cover them all.

I start with easy to use JS based template engine.
They are Pug, Nunjucks, EJS, Handlebars.
And test each with Grunt, Express, and Oak.

![Templating Engine][illustration-engine]

Then I might continue with other stuff as well,
such as Jinja2, Twig, Blade, and else.
I learn those things a decade ago, and
I need to refresh my youth memories,
for use with the nowadays era of modern web development.
In short, update my personal knowledge.

#### Comparison

For you who haven't got any idea what template is,
you can have a look at these express examples below:

* Pug

![Express: Loop in Pug][03-pug-panes]

* Nunjucks

![Express: Loop in Nunjucks][03-nunjucks-panes]

* EJS

![Express: Loop in EJS][03-ejs-panes]

* Handlebars

![Express: Loop in Handlebars][03-handlebars-panes]

#### Step By Step.

I arrange this article series as below:

1. Fundamental Stuff: Issue with HTML.

2. Render Static HTML Using Grunt:
   Pug, Nunjucks, EJS, Handlebars

3. Server Side Express and Koa (NodeJS):
   Pug, Nunjucks, EJS, Handlebars

4. Static Site Generator:
   Eleventy+Nunjucks, Pelican+Jinja, Jekyll+Liquid

![Templating Engine in ExpressJS][illustration-express]

#### On Consideration

> I'm not sure

I consider to continue, if I could have time.

5. Server Side Oak (DenoTS): : (maybe) Pug, Nunjucks, EJS, Handlebars

After above stuff, I will consider continue to Non JS Web Framework:

a. PHP Symfony: Twig
   
b. PHP Laravel: Blade
   
c. Flask: Jinja2

d. Ruby on Rails: Not known yet

e. Java Spring: Not known yet

#### Source Examples

You can obtain source examples here:

* [html-basic][source-example]

-- -- --

### What's Next?

Before we begin, we need a HTML case.
A basic example, that will be used, as a based of all template engine,
throughout this article series

Consider continue reading [ [Template - HTML - Example Case][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}frontend/2020/05/03/template-html-link/

[source-example]:           {{< tutor-css-tools >}}/html-preprocessor/

[03-pug-panes]:             {{< assets-frontend >}}/2020/05/03-pug-panes.png
[03-nunjucks-panes]:        {{< assets-frontend >}}/2020/05/03-nunjucks-panes.png
[03-ejs-panes]:             {{< assets-frontend >}}/2020/05/03-ejs-panes.png
[03-handlebars-panes]:      {{< assets-frontend >}}/2020/05/03-handlebars-panes.png

[illustration-engine]:      {{< assets-frontend >}}/2020/05/templating-engine.png
[illustration-express]:     {{< assets-frontend >}}/2020/05/express-template-engine.png
[learning-html-step]:       {{< assets-frontend >}}/2020/preface-learning-html-step-by-step.png
