---
type   : post
title  : "Template - HTML - Example Case"
date   : 2020-05-03T09:17:35+07:00
slug   : template-html-link
categories: [frontend]
tags      : [html]
keywords  : [overview]
author : epsi
opengraph:
  image: assets-frontend/2020/05/01-html-hyperlink.png

toc    : "toc-2020-05-template"

excerpt:
  Build a HTML Case for Use with This Article Series.

---

### Preface

> Goal: Build a HTML Case for Use with This Article Series.

#### Source Examples

You can obtain source examples here:

* [html-preprocessor/01-basic][source-example]

#### Reusable Parts

While working with many pages,
HTML documents might have occuring parts,
such as: sidebar, navbar, header, footer, and so on.
Usually, the only difference is just the content.
The issue with HTML document is, it is not modular.
You have to put all the html parts in the document.
This means we cannot reuse html parts.

> The issue with HTML document is, it is not modular.

From historical perspective,
`HTML Preprocessor` only solve this issue.
But later, added functionality such as `template inheritance`,
and `block building` and variables.

-- -- --

### HTML Case

We begin with pure `html` before using `templating engine` later.

#### Simple Hyperlink Pages

> How do I make each hyperlink refer to each content?

I found a good case, based on a beginner question.
Consider a page containing hyperlinks in figure below:

![HTML: Hyperlink Example][01-html-hyperlink]

This should be a simple task.
When the `HTML` clicked,
it must show the content of `HTML page`.
When the `CSS` clicked,
it must show the content of `CSS page`, and so on.

#### w3schools Example

> A real life page

With help of `w3c.css` the `sidebar` would looks like below code:

* [w3css_sidebar.asp](https://www.w3schools.com/w3css/w3css_sidebar.asp)

{{< highlight html >}}
  <div class="w3-sidebar w3-bar-block w3-card w3-animate-left" 
       style="display:none" id="mySidebar">
    <button class="w3-bar-item w3-button w3-large"
       id="closeNav" >Close &times;</button>
    <a class="w3-bar-item w3-button"
       href="index.html">Home</a>
    <a class="w3-bar-item w3-button"
       href="html.html">HTML Link</a>
    <a class="w3-bar-item w3-button"
       href="css.html">CSS Link</a>
    <a class="w3-bar-item w3-button"
       href="php.html">PHP Link</a>
    <a class="w3-bar-item w3-button"
       href="javascript.html">Javascript Link</a>
  </div>
{{< / highlight >}}

And the `navbar`, also

* [w3css_navigation.asp](https://www.w3schools.com/w3css/w3css_navigation.asp)

{{< highlight html >}}
    <div class="w3-bar w3-black">
      <a class="w3-bar-item w3-button"
         href="index.html">Home</a>
      <a class="w3-bar-item w3-button"
         href="html.html">HTML</a>
      <a class="w3-bar-item w3-button"
         href="css.html" >CSS</a>
      <a class="w3-bar-item w3-button"
         href="php.html" >PHP</a>
      <a class="w3-bar-item w3-button"
         href="javascript.html">Javascript</a>
    </div>
{{< / highlight >}}

Now we can have complete page as below code:

{{< highlight html >}}
<!DOCTYPE html>
<html>
<head>
  <title>HTML Link Example: Home</title>
  <meta charset="utf-8">
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  <script src="js/script.js"></script>
</head>
<body>

  <div class="w3-sidebar w3-bar-block w3-card w3-animate-left" 
       style="display:none" id="mySidebar">
    ...
  </div>

  <div id="main">

    <div class="w3-teal">
      <button class="w3-button w3-teal w3-xlarge"
              id="openNav">&#9776;</button>
      <div class="w3-container">
        <h1>My Page</h1>
      </div>
    </div>

    <div class="w3-bar w3-black">
      ...
    </div>

    <div class="w3-container">
      <p>Home Page</p>
    </div>

  </div>

</body>
</html>
{{< / highlight >}}

You can obtain the source `html` code here:

* [html-preprocessor/01-basic/index.html][01-basic-index]

Alowng with the `javascript` code here:

* [html-preprocessor/01-basic/js/script.js][01-basic-js-script]

This also fine with single `html` document.

#### Directory Structure.

> Requirement: The example case must consist multiple pages
  that refer to the same HTML parts.

{{< highlight bash >}}
❯ tree
.
├── css.html
├── html.html
├── index.html
├── javascript.html
├── js
│   └── script.js
└── php.html
{{< / highlight >}}

![HTML: Example: Tree][01-html-tree-example]

All these `html` artefacts contain the same `sidebar` and `navbar`.
And actually almost the same `head`.

The only different parts are,
the `title` and the `content` as shown in below code:

{{< highlight html >}}
<!DOCTYPE html>
<html>
<head>
  <title>HTML Link Example: CSS</title>
  ...
</head>
<body>

  ...

  <div id="main">

    ...

    <div class="w3-container">
      <p>CSS Page</p>
    </div>

  </div>

</body>
</html>
{{< / highlight >}}

#### Editing Parts

> Issue: You repeat yourself.

The problem comes when you have to,
alter the hyperlink content in multiple `html` pages.
You have to edit all the `html` files.

This tedious task, is a kind of boring.
And also error prone.
We need a tool to solve the situation.

-- -- --

### What's Next?

`HTML Preprocessor` comes to solve this issue.
In the next part, we have four example: `pug`, `nunjucks`, `EJS` and `handlebars`.

Consider continue reading [ [Template - Grunt - Pug][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}frontend/2020/05/05/template-grunt-pug/

[01-html-hyperlink]:        {{< assets-frontend >}}/2020/05/01-html-hyperlink.png
[01-html-tree-example]:     {{< assets-frontend >}}/2020/05/01-html-tree-example.png

[source-example]:           {{< tutor-css-tools >}}/html-preprocessor/01-basic/
[01-basic-index]:           {{< tutor-css-tools >}}/html-preprocessor/01-basic/index.html
[01-basic-js-script]:       {{< tutor-css-tools >}}/html-preprocessor/01-basic/js/script.js
