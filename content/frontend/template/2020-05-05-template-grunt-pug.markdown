---
type   : post
title  : "Template - Grunt - Pug"
date   : 2020-05-05T09:17:35+07:00
slug   : template-grunt-pug
categories: [frontend]
tags      : [node, grunt, pug]
keywords  : [template]
author : epsi
opengraph:
  image: assets-frontend/2020/05/01-html-hyperlink.png

toc    : "toc-2020-05-template"

excerpt:
  Separate HTML parts using Pug.
  Build a Static Files using Grunt.

---

### Preface

> Goal: Build static HTML files using partial Pug utilizing Grunt.

`pug` is good for beginner who want to step into `templating engine`.

* First the indented looks, that is entirely different with `html`.

* Second, it just works with Command Line Interface.
  No need third party tools such as `grunt` or `gulp`.

Unfortunately, not all `html processor` as easy as `pug`.
So for the rest of the chapter here,
we are going to build `html` using `grunt`.

#### Official Documentation

* [pugjs.org](https://pugjs.org/)

* [gruntjs.com](https://gruntjs.com/)

#### Pug With Grunt

I have already wrote an article about `pug` here:

* [Tools - HTML - Pug][local-pug]

Along with an example of how to build `grunt`.

* [Task Runner - Grunt][local-grunt]

#### Source Examples

You can obtain source examples here:

* [html-preprocessor/02-grunt/pug/][source-example]

-- -- --

### 1: Pug Case

We use `pug` as `html preprocessor` to build `html` documents.
Consider transform our last **HTML Case** into **Pug Case**.

#### Install

{{< highlight bash >}}
$ npm i -g pug-cli
{{< / highlight >}}

#### Directory Structure

Consider `pug` structure.

{{< highlight bash >}}
└── views
    ├── css.pug
    ├── html.pug
    ├── index.pug
    ├── javascript.pug
    ├── partials
    │   ├── head.pug
    │   ├── layout.pug
    │   ├── navbar.pug
    │   └── sidebar.pug
    └── php.pug
{{< / highlight >}}

We want the `pug` to build `html` artefacts as below:

{{< highlight bash >}}
└── build
    ├── css.html
    ├── html.html
    ├── index.html
    ├── javascript.html
    └── php.html
{{< / highlight >}}

![Pug: Example: Tree][02-pug-tree-example]

Now we can separate each html parts.

#### Layout

* [html-preprocessor/.../views/partials/layout.pug][02-views-partials-layout]

{{< highlight haskell >}}
doctype html
html(lang="en")

- var title = "Link Example: Default"

  head
    include head.pug
  body
    include sidebar.pug

    #main
      include navbar.pug
      .w3-container
        block content
          p Default Page
{{< / highlight >}}

As shown in order we have three `include` artefacts.

* `include head.pug`,
* `include sidebar.pug`,
* `include navbar.pug`,

One variable named `title`,

{{< highlight haskell >}}
- var title = "Link Example: Default"
{{< / highlight >}}

And one block named `content`.

{{< highlight haskell >}}
block content
  p Default Page
{{< / highlight >}}

![Jade/Pug: ViM NERDTree Panes][02-pug-nerdtree]

#### Head

It is a simple `pug` translation.

* [html-preprocessor/.../views/partials/head.pug][02-views-partials-head]

{{< highlight haskell >}}
meta(charset='utf-8')
block title
  title= title
meta(name='viewport', content="width=device-width, initial-scale=1")
link(href='https://www.w3schools.com/w3css/4/w3.css', rel='stylesheet', type='text/css')
script(src='../js/script.js')
script(src='//localhost:35729/livereload.js')
{{< / highlight >}}

Here we put the `title` variable inside a block named `title`.

{{< highlight haskell >}}
block title
  title= title
{{< / highlight >}}

I also add `livereload` for use with `grunt` later.

#### Sidebar

It is simply a `html` tag in `pug`. Nothing special.

* [html-preprocessor/.../views/partials/sidebar.pug][02-views-partials-sidebar]

{{< highlight haskell >}}
#mySidebar.w3-sidebar.w3-bar-block.w3-card.w3-animate-left(style='display:none')
  button#closeNav.w3-bar-item.w3-button.w3-large 
    | Close &times;
  a.w3-bar-item.w3-button(href='index.html')
    | Home
  a.w3-bar-item.w3-button(href='html.html')
    | HTML Link
  a.w3-bar-item.w3-button(href='css.html')
    | CSS Link
  a.w3-bar-item.w3-button(href='php.html')
    | PHP Link
  a.w3-bar-item.w3-button(href='javascript.html')
    | Javascript Link
{{< / highlight >}}

#### Navbar

It is also simply a `html` tag in `pug`. Nothing special either.

* [html-preprocessor/.../views/partials/navbar.pug][02-views-partials-navbar]

{{< highlight haskell >}}
.w3-teal
  button#openNav.w3-button.w3-teal.w3-xlarge
    | &#9776;
  .w3-container
    h1 My Page
.w3-bar.w3-black
  a.w3-bar-item.w3-button.w3-large(href='index.html')
    | Home
  a.w3-bar-item.w3-button.w3-large(href='html.html')
    | HTML
  a.w3-bar-item.w3-button.w3-large(href='css.html')
    | CSS
  a.w3-bar-item.w3-button.w3-large(href='php.html')
    | PHP
  a.w3-bar-item.w3-button.w3-large(href='javascript.html')
    | Javascript
{{< / highlight >}}

#### Index

By extending the `partials/layout.pug`,
we can alter the content of each block: `title` and `content`.

* [html-preprocessor/.../views/index.pug][02-views-index]

{{< highlight haskell >}}
extends partials/layout.pug

block title
  title Link Example: Home

block content
  p Home Page
{{< / highlight >}}

-- -- --

### 2: Pug Content Propagation

> How does it works ?

#### Block and Inheritance

In `partials/layout.pug` we have this block.

{{< highlight haskell >}}
    #main
      include navbar.pug
      .w3-container
        block content
          p Default Page
{{< / highlight >}}

Then we can replace the content,
of the block in child layout `index.pug`.

{{< highlight haskell >}}
extends partials/layout.pug

block content
  p Home Page
{{< / highlight >}}

#### Title: Using Variable

> Variable Propagation Between Template

In `partials/layout.pug` we have this variable.

{{< highlight haskell >}}
- var title = "Link Example: Default"
{{< / highlight >}}

This should work perfectly with include `partials/head.pug`

{{< highlight haskell >}}
  title= title
{{< / highlight >}}

But unfortunately, `pug` cannot have variable,
in top level of extended files.
This means we cannot alter the default variable.

#### Title: Using Block

The solution is to wrapped the content in `block` instead.

As in `index.pug` below:

{{< highlight haskell >}}
block title
  title Link Example: Home
{{< / highlight >}}

The `partials/head.pug` would be

{{< highlight haskell >}}
block title
  title Link Example: Default
{{< / highlight >}}

Or if you wish to use default variable.

{{< highlight haskell >}}
block title
  title =title
{{< / highlight >}}

Where the `title` variable comes from `partials/layout.pug`.

#### Main Files

This time, all the main files can be so short as below:

* [html-preprocessor/.../views/css.pug][02-views-css]

{{< highlight haskell >}}
extends partials/layout.pug

block title
  title Link Example: CSS

block content
  p CSS Page
{{< / highlight >}}

* [html-preprocessor/.../views/php.pug][02-views-php]

{{< highlight haskell >}}
extends partials/layout.pug

block title
  title Link Example: PHP

block content
  p PHP Page
{{< / highlight >}}

#### Result

Finally we can check if the result is right in `build/index.html`.

* [html-preprocessor/.../build/index.html][02-build-index]

{{< highlight html >}}
<!DOCTYPE html>
<html lang="en"></html>
<head>
  <meta charset="utf-8">
  <title>Link Example: Home</title>
  ...
</head>
<body>
  ...
  <div id="main">
    ...
    <div class="w3-container">
      <p>Home Page</p>
    </div>
  </div>
</body>
{{< / highlight >}}

-- -- --

### 3: Grunt Configuration

#### Command Line Interface

Without `grunt`, you can run `pug` in command line interface.

{{< highlight bash >}}
$ cd views
$ pug -w -P -p . -o ../build *.pug
{{< / highlight >}}

* .

Now consider discuss this `grunt` stuff.

#### package.json

All you need to know is the `devDependencies`.

* [gitlab.com/.../package.json][02-package-json].

{{< highlight javascript >}}
  "devDependencies": {
    "grunt": "^1.0.1",
    "grunt-contrib-pug": "^2.0.0",
    "grunt-contrib-watch": "^1.1.0",
    "pug": "^2.0.4"
  }
{{< / highlight >}}

To begin with `grunt`, you need to run `npm install` first.

#### Gruntfile.js

You also need to a configuration file named `Gruntfile.js`.

* [gitlab.com/.../Gruntfile.js][02-gruntfile-js]

{{< highlight javascript >}}
module.exports = function(grunt) {
  // configure the tasks
  let config = {

    pug: {
      compile: {
        options: {
          data: {
            debug: false
          },
          pretty: true
        },
        files: [ {
          cwd: "views",
          src: "*.pug",
          dest: "build",
          expand: true,
          ext: ".html"
        } ]
      }
    },

    watch: {
      pug: {
        files: ['views/**/*'],
        tasks: ['pug'],
        options: {
          livereload: true,
          interrupt: false,
          spawn: false
        }
      }
    }

  };

  grunt.initConfig(config);

  // load the tasks
  grunt.loadNpmTasks('grunt-contrib-pug');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // define the tasks
  grunt.registerTask('default', [
    'pug', 'watch'
  ] );

};
{{< / highlight >}}

The configuration is self explanatory.
Just read the official documentation of `grunt-contrib-pug`.

#### Running Grunt

![Pug: Grunt: Watch][02-pug-grunt-watch]

Now you can build your own static `html` files using `pug`.

-- -- --

### What's Next?

We still have three `grunt` example: `nunjucks`, `EJS` and `handlebars`.

Consider continue reading [ [Template - Grunt - Nunjucks][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}frontend/2020/05/06/template-grunt-nunjucks/
[local-pug]:                https://epsi-rns.gitlab.io/frontend/2020/04/07/tools-html-pug/
[local-grunt]:              https://epsi-rns.gitlab.io/frontend/2020/04/11/tools-task-runner-grunt/

[02-pug-tree-example]:      {{< assets-frontend >}}/2020/05/02-pug-tree-example.png
[02-pug-grunt-watch]:       {{< assets-frontend >}}/2020/05/02-pug-grunt-watch.png
[02-pug-nerdtree]:          {{< assets-frontend >}}/2020/05/02-pug-nerdtree.png

[source-example]:           {{< tutor-css-tools >}}/html-preprocessor/02-grunt/pug/
[02-package-json]:          {{< tutor-css-tools >}}/html-preprocessor/02-grunt/pug/package.json
[02-gruntfile-js]:          {{< tutor-css-tools >}}/html-preprocessor/02-grunt/pug/Gruntfile.js
[02-build-index]:           {{< tutor-css-tools >}}/html-preprocessor/02-grunt/pug/build/index.html
[02-views-css]:             {{< tutor-css-tools >}}/html-preprocessor/02-grunt/pug/views/css.pug
[02-views-html]:            {{< tutor-css-tools >}}/html-preprocessor/02-grunt/pug/views/html.pug
[02-views-index]:           {{< tutor-css-tools >}}/html-preprocessor/02-grunt/pug/views/index.pug
[02-views-javascript]:      {{< tutor-css-tools >}}/html-preprocessor/02-grunt/pug/views/javascript.pug
[02-views-php]:             {{< tutor-css-tools >}}/html-preprocessor/02-grunt/pug/views/php.pug
[02-views-partials-layout]: {{< tutor-css-tools >}}/html-preprocessor/02-grunt/pug/views/partials/layout.pug
[02-views-partials-head]:   {{< tutor-css-tools >}}/html-preprocessor/02-grunt/pug/views/partials/head.pug
[02-views-partials-sidebar]:{{< tutor-css-tools >}}/html-preprocessor/02-grunt/pug/views/partials/sidebar.pug
[02-views-partials-navbar]: {{< tutor-css-tools >}}/html-preprocessor/02-grunt/pug/views/partials/navbar.pug
