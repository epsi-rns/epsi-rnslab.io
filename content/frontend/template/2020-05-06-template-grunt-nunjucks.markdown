---
type   : post
title  : "Template - Grunt - Nunjucks"
date   : 2020-05-06T09:17:35+07:00
slug   : template-grunt-nunjucks
categories: [frontend]
tags      : [node, grunt, nunjucks]
keywords  : [template]
author : epsi
opengraph:
  image: assets-frontend/2020/05/01-html-hyperlink.png

toc    : "toc-2020-05-template"

excerpt:
  Separate HTML parts using Nunjucks.
  Build a Static Files using Grunt.

---

### Preface

> Goal: Build static HTML files using partial Nunjucks utilizing Grunt.

Nunjucks is my favorite templating engine. It is powerful.
Its syntax is simply a html document,
so you can just copy-paste, and change a bit of your code.
But I can't show you the full power here in this article.
This article will only provide a simple example for beginner.

#### Official Documentation

* [mozilla.github.io/nunjucks](https://mozilla.github.io/nunjucks/)

* [gruntjs.com](https://gruntjs.com/)

#### Source Examples

You can obtain source examples here:

* [html-preprocessor/02-grunt/nunjucks/][source-example]

-- -- --

### 1: Nunjucks Case

We use `nunjucks` as `html preprocessor` to build `html` documents.
Consider transform our last **HTML Case** into **Nunjucks Case**.

#### Directory Structure

Consider `nunjucks` structure.

{{< highlight bash >}}
└── views
    ├── css.njk
    ├── html.njk
    ├── index.njk
    ├── javascript.njk
    ├── partials
    │   ├── head.njk
    │   ├── layout.njk
    │   ├── navbar.njk
    │   └── sidebar.njk
    └── php.njk
{{< / highlight >}}

We want the `nunjucks` to build `html` artefacts as below:

{{< highlight bash >}}
└── build
    ├── css.html
    ├── html.html
    ├── index.html
    ├── javascript.html
    └── php.html
{{< / highlight >}}

![Nunjucks: Example: Tree][02-nunjucks-tree-example]

Now we can separate each html parts.

#### Layout

* [html-preprocessor/.../views/partials/layout.njk][02-views-partials-layout]

{{< highlight jinja >}}
<!DOCTYPE html>
<html lang="en">

<head>
  {% include "partials/head.njk" %}
</head>
<body>
  {% include "partials/sidebar.njk" %}

  <div id="main">
    {% include "partials/navbar.njk" %}

    <div class="w3-container">
    {% block main %}
      <p>Default Page</p>
    {% endblock %}
    </div>
  </div>

</body>
</html>
{{< / highlight >}}

As shown in order we have three `include` artefacts.

* `include head.njk`,
* `include sidebar.njk`,
* `include navbar.njk`,

And one block named `main`.

{{< highlight jinja >}}
{% block main %}
  <p>Default Page</p>
{% endblock %}
{{< / highlight >}}

![Nunjucks: ViM NERDTree Panes][02-nunjucks-nerdtree]

#### Head

It is almost the same with the original head.

* [html-preprocessor/.../views/partials/head.njk][02-views-partials-head]

{{< highlight html >}}

  <title>{{ title or "Link Example: Default" }}</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  <script src="../js/script.js"></script>
  <script src="//localhost:35729/livereload.js"></script>
{{< / highlight >}}

Here we put the default title if we cannot find `title` variable.

{{< highlight jinja >}}
<title>{{ title or "Link Example: Default" }}</title>
{{< / highlight >}}

I also add `livereload` for use with `grunt` later.

#### Sidebar

It is a simply a verbatim copy-paste from the original `html`.

* [html-preprocessor/.../views/partials/sidebar.njk][02-views-partials-sidebar]

{{< highlight html >}}

  <div class="w3-sidebar w3-bar-block w3-card w3-animate-left" 
       style="display:none" id="mySidebar">
    <button class="w3-bar-item w3-button w3-large"
       id="closeNav" >Close &times;</button>
    <a class="w3-bar-item w3-button"
       href="index.html">Home</a>
    <a class="w3-bar-item w3-button"
       href="html.html">HTML Link</a>
    <a class="w3-bar-item w3-button"
       href="css.html">CSS Link</a>
    <a class="w3-bar-item w3-button"
       href="php.html">PHP Link</a>
    <a class="w3-bar-item w3-button"
       href="javascript.html">Javascript Link</a>
  </div>
{{< / highlight >}}

#### Navbar

It is also a an exact copy from the original `html`.

* [html-preprocessor/.../views/partials/navbar.njk][02-views-partials-navbar]

{{< highlight html >}}

    <div class="w3-teal">
      <button class="w3-button w3-teal w3-xlarge"
              id="openNav">&#9776;</button>
      <div class="w3-container">
        <h1>My Page</h1>
      </div>
    </div>

    <div class="w3-bar w3-black">
      <a class="w3-bar-item w3-button"
         href="index.html">Home</a>
      <a class="w3-bar-item w3-button"
         href="html.html">HTML</a>
      <a class="w3-bar-item w3-button"
         href="css.html" >CSS</a>
      <a class="w3-bar-item w3-button"
         href="php.html" >PHP</a>
      <a class="w3-bar-item w3-button"
         href="javascript.html">Javascript</a>
    </div>
{{< / highlight >}}

#### Index

By extending the `partials/layout.njk`,
we can alter the content of `main` block,
and also setup the `title` variable.

* [html-preprocessor/.../views/index.njk][02-views-index]

{{< highlight jinja >}}
{% extends "partials/layout.njk" %}

{% set title = "Link Example: Home" %}

{% block main %}
  <p>Home Page</p>
{% endblock %}
{{< / highlight >}}

-- -- --

### 2: Nunjucks Content Propagation

> How does it works ?

#### Block and Inheritance

In `partials/layout.njk` we have this block.

{{< highlight jinja >}}
{% block main %}
  <p>Default Page</p>
{% endblock %}
{{< / highlight >}}

Then we can replace the content,
of the block in child layout `index.njk`.

{{< highlight jinja >}}
{% block main %}
  <p>Home Page</p>
{% endblock %}
{{< / highlight >}}

#### Title

> Variable Propagation Between Template

In `index.njk` we have this variable.

{{< highlight jinja >}}
{% set title = "Link Example: Home" %}
{{< / highlight >}}

This variable will be utilized in `partials/head.njk`

{{< highlight jinja >}}
<title>{{ title or "Link Example: Default" }}</title>
{{< / highlight >}}

This is almost self explanatory.

#### Main Files

Instantly, all the main files can be so short as below:

* [html-preprocessor/.../views/css.njk][02-views-css]

{{< highlight jinja >}}
{% extends "partials/layout.njk" %}

{% set title = "Link Example: CSS" %}

{% block main %}
  <p>CSS Page</p>
{% endblock %}
{{< / highlight >}}

* [html-preprocessor/.../views/php.njk][02-views-php]

{{< highlight jinja >}}
{% extends "partials/layout.njk" %}

{% set title = "Link Example: PHP" %}

{% block main %}
  <p>PHP Page</p>
{% endblock %}
{{< / highlight >}}

#### Result

Conclusively, we can check the result in `build/index.html`,
against the original `html`.

* [html-preprocessor/.../build/index.html][02-build-index]

{{< highlight html >}}
<!DOCTYPE html>
<html lang="en">

<head>
  
  <title>Link Example: Home</title>
  ...
</head>
<body>
  ...

  <div id="main">
    ...
    <div class="w3-container">
      <p>Home Page</p>
    </div>
  </div>

</body>
</html>

{{< / highlight >}}

-- -- --

### 3: Grunt Configuration

#### Command Line Interface

I cannot figure out how to use `nunjucks-cli` properly.
Luckily, we have this good `grunt`, as replacement.

#### package.json

All you need to know is the `devDependencies`.

* [gitlab.com/.../package.json][02-package-json].

{{< highlight javascript >}}
  "devDependencies": {
    "grunt": "^1.0.1",
    "grunt-contrib-watch": "^1.1.0",
    "grunt-nunjucks-2-html": "^2.2.0",
    "nunjucks": "^3.2.1"
  }
{{< / highlight >}}

To begin with `grunt`, you need to run `npm install` first.

#### Gruntfile.js

You also need to a configuration file named `Gruntfile.js`.

* [gitlab.com/.../Gruntfile.js][02-gruntfile-js]

{{< highlight javascript >}}
module.exports = function(grunt) {
  // configure the tasks
  let config = {

    nunjucks: {
      options: {
        data:  grunt.file.readJSON('data.json'),
        paths: 'views'
      },
      render: {
        files: [ {
          cwd: "views",
          src: "*.njk",
          dest: "build",
          expand: true,
          ext: ".html"
        } ]
      }
    },

    watch: {
      nunjucks: {
        files: ['views/**/*'],
        tasks: ['nunjucks'],
        options: {
          livereload: true,
          interrupt: false,
          spawn: false
        }
      }
    }

  };

  grunt.initConfig(config);

  // load the tasks
  grunt.loadNpmTasks('grunt-nunjucks-2-html');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // define the tasks
  grunt.registerTask('default', [
    'nunjucks', 'watch'
  ] );

};
{{< / highlight >}}

The configuration is self explanatory.
Just read the official documentation of `grunt-nunjucks-2-html`.

#### Data

We can skip data in `grunt`, and your build will be working just fine.
But just in case you need to surpress warning message,
you can put empty `data.json` as below:

{{< highlight javascript >}}
{
}
{{< / highlight >}}

#### Running Grunt

![Nunjucks: Grunt: Watch][02-nunjucks-grunt-watch]

Now you can build your own static `html` files using `nunjucks`.

-- -- --

### What's Next?

We still have two `grunt` example: EJS` and `handlebars`.

Consider continue reading [ [Template - Grunt - EJS][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}frontend/2020/05/07/template-grunt-ejs/

[02-nunjucks-tree-example]: {{< assets-frontend >}}/2020/05/02-nunjucks-tree-example.png
[02-nunjucks-grunt-watch]:  {{< assets-frontend >}}/2020/05/02-nunjucks-grunt-watch.png
[02-nunjucks-nerdtree]:     {{< assets-frontend >}}/2020/05/02-nunjucks-nerdtree.png

[source-example]:           {{< tutor-css-tools >}}/html-preprocessor/02-grunt/nunjucks/
[02-package-json]:          {{< tutor-css-tools >}}/html-preprocessor/02-grunt/nunjucks/package.json
[02-gruntfile-js]:          {{< tutor-css-tools >}}/html-preprocessor/02-grunt/nunjucks/Gruntfile.js
[02-build-index]:           {{< tutor-css-tools >}}/html-preprocessor/02-grunt/nunjucks/build/index.html
[02-views-css]:             {{< tutor-css-tools >}}/html-preprocessor/02-grunt/nunjucks/views/css.njk
[02-views-html]:            {{< tutor-css-tools >}}/html-preprocessor/02-grunt/nunjucks/views/html.njk
[02-views-index]:           {{< tutor-css-tools >}}/html-preprocessor/02-grunt/nunjucks/views/index.njk
[02-views-javascript]:      {{< tutor-css-tools >}}/html-preprocessor/02-grunt/nunjucks/views/javascript.njk
[02-views-php]:             {{< tutor-css-tools >}}/html-preprocessor/02-grunt/nunjucks/views/php.njk
[02-views-partials-layout]: {{< tutor-css-tools >}}/html-preprocessor/02-grunt/nunjucks/views/partials/layout.njk
[02-views-partials-head]:   {{< tutor-css-tools >}}/html-preprocessor/02-grunt/nunjucks/views/partials/head.njk
[02-views-partials-sidebar]:{{< tutor-css-tools >}}/html-preprocessor/02-grunt/nunjucks/views/partials/sidebar.njk
[02-views-partials-navbar]: {{< tutor-css-tools >}}/html-preprocessor/02-grunt/nunjucks/views/partials/navbar.njk


