---
type   : post
title  : "Template - Grunt - EJS"
date   : 2020-05-07T09:17:35+07:00
slug   : template-grunt-ejs
categories: [frontend]
tags      : [node, grunt, ejs]
keywords  : [template]
author : epsi
opengraph:
  image: assets-frontend/2020/05/01-html-hyperlink.png

toc    : "toc-2020-05-template"

excerpt:
  Separate HTML parts using EJS.
  Build a Static Files using Grunt.

---

### Preface

> Goal: Build static HTML files using partial EJS utilizing Grunt.

With EJS (embedded javascript),
you can directly use javascript inside your template.
It has no `extend` and `block` concept.
But you can have similar result with parameterized include.

#### Official Documentation

* [ejs.co](https://ejs.co/)

* [gruntjs.com](https://gruntjs.com/)

#### Source Examples

You can obtain source examples here:

* [html-preprocessor/02-grunt/ejs/][source-example]

-- -- --

### 1: EJS Case

We use `EJS` as `html preprocessor` to build `html` documents.
Consider transform our last **HTML Case** into **EJS Case**.

#### Directory Structure

Consider `ejs` structure.

{{< highlight bash >}}
└── views
    ├── css.ejs
    ├── html.ejs
    ├── index.ejs
    ├── javascript.ejs
    ├── partials
    │   ├── head.ejs
    │   ├── layout.ejs
    │   ├── navbar.ejs
    │   └── sidebar.ejs
    └── php.ejs
{{< / highlight >}}

We want the `ejs` to build `html` artefacts as below:

{{< highlight bash >}}
└── build
    ├── css.html
    ├── html.html
    ├── index.html
    ├── javascript.html
    └── php.html
{{< / highlight >}}

![EJS: Example: Tree][02-ejs-tree-example]

Now we can separate each html parts.

#### Layout

* [html-preprocessor/.../views/partials/layout.ejs][02-views-partials-layout]

{{< highlight javascript >}}
<!DOCTYPE html>
<html lang="en">

<head>
  <%- include('head',{title: title}) %>
</head>
<body>
  <%- include('sidebar') %>

  <div id="main">
    <%- include('navbar') %>

    <div class="w3-container">
      <p><%= content %></p>
    </div>
  </div>

</body>
</html>
{{< / highlight >}}

As shown in order we have three `include` artefacts.

* `include head.ejs`,
* `include sidebar.ejs`,
* `include navbar.ejs`,

And one output variable `<%=` named `content`.

{{< highlight javascript >}}
<p><%= content %></p>
{{< / highlight >}}

This output value,
will simply print variable,
in `HTML escape` fashioned.

![EJS: ViM NERDTree Panes][02-ejs-nerdtree]

#### Head

It is almost the same with the original head.

* [html-preprocessor/.../views/partials/head.ejs][02-views-partials-head]

{{< highlight html >}}

  <title><%= title || "Link Example: Default" %></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  <script src="../js/script.js"></script>
  <script src="//localhost:35729/livereload.js"></script>
{{< / highlight >}}

Here we put the default title if we cannot find `title` variable.

{{< highlight html >}}
<title><%= title || "Link Example: Default" %></title>
{{< / highlight >}}

I also add `livereload` for use with `grunt` later.

#### Sidebar

It is a simply a verbatim copy-paste from the original `html`.

* [html-preprocessor/.../views/partials/sidebar.ejs][02-views-partials-sidebar]

{{< highlight html >}}

  <div class="w3-sidebar w3-bar-block w3-card w3-animate-left" 
       style="display:none" id="mySidebar">
    <button class="w3-bar-item w3-button w3-large"
       id="closeNav" >Close &times;</button>
    <a class="w3-bar-item w3-button"
       href="index.html">Home</a>
    <a class="w3-bar-item w3-button"
       href="html.html">HTML Link</a>
    <a class="w3-bar-item w3-button"
       href="css.html">CSS Link</a>
    <a class="w3-bar-item w3-button"
       href="php.html">PHP Link</a>
    <a class="w3-bar-item w3-button"
       href="javascript.html">Javascript Link</a>
  </div>
{{< / highlight >}}

#### Navbar

It is also a an exact copy from the original `html`.

* [html-preprocessor/.../views/partials/navbar.ejs][02-views-partials-navbar]

{{< highlight html >}}

    <div class="w3-teal">
      <button class="w3-button w3-teal w3-xlarge"
              id="openNav">&#9776;</button>
      <div class="w3-container">
        <h1>My Page</h1>
      </div>
    </div>

    <div class="w3-bar w3-black">
      <a class="w3-bar-item w3-button"
         href="index.html">Home</a>
      <a class="w3-bar-item w3-button"
         href="html.html">HTML</a>
      <a class="w3-bar-item w3-button"
         href="css.html" >CSS</a>
      <a class="w3-bar-item w3-button"
         href="php.html" >PHP</a>
      <a class="w3-bar-item w3-button"
         href="javascript.html">Javascript</a>
    </div>
{{< / highlight >}}

#### Index

By including the `partials/layout.ejs`,
we can pass both the variable `title` and `content`.

* [html-preprocessor/.../views/index.ejs][02-views-index]

{{< highlight javascript >}}
<% 
  var title = "Link Example: Home";
  var content = "Home Page";
%>
<%-
  include('partials/layout',
  {title: title, content: content})
%>
{{< / highlight >}}

-- -- --

### 2: EJS Content Propagation

> How does it works ?

#### Passing Value

In `partials/layout.ejs` we have this output variable `<%=`.

{{< highlight javascript >}}
<p><%= content %></p>
{{< / highlight >}}

Then we can replace the content,
of the block in child layout `index.ejs`.

{{< highlight javascript >}}
<%  var content = "Home Page"; %>
<%- include('partials/layout', {content: content}) %>
{{< / highlight >}}

#### Title

> Variable Propagation Between Template

In `index.ejs` we have this variable.

{{< highlight javascript >}}
<%  var title = "Link Example: Home"; %>
<%- include('partials/layout', {title: title}) %>
{{< / highlight >}}

And in `partials/layout.ejs`, we pass the variable again.

{{< highlight javascript >}}
<head>
  <%- include('head',{title: title}) %>
</head>
{{< / highlight >}}

This variable will be utilized in `partials/head.ejs`

{{< highlight javascript >}}
<title><%= title || "Link Example: Default" %></title>
{{< / highlight >}}

This is also almost self explanatory.

#### Main Files

Instantly, all the main files can be so short as below:

* [html-preprocessor/.../views/css.ejs][02-views-css]

{{< highlight javascript >}}
<% 
  var title = "Link Example: CSS";
  var content = "CSS Page";
%>
<%-
  include('partials/layout',
  {title: title, content: content})
%>
{{< / highlight >}}

* [html-preprocessor/.../views/php.ejs][02-views-php]

{{< highlight javascript >}}
<% 
  var title = "Link Example: PHP";
  var content = "PHP Page";
%>
<%-
  include('partials/layout',
  {title: title, content: content})
%>
{{< / highlight >}}

#### Result

As usual, check `build/index.html` against the original `html`.

* [html-preprocessor/.../build/index.html][02-build-index]

{{< highlight html >}}
<!DOCTYPE html>
<html lang="en">

<head>
  
  <title>Link Example: Home</title>
  ...
</head>
<body>
  ...

  <div id="main">
    ...
    <div class="w3-container">
      <p>Home Page</p>
    </div>
  </div>

</body>
</html>
{{< / highlight >}}

-- -- --

### 3: Grunt Configuration

#### Command Line Interface

I won't bother looking for a CLI guidance.
I already comfortable with this good `grunt`.

#### package.json

All you need to know is the `devDependencies`.

* [gitlab.com/.../package.json][02-package-json].

{{< highlight javascript >}}
  "devDependencies": {
    "ejs": "^3.1.3",
    "grunt": "^1.0.1",
    "grunt-contrib-watch": "^1.1.0",
    "grunt-ejs": "^1.0.0"
  }
{{< / highlight >}}

To begin with `grunt`, you need to run `npm install` first.

#### Gruntfile.js

You also need to a configuration file named `Gruntfile.js`.

* [gitlab.com/.../Gruntfile.js][02-gruntfile-js]

{{< highlight javascript >}}
module.exports = function(grunt) {
  // configure the tasks
  let config = {

    ejs: {
      all: {
        cwd: 'views',
        src: ['**/*.ejs', '!partials/**/*'],
        dest: 'build/',
        expand: true,
        ext: '.html'
      }
    },

    watch: {
      ejs: {
        files: ['views/**/*'],
        tasks: ['ejs'],
        options: {
          livereload: true,
          interrupt: false,
          spawn: false
        }
      }
    }

  };

  grunt.initConfig(config);

  // load the tasks
  grunt.loadNpmTasks('grunt-ejs');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // define the tasks
  grunt.registerTask('default', [
    'ejs', 'watch'
  ] );

};
{{< / highlight >}}

The configuration is self explanatory.
Just read the official documentation of `grunt-ejs`.

#### Running Grunt

![EJS: Grunt: Watch][02-ejs-grunt-watch]

Now you can build your own static `html` files using `ejs`.

-- -- --

### What's Next?

We still have the last `grunt` example: `handlebars`.

Consider continue reading [ [Template - Grunt - Handlebars][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}frontend/2020/05/08/template-grunt-handlebars/

[02-ejs-tree-example]:      {{< assets-frontend >}}/2020/05/02-ejs-tree-example.png
[02-ejs-grunt-watch]:       {{< assets-frontend >}}/2020/05/02-ejs-grunt-watch.png
[02-ejs-nerdtree]:          {{< assets-frontend >}}/2020/05/02-ejs-nerdtree.png

[source-example]:           {{< tutor-css-tools >}}/html-preprocessor/02-grunt/ejs/
[02-package-json]:          {{< tutor-css-tools >}}/html-preprocessor/02-grunt/ejs/package.json
[02-gruntfile-js]:          {{< tutor-css-tools >}}/html-preprocessor/02-grunt/ejs/Gruntfile.js
[02-build-index]:           {{< tutor-css-tools >}}/html-preprocessor/02-grunt/ejs/build/index.html
[02-views-css]:             {{< tutor-css-tools >}}/html-preprocessor/02-grunt/ejs/views/css.ejs
[02-views-html]:            {{< tutor-css-tools >}}/html-preprocessor/02-grunt/ejs/views/html.ejs
[02-views-index]:           {{< tutor-css-tools >}}/html-preprocessor/02-grunt/ejs/views/index.ejs
[02-views-javascript]:      {{< tutor-css-tools >}}/html-preprocessor/02-grunt/ejs/views/javascript.ejs
[02-views-php]:             {{< tutor-css-tools >}}/html-preprocessor/02-grunt/ejs/views/php.ejs
[02-views-partials-layout]: {{< tutor-css-tools >}}/html-preprocessor/02-grunt/ejs/views/partials/layout.ejs
[02-views-partials-head]:   {{< tutor-css-tools >}}/html-preprocessor/02-grunt/ejs/views/partials/head.ejs
[02-views-partials-sidebar]:{{< tutor-css-tools >}}/html-preprocessor/02-grunt/ejs/views/partials/sidebar.ejs
[02-views-partials-navbar]: {{< tutor-css-tools >}}/html-preprocessor/02-grunt/ejs/views/partials/navbar.ejs


