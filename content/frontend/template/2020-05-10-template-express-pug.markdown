---
type   : post
title  : "Template - Express - Pug"
date   : 2020-05-10T09:17:35+07:00
slug   : template-express-pug
categories: [frontend]
tags      : [node, pug, express]
keywords  : [template]
author : epsi
opengraph:
  image: assets-frontend/2020/05/01-html-hyperlink.png

toc    : "toc-2020-05-template"

excerpt:
  Feeding Data in ExpressJS, and Process in a Loop with Pug.

---

### Preface

> Goal: Feeding Data in ExpressJS, and Process in a Loop with Pug.

How about making online site in localhost by request,
instead of build local files for every changes?
Yes, `express` is the right tools for beginner.
The configuration is so simple,
and you can have your site is ready to served in localhost.

Consider start with `pug` example.

#### Official Documentation

* [pugjs.org](https://pugjs.org/)

* [expressjs.com](https://expressjs.com/)

#### Source Examples

You can obtain source examples here:

* [html-preprocessor/03-express/pug/][source-example]

-- -- --

### Pug Case

The `pug` case is the same,
with the one utilizing `grunt` in previous article
with a few differences.

* `express` configuration

* Using data to provide hyperlink in both `navbar` and `sidebar`.

#### Directory Structure

Consider an `express` structure with `pug` views.

{{< highlight bash >}}
❯ tree
.
├── app.js
├── package.json
├── public
│   ├── css
│   │   └── w3.css
│   ├── favicon.ico
│   └── js
│       └── script.js
└── views
    ├── css.pug
    ├── html.pug
    ├── index.pug
    ├── javascript.pug
    ├── partials
    │   ├── head.pug
    │   ├── layout.pug
    │   ├── navbar.pug
    │   └── sidebar.pug
    └── php.pug

5 directories, 14 files
{{< / highlight >}}

![Pug: Example: Tree][03-pug-tree-example]

The `views` structure is similar to previous case with `grunt`.
I suggest that you have fast reading for this article first.

* [Template - Grunt - Pug][local-grunt-case]

#### package.json

All you need to know is the `devDependencies`.

* [gitlab.com/.../package.json][03-package-json].

{{< highlight javascript >}}
  "devDependencies": {
    "express": "^4.17.1",
    "pug": "^2.0.4"
  }
{{< / highlight >}}

To begin with `expressjs`, you need to run `npm install` first.

#### app.js

You also need to a write a simple application in `app.js`.

* [gitlab.com/.../app.js][03-app-js]

{{< highlight javascript >}}
// Basic Stuff
var express = require('express');
var path = require('path');
var app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.use(express.static(path.join(__dirname, 'public')));
app.locals.pretty = true;

// Example Pages
var pages = [
  { link: '/',     short: 'Home', long: 'Home'  },
  { link: '/html', short: 'HTML', long: 'HTML Link' },
  { link: '/css',  short: 'CSS',  long: 'CSS Link' },
  { link: '/php',  short: 'PHP',  long: 'PHP Link' },
  { link: '/javascript', short: 'Javascript', long: 'Javascript Link' }
];

// Router
app.get('/', function(req, res) {
  res.render('index', { pages: pages });
  console.log("choice page is index");
});

const choices = ['index', 'html', 'css', 'php', 'javascript'];
app.get('/:page', function(req, res) {
  page = req.params.page;
  
  if (choices.includes(page)) {
    res.render(page, { pages: pages });
    console.log("choice page is " +page);
  } else {
    res.send('404: Page not Found', 404);
  }
});

// Run, Baby Run
app.listen(3000);
console.log('Express started on port 3000');
{{< / highlight >}}

The application is short enough to be self explanatory.

#### Pug Template Specific Code

The only line we need to manage `pug` is this one below:

{{< highlight javascript >}}
app.set('view engine', 'pug');
{{< / highlight >}}

#### Hyperlink Data

> The same for every templates

To provide hyperlink in both `navbar` and `sidebar`,
we need to setup the data in `app.js` as below:

{{< highlight javascript >}}
var pages = [
  { link: '/',     short: 'Home', long: 'Home'  },
  { link: '/html', short: 'HTML', long: 'HTML Link' },
  { link: '/css',  short: 'CSS',  long: 'CSS Link' },
  { link: '/php',  short: 'PHP',  long: 'PHP Link' },
  { link: '/javascript', short: 'Javascript', long: 'Javascript Link' }
];
{{< / highlight >}}

We will feed the data for each request.

{{< highlight javascript >}}
app.get('/', function(req, res) {
  res.render('index', { pages: pages });
  console.log("choice page is index");
});
{{< / highlight >}}

The `sidebar` will use `long` text,
and the `navbar` will use `short` one.

#### Router

> The same for every templates

For simplicity, I put all the routes in `app.js`.

{{< highlight javascript >}}
app.get('/', function(req, res) {
  res.render('index', { pages: pages });
  console.log("choice page is index");
});

const choices = ['index', 'html', 'css', 'php', 'javascript'];
app.get('/:page', function(req, res) {
  page = req.params.page;
  
  if (choices.includes(page)) {
    res.render(page, { pages: pages });
    console.log("choice page is " +page);
  } else {
    res.send('404: Page not Found', 404);
  }
});
{{< / highlight >}}

If the request does not contain any of the above page,
`express` will send `404` error response.

#### Loop in Template

This time, consider alter a bit,
both the `sidebar` and the `navbar`,
so we can render each data in loop.

* [html-preprocessor/.../views/partials/sidebar.pug][03-views-partials-sidebar]

{{< highlight haskell >}}
#mySidebar.w3-sidebar.w3-bar-block.w3-card.w3-animate-left(style='display:none')
  button#closeNav.w3-bar-item.w3-button.w3-large 
    | Close &times;
  each page in pages
    a.w3-bar-item.w3-button(href=page.link)
      | #{page.long}
{{< / highlight >}}

* [html-preprocessor/.../views/partials/navbar.pug][03-views-partials-navbar]

{{< highlight haskell >}}
.w3-teal
  button#openNav.w3-button.w3-teal.w3-xlarge
    | &#9776;
  .w3-container
    h1 My Page
.w3-bar.w3-black
  each page in pages
    a.w3-bar-item.w3-button(href=page.link)
      | #{page.short}
{{< / highlight >}}

All you need to know is these lines:

{{< highlight haskell >}}
  each page in pages
    a.w3-bar-item.w3-button(href=page.link)
      | #{page.short}
{{< / highlight >}}

![Express: Loop in Pug][03-pug-panes]

This will render a hyperlink,
with result about the same with previous example.

#### Running Express

Running express is as easy as this command below.

{{< highlight bash >}}
$ node app.js
{{< / highlight >}}

![HTML: Hyperlink Example][01-html-hyperlink]

Click any hyperlink on the example case,
and you will get the response in `console.log` immediately.

![Express: node app.js][03-express-app]

You are ready to run a website,
with `expressjs` backend, using `pug`.

-- -- --

### What's Next?

We still have three `express` example: `nunjucks`, `EJS` and `handlebars`.
Consider continue reading [ [Template - Express - Nunjucks][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}frontend/2020/05/11/template-express-nunjucks/
[local-grunt-case]:         {{< baseurl >}}frontend/2020/05/05/template-grunt-pug/

[01-html-hyperlink]:        {{< assets-frontend >}}/2020/05/01-html-hyperlink.png
[03-pug-tree-example]:      {{< assets-frontend >}}/2020/05/03-pug-tree-example.png
[03-express-app]:           {{< assets-frontend >}}/2020/05/03-express-app.png
[03-pug-panes]:             {{< assets-frontend >}}/2020/05/03-pug-panes.png

[source-example]:           {{< tutor-css-tools >}}/html-preprocessor/03-express/pug/
[03-package-json]:          {{< tutor-css-tools >}}/html-preprocessor/03-express/pug/package.json
[03-app-js]:                {{< tutor-css-tools >}}/html-preprocessor/03-express/pug/app.js
[03-views-partials-sidebar]:{{< tutor-css-tools >}}/html-preprocessor/03-express/pug/views/partials/sidebar.pug
[03-views-partials-navbar]: {{< tutor-css-tools >}}/html-preprocessor/03-express/pug/views/partials/navbar.pug


