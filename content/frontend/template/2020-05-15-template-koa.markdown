---
type   : post
title  : "Template - Koa"
date   : 2020-05-15T09:17:35+07:00
slug   : template-koa
categories: [frontend]
tags      : [node, koa, pug, ejs, nunjucks]
keywords  : [template]
author : epsi
opengraph:
  image: assets-frontend/2020/05/01-html-hyperlink.png

toc    : "toc-2020-05-template"

excerpt:
  Templating Engine Configuration in KoaJS.

---

### Preface

> Goal: Templating Engine Configuration in KoaJS

Beside `ExpressJS`, there is also `KoaJS`.
Both are pretty popular old Web Framework in NodeJS.
And later, there is also `OakJS`, written for `DenoJS`.

We can rewrite our `express` configuration to `koa`.
All with the same logic and structure,
with our previous `express` example.

#### Official Documentation

* [koajs.com](https://koajs.com/)

-- -- --

### 1: EJS Case

> Rewrite Express to Koa, using EJS.

#### Source Examples

You can obtain source examples here:

* [html-preprocessor/03-koa/ejs/][source-example-ejs]

#### Directory Structure

The `views` structure is similar to previous case with `express`.
I suggest that you have fast reading for this article first.

* [Template - Express - EJS][local-express-ejs]

#### app.js

The `app.js` for `koajs` would be similar as this code below:

* [gitlab.com/.../ejs/app.js][03-ejs-app-js]

{{< highlight javascript >}}
const Koa    = require('koa');
const Router = require('koa-router');
const ejs    = require('koa-ejs');
const path   = require('path');
const serve  = require('koa-static');

const app = new Koa();
const router = new Router();

// Example Pages
const pages = [
  { link: '/',     short: 'Home', long: 'Home'  },
  { link: '/html', short: 'HTML', long: 'HTML Link' },
  { link: '/css',  short: 'CSS',  long: 'CSS Link' },
  { link: '/php',  short: 'PHP',  long: 'PHP Link' },
  { link: '/javascript', short: 'Javascript', long: 'Javascript Link' }
];

// Static Assets
app.use(serve('public'));

// Render EJS
ejs(app, {
  root: path.join(__dirname, 'views'),
  layout: false,
  viewExt: 'ejs',
  cache: false,
  debug: false
});

// Router
router.get('/', async ctx => {
  console.log('Showing index');
  await ctx.render('index', { pages: pages });
});

router.get('/:page', async ctx => {
  const choices = ['index', 'html', 'css', 'php', 'javascript'];
  const page = ctx.params.page;
  
  if (choices.includes(page)) {
    console.log('Showing ' + page);
    await ctx.render(page, { pages: pages });
  } else {
    console.log('404: Page not Found: '+ page);
    ctx.throw(404, '404: Page not Found');
  }
});

app.use(router.routes());

// Run, Baby Run
app.listen(3000);
console.log('Koa started on port 3000');
{{< / highlight >}}

The application is short enough to be self explanatory.

#### EJS Template Specific Code

Most configuration for all template,
in this article is the same except below code:

* The Header

{{< highlight javascript >}}
const Koa    = require('koa');
const Router = require('koa-router');
const ejs    = require('koa-ejs');
const path   = require('path');
const serve  = require('koa-static');
{{< / highlight >}}

* EJS Render

{{< highlight javascript >}}
ejs(app, {
  root: path.join(__dirname, 'views'),
  layout: false,
  viewExt: 'ejs',
  cache: false,
  debug: false
});
{{< / highlight >}}

#### Running Koa

Running express is as easy as this command below.

{{< highlight bash >}}
$ node app.js
{{< / highlight >}}

![HTML: Hyperlink Example][01-html-hyperlink]

Click any hyperlink on the example case,
and you will get the response in `console.log` immediately.

![Koa: node app.js][03-koa-app]

You are ready to run a website,
with `koa` backend, using `EJS`.

-- -- --

### 2: Pug Case

> Rewrite Express to Koa, using Pug.

#### Source Examples

You can obtain source examples here:

* [html-preprocessor/03-koa/pug/][source-example-pug]

#### Directory Structure

The `views` structure is similar to previous case with `express`.
I suggest that you have fast reading for this article first.

* [Template - Express - Pug][local-express-pug]

#### app.js

You can obtain the complete `app.js` for `koajs` here

* [gitlab.com/.../pug/app.js][03-pug-app-js]

![Koa: Configuration][03-koa-configuration]

#### Pug Template Specific Code

Most configuration for all template,
in this article is the same except below code:

* The Header

{{< highlight javascript >}}
const Koa    = require('koa');
const Router = require('koa-router');
const Pug    = require('koa-pug')
const path   = require('path');
const serve  = require('koa-static');
{{< / highlight >}}

* Pug Render

{{< highlight javascript >}}
const pug = new Pug({
  viewPath: path.resolve(__dirname, './views'),
  app: app // Binding
})
{{< / highlight >}}

-- -- --

### 3: Nunjucks Case

> Rewrite Express to Koa, using Nunjucks.

#### Source Examples

You can obtain source examples here:

* [html-preprocessor/03-koa/nunjucks/][source-example-nunjucks]

#### Directory Structure

The `views` structure is similar to previous case with `express`.
I suggest that you have fast reading for this article first.

* [Template - Express - Nunjucks][local-express-nunjucks]

#### app.js

You can obtain the complete `app.js` for `koajs` here

* [gitlab.com/.../nunjucks/app.js][03-nunjucks-app-js]

#### Nunjucks Template Specific Code

Most configuration for all template,
in this article is the same except below code:

* The Header

{{< highlight javascript >}}
const Koa      = require('koa');
const Router   = require('koa-router');
const nunjucks = require('koa-nunjucks-2');
const path     = require('path');
const serve    = require('koa-static');
{{< / highlight >}}

* Nunjucks Render

{{< highlight javascript >}}
app.use(nunjucks({
  ext: 'njk',
  path: path.join(__dirname, 'views'),
  nunjucksConfig: {
    trimBlocks: true
  }
}));
{{< / highlight >}}

-- -- --

### What's Next?

Consider continue reading [ [Template - Eleventy - Nunjucks][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}frontend/2020/05/21/template-11ty-nunjucks/
[local-express-pug]:        {{< baseurl >}}frontend/2020/05/10/template-express-pug/
[local-express-nunjucks]:   {{< baseurl >}}frontend/2020/05/11/template-express-nunjucks/
[local-express-ejs]:        {{< baseurl >}}frontend/2020/05/12/template-express-ejs/

[01-html-hyperlink]:        {{< assets-frontend >}}/2020/05/01-html-hyperlink.png
[03-koa-app]:               {{< assets-frontend >}}/2020/05/03-koa-app.png
[03-koa-configuration]:     {{< assets-frontend >}}/2020/05/03-koa-config.png

[source-example-pug]:       {{< tutor-css-tools >}}/html-preprocessor/03-koa/pug/
[source-example-ejs]:       {{< tutor-css-tools >}}/html-preprocessor/03-koa/ejs/
[source-example-nunjucks]:  {{< tutor-css-tools >}}/html-preprocessor/03-koa/nunjucks/
[03-pug-app-js]:            {{< tutor-css-tools >}}/html-preprocessor/03-koa/pug/app.js
[03-ejs-app-js]:            {{< tutor-css-tools >}}/html-preprocessor/03-koa/ejs/app.js
[03-nunjucks-app-js]:       {{< tutor-css-tools >}}/html-preprocessor/03-koa/nunjucks/app.js

