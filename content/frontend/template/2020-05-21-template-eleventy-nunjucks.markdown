---
type   : post
title  : "Template - Eleventy - Nunjucks"
date   : 2020-05-21T09:17:35+07:00
slug   : template-11ty-nunjucks
categories: [ssg]
tags      : [node, nunjucks, 11ty]
keywords  : [template]
author : epsi
opengraph:
  image: assets-frontend/2020/05-ssg/04-eleventy-hyperlink.png

toc    : "toc-2020-05-template"

excerpt:
  Feeding Data in Eleventy, and Process in a Loop with Nunjucks.

---

### Preface

> Goal: Feeding Data in Eleventy, and Process in a Loop with Nunjucks.

Consider continue with `nunjucks` example.
This time using SSG (static site generator),
called `eleventy` (`11ty)`.

#### Official Documentation

* [mozilla.github.io/nunjucks](https://mozilla.github.io/nunjucks/)

* [11ty.dev/docs/](https://www.11ty.dev/docs/)

#### Source Examples

You can obtain source examples here:

* [html-preprocessor/04-11ty/][source-example]

-- -- --

### Nunjucks Case

The `nunjucks` case is the same,
with the one utilizing `grunt` in previous article
with a few differences.

* `eleventy` configuration

* Using data to provide hyperlink in both `navbar` and `sidebar`.

#### Directory Structure

Consider an `eleventy` structure with `nunjucks` views.

{{< highlight bash >}}
❯ tree
.
├── assets
│   ├── css
│   │   └── w3.css
│   ├── favicon.ico
│   └── js
│       └── script.js
├── package.json
└── views
    ├── css.md
    ├── _data
    │   └── pages.json
    ├── html.md
    ├── _includes
    │   ├── layouts
    │   │   └── index.njk
    │   └── site
    │       ├── head.njk
    │       ├── navbar.njk
    │       └── sidebar.njk
    ├── index.md
    ├── javascript.md
    └── php.md

8 directories, 14 files
{{< / highlight >}}

_._

![Eleventy: Example: Tree][04-eleventy-tree-example]

The `views` structure is similar to previous case with `grunt`.
I suggest that you have fast reading for this article first.

* [Template - Grunt - Nunjucks][local-grunt-case]

#### package.json

All you need to know is the `devDependencies`.

* [gitlab.com/.../package.json][04-package-json].

{{< highlight javascript >}}
  "devDependencies": {
    "@11ty/eleventy": "^0.11.0"
  }
{{< / highlight >}}

To begin with `11ty`, you need to run `npm install` first.

#### .eleventy.js

You also need to a write a configuration in `.eleventy.js`.

* [gitlab.com/.../.eleventy.js][04-eleventy-js]

{{< highlight javascript >}}
module.exports = function(eleventyConfig) {

  // Directory Management
  eleventyConfig.addPassthroughCopy("assets");

  // Layout Alias
  eleventyConfig.addLayoutAlias("index", "layouts/index.njk");
  
  // Return your Config object
  return {
    // URL Related
    pathPrefix: "/",

    // Templating Engine
    templateFormats: [
      "njk",
      "html",
      "md"
    ],

    htmlTemplateEngine: "njk",

    // Directory Management
    passthroughFileCopy: true,
    dir: {
      input: "views",
      output: "dist",
      // ⚠️ These values are both relative to your input directory.
      includes: "_includes",
      data: "_data"
    }
  };
};
{{< / highlight >}}

Global configuration is a must have in SSG world.

#### Nunjucks Template Specific Code

Configuration for `nunjucks` has been setup in configuration above.

{{< highlight javascript >}}
    // Templating Engine
    templateFormats: [
      "njk",
      "html"
    ],

    htmlTemplateEngine: "njk",
{{< / highlight >}}

#### Hyperlink Data

In `eleventy`, data can be express in `yaml`, `toml`, `json` or `javascript`.

To provide hyperlink in both `navbar` and `sidebar`,
we need to setup the data in `pages.js` as below:

* [gitlab.com/.../_data/pages.json][04-pages-json].

{{< highlight javascript >}}
[
  { "link": "/",     "short": "Home", "long": "Home"  },
  { "link": "/html", "short": "HTML", "long": "HTML Link" },
  { "link": "/css",  "short": "CSS",  "long": "CSS Link" },
  { "link": "/php",  "short": "PHP",  "long": "PHP Link" },
  { "link": "/javascript", "short": "Javascript", "long": "Javascript Link" }
]
{{< / highlight >}}

The `sidebar` will use `long` text,
and the `navbar` will use `short` one.

#### Router

> Set automatically with SSG

#### Layout

* [html-preprocessor/.../views/_includes/layouts/index.njk][04-views-layouts-index]

{{< highlight jinja >}}
<!DOCTYPE html>
<html lang="en">

<head>
  {% include "site/head.njk" %}
</head>
<body>
  {% include "site/sidebar.njk" %}

  <div id="main">
    {% include "site/navbar.njk" %}

    <div class="w3-container">
    {{ content | safe }}
    </div>
  </div>

</body>
</html>
{{< / highlight >}}

As shown in order we have three `include` artefacts.

* `include head.njk`,
* `include sidebar.njk`,
* `include navbar.njk`,

And one `eleventy` specific tag `content`.

{{< highlight jinja >}}
    <div class="w3-container">
    {{ content | safe }}
    </div>
{{< / highlight >}}

![Nunjucks: ViM NERDTree Panes][04-nunjucks-nerdtree]

#### Head

It is almost the same with the original head.

* [html-preprocessor/.../views/_includes/site/head.njk][04-views-site-head]

{{< highlight html >}}
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>{{ title or "Link Example - Default" }}</title>

  <link rel="stylesheet" href="{{ "/assets/css/w3.css" | url }}">
  <script src="{{ "/assets/js/script.js" | url }}"></script>
{{< / highlight >}}

Here we put the default title if we cannot find `title` variable.

{{< highlight jinja >}}
<title>{{ title or "Link Example - Default" }}</title>
{{< / highlight >}}

No need to add `livereload`,
since utilized `browsersync` has internal sync.

#### Loop in Template

> Very similar with previous example

This time, consider alter a bit,
both the `sidebar` and the `navbar`,
so we can render each data in loop.

* [html-preprocessor/.../views/_includes/site/sidebar.njk][04-views-site-sidebar]

{{< highlight jinja >}}

  <div class="w3-sidebar w3-bar-block w3-card w3-animate-left" 
       style="display:none" id="mySidebar">
    <button class="w3-bar-item w3-button w3-large"
       id="closeNav" >Close &times;</button>
    {% for page in pages %}
      <a class="w3-bar-item w3-button"
         href="{{ page.link }}">{{ page.long }}</a>
    {% endfor %}
  </div>
{{< / highlight >}}

* [html-preprocessor/.../views/_includes/site/navbar.njk][04-views-site-navbar]

{{< highlight jinja >}}

    <div class="w3-teal">
      <button class="w3-button w3-teal w3-xlarge"
              id="openNav">&#9776;</button>
      <div class="w3-container">
        <h1>My Page</h1>
      </div>
    </div>

    <div class="w3-bar w3-black">
    {% for page in pages %}
      <a class="w3-bar-item w3-button"
         href="{{ page.link }}">{{ page.short }}</a>
    {% endfor %}
    </div>
{{< / highlight >}}

All you need to know is these lines:

{{< highlight jinja >}}
    {% for page in pages %}
      <a class="w3-bar-item w3-button"
         href="{{ page.link }}">{{ page.short }}</a>
    {% endfor %}
{{< / highlight >}}

![Eleventy: Loop in Nunjucks][04-nunjucks-panes]

This will render a hyperlink,
with result about the same with previous example.

#### Running 11ty

Running `eleventy` (`11ty`) is as easy as this command below.

{{< highlight bash >}}
$ eleventy --serve
{{< / highlight >}}

![HTML: Hyperlink Example][04-eleventy-hyperlink]

{{< highlight bash >}}
❯ eleventy --serve
Writing dist/css/index.html from ./views/css.html.
Writing dist/html/index.html from ./views/html.html.
Writing dist/index.html from ./views/index.html.
Writing dist/php/index.html from ./views/php.html.
Writing dist/javascript/index.html from ./views/javascript.html.
Copied 3 files / Wrote 5 files in 0.27 seconds (v0.11.0)
Watching…
[Browsersync] Access URLs:
 --------------------------------------
       Local: http://localhost:8080
    External: http://192.168.1.223:8080
 --------------------------------------
          UI: http://localhost:3001
 UI External: http://localhost:3001
 --------------------------------------
[Browsersync] Serving files from: dist
{{< / highlight >}}

![11ty: eleventy --serve][04-eleventy-serve]

You are ready to run a website,
with `eleventy` (`11ty`) SSG, using `nunjucks`.

-- -- --

### What's Next?

Consider continue reading [ [Template - Pelican - Jinja2][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}frontend/2020/05/22/template-pelican-jinja2/
[local-grunt-case]:         {{< baseurl >}}frontend/2020/05/06/template-grunt-nunjucks/

[04a-eleventy-hyperlink]:    {{< baseurl >}}assets/posts/frontend/2020/05-ssg/04-eleventy-hyperlink.png
[04a-eleventy-tree-example]: {{< baseurl >}}assets/posts/frontend/2020/05-ssg/04-eleventy-nunjucks-tree-example.png
[04a-eleventy-serve]:        {{< baseurl >}}assets/posts/frontend/2020/05-ssg/04-eleventy-serve.png
[04a-nunjucks-panes]:        {{< baseurl >}}assets/posts/frontend/2020/05-ssg/04-eleventy-nunjucks-panes.png
[04a-nunjucks-nerdtree]:     {{< baseurl >}}assets/posts/frontend/2020/05-ssg/04-eleventy-nunjucks-nerdtree.png

[04-eleventy-hyperlink]:    {{< assets-frontend >}}/2020/05-ssg/04-eleventy-hyperlink.png
[04-nunjucks-tree-example]: {{< assets-frontend >}}/2020/05-ssg/04-eleventy-nunjucks-tree-example.png
[04-eleventy-serve]:        {{< assets-frontend >}}/2020/05-ssg/04-eleventy-serve.png
[04-nunjucks-panes]:        {{< assets-frontend >}}/2020/05-ssg/04-eleventy-nunjucks-panes.png
[04-nunjucks-nerdtree]:     {{< assets-frontend >}}/2020/05-ssg/04-eleventy-nunjucks-nerdtree.png

[source-example]:           {{< tutor-css-tools >}}/html-preprocessor/04-11ty/
[04-package-json]:          {{< tutor-css-tools >}}/html-preprocessor/04-11ty/package.json
[04-eleventy-js]:           {{< tutor-css-tools >}}/html-preprocessor/04-11ty/.eleventy.js
[04-pages-json]:            {{< tutor-css-tools >}}/html-preprocessor/04-11ty/views/_data/pages.json

[04-views-layouts-index]:   {{< tutor-css-tools >}}/html-preprocessor/04-11ty/views/_includes/layouts/index.njk
[04-views-site-head]:       {{< tutor-css-tools >}}/html-preprocessor/04-11ty/views/_includes/site/head.njk
[04-views-site-navbar]:     {{< tutor-css-tools >}}/html-preprocessor/04-11ty/views/_includes/site/navbar.njk
[04-views-site-sidebar]:    {{< tutor-css-tools >}}/html-preprocessor/04-11ty/views/_includes/site/sidebar.njk
