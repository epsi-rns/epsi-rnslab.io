---
type   : post
title  : "Template - Pelican - Jinja2"
date   : 2020-05-22T09:17:35+07:00
slug   : template-pelican-jinja2
categories: [ssg]
tags      : [python, jinja2, pelican]
keywords  : [template]
author : epsi
opengraph:
  image: assets-frontend/2020/05-ssg/04-pelican-hyperlink.png

toc    : "toc-2020-05-template"

excerpt:
  Feeding Data in Pelican, and Process in a Loop with Jinja2.

---

### Preface

> Goal: Feeding Data in Pelican, and Process in a Loop with Jinja2.

Consider continue with `jinja2` example.
This time using SSG (static site generator),
called `pelican` blog.

`Jinja2` is very similar to `nunjucks`.
In fact `nunjucks` is heavily inspired by `jinja2`.

#### Official Documentation

* [jinja.palletsprojects.com/](https://jinja.palletsprojects.com/)

* [docs.getpelican.com/](https://docs.getpelican.com/)

#### Source Examples

You can obtain source examples here:

* [html-preprocessor/04-pelican/][source-example]

-- -- --

### Pelican Case

The `jinja2` case is the same,
with the one utilizing `grunt` in previous article
with a few differences.

* `pelican` configuration

* Using data to provide hyperlink in both `navbar` and `sidebar`.

#### Directory Structure

Consider an `pelican` structure with `jinja2` views.

{{< highlight bash >}}
❯ tree
.
├── content
│   └── pages
│       ├── css.md
│       ├── html.md
│       ├── index.md
│       ├── javascript.md
│       └── php.md
├── Makefile
├── pelicanconf.py
├── publishconf.py
├── tasks.py
└── tutor
    ├── static
    │   ├── css
    │   │   └── w3.css
    │   ├── favicon.ico
    │   └── js
    │       └── script.js
    └── templates
        ├── article.html
        ├── base.html
        ├── index.html
        ├── page.html
        └── site
            ├── head.html
            ├── navbar.html
            └── sidebar.html

8 directories, 19 files
{{< / highlight >}}

This `pelican` using theme structure.
This is why we have `article.html` and `index.html`,
although we only use `page.html` to generate static pages.

![Eleventy: Example: Tree][04-pelican-tree-example]

The `views` structure is similar to previous case with `grunt`.
I suggest that you have fast reading for this article first.

* [Template - Grunt - Nunjucks][local-grunt-case]

#### pelicanconf.py

You also need to a write a configuration in `pelicanconf.py`.

* [gitlab.com/.../pelicanconf.py][04-pelicanconf-py]

{{< highlight javascript >}}
#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'epsi'
# SITENAME = 'Yet Another Example'
SITEURL = ''

THEME = "tutor"

PATH = 'content'

TIMEZONE = 'Asia/Jakarta'

DEFAULT_LANG = 'en'

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

INDEX_URL             = 'blog/'
INDEX_SAVE_AS         = 'blog/index.html'
{{< / highlight >}}

Global configuration is a must have in SSG world.

#### Hyperlink Data

In `pelican`, data can be stored directly in configuration.
After all it is just a python script.

To provide hyperlink in both `navbar` and `sidebar`,
we need to setup additional confiuration in `pelicanconf.py` as below:

{{< highlight python >}}
# Data
PAGES = (
  { "link": "/",                "short": "Home", "long": "Home"  },
  { "link": "/pages/html.html", "short": "HTML", "long": "HTML Link" },
  { "link": "/pages/css.html",  "short": "CSS",  "long": "CSS Link" },
  { "link": "/pages/php.html",  "short": "PHP",  "long": "PHP Link" },
  { "link": "/pages/javascript.html", "short": "Javascript", "long": "Javascript Link" }
)
{{< / highlight >}}

The `sidebar` will use `long` text,
and the `navbar` will use `short` one.

#### Router

> Set automatically with SSG

#### Layout

* [html-preprocessor/.../tutor/templates/base.html][04-views-layouts-base]

{{< highlight jinja >}}
<!DOCTYPE html>
<html lang="en">

<head>
  {% include 'site/head.html' %}
</head>
<body>
  {% include 'site/sidebar.html' %}

  <div id="main">
    {% include 'site/navbar.html' %}

    <div class="w3-container">
    {% block content %}{% endblock %}
    </div>
  </div>

</body>
</html>
{{< / highlight >}}

As shown in order we have three `include` artefacts.

* `include head.html`,
* `include sidebar.html`,
* `include navbar.html`,

And one `pelican` block `content`.

{{< highlight jinja >}}
    <div class="w3-container">
    {% block content %}{% endblock %}
    </div>
{{< / highlight >}}

![Nunjucks: ViM NERDTree Panes][04-jinja2-nerdtree]

#### Head

It is almost the same with the original head.

* [html-preprocessor/.../tutor/templates/site/head.html][04-views-site-head]

{{< highlight html >}}
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  {% if not headtitle %}{% set headtitle = "Link Example - Default" %}{% endif %}
  <title>{{ headtitle }}</title>

  <link rel="stylesheet" type="text/css"
        href="{{ SITEURL }}/theme/css/w3.css">
  <script src="{{ SITEURL }}/theme/js/script.js"></script>
{{< / highlight >}}

Here we put the default title if we cannot find `title` variable.

{{< highlight jinja >}}
  {% if not headtitle %}{% set headtitle = "Link Example - Default" %}{% endif %}
  <title>{{ headtitle }}</title>
{{< / highlight >}}

#### Loop in Template

> Very similar with previous example

This time, consider alter a bit,
both the `sidebar` and the `navbar`,
so we can render each data in loop.

* [html-preprocessor/.../tutor/templates/site/sidebar.html][04-views-site-sidebar]

{{< highlight jinja >}}

  <div class="w3-sidebar w3-bar-block w3-card w3-animate-left" 
       style="display:none" id="mySidebar">
    <button class="w3-bar-item w3-button w3-large"
       id="closeNav" >Close &times;</button>
    {% for page in PAGES %}
      <a class="w3-bar-item w3-button"
         href="{{ page.link }}">{{ page.long }}</a>
    {% endfor %}
  </div>
{{< / highlight >}}

* [html-preprocessor/.../tutor/templates/site/navbar.html][04-views-site-navbar]

{{< highlight jinja >}}

    <div class="w3-teal">
      <button class="w3-button w3-teal w3-xlarge"
              id="openNav">&#9776;</button>
      <div class="w3-container">
        <h1>My Page</h1>
      </div>
    </div>

    <div class="w3-bar w3-black">
    {% for page in PAGES %}
      <a class="w3-bar-item w3-button"
         href="{{ page.link }}">{{ page.short }}</a>
    {% endfor %}
    </div>
{{< / highlight >}}

All you need to know is these lines:

{{< highlight jinja >}}
    {% for page in PAGES %}
      <a class="w3-bar-item w3-button"
         href="{{ page.link }}">{{ page.short }}</a>
    {% endfor %}
{{< / highlight >}}

![Pelican: Loop in Jinja2][04-jinja2-panes]

This will render a hyperlink,
with result about the same with previous example.

#### Running Pelican

Running `pelican` is as easy as this command below.

{{< highlight bash >}}
$ make devserver
{{< / highlight >}}

![HTML: Hyperlink Example][04-pelican-hyperlink]

{{< highlight bash >}}
❯ make devserver
pelican -lr /home/epsi/Documents/html-preprocessor/04-pelican/content -o /home/epsi/Documents/html-preprocessor/04-pelican/output -s /home/epsi/Documents/html-preprocessor/04-pelican/pelicanconf.py 

-> Modified: content, theme, settings. re-generating...
Done: Processed 0 articles, 0 drafts, 5 pages, 0 hidden pages and 0 draft pages in 0.29 seconds.
{{< / highlight >}}

![Pelican: make devserver][04-pelican-serve]

You are ready to run a website,
with `pelican` SSG, using `jinja2`.

-- -- --

### What's Next?

Consider continue reading [ [Template - Jekyll - Liquid][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}frontend/2020/05/23/template-jekyll-liquid/
[local-grunt-case]:         {{< baseurl >}}frontend/2020/05/06/template-grunt-nunjucks/

[04-pelican-hyperlink]:     {{< assets-frontend >}}/2020/05-ssg/04-pelican-hyperlink.png
[04-pelican-tree-example]:  {{< assets-frontend >}}/2020/05-ssg/04-pelican-jinja2-tree-example.png
[04-pelican-serve]:         {{< assets-frontend >}}/2020/05-ssg/04-pelican-make-devserver.png
[04-jinja2-panes]:          {{< assets-frontend >}}/2020/05-ssg/04-pelican-jinja2-panes.png
[04-jinja2-nerdtree]:       {{< assets-frontend >}}/2020/05-ssg/04-pelican-jinja2-nerdtree.png

[source-example]:           {{< tutor-css-tools >}}/html-preprocessor/04-pelican/
[04-pelicanconf-py]:        {{< tutor-css-tools >}}/html-preprocessor/04-pelican/pelicanconf.py
[04-views-layouts-base]:    {{< tutor-css-tools >}}/html-preprocessor/04-pelican/tutor/templates/base.html

[04-views-site-head]:       {{< tutor-css-tools >}}/html-preprocessor/04-pelican/tutor/templates/site/head.html
[04-views-site-navbar]:     {{< tutor-css-tools >}}/html-preprocessor/04-pelican/tutor/templates/site/navbar.html
[04-views-site-sidebar]:    {{< tutor-css-tools >}}/html-preprocessor/04-pelican/tutor/templates/site/sidebar.html
