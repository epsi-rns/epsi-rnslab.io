---
type   : post
title  : "Template - Jekyll - Liquid"
date   : 2020-05-23T09:17:35+07:00
slug   : template-jekyll-liquid
categories: [ssg]
tags      : [ruby, liquid, jekyll]
keywords  : [template]
author : epsi
opengraph:
  image: assets-frontend/2020/05-ssg/04-jekyll-hyperlink.png

toc    : "toc-2020-05-template"

excerpt:
  Feeding Data in Jekyll, and Process in a Loop with Liquid.

---

### Preface

> Goal: Feeding Data in Jekyll, and Process in a Loop with Liquid.

Consider continue have a look at templating engine
that we never use before.
We are going to use `liquid` example,
using SSG (static site generator), called `jekyll`.

#### Official Documentation

* [shopify.github.io/liquid/](https://shopify.github.io/liquid/)

* [jekyllrb.com/docs/](https://https://jekyllrb.com/docs/)

#### Source Examples

You can obtain source examples here:

* [html-preprocessor/04-jekyll/][source-example]

-- -- --

### Liquid Case

The `liquid` case is very similar with previously `nunjucks` example.

* `jekyll` configuration

* Using data to provide hyperlink in both `navbar` and `sidebar`.

#### Directory Structure

Consider an `jekyll` structure with `liquid` views.

{{< highlight bash >}}
❯ tree
.
├── assets
│   ├── css
│   │   └── w3.css
│   └── js
│       └── script.js
├── _config.yml
├── _data
│   └── pages.yml
├── favicon.ico
├── Gemfile
├── Gemfile.lock
├── _includes
│   ├── head.html
│   ├── navbar.html
│   └── sidebar.html
├── _layouts
│   └── index.html
├── pages
│   ├── css.md
│   ├── html.md
│   ├── index.md
│   ├── javascript.md
│   └── php.md
└── _posts

8 directories, 16 files
{{< / highlight >}}

_._

![Jekyll: Example: Tree][04-jekyll-tree-example]

#### Gemfile

All you need to know is the `jeykll` version.

* [gitlab.com/.../Gemfile][04-gemfile].

{{< highlight ruby >}}
source "https://rubygems.org"
gem "jekyll", "~> 3.8.6"
{{< / highlight >}}

To begin with `11ty`, you need to run `npm install` first.

#### _config.yml

You also need to a write a configuration in `_config.yml`.

* [gitlab.com/.../_config.yml][04-config-yml]

{{< highlight javascript >}}
# Site settings
title: Your mission. Good Luck!

# the subpath of your site, e.g. /blog
baseurl: "" 

# the base hostname & protocol for your site, e.g. http://example.com
url: "http://localhost:4000"
{{< / highlight >}}

Global configuration is a must have in SSG world.

#### Hyperlink Data

In `jekyll`, data can be express in `yaml`.

To provide hyperlink in both `navbar` and `sidebar`,
we need to setup the data in `pages.yml` as below:

* [gitlab.com/.../_data/pages.yml][04-pages-yml].

{{< highlight yaml >}}
# Helper for related links

- link : "/"
  short: "Home"
  long : "Home"

- link : "/html.html"
  short: "HTML"
  long : "HTML Link"

- link : "/css.html"
  short: "CSS"
  long : "CSS Link"

- link : "/php.html"
  short: "PHP"
  long : "PHP Link"

- link : "/javascript.html"
  short: "Javascript"
  long : "Javascript Link"
{{< / highlight >}}

The `sidebar` will use `long` text,
and the `navbar` will use `short` one.

#### Router

> Set automatically with SSG

#### Layout

* [html-preprocessor/.../_layouts/default.html][04-views-layouts-default]

{{< highlight jinja >}}
<!DOCTYPE html>
<html lang="en">

<head>
  {% include head.html %}
</head>
<body>
  {% include sidebar.html %}

  <div id="main">
    {% include navbar.html %}

    <div class="w3-container">
    {{ content }}
    </div>
  </div>

</body>
</html>
{{< / highlight >}}

As shown in order we have three `include` artefacts.

* `include head.html`,
* `include sidebar.html`,
* `include navbar.html`,

And one `jekyll` specific tag `content`.

{{< highlight jinja >}}
    <div class="w3-container">
    {{ content }}
    </div>
{{< / highlight >}}

![Nunjucks: ViM NERDTree Panes][04-liquid-nerdtree]

#### Head

It is almost the same with the original head.

* [html-preprocessor/.../_includes/head.html][04-views-sites-head]

{{< highlight html >}}
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>{{ title or "Link Example - Default" }}</title>

  <link rel="stylesheet" href="{{ "/assets/css/w3.css" | url }}">
  <script src="{{ "/assets/js/script.js" | url }}"></script>
{{< / highlight >}}

Here we put the default title if we cannot find `title` variable.

{{< highlight jinja >}}
<title>{{ title or "Link Example - Default" }}</title>
{{< / highlight >}}

No need to add `livereload`,
since utilized `browsersync` has internal sync.

#### Loop in Template

> Very similar with previous example

This time, consider alter a bit,
both the `sidebar` and the `navbar`,
so we can render each data in loop.

* [html-preprocessor/.../_includes/sidebar.html][04-views-site-sidebar]

{{< highlight jinja >}}

  <div class="w3-sidebar w3-bar-block w3-card w3-animate-left" 
       style="display:none" id="mySidebar">
    <button class="w3-bar-item w3-button w3-large"
       id="closeNav" >Close &times;</button>
    {% for page in site.data.pages %}
      <a class="w3-bar-item w3-button"
         href="{{ page.link }}">{{ page.long }}</a>
    {% endfor %}
  </div>
{{< / highlight >}}

* [html-preprocessor/.../_includes/navbar.html][04-views-site-navbar]

{{< highlight jinja >}}

    <div class="w3-teal">
      <button class="w3-button w3-teal w3-xlarge"
              id="openNav">&#9776;</button>
      <div class="w3-container">
        <h1>My Page</h1>
      </div>
    </div>

    <div class="w3-bar w3-black">
    {% for page in site.data.pages %}
      <a class="w3-bar-item w3-button"
         href="{{ page.link }}">{{ page.short }}</a>
    {% endfor %}
    </div>
{{< / highlight >}}

All you need to know is these lines:

{{< highlight jinja >}}
    {% for page in site.data.pages %}
      <a class="w3-bar-item w3-button"
         href="{{ page.link }}">{{ page.short }}</a>
    {% endfor %}
{{< / highlight >}}

![Jekyll: Loop in Liquid][04-liquid-panes]

This will render a hyperlink,
with result about the same with previous example.

#### Running Jekyll

Running `jekyll` is as easy as this command below.

{{< highlight bash >}}
$ jekyll serve
{{< / highlight >}}

![HTML: Hyperlink Example][04-jekyll-hyperlink]

{{< highlight bash >}}
jekyll serve
Configuration file: /home/epsi/Documents/html-preprocessor/04-jekyll/_config.yml
            Source: /home/epsi/Documents/html-preprocessor/04-jekyll
       Destination: /home/epsi/Documents/html-preprocessor/04-jekyll/_site
 Incremental build: disabled. Enable with --incremental
      Generating... 
                    done in 0.062 seconds.
 Auto-regeneration: enabled for '/home/epsi/Documents/html-preprocessor/04-jekyll'
    Server address: http://127.0.0.1:4000/
  Server running... press ctrl-c to stop.

{{< / highlight >}}

![Jekyll: jekyll serve][04-jekyll-serve]

You are ready to run a website,
with `jekyll` SSG, using `liquid`.

-- -- --

### What's Next?

Consider continue reading [ [Template - Hexo - EJS][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}frontend/2020/05/24/template-hexo-ejs/
[local-grunt-case]:         {{< baseurl >}}frontend/2020/05/06/template-grunt-nunjucks/

[04-jekyll-hyperlink]:      {{< assets-frontend >}}/2020/05-ssg/04-jekyll-hyperlink.png
[04-jekyll-tree-example]:   {{< assets-frontend >}}/2020/05-ssg/04-jekyll-liquid-tree-example.png
[04-jekyll-serve]:          {{< assets-frontend >}}/2020/05-ssg/04-jekyll-serve.png
[04-liquid-panes]:          {{< assets-frontend >}}/2020/05-ssg/04-jekyll-liquid-panes.png
[04-liquid-nerdtree]:       {{< assets-frontend >}}/2020/05-ssg/04-jekyll-liquid-nerdtree.png

[source-example]:           {{< tutor-css-tools >}}/html-preprocessor/04-jekyll/
[04-gemfile]:               {{< tutor-css-tools >}}/html-preprocessor/04-jekyll/Gemfile
[04-config-yml]:            {{< tutor-css-tools >}}/html-preprocessor/04-jekyll/_config.yml
[04-pages-yml]:             {{< tutor-css-tools >}}/html-preprocessor/04-jekyll//views/_data/pages.yml
[04-views-layouts-default]: {{< tutor-css-tools >}}/html-preprocessor/04-jekyll/_layouts/default.html

[04-views-site-head]:       {{< tutor-css-tools >}}/html-preprocessor/04-jekyll/_includes/head.html
[04-views-site-navbar]:     {{< tutor-css-tools >}}/html-preprocessor/04-jekyll/_includes/navbar.html
[04-views-site-sidebar]:    {{< tutor-css-tools >}}/html-preprocessor/04-jekyll/_includes/sidebar.html
