---
type   : post
title  : "Template - Hexo - EJS"
date   : 2020-05-24T09:17:35+07:00
slug   : template-hexo-ejs
categories: [ssg]
tags      : [node, hexo, ejs]
keywords  : [template]
author : epsi
opengraph:
  image: assets-frontend/2020/05-ssg/04-hexo-hyperlink.png

toc    : "toc-2020-05-template"

excerpt:
  Feeding Data in Hexo, and Process in a Loop with EJS.

---

### Preface

> Goal: Feeding Data in Hexo, and Process in a Loop with EJS.

Consider continue with `EJS` example.
This time using SSG (static site generator),
called `hexo`.

#### Official Documentation

* [ejs.co](https://ejs.co/#docs)

* [hexo.io/docs/](https://hexo.io/docs/)

#### Source Examples

You can obtain source examples here:

* [html-preprocessor/04-hexo/][source-example]

-- -- --

### EJS Case

The `EJS` case is the same,
with the one utilizing `grunt` in previous article
with a few differences.

* `hexo` configuration

* Using data to provide hyperlink in both `navbar` and `sidebar`.

#### Directory Structure

Consider an `hexo` structure with `EJS` views.

{{< highlight bash >}}
❯ tree
.
├── _config.yml
├── package.json
├── package-lock.json
├── source
│   ├── css.md
│   ├── _data
│   │   └── pages.yml
│   ├── html.md
│   ├── index.md
│   ├── javascript.md
│   └── php.md
└── themes
    └── tutor
        ├── layout
        │   ├── layout.ejs
        │   ├── page.ejs
        │   └── site
        │       ├── head.ejs
        │       ├── navbar.ejs
        │       └── sidebar.ejs
        └── source
            ├── css
            │   └── w3.css
            ├── favicon.ico
            └── js
                └── script.js

9 directories, 17 files
{{< / highlight >}}

_._

![Eleventy: Example: Tree][04-hexo-tree-example]

The `views` structure is similar to previous case with `grunt`.
I suggest that you have fast reading for this article first.

* [Template - Grunt - EJS][local-grunt-case]

#### package.json

All you need to know is the `devDependencies`.

* [gitlab.com/.../package.json][04-package-json].

{{< highlight javascript >}}
  "dependencies": {
    "hexo": "^3.9.0",
    "hexo-renderer-ejs": "^0.3.1",
    "hexo-server": "^0.3.3"
  }
{{< / highlight >}}

To begin with `hexo`, you need to run `npm install` first.

#### _config.yml

You also need to a write a configuration in `_config.yml`.

* [gitlab.com/.../_config.yml][04-config-yml]

{{< highlight yaml >}}
# Site
title         : Yet Another Example
language      : en
timezone      : Asia/Jakarta

# Extensions
## Themes: https://hexo.io/themes/
theme: tutor
# theme: landscape

# URL
url           : http://example/
# root: /child/
root          : /


# Directory
source_dir    : source
public_dir    : public
{{< / highlight >}}

Global configuration is a must have in SSG world.

#### Hyperlink Data

In `hexo`, data can be express in `yaml`.

To provide hyperlink in both `navbar` and `sidebar`,
we need to setup the data in `pages.yml` as below:

* [gitlab.com/.../source/_data/pages.yml][04-pages-yml].

{{< highlight yaml >}}
# Helper for related links

- link : "/"
  short: "Home"
  long : "Home"

- link : "/html.html"
  short: "HTML"
  long : "HTML Link"

- link : "/css.html"
  short: "CSS"
  long : "CSS Link"

- link : "/php.html"
  short: "PHP"
  long : "PHP Link"

- link : "/javascript.html"
  short: "Javascript"
  long : "Javascript Link"
{{< / highlight >}}

The `sidebar` will use `long` text,
and the `navbar` will use `short` one.

#### Router

> Set automatically with SSG

#### Layout

* [html-preprocessor/.../themes/tutor/layout/layout.ejs][04-views-layout-layout]

{{< highlight jinja >}}
<!DOCTYPE html>
<html lang="en">

<head>
  <%- include('site/head') %>
</head>
<body>
  <%- include('site/sidebar') %>

  <div id="main">
    <%- include('site/navbar') %>

    <div class="w3-container">
      <%- body %>
    </div>
  </div>

</body>
</html>
{{< / highlight >}}

As shown in order we have three `include` artefacts.

* `include head.njk`,
* `include sidebar.njk`,
* `include navbar.njk`,

And one eleventy specific tag `content`.

{{< highlight jinja >}}
    <div class="w3-container">
      <%- body %>
    </div>
{{< / highlight >}}

![Nunjucks: ViM NERDTree Panes][04-ejs-nerdtree]

#### Head

It is a little bit different with the original head.

* [html-preprocessor/.../themes/tutor/layout/site/head.ejs][04-views-site-head]

{{< highlight javascript >}}
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title><%= page.title || "Link Example - Default" %></title>

  <%- css('/css/w3.css') %>
  <%- js('/js/script.js') %>
{{< / highlight >}}

Here we put the default title if we cannot find `title` variable.

{{< highlight javascript >}}
<title><%= page.title || "Link Example - Default" %></title>
{{< / highlight >}}

#### Loop in Template

> Very similar with previous example

This time, consider alter a bit,
both the `sidebar` and the `navbar`,
so we can render each data in loop.

* [html-preprocessor/.../themes/tutor/layout/site/sidebar.ejs][04-views-site-sidebar]

{{< highlight javascript >}}

  <div class="w3-sidebar w3-bar-block w3-card w3-animate-left" 
       style="display:none" id="mySidebar">
    <button class="w3-bar-item w3-button w3-large"
       id="closeNav" >Close &times;</button>
    <% site.data.pages.forEach(function(page){ %>
      <a class="w3-bar-item w3-button"
         href="<%= page.link %>"><%= page.long %></a>
    <% }) %>
  </div>
{{< / highlight >}}

* [html-preprocessor/.../themes/tutor/layout/site/navbar.ejs][04-views-site-navbar]

{{< highlight javascript >}}

    <div class="w3-teal">
      <button class="w3-button w3-teal w3-xlarge"
              id="openNav">&#9776;</button>
      <div class="w3-container">
        <h1>My Page</h1>
      </div>
    </div>

    <div class="w3-bar w3-black">
    <% site.data.pages.forEach(function(page){ %>
      <a class="w3-bar-item w3-button"
         href="<%= page.link %>"><%= page.short %></a>
    <% }) %>
    </div>
{{< / highlight >}}

All you need to know is these lines:

{{< highlight javascript >}}
    <% site.data.pages.forEach(function(page){ %>
      <a class="w3-bar-item w3-button"
         href="<%= page.link %>"><%= page.short %></a>
    <% }) %>
{{< / highlight >}}

![Hexo: Loop in EJS][04-ejs-panes]

This will render a hyperlink,
with result about the same with previous example.

#### Running Hexo

Running `hexo` is as easy as this command below.

{{< highlight bash >}}
❯ hexo server
INFO  Start processing
INFO  Hexo is running at http://localhost:4000 . Press Ctrl+C to stop.
{{< / highlight >}}

![HTML: Hyperlink Example][04-hexo-hyperlink]

![Hexo: hexo server][04-hexo-serve]

You are ready to run a website,
with `hexo` SSG, using `EJS`.

-- -- --

### What's Next?

Consider continue reading [ [Template - Hugo - Template][local-whats-next] ].

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}frontend/2020/05/25/template-hugo-template/
[local-grunt-case]:         {{< baseurl >}}frontend/2020/05/07/template-grunt-ejs/

[04-hexo-hyperlink]:        {{< assets-frontend >}}/2020/05-ssg/04-hexo-hyperlink.png
[04-hexo-tree-example]:     {{< assets-frontend >}}/2020/05-ssg/04-hexo-ejs-tree-example.png
[04-hexo-serve]:            {{< assets-frontend >}}/2020/05-ssg/04-hexo-server.png
[04-ejs-panes]:             {{< assets-frontend >}}/2020/05-ssg/04-hexo-ejs-panes.png
[04-ejs-nerdtree]:          {{< assets-frontend >}}/2020/05-ssg/04-hexo-ejs-nerdtree.png

[source-example]:           {{< tutor-css-tools >}}/html-preprocessor/04-hexo/
[04-package-json]:          {{< tutor-css-tools >}}/html-preprocessor/04-hexo/package.json
[04-config-yml]:            {{< tutor-css-tools >}}/html-preprocessor/04-hexo/_config.yml
[04-pages-yml]:             {{< tutor-css-tools >}}/html-preprocessor/04-hexo/source/_data/pages.yml

[04-views-layout-layout]:   {{< tutor-css-tools >}}/html-preprocessor/04-hexo/themes/tutor/layout/layout.ejs
[04-views-site-head]:       {{< tutor-css-tools >}}/html-preprocessor/04-hexo/themes/tutor/layout/site/head.ejs
[04-views-site-navbar]:     {{< tutor-css-tools >}}/html-preprocessor/04-hexo/themes/tutor/layout/site/navbar.ejs
[04-views-site-sidebar]:    {{< tutor-css-tools >}}/html-preprocessor/04-hexo/themes/tutor/layout/site/sidebar.ejs
