---
type   : post
title  : "Template - Hugo - Template"
date   : 2020-05-25T09:17:35+07:00
slug   : template-hugo-template
categories: [ssg]
tags      : [go, hugo]
keywords  : [template]
author : epsi
opengraph:
  image: assets-frontend/2020/05-ssg/04-hugo-hyperlink.png

toc    : "toc-2020-05-template"

excerpt:
  Feeding Data in Hugo, and Process in a Loop with Go html/template.

---

### Preface

> Feeding Data in Hugo, and Process in a Loop with Go html/template.

Again, consider continue have a look at templating engine
that we never use before.
We are going to use `go html/template` example,
using SSG (static site generator),
called `hugo`.

#### Official Documentation

* [golang.org/pkg/html/template/](https://golang.org/pkg/html/template/)

* [gohugo.io/documentation/](https://gohugo.io/documentation/)

#### Source Examples

You can obtain source examples here:

* [html-preprocessor/04-hugo/][source-example]

-- -- --

### Go html/template Case

The `Go html/template` case is the same,
with the one utilizing `grunt` in previous article
with a few differences.

* `hugo` configuration

* Using data to provide hyperlink in both `navbar` and `sidebar`.

#### Directory Structure

Consider an `hugo` structure with `Go html/template` views.

{{< highlight bash >}}
❯ tree
.
├── config.toml
├── content
│   ├── css.md
│   ├── html.md
│   ├── _index.md
│   ├── javascript.md
│   └── php.md
├── resources
│   └── _gen
│       ├── assets
│       └── images
├── static
│   ├── css
│   │   └── w3.css
│   ├── favicon.ico
│   └── js
│       └── script.js
└── themes
    └── tutor
        ├── archetypes
        │   └── default.md
        ├── layouts
        │   ├── _default
        │   │   ├── baseof.html
        │   │   ├── list.html
        │   │   └── single.html
        │   ├── index.html
        │   └── partials
        │       ├── head.html
        │       ├── navbar.html
        │       └── sidebar.html
        ├── LICENSE
        └── theme.toml

14 directories, 19 files
{{< / highlight >}}

_._

![Hugo: Example: Tree][04-hugo-tree-example]

#### config.toml

You also need to a write a configuration in `_config.yml`.

* [gitlab.com/.../config.toml][04-config-toml]

{{< highlight toml >}}
# An example of Hugo site using Bulma for personal learning purpose.

baseURL      = "http://example/"
languageCode = "en-us"
title        = "Yet Another Example."
theme        = "tutor"

{{< / highlight >}}

Global configuration is a must have in SSG world.

#### Hyperlink Data

In `hugo`, data can be express in `yaml`, `toml` or `json`.

To provide hyperlink in both `navbar` and `sidebar`,
we need to setup the data in `pages.yml` as below:

* [gitlab.com/.../source/_data/pages.yml][04-pages-yml].

{{< highlight yaml >}}
# Helper for related links

- link : "/"
  short: "Home"
  long : "Home"

- link : "/html.html"
  short: "HTML"
  long : "HTML Link"

- link : "/css.html"
  short: "CSS"
  long : "CSS Link"

- link : "/php.html"
  short: "PHP"
  long : "PHP Link"

- link : "/javascript.html"
  short: "Javascript"
  long : "Javascript Link"
{{< / highlight >}}

The `sidebar` will use `long` text,
and the `navbar` will use `short` one.

#### Router

> Set automatically with SSG

#### Layout

* [html-preprocessor/.../layouts/_default/baseof.html][04-views-layouts-baseof]

{{< highlight jinja >}}
<!DOCTYPE html>
<html lang="en">

<head>
  {{- partial "head.html" . -}}
</head>
<body>
  {{- partial "sidebar.html" . -}}

  <div id="main">
    {{- partial "navbar.html" . -}}

    <div class="w3-container">
    {{ .Content }}
    </div>
  </div>

</body>
</html>
{{< / highlight >}}

As shown in order we have three `partial` artefacts.

* `partial head.html`,
* `partial sidebar.html`,
* `partial navbar.html`,

And one eleventy specific tag `content`.

{{< highlight jinja >}}
    <div class="w3-container">
    {{ .Content }}
    </div>
{{< / highlight >}}

![Nunjucks: ViM NERDTree Panes][04-template-nerdtree]

#### Head

It is a little bit different with the original head.

* [html-preprocessor/.../themes/tutor/layouts/partials/head.html][04-views-site-head]

{{< highlight javascript >}}
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>{{ .Page.Title | default "Link Example - Default" }}</title>

  <link rel="stylesheet" type="text/css"
        href="{{ "css/w3.css" | relURL }}">
  <script src="{{ "js/script.js" | relURL }}"></script>
{{< / highlight >}}

Here we put the default title if we cannot find `title` variable.

{{< highlight javascript >}}
<title>{{ .Page.Title | default "Link Example - Default" }}</title>
{{< / highlight >}}

#### Loop in Template

> Very similar with previous example

This time, consider alter a bit,
both the `sidebar` and the `navbar`,
so we can render each data in loop.

* [html-preprocessor/.../themes/tutor/layouts/partials/sidebar.html][04-views-site-sidebar]

{{< highlight javascript >}}

  <div class="w3-sidebar w3-bar-block w3-card w3-animate-left" 
       style="display:none" id="mySidebar">
    <button class="w3-bar-item w3-button w3-large"
       id="closeNav" >Close &times;</button>
    {{ range .Site.Data.pages }}
      <a class="w3-bar-item w3-button"
         href="{{ .link }}">{{ .long }}</a>
    {{ end }}
  </div>
{{< / highlight >}}

* [html-preprocessor/.../themes/tutor/layouts/partials/navbar.html][04-views-site-navbar]

{{< highlight javascript >}}

    <div class="w3-teal">
      <button class="w3-button w3-teal w3-xlarge"
              id="openNav">&#9776;</button>
      <div class="w3-container">
        <h1>My Page</h1>
      </div>
    </div>

    <div class="w3-bar w3-black">
    {{ range .Site.Data.pages }}
      <a class="w3-bar-item w3-button"
         href="{{ .link }}">{{ .short }}</a>
    {{ end }}
    </div>
{{< / highlight >}}

All you need to know is these lines:

{{< highlight javascript >}}
    {{ range .Site.Data.pages }}
      <a class="w3-bar-item w3-button"
         href="{{ .link }}">{{ .short }}</a>
    {{ end }}
{{< / highlight >}}

![Hugo: Loop in Go html/template][04-template-panes]

This will render a hyperlink,
with result about the same with previous example.

#### Running Hugo

Running `hugo` is as easy as this command below.

{{< highlight bash >}}
$ hugo server
{{< / highlight >}}

![HTML: Hyperlink Example][04-hugo-hyperlink]

{{< highlight bash >}}
hugo server --disableFastRender

                   | EN  
-------------------+-----
  Pages            | 10  
  Paginator pages  |  0  
  Non-page files   |  0  
  Static files     |  3  
  Processed images |  0  
  Aliases          |  0  
  Sitemaps         |  1  
  Cleaned          |  0  

Built in 22 ms
Watching for changes in /home/epsi/Documents/html-preprocessor/04-hugo/{content,data,static,themes}
Watching for config changes in /home/epsi/Documents/html-preprocessor/04-hugo/config.toml
Environment: "development"
Serving pages from memory
Web Server is available at http://localhost:1313/ (bind address 127.0.0.1)
Press Ctrl+C to stop
{{< / highlight >}}

![Hugo: hugo server][04-hugo-server]

You are ready to run a website,
with `hugo` SSG, using `Go html/template`.

-- -- --

### What's Next?

To be concluded, not anytime soon.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}frontend/2020/05/25/template-hugo-template/
[local-grunt-case]:         {{< baseurl >}}frontend/2020/05/07/template-grunt-ejs/

[04-hugo-hyperlink]:        {{< assets-frontend >}}/2020/05-ssg/04-hugo-hyperlink.png
[04-hugo-tree-example]:     {{< assets-frontend >}}/2020/05-ssg/04-hugo-template-tree-example.png
[04-hugo-server]:           {{< assets-frontend >}}/2020/05-ssg/04-hugo-server.png
[04-template-panes]:        {{< assets-frontend >}}/2020/05-ssg/04-hugo-template-panes.png
[04-template-nerdtree]:     {{< assets-frontend >}}/2020/05-ssg/04-hugo-template-nerdtree.png

[source-example]:           {{< tutor-css-tools >}}/html-preprocessor/04-hugo/
[04-config-toml]:           {{< tutor-css-tools >}}/html-preprocessor/04-hugo/config.toml
[04-pages-yml]:             {{< tutor-css-tools >}}/html-preprocessor/04-hugo/source/_data/pages.yml

[04-views-layouts-baseof]:  {{< tutor-css-tools >}}/html-preprocessor/04-hugo/themes/tutor/layouts/_default/baseof.html
[04-views-site-head]:       {{< tutor-css-tools >}}/html-preprocessor/04-hugo/themes/tutor/layout/partials/head.html
[04-views-site-navbar]:     {{< tutor-css-tools >}}/html-preprocessor/04-hugo/themes/tutor/layout/partials/navbar.html
[04-views-site-sidebar]:    {{< tutor-css-tools >}}/html-preprocessor/04-hugo/themes/tutor/layout/partials/sidebar.html
