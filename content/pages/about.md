+++
title      = "About Epsi"
rssExclude = true
numerator  = true
progressbar= true
+++
 
Hello, my name is Epsi. I'm an open source enthusiast.
I have made a ready to use, Hugo site customization.

{{< social-accounts >}}

	There are so many things to say.
	I don't want to live in regrets.
	So I make this blog.

{{< articles-count >}}

	It's not that I need to share.
	I just want to back it up to cloud.

{{< barchart-count >}}

Although I like command line, I'm not a hacker.
You won't find any illegal activities in this site.

	I know nothing. Nothing at all.

-- -- --

I am a mechanical engineer.
Neither software engineer, nor financial engineer.

![adulthood][welder]

[//]: <> ( -- -- -- links below -- -- -- )

[image-13]:   {{< baseurl >}}assets/site/images/authors/epsi-13.jpg
[welder]:     {{< baseurl >}}assets/site/images/authors/tukang-las.jpg

