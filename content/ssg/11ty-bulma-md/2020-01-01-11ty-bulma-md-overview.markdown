---
type   : post
title  : "Eleventy - Bulma MD - Overview"
date   : 2020-01-01T09:17:35+07:00
slug   : 11ty-bulma-md-overview
categories: [ssg]
tags      : [11ty]
keywords  : [static site, custom theme, summary, overview]
author : epsi
opengraph:
  image: assets-ssg/2020/03/11ty-bulma-md-preview.png

toc    : "toc-2020-01-11ty-bulma-md-step"

excerpt:
  Building Eleventy Site Step by step,
  with Bulma as stylesheet frontend,
  featuring Material Design.

---

### Preface

> Goal: Explanation of Eleventy Repository Step By Step

#### Eleventy Bulma Test Drive

An example of Eleventy site using Bulma for personal learning purpose.

> 11ty + Nunjucks + Bulma + Custom SASS

![Eleventy Bulma MD: Preview][11ty-bulma-md-preview]

Here is the summary of Eleventy with Bulma CSS Framework,
using `Nunjucks` template.

I'm using Bulma, enhanced with custom SASS containing

* Custom Helper Spacing

* Material Design

#### A Journey with SSG

> The need to move on

On december 2019 I finally decided to setup my fourth blog.
Now I have four blog, each with different static site generator.

1. on github: still with Jekyll

2. on gitlab: Hugo

3. on netlify using github: Hexo

4. on bitbucket: Pelican

I have already made two article series:
Hugo, then Hugo again, then Hexo.
I have three more SSG repositories,
that haven't been made as an article series:
Eleventy, Pelican, and Jekyll.
Rewrite the Hugo or Hexo article series,
to other SSG shouldn't be hard.

I eager to learn something new such as:
CSS without framework, Tailwind CSS, Gatsby, and also RedwoodJS.
Before I move on to this new knowledge frontier,
I need to share my old knowledge to article series.

![What is SSG Good for?][what-ssg-good-for]

#### A Journey with Eleventy

> My favorite SSG

`Eleventy` means freedom,
you can extend blog with just javascript right away.
And `eleventy` can works with many kind of templating engine.

`Eleventy` with `nunjucks` is my favorite SSG.
Nunjucks is great with template inheritance,
so I do not need to repeat myself,
and make my code efficiently elegant.
My attempt to use `pug` in `eleventy` is failed miserably,
so I decide to stay with `nunjucks` anyway.
In fact, my experience with `nunjuck` comes with great satisfaction.

The thing that I haven't test with eleventy is,
real life blog performance with hundred articles.
I still don't know if it is slow or fast enough in production.

However, here a I am, for you folks,
I can't wait to write down,
my eleventy experience,
as an article series.

#### Prerequisite

I use `Bulma MD` for this guidance.
The CSS part in this article series is
based on that `Bulma MD` article series.
Of course you can adapt this to any CSS framework that you like,
such as <code>Materialize</code> or <code>Bootstrap</code>.

I suggest you to visit this article,
before you dive in to this long `eleventy` article series.

* [Bulma MD - Overview][local-bulma-md-overview]

-- -- --

### About Eleventy

#### Official Site.

* [11ty.dev](https://www.11ty.dev/)

#### Why Static Site Generator?

No need to go deep learning backend.
So you can mock-up your theme quickly.

#### Why Markdown?

Most SSG is using markdown, instead of HTML.
Markdown reduce the complexity of formatting.
Reduce time wasted on formatting.
Hence concentrate more on content.

#### Why Eleventy?

> I do not know.

My first decision to learn `11ty` is simple,
on of my friends also use `11ty`.
And he looks cool with that.

#### Why Nunjucks?

> Javascript is very common

Yes, it use plain javascript.
Thus, it is easier to use than Liquid in Jekyll, or Chroma in Hugo.
This `nunjucks` support,
make `eleventy` **sexier** than `Hugo` or `Jekyll`.

The same reason does with `Jinja2` with Pelican.
You can simply extend it with yout own python script.

`Nunjucks` is the javascript port of `Jinja2`.

#### The Downside of Eleventy

> Don't worry!

Eleventy configuration is harder to setup,
compared with other SSG. It is the cost of its flexibility.
Eleventy would not just run without proper configuration,
but once you get it running, you will be just fine.

Most SSG case that I made require one step for starting up the website.
But in `eleventy` case I must make three steps,
just to show html without css.
But don't worry, I have already make this three steps for you,
so you can walk through each steps easily.

Worst than that,
there is very few tutorial available for eleventy.
But again, don't worry.
I have already made a ready to use example,
on how to setup eleventy site.
So you don't need deal with the complexity,
of start everything from the scratch.

#### Demo Eleventy

Unfortunately, I do not create Demo site with Bulma.
All I have is this `materialize` version of this tutorial.

* [11ty: Demo][11ty-mat]

-- -- --

### The Article Series

#### Making a Theme

Supposed you want to make your own theme.
What would you do first?

* Step One: **Start From the Layout**:
  Although you can create the layout with pure HTML.
  Static site generator can make this job easier.

* Step Two: **Adding Stylesheet**:
  General design require Sass, for nice looks.
  And carefully crafted responsive layout.

* Step Three: **Custom Pages**:
  You need to populate with content example.
  From post and page, to archive and taxonomy.
  This require a lot of loop, that we can achieve using Nunjucks.
  And Sass for finishing touch.
  Each chapter will modify **layout**.

* Step Four: **Additional Feature**
  Such as static data, widget, or making pagination.

* Step Five: **Page Content**:
  Adding layout such as page header, footer, and navigation.
  Also adding feature, such as meta SEO, and commenting system.
  Then a few Sass adjustment to handle rendered markdown content in post.
  For coder, you need to test syntax highlighting.
  As a bonus tips, math lover should embrace MathJax.

* Additionaly: **Deploy**:
  This is a huge topic that deserve thier own article series.
  Deploy is discussed in separated article along with other SSG.

These article series cover most of those five main topics above.
The stylesheet design covered in different article series.
And additional stuff as beyond making site,
such as deployment covered in different article series.

#### Expectation

> No. Not at all.

* **Case can be applied somewhere else**.
  I do not expect most coder to use Eleventy.
  In fact, I love diversity, and static site world support diversities.
  I provide this eleventy case, just for an example.
  After this you might want to apply methods covered here,
  somewhere else with different static site generator.

* **Beginner have an example case**.
  With this case, at least beginner can make their own blog easier.
  For either portfolio, or maybe personal blog,
  or even making company profile for family business.

* **Starting point for a serious writer**.
  I also do not want to go political,
  that every coder should share their knowledge,
  and pour code into blog.
  I just feel that maybe there is someone out there,
  need to learn to share something,
  and need to start with blogging or youtubing.
  Static site generator is,
  a starting point for serious writer.
  After all, making a blog site is easy and fun.

#### Theming Skill

> Should I make custom theme?

Even when you are using ready use theme,
you might still need customization to suit your needs.
This adjustment is a reccuring issue that arise regularly.

I do not ask for every reader to be capable of making custom theme,
nor encourage people to avoid third party theme.
Let the design part be somebody else's job.

This article series give you understanding,
on how to modify existing theme,
to alter a bit to match your requirement.

#### What is Not?

This is yet, just another static site tutorial, for beginner.
Nothing super duper here. No database, no login, no API, no animation.

After writing four hundreds of basic articles for,
now I feels like __mediocre specialist__.
I have to move on someday soon.

#### Disclaimer

This is would not be the best blog template that you ever have.
Because I only put most common stuff,
to keep the tutorial simple.

After you learn this guidance,
you understand the fundamental skill.
Thus, a base for you, to make your own blog site.
With your imagination, you may continue,
to build your own super duper `Eleventy` site.
Far better than, what I have achieved.

#### Content

A site need content.
I do not want __lorem ipsum__.

To make this step by step tutorial alive,
I choose most common topic, quote from movies.

I put some lyrics from great beautiful songs.
I'm not sure exactly what these songs means.
I just find it easier to use this kind of content.

Without the content, you can still make your site anyway.
You do not really need to understand the story behind the content.

#### Table of Content

The table content is available as header on each tutorial.
There, I present a Hexo Tutorial, step by step, for beginners.

-- -- --

### Links

#### Hugo Step By Step

This repository:

* [Eleventy Step by Step Repository][tutorial-11ty]

#### Frankenbulma (Bulma Material Design) Step By Step

Additional guidance:

* [Bulma Material Design Step by Step Repository][tutorial-bulma-md]

[tutorial-11ty]:    https://gitlab.com/epsi-rns/tutor-11ty-bulma-md/
[tutorial-bulma-md]:https://gitlab.com/epsi-rns/tutor-html-bulma-md/

-- -- --

### Chapter Step by Step

#### Tutor 01

> Running Eleventy at The First Time

* Configuration: Minimal

![Eleventy Bulma MD: Tutor 01][11ty-bulma-md-preview-01]

#### Tutor 02

> Generate Only Pure HTML

* Configuration: Basic

* Setup Directory for Minimal Eleventy

* Basic Layout: Base, Page, Post

* Partials: Site Wide: HTML Head, Header, Footer

* Basic Content: Page and Post

* Nunjucks: Basic Loop

![Eleventy Bulma MD: Tutor 02][11ty-bulma-md-preview-02]

-- -- --

#### Tutor 03

> Common Layout (without stylesheet)

* Configuration: Alias, Filter, Collection

* Additional Layout: Home, Archive, Index

* Additional Layout: Tags List and Generate Each Tag Name

* Basic View: Index, about

* Metadata

* Nunjucks: Tag Loop

![Eleventy Bulma MD: Tutor 03][11ty-bulma-md-preview-03]

-- -- --

#### Tutor 04

> Add Pure Bulma CSS Framework

* Layout Inheritance (simple): columns-double

* Configuration: Move main configuration to above.

* Standard Header (jquery or vue) and Footer

* Enhance All Layouts with Bulma CSS

* Apply Two Column Responsive Layout for Most Layout

* Nunjucks: More Blocks

![Eleventy Bulma MD: Tutor 04][11ty-bulma-md-preview-04]

-- -- --

#### Tutor 05

> Add Custom SASS (Custom Design)

* Layout Inheritance (with variables): columns-double

* Configuration: Nothing is changed.

* SASS: Bulma : Alter Navbar

* SASS: Spacing Helper, Google Material Color

* Nice Header and Footer

* Apply Google Material Pallete for All Layout

* Class: main-wrapper, aside-wrapper, blog-header, blog-body

* Nice Tag Badge in Tags Page

* Nunjucks: Nice post index using bulma inside loop

![Eleventy Bulma MD: Tutor 05][11ty-bulma-md-preview-05]

-- -- --

#### Tutor 06

> Refactoring with Nunjucks

* Layout Inheritance: columns-double

* Nunjucks: Simplified All Layout Using Template Inheritance

* Partials: Index: Blog List, Tags, By Year, By Month

> Configuration

* Filter: mapdate, groupBy

* Collection: posts

* Shortcodes: excerpt

> Features

* More Content: Lyrics and Quotes. Need this content for demo

* Custom Output: JSON

> Decoration on Views

* View: Archive Index: Simple, By Year, By Month Tree.

* View: Tags Index: List Tree

* View: Nice Landing Page

* Content: Excerpt (<! --more-- >): Read More Separator

![Eleventy Bulma MD: Tutor 06][11ty-bulma-md-preview-06]

-- -- --

#### Tutor 07

> Features

* Blog Pagination: Adjacent, Indicator, Responsive

* Design: Apply Box Design, for Tag, and Archive

* Widget: Friends, Archive Tree, Categories, Tags, Recent Post, Related Post

> Refactoring with Nunjucks

* Layout Inheritance: single-double

* Partials: Page (blog-header), Post (blog-header), Widget (template)

* Nunjucks: Color using Template Inheritance

> Configuration

* Filter: limit, values, keys, shuffle, hashIt, pagerIt

* Collection: postsPrevNext

![Eleventy Bulma MD: Tutor 07][11ty-bulma-md-preview-07]

-- -- --

#### Tutor 08

> Optional Feature

* Design: Apply Multi Column Responsive List, for Tag, and Archive

* Post: Header, Footer, Navigation

* Search: lunr.js

> Configuration

* Filter: keyJoin

* Some Shortcodes

> Blog Content

* Post: Markdown Content

* Post: Table of Content

* Post: Responsive Images

* Post: Shortcodes

* Post: Mathjax

* Post: Syntax Highlight: Bulma CSS Reset and Add Prism CSS

* Stylesheet: Bulma Title: CSS Fix

> Finishing

* Layout: Service

* Meta: HTML, SEO, Opengraph, Twitter

* RSS: Filter to Show Only Post Kind

* Metadata: Complete Author Info

![Eleventy Bulma MD: Tutor 08][11ty-bulma-md-preview-08]

-- -- --

### Begin The Guidance

Let's get the tutorial started.
Long and short of it, this is the tale.

Consider continue reading [ [Eleventy - Minimal][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/01/02/11ty-minimal/

[repository]:   https://gitlab.com/epsi-rns/tutor-11ty-bulma-md

[coloring-material]:{{< baseurl >}}assets/posts/design/2020/09-impress/coloring-candyclone.png
[what-ssg-good-for]:{{< assets-ssg >}}/2020/what-ssg-good-for.png

[11ty-bulma-md-preview]:   https://gitlab.com/epsi-rns/tutor-11ty-bulma-md/raw/master/11ty-bulma-md-preview.png
[11ty-bulma-md-preview-01]:https://gitlab.com/epsi-rns/tutor-11ty-bulma-md/raw/master/tutor-01/11ty-bulma-md-preview.png
[11ty-bulma-md-preview-02]:https://gitlab.com/epsi-rns/tutor-11ty-bulma-md/raw/master/tutor-02/11ty-bulma-md-preview.png
[11ty-bulma-md-preview-03]:https://gitlab.com/epsi-rns/tutor-11ty-bulma-md/raw/master/tutor-03/11ty-bulma-md-preview.png
[11ty-bulma-md-preview-04]:https://gitlab.com/epsi-rns/tutor-11ty-bulma-md/raw/master/tutor-04/11ty-bulma-md-preview.png
[11ty-bulma-md-preview-05]:https://gitlab.com/epsi-rns/tutor-11ty-bulma-md/raw/master/tutor-05/11ty-bulma-md-preview.png
[11ty-bulma-md-preview-06]:https://gitlab.com/epsi-rns/tutor-11ty-bulma-md/raw/master/tutor-06/11ty-bulma-md-preview.png
[11ty-bulma-md-preview-07]:https://gitlab.com/epsi-rns/tutor-11ty-bulma-md/raw/master/tutor-07/11ty-bulma-md-preview.png
[11ty-bulma-md-preview-08]:https://gitlab.com/epsi-rns/tutor-11ty-bulma-md/raw/master/tutor-08/11ty-bulma-md-preview.png

[local-bulma-md-overview]: {{< baseurl >}}frontend/2019/06/11/bulma-md-overview/
[11ty-mat]: https://eleventy-step.netlify.app/
