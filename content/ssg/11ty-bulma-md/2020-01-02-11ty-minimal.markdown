---
type   : post
title  : "Eleventy - Minimal"
date   : 2020-01-02T09:17:35+07:00
slug   : 11ty-minimal
categories: [ssg]
tags      : [11ty]
keywords  : [install, npm, run server, minimal, port]
author : epsi
opengraph:
  image: assets-ssg/2020/01/01-11ty-vim-config.png

toc    : "toc-2020-01-11ty-bulma-md-step"

excerpt:
  Building 11ty Site Step by step, with Bulma MD as stylesheet frontend.
  Show Minimal Homepage in 11ty.

---

### Preface

> Goal: Show Minimal Homepage in Eleventy.

The first three chapter use only `html` without any stylesheet.
So you have understanding of eleventy, 
without the burden of learning stylesheet in the same time.

#### Source Code

This article use [tutor-01][tutor-html-master-01] theme.
We will create it step by step.

#### Layout Preview for Tutor 01

![Nunjucks: Layout Preview for Tutor 01][template-inheritance-01]

[template-inheritance-01]:  {{< assets-ssg >}}/2020/02/01-template-inheritance.png

-- -- --

### 1: Install Eleventy

`Eleventy` run under NodeJS.
It means you can have `eleventy` in most OS, such as Windows and Linux.

#### Global

{{< highlight bash >}}
$ npm i -g @11ty/eleventy
{{< / highlight >}}

![11ty: Install: Global][image-ss-01-i-global]

Now you can:

{{< highlight bash >}}
$ eleventy --version
0.11.0
{{< / highlight >}}

![11ty: Version][image-ss-01-version]

#### package.json

For daily development you can use `package.json` instead:

* [gitlab.com/.../package.json][tutor-package-json]

{{< highlight javascript >}}
{
  ...
  "devDependencies": {
    "@11ty/eleventy": "^0.11.0"
  }
}
{{< / highlight >}}

You should do this first

{{< highlight bash >}}
$ npm install
{{< / highlight >}}

![11ty: Install: package.json][image-ss-01-i-package]

#### Nunjucks

> Where is Nunjucks?

Why don't I include `nunjucks` in `package.json`?

Because `nunjucks` is already in `eleventy` package:

* node_modules/@11ty/eleventy/node_modules/nunjucks

-- -- --

### 2: Minimal Configuration

Due to `eleventy` flexibility,
there is no such thing as `eleventy init project`.
You have to do it yourself.

#### Directory Tree: Minimal Artefacts

Consider create new Eleventy site manually.

{{< highlight bash >}}
$ tree
.
├── .eleventy.js
├── package.json
└── views
    ├── _includes
    │   └── layouts
    │       └── home.njk
    └── index.html
{{< / highlight >}}

![11ty: Tree: Views only][image-ss-01-tree-views]

__.__

You need this configuration artefact:

* [gitlab.com/.../.eleventy.js][tutor-configuration]

You need these two artefacts:

* [gitlab.com/.../views/index.html][tutor-contents-index]

* [gitlab.com/.../views/_includes/layouts/home.njk][tutor-includes-home]

Note the dot in `.eleventy.js`.

#### .eleventy.js

First thing first: The configuration
Start from the very simple one.

* [gitlab.com/.../.eleventy.js][tutor-configuration]

{{< highlight javascript >}}
module.exports = function(eleventyConfig) {

  // Return your Config object
  return {
    // Templating Engine
    templateFormats: [
      "njk",
      "html"
    ],

    // Directory Management
    dir: {
      // default
      input: "views",
      output: "_site",
      // ⚠️ This value is relative to your input directory.
      includes: "_includes",
    }
  };
};
{{< / highlight >}}

![11ty: A Very Simple Configuration][image-ss-01-config]

If you do not understand about `11ty` configuration,
always refer to official documentation.

* [www.11ty.dev/docs/config](https://www.11ty.dev/docs/config/)

#### Directory management

Consider have a look at a part of this config.

{{< highlight javascript >}}
    // Directory Management
    dir: {
      // default
      input: "views",
      output: "_site",
      // ⚠️ This value is relative to your input directory.
      includes: "_includes",
    }
{{< / highlight >}}

In directory tree manner, this could be represent as below:

{{< highlight bash >}}
$ tree -a
.
├── _site
└── views
    └── _includes
{{< / highlight >}}

__.__

-- -- --

### Minimal Views

Consider create our very first index page.

* Page: index.html

* Template: home.njk

#### Page: index.html

* [gitlab.com/.../views/index.html][tutor-contents-index]

{{< highlight html >}}
---
layout    : layouts/home
---

  <p>To have, to hold, to love,
  cherish, honor, and protect?</p>
  
  <p>To shield from terrors known and unknown?
  To lie, to deceive?</p>

  <p>To live a double life,
  to fail to prevent her abduction,
  erase her identity, 
  force her into hiding,
  take away all she has known.</p>
{{< / highlight >}}

Note I in the frontmatter.
That I want to pass the content into `layouts/home.njk`.

{{< highlight html >}}
---
layout    : layouts/home
---
{{< / highlight >}}

I use the `yaml` frontmatter.
But you may use other format as well.

* [www.11ty.dev/docs/data-frontmatter](https://www.11ty.dev/docs/data-frontmatter/)

#### Layout: home.njk

* [gitlab.com/.../views/_includes/layouts/home.njk][tutor-includes-home]

{{< highlight html >}}
<html>
<head>
  <title>Your mission. Good Luck!</title>
</head>
<body>
  <blockquote>
    <i>Your mission, should you decide to accept it.</i>
  </blockquote>

  <main role="main">
    <strong>This is a page kind layout.</strong>
    <br/>

    {{ content | safe }}
  </main>

  <blockquote>
    <i>As always, should you be caught or killed,
    any knowledge of your actions will be disavowed.</i>
  </blockquote>
</body>
</html>
{{< / highlight >}}

Pay attention to `nunjucks` statement here:

{{< highlight jinja >}}
    {{ content | safe }}
{{< / highlight >}}

![11ty: NERDTree Nunjucks][image-ss-01-nunjucks]

#### Build

Now that the artefact are ready,
you can build eleventy site.

{{< highlight bash >}}
$ eleventy
Writing _site/index.html from ./views/index.html.
Wrote 1 file in 0.51 seconds (v0.11.0)
{{< / highlight >}}

![11ty: Build Site][image-ss-01-build-site]

__.__

#### Directory Tree: With Build

Consider revisit our directory tree.
This time with generated `_site`:

{{< highlight bash >}}
$ tree -a
.
├── .eleventy.js
├── package.json
├── _site
│   └── index.html
└── views
    ├── _includes
    │   └── layouts
    │       └── home.njk
    └── index.html

4 directories, 5 files
{{< / highlight >}}

![11ty: Tree with Build][image-ss-01-tree-build]

_._

-- -- --

### 4: Running for the First Time

You can also run `eleventy` right away,
and get the result instantly in web browser using `browsersync`.

{{< highlight bash >}}
$ eleventy --serve
Writing _site/index.html from ./views/index.html.
Wrote 1 file in 0.44 seconds (v0.11.0)
Watching…
[Browsersync] Access URLs:
 ---------------------------------------
       Local: http://localhost:8080
    External: http://192.168.43.133:8080
 ---------------------------------------
          UI: http://localhost:3001
 UI External: http://localhost:3001
 ---------------------------------------
[Browsersync] Serving files from: _site
{{< / highlight >}}

![11ty: eleventy --serve, with browsersync][image-ss-01-serve]

__.__

#### Browser Preview

Just check it out in you favorite browser.

* <http://localhost:8080>

![11ty: Preview in Browser][image-ss-01-browser]

#### Multiple Eleventy

It is common to work with multiple Eleventy at one time,
for one reason or another.
You can achieve it by passing browsersync parameter argument.

{{< highlight bash >}}
$ eleventy --serve --port=5000
Writing _site/index.html from ./views/index.html.
Wrote 1 file in 0.32 seconds (v0.11.0)
Watching…
[Browsersync] Access URLs:
 ---------------------------------------
       Local: http://localhost:5000
    External: http://192.168.43.133:5000
 ---------------------------------------
          UI: http://localhost:3001
 UI External: http://localhost:3001
 ---------------------------------------
[Browsersync] Serving files from: _site
{{< / highlight >}}

![11ty: eleventy --serve --port=5000][image-ss-01-port]

Now you are ready to explore inside Eleventy.
__.__

-- -- --

### What is Next ?

Consider continue reading [ [Eleventy - Layout][local-whats-next] ].
We are going to make theme from scratch,
with pure HTML without any CSS.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/01/03/11ty-layout/

[tutor-html-master-01]: {{< tutor-11ty-bulma-md >}}/tutor-01/

[tutor-package-json]:   {{< tutor-11ty-bulma-md >}}/tutor-01/package.json
[tutor-configuration]:  {{< tutor-11ty-bulma-md >}}/tutor-01/.eleventy.js
[tutor-contents-index]: {{< tutor-11ty-bulma-md >}}/tutor-01/views/index.html
[tutor-includes-home]:  {{< tutor-11ty-bulma-md >}}/tutor-01/views/_includes/layouts/home.njk

[image-ss-01-i-global]:     {{< assets-ssg >}}/2020/01/01-npm-install-11ty-global.png
[image-ss-01-i-package]:    {{< assets-ssg >}}/2020/01/01-npm-install-11ty-package.png
[image-ss-01-version]:      {{< assets-ssg >}}/2020/01/01-11ty-version.png
[image-ss-01-config]:       {{< assets-ssg >}}/2020/01/01-11ty-vim-config.png
[image-ss-01-tree-views]:   {{< assets-ssg >}}/2020/01/01-11ty-tree-views.png
[image-ss-01-tree-build]:   {{< assets-ssg >}}/2020/01/01-11ty-tree-build.png
[image-ss-01-build-site]:   {{< assets-ssg >}}/2020/01/01-11ty-build-site.png
[image-ss-01-serve]:        {{< assets-ssg >}}/2020/01/01-11ty-serve.png
[image-ss-01-browser]:      {{< assets-ssg >}}/2020/01/01-11ty-browser.png
[image-ss-01-port]:         {{< assets-ssg >}}/2020/01/01-11ty-port.png
[image-ss-01-nunjucks]:     {{< assets-ssg >}}/2020/01/01-nerdtree-nunjucks.png
