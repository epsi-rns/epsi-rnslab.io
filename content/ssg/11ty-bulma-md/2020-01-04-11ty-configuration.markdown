---
type   : post
title  : "Eleventy - Configuration"
date   : 2020-01-04T09:17:35+07:00
slug   : 11ty-configuration
categories: [ssg]
tags      : [11ty]
keywords  : [configuration, layout, nunjucks]
author : epsi
opengraph:
  image: assets-ssg/2020/01/01-11ty-vim-config.png

toc    : "toc-2020-01-11ty-bulma-md-step"

excerpt:
  Building 11ty Site Step by step, with Bulma MD as stylesheet frontend.
  Configure most common settings.

---

### Preface

> Goal: Configure most common settings.

This chapter will introduce common basic configuration,
that required to make a simple site.

#### Source Code

This article use [tutor-03][tutor-html-master-03] theme.
We will create it step by step.

#### Layout Preview for Tutor 03

![Nunjucks: Layout Preview for Tutor 03][template-inheritance-03]

[template-inheritance-03]:  {{< assets-ssg >}}/2020/02/03-template-inheritance.png

-- -- --

### 1: Layout Alias

#### Issue

Remember this frontmatter below:

{{< highlight markdown >}}
---
layout    : layouts/post
title     : Every Day
---
{{< / highlight >}}

For a real world site that grow to hundred of pages,
it would be nice if we can write the layout,
without worry about which directory this layout comes from.

#### .eleventy.js

We can utilize `addLayoutAlias` in configuration.

* [gitlab.com/.../.eleventy.js][tutor-configuration]

{{< highlight javascript >}}
module.exports = function(eleventyConfig) {
  ...

  // Layout Alias
  eleventyConfig.addLayoutAlias("home",     "layouts/home.njk");
  eleventyConfig.addLayoutAlias("page",     "layouts/page.njk");
  eleventyConfig.addLayoutAlias("post",     "layouts/post.njk");
  eleventyConfig.addLayoutAlias("archive",  "layouts/archive.njk");
  eleventyConfig.addLayoutAlias("tags",     "layouts/tags.njk");
  eleventyConfig.addLayoutAlias("tag-name", "layouts/tag-name.njk");
  
  ...
};
{{< / highlight >}}

![11ty: Layout Alias in Eleventy Configuration][image-ss-03-vim-config]

#### Layout as Page Kind

Now we can rewrite the layout shorter.

{{< highlight markdown >}}
---
layout    : post
title     : Every Day
---
{{< / highlight >}}

This way, we can rename layout directory and change the configuration,
without touching any post content.

It is also easier to remember layout as page kind.
We can say that this content wear `post` layout.

-- -- --

### 2: Data: Title

As common in SSG, eleventy also suppport data.

#### .eleventy.js

We have to change the configuration a bit.

* [gitlab.com/.../.eleventy.js][tutor-configuration]

{{< highlight javascript >}}
module.exports = function(eleventyConfig) {
  // Return your Config object
  return {
    ...

    // Directory Management
    dir: {
      input: "views",
      output: "dist",
      // ⚠️ These values are both relative to your input directory.
      includes: "_includes",
      data: "_data"
    }
  };
};
{{< / highlight >}}

#### Data: Introducing Metadata

> Separate Data from Configuration

Most common data is, of course the site metadata,
such as default site title and so on.

* [gitlab.com/.../_data/metadata.js][tutor-metadata-js]

{{< highlight javascript >}}
module.exports = {
  "title": "Your mission. Good Luck!"
};
{{< / highlight >}}

Why would I need another configuration file?
Well, configuration tends to grow.
At first it makes sense to put,
the whole thing in configuration file,
just like any other SSG does.
After sometimes, you have a bunch of metadata,
and it is got harder to differ which one is configuration,
and which one is custom user data.
So here it is, from the very start,
we put all data somewhere elese outside the `.eleventy.js`.

#### Layout: Nunjucks Head

Now you can change your old `head.njk`

{{< highlight jinja >}}
  <title>{{ title or "Your mission. Good Luck!" }}</title>
{{< / highlight >}}

To this below

* [gitlab.com/.../views/_includes/site/head.njk][tutor-vi-s-head]

{{< highlight jinja >}}
  <title>{{ title or metadata.title }}</title>
{{< / highlight >}}

-- -- --

### 3: Assets: Pass Through Copy

We all love static assets,
from stylesheet, font, javascript, and favicon.

#### .eleventy.js

`Eleventy` copy static assets,
to output directory such as `dist` or `_site`
using this configuration:

* [gitlab.com/.../.eleventy.js][tutor-configuration]

{{< highlight javascript >}}
module.exports = function(eleventyConfig) {

  // Directory Management
  eleventyConfig.addPassthroughCopy("assets");

  ...

  // Return your Config object
  return {
    ...

    // Directory Management
    passthroughFileCopy: true,    
    dir: {
      ...
      output: "dist",
      ...
    }
  };
};
{{< / highlight >}}

You can read in `11ty` official documentation.

#### What's New Here

The first line contain which directory will copied.

{{< highlight javascript >}}
  eleventyConfig.addPassthroughCopy("assets");
{{< / highlight >}}

The second line, is just a flag,
whether you enable it, or not.

{{< highlight javascript >}}
    passthroughFileCopy: true,
{{< / highlight >}}

#### Asset Example

Now you can check in directory tree,
where the `favicon` copied.

{{< highlight bash >}}
.
├── assets
│   └── favicon.ico
├── dist
│   ├── assets
│   │   └── favicon.ico
│   ├── index.html
│   └── pages
│       ├── about
│       │   └── index.html
│       └── index.html
└── views
{{< / highlight >}}

#### Partial: Nunjucks Head

Now you can add favicon in `head.njk`.

* [gitlab.com/.../views/_includes/site/head.njk][tutor-vi-s-head]

{{< highlight jinja >}}
  <title>{{ title or metadata.title }}</title>
  <link href="/assets/favicon.ico"
        rel="shortcut icon" type="image/x-icon" />
{{< / highlight >}}

-- -- --

### 4: Filter: MomentJS

Don't you want to show proper date in your post?
There are some javascript libraries capable to do this.
One of them is `moment` library.

#### Official Site

* [momentjs.com](https://momentjs.com/)

#### Reference

This `eleventy filter` is verbatim copy of code from this article.

* [From Jekyll to Eleventy](https://www.webstoemp.com/blog/from-jekyll-to-eleventy/)

#### package.json

You should install the `moment` library first.

* [gitlab.com/.../package.json][tutor-package-json]

{{< highlight javascript >}}
  "devDependencies": {
    "@11ty/eleventy": "^0.10.0",
    "moment": "^2.24.0"
  }
{{< / highlight >}}

#### .eleventy.js

Consider create a filter named `date` using `addNunjucksFilter`.

* [gitlab.com/.../.eleventy.js][tutor-configuration]

{{< highlight javascript >}}
const moment = require("moment");

module.exports = function(eleventyConfig) {
  ...
  
  // Copy paste from Jérôme Coupé
  eleventyConfig.addNunjucksFilter("date", function(date, format) {
    return moment(date).format(format);
  });

  ...
};
{{< / highlight >}}

#### Using Date Filter

Now you can use `date` filter in post layout:

{{< highlight jinja >}}
{% extends "layouts/base.njk" %}

{% block main %}
  <h2>{{ title or metadata.title }}</h2>
  <pre>{{ page.date | date('MMMM Do, YYYY') }}</pre>
  <br/>

  <strong>This is a post kind layout.</strong>
  <br/>

  {{ content | safe }}
{% endblock %}
{{< / highlight >}}

-- -- --

### 5: URL: Post Permalink

Furthermore we can utilize this `moment js` to manage post permalink.

#### Reference

* [www.11ty.dev/docs/permalinks](https://www.11ty.dev/docs/permalinks/)

#### Page Content: every-day.md

* [gitlab.com/.../views/posts/every-day.md][tutor-vc-po-everyday]

{{< highlight markdown >}}
---
layout    : post
title     : Every Day
date      : 2015-10-03 08:08:15
slug      : every-day
permalink : "/{{ date | date('YYYY/MM/DD') }}/{{ slug }}/index.html"
---

I've been thinking about this
over and over and over.
<!-- more --> 

I mean, really, truly,
imagine it.
{{< / highlight >}}

Have a look at this line in frontmatter:

{{< highlight yaml >}}
permalink : "/{{ date | date('YYYY/MM/DD') }}/{{ slug }}/index.html"
{{< / highlight >}}

This will be rendered as:

* `dist/2015/10/03/every-day/index.html`

#### .eleventy.js

Since you content is written in `markdown` format,
you need to enable `nunjucks` in configuration.

* [gitlab.com/.../.eleventy.js][tutor-configuration]

{{< highlight javascript >}}
    markdownTemplateEngine: "njk",
    htmlTemplateEngine: "njk",
    dataTemplateEngine: false,
{{< / highlight >}}

#### Directory Tree: Output

Check and recheck, as always.

{{< highlight bash >}}
$ tree dist
dist
├── 2015
│   └── 10
│       └── 03
│           └── every-day
│               └── index.html
├── assets
│   └── favicon.ico
├── index.html
└── pages
    ├── about
    │   └── index.html
    └── index.html

7 directories, 5 files
{{< / highlight >}}

![11ty: Tree: dist][image-ss-03-tree-dist]

-- -- --

### 6: Config Object

The style of writing configration is different for each coder.
I'd rather separate stuff, such as Config object.
So instead of writing like this below:

{{< highlight javascript >}}
module.exports = function(eleventyConfig) {

  ...

  // Return your Config object
  return {
    // URL Related
    pathPrefix: "/",
    ...
  };
};
{{< / highlight >}}

I write the config as below:

{{< highlight javascript >}}
// Put main config above
// to avoid distraction from complex configuration
const config = {
  // URL Related
  pathPrefix: "/",
  ...
};

module.exports = function(eleventyConfig) {

  ...

  // Return your Config object
  return config;
};
{{< / highlight >}}

![11ty: Configuration Object in .eleventy.js][image-ss-03-config-object]

I like to put the main configuration on top.
Because the other part tends to get longer as the site grow.
I will use this style, starting in the next configuration.

-- -- --

### What is Next ?

Consider continue reading [ [Eleventy - Collections][local-whats-next] ].
Still minimalist with pure HTML without any stylesheet.
We are going to go deep, examine `collections` in `eleventy`.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/01/05/11ty-collections/

[tutor-html-master-03]: {{< tutor-11ty-bulma-md >}}/tutor-03/

[tutor-package-json]:   {{< tutor-11ty-bulma-md >}}/tutor-03/package.json
[tutor-configuration]:  {{< tutor-11ty-bulma-md >}}/tutor-03/.eleventy.js
[tutor-metadata-js]:    {{< tutor-11ty-bulma-md >}}/tutor-03/views/_data/metadata.js
[tutor-vc-po-everyday]: {{< tutor-11ty-bulma-md >}}/tutor-03/views/posts/every-day.md

[tutor-vi-s-head]:      {{< tutor-11ty-bulma-md >}}/tutor-03/views/_includes/site/head.njk

[image-ss-03-tree-dist]:    {{< assets-ssg >}}/2020/01/03-tree-dist.png
[image-ss-03-config-object]:{{< assets-ssg >}}/2020/01/03-config-object.png
[image-ss-03-vim-config]:   {{< assets-ssg >}}/2020/01/03-vim-configuration.png
