---
type   : post
title  : "Eleventy - Collections"
date   : 2020-01-05T09:17:35+07:00
slug   : 11ty-collections
categories: [ssg]
tags      : [11ty]
keywords  : [template engine, nunjucks, collections, tags, pagination]
author : epsi
opengraph:
  image: assets-ssg/2020/01/03-vim-songs-tags.png

toc    : "toc-2020-01-11ty-bulma-md-step"

excerpt:
  Building 11ty Site Step by step, with Bulma MD as stylesheet frontend.
  Apply Tag Collection based on Zach's Code.

---

### Preface

> Goal: Apply Tag Collection based on Zach's Code.

#### Source Code

This article use [tutor-03][tutor-html-master-03] theme.
We will create it step by step.

-- -- --

### 1: About Collections

> Freedom

Just like most SSG, `eleventy` can show article index,
by chronology, or by tags.
Those content can be queried, filtered, and finally shown,
as the site author decide to.
Eleventy arrange this content list with `collections`.

Eleventy comes with built in `tag` collection.
This way, `eleventy` can create tag based list manually.
Although first this looks thougher than most common SSG,
this eleventy approach also free you from SSG limitation.
Because you can freely make your own kind of collections

#### Official Documentation

* [www.11ty.dev/docs/collections](https://www.11ty.dev/docs/collections/)

#### Reference

* [Zero Maintenance Tag Pages for your Blog](https://www.11ty.dev/docs/quicktips/tag-pages/)

* [11ty / eleventy-base-blog](https://github.com/11ty/eleventy-base-blog)

-- -- --

### 2: Archive

Before we go on into custom collections,
consider refresh our memory to our simple default collections.

#### Reference

* [www.11ty.dev/docs/collections](https://www.11ty.dev/docs/collections/)

#### Page Content: pages/archive

* [gitlab.com/.../views/pages/archive.html][tutor-vc-pa-archive]

{{< highlight jinja >}}
---
layout    : archive
title     : Archive
permalink : /pages/
eleventyExcludeFromCollections: true
---

  <ul>
    {%- for post in collections.all | reverse -%}
    <li>
      <a href="{{ post.url | url }}">
        {{ post.data.title }}</a>
    </li>
    {%- endfor -%}
  </ul>
{{< / highlight >}}

This time this content wears `archive` layout.

#### Layout: Nunjucks Archive

* [gitlab.com/.../views/_includes/layouts/archive.njk][tutor-vl-archive]

Template inheritance in `nunjucks`,
start with the word `extends`.

{{< highlight jinja >}}
{% extends "layouts/base.njk" %}

{% block main %}
  <h2>{{ title or metadata.title }}</h2>
  <strong>This is an archive kind layout.</strong>
  <br/>

  {{ content | safe }}
{% endblock %}
{{< / highlight >}}

#### Render: Browser

Now you can see the result in the browser.

![11ty: Page Content: pages/archive][image-vc-pa-archive]

#### The Loop

The loop is as simple as all collections,
but shown backward:

{{< highlight jinja >}}
    {% for post in collections.all | reverse %}
      ...
    {% endfor %}
{{< / highlight >}}

The word `reverse` here is builtin filter from `nunjucks`.

* [Templating#reverse](https://mozilla.github.io/nunjucks/templating.html#reverse)

-- -- --

### 3: Keys in Collections

#### The Riddle?

> What other keys are inside collections?

So what is this `collections.all` after all?
Or let me rephrase with better question.
What's inside these collections?

#### .eleventy.js

To answer this we need other other `filter` in `.eleventy.js`.

{{< highlight jinja >}}
  // values Filter: MDN web docs
  eleventyConfig.addNunjucksFilter("keys", function(array) {
    return Object.keys(array);
  });
{{< / highlight >}}

#### Page Content: pages/keys

And apply to views

* [gitlab.com/.../views/pages/keys.html][tutor-vc-pa-keys]

{{< highlight jinja >}}
  <p>collections: [{{ collections | keys | join(", ") }}]</p>
{{< / highlight >}}

#### Render: Browser

Now you can see the result in the browser.

{{< highlight yaml >}}
collections: [all, subtitle, story, tagList]
{{< / highlight >}}

It turned out that `collections` has four members

* all (default)

* subtitle (tag)

* story (tag)

* tagList (manual collections)

![11ty: Page Content: pages/keys][image-vc-pa-keys]

#### Manual

We have this `tagList` from manual `collection` in `.eleventy.js`.

{{< highlight javascript >}}
  // Copy paste from Zach
  eleventyConfig.addCollection("tagList",
    require("./views/_11ty/getTagList"));
{{< / highlight >}}

#### Tags

And we have these `subtitle` and `story` from frontmatter.

* [gitlab.com/.../views/posts/every-day.md][tutor-vc-po-everyday]

{{< highlight yaml >}}
layout    : post
title     : Every Day
date      : 2015-10-03 08:08:15
slug      : every-day
tags      : ['subtitle', 'story']
{{< / highlight >}}
-- -- --

### 4: Set (ECMAScript 2015)

Before we get down to generate each tag page,
consider this simple javascript.

#### Reference

* [developer.mozilla.org/.../Set](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set)

#### The Songs Script

This script utilize `Set` introduced in `ECMAScript 2015`,
to collect tag names.
And convert the the `set` output back simple `array`.

* [gitlab.com/.../_11ty/songs.js][get-tag-example-songs]

{{< highlight javascript >}}
songs  = [
  { title: "Cantaloupe Island",          tags: ["60s", "jazz"] },
  { title: "Let it Be",                  tags: ["60s", "rock"] },
  { title: "Knockin' on Heaven's Door",  tags: ["70s", "rock"] },
  { title: "Emotion",                    tags: ["70s", "pop"] },
  { title: "The River" }
];

let tagSet = new Set();

// using set feature to collect tag names
songs.forEach(function(song) {
  if( "tags" in song ) {   
    let tags = song.tags;
    console.log(tags);

    for (const tag of tags) {
      tagSet.add(tag);
    }
  }
});

console.log(tagSet);

// normalize to array
let alltags = [...tagSet];

console.log(alltags);
{{< / highlight >}}

![11ty: Set in ECMA2015][image-vim-songs-tags]

#### Running in CLI

Run this script, and you will get

{{< highlight javascript >}}
$ node songs.js
[ '60s', 'jazz' ]
[ '60s', 'rock' ]
[ '70s', 'rock' ]
[ '70s', 'pop' ]
Set(5) { '60s', 'jazz', 'rock', '70s', 'pop' }
[ '60s', 'jazz', 'rock', '70s', 'pop' ]
{{< / highlight >}}

This way, you can apply to `11ty` tags in frontmatter.

#### Page Content: every-day.md

* [gitlab.com/.../views/posts/every-day.md][tutor-vc-po-everyday]

{{< highlight markdown >}}
---
layout    : post
title     : Every Day
date      : 2015-10-03 08:08:15
slug      : every-day
tags      : ['subtitle', 'story']
---

I've been thinking about this
over and over and over.
<!-- more --> 

I mean, really, truly,
imagine it.
{{< / highlight >}}

Have a look at this line in frontmatter:

{{< highlight yaml >}}
tags      : ['subtitle', 'story']
{{< / highlight >}}

These `tags` in frontmatter will be,
compiled internally by `11ty` into `collections`.
So later, you can access directly as below:

{{< highlight javascript >}}
  {% set postslist = collections[ tag ] %}
{{< / highlight >}}

-- -- --

### 5: Collections: getTagList.js

Consider apply those script above to eleventy.

#### .eleventy.js

We are going to add this script manually as a `collections`.

* [gitlab.com/.../.eleventy.js][tutor-configuration]

{{< highlight javascript >}}
  // Copy paste from Zach
  eleventyConfig.addCollection("tagList",
    require("./views/_11ty/getTagList"));
{{< / highlight >}}

#### Complete Script

You can use complete script from here:

* [eleventy-base-blog/.../getTagList.js][get-tag-list-original]

#### Simpler Script

For explanation purpose, I stripped down the script.

The underscore in `_11ty`, means this folder is not a website content,
and won't be rendered. Very useful for utility folder.

* [gitlab.com/.../_11ty/getTagList.js][get-tag-list-simpler]

{{< highlight javascript >}}
module.exports = function(collection) {
  let tagSet = new Set();

  collection.getAll().forEach(function(item) {
    if( "tags" in item.data ) {
      let tags = item.data.tags;

      for (const tag of tags) {
        tagSet.add(tag);
      }
    }
  });

  return [...tagSet];
};
{{< / highlight >}}

![11ty: ViM getTagList.js][image-vim-gettaglist]

This script is very similar to previous `CLI` script above.

-- -- --

### 6: Loop: All Tags

With the result of above `tagList` collections,
we can just show the tag,
just like we display the index page.

### Page Content: tags.html

* [gitlab.com/.../views/tags.html][tutor-vc-pa-tags]

{{< highlight javascript >}}
---
layout    : tags
title     : List of Tags
eleventyExcludeFromCollections: true
---

  <ul>
    {% for tag in collections.tagList %}
    <li>
      {% set tagUrl %}/tags/{{ tag }}/{% endset %}
      <a href="{{ tagUrl | url }}" class="tag">{{ tag }}</a>
    </li>
    {%- endfor -%}
  </ul>
{{< / highlight >}}

#### Layout: Nunjucks Tags

Nothing special yet about this layout.

* [gitlab.com/.../views/_includes/layouts/tags.njk][tutor-vl-tags]

Template inheritance in `nunjucks`,
start with the word `extends`.

{{< highlight jinja >}}
{% extends "layouts/base.njk" %}

{% block main %}
  <h2>{{ title or metadata.title }}</h2>
  <strong>This is a tags kind layout.</strong>
  <br/>

  {{ content | safe }}
{% endblock %}
{{< / highlight >}}

#### Render: Browser

Now you can see the result in the browser.

![11ty: Page Content: tags][image-vc-pa-tags]

#### The Loop

The loop is as simple as all collections,
but shown backward:

{{< highlight jinja >}}
    {% for tag in collections.tagList %}
      ...
    {% endfor %}
{{< / highlight >}}

We use manually crafted `taglist` collection.

-- -- --

### 7: Pagination: Tag Name

The last page is a little bit hard,
because this page utilize pagination.
And pagination in eleventy have weird approach.

This page will generate many pages.
One page for each tag name.

#### Reference

> A brilliant approach.

* [www.11ty.dev/docs/pagination](https://www.11ty.dev/docs/pagination/)

#### Paginated Content: tag-name.html

Pagination also can be used to manage `tag` names.

* [gitlab.com/.../views/tag-name.html][tutor-vc-pa-tag-name]

{{< highlight javascript >}}
---
layout    : tag-name
eleventyExcludeFromCollections: true

pagination:
  data: collections
  size: 1
  alias: tag
  filter: tagList

permalink: /tags/{{ tag }}/
---
{{< / highlight >}}

#### Directory Tree

This arrangement will automatically generate tag pages as below:

![11ty: Tree of Tag Names][image-ss-03-tree-tags]

Notice there is also `all` tags.

#### Filter

I add `filter: tagList`,
to avoid `tagList/index.html` comes out in the tree.

-- -- --

### 8: Render Data

Beside `permalink`, we can render any data, with `pagination`.
For example we can change the title to reflect current tag.

{{< highlight javascript >}}
---
...
permalink: /tags/{{ tag }}/

renderData:
  title: Tagged “{{ tag }}”
---
{{< / highlight >}}

We can use this render data in layout.

#### Layout: Nunjucks Tag Name

Beware of the difference.
This layout use `renderData.title`.

* [gitlab.com/.../views/_includes/layouts/tag-name.njk][tutor-vl-tag-name]

{{< highlight jinja >}}
{% extends "layouts/base.njk" %}

{% block main %}
  <h2>{{ renderData.title or title or metadata.title }}</h2>
  <strong>This is a tag-name kind layout.</strong>
  <br/>

  {{ content | safe }}
{% endblock %}
{{< / highlight >}}

#### Layout: Nunjucks Head

Also use it in head.

* [gitlab.com/.../views/_includes/site/head.njk][tutor-vi-s-head]

{{< highlight jinja >}}
  <title>{{ renderData.title or title or metadata.title }}</title>
  <link href="/assets/favicon.ico"
        rel="shortcut icon" type="image/x-icon" />
{{< / highlight >}}

-- -- --

### 9: Using Collections Object

We still have to output the post for each tag.

#### Reference

The code is almost verbatim copy, from this link below.

* [Zero Maintenance Tag Pages for your Blog](https://www.11ty.dev/docs/quicktips/tag-pages/)

### Paginated Content: tag-name.html

* [gitlab.com/.../views/tag-name.html][tutor-vc-pa-tag-name]

{{< highlight javascript >}}
---
layout    : tag-name
eleventyExcludeFromCollections: true

pagination:
  data: collections
  size: 1
  alias: tag

permalink: /tags/{{ tag }}/

renderData:
  title: Tagged “{{ tag }}”

---

  {% set postslist = collections[ tag ] %}
  <ul>
    {%- for post in postslist -%}
    <li>
      <a href="{{ post.url | url }}">
        {{ post.data.title }}</a>
    </li>
    {%- endfor -%}
  </ul>
  
  <p>List of <a href="{{ '/tags/' | url }}">all tags</a>.</p>
{{< / highlight >}}

![11ty: NERDTree of Tag Names with Source][image-ss-03-tag-name]

#### Render: Browser

Now you can see the result in the browser.

![11ty: Page Content: tag-name][image-vc-pa-tag-name]

#### The Loop

The loop is just about using collections.

{{< highlight jinja >}}
    {%- for post in collections[ tag ] -%}
       ...
    {%- endfor -%}
{{< / highlight >}}

> Wait?

Where is this `collections[ tag ]` comes from???

#### Explanation

The official `11ty` site said: 

	First up notice how we’re pointing our pagination
	to iterate over collections,
	which is an object keyed with tag names pointing
	to the collection of content containing that tag.

This will be clear in this reference:

* [Working with Collections](https://www.pborenstein.com/posts/collections/)

Allright, the hardest part is over.
The next chapter should be easy.

-- -- --

### What is Next ?

Consider continue reading [ [Eleventy - Bulma - CSS Intro][local-whats-next] ].
We need to revamp the looks of this good eleventy,
with proper stylesheet.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/01/07/11ty-bulma-css-intro/

[tutor-html-master-03]: {{< tutor-11ty-bulma-md >}}/tutor-03/

[get-tag-list-original]:https://github.com/11ty/eleventy-base-blog/blob/master/_11ty/getTagList.js
[get-tag-list-simpler]: {{< tutor-11ty-bulma-md >}}/tutor-03/views/_11ty/getTagList.js
[get-tag-example-songs]:{{< tutor-11ty-bulma-md >}}/tutor-03/views/_11ty/songs.js

[tutor-configuration]:  {{< tutor-11ty-bulma-md >}}/tutor-03/.eleventy.js
[tutor-vi-s-head]:      {{< tutor-11ty-bulma-md >}}/tutor-03/views/_includes/site/head.njk
[tutor-vl-archive]:     {{< tutor-11ty-bulma-md >}}/tutor-03/views/_includes/layouts/archive.njk
[tutor-vl-tags]:        {{< tutor-11ty-bulma-md >}}/tutor-03/views/_includes/layouts/tags.njk
[tutor-vl-tag-name]:    {{< tutor-11ty-bulma-md >}}/tutor-03/views/_includes/layouts/tag-name.njk
[tutor-vc-pa-archive]:  {{< tutor-11ty-bulma-md >}}/tutor-03/views/pages/archive.html
[tutor-vc-pa-keys]:     {{< tutor-11ty-bulma-md >}}/tutor-03/views/pages/keys.html
[tutor-vc-pa-tags]:     {{< tutor-11ty-bulma-md >}}/tutor-03/views/tags.html
[tutor-vc-pa-tag-name]: {{< tutor-11ty-bulma-md >}}/tutor-03/views/tag-name.html
[tutor-vc-po-everyday]: {{< tutor-11ty-bulma-md >}}/tutor-03/views/posts/every-day.md

[image-vim-songs-tags]: {{< assets-ssg >}}/2020/01/03-vim-songs-tags.png
[image-vim-gettaglist]: {{< assets-ssg >}}/2020/01/03-vim-gettaglist.png

[image-vc-pa-archive]:  {{< assets-ssg >}}/2020/01/03-pages-archive.png
[image-vc-pa-tags]:     {{< assets-ssg >}}/2020/01/03-pages-tags.png
[image-vc-pa-keys]:     {{< assets-ssg >}}/2020/01/03-pages-keys.png
[image-vc-pa-tag-name]:     {{< assets-ssg >}}/2020/01/03-pages-tag-name.png
[image-ss-03-tree-dist]:    {{< assets-ssg >}}/2020/01/03-tree-dist.png
[image-ss-03-tree-tags]:    {{< assets-ssg >}}/2020/01/03-tree-dist-tags.png
[image-ss-03-tag-name]:     {{< assets-ssg >}}/2020/01/03-nerdtree-tag-name.png
