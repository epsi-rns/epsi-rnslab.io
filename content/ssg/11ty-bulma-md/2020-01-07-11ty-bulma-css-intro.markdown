---
type   : post
title  : "Eleventy - Bulma - CSS Intro"
date   : 2020-01-07T09:17:35+07:00
slug   : 11ty-bulma-css-intro
categories: [ssg]
tags      : [11ty, bulma, css]
keywords  : [assets, nunjucks, layout, refactoring template]
author : epsi
opengraph:
  image: assets-ssg/2020/01/04-vim-base.png

toc    : "toc-2020-01-11ty-bulma-md-step"

excerpt:
  Building 11ty Site Step by step, with Bulma MD as stylesheet frontend.
  Revamp the looks, of current Eleventy layout, with Bulma Stylesheet.

---

### Preface

> Goal: Revamp the looks,
> of current Eleventy layout, with Bulma Stylesheet.

#### Source Code

This article use [tutor-04][tutor-html-master-04] theme.
We will create it step by step.

#### Not a CSS Tutorial

This article is not a CSS tutorial,
but rather than applying simple CSS theme,
into `Eleventy` layout.
This CSS theme is a based on a SASS theme,
that we are going to discuss later.

#### Layout Preview for Tutor 04

![Nunjucks: Layout Preview for Tutor 04][template-inheritance-04]

[template-inheritance-04]:  {{< assets-ssg >}}/2020/02/04-template-inheritance.png

-- -- --

### 1: About Bulma Stylesheet

> Use Stylesheet in Eleventy.

In previous articles,
we have already refactor pages into a few layouts.
All done without stylesheet,
so you have understanding of eleventy layout, 
without the burden of learning stylesheet.

#### The Choice

Now it is a good time to give this site a good looks.
There are many options for this such as:
`Bootstrap`, `Materialize CSS`, `Semantic UI`, `Bulma`,
or even better custom tailor made without CSS Frameworks,
or `Tailwind CSS`.

#### Prerequiste

> Use Bulma Stylesheet in Eleventy.

My choice comes to Bulma,
because I have already make a few articles about Bulma,
with a lot of details explanation:

* [Bulma - Overview][local-bulma]

This article rely on Bulma stylesheet on the article above.
You should give a fast reading of above Bulma article first,
before continue reading this article.

#### Responsive Design

Our base layout is based on responsive design.
This below is general preview of,
what responsive page that we want to achieve.

![11ty: General Preview of Responsive Page Design][image-png-layout-page]

Different website might have different preview.
Source image is available in inkscape SVG format,
so you can modify, and make your own preview quickly.

* [Responsive Page: Image Source][image-svg-layout-page]

This responsive design has already discussed in Bulma article series.

#### Related Article: CSS

I wrote about Bulma Navigation Bar that used in this article.
Step by step article, about building Navigation Bar.
You can read here:

* [Bulma - Navigation Bar][local-bulma-navbar]

* [Bulma SASS - Responsive Layout][local-bulma-layout]

#### Reference

* [bulma.io/documentation](https://bulma.io/documentation/)

-- -- --

### 2: Prepare: Assets

#### Directory Tree: Assets

The `main.css` assets is the only custom stylesheet.

{{< highlight bash >}}
$ tree assets
assets
├── css
│   ├── bulma.css
│   └── main.css
├── favicon.ico
├── images
│   ├── light-grey-terrazzo.png
│   └── logo-gear.png
└── js
    ├── bulma-burger-jquery.js
    ├── bulma-burger-plain.js
    ├── bulma-burger-vue.js
    ├── jquery-slim.min.js
    └── vue.min.js

3 directories, 10 files
{{< / highlight >}}

Additionaly, I add three different javascript choices.
`jquery`, `vue`, and `plain native vanilla`.
For simplicity reason, and also neutrality reason,
for the rest of the chapter, I use `plain` javascript.

![11ty: Tree: Assets][image-ss-04-tree-assets]

#### .eleventy.js

The assets management is set in configuration below.

* [gitlab.com/.../.eleventy.js][tutor-configuration]

{{< highlight javascript >}}
module.exports = function(eleventyConfig) {

  // Directory Management
  eleventyConfig.addPassthroughCopy("assets");

  ...

  // Return your Config object
  return {
    ...

    // Directory Management
    passthroughFileCopy: true,    
    dir: {
      ...
      output: "dist",
      ...
    }
  };
};
{{< / highlight >}}

You can read in `11ty` official documentation.

-- -- --

### 3: Layout: Refactoring Base

We should start over again from the very fundamental layout.

#### Layout: Nunjucks Base

* [gitlab.com/.../views/_includes/layouts/base.njk][tutor-vl-base]

{{< highlight jinja >}}
<!DOCTYPE html>
<html lang="en">

<head>
  {% include "site/head.njk" %}
</head>

<body>
  <!-- header -->
  {% include "site/header.njk" %}

  {% block general %}
  <!-- main -->
  <div class="columns is-8 layout-base maxwidth">
    {% block main %}
    {{ content | safe }}
    {% endblock %}

    {% block aside %}{% endblock %}
  </div>
  {% endblock %}

  <!-- footer -->
  {% include "site/footer.njk" %}
  {% include "site/scripts.njk" %}
</body>

</html>
{{< / highlight >}}

We have two `block`s here:

* `main`,

* `aside`.

And four `include`s:

* `head`,

* `header`,

* `footer`,

* `script` (new).

![11ty: Base Layout][image-ss-04-vim-base]

#### Partial: Nunjucks Head

* [gitlab.com/.../views/_includes/site/head.njk][tutor-vi-s-head]

There is a few additional changes here.
Now the header contain stylesheets and meta tag.

{{< highlight jinja >}}
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible"
        content="IE=edge,chrome=1">
  <!-- Let browser know website is optimized for mobile -->
  <meta name="viewport"
        content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>{{ renderData.title or title or metadata.title }}</title>

  <!-- CSS Framework -->
  <link type="text/css" rel="stylesheet"
        href="{{ "/assets/css/bulma.css" | url }}">
  <!-- Custom CSS -->
  <link type="text/css" rel="stylesheet"
        href="{{ "/assets/css/main.css" | url }}">

  <link rel="shortcut icon" type="image/x-icon" 
        href="{{ "/assets/favicon.ico" | url }}"/>
{{< / highlight >}}

#### Partial: Nunjucks Header

It is a long header, so I crop the code.
You can get the complete code in the repository.

* [gitlab.com/.../views/_includes/site/header.njk][tutor-vi-s-header]

{{< highlight html >}}
  <nav role="navigation" aria-label="main navigation"
       class="navbar is-fixed-top is-dark maxwidth"
       id="navbar-vue-app">
    <div class="navbar-brand">
      <a class="navbar-item"
         href="{{ "/" | url }}">
        <img src="{{ "/assets/images/logo-gear.png" | url }}" 
           alt="Home" />
      </a>
      <a class="navbar-item"
         href="{{ "/pages/" | url }}">
        Blog
      </a>

      <a role="button" ...>
        ...
      </a>
    </div>

    <div id="navbarBulma" class="navbar-menu"
         v-bind:class="{'is-active': isOpen}">
      ...
    </div>
  </nav>
{{< / highlight >}}

This is an example of header using `vue` .
You can switch to `jquery` or `plain` with example in repository.

#### Partial: Nunjucks Scripts

You can choose, which javascript to use:

* Vue

{{< highlight html >}}
  <!-- JavaScript at end of body for optimized loading -->
  <script src="{{ "/assets/js/vue.min.js" | url }}"></script>
  <script src="{{ "/assets/js/bulma-burger-vue.js" | url }}"></script>
{{< / highlight >}}

* jQuery

{{< highlight html >}}
  <!-- JavaScript at end of body for optimized loading -->
  <script src="{{ "/assets/js/jquery-slim.min.js" | url }}"></script>
  <script src="{{ "/assets/js/bulma-burger-jquery.js" | url }}"></script>
{{< / highlight >}}

* Plain Native Vanilla

{{< highlight html >}}
  <!-- JavaScript at end of body for optimized loading -->
  <script src="{{ "/assets/js/bulma-burger-plain.js" | url }}"></script>
{{< / highlight >}}

You can have the code here:

* [gitlab.com/.../views/_includes/site/scripts.njk][tutor-vi-s-scripts]

#### Layout: Nunjucks Footer

* [gitlab.com/.../views/_includes/site/footer.njk][tutor-vi-s-footer]

{{< highlight html >}}
  <footer class="site-footer">
    <div class="navbar is-fixed-bottom maxwidth
          is-dark has-text-centered is-vcentered">
      <div class="column">
        &copy; {{ metadata.now | date("Y") }}
      </div>
    </div>
  </footer>
{{< / highlight >}}

Wait....!!
what is this `metadata.now`?

#### Data: metadata.js

Most common data is, of course the site metadata,
such as default site title and so on.

* [gitlab.com/.../_data/metadata.js][tutor-metadata-js]

{{< highlight javascript >}}
module.exports = {
  "title": "Your mission. Good Luck!",
  now: new Date()
};
{{< / highlight >}}

I've got this good ide from **webstoemp**:

* [From Jekyll to Eleventy](https://www.webstoemp.com/blog/from-jekyll-to-eleventy/)

-- -- --

### 4: Page Content: Home

Consider use `home` layout` to begin,
and all other layout later.

#### Layout: Nunjucks Home

> This is a single column design.

* [gitlab.com/.../views/_includes/layouts/home.njk][tutor-vl-home]

Template inheritance in `nunjucks`,
start with the word `extends`.

{{< highlight jinja >}}
{% extends "layouts/base.njk" %}

{% block main %}
  <main role="main" 
        class="column is-full">
    <article class="blog-post box">
      <h2 class="title is-4"
        >{{ renderData.title or title or metadata.title }}</h2>

      {{ content | safe }}

      <div class="notification is-info">
        This is a home kind layout,
        to show landing page.
      </div>
    </article>
{% endblock %}
{{< / highlight >}}

There will be double columns design in other layout.
Both single column and double columns design,
`extends` the same base parent.

#### Page Content: index

It is a little bit different with previous `index.html`.
I utilize Bulma's `box` class.

* [gitlab.com/.../views/index.html][tutor-vc-index]

{{< highlight html >}}
---
layout    : home
eleventyExcludeFromCollections: true
---

  <div class="box">
    To have, to hold, to love,
    cherish, honor, and protect?</div>
  
  <div class="box">
    To shield from terrors known and unknown?
    To lie, to deceive?</div>

  <div class="box">
    To live a double life,
    to fail to prevent her abduction,
    erase her identity, 
    force her into hiding,
    take away all she has known.</div>
{{< / highlight >}}

#### Render: Browser

Open in your favorite browser.
You should see, a simple homepage, by now.

* <http://localhost:8080/>

Well, I cut the content a little to make this screenshot below:

![11ty: Page Content: Home][image-vc-pa-home]

Notice that this is a single column Bulma page.
The other page is double columns,
and deserve a its own explanation.

-- -- --

### What is Next ?

Consider continue reading [ [Eleventy - Bulma - CSS Layout][local-whats-next] ].
We are going to explore simple template inheritance in Nunjucks.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/01/08/11ty-bulma-css-layout/
[local-bulma]:          {{< baseurl >}}frontend/2019/03/01/bulma-overview/
[local-bulma-navbar]:   {{< baseurl >}}frontend/2019/03/03/bulma-navbar/
[local-bulma-layout]:   {{< baseurl >}}frontend/2019/03/07/bulma-sass-layout/

[tutor-html-master-04]: {{< tutor-11ty-bulma-md >}}/tutor-04/

[tutor-configuration]:  {{< tutor-11ty-bulma-md >}}/tutor-04/.eleventy.js
[tutor-metadata-js]:    {{< tutor-11ty-bulma-md >}}/tutor-04/views/_data/metadata.js
[tutor-vl-base]:        {{< tutor-11ty-bulma-md >}}/tutor-04/views/_includes/layouts/base.njk
[tutor-vl-home]:        {{< tutor-11ty-bulma-md >}}/tutor-04/views/_includes/layouts/home.njk
[tutor-vi-s-head]:      {{< tutor-11ty-bulma-md >}}/tutor-04/views/_includes/site/head.njk
[tutor-vi-s-header]:    {{< tutor-11ty-bulma-md >}}/tutor-04/views/_includes/site/header.njk
[tutor-vi-s-footer]:    {{< tutor-11ty-bulma-md >}}/tutor-04/views/_includes/site/footer.njk
[tutor-vi-s-scripts]:   {{< tutor-11ty-bulma-md >}}/tutor-04/views/_includes/site/scripts.njk
[tutor-vc-index]:       {{< tutor-11ty-bulma-md >}}/tutor-02/views/index.html

[image-ss-04-tree-assets]:  {{< assets-ssg >}}/2020/01/04-tree-assets.png
[image-ss-04-vim-base]:     {{< assets-ssg >}}/2020/01/04-vim-base.png

[image-vc-pa-home]:     {{< assets-ssg >}}/2020/01/04-pages-home.png

[image-png-layout-page]:    {{< baseurl >}}assets/posts/frontend/2019/12/layout-page.png
[image-svg-layout-page]:    {{< baseurl >}}assets/posts/frontend/2019/12/layout-page.svg
