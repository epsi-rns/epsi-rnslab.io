---
type   : post
title  : "Eleventy - Bulma - CSS Layout"
date   : 2020-01-08T09:17:35+07:00
slug   : 11ty-bulma-css-layout
categories: [ssg]
tags      : [11ty, bulma, css]
keywords  : [assets, nunjucks, layout, template inheritance, refactoring template]
author : epsi
opengraph:
  image: assets-ssg/2020/01/04-vim-columns-double.png

toc    : "toc-2020-01-11ty-bulma-md-step"

excerpt:
  Building 11ty Site Step by step, with Bulma MD as stylesheet frontend.
  Using simple template inheritance with Nunjucks in Eleventy Layout.

---

### Preface

> Goal: Using simple template inheritance with Nunjucks in Eleventy Layout.

#### Source Code

This article use [tutor-04][tutor-html-master-04] theme.
We will create it step by step.

#### Related Article: CSS

This article is not a CSS tutorial,
but rather than applying simple CSS theme,
into `eleventy` layout.

The CSS part of this article is,
already discussed in this SASS article below.
The only different is that I use plain CSS instead of SASS.

* [Bulma - SASS - Layout][local-bulma-layout]

-- -- --

### 5: Page Content: Page, Post

Our Page Kind, and Post Kind is simply,
a double columns version of previous article.

#### Layout: Nunjucks Page

* [gitlab.com/.../views/_includes/layouts/page.njk][tutor-vl-page]

{{< highlight jinja >}}
{% extends "layouts/base.njk" %}

{% block main %}
  <main role="main" 
        class="column is-two-thirds">
    <article class="blog-post box">
      <h2 class="title is-4"
        >{{ renderData.title or title or metadata.title }}</h2>
      {{ content | safe }}
    </article>
  </main>
{% endblock %}

{% block aside %}
  <aside class="column is-one-thirds">
    <div class="box">
      This is a page kind layout.
    </div>
  </aside>
{% endblock %}
{{< / highlight >}}

![11ty: Block extending Base Layout][image-ss-04-block-base]

#### Page Content: pages/about

No difference with previous chapter.

* [gitlab.com/.../views/pages/about.md][tutor-vc-pa-about]

{{< highlight markdown >}}
---
layout    : page
title     : About Rescue Mission
---
This was not a rescue mission!

Let me put to you like this.
If the secretary wanted me out of there,
then things are really bad out here.
{{< / highlight >}}

We can say that this content wear `page` layout.

#### Render: Browser

Open in your favorite browser.
You should see, a black and white about page, by now.

![11ty: Page Content: pages/about][image-vc-pa-about]

#### Layout: Nunjucks Post

* [gitlab.com/.../views/_includes/layouts/post.njk][tutor-vl-post]

{{< highlight jinja >}}
{% extends "layouts/base.njk" %}

{% block main %}
  <main role="main" 
        class="column is-two-thirds">
    <article class="blog-post box">
      <h2 class="title is-4"
        >{{ title or metadata.title }}</h2>
      <p><strong>{{ page.date | date('MMMM Do, YYYY') }}</strong></p>
      {{ content | safe }}
    </article>
  </main>
{% endblock %}

{% block aside %}
  <aside class="column is-one-thirds">
    <div class="box">
      This is a post kind layout.
    </div>
  </aside>
{% endblock %}
{{< / highlight >}}

#### Page Content: every-day.md

No difference with previous chapter.

* [gitlab.com/.../views/posts/every-day.md][tutor-vc-po-everyday]

{{< highlight markdown >}}
---
layout    : post
title     : Every Day
date      : 2015-10-03 08:08:15
slug      : every-day
tags      : ['subtitle', 'story']
permalink : "/{{ date | date('YYYY/MM/DD') }}/{{ slug }}/index.html"
---

I've been thinking about this
over and over and over.
<!-- more --> 

I mean, really, truly,
imagine it.
{{< / highlight >}}

#### Render: Browser

Open in your favorite browser.
You should see, a black and white post, by now.

![11ty: Post Content: posts/every-day][image-vc-po-everyday]

-- -- --

### 6: Template Inheritance: Double Columns

> Can you see the pattern?

We have very similar pages with the Bulma's double columns layout,
and we write down the same layout over and over again in each layout:
`post`, `page`, `archive`, `tag`, `tag-name`,
and who knows how this site will grow later.

Nunjucks is really powerful templating engine,
and capable to solve this situation very nicely.
We do not need to exhaustingly repeat ourselves.

#### Parent Layout: Nunjucks Columns Double

Consider create a parent layout for double columns layout.
This is basically the same with `page layout` above,
but with two new blocks,
that can be replaced later in any child template.

* [gitlab.com/.../views/_includes/layouts/columns-double.njk][tutor-vl-double]

{{< highlight jinja >}}
{% extends "layouts/base.njk" %}

{% block main %}
  <main role="main" 
        class="column is-two-thirds">
    <article class="blog-post box">
    {% block main_header %}
      <h2 class="title is-4"
        >{{ renderData.title or title or metadata.title }}</h2>
      {{ content | safe }}
    {% endblock %}
    </article>
  </main>
{% endblock %}

{% block aside %}
  <aside class="column is-one-thirds">
    <div class="box">
    {% block aside_body %}
      This is a default kind layout.
    {% endblock %}
    </div>
  </aside>
{% endblock %}
{{< / highlight >}}

We have two `block`s here:

* `main_header`,

* `aside_body`.

![11ty: Double Columns Layout][image-ss-04-vim-double]

Now we can rewrite `page` kind and `post` kind.

#### Layout: Nunjucks Page

{{< highlight jinja >}}
{% extends "layouts/columns-double.njk" %}

{% block aside_body %}
  This is a page kind layout.
{% endblock %}
{{< / highlight >}}

Notice that we do not need to replace `main_header`,
if we don't want to change the default content.

#### Layout: Nunjucks Post

{{< highlight jinja >}}
{% extends "layouts/columns-double.njk" %}

{% block main_header %}
  <h2 class="title is-4"
    >{{ renderData.title or title or metadata.title }}</h2>
  <p><strong>{{ page.date | date('MMMM Do, YYYY') }}</strong></p>
  {{ content | safe }}
{% endblock %}

{% block aside_body %}
  This is a page kind layout.
{% endblock %}
{{< / highlight >}}

As there is no need to write the same thing,
over and over again for each template,
now writing template is not exhausting anymore.

-- -- --

### 7: Page Content: Archive, Tag, Tag Name

With the method above,
we can rewrite the all other layouts as well.

#### Child Layout: Nunjucks Archive

Template inheritance in `nunjucks`,
start with the word `extends`.

* [gitlab.com/.../views/_includes/layouts/archive.njk][tutor-vl-archive]

{{< highlight jinja >}}
{% extends "layouts/columns-double.njk" %}

{% block aside_body %}
  This is an archive kind layout.
{% endblock %}
{{< / highlight >}}

![11ty: Multiple Template Inheritance][image-ss-04-inheritance]

#### Page Content: pages/archive

This content wear `archive` layout.
I only add `content` class.

* [gitlab.com/.../views/pages/archive.html][tutor-vc-pa-archive]

{{< highlight jinja >}}
---
layout    : archive
title     : Archive
permalink : /pages/
eleventyExcludeFromCollections: true
---

  <div class="content">
  <ul>
    {%- for post in collections.all | reverse -%}
    <li>
      <a href="{{ post.url | url }}">
        {{ post.data.title }}</a>
    </li>
    {%- endfor -%}
  </ul>
  </div>
{{< / highlight >}}

![11ty: Page Content: pages/archive][image-vc-pa-archive]

#### Child Layout: Nunjucks Tags

* [gitlab.com/.../views/_includes/layouts/tags.njk][tutor-vl-tags]

{{< highlight jinja >}}
{% extends "layouts/columns-double.njk" %}

{% block aside_body %}
  This is a tags kind layout.
{% endblock %}
{{< / highlight >}}

#### Page Content: Tags

I only add `content` class.
And Bulma's `tag` class in each link.

* [gitlab.com/.../views/tags.html][tutor-vc-pa-tags]

{{< highlight jinja >}}
---
layout    : tags
title     : List of Tags
eleventyExcludeFromCollections: true
---

  <div class="content">
  <ul>
    {% for tag in collections.tagList %}
    <li>
      {% set tagUrl %}/tags/{{ tag }}/{% endset %}
      <a href="{{ tagUrl | url }}" class="tag is-link">{{ tag }}</a>
    </li>
    {%- endfor -%}
  </ul>
  </div>
{{< / highlight >}}

![11ty: Page Content: tags][image-vc-pa-tags]

#### Child Layout: Nunjucks Tag Name

* [gitlab.com/.../views/_includes/layouts/tag-name.njk][tutor-vl-tag-name]

{{< highlight jinja >}}
{% extends "layouts/columns-double.njk" %}

{% block aside_body %}
  This is a tag-name kind layout.
{% endblock %}
{{< / highlight >}}

#### Page Content: Tag Name (paginated)

I only add `content` class.

* [gitlab.com/.../views/tag-name.html][tutor-vc-pa-tag-name]

{{< highlight jinja >}}
---
layout    : tag-name
eleventyExcludeFromCollections: true

pagination:
  data: collections
  size: 1
  alias: tag

permalink: /tags/{{ tag }}/

renderData:
  title: Tagged “{{ tag }}”

---
  <div class="content">
  {% set postslist = collections[ tag ] %}
  <ul>
    {%- for post in postslist -%}
    <li>
      <a href="{{ post.url | url }}">
        {{ post.data.title }}</a>
    </li>
    {%- endfor -%}
  </ul>
  </div>

  <p><a href="{{ '/tags/' | url }}"
        class="button is-small"
       >List of all tags</a></p>
{{< / highlight >}}

![11ty: Page Content: tag-name][image-vc-pa-tag-name]

As you can see there is almost nothing happened in each page content.
Most have been done within the `columns-double.njk` layout,
using default Bulma stylesheet feature.
It means, whenever changes happened,
you only need to rewrite the parent layout.

> Template inheritance is very useful in this case.

-- -- --

### What is Next ?

Just remember that what we have learned above is,
just a simple template inheritance.
The power of template inheritance is not finished here.
We are going to face variable passing between parent and child,
so that page behaviour can be changed in more flexible manners.

In order to do that, we require more powerful stylesheet,
with various looks possibility.
We need to explore SASS in the next article,
with a new theme called Bulma Material Design.

Consider continue reading [ [Eleventy - Bulma - SASS Intro][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/01/09/11ty-bulma-sass-intro/
[local-bulma-layout]:   {{< baseurl >}}frontend/2019/03/07/bulma-sass-layout/

[tutor-html-master-04]: {{< tutor-11ty-bulma-md >}}/tutor-04/

[tutor-vl-page]:        {{< tutor-11ty-bulma-md >}}/tutor-04/views/_includes/layouts/page.njk
[tutor-vl-post]:        {{< tutor-11ty-bulma-md >}}/tutor-04/views/_includes/layouts/post.njk
[tutor-vl-double]:      {{< tutor-11ty-bulma-md >}}/tutor-04/views/_includes/layouts/columns-double.njk
[tutor-vl-archive]:     {{< tutor-11ty-bulma-md >}}/tutor-04/views/_includes/layouts/archive.njk
[tutor-vl-tags]:        {{< tutor-11ty-bulma-md >}}/tutor-04/views/_includes/layouts/tags.njk
[tutor-vl-tag-name]:    {{< tutor-11ty-bulma-md >}}/tutor-04/views/_includes/layouts/tag-name.njk
[tutor-vc-pa-about]:    {{< tutor-11ty-bulma-md >}}/tutor-04/views/pages/about.md
[tutor-vc-pa-archive]:  {{< tutor-11ty-bulma-md >}}/tutor-04/views/pages/archive.html
[tutor-vc-pa-tags]:     {{< tutor-11ty-bulma-md >}}/tutor-04/views/tags.html
[tutor-vc-pa-tag-name]: {{< tutor-11ty-bulma-md >}}/tutor-04/views/tag-name.html
[tutor-vc-po-everyday]: {{< tutor-11ty-bulma-md >}}/tutor-04/views/posts/every-day.md

[image-vc-pa-about]:    {{< assets-ssg >}}/2020/01/04-pages-about.png
[image-vc-pa-archive]:  {{< assets-ssg >}}/2020/01/04-pages-archive.png
[image-vc-pa-tags]:     {{< assets-ssg >}}/2020/01/04-pages-tags.png
[image-vc-pa-tag-name]: {{< assets-ssg >}}/2020/01/04-pages-tag-name.png
[image-vc-po-everyday]: {{< assets-ssg >}}/2020/01/04-posts-everyday.png
[image-ss-04-vim-double]:   {{< assets-ssg >}}/2020/01/04-vim-columns-double.png
[image-ss-04-block-base]:   {{< assets-ssg >}}/2020/01/04-block-base.png
[image-ss-04-inheritance]:  {{< assets-ssg >}}/2020/01/04-template-inheritance.png
