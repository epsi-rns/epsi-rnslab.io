---
type   : post
title  : "Eleventy - Bulma - SASS Intro"
date   : 2020-01-09T09:17:35+07:00
slug   : 11ty-bulma-sass-intro
categories: [ssg]
tags      : [11ty, bulma, sass]
keywords  : [assets, nunjucks, layout, refactoring template, material colors]
author : epsi
opengraph:
  image: assets-ssg/2020/01/04-vim-base.png

toc    : "toc-2020-01-11ty-bulma-md-step"

excerpt:
  Building 11ty Site Step by step, with Bulma MD as stylesheet frontend.
  Apply SASS in Eleventy using Bulma Material Design.

---

### Preface

> Goal: Apply SASS in Eleventy using Bulma Material Design.

#### Source Code

This article use [tutor-05][tutor-html-master-05] theme.
We will create it step by step.

#### Not a SASS Tutorial

This article is not a SASS tutorial,
but rather than providing example of applying custom SASS theme,
into `eleventy` layout. We are going to use this custom SASS theme,
for the rest of this article series.

#### Layout Preview for Tutor 05

![Nunjucks: Layout Preview for Tutor 05][template-inheritance-05]

[template-inheritance-05]:  {{< assets-ssg >}}/2020/02/05-template-inheritance.png

-- -- --

### 1: About Bulma MD Stylesheet

> Use SASS in Eleventy.

This is basically just changing theme,
from using simple CSS theme to customizable SASS theme.
We have to start the previous chapter all over again,
with extra difficulties.

#### The Choice

There is always a place to enhance this site looks.
SASS is not the only CSS preprocessor.
There are many options for this such as:
`Stylus`, `Less`, or even `PostCSS` via `PreCSS`.
`Bulma` is using `SASS`, so `SASS` is our choice by now.

	Create necessary sass, as you need.
	However, still Bulma can save you,
	from writing a lot of CSS code.

#### Prerequiste

> Use Bulma MD in Eleventy.

This article rely on Bulma MD in article below.
You should give a fast reading of this Bulma MD article first,
before continue reading this article.

* [Bulma MD - Overview][local-bulma-md]

#### Reference

* [sass-lang.com](https://sass-lang.com/)

-- -- --

### 2: Prepare: Assets

With the right configuration, `Eleventy` can manage `SASS`.
This tutorial using conventinal way, running SASS from CLI.

#### Directory Tree: CSS

First thing to do is to create `sass` directory,
in the same level with `assets`.

{{< highlight bash >}}
❯ tree -d sass assets -L 1
sass
├── css
└── vendors
assets
├── css
├── images
└── js

5 directories
{{< / highlight >}}

Note that you can use other directory as well.
This is just an example.

![11ty: Tree: CSS][image-ss-05-tree-both]

[Bulma MD][local-bulma-md] has a bunch of SASS.
If you take a look closer of the `sass/css`,
it contains a few separated stylesheet,
prepared design to build a blog site.

{{< highlight bash >}}
❯ tree sass/css
sass/css
├── bulma
│   ├── _derived-variables.sass
│   └── _initial-variables.sass
├── bulma.sass
├── fontawesome.scss
├── helper
│   └── _spacing.sass
├── helper.scss
├── main
│   ├── _decoration.sass
│   ├── _layout-content.sass
│   ├── _layout-page.sass
│   └── _list.sass
├── main.sass
├── materialize
│   ├── _color-classes.scss
│   ├── _color-variables.scss
│   └── _shadow.scss
└── post
    └── _content.sass

5 directories, 15 files
{{< / highlight >}}

So expect, to face new custom class in example ahead.

![11ty: Tree: SASS][image-ss-05-tree-sass]

__.__

#### .eleventy.js

`Eleventy` can manage `SASS`.
But I skip this method choose the other way.

#### Running SASS manually in CLI

I'm using `dart-sass` and set the `~/.zshrc` or `~/.bashrc`.

{{< highlight bash >}}
alias dart-sass='/media/Works/bin/dart-sass/sass'
{{< / highlight >}}

Then run the `dart-sass`.

{{< highlight bash >}}
$ dart-sass --watch -I sass sass/css:assets/css
Compiled sass/css/fontawesome.scss to assets/css/fontawesome.css.
Compiled sass/css/helper.scss to assets/css/helper.css.
Compiled sass/css/bulma.sass to assets/css/bulma.css.
Compiled sass/css/main.sass to assets/css/main.css.
Sass is watching for changes. Press Ctrl-C to stop.
{{< / highlight >}}

![11ty: Dart SASS][image-ss-05-dart-sass]

This will compile into `assets/css` directory.

{{< highlight bash >}}
❯ tree assets/css
assets/css
├── bulma.css
├── bulma.css.map
├── fontawesome.css
├── fontawesome.css.map
├── helper.css
├── helper.css.map
├── main.css
└── main.css.map

0 directories, 8 files
{{< / highlight >}}

![11ty: Tree: CSS][image-ss-05-tree-css]

-- -- --

### 3: Layout: Refactoring Base

As we did with previous articles.
We should start over again from layout.

#### Layout: Nunjucks Base

* [gitlab.com/.../views/_includes/layouts/base.njk][tutor-vl-base]

No difference here. Just leave this `base` as it was.

#### Layout: Nunjucks Head

* [gitlab.com/.../views/_includes/site/head.njk][tutor-vi-s-head]

There is no changes here either.

#### Partial: Nunjucks Header

It is a long header, so I crop the code.
You can get the complete code in the repository.

* [gitlab.com/.../views/_includes/site/header.njk][tutor-vi-s-header]

{{< highlight html >}}
  <nav role="navigation" aria-label="main navigation"
       class="navbar is-fixed-top is-white maxwidth
              white z-depth-3 hoverable">
    <div class="navbar-brand">
      <a class="navbar-item"
         href="{{ "/" | url }}">
        <img src="{{ "/assets/images/logo-gear.png" | url }}" 
           alt="Home" />
      </a>
      <a class="navbar-item"
         href="{{ "/pages/" | url }}">
        Blog
      </a>

      <a role="button" ...>
        ...
      </a>
    </div>

    <div id="navbarBulma" class="navbar-menu">
      ...
    </div>
  </nav>
{{< / highlight >}}

This is an example of header using `vue` .
You can switch to `jquery` or `plain` with example in repository.

#### Partial: Nunjucks Scripts

I choose `plain native vanilla`.

* [gitlab.com/.../views/_includes/site/scripts.njk][tutor-vi-s-scripts]

{{< highlight html >}}
  <!-- Additional javaScript here -->
{{< / highlight >}}

And move the javascript to the head.

{{< highlight html >}}
  <script src="{{ "/assets/js/bulma-burger-plain.js" | url }}"></script>
{{< / highlight >}}

#### Partial: Nunjucks Footer

Like I said, we utilize new custom class.

* [gitlab.com/.../views/_includes/site/footer.njk][tutor-vi-s-footer]

{{< highlight html >}}
  <footer class="site-footer">
    <div class="navbar is-fixed-bottom maxwidth
          is-white has-text-centered is-vcentered
          white z-depth-3 hoverable">
      <div class="column">
        &copy; {{ metadata.now | date("Y") }}
      </div>
    </div>
  </footer>
{{< / highlight >}}

-- -- --

### 4: Page Content: Home

Consider use `home` layout` to begin,
and all other layout later.

#### Layout: Nunjucks Home

> This is a single column design.

There are a lot of changes in here.
Now the layout is using `main-wrapper`,
and inside `main-wrapper` has `blog-header` and `blog-body`.

* [gitlab.com/.../views/_includes/layouts/home.njk][tutor-vl-home]

{{< highlight jinja >}}
{% extends "layouts/base.njk" %}

{% block main %}
  <main role="main" 
        class="column is-full">

    <section class="main-wrapper blue-grey">
      <div class="blog white z-depth-3 hoverable">

        <section class="blog-header has-text-centered
                        blue-grey lighten-5">
          <div class="main_title"> 
            <h4 class="title is-4"
                itemprop="name headline">
              {{ renderData.title or title or metadata.title }}</h4>
          </div>
        </section>

        <article class="blog-body has-text-centered"
                 itemprop="articleBody">
          {{ content | safe }}
          <div class="notification blue darken-3 is-dark">
            This is a home kind layout,
            to show landing page.
          </div>
        </article>

      </div>
    </section>

  </main>
{% endblock %}
{{< / highlight >}}

There will be double columns design in other layout.
Both single column and double columns design,
`extends` the same base parent.

#### Page Content: Index (Home)

The only differences is that I add custom color class for each box.

* [gitlab.com/.../views/index.html][tutor-vc-index]

{{< highlight html >}}
---
layout    : home
eleventyExcludeFromCollections: true
---

  <div class="box blue lighten-1">
    To have, to hold, to love,
    cherish, honor, and protect?</div>
  
  <div class="box blue lighten-3">
    To shield from terrors known and unknown?
    To lie, to deceive?</div>

  <div class="box blue lighten-5">
    To live a double life,
    to fail to prevent her abduction,
    erase her identity, 
    force her into hiding,
    take away all she has known.</div>
{{< / highlight >}}

#### Render: Browser

Open in your favorite browser.
You should see, a blue colored homepage, by now.

* <http://localhost:8080/>

![11ty: Page Content: Home][image-vc-pa-home]

Notice that this is a single column page.
All other page is double columns,
and again, deserve its own explanation.

-- -- --

### What is Next ?

Consider continue reading [ [Eleventy - Bulma - SASS Layout][local-whats-next] ].
We are going to explore complex template inheritance in Nunjucks.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/01/10/11ty-bulma-sass-layout/
[local-bulma-md]:       {{< baseurl >}}frontend/2019/06/11/bulma-md-overview/

[tutor-html-master-05]: {{< tutor-11ty-bulma-md >}}/tutor-05/

[tutor-configuration]:  {{< tutor-11ty-bulma-md >}}/tutor-05/.eleventy.js
[tutor-vc-index]:       {{< tutor-11ty-bulma-md >}}/tutor-02/views/index.html
[tutor-vl-base]:        {{< tutor-11ty-bulma-md >}}/tutor-05/views/_includes/layouts/base.njk
[tutor-vl-home]:        {{< tutor-11ty-bulma-md >}}/tutor-05/views/_includes/layouts/home.njk
[tutor-vi-s-head]:      {{< tutor-11ty-bulma-md >}}/tutor-05/views/_includes/site/head.njk
[tutor-vi-s-header]:    {{< tutor-11ty-bulma-md >}}/tutor-05/views/_includes/site/header.njk
[tutor-vi-s-footer]:    {{< tutor-11ty-bulma-md >}}/tutor-05/views/_includes/site/footer.njk
[tutor-vi-s-scripts]:   {{< tutor-11ty-bulma-md >}}/tutor-05/views/_includes/site/scripts.njk

[image-ss-05-tree-both]:    {{< assets-ssg >}}/2020/01/05-tree-both.png
[image-ss-05-tree-sass]:    {{< assets-ssg >}}/2020/01/05-tree-sass.png
[image-ss-05-dart-sass]:    {{< assets-ssg >}}/2020/01/05-dart-sass.png
[image-ss-05-tree-css]:     {{< assets-ssg >}}/2020/01/05-tree-css.png

[image-vc-pa-home]:     {{< assets-ssg >}}/2020/01/05-pages-home.png
