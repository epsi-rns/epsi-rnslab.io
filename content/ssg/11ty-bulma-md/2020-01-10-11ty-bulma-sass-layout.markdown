---
type   : post
title  : "Eleventy - Bulma - SASS Layout"
date   : 2020-01-10T09:17:35+07:00
slug   : 11ty-bulma-sass-layout
categories: [ssg]
tags      : [11ty, bulma, sass]
keywords  : [assets, nunjucks, layout, template inheritance, material colors]
author : epsi
opengraph:
  image: assets-ssg/2020/01/05-vim-columns-double.png

toc    : "toc-2020-01-11ty-bulma-md-step"

excerpt:
  Building 11ty Site Step by step, with Bulma MD as stylesheet frontend.
  Using custom template inheritance with Nunjucks in Eleventy Layout.

---

### Preface

> Goal: Using custom template inheritance with Nunjucks in Eleventy Layout.

#### Source Code

This article use [tutor-05][tutor-html-master-05] theme.
We will create it step by step.

-- -- --

### 5: Page Content: Page, Post

Our Page Kind, and Post Kind is simply,
a double columns version of previous article.

#### Layout: Nunjucks Page

Just like previous chapter,
but very long layout.

* [gitlab.com/.../views/_includes/layouts/page.njk][tutor-vl-page]

{{< highlight jinja >}}
{% extends "layouts/base.njk" %}

{% block main %}
  <main role="main" 
        class="column is-two-thirds">

    <section class="main-wrapper light-blue">
      <div class="blog white z-depth-3 hoverable">

        <section class="blog-header light-blue lighten-5">
          <div class="main_title"> 
            <h2 class="title is-4" itemprop="name headline">
              {{ renderData.title or title or metadata.title }}</h2>
          </div>
        </section>

        <article class="blog-body" itemprop="articleBody">
          {{ content | safe }}
        </article>

      </div>
    </section>

  </main>
{% endblock %}

{% block aside %}
  <aside class="column is-one-thirds">

    <section class="aside-wrapper pink">
      <div class="widget white z-depth-3 hoverable">

        <div class="widget-header pink lighten-4">
          <strong>Side Menu</strong>
          <span class="fas fa-tags is-pulled-right"></span>
        </div>

        <div class="widget-body">
          This is a tags kind layout.
        </div>

      </div>
    </section>

  </aside>
{% endblock %}
{{< / highlight >}}

#### Page Content: pages/about

No difference with previous chapter.
Just leave the content that way.

* [gitlab.com/.../views/pages/about.md][tutor-vc-pa-about]

#### Render: Browser

Open in your favorite browser.
You should see, a colored about page, by now.

![11ty: Page Content: pages/about][image-vc-pa-about]

#### Layout: Nunjucks Post

Just like previous chapter,
but very long layout.

* [gitlab.com/.../views/_includes/layouts/post.njk][tutor-vl-post]

{{< highlight jinja >}}
{% extends "layouts/base.njk" %}

{% block main %}
  <main role="main" 
        class="column is-two-thirds">

    <section class="main-wrapper blue">
      <div class="blog white z-depth-3 hoverable">

        <section class="blog-header blue lighten-5">
          <div class="main_title"> 
            <h2 class="title is-4" itemprop="name headline">
              <a href="{{ title or metadata.title }}"
                >{{ title or metadata.title }}</a></h2>
            <p><strong>{{ page.date | date('MMMM Do, YYYY') }}</strong></p>
          </div>
        </section>

        <article class="blog-body" itemprop="articleBody">
          {{ content | safe }}
        </article>

      </div>
    </section>

  </main>
{% endblock %}

{% block aside %}
  <aside class="column is-one-thirds">

    <section class="aside-wrapper teal">
      <div class="widget white z-depth-3 hoverable">

        <div class="widget-header teal lighten-4">
          <strong>Side Menu</strong>
          <span class="fas fa-scroll is-pulled-right"></span>
        </div>

        <div class="widget-body">
          This is a post kind layout.
        </div>

      </div>
    </section>

  </aside>
{% endblock %}
{{< / highlight >}}

#### Page Content: every-day.md

No difference with previous chapter.
Just leave the content that way.

* [gitlab.com/.../views/posts/every-day.md][tutor-vc-po-everyday]

#### Render: Browser

Open in your favorite browser.
You should see, a colored post, by now.

![11ty: Post Content: posts/every-day][image-vc-po-everyday]

The looks has dramatically appealing compared to the previous one.

-- -- --

### 6: Template Inheritance: Double Columns

> Using the pattern to create parent layout.

As we did, we do it again.
Analyze pattern from one or two existing template,
write down anything similar,
and take out any differences into a few block.

#### Parent Layout: Nunjucks Columns Double

Consider create a parent layout for double columns layout.

* [gitlab.com/.../views/_includes/layouts/columns-double.njk][tutor-vl-double]

{{< highlight jinja >}}
{% extends "layouts/base.njk" %}

{% block main %}
{%- set color %}{% block main_color %}blue{% endblock %}{% endset -%}

  <main role="main" 
        class="column is-two-thirds">

    <section class="main-wrapper {{ color }}">
      <div class="blog white z-depth-3 hoverable">

        <section class="blog-header {{ color }} lighten-5">
        {% block blog_header %}
          <div class="main_title"> 
            <h2 class="title is-4" itemprop="name headline">
              <a href="{{ title or metadata.title }}"
                >{{ title or metadata.title }}</a></h2>
          </div>
        {% endblock %}
        </section>

        <article class="blog-body" itemprop="articleBody">
          {{ content | safe }}
        </article>

      </div>
    </section>

  </main>
{% endblock %}

{% block aside %}
{%- set color %}{% block aside_color %}green{% endblock %}{% endset -%}

  <aside class="column is-one-thirds">

    <section class="aside-wrapper {{ color }}">
      <div class="widget white z-depth-3 hoverable">

        <div class="widget-header {{ color }} lighten-4">
        {% block widget_header %}
          <strong>Side Menu</strong>
          <span class="fas fa-scroll is-pulled-right"></span>
        {% endblock %}
        </div>

        <div class="widget-body">
        {% block widget_body %}
          <p>This is a widget body.</p>
        {% endblock %}
        </div>

      </div>
    </section>

  </aside>
{% endblock %}
{{< / highlight >}}

We have three `block`s here:

* `blog_header`,

* `widget_header`.

* `widget_body`.

And two additional `block`s to handle variable

* `main_color`,

* `aside_color`.

![11ty: Double Columns Layout][image-ss-05-vim-double]

Now we can rewrite `page` kind and `post` kind.

#### Layout: Nunjucks Page

Notice how the color variable propagate,
from child layout to parent layout.

{{< highlight jinja >}}
{% extends "layouts/columns-double.njk" %}

{% block main_color %}pink{% endblock %}
{% block aside_color %}green{% endblock %}

{% block widget_body %}
  <p>This is a page kind layout.</p>
{% endblock %}
{{< / highlight >}}

Notice that we only need to replace block,
that has different content with parent layout.

#### Layout: Nunjucks Post

Notice how the color variable propagate,
from child layout to parent layout.

{{< highlight jinja >}}
{% extends "layouts/columns-double.njk" %}

{% block main_color %}blue{% endblock %}
{% block aside_color %}teal{% endblock %}

{% block blog_header %}
  <div class="main_title"> 
    <h4 class="title is-4" itemprop="name headline">
      <a href="{{ page.url | url }}">
        {{ renderData.title or title or metadata.title }}
      </a></h4>
    <b>{{ page.date | date('MMMM Do, YYYY') }}</b>
  </div>
{% endblock %}

{% block widget_body %}
  <p>This is a post kind layout.</p>
{% endblock %}
{{< / highlight >}}

The length of the layout decrease significanly.

-- -- --

### 7: Page Content: Archive, Tag, Tag Name

Again, we can rewrite the all other layouts as well.
This time, each page content has many additional html tags,
along with custom class, to adopt new theme design.

#### Child Layout: Nunjucks Archive

Template inheritance in `nunjucks`,
start with the word `extends`.

* [gitlab.com/.../views/_includes/layouts/archive.njk][tutor-vl-archive]

{{< highlight jinja >}}
{% extends "layouts/columns-double.njk" %}

{% block main_color %}cyan{% endblock %}
{% block aside_color %}purple{% endblock %}

{% block widget_body %}
  <p>This is an archive kind layout.</p>
{% endblock %}
{{< / highlight >}}

![11ty: Multiple Template Inheritance][image-ss-05-inheritance]

#### Page Content: pages/archive

I only add `content` class.

* [gitlab.com/.../views/pages/archive.html][tutor-vc-pa-archive]

{{< highlight jinja >}}
---
layout    : archive
title     : Archive
permalink : /pages/
eleventyExcludeFromCollections: true
---

  <div class="archive-list">
  {%- for post in collections.all | reverse -%}
    <div class="archive-item meta-item">
      <div class="meta_link has-text-right">
        <time class="meta_time is-pulled-right"
              datetime="{{ post.date | date() }}">
          {{ post.date | date('MMM DD, Y') }}&nbsp;
          &nbsp;<span class="fa fa-calendar"></span></time></div>
      <div class="is-pulled-left">
      <a href="{{ post.url | url }}">
        {{ post.data.title }}
      </a></div>
      <div class="is-clearfix"></div>
    </div>
  {%- endfor -%}
  </div>
{{< / highlight >}}

![11ty: Page Content: pages/archive][image-vc-pa-archive]

#### Child Layout: Nunjucks Tags

* [gitlab.com/.../views/_includes/layouts/tags.njk][tutor-vl-tags]

{{< highlight jinja >}}
{% extends "layouts/columns-double.njk" %}

{% block main_color %}light-blue{% endblock %}
{% block aside_color %}lime{% endblock %}

{% block widget_body %}
  <p>This is a tags kind layout.</p>
{% endblock %}
{{< / highlight >}}

#### Page Content: Tags

I only add `content` class.
And Bulma's `tag` class in each link.

* [gitlab.com/.../views/tags.html][tutor-vc-pa-tags]

{{< highlight jinja >}}
---
layout    : tags
title     : List of Tags
eleventyExcludeFromCollections: true
---

  <div class="field is-grouped is-grouped-multiline">
  {%- for tag in collections.tagList -%}
    {%- set postsList = collections[ tag ] -%}
    {%- set listCount = postsList.length -%}
    {%- set tagUrl %}/tags/{{ tag | slug }}/{% endset -%}

    {% if listCount %}
    <div class="tags has-addons">
      <a href="{{ tagUrl | url }}">
        <div class="tag is-light blue lighten-2 z-depth-1"
          >{{ tag }} 
        </div><div class="tag is-dark blue darken-2 z-depth-1"
          >{{ listCount }}
        </div>
      </a>
    </div>
    &nbsp;
    {% endif %}
  {%- endfor -%}
    <div class="tags dummy"></div>
  </div>

  <section class="p-y-5" id="archive">
    {% for tag in collections.tagList %}
      {% set tagUrl %}/tags/{{ tag }}/{% endset %}
      <div id="{{ tag | slug }}" class ="anchor-target">
        <a href="{{ tagUrl | url }}">
        <span class="fa fa-tag"></span> 
        &nbsp; {{ tag }} </a>
      </div>
    {%- endfor -%}
  </section>
{{< / highlight >}}

![11ty: Page Content: tags][image-vc-pa-tags]

#### Child Layout: Nunjucks Tag Name

* [gitlab.com/.../views/_includes/layouts/tag-name.njk][tutor-vl-tag-name]

{{< highlight jinja >}}
{% extends "layouts/columns-double.njk" %}

{% block main_color %}indigo{% endblock %}
{% block aside_color %}orange{% endblock %}

{% block widget_body %}
  <p>This is a tag-name kind layout.</p>
{% endblock %}
{{< / highlight >}}

#### Page Content: Tag Name (paginated)

I only add `content` class.

* [gitlab.com/.../views/tag-name.html][tutor-vc-pa-tag-name]

{{< highlight jinja >}}
---
layout    : tag-name
eleventyExcludeFromCollections: true

pagination:
  data: collections
  size: 1
  alias: tag

permalink: /tags/{{ tag }}/

renderData:
  title: Tagged “{{ tag }}”

---

  {% set postslist = collections[ tag ] %}
  <div class="archive-list">
  {%- for post in postslist -%}
    <div class="archive-item meta-item">
      <div class="meta_link has-text-right">
        <time class="meta_time is-pulled-right"
              datetime="{{ post.date | date() }}">
          {{ post.date | date('MMM DD, Y') }}&nbsp;
          &nbsp;<span class="fa fa-calendar"></span></time></div>
      <div class="is-pulled-left">
      <a href="{{ post.url | url }}">
        {{ post.data.title }}
      </a></div>
      <div class="is-clearfix"></div>
    </div>
  {%- endfor -%}
  </div>

  <p><a href="{{ '/tags/' | url }}"
        class="button is-small"
       >List of all tags</a></p>
{{< / highlight >}}

![11ty: Page Content: tag-name][image-vc-pa-tag-name]

Although the frame have been done,
within the `columns-double.njk` layout,
All page contents in this chapter are revamped,
to adopt custom class in the Bulma Material Design stylesheet

-- -- --

### What is Next ?

Consider continue reading [ [Eleventy - Custom Index - Content][local-whats-next] ].
We are going to populate content, for use with some index page,
such as archive, tags, paginated blog post.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/01/11/11ty-index-content/

[tutor-html-master-01]: {{< tutor-11ty-bulma-md >}}/tutor-01/

[tutor-vl-page]:        {{< tutor-11ty-bulma-md >}}/tutor-05/views/_includes/layouts/page.njk
[tutor-vl-post]:        {{< tutor-11ty-bulma-md >}}/tutor-05/views/_includes/layouts/post.njk
[tutor-vl-double]:      {{< tutor-11ty-bulma-md >}}/tutor-05/views/_includes/layouts/columns-double.njk
[tutor-vl-archive]:     {{< tutor-11ty-bulma-md >}}/tutor-05/views/_includes/layouts/archive.njk
[tutor-vl-tags]:        {{< tutor-11ty-bulma-md >}}/tutor-05/views/_includes/layouts/tags.njk
[tutor-vl-tag-name]:    {{< tutor-11ty-bulma-md >}}/tutor-05/views/_includes/layouts/tag-name.njk

[tutor-vc-pa-about]:    {{< tutor-11ty-bulma-md >}}/tutor-05/views/pages/about.md
[tutor-vc-pa-archive]:  {{< tutor-11ty-bulma-md >}}/tutor-05/views/pages/archive.html
[tutor-vc-pa-tags]:     {{< tutor-11ty-bulma-md >}}/tutor-05/views/tags.html
[tutor-vc-pa-tag-name]: {{< tutor-11ty-bulma-md >}}/tutor-05/views/tag-name.html

[tutor-vc-po-everyday]: {{< tutor-11ty-bulma-md >}}/tutor-05/views/posts/every-day.md

[image-vc-pa-about]:    {{< assets-ssg >}}/2020/01/05-pages-about.png
[image-vc-pa-archive]:  {{< assets-ssg >}}/2020/01/05-pages-archive.png
[image-vc-pa-tags]:     {{< assets-ssg >}}/2020/01/05-pages-tags.png
[image-vc-pa-tag-name]: {{< assets-ssg >}}/2020/01/05-pages-tag-name.png
[image-vc-po-everyday]: {{< assets-ssg >}}/2020/01/05-posts-everyday.png
[image-ss-05-vim-double]:   {{< assets-ssg >}}/2020/01/05-vim-columns-double.png
[image-ss-05-inheritance]:  {{< assets-ssg >}}/2020/01/05-template-inheritance.png
