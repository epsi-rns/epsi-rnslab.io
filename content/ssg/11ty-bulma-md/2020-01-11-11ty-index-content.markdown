---
type   : post
title  : "Eleventy - Custom Index - Content"
date   : 2020-01-11T09:17:35+07:00
slug   : 11ty-index-content
categories: [ssg]
tags      : [11ty]
keywords  : [markdown, populate content]
author : epsi
opengraph:
  image: assets-ssg/2020/01/06-tree-content.png

toc    : "toc-2020-01-11ty-bulma-md-step"

excerpt:
  Building 11ty Site Step by step, with Bulma MD as stylesheet frontend.
  Populate content for use with special custom index pages.

---

### Preface

> Goal: Populate content for use with special custom index pages.

#### Source Code

This article use [tutor-06][tutor-html-master-06] theme.
We will create it step by step.

#### Prerequiste

> Use Bulma MD in Eleventy.

From this article forward,
the stylesheet rely on Bulma MD in article below.
You should give a fast reading of this Bulma MD article first,
before continue reading this article.

* [Bulma MD - Overview][local-bulma-md]

#### Layout Preview for Tutor 06

![Nunjucks: Layout Preview for Tutor 06][template-inheritance-06]

[template-inheritance-06]:  {{< assets-ssg >}}/2020/02/06-template-inheritance.png

-- -- --

### 1: Populate the Content

As always, populate content with good reading is,
as hard as giving meaningful variable.
Luckily I have done it for you.

#### Custom Index Page

We are going to explore custom index page such as:

1. Archive

   * <http://localhost:8080/pages/archive-by-year/>
   
   * <http://localhost:8080/pages/archive-by-month/>

2. Tags

   * <http://localhost:8080/tags/>

3. Blog (Article List with Pagination)

   * <http://localhost:8080/pages/>

![Illustration: Custom Layouts][illust-custom-layout]

For this to work, we need a proper blog content,
with such diversity such as posting year, and tags for each post,
so we can represent the list nicely,
with sorting, and also grouping whenever necessary.

My content choice comes to `song lyrics`.
We can tag `song lyrics` with its genre,
and naturally `song lyrics` also has date released.
I also add `movie quotes` to examine what does it looks,
to have entirely different folder category.

#### Pattern

Our typical content is usually short quote from a lyric,
with four frontmatter items as below:

* [gitlab.com/.../views/lyrics/5fdp-wash-it-all-the-way.md][tutor-po-wash-it-all]

{{< highlight yaml >}}
---
layout    : post
title     : 5FDP - Wash It All The Way
date      : 2020-02-23 07:35:05
tags      : ["metal", "2010s"]
---

I'm wasting here\
Can anyone\
Wash it all away?
<!--more-->

I won't change for you\
And I can't take the pain\
There's nothing you can do\
And there's nothing you can say
{{< / highlight >}}

All example content should follow the pattern above.
All these content wear `post` layout.

#### How Many?

We need to populate the content,
with about ten lyrics and five quotes,
so that we have enough content to simulate pagination later on.

{{< highlight bash >}}
❯ tree views/lyrics views/quotes
views/lyrics
├── 5fdp-wash-it-all-the-way.md
├── deftones-be-quiet-and-drive.md
├── disturbed-stupify.md
├── eurythmics-sweet-dreams.md
├── house-of-pain-jump-around.md
├── marylin-manson-redeemer.md
├── onyx-slam.md
├── public-enemy-bring-the-noise.md
├── ratm-bulls-on-parade.md
├── soulfly-jump-da-fuck-up.md
├── soundgarden-black-hole-sun.md
└── white-zombie-more-human-than-human.md
views/quotes
├── dead-poets-society.md
├── every-day.md
├── fight-club.md
├── jerry-maguire.md
└── scott-pilgrim.md

0 directories, 17 files
{{< / highlight >}}

![11ty: All Content][image-ss-tree-content]

Also edit the frontmatter as necessary,
for the rest of the content.

#### Test

Consider to check, if this content works.

* Archive

![11ty: Page Content: pages/index][image-vc-pa-archive]

* Tags

![11ty: Page Content: tags][image-vc-pa-tags]

-- -- --

### 2: Configuration: Posts Collection

#### .eleventy.js: 

For further use, I add a collection that contain only `post` kind,
by checking for each `item.data.layout` from frontmatter.

* [gitlab.com/.../.eleventy.js][tutor-configuration]

{{< highlight javascript >}}
  // Filter using `Array.filter`
  eleventyConfig.addCollection("posts", function(collection) {
    return collection.getAll().filter(function(item) {
      // Filter by layout name
      return "post" === item.data.layout;
    });
  });
{{< / highlight >}}

We can use later as:

{{< highlight jinja >}}
{% set posts = collections.posts %}
{{< / highlight >}}

-- -- --

### 3: Header Menu

To make our live easier we need to alter our header menu a bit.

![11ty: Header Menu for Custom Index][image-ss-header-menu]

#### Partial: Nunjucks Header

It is a long header, so I crop the code.
You can get the complete code in the repository.

* [gitlab.com/.../views/_includes/site/header.njk][tutor-vi-s-header]


{{< highlight html >}}
    <div class="navbar-brand">
      <a class="navbar-item"
         href="{{ "/" | url }}">
        <img src="{{ "/assets/images/logo-gear.png" | url }}" 
           alt="Home" />
      </a>
      <a class="navbar-item"
         href="{{ "/pages/" | url }}">
        Blog
      </a>

      ...
    </div>
{{< / highlight >}}

And

{{< highlight html >}}
      <div class="navbar-start">
        <div class="navbar-item has-dropdown is-hoverable">
          <a class="navbar-link">
            Archives
          </a>

          <div class="navbar-dropdown">
            <a class="navbar-item"
               href="{{ "/tags/" | url }}">
              By Tags
            </a>
            <hr class="navbar-divider">
            <a class="navbar-item"
               href="{{ "/pages/archive-by-month/" | url }}">
              By Month
            </a>
            <a class="navbar-item"
               href="{{ "/pages/archive-by-year/" | url }}">
              By Year
            </a>
            <a class="navbar-item"
               href="{{ "/pages/archive-simple/" | url }}">
              Simple
            </a>
          </div>
        </div>

        <a class="navbar-item"
           href="{{ "/pages/about/" | url }}">
          About
        </a>
      </div>
{{< / highlight >}}

-- -- --

### 4: Landing Page

We can also give a proper URL in landing so you can access
archives page and tags page more quickly.

#### Page Content: index

It is actually very similar with previous `index.html`.

* [gitlab.com/.../views/index.html][tutor-vc-index]

{{< highlight html >}}
---
layout    : home
eleventyExcludeFromCollections: true
---

  <br/>

  <p>
    <a class="button is-dark
    blue-grey darken-2 hoverable m-b-5"
       href="{{ site.baseurl }}/pages/archive-by-month/"
      >Articles Sorted by Month</a>
    <a class="button is-dark
    blue-grey darken-1 hoverable m-b-5"
       href="{{ site.baseurl }}/tags/"
      >Articles Sorted by Tag</a>
  </p>

  <p>As always,
  should you be caught or killed,
  any knowledge of your actions will be disavowed.</p>

  <div class="justify-content-center">
    <img src="{{ "/assets/images/cards/one-page.png" | url }}" 
         alt="business card">
  </div>

  <p>
    <span class="fas fa-home"></span>&nbsp;
      Whitewood Street, Monday Market,
      East Jakarta, 55112, Indonesia.
  </p>
{{< / highlight >}}

![11ty: Landing Page][image-ss-landing-page]

-- -- --

### 5: Permalink

> Update: New in Eleventy 0.11.0.

You can add `permalink`, with global setting , or per directory basis,
instead of setting-up each file's frontmatter.

For example in lyrics directory you can add

* `views/lyrics/lyrics.11tydata.js`.

{{< highlight javascript >}}
// per directory basis
// instead of `_data/eleventyComputed.js`

const moment   = require("moment");
const category = 'lyrics'

module.exports = {
  eleventyComputed: {
    permalink: (data) => {
      if (!data.slug) return null;
      else {
        date = moment(data.date).format('YYYY/MM/DD');
        return `${category}/${date}/${data.slug}.html`;
      }
    }
  }
};
{{< / highlight >}}

Now add slug in frontmatter, for example

{{< highlight yaml >}}
layout    : post
title     : Marilyn Manson - Redeemer
date      : 2015-07-25 07:35:05
slug      : marilyn-manson-redeemer
tags      : ["industrial metal", "90s"]
{{< / highlight >}}

This will result an URL such as:

* `http://localhost:8080/lyrics/2015/07/25/marilyn-manson-redeemer.html`

This way, you can freely name your file,
for example with jekyll style of filenaming.
Which is very nice,
because you can sort the date in any file manager, such as:

* `2015-07-25-marilyn-manson-redeemer.md`.

I use conditional `if (!data.slug) return null;`,
so that I can migrate easily.
The old URL which does not have any slug setting in frontmatter,
should also be working.
This is important, when you don't want to change your URL.
Thera are many reason for this,
for example your URL has been a reference in other site,
or cached in google search.


-- -- --

### What is Next ?

Consider continue reading [ [Eleventy - Custom Index - Archive][local-whats-next] ].
We are going to use this populated content, with archive page,
sorted by date, and grouped by month and year.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/01/12/11ty-index-archive/
[local-bulma-md]:       {{< baseurl >}}frontend/2019/06/11/bulma-md-overview/

[tutor-html-master-06]: {{< tutor-11ty-bulma-md >}}/tutor-06/

[tutor-configuration]:  {{< tutor-11ty-bulma-md >}}/tutor-06/.eleventy.js
[tutor-vc-index]:       {{< tutor-11ty-bulma-md >}}/tutor-06/views/index.html
[tutor-po-wash-it-all]: {{< tutor-11ty-bulma-md >}}/tutor-06/views/lyrics/5fdp-wash-it-all-the-way.md
[tutor-vi-s-header]:    {{< tutor-11ty-bulma-md >}}/tutor-06/views/_includes/site/header.njk

[image-ss-tree-content]:{{< assets-ssg >}}/2020/01/06-tree-content.png
[image-ss-header-menu]: {{< assets-ssg >}}/2020/01/06-header-menu.png
[image-ss-landing-page]:{{< assets-ssg >}}/2020/01/06-landing-page.png
[image-vc-pa-archive]:  {{< assets-ssg >}}/2020/01/06-pages-index-content-test.png
[image-vc-pa-tags]:     {{< assets-ssg >}}/2020/01/06-pages-tags-content-test.png

[illust-custom-layout]: {{< assets-ssg >}}/2020/01/custom-layout.png
