---
type   : post
title  : "Eleventy - Custom Index - Tags"
date   : 2020-01-13T09:17:35+07:00
slug   : 11ty-index-tags
categories: [ssg]
tags      : [11ty]
keywords  : [nunjucks, layout, list, index, tags]
author : epsi
opengraph:
  image: assets-ssg/2020/01/05-vim-columns-double.png

toc    : "toc-2020-01-11ty-bulma-md-step"

excerpt:
  Building 11ty Site Step by step, with Bulma MD as stylesheet frontend.
  Build tags page in tree fashioned looks, and
  build each tag-name page, using previous sorting and grouping filter.

---

### Preface

> Goal: Build tag tree and each tag-name page,
> using previous sorting and grouping filter.

#### Source Code

This article use [tutor-06][tutor-html-master-06] theme.
We will create it step by step.

-- -- --

### 1: Views: Tags

First thing to do is refactoring code.

#### Page Content: Nunjucks: Tags

Consider change the `html` page content with `nunjucks` code below.

* [gitlab.com/.../views/tags.html][tutor-vc-pa-tags]

{{< highlight jinja >}}
---
layout    : tags
title     : List of Tags
eleventyExcludeFromCollections: true
---

{% include "index/tags-badge.njk" %}
{% include "index/tags-list.njk" %}
{{< / highlight >}}

#### Partial: Nunjucks: Tags Badge

And move the header from previous page content into layout.

* [gitlab.com/.../views/_includes/index/tags-badge.njk][tutor-vl-tags-badge]

{{< highlight jinja >}}
  <div class="field is-grouped is-grouped-multiline">
  {%- for tag in collections.tagList -%}
    {%- set postsList = collections[ tag ] -%}
    {%- set listCount = postsList.length -%}
    {%- set tagUrl %}/tags/{{ tag | slug }}/{% endset -%}

    {% if listCount %}
    <div class="tags has-addons">
      <a href="{{ tagUrl | url }}">
        <div class="tag is-light blue lighten-2 z-depth-1"
          >{{ tag }} 
        </div><div class="tag is-dark blue darken-2 z-depth-1"
          >{{ listCount }}
        </div>
      </a>
    </div>
    &nbsp;
    {% endif %}
  {%- endfor -%}
    <div class="tags dummy"></div>
  </div>
{{< / highlight >}}

#### Partial: Nunjucks: Tags List

And move the header from previous page content into layout.

* [gitlab.com/.../views/_includes/index/tags-list.njk][tutor-vl-tags-list]

{{< highlight jinja >}}
  <section class="p-y-5" id="archive">
    {% for tag in collections.tagList %}
      {% set tagUrl %}/tags/{{ tag }}/{% endset %}
      <div id="{{ tag | slug }}" class ="anchor-target">
        <a href="{{ tagUrl | url }}">
        <span class="fa fa-tag"></span> 
        &nbsp; {{ tag }} </a>
      </div>
    {%- endfor -%}
  </section>
{{< / highlight >}}

You will see previous the looks as below:

![11ty: Page Content: tags][image-vc-pa-tags]

#### Partial: Nunjucks: Tags Tree

Consider go further with tree.
I mean, each tag's posts can also be displayed, right?
We are going to utilize built in `collections[ tag ]`.

* [gitlab.com/.../views/_includes/index/tags-tree.njk][tutor-vl-tags-tree]

{{< highlight jinja >}}
  <section class="p-y-5" id="archive">
    {% for tag in collections.tagList %}
      {%- set postsList = collections[ tag ] -%}
      <div id="{{ tag | slug }}" class ="anchor-target">
        <span class="fa fa-tag"></span> 
        &nbsp; {{ tag }}
      </div>

      <div class="archive-list p-y-5">
      {%- for post in postsList -%}
        {% include "index/each-post.njk" %}
      {%- endfor -%}
      </div>
    {%- endfor -%}
  </section>
{{< / highlight >}}

![11ty: Layout: Tags Badge and Tags Tree][image-layout-tags]

#### Page Content: Nunjucks: Tags

Do not forget to modify the page content.

* [gitlab.com/.../views/tags.html][tutor-vc-pa-tags]

{{< highlight jinja >}}
---
layout    : tags
title     : List of Tags
eleventyExcludeFromCollections: true
---

{% include "index/tags-badge.njk" %}
{% include "index/tags-tree.njk" %}
{{< / highlight >}}

Now we have our new looks as shown in figure below:

* <http://localhost:8080/tags/>

![11ty: Page Content: tags tree][image-vc-pa-tags-tree]

-- -- --

### 2: Views: Tag Name

There is no need to refactor the code.
We can either utilize `by-year.njk` or `by-month.njk`, 
or even the simple one.

#### Page Content: Nunjucks: Tags

Consider change the `html` page content with `nunjucks` code below.

* [gitlab.com/.../views/tags.html][tutor-vc-pa-tags]

{{< highlight jinja >}}
---
...
---

{% set posts = collections[ tag ] %}
{# include "index/by-year.njk" #}
{% include "index/by-month.njk" %}

  <p><a href="{{ '/tags/' | url }}" 
        class="button is-small"
       >List of all tags</a></p>
{{< / highlight >}}

#### Render: Browser

Now you can see the result in your favorite browser.

* <http://localhost:8080/tags/nu-metal/>

![11ty: Page Content: tag name][image-vc-pa-tag-name]

This is also displayed in tree fashioned,
by grouping the posts monthly.

-- -- --

### What is Next ?

Consider continue reading [ [Eleventy - Custom Index - Blog][local-whats-next] ].
We are going to make a blog list.
This is actually just an article index with different looks.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/01/14/11ty-index-blog/
[local-bulma]:          {{< baseurl >}}frontend/2019/03/01/bulma-overview/

[tutor-html-master-06]: {{< tutor-11ty-bulma-md >}}/tutor-06/

[tutor-configuration]:  {{< tutor-11ty-bulma-md >}}/tutor-06/.eleventy.js
[tutor-vl-tags-badge]:  {{< tutor-11ty-bulma-md >}}/tutor-06/views/_includes/index/tags-badge.njk
[tutor-vl-tags-list]:   {{< tutor-11ty-bulma-md >}}/tutor-06/views/_includes/index/tags-list.njk
[tutor-vl-tags-tree]:   {{< tutor-11ty-bulma-md >}}/tutor-06/views/_includes/index/tags-tree.njk
[tutor-vc-pa-tags]:     {{< tutor-11ty-bulma-md >}}/tutor-06/views/tags.html
[tutor-vc-pa-tag-name]: {{< tutor-11ty-bulma-md >}}/tutor-06/views/tag-name.html

[image-vc-pa-tags]:     {{< assets-ssg >}}/2020/01/06-pages-tags-content-test.png
[image-vc-pa-tags-tree]:{{< assets-ssg >}}/2020/01/06-pages-tags-tree.png
[image-vc-pa-tag-name]: {{< assets-ssg >}}/2020/01/06-pages-tag-name.png
[image-layout-tags]:    {{< assets-ssg >}}/2020/01/06-layout-tags.png
