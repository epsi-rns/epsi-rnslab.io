---
type   : post
title  : "Eleventy - Custom Index - Blog"
date   : 2020-01-14T09:17:35+07:00
slug   : 11ty-index-blog
categories: [ssg]
tags      : [11ty]
keywords  : [nunjucks, layout, list, index, blog index, article list]
author : epsi
opengraph:
  image: assets-ssg/2020/01/05-vim-columns-double.png

toc    : "toc-2020-01-11ty-bulma-md-step"

excerpt:
  Building 11ty Site Step by step, with Bulma MD as stylesheet frontend.
  Build blog list, an article index with different looks,
  for use with pagination later.

---

### Preface

> Goal: Build blog list, an article index with different looks.

#### Source Code

This article use [tutor-06][tutor-html-master-06] theme.
We will create it step by step.

-- -- --

### 1: Layout: Blog

I need to be creative,
so I put more than one design to represent my blog list.
What I mean is, you don't need to be stuck in just one layout design.
And then, you can make your own layout design.

#### Layout: Nunjucks Blog

First we need to create new Layout.

* [gitlab.com/.../views/_includes/layouts/blog.njk][tutor-vl-blog]

{{< highlight jinja >}}
{% extends "layouts/columns-double.njk" %}

{% block main_color %}brown{% endblock %}
{% block aside_color %}light-green{% endblock %}

{% block widget_body %}
  <p>This is a blog kind layout.
  This layout is intended to show blog list.</p>
{% endblock %}
{{< / highlight >}}

#### .eleventy.js

And also set a new alias.

* [gitlab.com/.../.eleventy.js][tutor-configuration]

{{< highlight javascript >}}
  eleventyConfig.addLayoutAlias("blog",     "layouts/blog.njk");
{{< / highlight >}}

-- -- --

### 2: Views: Simple

> Start with simple example

#### Partial: Nunjucks: Simple

Consider, add a page content to layout as below.

* [gitlab.com/.../views/_includes/index/blog-simple.njk][tutor-vl-blog-simple]

{{< highlight jinja >}}
  <div class="archive-list">
  {%- for post in posts -%}
    {% include "index/each-post.njk" %}
  {%- endfor -%}
  </div>
{{< / highlight >}}

Where the `posts` is defined in content page.

#### Page Content: Nunjucks: pages/blog

Now let's you have yourself your simple index pages.

And our old content page would be as simple as this code below:

* [gitlab.com/.../views/pages/blog.html][tutor-vc-pa-blog]

{{< highlight jinja >}}
---
layout    : blog
title     : Blog List
permalink : /pages/
eleventyExcludeFromCollections: true
---

{%- set posts = collections.posts -%}
{%- set posts = posts | sort(false, true, 'date') | reverse -%}
{% include "index/blog-simple.njk" %}
{{< / highlight >}}

Since there is no grouping required with pagination,
the sorting process should be is easier.
We just need to sort by date field for each post object.

#### Render: Browser

Now you can see the result in your favorite browser.

* <http://localhost:8080/pages/>

![11ty: Page Content: Simple Blog List][image-vc-pa-blog-simp]

-- -- --

### 3: Views: Meta

I usually use this blog post design, in conjuction with pagination.
For this pagination page, I need to show excerpt,
and some also necessary meta information for each post.
There is no grouping required for pagination pages.

#### Page Content: Nunjucks: pages/blog

Change the `pages/blog` content to use `index/blog-list`

* [gitlab.com/.../views/pages/blog.html][tutor-vc-pa-blog]

{{< highlight jinja >}}
{%- set posts = collections.posts -%}
{%- set posts = posts | sort(false, true, 'date') | reverse -%}
{% include "index/blog-list.njk" %}
{{< / highlight >}}

#### Partial: Nunjucks: Blog List

Consider, move the page content to layout as below.

* [gitlab.com/.../views/_includes/index/blog-list.njk][tutor-vl-blog-list]

{{< highlight jinja >}}
  <div class="post-list">
  {%- for post in posts  -%}
    <div class="archive p-b-10">
    <div class="meta-item p-10 hoverable">

      <strong><a class="meta_link brown-text text-darken-3" 
        href="{{ post.url | url }}">{{ post.data.title }}
      </strong></a>

      {% include "index/blog-each-meta.njk" %}

    </div></div>
  {%- endfor -%}
  </div>
{{< / highlight >}}

Where the `posts` is defined in content page.

#### Partial: Nunjucks: Each Meta

Each meta can be represent as `nunjucks` template below:

* [gitlab.com/.../views/_includes/index/blog-each-meta.njk][tutor-vl-each-meta]

{{< highlight jinja >}}
      <div class="meta">
        <div class="meta_time is-pulled-left">
          <i class="fa fa-calendar"></i>
          <time datetime="{{ post.date | date() }}">
          {{ post.date | date('MMM D, Y') }}</time>
        </div>      
        <div class="meta_tags is-pulled-right">
          {%- for tag in post.data.tags  -%}
            {%- set tagUrl %}/tags/{{ tag }}/{% endset -%}
            <a class="tag is-small is-dark brown z-depth-1 hoverable"
               href="{{ tagUrl | url }}"
              >{{ tag }}&nbsp;<span class="fa fa-folder"></span>
            </a>&nbsp;
          {%- endfor -%}
        </div>
      </div> 

      <div class="is-clearfix"></div>
{{< / highlight >}}

Where the `post` is defined in `blog-list` layout.

#### Render: Browser

Now you can see the result in your favorite browser.

* <http://localhost:8080/pages/>

![11ty: Page Content: Blog List with Meta][image-vc-pa-blog-meta]

-- -- --

### 4: Shortcode: Simple Example

We need to understand what shortcode is using simple example.

#### Official Documentation

* [www.11ty.dev/docs/shortcodes](https://www.11ty.dev/docs/shortcodes/)

#### Simple Shortcode

Using fat arrow in javascript as function return equivalent.

* [gitlab.com/.../_11ty/simpleShortcode.js][shortcode-simple]

{{< highlight javascript >}}
exports.showText = (doc) => {
  return 'This is an example shortcode.';
}
{{< / highlight >}}

#### Page Content: Jerry Maguire

And put it somewhere inside one of your content:

{{< highlight javascript >}}
---
layout    : post
title     : Jerry Maguire
date      : 2015-01-01 17:35:15
tags      : ['subtitle', 'story']
---

You had me at Hello.

{% exampleShortcode %}
{{< / highlight >}}

#### Render: Browser

Then you'll see

* <http://localhost:8080/quotes/jerry-maguire/>

![11ty: Shortcode: simple example][image-sc-simple]

-- -- --

### 5: Shortcode: Excerpt

We also can utilize shortcode, to manage excerpt.

#### Verbatim Copy

The excerpt shortcode that use is,
verbatim copy of code made by Philipp Rudloff.

* [github.com/11ty/eleventy/issues/179](https://github.com/11ty/eleventy/issues/179)

* [github.com/11ty/eleventy/issues/479](https://github.com/11ty/eleventy/issues/479)

#### Source Code

* [gitlab.com/.../_11ty/excerpt.js][shortcode-excerpt]

{{< highlight javascript >}}
const excerptMinimumLength = 140;
const excerptSeparator = '<!--more-->'

/**
 * Extracts the excerpt from a document.
 *
 * @param {*} doc A real big object full of all sorts of information about a document.
 * @returns {String} the excerpt.
 */

exports.extractExcerpt = (doc) => {
  if (!doc.hasOwnProperty('templateContent')) {
    console.warn('Failed to extract excerpt: Document has no property `templateContent`.');
    return;
  }

  const content = doc.templateContent;

  if (content.includes(excerptSeparator)) {
    return content.substring(0, content.indexOf(excerptSeparator)).trim();
  }
  else if (content.length <= excerptMinimumLength) {
    return content.trim();
  }

  const excerptEnd = findExcerptEnd(content);
  return content.substring(0, excerptEnd).trim();
}

/**
 * Finds the end position of the excerpt of a given piece of content.
 * This should only be used when there is no excerpt marker in the content (e.g. no `<!--more-->`).
 *
 * @param {String} content The full text of a piece of content (e.g. a blog post)
 * @param {Number?} skipLength Amount of characters to skip before starting to look for a `</p>`
 * tag. This is used when calling this method recursively.
 * @returns {Number} the end position of the excerpt
 */
function findExcerptEnd(content, skipLength = 0) {
  if (content === '') {
    return 0;
  }

  const paragraphEnd = content.indexOf('</p>', skipLength) + 4;

  if (paragraphEnd < excerptMinimumLength) {
    return paragraphEnd + findExcerptEnd(content.substring(paragraphEnd), paragraphEnd);
  }

  return paragraphEnd;
}
{{< / highlight >}}

* .

#### Page Content: Jerry Maguire

And put it somewhere inside one of your content:

{{< highlight javascript >}}
---
layout    : post
title     : Jerry Maguire
date      : 2015-01-01 17:35:15
tags      : ['subtitle', 'story']
excerpt   : "Show me the money!"
---

You had me at Hello.

<!--more-->

You complete me.
{{< / highlight >}}

With code above we have two options.
We can either use `<!--more-->`, or the first paragraph.

-- -- --

### 6: Views: Excerpt

Consider complete this blog list view with these excerpt solution.

#### Excerpt form Frontmatter

I prefer the third option,
to manage excerpt from frontmatter.

{{< highlight yaml >}}
layout    : post
title     : Jerry Maguire
date      : 2015-01-01 17:35:15
tags      : ['subtitle', 'story']
excerpt   : "Show me the money!"
{{< / highlight >}}

#### Partial: Nunjucks: Blog List

Consider, move the page content to layout as below.

* [gitlab.com/.../views/_includes/index/blog-list.njk][tutor-vl-blog-list]

{{< highlight jinja >}}
  <div class="post-list">
  {%- for post in posts  -%}
    <div class="archive p-b-10">
    <div class="meta-item p-10 hoverable">

      <strong><a class="meta_link brown-text text-darken-3" 
        href="{{ post.url | url }}">{{ post.data.title }}
      </strong></a>

      {% include "index/blog-each-meta.njk" %}

      <div>
      {% if post.data.excerpt %}
        {{ post.data.excerpt }}
      {% else %}
        {% excerpt post %}
      {% endif %}
      </div>

      <div class="read-more is-light">
        <a href="{{ post.url | url }}" 
           class="button is-dark is-small brown z-depth-1 hoverable"
          >Read More&nbsp;</a>
      </div>

    </div></div>
  {%- endfor -%}
  </div>
{{< / highlight >}}

I also add `read more` button.

#### Render: Browser

The blog list, now looks like this below:

* <http://localhost:8080/pages/>

![11ty: Page Content: Blog List with Excerpt][image-vc-pa-blog-list]

-- -- --

### What is Next ?

Consider continue reading [ [Eleventy - Custom Output][local-whats-next] ].
We are going to make an archive in JSON form.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/01/15/11ty-index-custom-output/
[local-bulma]:          {{< baseurl >}}frontend/2019/03/01/bulma-overview/

[tutor-html-master-06]: {{< tutor-11ty-bulma-md >}}/tutor-06/

[tutor-configuration]:  {{< tutor-11ty-bulma-md >}}/tutor-06/.eleventy.js
[shortcode-simple]:     {{< tutor-11ty-bulma-md >}}/tutor-06/views/_11ty/simpleShortcode.js
[shortcode-excerpt]:    {{< tutor-11ty-bulma-md >}}/tutor-06/views/_11ty/excerpt.js
[tutor-vl-blog]:        {{< tutor-11ty-bulma-md >}}/tutor-06/views/_includes/layouts/blog.njk
[tutor-vl-blog-list]:   {{< tutor-11ty-bulma-md >}}/tutor-06/views/_includes/index/blog-list.njk
[tutor-vl-blog-simple]: {{< tutor-11ty-bulma-md >}}/tutor-06/views/_includes/index/blog-simple.njk
[tutor-vl-each-meta]:   {{< tutor-11ty-bulma-md >}}/tutor-06/views/_includes/index/blog-each-meta.njk
[tutor-vc-pa-blog]:     {{< tutor-11ty-bulma-md >}}/tutor-06/views/pages/blog.html

[image-vc-pa-blog-simp]:{{< assets-ssg >}}/2020/01/06-pages-blog-simple.png
[image-vc-pa-blog-meta]:{{< assets-ssg >}}/2020/01/06-pages-blog-meta.png
[image-vc-pa-blog-list]:{{< assets-ssg >}}/2020/01/06-pages-blog-list.png
[image-sc-simple]:      {{< assets-ssg >}}/2020/01/06-shortcode-example.png

