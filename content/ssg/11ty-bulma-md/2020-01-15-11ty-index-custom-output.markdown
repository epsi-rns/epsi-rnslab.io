---
type   : post
title  : "Eleventy - Custom Output"
date   : 2020-01-15T09:17:35+07:00
slug   : 11ty-index-custom-output
categories: [ssg]
tags      : [11ty]
keywords  : [nunjucks, json, yaml, custom output]
author : epsi
opengraph:
  image: assets-ssg/2020/01/05-vim-columns-double.png

toc    : "toc-2020-01-11ty-bulma-md-step"

excerpt:
  Building 11ty Site Step by step, with Bulma MD as stylesheet frontend.
  More about blog archive example, from just html, 
  to custom content output such as json, text and yaml.

---

### Preface

> Goal: Custom content output such as JSON.

#### Source Code

This article use [tutor-06][tutor-html-master-06] theme.
We will create it step by step.

-- -- --

### 1: Archive Data

Eleventy can read data, and can also produce data.

#### Frontmatter: Layout

Just set none.

#### Permalink

For this to works, you can use something like this

{{< highlight yaml >}}
---
permalink: pages/index.json
---
{{< / highlight >}}

* [www.11ty.io/docs/permalinks/][permalinks]

#### Page Content: JSON Example

The complete `JSON` example is below:

* [gitlab.com/.../views/pages/archive-json.html][tutor-vc-json]

{{< highlight jinja >}}
---
permalink: pages/index.json
---

{%- set posts = collections.posts -%}
{%- set postCount = posts.length -%}
{%- set cursor = 1 -%}

{
  {% for post in posts %}
    "{{ post.date | date('YYMMDDmm') }}": {
      "title": "{{ post.data.title }}",
      "url":   "{{ post.url }}"
    }
    {%-if cursor != postCount %},{% endif -%}
    {%-set cursor = cursor + 1 %}
  {% endfor %}
}
{{< / highlight >}}

I give an identitiy number for each post using dates and minutes.
It is rarely that a person blog in the same exact dates and minutes.
So I guess, giving an ID like this is valid enough.

#### Render: Browser

The blog list, now looks like this below:

* <http://localhost:8080/pages/index.json>

![11ty: Custom Output: Archive in JSON][image-vc-json]

#### Data: archives.json

Now save the output to a file called `_data/archives.json`.

* [gitlab.com/.../_data/archives.json][tutor-archives-json]

{{< highlight json >}}
{
  
    "15032535": {
      "title": "Deftones - Be Quiet and Drive",
      "url":   "/lyrics/deftones-be-quiet-and-drive/"
    },
  
    "18091335": {
      "title": "Disturbed - Stupify",
      "url":   "/lyrics/disturbed-stupify/"
    },
  
    "15071535": {
      "title": "Marilyn Manson - Sweet Dreams",
      "url":   "/lyrics/eurythmics-sweet-dreams/"
    },
  
    "18011535": {
      "title": "House of Pain - Jump Around",
      "url":   "/lyrics/house-of-pain-jump-around/"
    },
  
    "15072535": {
      "title": "Marilyn Manson - Redeemer",
      "url":   "/lyrics/marylin-manson-redeemer/"
    },
  ...
}
{{< / highlight >}}

![11ty: Data archive.json][image-ss-06-vim-json]

We are going to use this data in later chapter.

-- -- --

### What is Next ?

Consider continue reading [ [Eleventy - Nunjucks - Layout][local-whats-next] ].
We are going to revisit layouts, foucing on columns and colors.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/01/16/11ty-nunjucks-layout/
[local-bulma]:          {{< baseurl >}}frontend/2019/03/01/bulma-overview/
[permalinks]:           https://www.11ty.io/docs/permalinks/#custom-file-formats

[tutor-html-master-06]: {{< tutor-11ty-bulma-md >}}/tutor-06/

[tutor-configuration]:  {{< tutor-11ty-bulma-md >}}/tutor-06/.eleventy.js
[tutor-archives-json]:  {{< tutor-11ty-bulma-md >}}/tutor-06/views/_data/archives.json
[tutor-vc-json]:        {{< tutor-11ty-bulma-md >}}/tutor-06/views/pages/archive-json.html

[image-vc-json]:        {{< assets-ssg >}}/2020/01/06-pages-archive-json.png
[image-ss-06-vim-json]: {{< assets-ssg >}}/2020/01/06-vim-archive-json.png
