---
type   : post
title  : "Eleventy - Nunjucks - Layouts"
date   : 2020-01-16T09:17:35+07:00
slug   : 11ty-nunjucks-layout
categories: [ssg]
tags      : [11ty]
keywords  : [nunjucks, layout, columns, color, panel, sidebar]
author : epsi
opengraph:
  image: assets-ssg/2020/01/07-nerdtree-layouts.png

toc    : "toc-2020-01-11ty-bulma-md-step"

excerpt:
  Building 11ty Site Step by step, with Bulma MD as stylesheet frontend.
  Columns and Colors with Nunjucks Layout in Eleventy.
---

### Preface

> Goal: Columns and Colors with Nunjucks Layout in Eleventy.

#### Source Code

This article use [tutor-07][tutor-html-master-07] theme.
We will create it step by step.

#### Layout Preview for Tutor 07

![Nunjucks: Layout Preview for Tutor 07][template-inheritance-07]

[template-inheritance-07]:  {{< assets-ssg >}}/2020/02/07-template-inheritance.png

-- -- --

### 1: Single Column

We meet again with layout, for the third time.
We require both column kind:

* Single Column: child of Base Layout.

* Double Columns: child of Base Layout.

We are going to get rid unused sidebar.
So that all pages will be a `column-single` layout,
except `page` layout and `post` layout.

The directory tree can be shown as below figure:

![11ty: NERDTree: A Few Layouts][image-nerdtree-layout]

#### Parent Layout: Nunjucks Single Column

Consider create a parent layout for single column layout.
This layout is equipped with two blocks: `blog_header`, and `pagination`.

* [gitlab.com/.../views/_includes/layouts/column-single.njk][tutor-vl-single]

{{< highlight jinja >}}
{% extends "layouts/base.njk" %}

{% block main %}
{%- set color %}{% block main_color %}blue{% endblock %}{% endset -%}

  <main role="main" 
        class="column is-full">
    <section class="main-wrapper {{ color }}">
      <div class="blog white z-depth-3 hoverable">

        <section class="blog-header has-text-centered
                      {{ color }} lighten-5">
        {% block blog_header %}
          <div class="main_title"> 
            <h2 class="title is-4"
                itemprop="name headline">
              {{ renderData.title or title or metadata.title }}</h2>
          </div>
        {% endblock %}
        </section>

        {%- if showPagination -%}
        <section class="blog-index center-align" itemprop="pagination">
        {% block pagination %}{% endblock %}
        </section>
        {%- endif -%}

        <section class="blog-index" itemprop="ItemList">
          {{ content | safe }}
        </section>

      </div>
    </section>
  </main>
{% endblock %}
{{< / highlight >}}

We will use `pagination` block later.

#### Difference

Block `blog_header` is simply default content as below:

{{< highlight jinja >}}
{% block blog_header %}
  <div class="main_title"> 
    <h2 class="title is-4" itemprop="name headline">
      {{ renderData.title or title or metadata.title }}</h2>
  </div>
{% endblock %}
{{< / highlight >}}

And the `pagination` block is just an empty block.

{{< highlight jinja >}}
{%- if showPagination -%}
  <section class="blog-index center-align" itemprop="pagination">
  {% block pagination %}{% endblock %}
  </section>
{%- endif -%}
{{< / highlight >}}

#### Default Color

The default color for `main` block is set as below code:

{{< highlight jinja >}}
{%- set color %}{% block main_color %}blue{% endblock %}{% endset -%}
{{< / highlight >}}

-- -- --

### 2: Double Columns

Most of the layout remain the same,
except the `blog_header` part using external nunjucks partial.

#### Parent Layout: Nunjucks Double Columns

Consider modify the parent layout for double column layout.

The `main` block in this layout is,
equipped with one `blog_header` blocks:

* [gitlab.com/.../views/_includes/layouts/columns-double.njk][tutor-vl-double]

{{< highlight jinja >}}
{% extends "layouts/base.njk" %}

{% block main %}
{%- set color %}{% block main_color %}blue{% endblock %}{% endset -%}

  <main role="main" 
        class="column is-two-thirds">

    <section class="main-wrapper {{ color }}">
      <div class="blog white z-depth-3 hoverable">

        <section class="blog-header {{ color }} lighten-5">
        {% block blog_header %}
          {% include "page/blog-header.njk" %}
        {% endblock %}
        </section>

        <article class="blog-body" itemprop="articleBody">
          {{ content | safe }}
        </article>

      </div>
    </section>

  </main>
{% endblock %}
{{< / highlight >}}

The `aside` block in this layout is remain intact.

{{< highlight jinja >}}
{% block aside %}
{%- set color %}{% block aside_color %}green{% endblock %}{% endset -%}

  <aside class="column is-one-thirds">

    <section class="aside-wrapper {{ color }}">
      <div class="widget white z-depth-3 hoverable">

        <div class="widget-header {{ color }} lighten-4">
        {% block widget_header %}
          <strong>Side Menu</strong>
          <span class="fas fa-scroll is-pulled-right"></span>
        {% endblock %}
        </div>

        <div class="widget-body">
        {% block widget_body %}
          <p>This is a widget body.</p>
        {% endblock %}
        </div>

      </div>
    </section>

  </aside>
{% endblock %}
{{< / highlight >}}

#### Difference

The only difference is the `blog_header` block,
now include an external nunjucks partial.

{{< highlight jinja >}}
<section class="blog-header {{ color }} lighten-5">
{% block blog_header %}
  {% include "page/blog-header.njk" %}
{% endblock %}
</section>
{{< / highlight >}}

#### Partial: Page: Blog Header

* [gitlab.com/.../views/_includes/page/blog-header.njk][tutor-vi-pa-blog-header]

{{< highlight jinja >}}
  <div class="main_title"> 
    <h2 class="title is-4" itemprop="name headline">
      <a href="{{ title or metadata.title }}"
        >{{ title or metadata.title }}</a></h2>
  </div>
{{< / highlight >}}

#### Default Color

The default color for `main` block is set as below code:

{{< highlight jinja >}}
{%- set color %}{% block main_color %}blue{% endblock %}{% endset -%}
{{< / highlight >}}

While the default color for `aside` block is set as below code:

{{< highlight jinja >}}
{%- set color %}{% block aside_color %}green{% endblock %}{% endset -%}
{{< / highlight >}}

-- -- --

### 3: Page and Post

Consider have applying the template for `page`  kind and `post` kind.

The example of color setting for each pages,
can be shown as below figure.
Most default color cab be overriden in frontmatter.

![11ty: NERDTree: A Few Pages][image-nerdtree-pages]

#### Layout: Nunjucks Page

We can override the default color.

* [gitlab.com/.../views/_includes/layouts/page.njk][tutor-vl-page]

{{< highlight jinja >}}
{% extends "layouts/columns-double.njk" %}

{% block main_color %}{{ color or 'pink' }}{% endblock %}
{% block aside_color %}green{% endblock %}
{{< / highlight >}}

#### Layout: Nunjucks Post

The `blog_header` part has external partials..
Also overriding the default color.

* [gitlab.com/.../views/_includes/layouts/post.njk][tutor-vl-post]

{{< highlight jinja >}}
{% extends "layouts/columns-double.njk" %}

{% block main_color %}{{ color or 'blue' }}{% endblock %}
{% block aside_color %}grey{% endblock %}

{% block blog_header %}
  {% include "post/blog-header.njk" %}
{% endblock %}
{{< / highlight >}}

It is mandatory to set both colors for `main` and `aside`.
Unless you redefine `main` block or `aside` block completely.

#### Partial: Post: Blog Header

* [gitlab.com/.../views/_includes/post/blog-header.njk][tutor-vi-po-blog-header]

{{< highlight jinja >}}
  <div class="main_title"> 
    <h2 class="title is-4" itemprop="name headline">
      <a href="{{ page.url | url }}">
        {{ renderData.title or title or metadata.title }}
      </a></h2>
    <b>{{ page.date | date('MMMM Do, YYYY') }}</b>
  </div>
{{< / highlight >}}

#### Page Content

Since we are not going to use,
default color for pages and post,
just leave the the content intact.
All pages will have `pink` color,
and all post will have `blue` color.
However if you wish for an example,
you can examine this content below:

* [gitlab.com/.../views/quotes/jerry-maguire.md][tutor-vc-po-maguire]

{{< highlight markdown >}}
---
layout    : post
title     : Jerry Maguire
date      : 2015-01-01 17:35:15
tags      : ['subtitle', 'story']
color     : red
---

You had me at Hello.

<!--more-->

You complete me.
{{< / highlight >}}

![11ty: Post Content: quotes/jerry-maguire][image-vc-po-maguire]

As a summary, the color will inherit,
from `jerry-maguire.md` frontmatter,
to `post` layout, then to `double-columns` layout.

![11ty: Color Property Propagation][image-color-inherit]

-- -- --

### 4: Child: Archive, Blog, Tags, Tag Name

With the same logic, we can apply colors to all other pages as well.

#### Layout: Nunjucks Archive

We can override the default color.

* [gitlab.com/.../views/_includes/layouts/archive.njk][tutor-vl-archive]

{{< highlight jinja >}}
{% extends "layouts/column-single.njk" %}

{% block main_color %}{{ color or 'cyan' }}{% endblock %}
{{< / highlight >}}

#### Layout: Nunjucks Blog

We can override the default color.

* [gitlab.com/.../views/_includes/layouts/blog.njk][tutor-vl-blog]

{{< highlight jinja >}}
{% extends "layouts/column-single.njk" %}

{% block main_color %}{{ color or 'brown' }}{% endblock %}
{{< / highlight >}}

#### Layout: Nunjucks Tags

We can override the default color.

* [gitlab.com/.../views/_includes/layouts/tags.njk][tutor-vl-tags]

{{< highlight jinja >}}
{% extends "layouts/column-single.njk" %}

{% block main_color %}{{ color or 'light-blue' }}{% endblock %}
{{< / highlight >}}

#### Layout: Nunjucks Tag Name

We can override the default color.

* [gitlab.com/.../views/_includes/layouts/tag-name.njk][tutor-vl-tag-name]

{{< highlight jinja >}}
{% extends "layouts/column-single.njk" %}

{% block main_color %}{{ color or 'indigo' }}{% endblock %}
{{< / highlight >}}

#### Page Content: Archive

Now we can set for both archive pages.

* [gitlab.com/.../views/pages/archive-by-year.html][tutor-vc-pa-by-year]

{{< highlight jinja >}}
---
layout    : archive
title     : Archive by Year
eleventyExcludeFromCollections: true
color     : cyan
---

{% set posts = collections.posts %}
{% include "index/by-year.njk" %}
{{< / highlight >}}

and

* [gitlab.com/.../views/pages/archive-by-month.html][tutor-vc-pa-by-month]

{{< highlight jinja >}}
---
layout    : archive
title     : Archive by Month
eleventyExcludeFromCollections: true
color     : cyan
---

{% set posts = collections.posts %}
{% include "index/by-month.njk" %}
{{< / highlight >}}

#### Page Content: Blog

To use default color,
just leave the blog page intact, 
or just comment the color setting in frontmatter.

* [gitlab.com/.../views/pages/blog.html][tutor-vc-pa-blog]

{{< highlight jinja >}}
---
layout    : index
title     : Blog List
eleventyExcludeFromCollections: true
# color   : blue
---

{%- set posts = collections.posts -%}
{%- set posts = posts | sort(false, true, 'date') | reverse -%}
{% include "index/blog-list.njk" %}
{{< / highlight >}}

#### Page Content: Tags 

Or you can have a choice whether,
the content have different color.

* [gitlab.com/.../views/tags.html][tutor-vc-pa-tags]

{{< highlight jinja >}}
---
layout    : tags
title     : List of Tags
eleventyExcludeFromCollections: true
# color     : blue
---

{% set color = 'lime' %}
{% include "index/tags-badge.njk" %}
{% include "index/tags-tree.njk" %}
{{< / highlight >}}

![11ty: Page Content: Tags with different Content Colors][image-vc-pa-tags]

#### Page Content: Tag Name

Or you whatever color you want.

* [gitlab.com/.../views/tag-name.html][tutor-vc-pa-tag-name]

{{< highlight jinja >}}
---
layout    : tag-name
...
color     : indigo
---

{# set color = 'indigo' #}
{% set posts = collections[ tag ] %}
...
{{< / highlight >}}

-- -- --

### 5: Child: Home

Of course you can still also access `base` layout directly.

#### Layout: Nunjucks Home

This way you still have total control of your `HTML layout`.

* [gitlab.com/.../views/_includes/layouts/home.njk][tutor-vl-home]

{{< highlight jinja >}}
{% extends "layouts/base.njk" %}

{% block main %}
  {%- set color %}{{ color or 'blue-grey' }}{% endset -%}
  <main role="main" 
        class="column is-full">

    <section class="main-wrapper {{ color }}">
      <div class="blog white z-depth-3 hoverable">

        <article class="blog-body has-text-centered"
                 itemprop="articleBody">
          {{ content | safe }}
        </article>

      </div>
    </section>

  </main>
{% endblock %}
{{< / highlight >}}

#### Page Content: Landing Page

The content is remain untouched.

* [gitlab.com/.../views/index.html][tutor-vc-pa-home]

{{< highlight html>}}
---
layout    : home
eleventyExcludeFromCollections: true
color     : indigo
---

  <br/>
...
{{< / highlight >}}

-- -- --

### 6: Summary

As a summary, here is columns and colors.

#### Template Inheritance

Parent-child relation:

1. Single Column: extend Base

2. Double Columns: extend Base

* Page: extend Double Columns

* Post: extend Double Columns

* Archive: extend Single Column

* Blog: extend Single Column

* Tags, Tag Name: extend Single Column

* Home: extend Base

#### Default Color Variable Propagation

The default colors set as below:

1. Single Column: extend Base

{{< highlight jinja >}}
{% block main %}
  {%- set color %}{% block main_color %}blue{% endblock %}{% endset -%}
  ...
{% endblock %}
{{< / highlight >}}

2. Double Columns: extend Base

{{< highlight jinja >}}
{% block main %}
  {%- set color %}{% block main_color %}blue{% endblock %}{% endset -%}
  ...
{% endblock %}

{% block aside %}
  {%- set color %}{% block aside_color %}green{% endblock %}{% endset -%}
  ...
{% endblock %}
{{< / highlight >}}

* Page: extend Double Columns

{{< highlight jinja >}}
  {% block main_color %}{{ color or 'pink' }}{% endblock %}
  {% block aside_color %}green{% endblock %}
{{< / highlight >}}

* Post: extend Double Columns

{{< highlight jinja >}}
  {% block main_color %}{{ color or 'blue' }}{% endblock %}
  {% block aside_color %}grey{% endblock %}
{{< / highlight >}}

* Archive: extend Single Column

{{< highlight jinja >}}
  {% block main_color %}{{ color or 'cyan' }}{% endblock %}
{{< / highlight >}}

* Blog: extend Single Column

{{< highlight jinja >}}
  {% block main_color %}{{ color or 'brown' }}{% endblock %}
{{< / highlight >}}

* Tags: extend Single Column

{{< highlight jinja >}}
  {% block main_color %}{{ color or 'light-blue' }}{% endblock %}
{{< / highlight >}}

* Tag Name: extend Single Column

{{< highlight jinja >}}
  {% block main_color %}{{ color or 'indigo' }}{% endblock %}
{{< / highlight >}}

* Home: extend Base

{{< highlight jinja >}}
  {%- set color %}{{ color or 'blue-grey' }}{% endset -%}
{{< / highlight >}}

#### Overriding Color

For both content and layout,
just set in frontmatter as below example:

{{< highlight jinja >}}
---
...
color     : grey-blue
---
{{< / highlight >}}

For content override only and omitting layout color override,
Just set in content body as below example:

{{< highlight jinja >}}
---
...
---

{% set color = 'grey-blue' %}
...
{{< / highlight >}}

The later enable you to have two different colors,
between the layout color, and the content color.
We will see more content color in the next chapter.

-- -- --

### What is Next ?

Consider continue reading [ [Eleventy - Nunjucks - Index List][local-whats-next] ].
We are going to make index pages with responsive colored box.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/01/17/11ty-nunjucks-index-list/

[tutor-html-master-07]: {{< tutor-11ty-bulma-md >}}/tutor-07/

[tutor-vl-double]:      {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/layouts/columns-double.njk
[tutor-vl-single]:      {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/layouts/column-single.njk
[tutor-vl-page]:        {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/layouts/page.njk
[tutor-vl-post]:        {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/layouts/post.njk
[tutor-vl-home]:        {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/layouts/home.njk
[tutor-vl-blog]:        {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/layouts/blog.njk
[tutor-vl-archive]:     {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/layouts/archive.njk
[tutor-vl-tags]:        {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/layouts/tags.njk
[tutor-vl-tag-name]:    {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/layouts/tag-name.njk

[tutor-vi-pa-blog-header]:  {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/page/blog-header.njk
[tutor-vi-po-blog-header]:  {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/post/blog-header.njk

[tutor-vc-po-maguire]:  {{< tutor-11ty-bulma-md >}}/tutor-07/views/quotes/jerry-maguire.md
[tutor-vc-pa-blog]:     {{< tutor-11ty-bulma-md >}}/tutor-07/views/pages/blog.html
[tutor-vc-pa-home]:     {{< tutor-11ty-bulma-md >}}/tutor-07/views/index.html
[tutor-vc-pa-by-year]:  {{< tutor-11ty-bulma-md >}}/tutor-06/views/pages/archive-by-year.html
[tutor-vc-pa-by-month]: {{< tutor-11ty-bulma-md >}}/tutor-06/views/pages/archive-by-month.html
[tutor-vc-pa-tags]:     {{< tutor-11ty-bulma-md >}}/tutor-07/views/tags.html
[tutor-vc-pa-tag-name]: {{< tutor-11ty-bulma-md >}}/tutor-07/views/tag-name.html

[image-vc-json]:        {{< assets-ssg >}}/2020/01/06-pages-archive-json.png
[image-ss-06-vim-json]: {{< assets-ssg >}}/2020/01/06-vim-archive-json.png

[image-vc-po-maguire]:  {{< assets-ssg >}}/2020/01/07-post-maguire.png
[image-vc-pa-tags]:     {{< assets-ssg >}}/2020/01/07-page-tags.png
[image-nerdtree-layout]:{{< assets-ssg >}}/2020/01/07-nerdtree-layouts.png
[image-nerdtree-pages]: {{< assets-ssg >}}/2020/01/07-nerdtree-pages.png
[image-color-inherit]:  {{< assets-ssg >}}/2020/01/07-color-inheritance.png
