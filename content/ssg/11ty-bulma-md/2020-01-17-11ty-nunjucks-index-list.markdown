---
type   : post
title  : "Eleventy - Nunjucks - Index List"
date   : 2020-01-17T09:17:35+07:00
slug   : 11ty-nunjucks-index-list
categories: [ssg]
tags      : [11ty]
keywords  : [nunjucks, layout, columns, color, multiline, responsive]
author : epsi
opengraph:
  image: assets-ssg/2020/01/07-multiline-tags-mobile.png

toc    : "toc-2020-01-11ty-bulma-md-step"

excerpt:
  Building 11ty Site Step by step, with Bulma MD as stylesheet frontend.
  Reponsive column for index and colors in content with Nunjucks Layout.
---

### Preface

> Goal: Reponsive column for index and colors in content with Nunjucks Layout.

#### Source Code

This article use [tutor-07][tutor-html-master-07] theme for colors,
and additionaly [tutor-08][tutor-html-master-08] theme
for multiline responsive content.
We will create it step by step.

-- -- --

### 1: Colors: Blog Index

> How about content?

Previously, we have manage color for parent layout.

#### Page Content: Nunjucks: pages/blog

It is simply by reading the `color` variable set in pages,
either in frontmatter or in body content, or both.

* [gitlab.com/.../views/pages/blog.html][tutor-vc-pa-blog]

{{< highlight jinja >}}
---
...
color   : lime
---

{% set color = 'pink' %}
{% include "index/blog-list.njk" %}
{{< / highlight >}}

We are going to see what is going to happend if we set color,
in both frontmatter and body content.

#### Partial: Nunjucks: Blog List

Consider, move the page content to layout as below.

* [gitlab.com/.../views/_includes/index/blog-list.njk][tutor-vl-blog-list]

{{< highlight jinja >}}

  {% set color = color or 'brown' %}

  ..

      <strong><a class="meta_link {{ color }}-text text-darken-3" 
        href="{{ post.url | url }}">{{ post.data.title }}
      </strong></a>

      {% include "index/blog-each-meta.njk" %}

      ...

      <div class="read-more is-light">
        <a href="{{ post.url | url }}" 
           class="button is-dark is-small {{ color }} z-depth-1 hoverable"
          >Read More&nbsp;</a>
      </div>

  ..
{{< / highlight >}}

Where the `color` is defined in content page.

#### Partial: Nunjucks: Each Meta

Each meta can be represent as `nunjucks` template below:

* [gitlab.com/.../views/_includes/index/blog-each-meta.njk][tutor-vl-each-meta]

{{< highlight jinja >}}
...
    {%- for tag in post.data.tags  -%}
      {%- set tagUrl %}/tags/{{ tag }}/{% endset -%}
      <a class="tag is-small is-dark {{ color }} z-depth-1 hoverable"
         href="{{ tagUrl | url }}"
        >{{ tag }}&nbsp;<span class="fa fa-folder"></span>
      </a>&nbsp;
    {%- endfor -%}
...
{{< / highlight >}}

Where the `color` has already set in `blog-list` layout.

#### Render: Browser

Now you can see the result in your favorite browser.

* <http://localhost:8080/pages/>

![11ty: Page Content: Blog List with Color][image-vc-pa-blog-color]

This page has `lime` color in frame, and `pink` color in content.

> But why?

#### How does it works?

In frame layout,
it simply read `pink` from the frontmatter.
and omit default `brown` value in `layout/blog.njk`.

In content,
it is also read `pink` from the frontmatter,
but get reassign to `lime` in content body.
Thus, also omit default `brown` value in `index/blog-list.njk`.

-- -- --

### 2: Colors: Other Index

For other index page you can write similarly.

I am intentionally, not giving complete codes,
because I'm going to alter code in responsive section,
later in this article.

#### Partial: Nunjucks: By Year

There is no default color here, just `{{ color }}`.
This means, setting color in page is mandatory.

* [gitlab.com/.../views/_includes/index/by-year.njk][tutor-vl-by-year]

I am intentionally, not giving complete codes,
because I'm going to alter code in responsive section,
later in this article.


{{< highlight jinja >}}
    ...
      <div class="widget-header {{ color }} lighten-4">

        <div class ="anchor-target archive-year" 
             id="{{ year }}">{{ year }}</div>

      </div>
    ...
{{< / highlight >}}

If you want your code to be safe, you can add default color.

{{< highlight jinja >}}
  {% set color = color or 'blue' %}
{{< / highlight >}}

I think it is better to have safer code.
This example mean to be just an example.

#### Partial: Nunjucks: By Month

There is also, no default color here, just `{{ color }}`.
Therefore, setting color in page is mandatory.

* [gitlab.com/.../views/_includes/index/by-month.njk][tutor-vl-by-month]

I am intentionally, not giving complete codes,
because I'm going to alter code in responsive section,
later in this article.

{{< highlight jinja >}}
      ...
      <div class="widget white z-depth-1 hoverable m-b-10">
        <div class="widget-header {{ color }} lighten-4">

          <div class ="archive-month" 
               id="{{ year }}-{{ month }}">
            {{ postsInMonth[0].monthtext }}</div>

        </div>
      ...
{{< / highlight >}}

Again, if you want your code to be safe, you can add default color.

{{< highlight jinja >}}
  {% set color = color or 'blue' %}
{{< / highlight >}}

I think it is better to have safer code.
This example mean to be just an example.

#### Partial: Nunjucks: Tags Badge

This below is a complete code.

* [gitlab.com/.../views/_includes/index/tags-badge.njk][tutor-vl-tags-badge]

{{< highlight jinja >}}
  {% set color = color or 'blue' %}

  <div class="field is-grouped is-grouped-multiline">
  {%- for tag in collections.tagList -%}
    {%- set postsList = collections[ tag ] -%}
    {%- set listCount = postsList.length -%}
    {%- set tagUrl %}/tags/{{ tag | slug }}/{% endset -%}

    {% if listCount %}
    <div class="tags has-addons">
      <a href="{{ tagUrl | url }}">
        <div class="tag is-light {{ color }} lighten-2 z-depth-1"
          >{{ tag }} 
        </div><div class="tag is-dark {{ color }} darken-2 z-depth-1"
          >{{ listCount }}
        </div>
      </a>
    </div>
    &nbsp;
    {% endif %}
  {%- endfor -%}
    <div class="tags dummy"></div>
  </div>
{{< / highlight >}}

#### Partial: Nunjucks: Tags Tree

I am intentionally, not giving complete codes,
because I'm going to alter code in responsive section,
later in this article.

* [gitlab.com/.../views/_includes/index/tags-tree.njk][tutor-vl-tags-tree]

{{< highlight jinja >}}

  {% set color = color or 'blue' %}

    ...
      <div class="widget-header {{ color }} lighten-4">

        <div id="{{ tag | slug }}" class ="anchor-target">
          <span class="fa fa-tag"></span> 
          &nbsp; {{ tag }}
        </div>

      </div>
    ...
{{< / highlight >}}

-- -- --

### 3: Responsive: Index

Consider fast forward to [tutor-08][tutor-html-master-08] theme
for multiline responsive content.
We need to fast forward, because we need to see the difference.

In `Bulma`, there is a `is-multiline` class,
that enable each box to flow downward if the page width is not enough.
The later image will explain better, than explaining by word.

#### General Preview

Our base layout is based on responsive design.
This below is general preview of,
what responsive content that we want to achieve.

![11ty: General Preview of Responsive Content Design][image-png-layout-content]

Source image is available in inkscape SVG format,
so you can modify, and make your own preview quickly.

* [Responsive Content: Image Source][image-svg-layout-content]

Just keep in mind that,
responsive content is different with responsive page.
The responsive content is placed inside a responsive page.

#### Using Multiline Class

Consider this code below:

{{< highlight html >}}
<div class="columns is-multiline p-y-5" id="archive">
{%- for year, postsInYear in groupByYear -%}
  <section class="column is-full-mobile
              is-half-tablet is-one-third-widescreen">
    ...
  </section>
{%- endfor -%}
</div>
{{< / highlight >}}

This code has:

* One Column in mobile screen: Full.

* Two Columns in tablet screen: Half.

* Three Columns in tablet screen: One Third.

You can make your own arrangement.

#### Partial: Nunjucks: By Year

* [gitlab.com/.../views/_includes/index/by-year.njk][tutor-08-by-year]

{{< highlight jinja >}}
{%- set posts = posts | mapdate -%}
{%- set groupByYear = posts | groupBy('year') | dictsort | reverse -%}

<div class="columns is-multiline p-y-5" id="archive">
{%- for year, postsInYear in groupByYear -%}
  <section class="column is-full-mobile
              is-half-tablet is-one-third-widescreen">

    <div class="widget white z-depth-1 hoverable m-b-10">
      <div class="widget-header {{ color }} lighten-4">

        <div class ="anchor-target archive-year" 
             id="{{ year }}">{{ year }}</div>

      </div>
      <div class="widget-body">

        <div class="archive-list p-y-5">
          {%- for post in postsInYear | sort(false, true, 'month') -%}
            {% include "index/each-post.njk" %}
          {%- endfor -%}
        </div>

      </div>
    </div>

  </section>
{%- endfor -%}
</div>
{{< / highlight >}}

![11ty: Responsive Content: Multiline Archive by Year][image-vc-by-year]

#### Change: Nunjucks: By Year

I simply change the section.

{{< highlight html >}}
<div id="archive">
{%- for year, postsInYear in groupByYear -%}
  <section>
    ...
  </section>
{%- endfor -%}
</div>
{{< / highlight >}}

Into this box frame layout below:

{{< highlight html >}}
<div class="columns is-multiline p-y-5" id="archive">
{%- for year, postsInYear in groupByYear -%}
  <section class="column is-full-mobile
              is-half-tablet is-one-third-widescreen">
    ...
  </section>
{%- endfor -%}
</div>
{{< / highlight >}}

#### Partial: Nunjucks: By Month

* [gitlab.com/.../views/_includes/index/by-month.njk][tutor-08-by-month]

{{< highlight jinja >}}
{%- set posts = posts | mapdate -%}
{%- set groupByYear = posts | groupBy('year') | dictsort | reverse -%}

{%- for year, postsInYear in groupByYear -%}
<div id="archive">
  <div>
    <div class ="anchor-target archive-year p-b-5" 
         id="{{ year }}">
      {%- if year == (metadata.now | date("Y")) -%}
        <strong>This year's posts ({{ year }})</strong>
      {%- else -%}
        <strong>{{ year }}</strong>
      {%- endif -%}
    </div>

    {%- set groupByMonth = postsInYear | groupBy('month') -%}
    {%- set groupByMonth = groupByMonth | dictsort | reverse -%}

    <div class="columns is-multiline p-y-5">
    {%- for month, postsInMonth in groupByMonth -%}
    <section class="column is-full-mobile
              is-half-tablet is-one-third-widescreen">

      <div class="widget white z-depth-1 hoverable m-b-10">
        <div class="widget-header {{ color }} lighten-4">

          <div class ="archive-month" 
               id="{{ year }}-{{ month }}">
            {{ postsInMonth[0].monthtext }}</div>

        </div>
        <div class="widget-body">

          <div class="archive-list">
            {%- for post in postsInMonth | sort(false, true, 'month') -%}
              {% include "index/each-post.njk" %}
            {%- endfor -%}
          </div>

        </div>
      </div>

    </section>
    {%- endfor -%}
    </div>

  </div>

{%- endfor -%}
</div>
{{< / highlight >}}

![11ty: Responsive Content: Multiline Archive by Month][image-vc-by-month]

#### Change: Nunjucks: By Month

This is a little bit complex.

{{< highlight html >}}
<div id="archive">
  ...
    {%- for month, postsInMonth in groupByMonth -%}
    <section class="p-y-5">
      ...
    </section>
    {%- endfor -%}
  ...
</div>
{{< / highlight >}}

Into this box frame layout below:

{{< highlight html >}}
<div id="archive">
  ...
    <div class="columns is-multiline p-y-5">
    {%- for month, postsInMonth in groupByMonth -%}
    <section class="column is-full-mobile
              is-half-tablet is-one-third-widescreen">

      ...
    </section>
    {%- endfor -%}
  ...
</div>
{{< / highlight >}}

#### Partial: Nunjucks: Tags Tree

* [gitlab.com/.../views/_includes/index/tags.njk][tutor-08-tags-tree]

{{< highlight jinja >}}

  {% set color = color or 'blue' %}

  <section class="columns is-multiline p-y-5" id="archive">
  {% for tag in collections.tagList %}
  <div class="column is-full-mobile
              is-half-tablet is-one-third-widescreen">
    {%- set postsList = collections[ tag ] -%}

    <div class="widget white z-depth-1 hoverable m-b-10">
      <div class="widget-header {{ color }} lighten-4">

        <div id="{{ tag | slug }}" class ="anchor-target">
          <span class="fa fa-tag"></span> 
          &nbsp; {{ tag }}
        </div>

      </div>
      <div class="widget-body">

        <div class="archive-list p-y-5">
        {%- for post in postsList -%}
          {% include "index/each-post.njk" %}
        {%- endfor -%}
      </div>

      </div>
    </div>

  </div>
  {%- endfor -%}
  </section>
{{< / highlight >}}

#### Change: Nunjucks: Tags Tree

This is also need carefully carved.

{{< highlight html >}}
  {% set color = color or 'blue' %}

  <section class="p-y-5" id="archive">
  {% for tag in collections.tagList %}
    {%- set postsList = collections[ tag ] -%}

    ...

  {%- endfor -%}
  </section>
{{< / highlight >}}

Into this box frame layout below:

{{< highlight html >}}

  {% set color = color or 'blue' %}

  <section class="columns is-multiline p-y-5" id="archive">
  {% for tag in collections.tagList %}
  <div class="column is-full-mobile
              is-half-tablet is-one-third-widescreen">
    {%- set postsList = collections[ tag ] -%}

    ...

  </div>
  {%- endfor -%}
  </section>
{{< / highlight >}}

#### Responsive Tags

You can also examine in figure below,
how the each screen suit this multiline layout:

* Mobile (Full): One Column

![11ty: Multiline Tags Tree in Mobile Screen][image-vc-tags-mobile]

* Tablet (Half): Two Columns

![11ty: Multiline Tags Tree in Tablet Screen][image-vc-tags-tablet]

* Wide Screen (One Third): Three Columns

![11ty: Multiline Tags Tree in Wide Screen][image-vc-tags-wide]

-- -- --

### What is Next ?

Consider continue reading [ [Eleventy - Nunjucks - Widget][local-whats-next] ].
We are going to explore miscellanous range loop,
to achieved wordpress like side panel widget.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/01/18/11ty-widget-simple-html/
[permalinks]:           https://www.11ty.io/docs/permalinks/#custom-file-formats

[tutor-html-master-07]: {{< tutor-11ty-bulma-md >}}/tutor-07/
[tutor-html-master-08]: {{< tutor-11ty-bulma-md >}}/tutor-08/

[tutor-vl-blog-list]:   {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/index/blog-list.njk
[tutor-vl-each-meta]:   {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/index/blog-each-meta.njk
[tutor-vl-by-year]:     {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/index/by-year.njk
[tutor-vl-by-month]:    {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/index/by-month.njk
[tutor-vl-tags-badge]:  {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/index/tags-badge.njk
[tutor-vl-tags-tree]:   {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/index/tags-tree.njk
[tutor-vc-pa-blog]:     {{< tutor-11ty-bulma-md >}}/tutor-07/views/pages/blog.html

[tutor-08-by-year]:     {{< tutor-11ty-bulma-md >}}/tutor-08/views/_includes/index/by-year.njk
[tutor-08-by-month]:    {{< tutor-11ty-bulma-md >}}/tutor-08/views/_includes/index/by-month.njk
[tutor-08-tags-tree]:   {{< tutor-11ty-bulma-md >}}/tutor-00/views/_includes/index/tags-tree.njk

[image-vc-pa-blog-color]:   {{< assets-ssg >}}/2020/01/07-pages-blog-color.png
[image-vc-by-year]:     {{< assets-ssg >}}/2020/01/07-multiline-archive-by-year.png
[image-vc-by-month]:    {{< assets-ssg >}}/2020/01/07-multiline-archive-by-month.png
[image-vc-tags-mobile]: {{< assets-ssg >}}/2020/01/07-multiline-tags-mobile.png
[image-vc-tags-tablet]: {{< assets-ssg >}}/2020/01/07-multiline-tags-tablet.png
[image-vc-tags-wide]:   {{< assets-ssg >}}/2020/01/07-multiline-tags-wide.png

[image-png-layout-content]: {{< baseurl >}}assets/posts/frontend/2019/12/layout-content.png
[image-svg-layout-content]: {{< baseurl >}}assets/posts/frontend/2019/12/layout-content.svg
