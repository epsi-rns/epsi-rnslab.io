---
type   : post
title  : "Eleventy - Widget - Simple HTML"
date   : 2020-01-18T09:17:35+07:00
slug   : 11ty-widget-simple-html
categories: [ssg]
tags      : [11ty]
keywords  : [nunjucks, layout, color, widget, affiliates, responsive image]
author : epsi
opengraph:
  image: assets-ssg/2020/01/07-nerdtree-widget.png

toc    : "toc-2020-01-11ty-bulma-md-step"

excerpt:
  Building 11ty Site Step by step, with Bulma MD as stylesheet frontend.
  Simple HTML to achieved wordpress like side panel widget.
---

### Preface

> Goal: Simple HTML to achieved wordpress like side panel widget.

#### Source Code

This article use [tutor-07][tutor-html-master-07] theme.
We will create it step by step.

-- -- --

### 1: Prepare

These widget will be shown in both `page` kind and `post` kind,
using `columns-double` layout.
We need to change the whole part of the `aside` block,
so that `aside` block can contain multiple widget.

#### Layout: Page

The `aside` block is depend on what `widget` you need to show.
My default arrangement can be shown as below:

* [gitlab.com/.../views/_includes/layouts/page.njk][tutor-vl-page]

{{< highlight jinja >}}
{% extends "layouts/columns-double.njk" %}

{% block main_color %}{{ color or 'pink' }}{% endblock %}

{% block aside %}
  <aside class="column is-one-thirds">
    {% include "widget/recent-post.njk" %}
    {% include "widget/tags.njk" %}
  </aside>
{% endblock %}
{{< / highlight >}}

If you want to use different widget,
such as display only `affiliates` widget,
you can change this `aside` block into this:

{{< highlight jinja >}}
  <aside class="column is-one-thirds">
    {% include "widget/affiliates.njk" %}
  </aside>
{{< / highlight >}}

#### Layout: Post

So is the `post` kind, it has `widget` on right `sidebar`,
with my default arrangement as below layout::

* [gitlab.com/.../views/_includes/layouts/post.njk][tutor-vl-post]

{{< highlight jinja >}}
{% extends "layouts/columns-double.njk" %}

{% block main_color %}{{ color or 'blue' }}{% endblock %}

{% block blog_header %}
  {% include "post/blog-header.njk" %}
{% endblock %}

{% block aside %}
  <aside class="column is-one-thirds">
    {%- if related_link_ids -%}
      {% include "widget/related-posts.njk" %}
    {%- endif -%}

    {% include "widget/archive.njk" %}
    {% include "widget/tags.njk" %}
  </aside>
{% endblock %}
{{< / highlight >}}

You are freely to change what `widget` you want to show.
I utilize unused `block` to contain all widget.

{{< highlight jinja >}}
{% block aside_widget_all %}
  <aside class="column is-one-thirds">
    {% include "widget/tags.njk" %}
    {% include "widget/recent-posts.njk" %}
    {% include "widget/archive-grouped.njk" %}
   
    {%- if related_link_ids -%}
      {% include "widget/related-posts.njk" %}
    {%- endif -%}

    {% include "widget/friends.njk" %}
    {% include "widget/archive-gitlab.njk" %}
    {% include "widget/archive-github.njk" %}
    
    {% include "widget/oto-spies.njk" %}
    {% include "widget/affiliates.njk" %}
  </aside>
{% endblock %}
{{< / highlight >}}

It is basically just making a scrathpad note for myself.
So that, I can copy paste any line from this `block`,
whenever I need to.

-- -- --

### 2: Widget Parent Class

The good thing about `nunjucks` is,
that you can `extend` any partial template, from a custom parent.

#### Parent Layout: Widget

This means you can refactor common component such as `widget`,
into a generic parent as below:

* [gitlab.com/.../views/_includes/layouts/widget.njk][tutor-vl-widget]

{{< highlight jinja >}}
<section class="aside-wrapper {{ color }}">
  <div class="widget white z-depth-3 hoverable">

    <div class="widget-header {{ color }} lighten-4">
    {% block widget_header %}
      <strong>Side Widget</strong>
      <span class="fas fa-fingerprint is-pulled-right"></span>
    {% endblock %}
    </div>
    
    <div class="widget-body">
    {% block widget_body %}
    {% endblock %}
    </div>

  </div>
</section>
{{< / highlight >}}

![11ty: NERDTree: Widget Layout][image-nerdtree-widget]

-- -- --

### 3: Simple Example

#### Layout: Nunjucks Page or Post

Consider examine only affiliate links:

{{< highlight jinja >}}
  <aside class="column is-one-thirds">
    {% include "widget/affiliates.njk" %}
  </aside>
{{< / highlight >}}

#### Partial Widget: HTML Affiliates

Now we can use above template,
for our simple widget example as below code:

* [gitlab.com/.../views/_includes/widget/affiliates.njk][tutor-vw-affiliates]

{{< highlight jinja >}}
{% extends "layouts/widget.njk" %}

{% set color = 'green' %}

{% block widget_header %}
  <strong>Affiliates</strong>
  <span class="fas fa-fingerprint is-pulled-right"></span>
{% endblock %}

{% block widget_body %}
  <ul class="widget-list">
    <li><a href="http://epsi-rns.github.io/"
          >Linux/BSD Desktop Customization</a></li>
    <li><a href="http://epsi-rns.gitlab.io/"
          >Mobile/Web Development Blog</a></li>
    <li><a href="http://oto-spies.info/"
          >Car Painting and Body Repair.</a></li>
  </ul>
{% endblock %}
{{< / highlight >}}

This the basic of HTML class required for the rest of this article.

#### Render: Browser

You can open `page` kind, or `post` kind, to test this `widget`.

![11ty: Widget: Affiliates][image-wi-affiliates]

-- -- --

### 4: Responsive Image

No need any additional setting,
Bulma has already use `img { max-width: 100%}` by default.
So you can have your own commercial advertisement.

#### Layout: Nunjucks Page or Post

Consider examine only commercial links:

{{< highlight jinja >}}
  <aside class="column is-one-thirds">
    {% include "widget/oto-spies.njk" %}
  </aside>
{{< / highlight >}}

#### Partial Widget: HTML Oto Spies

* [gitlab.com/.../views/_includes/widget/oto-spies.njk][tutor-vw-oto-spies]

{{< highlight jinja >}}
{% extends "layouts/widget.njk" %}

{% set color = 'grey' %}

{% block widget_header %}
  <strong>Car Painting and Body Repair</strong>
  <span class="fas fa-car-side is-pulled-right"></span>
{% endblock %}

{% block widget_body %}
  <div class="center-align">
    <a href="http://oto-spies.info/">
      <img src="{{ "/assets/images/adverts/oto-spies-01.png" | url }}"
           alt="Oto Spies"></a>
  </div>
{% endblock %}
{{< / highlight >}}

Pick the right color, that match your business.

#### Render: Browser

You can open `page` kind, or `post` kind, to test this `widget`.

![11ty: Widget: Oto Spies][image-wi-oto-spies]

That both two example use pure HTML.
Now we need to leverage to use loop with `nunjucks`,
and also using static data with `nunjucks`.

-- -- --

### What is Next ?

Consider continue reading [ [Eleventy - Widget - Nunjucks Loop][local-whats-next] ].
We are going to explore nunjucks's loop with widget.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/01/19/11ty-widget-nunjucks-loop/

[tutor-html-master-07]: {{< tutor-11ty-bulma-md >}}/tutor-07/

[tutor-configuration]:  {{< tutor-11ty-bulma-md >}}/tutor-07/.eleventy.js
[tutor-vl-page]:        {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/layouts/page.njk
[tutor-vl-post]:        {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/layouts/post.njk
[tutor-vl-widget]:      {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/layouts/widget.njk
[tutor-vw-affiliates]:  {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/widget/affiliates.njk
[tutor-vw-recent-posts]:{{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/widget/recent-posts.njk
[tutor-vw-tags]:        {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/widget/tags.njk

[image-wi-affiliates]:  {{< assets-ssg >}}/2020/01/07-widget-affiliates.png
[image-wi-oto-spies]:   {{< assets-ssg >}}/2020/01/07-widget-oto-spies.png
[image-nerdtree-widget]:{{< assets-ssg >}}/2020/01/07-nerdtree-widget.png
