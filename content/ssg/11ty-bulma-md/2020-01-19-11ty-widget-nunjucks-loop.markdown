---
type   : post
title  : "Eleventy - Widget - Nunjucks Loop"
date   : 2020-01-19T09:17:35+07:00
slug   : 11ty-widget-nunjucks-loop
categories: [ssg]
tags      : [11ty]
keywords  : [nunjucks, widget, recent post, tags, archives]
author : epsi
opengraph:
  image: assets-ssg/2020/01/07-widget-archive-grouped.png

toc    : "toc-2020-01-11ty-bulma-md-step"

excerpt:
  Building 11ty Site Step by step, with Bulma MD as stylesheet frontend.
  Miscellanous range loop to achieved wordpress like side panel widget.
---

### Preface

> Goal: Miscellanous range loop to achieved wordpress like side panel widget.

#### Source Code

This article use [tutor-07][tutor-html-master-07] theme.
We will create it step by step.

-- -- --

### 5: Recent Posts

This require a new filter in main configuration.

#### Filter: Limit

Since nunjucks does not have a builty in function for limit.
We need to add `limit` filter in `.eleventy.js`:

* [gitlab.com/.../.eleventy.js][tutor-configuration]

{{< highlight javascript >}}
  // Limit Filter: Copy paste from Jérôme Coupé
  eleventyConfig.addNunjucksFilter("limit", function(array, limit) {
    return array.slice(0, limit);
  });
{{< / highlight >}}

Later you can use this filter as code below:

{{< highlight jinja >}}
  {% set posts = collections.posts | limit(5) %}
{{< / highlight >}}

You can see how flexible nunjucks is.

#### Layout: Nunjucks Page or Post

Consider examine only recent posts:

{{< highlight jinja >}}
  <aside class="column is-one-thirds">
    {% include "widget/recent-posts.njk" %}
  </aside>
{{< / highlight >}}

#### Partial Widget: Nunjucks Recent Post

* [gitlab.com/.../views/_includes/widget/recent-posts.njk][tutor-vw-recent-posts]

{{< highlight jinja >}}
{% extends "layouts/widget.njk" %}

{% set color = 'lime' %}

{% block widget_header %}
  <strong>Recent Posts</strong>
  <span class="fa fa-newspaper is-pulled-right"></span>
{% endblock %}

{% block widget_body %}
  {% set posts = collections.posts | limit(5) %}
  <ul class="widget-list">
    {%- for post in posts -%}
    <li><a href="{{ post.url | url }}">{{ post.data.title }}</a></li>
    {%- endfor -%}
  </ul>
{% endblock %}
{{< / highlight >}}

#### The Loop

After sorting the posts array by date,
I limit the result for only first five result.

{{< highlight jinja >}}
  {% set posts = collections.posts | limit(5) %}
  <ul class="widget-list">
    {%- for post in posts -%}
    <li><a href="{{ post.url | url }}">{{ post.data.title }}</a></li>
    {%- endfor -%}
  </ul>
{{< / highlight >}}

#### Render: Browser

You can open `page` kind, or `post` kind, to test this `widget`.

![11ty: Widget: Recent Posts][image-wi-recent-posts]

-- -- --

### 6: Tags

Just a simple loop example.

#### Layout: Nunjucks Page or Post

Consider examine only tags:

{{< highlight jinja >}}
  <aside class="column is-one-thirds">
    {% include "widget/tags.njk" %}
  </aside>
{{< / highlight >}}

#### Partial Widget: Nunjucks Recent Post

* [gitlab.com/.../views/_includes/widget/tags.njk][tutor-vw-tags]

{{< highlight jinja >}}
{% extends "layouts/widget.njk" %}

{% set color = 'light-blue' %}

{% block widget_header %}
  <strong>Tags</strong>
  <span class="fa fa-tag is-pulled-right"></span>
{% endblock %}

{% block widget_body %}
  {%- for tag in collections.tagList -%}
    {%- set tagUrl %}/tags/{{ tag | slug }}/{% endset -%}
    <a class="tag is-small is-light
              {{ color }} z-depth-1 hoverable p-b-5"
       href="{{ tagUrl | url }}">
      <span class="fa fa-tag"></span>&nbsp;{{ tag }}
    </a>&nbsp;
  {%- endfor -%}
  
  <div class="clearfix"></div>
{% endblock %}
{{< / highlight >}}

#### The Loop

This is actually, only a simple loop example,
but with complex html formatting.

{{< highlight jinja >}}
  {%- for tag in collections.tagList -%}
    {%- set tagUrl %}/tags/{{ tag | slug }}/{% endset -%}
    <a class="tag is-small is-light
              {{ color }} z-depth-1 hoverable p-b-5"
       href="{{ tagUrl | url }}">
      <span class="fa fa-tag"></span>&nbsp;{{ tag }}
    </a>&nbsp;
  {%- endfor -%}
{{< / highlight >}}

#### Render: Browser

You can open `page` kind, or `post` kind, to test this `widget`.

![11ty: Widget: Tags][image-wi-tags]

-- -- --

### 7: Grouped Archives

Making a grouping archive by month and year is not hard,
it just need carefully carved with patient.
Consider this step by step procedure to make this happened:

* Grouping by Year

* Grouping by Year, then Month

* Give a Nice Formatting.

#### Filter: mapdate

We need previously discussed `mapdate` filter,
to sort `year` and `month`.

* [gitlab.com/.../.eleventy.js][tutor-configuration]

{{< highlight javascript >}}
  // Custom: Grouping by date
  eleventyConfig.addNunjucksFilter("mapdate", function(posts) {
    return posts.map(post => ({ 
      ...post,
      year:      moment(post.date).format("Y"),
      month:     moment(post.date).format("MM"),
      monthtext: moment(post.date).format("MMMM")
    }));
  });
{{< / highlight >}}

You might want to read the article again.

* [Eleventy - Custom Index - Archive][local-archive]

#### Grouping by Year

This require two loops.

1. Year Loop, using `mapdate` Filter

2. Posts Loop.

* [gitlab.com/.../views/_includes/widget/archive-year.njk][tutor-vw-ar-year]

{{< highlight jinja >}}
{% block widget_body %}

  {% set posts = collections.posts %}
  {% set page_year = page.date | date('Y')  %}

  {%- set posts = posts | mapdate  -%}
  {%- set groupByYear = posts | groupBy('year') | dictsort | reverse -%}

  {%- for year, postsInYear in groupByYear -%}
      {%- if year == page_year -%}
        <ul class="widget-list">
          {%- for post in postsInYear | sort(false, true, 'month') -%}
            <li>
              <a href="{{ post.url | url }}">
                {{ post.data.title }}
              </a>
            </li>
            {%- endfor -%}
        </ul>
      {%- endif -%}
  {%- endfor -%}

{% endblock %}
{{< / highlight >}}

![11ty: Widget: Archive This Year][image-wi-ar-year]

#### Grouping by Year, then Month

This require three loops.

1. Year Loop, using `mapdate` Filter

2. Month in Year Loop, using `mapdate` Filter

3. Posts Loop.

* [gitlab.com/.../views/_includes/widget/archive-month.njk][tutor-vw-ar-month]

{{< highlight jinja >}}
{% extends "layouts/widget.njk" %}

{% set color = 'green' %}

{% block widget_header %}
  <strong>Archive This Month</strong>
  <span class="fa fa-archive is-pulled-right"></span>
{% endblock %}

{% block widget_body %}

  {% set posts = collections.posts %}
  {% set page_year = page.date | date('Y')  %}
  {% set page_month = page.date | date('MM')  %}

  {%- set posts = posts | mapdate  -%}
  {%- set groupByYear = posts | groupBy('year') | dictsort | reverse -%}

  {%- for year, postsInYear in groupByYear -%}
      {%- if year == page_year -%}
        {%- set groupByMonth = postsInYear | groupBy('month') -%}
        {%- set groupByMonth = groupByMonth | dictsort | reverse -%}

        {%- for month, postsInMonth in groupByMonth -%}
          {%- if month == page_month -%}
            <ul class="widget-list">
            {%- for post in postsInMonth | sort(false, true, 'month') -%}

            <li>
              <a href="{{ post.url | url }}">
                {{ post.data.title }}
              </a>
            </li>
            {%- endfor -%}
            </ul>
          {%- endif -%}
        {%- endfor -%}
      {%- endif -%}
  {%- endfor -%}

{% endblock %}
{{< / highlight >}}

![11ty: Widget: Archive This Month][image-wi-ar-month]

#### Give a Nice Formatting.

Each stuff should have their own header.

* [gitlab.com/.../views/_includes/widget/archive-grouped.njk][tutor-vw-ar-grouped]

{{< highlight jinja >}}
{% extends "layouts/widget.njk" %}

{% set color = 'green' %}

{% block widget_header %}
  <strong>Archive</strong>
  <span class="fa fa-archive is-pulled-right"></span>
{% endblock %}

{% block widget_body %}

  {% set posts = collections.posts %}
  {% set page_year = page.date | date('Y')  %}
  {% set page_month = page.date | date('MM')  %}

  {%- set posts = posts | mapdate  -%}
  {%- set groupByYear = posts | groupBy('year') | dictsort | reverse -%}

  {%- for year, postsInYear in groupByYear -%}
      <div class ="archive-year p-t-5" id="{{ year }}">
        <a href="{{ "/pages/archives-by-month" | url }}#{{ year }}">
        {{ year }}</a>
      </div>

      {%- if year == page_year -%}
        {%- set groupByMonth = postsInYear | groupBy('month') -%}
        {%- set groupByMonth = groupByMonth | dictsort | reverse -%}

        <ul class="widget-archive m-t-0">
        {%- for month, postsInMonth in groupByMonth -%}
          <li class="list-month">
            <span id="{{ year }}-{{ month }}">
                  <a href="{{ "/pages/archives-by-month" | url }}#{{ year }}-{{ month }}">
                  {{ month }}</a> - {{ year }}</span>
          </li>

          {%- if month == page_month -%}
          <li>
            <ul class="widget-list">
            {%- for post in postsInMonth | sort(false, true, 'month') -%}

            <li>
              <a href="{{ post.url | url }}">
                {{ post.data.title }}
              </a>
            </li>
            {%- endfor -%}
            </ul>
          </li>
          {%- endif -%}
        {%- endfor -%}
        </ul>
      {%- endif -%}
  {%- endfor -%}

{% endblock %}
{{< / highlight >}}

![11ty: Widget: Archive Tree Grouped][image-wi-ar-grouped]

This widget is specifically made for `post` kind,
because we have already set date for each `post` kind.

-- -- --

### What is Next ?

Consider continue reading [ [Eleventy - Widget - Data][local-whats-next] ].
We are going to explore eleventy static data feature with widget.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/01/20/11ty-widget-data/
[local-archive]:        {{< baseurl >}}ssg/2020/01/12/11ty-index-archive/

[tutor-html-master-07]: {{< tutor-11ty-bulma-md >}}/tutor-07/

[tutor-configuration]:  {{< tutor-11ty-bulma-md >}}/tutor-07/.eleventy.js
[tutor-vl-page]:        {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/layouts/page.njk
[tutor-vl-post]:        {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/layouts/post.njk
[tutor-vl-widget]:      {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/layouts/widget.njk
[tutor-vw-affiliates]:  {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/widget/affiliates.njk
[tutor-vw-recent-posts]:{{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/widget/recent-posts.njk
[tutor-vw-tags]:        {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/widget/tags.njk
[tutor-vw-ar-year]:     {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/widget/archive-year.njk
[tutor-vw-ar-month]:    {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/widget/archive-month.njk
[tutor-vw-ar-grouped]:  {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/widget/archive-grouped.njk

[image-wi-recent-posts]:{{< assets-ssg >}}/2020/01/07-widget-recent-posts.png
[image-wi-tags]:        {{< assets-ssg >}}/2020/01/07-widget-tags.png
[image-wi-ar-year]:     {{< assets-ssg >}}/2020/01/07-widget-archive-year.png
[image-wi-ar-month]:    {{< assets-ssg >}}/2020/01/07-widget-archive-month.png
[image-wi-ar-grouped]:  {{< assets-ssg >}}/2020/01/07-widget-archive-grouped.png
