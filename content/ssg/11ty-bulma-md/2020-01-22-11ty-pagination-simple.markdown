---
type   : post
title  : "Eleventy - Pagination - Simple"
date   : 2020-01-22T09:17:35+07:00
slug   : 11ty-pagination-simple
categories: [ssg]
tags      : [11ty, bulma]
keywords  : [nunjucks, pagination, simple, first, last, previous, next]
author : epsi
opengraph:
  image: assets-ssg/2020/01/07-pg-content.png

toc    : "toc-2020-01-11ty-bulma-md-step"

excerpt:
  Building 11ty Site Step by step, with Bulma MD as stylesheet frontend.
  Pagination for dummies, a simple start for beginner.
---

### Preface

> Goal: A Simple Pagination.

Every journey start with a single step.

#### Source Code

This article use [tutor-07][tutor-html-master-07] theme.
We will create it step by step.

-- -- --

### 1: Preview: General

This is what we want to achieve in this tutorial.

![11ty Pagination: Simple Pagination with Navigation][image-pg-simple]

#### Layout: Nunjucks Blog

Consider use `pagination/01-simple` partial, in `blog.njk`.

* [gitlab.com/.../views/_includes/layouts/blog.njk][tutor-vl-blog]
  
{{< highlight jinja >}}
  {% include "pagination/01-simple.njk" %}
{{< / highlight >}}

#### HTML Preview

Consider fast forward to the last page,
so we can have both enabled button and disabled button.
We need those two kinds of button as an ideal example

![11ty Pagination: Enable and Disable Button][image-pg-simple-last]

The HTML that we want to achieve in this article, is similar as below.

{{< highlight html >}}
<nav class="pagination is-small is-centered"
     role="navigation" 
     aria-label="pagination">

    <!-- First Page. -->   
      <a class="pagination-previous" 
         href="/pages/index.html"
         rel="first">First</a>

    <!-- Previous Page. -->
      <a class="pagination-previous" 
         href="/pages/page-8/index.html" 
         rel="prev">Previous</a>

    <!-- Indicator Number. -->
    <a class="pagination-link"
       disabled>Page: 9 of 9</a>

    <!-- Next Page. -->
      <a class="pagination-next" 
         title="This is the last page"
         disabled="">Next</a>

    <!-- Last Page. -->
      <a class="pagination-next" 
         title="This is the last page"
         disabled="">Last</a>

    <!-- Dummy. Do not delete! -->
    <ul class="pagination-list">
    </ul>

</nav>
{{< / highlight >}}

We will achieve this with `eleventy` code.

-- -- --

### 2: Navigation: Previous and Next

{{< highlight jinja >}}
{% set totalPages = pagination.links.length %}
{% set current    = pagination.pageNumber + 1 %}

{% if totalPages > 1 %}
<nav class="pagination is-small is-centered" 
     role="navigation" aria-label="pagination">

    <!-- Previous Page. -->
    {% if pagination.previousPageLink %}
      <a class="pagination-previous hoverable"
         href="{{ pagination.previousPageLink }}" 
         rel="prev">Previous</a>
    {% else %}
      <a class="pagination-previous"
         title="This is the first page"
         disabled>Previous</a>
    {% endif %}

    <!-- Next Page. -->
    {% if pagination.nextPageLink %}
      <a class="pagination-next hoverable"
         href="{{ pagination.nextPageLink }}"
         rel="next">Next</a>
    {% else %}
      <a class="pagination-next"
         title="This is the last page"
         disabled>Next</a>
    {% endif %}

</nav>
{% endif %}

{{< / highlight >}}

#### Browser: Pagination Preview

Examine any page in blog section.

* <http://localhost:8080/pages/page-9/index.html>

![11ty Pagination: Simple Previous Next][image-pg-prev-next]

#### How does it works ?

This code above rely on 

* Previous Page: `pagination.previousPageLink`, and 

* Next Page: `pagination.nextPageLink`.

This should be self explanatory.

#### Stylesheet: Bulma Class

Bulma specific class are these below

* ul: `pagination`

* a: `pagination-previous`,
     `pagination-next`
     
* a: `disabled`

Bulma doesn't put all pagination class in `ul li`.

-- -- --

### 3: Navigation: First and Last

Consider also add first page and last page.

{{< highlight jinja >}}
{% set totalPages = pagination.links.length %}
{% set current    = pagination.pageNumber + 1 %}

{% if totalPages > 1 %}
<nav class="pagination is-small is-centered" 
     role="navigation" aria-label="pagination">

    <!-- First Page. -->
    {% if current  > 1 %}
      <a class="pagination-previous hoverable"
         href="{{ pagination.firstPageLink }}"
         rel="first">First</a>
    {% else %}
      <a class="pagination-previous"
         title="This is the first page"
         disabled>First</a>
    {% endif %}

    <!-- Last Page. -->
    {% if current  != totalPages %}
      <a class="pagination-next hoverable"
         href="{{ pagination.lastPageLink }}"
         rel="last">Last</a>
    {% else %}
      <a class="pagination-next"
         title="This is the last page"
         disabled>Last</a>
    {% endif %}

</nav>
{% endif %}
{{< / highlight >}}

#### Browser: Pagination Preview

Examine any page in blog section.

* <http://localhost:8080/blog/page-9/>

![11ty Pagination: Simple First Last][image-pg-first-last]

#### How does it works ?

`Eleventy` does not have a special method,
to show first page and last page,
so we must rely on our logic.

{{< highlight javascript >}}
{% set totalPages = pagination.links.length %}
{% set current    = pagination.pageNumber + 1 %}
{{< / highlight >}}

* First Page: `{% if current  > 1 %}`.

* Last Page: `{% if current  != totalPages %}`.

This code above rely on 

* First Page: `pagination.firstPageLink`, and 

* Last Page: `pagination.lastPageLink`.

-- -- --

### 4: Active Page Indicator

And also the pagination positioning in between,
in the middle of those.

{{< highlight jinja >}}
{% set totalPages = pagination.links.length %}
{% set current    = pagination.pageNumber + 1 %}

{% if totalPages > 1 %}
<nav class="pagination is-small is-centered" 
     role="navigation" aria-label="pagination">

    <!-- Indicator Number. -->
    <a class="pagination-link" 
       disabled>Page: {{ current }} of {{ totalPages }}</a>

</nav>
{% endif %}
{{< / highlight >}}

#### Browser: Pagination Preview

Examine any page in blog section.

* <http://localhost:4000/blog/page-9/>

![11ty Pagination: Simple Positioning Indicator][image-pg-indicator]

#### How does it works ?

This just show:

{{< highlight jinja >}}
Page: {{ current }} of {{ totalPages }}
{{< / highlight >}}

-- -- --

### 5: Dummy List

All above are pagination indicators.
We will use number later using `ul li` structure.
We can put it here, just in case we want to add number something later.

{{< highlight javascript >}}
    <!-- Dummy. Do not delete! -->
    <ul class="pagination-list">
    </ul>
{{< / highlight >}}

-- -- --

### 6: Summary

Complete code is here below:

* [gitlab.com/.../views/_includes/pagination/01-simple.njk][tutor-pg-simple]

{{< highlight jinja >}}
{% set totalPages = pagination.links.length %}
{% set current    = pagination.pageNumber + 1 %}

{% if totalPages > 1 %}
<nav class="pagination is-small is-centered" 
     role="navigation" aria-label="pagination">

    <!-- First Page. -->
    {% if current  > 1 %}
      <a class="pagination-previous hoverable"
         href="{{ pagination.firstPageLink }}"
         rel="first">First</a>
    {% else %}
      <a class="pagination-previous"
         title="This is the first page"
         disabled>First</a>
    {% endif %}

    <!-- Previous Page. -->
    {% if pagination.previousPageLink %}
      <a class="pagination-previous hoverable"
         href="{{ pagination.previousPageLink }}" 
         rel="prev">Previous</a>
    {% else %}
      <a class="pagination-previous"
         title="This is the first page"
         disabled>Previous</a>
    {% endif %}

    <!-- Indicator Number. -->
    <a class="pagination-link" 
       disabled>Page: {{ current }} of {{ totalPages }}</a>

    <!-- Next Page. -->
    {% if pagination.nextPageLink %}
      <a class="pagination-next hoverable"
         href="{{ pagination.nextPageLink }}"
         rel="next">Next</a>
    {% else %}
      <a class="pagination-next"
         title="This is the last page"
         disabled>Next</a>
    {% endif %}

    <!-- Last Page. -->
    {% if current  != totalPages %}
      <a class="pagination-next hoverable"
         href="{{ pagination.lastPageLink }}"
         rel="last">Last</a>
    {% else %}
      <a class="pagination-next"
         title="This is the last page"
         disabled>Last</a>
    {% endif %}

    <!-- Dummy. Do not delete! -->
    <ul class="pagination-list">
    </ul>

</nav>
{% endif %}
{{< / highlight >}}

#### Browser: Pagination Preview

Examine any page in blog section.

* <http://localhost:4000/blog/page-5/>

![11ty Pagination: Simple Pagination with Navigation][image-pg-simple]

-- -- --

### What is Next ?

Consider continue reading [ [Eleventy - Pagination - Number][local-whats-next] ].
We should move on to pagination number.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/01/23/11ty-pagination-number/

[tutor-html-master-07]: {{< tutor-11ty-bulma-md >}}/tutor-07/

[tutor-configuration]:  {{< tutor-11ty-bulma-md >}}/tutor-07/.eleventy.js
[tutor-vl-blog]:        {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/layouts/blog.njk
[tutor-pg-simple]:      {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/pagination/01-simple.njk

[image-pg-simple]:      {{< assets-ssg >}}/2020/01/07-pg-01-simple.png
[image-pg-simple-last]: {{< assets-ssg >}}/2020/01/07-pg-01-simple-last.png
[image-pg-prev-next]:   {{< assets-ssg >}}/2020/01/07-pg-01-simple-prev-next.png
[image-pg-first-last]:  {{< assets-ssg >}}/2020/01/07-pg-01-simple-first-last.png
[image-pg-indicator]:   {{< assets-ssg >}}/2020/01/07-pg-01-simple-indicator.png
