---
type   : post
title  : "Eleventy - Pagination - Number"
date   : 2020-01-23T09:17:35+07:00
slug   : 11ty-pagination-number
categories: [ssg]
tags      : [11ty, bulma]
keywords  : [nunjucks, pagination, number]
author : epsi
opengraph:
  image: assets-ssg/2020/01/07-pg-02-number.png

toc    : "toc-2020-01-11ty-bulma-md-step"

excerpt:
  Building 11ty Site Step by step, with Bulma MD as stylesheet frontend.
  Pagination for dummies, list all of page.
---

### Preface

> Goal: Pagination using Number.

#### Source Code

This article use [tutor-07][tutor-html-master-07] theme.
We will create it step by step.

-- -- --

### 1: Prepare

#### Layout: Nunjucks Blog

Consider use `pagination/02-number` partial, in `blog.njk`.

* [gitlab.com/.../views/_includes/layouts/blog.njk][tutor-vl-blog]
  
{{< highlight jinja >}}
  {% include "pagination/02-number.njk" %}
{{< / highlight >}}

-- -- --

### 2: Preview: General

This is what we want to achieve in this tutorial.

![11ty Pagination: Number Pagination without Navigation][image-pg-number]

#### HTML Preview

The HTML that we want to achieve in this article, is similar as below.

{{< highlight html >}}
    <ul class="pagination-list">
      <!-- Page numbers. -->

      <li>
        <a class="pagination-link hoverable"
           href="/pages/index.html"
           aria-label="Goto page 1">
          1
        </a>
      </li>

      <li>
        <a class="pagination-link hoverable"
           href="/pages/page-2/index.html"
           aria-label="Goto page 2">
          2
        </a>
      </li>

      ...

      <li>
        <a class="pagination-link hoverable"
           href="/pages/page-8/index.html"
           aria-label="Goto page 8">
          8
        </a>
      </li>

      <li>
       <a class="pagination-link is-current brown" 
           aria-label="Page 9">
          9
        </a>
      </li>

    </ul>
{{< / highlight >}}

We will achieve this with `eleventy` code.

-- -- --

### 3: Navigation: Number

Pagination by number is also simple.

#### Partial: Pagination Number

* [gitlab.com/.../views/_includes/pagination/02-number.njk][tutor-pg-number]

{{< highlight jinja >}}
{% set totalPages = pagination.links.length %}
{% set current    = pagination.pageNumber + 1 %}

{% if totalPages > 1 %}
<nav class="pagination is-small is-centered" 
     role="navigation" aria-label="pagination">

    <ul class="pagination-list">
    <!-- Page numbers. -->
    {% for cursor, link in pagination.links | hashIt %}
      <li>
      {% if cursor != current  %}
        <a class="pagination-link hoverable"
           href="{{ link }}"
           aria-label="Goto page {{ cursor }}">
          {{ cursor }}
        </a>
       {% else %}
       <a class="pagination-link is-current {{ color }}" 
           aria-label="Page {{ cursor }}">
          {{ cursor }}
        </a>
 
      {% endif %}
      </li>
    {% endfor %}
    </ul>

</nav>
{% endif %}
{{< / highlight >}}

#### Browser: Pagination Preview

![11ty Pagination: Number List][image-pg-number-last]

#### How does it works ?

Just a simple loop:

{{< highlight javascript >}}
    {% for cursor, link in pagination.links | hashIt %}
      ...
    {% endfor %}
{{< / highlight >}}

And simple conditional:

{{< highlight javascript >}}
{% set current    = pagination.pageNumber + 1 %}
...

      {% if cursor != current  %}
        <a class="pagination-link hoverable"
           href="{{ link }}">
          {{ cursor }}
        </a>
       {% else %}
       <a class="pagination-link is-current {{ color }}">
          {{ cursor }}
        </a> 
      {% endif %}
{{< / highlight >}}

There is, no `href` link for current page.

-- -- --

### What is Next ?

Consider continue reading [ [Eleventy - Pagination - Navigation][local-whats-next] ].
Addtionally, we can decorate pagination number,
with specific navigation class using Bulma.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/01/24/11ty-pagination-navigation/

[tutor-html-master-07]: {{< tutor-11ty-bulma-md >}}/tutor-07/

[tutor-configuration]:  {{< tutor-11ty-bulma-md >}}/tutor-07/.eleventy.js
[tutor-vl-blog]:        {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/layouts/blog.njk
[tutor-pg-number]:      {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/pagination/02-number.njk

[image-pg-number]:      {{< assets-ssg >}}/2020/01/07-pg-02-number.png
[image-pg-number-last]: {{< assets-ssg >}}/2020/01/07-pg-02-number-last.png
