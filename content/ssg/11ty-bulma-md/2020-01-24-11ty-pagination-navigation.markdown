---
type   : post
title  : "Eleventy - Pagination - Navigation"
date   : 2020-01-24T09:17:35+07:00
slug   : 11ty-pagination-navigation
categories: [ssg]
tags      : [11ty, bulma]
keywords  : [nunjucks, pagination, number, navigation]
author : epsi
opengraph:
  image: assets-ssg/2020/01/07-pg-02-navigation.png

toc    : "toc-2020-01-11ty-bulma-md-step"

excerpt:
  Building 11ty Site Step by step, with Bulma MD as stylesheet frontend.
  Add Navigation to pagination, along with number inside one list tag.
---

### Preface

> Goal: Add Navigation to pagination, combined with simple numbering.

#### Source Code

This article use [tutor-07][tutor-html-master-07] theme.
We will create it step by step.

-- -- --

### 1: Prepare

We are still using pagination `number`, but this time with navigation.

#### Preview: General

This is what we want to achieve in this tutorial.

![11ty Pagination: Number Pagination with Navigation][image-pg-navigation]

#### Layout: Nunjucks Blog

In `blog.njk`, you can use:

* `pagination/02-number-nav-01` partial, or

* `pagination/02-number-nav-02` partial

* `pagination/02-number-nav-03` partial
 
-- -- --  

### 2: Navigation: Outside List Style

Consider give it a complete navigation:

* `prev`, `next`, and

* `first`, `last`

#### Layout: Nunjucks Blog

{{< highlight jinja >}}
  {% include "pagination/02-number-nav-01.njk" %}
{{< / highlight >}}

#### Bulma Pagination

We can utilize example from Bulma official documentation.
Which has a very nice looks.

* [gitlab.com/.../views/_includes/pagination/02-number-nav-01.njk][tutor-pg-nav-01]

{{< highlight html >}}
{% set totalPages = pagination.links.length %}
{% set current    = pagination.pageNumber + 1 %}

{% if totalPages > 1 %}
<nav class="pagination is-small is-centered" 
     role="navigation" aria-label="pagination">

    <!-- First Page. -->
    {% if current  > 1 %}
      <a class="pagination-previous hoverable"
         href="{{ pagination.firstPageLink }}"
         rel="first">First</a>
    {% else %}
      <a class="pagination-previous"
         title="This is the first page"
         disabled>First</a>
    {% endif %}

    <!-- Previous Page. -->
    {% if pagination.previousPageLink %}
      <a class="pagination-previous hoverable"
         href="{{ pagination.previousPageLink }}" 
         rel="prev">Previous</a>
    {% else %}
      <a class="pagination-previous"
         title="This is the first page"
         disabled>Previous</a>
    {% endif %}
    
    <!-- Next Page. -->
    {% if pagination.nextPageLink %}
      <a class="pagination-next hoverable"
         href="{{ pagination.nextPageLink }}"
         rel="next">Next</a>
    {% else %}
      <a class="pagination-next"
         title="This is the last page"
         disabled>Next</a>
    {% endif %}

    <!-- Last Page. -->
    {% if current  != totalPages %}
      <a class="pagination-next hoverable"
         href="{{ pagination.lastPageLink }}"
         rel="last">Last</a>
    {% else %}
      <a class="pagination-next"
         title="This is the last page"
         disabled>Last</a>
    {% endif %}

    <ul class="pagination-list">
    <!-- Page numbers. -->
    {% for cursor, link in pagination.links | hashIt %}
      <li>
      {% if cursor != current  %}
        <a class="pagination-link hoverable"
           href="{{ link }}"
           aria-label="Goto page {{ cursor }}">
          {{ cursor }}
        </a>
       {% else %}
       <a class="pagination-link is-current {{ color }}" 
           aria-label="Page {{ cursor }}">
          {{ cursor }}
        </a>
 
      {% endif %}
      </li>
    {% endfor %}
    </ul>

</nav>
{% endif %}
{{< / highlight >}}

#### Browser: Pagination Preview

Which shown nice pagination.

![11ty Pagination: Outside List Style][image-pg-nav-01]

-- -- --

### 3: Navigation: Inline List Style

Alternatively you can wrap all the navigation,
inside `ul li` tags.

#### Layout: Nunjucks Blog

{{< highlight jinja >}}
  {% include "pagination/02-number-nav-02.njk" %}
{{< / highlight >}}

#### Bulma Pagination

Consider give it a complete navigation:

* `prev`, `next`, and

* `first`, `last`

But this time, put the navigation inside the `<li>...</li>`.

* [gitlab.com/.../views/_includes/pagination/02-number-nav-02.njk][tutor-pg-nav-02]

{{< highlight html >}}
{% set totalPages = pagination.links.length %}
{% set current    = pagination.pageNumber + 1 %}

{% if totalPages > 1 %}
<nav class="pagination is-small is-centered" 
     role="navigation" aria-label="pagination">

    <ul class="pagination-list">

      <!-- First Page. -->
      <li>
      {% if current > 1 %}
        <a class="pagination-previous hoverable"
           href="{{ pagination.firstPageLink }}"
           rel="first">
          &laquo;&laquo;&nbsp;</a>
      {% else %}
        <a class="pagination-previous"
           title="This is the first page"
           disabled>
          &laquo;&laquo;&nbsp;</a>
      {% endif %}
      </li>

      <!-- Previous Page. -->
      <li>
      {% if pagination.previousPageLink %}
        <a class="pagination-previous hoverable"
           href="{{ pagination.previousPageLink }}"
           rel="prev">
          &laquo;&nbsp;</a>
      {% else %}
        <a class="pagination-previous"
           title="This is the first page"
           disabled>
          &laquo;&nbsp;</a>
      {% endif %}
      </li>

    <!-- Page numbers. -->
    {% for cursor, link in pagination.links | hashIt %}
      <li>
      {% if cursor != current  %}
        <a class="pagination-link hoverable"
           href="{{ link }}"
           aria-label="Goto page {{ cursor }}">
          {{ cursor }}
        </a>
       {% else %}
       <a class="pagination-link is-current {{ color }}" 
           aria-label="Page {{ cursor }}">
          {{ cursor }}
        </a>
 
      {% endif %}
      </li>
    {% endfor %}

      <!-- Next Page. -->
      <li>
      {% if pagination.nextPageLink %}
        <a class="pagination-next hoverable"
           href="{{ pagination.nextPageLink }}"
           rel="next">
          &nbsp;&raquo;</a>
      {% else %}
        <a class="pagination-next"
           title="This is the last page"
           disabled>
          &nbsp;&raquo;</a>
      {% endif %}
      </li>

      <!-- Last Page. -->
      <li>
      {% if current != totalPages %}
        <a class="pagination-next hoverable"
           href="{{ pagination.lastPageLink }}"
           rel="last">
          &nbsp;&raquo;&raquo;</a>
      {% else %}
        <a class="pagination-next"
           title="This is the last page"
           disabled>
          &nbsp;&raquo;&raquo;</a>
      {% endif %}
      </li>

    </ul>

</nav>
{% endif %}
{{< / highlight >}}

We simply use these `&raquo;` and `&laquo;` for navigation icon.

#### Browser: Pagination Preview

Which shown, another kind of pagination.

![11ty Pagination: Inline List Style][image-pg-nav-02]

Bulma capable to show both, without any custom stylesheet.

#### Responsive Stylesheet: Bulma Mixins

By the help of mixins/breakpoints,
we can write it in bulma style.

* [gitlab.com/.../sass/css/main/_pagination.sass][tutor-sass-pagination]

{{< highlight sass >}}
+tablet
  li.icon-previous a:after
    content: " Previous"
  li.icon-next a:before
    content: "Next "
  li.icon-first a:after
    content: " First"
  li.icon-last a:before
    content: "Last "
{{< / highlight >}}

#### SASS: Bulma Breakpoint Variables.

Bulma 0.7, or Bulma 0.8 breakpoints are defined as below.

{{< highlight sass >}}
// The container horizontal gap,
// which acts as the offset for breakpoints

$gap        : 64px !default
$tablet     : 769px !default
$desktop    : 960px + (2 * $gap) !default
$widescreen : 1152px + (2 * $gap) !default
$fullhd     : 1344px + (2 * $gap) !default
{{< / highlight >}}

#### Responsive Class

Now we can add class in each respective element.

{{< highlight html >}}
{% set totalPages = pagination.links.length %}
{% set current    = pagination.pageNumber + 1 %}

{% if totalPages > 1 %}
<nav class="pagination is-small is-centered" 
     role="navigation" aria-label="pagination">

    <ul class="pagination-list">

      <!-- First Page. -->
      <li class="icon-first">
      ...
      </li>

      <!-- Previous Page. -->
      <li class="icon-previous">
      ...
      </li>

      <!-- Page numbers. -->
      {% for cursor, link in pagination.links | hashIt %}
        ..
      {% endfor %}

      <!-- Next Page. -->
      <li class="icon-next">
      ...
      </li>

      <!-- Last Page. -->
      <li class="icon-last">
      ...
      </li>

    </ul>

</nav>
{% endif %}
{{< / highlight >}}

#### Browser: Responsive Pagination Preview

In tablet screen, this will show different looks

![11ty Pagination: Responsive Inline List Style][image-pg-nav-02-res]

-- -- --

### 4: Navigation: Font Awesome

We can improve the looks of inline list style above.

#### Layout: Nunjucks Blog

{{< highlight jinja >}}
  {% include "pagination/02-number-nav-03.njk" %}
{{< / highlight >}}

#### Bulma Pagination

* [gitlab.com/.../views/_includes/pagination/02-number-nav-03.njk][tutor-pg-nav-03]

{{< highlight html >}}
{% set totalPages = pagination.links.length %}
{% set current    = pagination.pageNumber + 1 %}

{% if totalPages > 1 %}
<nav class="pagination is-small is-centered" 
     role="navigation" aria-label="pagination">

    <ul class="pagination-list">

      <!-- First Page. -->
      <li>
      {% if current > 1 %}
        <a class="pagination-previous hoverable"
           href="{{ pagination.firstPageLink }}"
           rel="first">
          <span class="fas fa-step-backward"></span>&nbsp;</a>
      {% else %}
        <a class="pagination-previous"
           title="This is the first page"
           disabled>
          <span class="fas fa-step-backward"></span>&nbsp;</a>
      {% endif %}
      </li>

      <!-- Previous Page. -->
      <li>
      {% if pagination.previousPageLink %}
        <a class="pagination-previous hoverable"
           href="{{ pagination.previousPageLink }}"
           rel="prev">
          <span class="fas fa-backward"></span>&nbsp;</a>
      {% else %}
        <a class="pagination-previous"
           title="This is the first page"
           disabled>
          <span class="fas fa-backward"></span>&nbsp;</a>
      {% endif %}
      </li>

    <!-- Page numbers. -->
    {% for cursor, link in pagination.links | hashIt %}
      <li>
      {% if cursor != current  %}
        <a class="pagination-link hoverable"
           href="{{ link }}"
           aria-label="Goto page {{ cursor }}">
          {{ cursor }}
        </a>
       {% else %}
       <a class="pagination-link is-current {{ color }}" 
           aria-label="Page {{ cursor }}">
          {{ cursor }}
        </a>
 
      {% endif %}
      </li>
    {% endfor %}

      <!-- Next Page. -->
      <li>
      {% if pagination.nextPageLink %}
        <a class="pagination-next hoverable"
           href="{{ pagination.nextPageLink }}"
           rel="next">&nbsp;
          <span class="fas fa-forward"></span></a>
      {% else %}
        <a class="pagination-next"
           title="This is the last page"
           disabled>&nbsp;
          <span class="fas fa-forward"></span></a>
      {% endif %}
      </li>

      <!-- Last Page. -->
      <li>
      {% if current != totalPages %}
        <a class="pagination-next hoverable"
           href="{{ pagination.lastPageLink }}"
           rel="last">&nbsp;
          <span class="fas fa-step-forward"></span></a>
      {% else %}
        <a class="pagination-next"
           title="This is the last page"
           disabled>&nbsp;
          <span class="fas fa-step-forward"></span></a>
      {% endif %}
      </li>

    </ul>

</nav>
{% endif %}
{{< / highlight >}}

#### Browser: Pagination Preview

Which shown, another kind of pagination.

![11ty Pagination: Inline List Style with Awesome Font][image-pg-nav-03]

#### Responsive Stylesheet: Bulma Mixins

By the help of Bulma's mixins/breakpoints,
we can put additional rules in pagination stylesheet.

* [gitlab.com/.../sass/css/main/_pagination.sass][tutor-sass-pagination]

{{< highlight sass >}}
+tablet
  li.icon-previous a:after
    content: " Previous"
  li.icon-next a:before
    content: "Next "
  li.icon-first a:after
    content: " First"
  li.icon-last a:before
    content: "Last "
  li.icon-previous a span.fas,
  li.icon-next a span.fas,
  li.icon-first a span.fas,
  li.icon-last a span.fas
    display: none
{{< / highlight >}}

#### Responsive Class

Now we can add class in each respective element.

* `icon-first`

* `icon-previous`

* `icon-next`

* `icon-last`

#### Browser: Responsive Pagination Preview

In tablet screen, this will show different looks

![11ty Pagination: Responsive Inline List Style][image-pg-nav-03-res]

-- -- --

### What is Next ?

Do not get confused yet.
Keep calm, just like this cute kitten.
Our pagination tutorial still have some materials to go.

![adorable kitten][image-kitten]

Consider continue reading [ [Eleventy - Pagination - Adjacent][local-whats-next] ].
We are going to explore an advance topic, the adjacent pagination.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/01/25/11ty-pagination-adjacent/
[image-kitten]:             {{< baseurl >}}/assets/site/images/cats/rambu-04.jpg

[tutor-html-master-07]: {{< tutor-11ty-bulma-md >}}/tutor-07/

[tutor-configuration]:  {{< tutor-11ty-bulma-md >}}/tutor-07/.eleventy.js
[tutor-vl-blog]:        {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/layouts/blog.njk
[tutor-pg-nav-01]:      {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/pagination/02-number-nav-01.njk
[tutor-pg-nav-02]:      {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/pagination/02-number-nav-02.njk
[tutor-pg-nav-03]:      {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/pagination/02-number-nav-03.njk

[tutor-sass-pagination]:{{< tutor-11ty-bulma-md >}}/tutor-07/sass/css/main/_pagination.sass

[image-pg-navigation]:  {{< assets-ssg >}}/2020/01/07-pg-02-navigation.png
[image-pg-nav-01]:      {{< assets-ssg >}}/2020/01/07-pg-02-navigation-01.png
[image-pg-nav-02]:      {{< assets-ssg >}}/2020/01/07-pg-02-navigation-02.png
[image-pg-nav-03]:      {{< assets-ssg >}}/2020/01/07-pg-02-navigation-03.png
[image-pg-nav-02-res]:  {{< assets-ssg >}}/2020/01/07-pg-02-navigation-02-responsive.png
[image-pg-nav-03-res]:  {{< assets-ssg >}}/2020/01/07-pg-02-navigation-03-responsive.png
