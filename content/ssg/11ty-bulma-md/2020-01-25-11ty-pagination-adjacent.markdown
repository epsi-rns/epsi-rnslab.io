---
type   : post
title  : "Eleventy - Pagination - Adjacent"
date   : 2020-01-25T09:17:35+07:00
slug   : 11ty-pagination-adjacent
categories: [ssg]
tags      : [11ty, bulma]
keywords  : [nunjucks, pagination, adjacent, javascript]
author : epsi
opengraph:
  image: assets-ssg/2020/01/07-pg-03-adjacent.png

toc    : "toc-2020-01-11ty-bulma-md-step"

excerpt:
  Building 11ty Site Step by step, with Bulma MD as stylesheet frontend.
  Advance pagination uysing adjacent Filter. Not so for dummies.
---

### Preface

> Goal: Apply Glenn McComb Pagination using Eleventy.

#### Source Code

This article use [tutor-07][tutor-html-master-07] theme.
We will create it step by step.

-- -- --

### 1: Source

I respect copyright.
The code below copied, and pasted from:

* [How to build custom Hugo pagination][glennmccomb]

I made a slight modification.
But of course the logic remain the same.

I mostly write it down in my blog,
because I do not want to forget what I learn.
An easier place for me to find the modified version of this good code.

-- -- --

### 2: Preview: General

It is not easy to explain by words.
Let me explain what we want to achive by using these images below.
The brown box, is the active page.
since we have 17 articles and 3 page for each pagination,
then we have 9 pages of pagination.
We have from first page (1), to last page (9).

#### Animation: Combined Version

This is the complete version.
We will achieve this later.

![11ty Pagination: Combined Animation][image-indicator-gif]

#### Animation: Stripped Version

I'm following **Glenn McComb** code,
combined with my own code,
and this is the result.

![11ty Pagination: Adjacent Animation][image-adjacent-gif]

#### Sample: An Example

Consider get one frame, a sample, because we need an example.
This is what we want to achieve in this tutorial.

![11ty Pagination: Adjacent Page 9][image-pg-stripped-09]

#### HTML Preview

The HTML that we want to achieve in this article, is similar as below.

{{< highlight html >}}
<nav class="pagination is-small is-centered" ...>
    <ul class="pagination-list">
      <li><a class="pagination-link" ...>3</a></li>
      <li><a class="pagination-link" ...>4</a></li>
      <li><a class="pagination-link is-current" >5</a></li>
      <li><a class="pagination-link" ...>6</a></li>
      <li><a class="pagination-link" ...>7</a></li>
    </ul>
</nav>
{{< / highlight >}}

Or in detail, you can breakdown each as below:

{{< highlight html >}}
<nav class="pagination is-small is-centered"
     role="navigation" aria-label="pagination">
  <ul class="pagination-list">

      <li>
        <a class="pagination-link hoverable"
           href="/pages/page-3/index.html"
           aria-label="Goto page 3">
          3
        </a>        

           
      <li>
        <a class="pagination-link hoverable"
           href="/pages/page-4/index.html"
           aria-label="Goto page 4">
          4
        </a>
      </li>

      <li>
        <a class="pagination-link is-current brown"
           aria-label="Page 5">
          5
        </a>
      </li>

      <li>
        <a class="pagination-link hoverable"
           href="/pages/page-6/index.html"
           aria-label="Goto page 6">
          6
        </a>
      </li>

      <li>
        <a class="pagination-link hoverable"
           href="/pages/page-7/index.html"
           aria-label="Goto page 7">
          7
        </a>
      </li>

  </ul>
</nav>
{{< / highlight >}}

We will achieve this with `eleventy` code.

#### The Riddle

> How do we achieve this ?

-- -- --

### 3: Custom Pagination Filter

This would be better,
if we move complex code from template,
to a javascript helper.

#### Filter: isShowAdjacent

We need to add `isShowAdjacent`.

* [gitlab.com/.../.eleventy.js][tutor-configuration]

{{< highlight javascript >}}
  // Pagination Page: Another tailor made script based on Glenn Mc Comb.
  // Checking Cursor Flag for Adjacent Pagination
  eleventyConfig.addNunjucksFilter("isShowAdjacent",
    function(cursor, current, totalPages, adjacentLinks) {
      return pagination.isShowAdjacent(cursor, current, totalPages, adjacentLinks);
    }
  );
{{< / highlight >}}

Do not forget to load the script first.

{{< highlight javascript >}}
  const pagination = require("./views/_11ty/pagination");
{{< / highlight >}}

And the script is shown below:

* [gitlab.com/.../_11ty/pagination.js][helper-pagination]

{{< highlight javascript >}} 
// Pagination links : Based on Glenn Mc Comb
// https://glennmccomb.com/articles/how-to-build-custom-hugo-pagination/
// Adjacent: Number of links either side of the current page

exports.isShowAdjacent = function(cursor, current, totalPages, adjacentLinks) {
  // initialize variables
  let showCursorFlag = false;

  const maxLinks      = (adjacentLinks * 2) + 1;
  const lowerLimit    = 1 + adjacentLinks;
  const upperLimit    = totalPages - adjacentLinks;
  
  if (totalPages > maxLinks) {
    // Complex page numbers.
    
    if (current <= lowerLimit) {
      // Lower limit pages.
      // If the user is on a page which is in the lower limit.
      if (cursor <= maxLinks)
         showCursorFlag = true;
    } else if (current >= upperLimit) {
      // Upper limit pages.
      // If the user is on a page which is in the upper limit.
      if (cursor > (totalPages - maxLinks))
         showCursorFlag = true;
    } else {
      // Middle pages.
      if ( (cursor >= current - adjacentLinks)
      &&   (cursor <= current + adjacentLinks) )
         showCursorFlag = true;
    }
  } else {
    // Simple page numbers.
    showCursorFlag = true;
  }

  return showCursorFlag;
}
{{< / highlight >}}

![11ty Pagination: Pagination Helper: isShowAdjacent][image-vim-pagination]

You can pipe this filter later as code below:

{{< highlight jinja >}}
  {% set showCursorFlag = cursor | 
           isShowAdjacent(current, totalPages, adjacentLinks) %}
{{< / highlight >}}

This `showCursorFlag` should be checked in every loop.

#### Skeleton

To get more understanding about this filter,
consider to examine this skeleton below:

{{< highlight javascript >}}
exports.isShowAdjacent = function(...) {
  // initialize variables ...
  
  if (totalPages > maxLinks) {
  // Complex page numbers.    
    if (current <= lowerLimit) {
      // Lower limit pages ...
    } else if (current >= upperLimit) {
      // Upper limit pages ...
    } else {
      // Middle pages ...
    }
  } else {
  // Simple page numbers.
    ...
  }

  return showCursorFlag;
}
{{< / highlight >}}

-- -- --

### 4: Math: Basic Algebra

#### Assumption

Consider our previous example,
a blog post contain **seventeen** posts.
This time with two **adjacent**.
It means, two indicators before selected page,
and another two indicators after selected page,

{{< highlight conf >}}
# CONST

Total Post   = 17
Per Page     = 2

adjacentLinks = 2

# COMPUTED

totalPages   = 9
{{< / highlight >}}

#### Equation

We should manually, do the math.

{{< highlight javascript >}}
EQUATION

maxLinks   = (adjacentLinks * 2) + 1 = 5
lowerLimit =  1 + adjacentLinks      = 3
upperLimit =  9 - adjacentLinks      = 7
{{< / highlight >}}

The real code is shown as below:

{{< highlight jinja >}}
  {% set adjacentLinks = 2 %}
  {% set current       = pagination.pageNumber + 1 %}
  {% set maxLinks      = (adjacentLinks * 2) + 1 %}
  {% set lowerLimit    = 1 + adjacentLinks %}
  {% set upperLimit    = totalPages - adjacentLinks %}
{{< / highlight >}}

Again, that source above are, **ported** from:

* [How to build custom Hugo pagination][glennmccomb]

#### Table

The result is on this table below.

{{< highlight conf >}}
# ALGEBRA

+------------+-------+-------+-------+-------+-------+
| pagination |   2   |   3   |   5   |  10   |  20   |
+------------+-------+-------+-------+-------+-------+
| VARIABLE                                           |
| totalPages |   9   |   6   |   4   |   2   |  N/A  |
| maxLinks   |   5   |   5   |   5   |   5   |  N/A  |
| lowerLimit |   3   |   3   |   3   |   3   |  N/A  |
| upperLimit |   7   |   4   |   2   |   0   |  N/A  |
+------------+-------+-------+-------+-------+-------+
{{< / highlight >}}

-- -- --

### 5: Adjacent Code

This utilized `showCursorFlag` with complex algorithm.

#### Layout: Nunjucks Blog

Consider use `pagination/03-adjacent` partial, in `blog.njk`.

* [gitlab.com/.../views/_includes/layouts/blog.njk][tutor-vl-blog]
  
{{< highlight jinja >}}
  {% include "pagination/03-adjacent.njk" %}
{{< / highlight >}}

#### Checking The Flag

In every loop, this flag should be check first and use later.

{{< highlight jinja >}}
    {% for cursor, link in pagination.links | hashIt %}

      {% set showCursorFlag = cursor | 
               isShowAdjacent(current, totalPages, adjacentLinks) %}

      {# Use Flag: Show Pager. #}
      ...

    {% endfor %}
{{< / highlight >}}

#### Using The Flag

And showing the number whenever the `showCursorFlag` comes out.
With similar code as previous article.

{{< highlight jinja >}}
    {# <!-- Page numbers. --> #}
    {% for cursor, link in pagination.links | hashIt %}

      {% set showCursorFlag = cursor | 
               isShowAdjacent(current, totalPages, adjacentLinks) %}

      {# Use Flag: Show Pager. #}
      {% if showCursorFlag %}
      <li>
        {% if cursor == current  %}

        <a class="pagination-link is-current {{ color }}"
           aria-label="Page {{ cursor }}">
          {{ cursor }}
        </a>

        {% else %}

        <a class="pagination-link hoverable"
           href="{{ link }}"
           aria-label="Goto page {{ cursor }}">
          {{ cursor }}
        </a>

        {% endif %}
      </li>
      {% endif %}

    {% endfor %}
{{< / highlight >}}

#### Preview

This is what we have achieved in this tutorial.

![11ty Pagination: Adjacent Pagination][image-pg-adjacent]

#### The Same Riddle

How does it works ?

> Really! It is confusing.

Luckily, there is explanation for this, for the curious one.

#### Math in Details

I provide separate article with thorough explanation of the logic,
used by this **Glenn McComb Pagination**.

-- -- --

### What is Next ?

Do not stop reading.
Consider continue to [ [Eleventy - Pagination - Logic][local-whats-next] ].
We are going to reverse engineering,
the Glenn McComb Pagination using Math and Table.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/01/26/11ty-pagination-logic/
[glennmccomb]:          https://glennmccomb.com/articles/how-to-build-custom-hugo-pagination/

[tutor-html-master-07]: {{< tutor-11ty-bulma-md >}}/tutor-07/

[tutor-configuration]:  {{< tutor-11ty-bulma-md >}}/tutor-07/.eleventy.js
[helper-pagination]:    {{< tutor-11ty-bulma-md >}}/tutor-07/views/_11ty/pagination.js
[tutor-pg-adjacent]:    {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/pagination/03-adjacent.njk
[tutor-pg-legacy]:      {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/pagination/03-adjacent-legacy.njk
[tutor-vl-blog]:        {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/layouts/blog.njk

[image-pg-adjacent]:    {{< assets-ssg >}}/2020/01/07-pg-03-adjacent.png
[image-pg-stripped-09]: {{< assets-ssg >}}/2020/01/07-pg-03-adjacent-page-09.png
[image-vim-pagination]: {{< assets-ssg >}}/2020/01/07-pg-03-vim-adjacent-js.png

[image-adjacent-gif]:   {{< assets-ssg >}}/2020/01/11ty-bulma-adjacent-animate.gif
[image-indicator-gif]:  {{< assets-ssg >}}/2020/01/11ty-bulma-indicator-animate.gif

