---
type   : post
title  : "Eleventy - Pagination - Logic"
date   : 2020-01-26T09:17:35+07:00
slug   : 11ty-pagination-logic
categories: [ssg]
tags      : [11ty, bulma]
keywords  : [nunjucks, pagination, adjacent, logic]
author : epsi
opengraph:
  image: assets-ssg/2020/01/07-pg-03-adjacent.png

toc    : "toc-2020-01-11ty-bulma-md-step"

excerpt:
  Building 11ty Site Step by step, with Bulma MD as stylesheet frontend.
  Reverse engineering of Glenn McComb Pagination using Math and Table.
---

### Preface

> Goal: Explaining Glenn McComb Pagination using Math and Table.

#### Source Code

This article use [tutor-07][tutor-html-master-07] theme.
We will create it step by step.

-- -- --

### 1: Preview: Detail

Consider, have a look at the pagination below in a stripped down model.

#### Layout: Nunjucks Blog

We are still using `pagination/03-adjacent` partial, in `blog.njk`.

* [gitlab.com/.../views/_includes/layouts/blog.njk][tutor-vl-blog]
  
{{< highlight jinja >}}
  {% include "pagination/03-adjacent.njk" %}
{{< / highlight >}}

And the `pagination/03-adjacent` partial is rely on this filter.

{{< highlight jinja >}}
  {% set showCursorFlag = cursor | 
           isShowAdjacent(current, totalPages, adjacentLinks) %}
{{< / highlight >}}

#### Filter: Pagination Helper

Which the filter itself has this skeleton below:

* [gitlab.com/.../_11ty/pagination.js][helper-pagination]

{{< highlight javascript >}}
exports.isShowAdjacent = function(...) {
  // initialize variables ...
  
  if (totalPages > maxLinks) {
  // Complex page numbers.    
    if (current <= lowerLimit) {
      // Lower limit pages ...
    } else if (current >= upperLimit) {
      // Upper limit pages ...
    } else {
      // Middle pages ...
    }
  } else {
  // Simple page numbers.
    ...
  }

  return showCursorFlag;
}
{{< / highlight >}}

#### Structure

This will only show one part:

* Middle Pagination: **Glenn McComb**

#### Each Pagination

Consider, have a look at the animation above, frame by frame.
I'm going to do some reverse engineering,
to accomplish better understanding on how this pagination works.

We have from first page (1), to last page (9).

![11ty Pagination: Adjacent Page 1][image-pg-stripped-01]

![11ty Pagination: Adjacent Page 2][image-pg-stripped-02]

![11ty Pagination: Adjacent Page 3][image-pg-stripped-03]

![11ty Pagination: Adjacent Page 4][image-pg-stripped-04]

![11ty Pagination: Adjacent Page 5][image-pg-stripped-05]

![11ty Pagination: Adjacent Page 6][image-pg-stripped-06]

![11ty Pagination: Adjacent Page 7][image-pg-stripped-07]

![11ty Pagination: Adjacent Page 8][image-pg-stripped-08]

![11ty Pagination: Adjacent Page 9][image-pg-stripped-09]

#### Table

We can rewrite the table with additional rows as below.

{{< highlight conf >}}
+------------+-------+-------+-------+-------+-------+
| pagination |   2   |   3   |   5   |  10   |  20   |
+------------+-------+-------+-------+-------+-------+
| VARIABLE                                           |
| totalPages |   9   |   6   |   4   |   2   |  N/A  |
| maxLinks   |   5   |   5   |   5   |   5   |  N/A  |
| lowerLimit |   3   |   3   |   3   |   3   |  N/A  |
| upperLimit |   7   |   4   |   2   |   0   |  N/A  |
+------------+-------+-------+-------+-------+-------+
| MIDDLE PAGINATION                                  |
| page  = 1  | 1..5  | 1..5  | 1..4  | 1..2  |-------+
| page  = 2  | 1..5  | 1..5  | 1..4  | 1..2  |       |
| page  = 3  | 1..5  | 1..5  | 1..4  |-------+       |
| page  = 4  | 2..6  | 4..6  | 1..4  |               |
| page  = 5  | 3..7  | 4..6  |-------+               |
| page  = 6  | 4..8  | 4..6  |                       |
| page  = 7  | 5..9  |-------+                       |
| page  = 8  | 5..9  |                               |
| page  = 9  | 5..9  |                               |
+------------+-------+-------------------------------+
{{< / highlight >}}

-- -- --

### 2: Math: Conditional

#### Part: Middle Pages

This is already discussed in, so I won't explain it nomore.

* <https://glennmccomb.com/articles/how-to-build-custom-hexo-pagination/>

{{< highlight javascript >}}
    if (current <= lowerLimit) {
      // Lower limit pages.
      ...
    } else if (current >= upperLimit) {
      // Upper limit pages.
      ...
    } else {
      // Middle pages.
      if ( (cursor >= current - adjacentLinks)
      &&   (cursor <= current + adjacentLinks) )
         showCursorFlag = true;
    }
{{< / highlight >}}

What you need to know is the conditional result in table:

{{< highlight conf >}}
+------------+-------+
| pagination |   2   |
| adjacent   |   2   |
| total post |  17   |
+------------+-------+
| VARIABLE           |
| totalPages |   9   |
| maxLinks   |   5   |
| lowerLimit |   3   |
| upperLimit |   7   |
+------------+-------+--+
| selected   | adjacent |
+------------+----------+
| page  =  1 |   1..3   |
| page  =  2 |   1..4   |
| page  =  3 |   1..5   |
| page  =  4 |   2..6   |
| page  =  5 |   3..7   |
| page  =  6 |   4..8   |
| page  =  7 |   5..9   |
| page  =  8 |   5..9   |
| page  =  9 |   5..9   |
+------------+----------+
{{< / highlight >}}

#### Part: Lower Limit Pages

Consider stripped more for each part.

{{< highlight javascript >}}
    if (current <= lowerLimit) {
      // Lower limit pages.
      // If the user is on a page which is in the lower limit.
      if (cursor <= maxLinks)
         showCursorFlag = true;
    } else if (current >= upperLimit) {
      // Upper limit pages.
      ...
    } else {
      // Middle pages.
      ...
    }
{{< / highlight >}}

Notice that there is two part of conditional.

* Outer conditional:
  result true for the first three row,
  as defined by `lowerLimit`.

* Inner conditional: always result `1..5`.

Thus, the conditional result in table:

{{< highlight conf >}}
+------------+-------+-------+--------+
| selected   | lower | l max | result |
+------------+-------+-------+--------+
| page  =  1 |   T   | 1..5  |  1..5  |
| page  =  2 |   T   | 1..5  |  1..5  |
| page  =  3 |   T   | 1..5  |  1..5  |
| page  =  4 |       | 1..5  |        |
| page  =  5 |       | 1..5  |        |
| page  =  6 |       | 1..5  |        |
| page  =  7 |       | 1..5  |        |
| page  =  8 |       | 1..5  |        |
| page  =  9 |       | 1..5  |        |
+------------+-------+-------+--------+
{{< / highlight >}}

#### Combined: All Conditional

Now we have all the logic combined at once.

{{< highlight conf >}}
+------------+-------+
| pagination |   1   |
| adjacent   |   2   |
| totalPost  |  10   |
+------------+-------+
| VARIABLE           |
| totalPages |  10   |
| maxLinks   |   5   |
| lowerLimit |   3   |
| upperLimit |   7   |
+------------+-------+-+-------+-------+-------+-------+
| selected   | adjacent| lower | l max | upper | u max |
+------------+---------+-------+-------+-------+-------+
| cursor = 1 |  1..3   |   T   | 1..5  |       | 5..9  |
| cursor = 2 |  1..4   |   T   | 1..5  |       | 5..9  |
| cursor = 3 |  1..5   |   T   | 1..5  |       | 5..9  |
| cursor = 4 |  2..6   |       | 1..5  |       | 5..9  |
| cursor = 5 |  3..7   |       | 1..5  |       | 5..9  |
| cursor = 6 |  4..8   |       | 1..5  |       | 5..9  |
| cursor = 7 |  5..9   |       | 1..5  |   T   | 5..9  |
| cursor = 8 |  5..9   |       | 1..5  |   T   | 5..9  |
| cursor = 9 |  5..9   |       | 1..5  |   T   | 5..9  |
+------------+---------+-------+-------+-------+-------+
{{< / highlight >}}

#### Final Result

As a conclusion table.

{{< highlight conf >}}
+------------+-------+
| VARIABLE           |
| totalPages |  10   |
| maxLinks   |   5   |
| lowerLimit |   3   |
| upperLimit |   8   |
+------------+-------+-------+---------+
| selected   | lower | upper | adjacent|
+------------+-------+-------+---------+
| cursor = 1 | 1..5  |       |         |
| cursor = 2 | 1..5  |       |         |
| cursor = 3 | 1..5  |       |         |
| cursor = 4 |       |       |  2..6   |
| cursor = 5 |       |       |  3..7   |
| cursor = 6 |       |       |  4..8   |
| cursor = 7 |       | 5..9  |         |
| cursor = 8 |       | 5..9  |         |
| cursor = 9 |       | 5..9  |         |
+------------+-------+-------+---------+
| selected   | if elsif else | result  |
+------------+---------------+---------+
| cursor = 1 |               |  1..5   |
| cursor = 2 |               |  1..5   |
| cursor = 3 |               |  1..5   |
| cursor = 4 |               |  2..6   |
| cursor = 5 |               |  3..7   |
| cursor = 6 |               |  4..8   |
| cursor = 7 |               |  5..9   |
| cursor = 8 |               |  5..9   |
| cursor = 9 |               |  5..9   |
+------------+---------------+---------+
{{< / highlight >}}

-- -- --

### 3: Legacy Code

If you find the javascript above in filter is long,
you need to consider this legacy code below.
This is what I use before I put the logic into javascript filter.

> Whoaaa... This legacy code is complex!

The script in javascript filter is direct port,
of this nunjucks template script,
that I have made about a year ago.

* [gitlab.com/.../views/_includes/pagination/03-adjacent-legacy.njk][tutor-pg-legacy]

{{< highlight jinja >}}
    {# <!-- Page numbers. --> #}
    {% for cursor, link in pagination.links | hashIt %}

      {% set showCursorFlag = false %}

      {# Set Flag: Complex code. #}
      {% if (totalPages > maxLinks) %}
        {# <!-- Complex page numbers. --> #}

        {# <!-- Lower limit pages. --> #}
        {# <!-- If the user is on a page which is in the lower limit.  --> #}
        {% if (current <= lowerLimit) %}

          {# <!-- If the current loop page is less than max_links. --> #}
          {% if (cursor <= maxLinks) %}
            {% set showCursorFlag = true %}
          {% endif %}

        {# <!-- Upper limit pages. --> #}
        {# <!-- If the user is on a page which is in the upper limit. --> #}
        {% elif (current >= upperLimit) %}

          {# <!-- If the current loop page is greater than total pages minus $max_links --> #}
          {% if (cursor > (totalPages - maxLinks)) %}
            {% set showCursorFlag = true %}
          {% endif %}

        {# <!-- Middle pages. --> #}
        {% else %}
          
          {% if ( (cursor >= current - adjacentLinks) 
              and (cursor <= current + adjacentLinks) ) %}
            {% set showCursorFlag = true %}
          {% endif %}

        {% endif %}

      {% else %}
        {# <!-- Simple page numbers. --> #}
        {% set showCursorFlag = true %}
      {% endif %}

      {# Use Flag: Show Pager. #}
      {% if showCursorFlag %}
      <li>...</li>
      {% endif %}

    {% endfor %}
{{< / highlight >}}

You can compare the legacy script with the new filter below.

![11ty Pagination: Pagination Helper: isShowAdjacent][image-vim-pagination]

We are not going to use this legacy script anymore.
But this script is still useful to debug,
or to check value that I need to compare with table above.

-- -- --

### What is Next ?

Consider continue reading [ [Eleventy - Pagination - Indicator][local-whats-next] ].
We are going to add indicator, and putting all pagination part together.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/01/27/11ty-pagination-indicator/

[tutor-html-master-07]: {{< tutor-11ty-bulma-md >}}/tutor-07/

[helper-pagination]:    {{< tutor-11ty-bulma-md >}}/tutor-07/views/_11ty/pagination.js
[tutor-vl-blog]:        {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/layouts/blog.njk
[tutor-pg-legacy]:      {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/pagination/03-adjacent-legacy.njk

[image-pg-stripped-01]: {{< assets-ssg >}}/2020/01/07-pg-03-adjacent-page-01.png
[image-pg-stripped-02]: {{< assets-ssg >}}/2020/01/07-pg-03-adjacent-page-02.png
[image-pg-stripped-03]: {{< assets-ssg >}}/2020/01/07-pg-03-adjacent-page-03.png
[image-pg-stripped-04]: {{< assets-ssg >}}/2020/01/07-pg-03-adjacent-page-04.png
[image-pg-stripped-05]: {{< assets-ssg >}}/2020/01/07-pg-03-adjacent-page-05.png
[image-pg-stripped-06]: {{< assets-ssg >}}/2020/01/07-pg-03-adjacent-page-06.png
[image-pg-stripped-07]: {{< assets-ssg >}}/2020/01/07-pg-03-adjacent-page-07.png
[image-pg-stripped-08]: {{< assets-ssg >}}/2020/01/07-pg-03-adjacent-page-08.png
[image-pg-stripped-09]: {{< assets-ssg >}}/2020/01/07-pg-03-adjacent-page-09.png
[image-vim-pagination]: {{< assets-ssg >}}/2020/01/07-pg-03-vim-adjacent-js.png
