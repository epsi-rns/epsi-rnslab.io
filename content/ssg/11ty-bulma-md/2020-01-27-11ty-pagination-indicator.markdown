---
type   : post
title  : "Eleventy - Pagination - Indicator"
date   : 2020-01-27T09:17:35+07:00
slug   : 11ty-pagination-indicator
categories: [ssg]
tags      : [11ty, bulma]
keywords  : [nunjucks, pagination, adjacent, indicator]
author : epsi
opengraph:
  image: assets-ssg/2020/01/07-pg-04-indicator.png

toc    : "toc-2020-01-11ty-bulma-md-step"

excerpt:
  Building 11ty Site Step by step, with Bulma MD as stylesheet frontend.
  Middle pagination. Not so for dummies. Combined with Indicator.
---

### Preface

> Goal: Add indicator, and putting all pagination part together.

#### Source Code

This article use [tutor-07][tutor-html-master-07] theme.
We will create it step by step.

-- -- --

### 1: Source

I respect copyright.
Most of the code below, especially middle pagination,
copied and pasted from:

* [How to build custom Hugo pagination][glennmccomb]

The rest is my modification.

-- -- --

### 2: Prepare

Just a reminder.

#### Layout: Nunjucks Blog

Consider use `pagination/02-number` partial, in `blog.njk`.

* [gitlab.com/.../views/_includes/layouts/blog.njk][tutor-vl-blog]
  
{{< highlight jinja >}}
  {% include "pagination/04-indicator.njk" %}
{{< / highlight >}}

#### Filter: Pagination Helper

The pagination helper part,
will remain the same for the rest of tutorial.

-- -- --

### 3: Preview: General

This is what we want to achieve in this tutorial.

![11ty Pagination: Pagination with Indicator][image-pg-indicator]

#### Structure

This consist of at least seven parts:

* Previous Page: **&laquo;** or AwesomeFont's Backward Icon

* First Page: always 1

* Left Indicator

* Middle Pagination: **Glenn McComb**

* Right Indicator

* Last Page: always the same number

* Next Page: **&raquo;** or AwesomeFont's Forward Icon

We will not discuss about **Middle Pagination**,
as it has already been discussed in previous article.

#### HTML Preview

The HTML that we want to achieve is similar as below.

{{< highlight html >}}
<nav class="pagination is-small is-centered" ...>

    <ul class="pagination-list">

      <!-- Previous Page. -->
      <li class="blog-previous"><a class="pagination-previous" ...>
          <span class="fas fa-backward"></span>&nbsp;</a></li>

      <!-- First Page. -->
      <li><a class="pagination-link" ...>1</a></li>

      <!-- Early (More Pages) Indicator. -->
      <li><span class="pagination-ellipsis">&hellip;</span></li>

      <li><a class="pagination-link" ...>3</a></li>
      <li><a class="pagination-link" ...>4</a></li>
      <li><a class="pagination-link is-current" >5</a></li>
      <li><a class="pagination-link" ...>6</a></li>
      <li><a class="pagination-link" ...>7</a></li>

      <!-- Late (More Pages) Indicator. -->
      <li><span class="pagination-ellipsis">&hellip;</span></li>

      <!-- Last Page. -->
      <li><a class="pagination-link"  ...>9</a></li>

      <!-- Next Page. -->
      <li class="blog-next"><a class="pagination-next" ...>
          &nbsp;<span class="fas fa-forward"></span></a></li>

    </ul>

</nav>
{{< / highlight >}}

#### Small Preview

This is the complete version.

![11ty Pagination: Combined Animation][image-indicator-gif]

#### Wide Preview

Wide version, in responsive context, is slightly different.

![11ty Pagination: Preview Wide][image-indicator-wide]

We will use responsive CSS later to achieve this effect.

#### Partial: Indicator Skeleton

As usual, the skeleton, to show the complexity.

* [gitlab.com/.../views/_includes/pagination/04-indicator.njk][tutor-pg-indicator]

{{< highlight jinja >}}
{% if totalPages > 1 %}
<nav class="pagination is-small is-centered" ...>
  <ul class="pagination-list">

    <!-- Previous Page. -->
    <!-- First Page. -->
    <!-- Early (More Pages) Indicator. -->

    <!-- Page numbers. -->
    {% for cursor, link in pagination.links | hashIt %}
      ...
    {% endfor %}

    <!-- Late (More Pages) Indicator. -->
    <!-- Last Page. -->
    <!-- Next Page. -->

  </ul>
</nav>
{% endif %}
{{< / highlight >}}

#### Each Pagination

Consider again, have a look at the animation above, frame by frame.

We have from first page (1), to last page (9).

![11ty Pagination: Indicator Page 1][image-pg-combined-01]

![11ty Pagination: Indicator Page 2][image-pg-combined-02]

![11ty Pagination: Indicator Page 3][image-pg-combined-03]

![11ty Pagination: Indicator Page 4][image-pg-combined-04]

![11ty Pagination: Indicator Page 5][image-pg-combined-05]

![11ty Pagination: Indicator Page 6][image-pg-combined-06]

![11ty Pagination: Indicator Page 7][image-pg-combined-07]

![11ty Pagination: Indicator Page 8][image-pg-combined-08]

![11ty Pagination: Indicator Page 9][image-pg-combined-09]

-- -- --

### 4: Navigation: Previous and Next

It is similar to our number pagination.

#### Navigation: Previous

{{< highlight jinja >}}
      <!-- Previous Page. -->
      <li class="icon-previous">
      {% if pagination.previousPageLink %}
        <a class="pagination-previous hoverable"
           href="{{ pagination.previousPageLink }}"
           rel="prev">
          <span class="fas fa-backward"></span>&nbsp;</a>
      {% else %}
        <a class="pagination-previous"
           title="This is the first page"
           disabled>
          <span class="fas fa-backward"></span>&nbsp;</a>
      {% endif %}
      </li>
{{< / highlight >}}

#### Navigation: Next

{{< highlight jinja >}}
      <!-- Next Page. -->
      <li class="icon-next">
      {% if pagination.nextPageLink %}
        <a class="pagination-next hoverable"
           href="{{ pagination.nextPageLink }}"
           rel="next">&nbsp;
          <span class="fas fa-forward"></span></a>
      {% else %}
        <a class="pagination-next"
           title="This is the last page"
           disabled>&nbsp;
          <span class="fas fa-forward"></span></a>
      {% endif %}
      </li>
{{< / highlight >}}

-- -- --

### 5: Navigation: First and Last

It is different to our simple pagination.
Although it is based on the same logic.

#### Navigation: First

This will not be shown,
if it is already be shown middle pagination.

{{< highlight jinja >}}
    {% if (totalPages > maxLinks) %}
      <!-- First Page. -->
      {% if (current - adjacentLinks > 1) %}
      <li>
        <a class="pagination-link hoverable"
           href="{{ pagination.firstPageLink }}"
           aria-label="Goto page 1">1</a>
      </li>
      {% endif %}
    {% endif %}
{{< / highlight >}}

#### Navigation: Last

This will not be shown,
if it is already be shown middle pagination.

{{< highlight jinja >}}
    {% if (totalPages > maxLinks) %}
      <!-- Last Page. -->
      {% if (current + adjacentLinks < totalPages) %}
      <li>
        <a class="pagination-link hoverable" 
           href="{{ pagination.lastPageLink }}"
           aria-label="Goto page {{ totalPages }}">
          {{ totalPages }}</a>
      </li>
      {% endif %}
    {% endif %}
{{< / highlight >}}

-- -- --

### 6: Indicator: Left and Right

These will only be shown, only if necessary.

#### Indicator: Left

{{< highlight jinja >}}
    {% if (totalPages > maxLinks) %}
      <!-- Early (More Pages) Indicator. -->
      {% if (current - adjacentLinks > 2) %}
      <li>
        <span class="pagination-ellipsis">&hellip;</span>
      </li>
      {% endif %}
    {% endif %}
{{< / highlight >}}

#### Indicator: Right

{{< highlight jinja >}}
    {% if (totalPages > maxLinks) %}
      <!-- Late (More Pages) Indicator. -->
      {% if (current + adjacentLinks < totalPages - 1)  %}
      <li>
        <span class="pagination-ellipsis">&hellip;</span>
      </li>
      {% endif %}
    {% endif %}
{{< / highlight >}}

-- -- --

### 7: Combined Code

It is about the right time to put all the code together.

* [gitlab.com/.../views/_includes/pagination/04-indicator.njk][tutor-pg-indicator]

{{< highlight jinja >}}
{% set totalPages = pagination.links.length %}
{% set current    = pagination.pageNumber + 1 %}
{% set adjacentLinks = 2 %}
{% set maxLinks      = (adjacentLinks * 2) + 1 %}

{% if totalPages > 1 %}
<nav class="pagination is-small is-centered"
     role="navigation" aria-label="pagination">

  <ul class="pagination-list">

      <!-- Previous Page. -->
      <li class="icon-previous">
      {% if pagination.previousPageLink %}
        <a class="pagination-previous hoverable"
           href="{{ pagination.previousPageLink }}"
           rel="prev">
          <span class="fas fa-backward"></span>&nbsp;</a>
      {% else %}
        <a class="pagination-previous"
           title="This is the first page"
           disabled>
          <span class="fas fa-backward"></span>&nbsp;</a>
      {% endif %}
      </li>

    {% if (totalPages > maxLinks) %}
      <!-- First Page. -->
      {% if (current - adjacentLinks > 1) %}
      <li>
        <a class="pagination-link hoverable"
           href="{{ pagination.firstPageLink }}"
           aria-label="Goto page 1">1</a>
      </li>
      {% endif %}

      <!-- Early (More Pages) Indicator. -->
      {% if (current - adjacentLinks > 2) %}
      <li>
        <span class="pagination-ellipsis">&hellip;</span>
      </li>
      {% endif %}
    {% endif %}

    <!-- Page numbers. -->
    {% for cursor, link in pagination.links | hashIt %}

      <!-- Adjacent script based on Glenn Mc Comb. -->
      {% set showCursorFlag = cursor | 
               isShowAdjacent(current, totalPages, adjacentLinks) %}

      <!-- Show Pager. -->
      {% if showCursorFlag %}
      <li>
        {% if cursor == current  %}

        <a class="pagination-link is-current {{ color }}"
           aria-label="Page {{ cursor }}">
          {{ cursor }}
        </a>

        {% else %}

        <a class="pagination-link hoverable"
           href="{{ link }}"
           aria-label="Goto page {{ cursor }}">
          {{ cursor }}
        </a>

        {% endif %}
      </li>
      {% endif %}

    {% endfor %}

    {% if (totalPages > maxLinks) %}
      <!-- Late (More Pages) Indicator. -->
      {% if (current + adjacentLinks < totalPages - 1)  %}
      <li>
        <span class="pagination-ellipsis">&hellip;</span>
      </li>
      {% endif %}

      <!-- Last Page. -->
      {% if (current + adjacentLinks < totalPages) %}
      <li>
        <a class="pagination-link hoverable" 
           href="{{ pagination.lastPageLink }}"
           aria-label="Goto page {{ totalPages }}">
          {{ totalPages }}</a>
      </li>
      {% endif %}
    {% endif %}

      <!-- Next Page. -->
      <li class="icon-next">
      {% if pagination.nextPageLink %}
        <a class="pagination-next hoverable"
           href="{{ pagination.nextPageLink }}"
           rel="next">&nbsp;
          <span class="fas fa-forward"></span></a>
      {% else %}
        <a class="pagination-next"
           title="This is the last page"
           disabled>&nbsp;
          <span class="fas fa-forward"></span></a>
      {% endif %}
      </li>

  </ul>
</nav>
{% endif %}
{{< / highlight >}}

-- -- --

### 8: Alternate Looks

Alternatively you can put all button outside the list style.

![11ty Pagination: Complete 1][image-pg-alternate-01]

![11ty Pagination: Complete 2][image-pg-alternate-02]

![11ty Pagination: Complete 3][image-pg-alternate-03]

![11ty Pagination: Complete 4][image-pg-alternate-04]

![11ty Pagination: Complete 5][image-pg-alternate-05]

![11ty Pagination: Complete 6][image-pg-alternate-06]

![11ty Pagination: Complete 7][image-pg-alternate-07]

![11ty Pagination: Complete 8][image-pg-alternate-08]

![11ty Pagination: Complete 9][image-pg-alternate-09]

-- -- --

### What is Next ?

Consider continue reading [ [Eleventy - Pagination - Responsive][local-whats-next] ].
We are going to explore responsive pagination, using mobile first.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/01/28/11ty-pagination-responsive/
[glennmccomb]:          https://glennmccomb.com/articles/how-to-build-custom-hugo-pagination/

[tutor-html-master-07]: {{< tutor-11ty-bulma-md >}}/tutor-07/

[tutor-vl-blog]:        {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/layouts/blog.njk
[tutor-pg-indicator]:   {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/pagination/04-indicator.njk

[image-pg-indicator]:   {{< assets-ssg >}}/2020/01/07-pg-04-indicator.png
[image-pg-combined-01]: {{< assets-ssg >}}/2020/01/07-pg-04-indicator-page-01.png
[image-pg-combined-02]: {{< assets-ssg >}}/2020/01/07-pg-04-indicator-page-02.png
[image-pg-combined-03]: {{< assets-ssg >}}/2020/01/07-pg-04-indicator-page-03.png
[image-pg-combined-04]: {{< assets-ssg >}}/2020/01/07-pg-04-indicator-page-04.png
[image-pg-combined-05]: {{< assets-ssg >}}/2020/01/07-pg-04-indicator-page-05.png
[image-pg-combined-06]: {{< assets-ssg >}}/2020/01/07-pg-04-indicator-page-06.png
[image-pg-combined-07]: {{< assets-ssg >}}/2020/01/07-pg-04-indicator-page-07.png
[image-pg-combined-08]: {{< assets-ssg >}}/2020/01/07-pg-04-indicator-page-08.png
[image-pg-combined-09]: {{< assets-ssg >}}/2020/01/07-pg-04-indicator-page-09.png

[image-indicator-gif]:  {{< assets-ssg >}}/2020/01/11ty-bulma-indicator-animate.gif
[image-indicator-wide]: {{< assets-ssg >}}/2020/01/07-pg-04-indicator-page-wide-05.png

[image-pg-alternate-01]:    {{< assets-ssg >}}/2020/01/07-pg-06-alternate-page-01.png
[image-pg-alternate-02]:    {{< assets-ssg >}}/2020/01/07-pg-06-alternate-page-02.png
[image-pg-alternate-03]:    {{< assets-ssg >}}/2020/01/07-pg-06-alternate-page-03.png
[image-pg-alternate-04]:    {{< assets-ssg >}}/2020/01/07-pg-06-alternate-page-04.png
[image-pg-alternate-05]:    {{< assets-ssg >}}/2020/01/07-pg-06-alternate-page-05.png
[image-pg-alternate-06]:    {{< assets-ssg >}}/2020/01/07-pg-06-alternate-page-06.png
[image-pg-alternate-07]:    {{< assets-ssg >}}/2020/01/07-pg-06-alternate-page-07.png
[image-pg-alternate-08]:    {{< assets-ssg >}}/2020/01/07-pg-06-alternate-page-08.png
[image-pg-alternate-09]:    {{< assets-ssg >}}/2020/01/07-pg-06-alternate-page-09.png
