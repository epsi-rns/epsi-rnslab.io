---
type   : post
title  : "Eleventy - Pagination - Responsive"
date   : 2020-01-28T09:17:35+07:00
slug   : 11ty-pagination-responsive
categories: [ssg]
tags      : [11ty, bulma]
keywords  : [nunjucks, pagination, sass, responsive, mobile first]
author : epsi
opengraph:
  image: assets-ssg/2020/01/07-pg-05-responsive.png

toc    : "toc-2020-01-11ty-bulma-md-step"

excerpt:
  Building 11ty Site Step by step, with Bulma MD as stylesheet frontend.
  Responsive pagination using Bulma. Not so for dummies.
---

### Preface

> Goal: Bringing responsive pagination, using mobile first.

#### Source Code

This article use [tutor-07][tutor-html-master-07] theme.
We will create it step by step.

-- -- --

### 1: Source

#### Original Source

I respect copyright.
The code below inspired by:

* <https://www.timble.net/blog/2015/05/better-pagination-for-jekyll/>

* <https://github.com/timble/jekyll-pagination>

I made a slight modification.
But of course the logic remain the same.

> Of course, this article is talking about SASS

#### Altered Source

> Coder will feel like home

While writing this CSS code,
I can't claim myself as a developer.
Because CSS is not a programming language.

But weird that, responsive design has their own logic.
So yeah, I have to code, a little.

-- -- --

### 2: Prepare

This step is required.

#### Preview: General

This is what we want to achieve in this tutorial.

![11ty Pagination: Responsive Pagination][image-pg-responsive]

#### Layout: Nunjucks Blog

Consider use `pagination/05-responsive` partial, in `blog.njk`.

* [gitlab.com/.../views/_includes/layouts/blog.njk][tutor-vl-blog]
  
{{< highlight jinja >}}
  {% include "pagination/05-responsive.njk" %}
{{< / highlight >}}

#### SASS: Main

* [gitlab.com/.../sass/css/main.sass][tutor-sass-main]

{{< highlight sass >}}
...

// Tailor Made
@import "main/layout-page"
@import "main/layout-content"
@import "main/decoration"
@import "main/pagination"
@import "main/list"

@import "post/content"
@import "post/navigation"
{{< / highlight >}}

-- -- --

### 3: Navigation: HTML Class

#### The Final Result.

Consider have a look at the image below.

![11ty Pagination: Responsive 2][image-pg-responsive-02]

The HTML that we want to achieve is similar as below.

{{< highlight html >}}
<nav class="pagination is-small is-centered" ...>

    <ul class="pagination-list">
      <!-- Previous Page. -->
      <li class="blog-previous"><a class="pagination-previous" ...
        ><span class="fas fa-step-backward"></span>&nbsp;</a></li>

      <!-- First Page. -->
      <li class="first"><a class="pagination-link" ...>1</a></li>

      <!-- Early (More Pages) Indicator. -->
      <li class="pages-indicator first"><span class="pagination-ellipsis">&hellip;</span></li>

      <!-- Page numbers. -->
      <li class="pagination--offset-2"><a class="pagination-link" ...>3</a></li>
      <li class="pagination--offset-1"><a class="pagination-link" ...>4</a></li>
      <li class="pagination--offset-0"><a class="pagination-link is-current" ...>5</a></li>
      <li class="pagination--offset-1"><a class="pagination-link" ...>6</a></li>
      <li class="pagination--offset-2"><a class="pagination-link" ...>7</a></li>

      <!-- Late (More Pages) Indicator. -->
      <li class="pages-indicator last"><span class="pagination-ellipsis">&hellip;</span></li>

      <!-- Last Page. -->
      <li class="last"><a class="pagination-link" ...>9</a></li>

      <!-- Next Page. -->
      <li class="blog-next"><a class="pagination-next" ...
        >&nbsp;<span class="fas fa-step-forward"></span></a></li>
    </ul>

</nav>
{{< / highlight >}}

![11ty Pagination: Responsive Animation][image-responsive-gif]

#### Middle Pagination

All you need to care is, only these lines.

{{< highlight html >}}
  <!-- Page numbers. -->
  <li class="pagination--offset-2">...</li>
  <li class="pagination--offset-1">...</li>
  <li class="pagination--offset-0">...</li>
  <li class="pagination--offset-1">...</li>
  <li class="pagination--offset-2">...</li>
{{< / highlight >}}

Our short term goal is,
to put the `pagination--offset`class.

#### Partial: Responsive Skeleton

As usual, the skeleton, to show the complexity.

* [gitlab.com/.../views/_includes/pagination/05-responsive.njk][tutor-pg-responsive]

{{< highlight jinja >}}
{% if totalPages > 1 %}
<nav class="pagination is-small is-centered" ...>
  <ul class="pagination-list">

      <!-- First Page (arrow). -->
      <!-- Previous Page (arrow). -->

    {% if (totalPages > maxLinks) %}
      <!-- First Page (number = 1). -->
      <!-- Early (More Pages) Indicator. -->
    {% endif %}

    <!-- Page numbers. -->
    {% for cursor, link in pagination.links | hashIt %}
      ...
    {% endfor %}

    {% if (totalPages > maxLinks) %}
      <!-- Late (More Pages) Indicator. -->
      <!-- Last Page (number = total page). -->
    {% endif %}

      <!-- Next Page (arrow). -->
      <!-- Last Page (arrow). -->
  </ul>
</nav>
{% endif %}
{{< / highlight >}}

#### Calculate Offset Value

Notice this part:

{{< highlight jinja >}}
        <!-- Calculate Offset Class. -->
        {% set offset = (cursor - current) | abs %}
        {% set pageOffsetClass = 'pagination--offset-' + offset %}
{{< / highlight >}}

Notice the new variable called `pageOffsetClass`.

#### Using Offset Class

All we need is just adding the offset class.

{{< highlight javascript >}}
      <li class="{{ pageOffsetClass }}">
        ...
      </li>
{{< / highlight >}}

#### Combined Code

{{< highlight jinja >}}
      <!-- Show Pager. -->
      {% if showCursorFlag %}
        <!-- Calculate Offset Class. -->
        {% set offset = (cursor - current) | abs %}
        {% set pageOffsetClass = 'pagination--offset-' + offset %}

      <li class="{{ pageOffsetClass }}">
        {% if cursor == current  %}

        <a class="pagination-link is-current {{ color }}"
           aria-label="Page {{ cursor }}">
          {{ cursor }}
        </a>

        {% else %}

        <a class="pagination-link hoverable"
           href="{{ link }}"
           aria-label="Goto page {{ cursor }}">
          {{ cursor }}
        </a>

        {% endif %}
      </li>
      {% endif %}
{{< / highlight >}}

That is all.
Now that the HTML part is ready, we should go on,
by setting up the responsive breakpoints using SCSS.

-- -- --

### 4: Responsive: Breakpoints

Responsive is easy if you understand the logic.

> It is all about breakpoints.

#### Preview: Each Breakpoint

Consider again, have a look at the animation above, frame by frame.
We have at least five breakpoint as six figures below:

![11ty Pagination: Responsive 1][image-pg-responsive-01]

![11ty Pagination: Responsive 2][image-pg-responsive-02]

![11ty Pagination: Responsive 3][image-pg-responsive-03]

![11ty Pagination: Responsive 4][image-pg-responsive-04]

![11ty Pagination: Responsive 5][image-pg-responsive-05]

#### SASS: Bulma Custom Breakpoint Variables.

I'm using custom breakpoint, instead of Bulma 7.x breakpoints.

* [gitlab.com/.../sass/css/main/_pagination.sass][tutor-sass-pagination]

{{< highlight sass >}}
// Breakpoint

$xs1: 0
$xs2: 320px
$xs3: 380px
$xs4: 480px
$sm1: 576px
$sm2: 600px
$md:  768px
$lg:  992px
$xl:  1200px
{{< / highlight >}}

The name inspired by Bootsrap,
but it has nothing do with Bootstrap.

#### SASS: Bulma Breakpoint Skeleton

With breakpoint above, we can setup css skeleton, with empty css rules.

{{< highlight sass >}}
ul.pagination-list
  +from($xs1)
  +from($xs2)
  +from($xs3)
  +from($xs4)
  +from($sm1)
  +from($sm2)
  +from($md)
  +from($lg)
  +from($xl)
{{< / highlight >}}

#### SASS: Using Bulma Breakpoint: Simple

We can fill any rules, as below:

{{< highlight sass >}}
+tablet
  li.icon-previous a:after
    content: " Previous"
  li.icon-next a:before
    content: "Next "
  li.icon-first a:after
    content: " First"
  li.icon-last a:before
    content: "Last "
  li.icon-previous a span.fas,
  li.icon-next a span.fas,
  li.icon-first a span.fas,
  li.icon-last a span.fas
    display: none
{{< / highlight >}}

#### SASS: Using Custom Breakpoint: Pagination Offset

We can fill any rules, inside `pagination-list` class as below:
Such as `pagination--offset` setting,
for each custom breakpoints.

{{< highlight sass >}}
// Responsiveness

ul.pagination-list
  li.pagination--offset-1,
  li.pagination--offset-2,
  li.pagination--offset-3,
  li.pagination--offset-4,
  li.pagination--offset-5,
  li.pagination--offset-6,
  li.pagination--offset-7
    display: none
  +from($xs1)
  +from($xs2)
  +from($xs3)
    li.pagination--offset-1
      display: inline-block
  +from($xs4)
    li.pagination--offset-2
      display: inline-block
  +from($sm1)
    li.icon-first,
    li.icon-last,
    li.pagination--offset-3
      display: inline-block
  +from($sm2)
    li.pagination--offset-4
      display: inline-block
  +from($md)
    li.pagination--offset-5,
    li.pagination--offset-6
      display: inline-block
  +from($lg)
    li.pagination--offset-7
      display: inline-block
  +from($xl)
{{< / highlight >}}

This setup breakpoint is actually up to you.
You may change to suit whatever you need.

#### SASS: Responsive Indicator

You can also add CSS rules for indicator, and stuff.

{{< highlight sass >}}
// Responsiveness

ul.pagination-list
  li.icon-first,
  li.icon-last,
  li.pages-indicator
    display: none
  +from($xs1)
  +from($xs2)
    li.pages-indicator
      display: inline-block
  +from($sm1)
    li.icon-first,
    li.icon-last,
{{< / highlight >}}

Short and simple.

#### SASS: Decoration

If you desire,
a slight enhancement without breaking the original looks.

Hover is a good idea.

{{< highlight sass >}}
// hover color

ul.pagination-list li 
  a:hover
    background-color: map-get($yellow, 'lighten-2')
    color: #000
  a.is-current:hover
    background-color: map-get($grey, 'base') !important
    color: #fff
{{< / highlight >}}

And so is gap margin.

{{< highlight sass >}}
// layout

nav.pagination
  margin-top: 10px
  margin-bottom: 10px
{{< / highlight >}}

#### SASS: Complete Code

Now you can have the complete code as below:

* [gitlab.com/.../sass/css/main/_pagination.sass][tutor-sass-pagination]

{{< highlight sass >}}
+tablet
  li.icon-previous a:after
    content: " Previous"
  li.icon-next a:before
    content: "Next "
  li.icon-first a:after
    content: " First"
  li.icon-last a:before
    content: "Last "
  li.icon-previous a span.fas,
  li.icon-next a span.fas,
  li.icon-first a span.fas,
  li.icon-last a span.fas
    display: none

// Breakpoint

$xs1: 0
$xs2: 320px
$xs3: 380px
$xs4: 480px
$sm1: 576px
$sm2: 600px
$md:  768px
$lg:  992px
$xl:  1200px

// Responsiveness

ul.pagination-list
  li.icon-first,
  li.icon-last,
  li.pages-indicator
    display: none
  li.pagination--offset-1,
  li.pagination--offset-2,
  li.pagination--offset-3,
  li.pagination--offset-4,
  li.pagination--offset-5,
  li.pagination--offset-6,
  li.pagination--offset-7
    display: none
  +from($xs1)
  +from($xs2)
    li.pages-indicator
      display: inline-block
  +from($xs3)
    li.pagination--offset-1
      display: inline-block
  +from($xs4)
    li.pagination--offset-2
      display: inline-block
  +from($sm1)
    li.icon-first,
    li.icon-last,
    li.pagination--offset-3
      display: inline-block
  +from($sm2)
    li.pagination--offset-4
      display: inline-block
  +from($md)
    li.pagination--offset-5,
    li.pagination--offset-6
      display: inline-block
  +from($lg)
    li.pagination--offset-7
      display: inline-block
  +from($xl)

// hover color

ul.pagination-list li 
  a:hover
    background-color: map-get($yellow, 'lighten-2')
    color: #000
  a.is-current:hover
    background-color: map-get($grey, 'base') !important
    color: #fff

// layout

nav.pagination
  margin-top: 10px
  margin-bottom: 10px
{{< / highlight >}}

-- -- --

### 5: Summary

You can have a look at our complete code here:

* [gitlab.com/.../views/_includes/pagination/05-responsive.njk][tutor-pg-responsive]
 
{{< highlight javascript >}}
{% set totalPages = pagination.links.length %}
{% set current    = pagination.pageNumber + 1 %}

{# 
* Pagination links 
* https://glennmccomb.com/articles/how-to-build-custom-hugo-pagination/
* Adjacent: Number of links either side of the current page
#}

{% if totalPages > 1 %}
<nav class="pagination is-small is-centered"
     role="navigation" aria-label="pagination">

  {% set adjacentLinks = 2 %}
  {% set maxLinks      = (adjacentLinks * 2) + 1 %}

  <ul class="pagination-list">

      <!-- First Page. -->
      <li class="icon-first">
      {% if current > 1 %}
        <a class="pagination-previous hoverable"
           href="{{ pagination.firstPageLink }}"
           rel="first">
          <span class="fas fa-step-backward"></span>&nbsp;</a>
      {% else %}
        <a class="pagination-previous"
           title="This is the first page"
           disabled>
          <span class="fas fa-step-backward"></span>&nbsp;</a>
      {% endif %}
      </li>

      <!-- Previous Page. -->
      <li class="icon-previous">
      {% if pagination.previousPageLink %}
        <a class="pagination-previous hoverable"
           href="{{ pagination.previousPageLink }}"
           rel="prev">
          <span class="fas fa-backward"></span>&nbsp;</a>
      {% else %}
        <a class="pagination-previous"
           title="This is the first page"
           disabled>
          <span class="fas fa-backward"></span>&nbsp;</a>
      {% endif %}
      </li>

    {% if (totalPages > maxLinks) %}
      <!-- First Page. -->
      {% if (current - adjacentLinks > 1) %}
      <li>
        <a class="pagination-link hoverable"
           href="{{ pagination.firstPageLink }}"
           aria-label="Goto page 1">1</a>
      </li>
      {% endif %}

      <!-- Early (More Pages) Indicator. -->
      {% if (current - adjacentLinks > 2) %}
      <li class="pages-indicator">
        <span class="pagination-ellipsis">&hellip;</span>
      </li>
      {% endif %}
    {% endif %}

    <!-- Page numbers. -->
    {% for cursor, link in pagination.links | hashIt %}

      <!-- Adjacent script based on Glenn Mc Comb. -->
      {% set showCursorFlag = cursor | 
               isShowAdjacent(current, totalPages, adjacentLinks) %}

      <!-- Show Pager. -->
      {% if showCursorFlag %}
        <!-- Calculate Offset Class. -->
        {% set offset = (cursor - current) | abs %}
        {% set pageOffsetClass = 'pagination--offset-' + offset %}

      <li class="{{ pageOffsetClass }}">
        {% if cursor == current  %}

        <a class="pagination-link is-current {{ color }}"
           aria-label="Page {{ cursor }}">
          {{ cursor }}
        </a>

        {% else %}

        <a class="pagination-link hoverable"
           href="{{ link }}"
           aria-label="Goto page {{ cursor }}">
          {{ cursor }}
        </a>

        {% endif %}
      </li>
      {% endif %}

    {% endfor %}

    {% if (totalPages > maxLinks) %}
      <!-- Late (More Pages) Indicator. -->
      {% if (current + adjacentLinks < totalPages - 1)  %}
      <li class="pages-indicator">
        <span class="pagination-ellipsis">&hellip;</span>
      </li>
      {% endif %}

      <!-- Last Page. -->
      {% if (current + adjacentLinks < totalPages) %}
      <li>
        <a class="pagination-link hoverable" 
           href="{{ pagination.lastPageLink }}"
           aria-label="Goto page {{ totalPages }}">
          {{ totalPages }}</a>
      </li>
      {% endif %}
    {% endif %}

      <!-- Next Page. -->
      <li class="icon-next">
      {% if pagination.nextPageLink %}
        <a class="pagination-next hoverable"
           href="{{ pagination.nextPageLink }}"
           rel="next">&nbsp;
          <span class="fas fa-forward"></span></a>
      {% else %}
        <a class="pagination-next"
           title="This is the last page"
           disabled>&nbsp;
          <span class="fas fa-forward"></span></a>
      {% endif %}
      </li>

      <!-- Last Page. -->
      <li class="icon-last">
      {% if current != totalPages %}
        <a class="pagination-next hoverable"
           href="{{ pagination.lastPageLink }}"
           rel="last">&nbsp;
          <span class="fas fa-step-forward"></span></a>
      {% else %}
        <a class="pagination-next"
           title="This is the last page"
           disabled>&nbsp;
          <span class="fas fa-step-forward"></span></a>
      {% endif %}
      </li>

  </ul>
</nav>
{% endif %}
{{< / highlight >}}

We will have to complete the code later, in the next article.
This has been a long article.
We still need a few changes, for screen reader.

#### Browser: Pagination Preview

Finally the animated version.

![11ty Pagination: Responsive Animation][image-responsive-gif]

-- -- --

### What is Next ?

Consider continue reading [ [Eleventy - Pagination - Screenreader][local-whats-next] ].
We are going to conclude pagination with screenreader.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/01/29/11ty-pagination-screenreader/
[glennmccomb]:          https://glennmccomb.com/articles/how-to-build-custom-hugo-pagination/

[tutor-html-master-07]: {{< tutor-11ty-bulma-md >}}/tutor-07/

[tutor-vl-blog]:        {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/layouts/blog.njk
[tutor-pg-responsive]:  {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/pagination/05-responsive.njk

[tutor-sass-pagination]:{{< tutor-11ty-bulma-md >}}/tutor-07/sass/css/main/_pagination.sass
[tutor-sass-main]:      {{< tutor-11ty-bulma-md >}}/tutor-07/sass/css/main.sass

[image-pg-responsive]:  {{< assets-ssg >}}/2020/01/07-pg-05-responsive.png
[image-pg-responsive-01]:   {{< assets-ssg >}}/2020/01/07-pg-05-responsive-01.png
[image-pg-responsive-02]:   {{< assets-ssg >}}/2020/01/07-pg-05-responsive-02.png
[image-pg-responsive-03]:   {{< assets-ssg >}}/2020/01/07-pg-05-responsive-03.png
[image-pg-responsive-04]:   {{< assets-ssg >}}/2020/01/07-pg-05-responsive-04.png
[image-pg-responsive-05]:   {{< assets-ssg >}}/2020/01/07-pg-05-responsive-05.png

[image-responsive-gif]: {{< assets-ssg >}}/2020/01/11ty-bulma-responsive-animate.gif
