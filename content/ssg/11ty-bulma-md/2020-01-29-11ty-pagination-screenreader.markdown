---
type   : post
title  : "Eleventy - Pagination - Screen Reader"
date   : 2020-01-29T09:17:35+07:00
slug   : 11ty-pagination-screenreader
categories: [ssg]
tags      : [11ty, bulma]
keywords  : [nunjucks, pagination, screenreader]
author : epsi
opengraph:
  image: assets-ssg/2020/01/07-pg-06-screenreader.png

toc    : "toc-2020-01-11ty-bulma-md-step"

excerpt:
  Building 11ty Site Step by step, with Bulma MD as stylesheet frontend.
  Accessability for pagination using Bulma.
  Complete the custom pagination.
  This the summary code for our pagination.
---

### Preface

> Goal: Bringing screen reader accessability in pagination.

#### Source Code

This article use [tutor-07][tutor-html-master-07] theme.
We will create it step by step.

-- -- --

### 1: Source

#### Screen Reader Class

I just follow bulma guidance:

* <https://bulma.io/documentation/modifiers/helpers/>

To hidden content visually,
you simply need to add `is-sr-only`.

{{< highlight html >}}
<span class="is-sr-only">Hidden Content</span>
{{< / highlight >}}

Alternatively you can use fontawesome class that is similar to bootstrap.

{{< highlight html >}}
<span class="sr-only">Hidden Content</span>
{{< / highlight >}}

This content can be read by screenreader.
You can test using `inspect element`.

#### Aria Label

Instead of class, we can also utilize `aria-label`

{{< highlight html >}}
        <a class="pagination-link is-current {{ color }}"
           aria-label="Page {{ cursor }}"
           aria-current="true">
          <span class="is-sr-only">Page </span>
          {{ cursor }}
        </a>
{{< / highlight >}}

-- -- --

### 2: Prepare

#### Preview: General

There shoud be nomore preview, because this is screen reader.
Luckily, this article meant to provide example of screenreader.
So I show all buttons in this template example.

![11ty Pagination: Pagination with Accessability][image-pg-screenreader]

You may freely change the appearance.
Just remove any button that you do not need.
Change the class, or even the SASS.
Just remember that in this article,
we should focus on accessability, instead the looks.

#### Testing

You can test using screen reader `inspect element`.

#### Layout: Nunjucks Blog

Consider use `pagination/06-screeenreader` partial, in `blog.njk`.

* [gitlab.com/.../views/_includes/layouts/blog.njk][tutor-vl-blog]
  
{{< highlight jinja >}}
  {% include "pagination/06-screeenreader.njk" %}
{{< / highlight >}}

-- -- --

### 3: Navigation: Previous and Next

Just a slight modification.

#### Navigation: Previous

{{< highlight html >}}
      <!-- Previous Page. -->
      {% if pagination.previousPageLink %}
        <a class="pagination-previous hoverable"
           href="{{ pagination.previousPageLink }}"
           aria-label="Go to previous page"
           rel="prev">
          <span class="fas fa-backward"></span>&nbsp;</a>
          <span class="sr-only">Previous</span>
      {% else %}
        <a class="pagination-previous"
           title="This is the first page"
           aria-label="First page"
           aria-current="true"
           disabled>
          <span class="fas fa-backward"></span>&nbsp;</a>
      {% endif %}
{{< / highlight >}}

#### Navigation: Next

{{< highlight html >}}
      <!-- Next Page. -->
      {% if pagination.nextPageLink %}
        <a class="pagination-next hoverable"
           href="{{ pagination.nextPageLink }}"
           aria-label="Go to next page"
           rel="next">&nbsp;
          <span class="fas fa-forward"></span></a>
          <span class="sr-only">Next</span>
      {% else %}
        <a class="pagination-next"
           title="This is the last page"
           aria-label="Last page"
           aria-current="true"
           disabled>&nbsp;
          <span class="fas fa-forward"></span></a>
      {% endif %}
{{< / highlight >}}

-- -- --

### 4: Navigation: First and Last

Just a slight modification.

#### Navigation: First

{{< highlight html >}}
      <!-- First Page. -->
      {% if current > 1 %}
        <a class="pagination-previous hoverable"
           href="{{ pagination.firstPageLink }}"
           aria-label="Go to first page"
           data-rel="first">
          <span class="fas fa-step-backward"></span>&nbsp;</a>
          <span class="sr-only">First</span>
      {% else %}
        <a class="pagination-previous"
           title="This is the first page"
           aria-label="First page"
           aria-current="true"
           disabled>
          <span class="fas fa-step-backward"></span>&nbsp;</a>
      {% endif %}
{{< / highlight >}}

#### Navigation: Last

{{< highlight html >}}
      <!-- Last Page. -->
      {% if current != totalPages %}
        <a class="pagination-next hoverable"
           href="{{ pagination.lastPageLink }}"
           aria-label="Go to last page"
           data-rel="last">&nbsp;
          <span class="fas fa-step-forward"></span></a>
          <span class="sr-only">Last</span>
      {% else %}
        <a class="pagination-next"
           title="This is the last page"
           aria-label="Last page"
           aria-current="true"
           disabled>&nbsp;
          <span class="fas fa-step-forward"></span></a>
      {% endif %}
{{< / highlight >}}

-- -- --

### 5: Middle Navigation: Number

{{< highlight html >}}
      <li class="{{ pageOffsetClass }}">
        {% if cursor == current  %}

        <a class="pagination-link is-current {{ color }}"
           aria-label="Page {{ cursor }}"
           aria-current="true">
          <span class="is-sr-only">Page </span>
          {{ cursor }}
        </a>

        {% else %}

        <a class="pagination-link hoverable"
           href="{{ link }}"
           aria-label="Goto page {{ cursor }}">
          <span class="sr-only">Goto page </span>{{ cursor }}
        </a>

        {% endif %}
      </li>
{{< / highlight >}}

-- -- --

### 6: Conclusion

Now the pagination tutorial is done.

![11ty Pagination: Alternate Animation][image-alternate-gif]

I think this is all for now.

-- -- --

### What is Next ?

Consider continue reading [ [Eleventy - Meta - SEO][local-whats-next] ].
We are going to explore content, starting from metadata in html head tag.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/01/31/11ty-meta-seo/
[glennmccomb]:          https://glennmccomb.com/articles/how-to-build-custom-hugo-pagination/

[tutor-html-master-07]: {{< tutor-11ty-bulma-md >}}/tutor-07/

[tutor-vl-blog]:        {{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/layouts/blog.njk
[tutor-pg-screenreader]:{{< tutor-11ty-bulma-md >}}/tutor-07/views/_includes/pagination/06-screenreader.njk

[image-pg-screenreader]:{{< assets-ssg >}}/2020/01/07-pg-06-screenreader.png
[image-alternate-gif]:  {{< assets-ssg >}}/2020/01/11ty-bulma-alternate-animate.gif
