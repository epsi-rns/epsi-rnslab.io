---
type   : post
title  : "Eleventy - Content - Blog Post"
date   : 2020-02-01T09:17:35+07:00
slug   : 11ty-content-post
categories: [ssg]
tags      : [11ty, bulma]
keywords  : [post header, post footer, post navigation, date time, timeago]
author : epsi
opengraph:
  image: assets-ssg/2020/02/08-blog-post-all.png

toc    : "toc-2020-01-11ty-bulma-md-step"

excerpt:
  Building 11ty Site Step by step, with Bulma MD as stylesheet frontend.
  More about blog post content, Header, Footer, and Navigation.
---

### Preface

> Goal: More about blog post content, Header, Footer, and Navigation.

#### Source Code

This article use [tutor-08][tutor-html-master-08] theme.
We will create it step by step.

#### Layout Preview for Tutor 08

![Nunjucks: Layout Preview for Tutor 08][template-inheritance-08]

[template-inheritance-08]:  {{< assets-ssg >}}/2020/02/08-template-inheritance.png

-- -- --

### 1: Prepare

This preparation is required.

#### Preview: General

Consider redesign the looks of blog post.
This is what we want to achieve in this tutorial.

![11ty Content: Preview All Artefacts][image-blog-post-all]

#### Layout: Nunjucks Post

We need to redesign `layout/post.njk` partial.

* [gitlab.com/.../views/_includes/layouts/post.njk][tutor-vl-post]

{{< highlight jinja >}}
{% block main %}
  {% set color = color or 'blue' %}

  <main role="main" 
        class="column is-two-thirds">

    <section class="main-wrapper {{ color }}">
      <div class="blog white z-depth-3 hoverable">

        <section class="blog-header {{ color }} lighten-5">
          {% include "post/blog-header.njk" %}
        </section>

        <article class="blog-body" itemprop="articleBody">
          {% if toc %}
            {% include toc %}
          {% endif %}

          <div class="blog-content">
            {{ content | safe }}
          </div>

          {% include "post/navigation.njk" %}
        </article>

        <section class="blog-footer {{ color }} lighten-5">
          {% include "post/blog-footer.njk" %}
        </section>

      </div>
    </section>

  </main>
{% endblock %}
{{< / highlight >}}

![11ty Content: ViM Post Layout][image-vim-layout-post]

Talking about schema, you can read this nice site:

* [schema.org/](http://schema.org/)

#### Partials: New Artefacts

The code above require three new partials.

Create three empty partial artefacts,

* `views/_includes/post/blog-header.njk`,

* `views/_includes/post/blog-footer.njk`,

* `views/_includes/post/navigation.njk`.

And one more artefact that is required in `post/blog-header.njk`.

* `views/_includes/post/time-elapsed.njk`

#### SASS: Main

And add relevant stylesheet

* [gitlab.com/.../sass/css/main.sass][tutor-sass-main]

{{< highlight sass >}}
...

// Tailor Made
@import "main/layout-page"
@import "main/layout-content"
@import "main/decoration"
@import "main/pagination"
@import "main/list"

@import "post/content"
@import "post/title"
@import "post/list"
@import "post/navigation"
@import "post/reset-highlight"
{{< / highlight >}}

Even with Bulma, we still need adjustment.
For example, you need to reset the font size, in Bulma Heading.

-- -- --

### 2: Header

Consider begin with header.

#### Preview

![11ty Content: Blog Post Header][image-blog-post-header]

#### Partial: Header: Minimum

The minimum header is simply showing title.

{{< highlight jinja >}}
  <div class="main_title"> 
    <h2 class="title is-4" itemprop="name headline">
      <a href="{{ page.url | url }}">
        {{ renderData.title or title or metadata.title }}
      </a></h2>
  </div>
{{< / highlight >}}

#### Partial: Header: Meta

Consider add **meta** data in post header.

* Author

{{< highlight jinja >}}
    {% if (author or metadata.author) %}
    <div class="is-pulled-left">
    <span class="meta_author tag is-small is-dark
                 indigo z-depth-1 hoverable">
        <span class="fa fa-user"></span>
        &nbsp;
        <span itemprop="author"
              itemscope itemtype="http://schema.org/Person">
        <span itemprop="name">{{ author or metadata.author }}</span></span>            
    </span>
    &nbsp;
    </div>
    {% endif %} 
{{< / highlight >}}

* Time Elapsed (include partial)

{{< highlight jinja >}}
    <div class="is-pulled-left">
      <span class="meta-time tag is-small is-dark
                   green z-depth-1 hoverable">
      {% include "post/time-elapsed.njk" %}
      </span>
      &nbsp;
    </div>
{{< / highlight >}}

* Tags

{{< highlight jinja >}}
    <div class="is-pulled-right">
    {% for tag in tags %}
      {% set tagUrl %}/tags/{{ tag | slug }}/{% endset %}
        <a href="{{ tagUrl | url }}">
          <span class="tag is-dark is-small teal z-depth-1 hoverable">
            <span class="fa fa-tag"></span>&nbsp;{{ tag }}</span></a>
      {% endfor %}
      &nbsp;
    </div>
{{< / highlight >}}

I use bulma `tag`,
with the eye candy Awesome Font.

#### Partial: Header: Complete Code

Here below is my actual code.

* [gitlab.com/.../views/_includes/post/blog-header.njk][tutor-vi-po-header]

{{< highlight html >}}
  <div class="main_title"> 
    <h2 class="title is-4" itemprop="name headline">
      <a href="{{ page.url | url }}">
        {{ renderData.title or title or metadata.title }}
      </a></h2>
  </div>

  <div class="field p-t-5">
    {% if (author or metadata.author) %}
    <div class="is-pulled-left">
    <span class="meta_author tag is-small is-dark
                 indigo z-depth-1 hoverable">
        <span class="fa fa-user"></span>
        &nbsp;
        <span itemprop="author"
              itemscope itemtype="http://schema.org/Person">
        <span itemprop="name">{{ author or metadata.author }}</span></span>            
    </span>
    &nbsp;
    </div>
    {% endif %} 

    <div class="is-pulled-left">
      <span class="meta-time tag is-small is-dark
                   green z-depth-1 hoverable">
      {% include "post/time-elapsed.njk" %}
      </span>
      &nbsp;
    </div>

    <div class="is-pulled-right">
    {% for tag in tags %}
      {% set tagUrl %}/tags/{{ tag | slug }}/{% endset %}
        <a href="{{ tagUrl | url }}">
          <span class="tag is-dark is-small teal z-depth-1 hoverable">
            <span class="fa fa-tag"></span>&nbsp;{{ tag }}</span></a>
      {% endfor %}
      &nbsp;
    </div>

  </div>

  <div class="is-clearfix p-b-5"></div>
{{< / highlight >}}

-- -- --

### 3: Elapsed Time

As it has been mentioned above, we need special partial for this.

#### Issue

As a static generator,
`eleventy` build the content whenever there are any changes.
If there are no changes, the generated time remain static.
It means we cannot tell relative time in string such as **three days ago**,
because after a week without changes, the time remain **three days ago**,
not changing into **ten days ago**.

The solution is using javascript.
You can download the script from here

* [timeago.org/](https://timeago.org/)

That way the time ago is updated dynamically as time passes,
as opposed to having to rebuild `eleventy` every day.

Do not forget to put the script in static directory.

* [gitlab.com/.../assets/js/timeago.min.js][tutor-assets-timeago]

#### Partial: Time Elapsed

* [gitlab.com/.../views/_includes/post/time-elapsed.njk][tutor-vi-po-elapsed]

{{< highlight javascript >}}
    <time datetime="{{ page.date | date() }}" 
          itemprop="datePublished">
    <span class="timeago"
          datetime="{{ page.date | date('Y-MM-DD hh:mm:ss') }}">
    </span></time>
    &nbsp; 

    <script src="{{ "/assets/js/timeago.min.js" | url }}"></script>
    <script type="text/javascript">
      var timeagoInstance = timeago();
      var nodes = document.querySelectorAll('.timeago');
      timeagoInstance.render(nodes, 'en_US');
      timeago.cancel();
      timeago.cancel(nodes[0]);
    </script>
{{< / highlight >}}

-- -- --

### 4: Footer

We already have `header`.
Why do not we continue with `footer`?

#### Preview

![11ty Content: Blog Post Footer][image-blog-post-footer]

#### Partial: Footer

* [gitlab.com/.../views/_includes/post/blog-footer.njk][tutor-vi-po-footer]

{{< highlight html >}}
  <footer class="columns m-5">

    <div class="column is-narrow has-text-centered">
      <img src="{{ "/assets/images/license/cc-by-sa.png" | url }}" 
           class="bio-photo" 
           height="31"
           width="88"
           alt="CC BY-SA 4.0"></a>
    </div>

    <div class="column has-text-left">
      This article is licensed under:&nbsp;
      <a href="https://creativecommons.org/licenses/by-sa/4.0/deed" 
         class="text-dark">
        <b>Attribution-ShareAlike</b>
        4.0 International (CC BY-SA 4.0)</a>
    </div>

  </footer>
{{< / highlight >}}

#### Assets: License

I have collect some image related with **license**,
so that you can use license easily.

* [gitlab.com/.../assets/images/license][tutor-assets-license]

-- -- --

### 5: Post Navigation

Each post also need simple navigation.

#### Preview

![11ty Content: Blog Post Navigation][image-blog-post-navi]

#### Issue

By default, `eleventy` does not have built in function,
to handle post navigation.
After duckduckwent fo a while, I found this issue below:

* <https://github.com/11ty/eleventy/issues/426>

Luckily, in that thread,
there is already a solution from `Boris Schapira`.
He propose to use `collection`, and it works well for me.

#### Colection: postsPrevNext

We need to add `postPrevNext` filter in `.eleventy.js`:

* [gitlab.com/.../.eleventy.js][tutor-configuration]

{{< highlight javascript >}}
  // Modified from Boris Schapira
  eleventyConfig.addCollection("postsPrevNext", function(collection) {
    var posts = collection.getAllSorted().filter(function(item) {
      // Filter by layout name
      return "post" === item.data.layout;
    });

    return helper.addPrevNext(posts);
  });
{{< / highlight >}}

Where the code in `helper.js` file is as below:

* [gitlab.com/.../_11ty/helper.js][helper-js]

{{< highlight javascript >}} 
// https://github.com/11ty/eleventy/issues/426
// Copy paste from Boris Schapira

exports.addPrevNext = function (collectionArray) {
  const l = collectionArray.length;
  for (let p = 0; p < l; p++) {
    if (p > 0)
      collectionArray[p].data.previous = {
        title: collectionArray[p - 1].data.title,
        url: collectionArray[p - 1].url
      };
    if (p < l - 1)
      collectionArray[p].data.next = {
        title: collectionArray[p + 1].data.title,
        url: collectionArray[p + 1].url
      };
  }
  return collectionArray;
}
{{< / highlight >}}

Later you can use this filter as code below:

{{< highlight jinja >}}
{% set posts = collections.postsPrevNext %}
{% for post in posts %}
  ...
{% endfor %}
{{< / highlight >}}

Then you can access `post.data.previous` and `post.data.next`.

#### How does it works?

> Magic

Imagine a blog with number of posts is `17`.
It has array of `[0..l6]` of post,

* For array `[1..16]`: add previous post data.

* For array `[0..15]`: add next post data.

I can understand the logic,
but I still amaze,
how he find the solution,
and also pour in code well.

#### Partial: Navigation

* [gitlab.com/.../views/_includes/post/navigation.njk][tutor-vi-po-navi]

{{< highlight javascript >}}
{% set posts = collections.postsPrevNext %}

{% for post in posts %}
  {% if (post.url == page.url) %}

<nav class="pagination is-centered" 
     role="navigation" aria-label="pagination">

    <!-- Previous Page. -->
    {% if post.data.previous %}
      <a class="button is-small pagination-previous post-previous"
         href="{{ post.data.previous.url | url }}"
         title="{{ post.data.previous.title }}">
        <span class="fas fa-chevron-left"></span>&nbsp;&nbsp;</a>
    {% endif %}

    {% if post.data.next %}
      <a class="button is-small pagination-next post-next"
         href="{{ post.data.next.url | url }}" 
         title="{{ post.data.next.title }}">&nbsp;&nbsp;
        <span class="fas fa-chevron-right"></span></a>
    {% endif %}
</nav>

  {% endif %}
{% endfor %}
{{< / highlight >}}

#### SASS: Post Navigation

Code above require two more classes

1. `post-previous`

2. `post-next`

* [gitlab.com/.../sass/css/post/_navigation.sass][tutor-sass-post-navi]

{{< highlight scss >}}
// -- -- -- -- --
// _post-navigation.sass

a.post-previous:after
  content: " Previous"

a.post-next:before
  content: "Next "

a.post-previous:hover,
a.post-next:hover
  background-color: map-get($yellow, 'lighten-2')
  color: #000
{{< / highlight >}}

__.__

-- -- --

### 6: Before Content: Table of Content

Sometimes we need to show recurring content, 
such as table of content in article series.
For article series, we only need one TOC.
And we do not want to repeat ourself,
writing it over and over again.

To solve this case, we need help form frontmatter.

#### Layout: Nunjucks Post

We are going to insert TOC, before the content,
by including `toc` something partial, provided from frontmatter.

* [gitlab.com/.../views/_includes/layouts/post.njk][tutor-vl-post]

{{< highlight jinja >}}
{% block main %}
  ...

        <article class="blog-body" itemprop="articleBody">
          {% if toc %}
            {% include toc %}
          {% endif %}

          <div class="blog-content">
            {{ content | safe }}
          </div>

          {% include "post/navigation.njk" %}
        </article>
  ...
{% endblock %}
{{< / highlight >}}

### Page Content: Example Frontmatter

Now here it is, the `TOC` in frontmatter,
as shown in this example below.

* [gitlab.com/.../views/lyrics/marylin-manson-redeemer.md][tutor-vc-pa-redeemer]

{{< highlight yaml >}}
---
layout    : post
title     : Marilyn Manson - Redeemer
date      : 2015-07-25 07:35:05

tags      : ["industrial metal", "90s"]
keywords  : ["OST", "Queen of the Damned"]

author    : marylin
toc       : "toc/2015-07-marylin.njk"
---

...
{{< / highlight >}}

#### Layout: Nunjucks TOC

Now you can have your TOC here.

* [gitlab.com/.../views/_includes/toc/2015-07-marylin.njk][tutor-vi-toc-marylin]

{{< highlight html >}}
  <div class="white hoverable p-t-5 p-b-5">
    <div class="widget-header blue lighten-4">

      <strong>Table of Content</strong>
      <span class="fa fa-archive is-pulled-right"></span>

    </div>
    <div class="widget-body blue lighten-5">

      <ul class="widget-list">
        <li><a href="{{ "/lyrics/eurythmics-sweet-dreams/" | url }}"
              >Marilyn Manson - Sweet Dreams</a></li>
        <li><a href="{{ "/lyrics/marylin-manson-redeemer/" | url }}"
              >Marilyn Manson - Redeemer</a></li>
      </ul>

    </div>
  </div>
{{< / highlight >}}

Notice the `.njk` file extension.
We are still using nunjucks.

#### Render: Browser

Now you can see the result in the browser.

![11ty Raw HTML: Table of Content][image-content-toc]

-- -- --

### 7: Conclusion

It is enough for now.
There are many part that can be enhanced, in about content area.

After all, it is about imagination.

-- -- --

### What is Next ?

Consider continue reading [ [Eleventy - Content - Markdown][local-whats-next] ].
We are going to explore markdown in blog post content.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/02/02/11ty-content-markdown/

[tutor-html-master-08]: {{< tutor-11ty-bulma-md >}}/tutor-08/

[tutor-configuration]:  {{< tutor-11ty-bulma-md >}}/tutor-08/.eleventy.js
[tutor-assets-timeago]: {{< tutor-11ty-bulma-md >}}/tutor-08/assets/js/timeago.min.js
[tutor-assets-license]: {{< tutor-11ty-bulma-md >}}/tutor-08/assets/images/license/
[tutor-sass-main]:      {{< tutor-11ty-bulma-md >}}/tutor-08/sass/css/main.sass
[tutor-sass-post-navi]: {{< tutor-11ty-bulma-md >}}/tutor-08/sass/css/post/_navigation.sass
[helper-js]:            {{< tutor-11ty-bulma-md >}}/tutor-06/views/_11ty/helper.js
[tutor-vl-post]:        {{< tutor-11ty-bulma-md >}}/tutor-08/views/_includes/layouts/post.njk
[tutor-vi-po-header]:   {{< tutor-11ty-bulma-md >}}/tutor-08/views/_includes/post/blog-header.njk
[tutor-vi-po-footer]:   {{< tutor-11ty-bulma-md >}}/tutor-08/views/_includes/post/blog-footer.njk
[tutor-vi-po-navi]:     {{< tutor-11ty-bulma-md >}}/tutor-08/views/_includes/post/navigation.njk
[tutor-vi-po-elapsed]:  {{< tutor-11ty-bulma-md >}}/tutor-08/views/_includes/post/time-elapsed.njk
[tutor-vi-toc-marylin]: {{< tutor-11ty-bulma-md >}}/tutor-08/views/_includes/toc/2015-07-marylin.njk
[tutor-vc-pa-redeemer]: {{< tutor-11ty-bulma-md >}}/tutor-08/views/lyrics/marylin-manson-redeemer.md

[image-blog-post-all]:      {{< assets-ssg >}}/2020/02/08-blog-post-all.png
[image-blog-post-header]:   {{< assets-ssg >}}/2020/02/08-blog-post-header.png
[image-blog-post-footer]:   {{< assets-ssg >}}/2020/02/08-blog-post-footer.png
[image-blog-post-navi]:     {{< assets-ssg >}}/2020/02/08-blog-post-navigation.png
[image-vim-layout-post]:    {{< assets-ssg >}}/2020/02/08-vim-layout-post.png
[image-content-toc]:        {{< assets-ssg >}}/2020/02/08-content-toc.png
