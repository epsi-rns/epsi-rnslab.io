---
type   : post
title  : "Eleventy - Content - Markdown"
date   : 2020-02-02T09:17:35+07:00
slug   : 11ty-content-markdown
categories: [ssg]
tags      : [11ty, bulma]
keywords  : [markdown, line break, title, link, image]
author : epsi
opengraph:
  image: assets-ssg/2020/02/08-markdown-vim-5fdp.png

toc    : "toc-2020-01-11ty-bulma-md-step"

excerpt:
  Building 11ty Site Step by step, with Bulma MD as stylesheet frontend.
  Real life markdown content for daily basis.
---

### Preface

> Goal: Common use of markdown in Eleventy content

#### Source Code

This article use [tutor-08][tutor-html-master-08] theme.
We will create it step by step.

-- -- --

### 0: Markdown Content

> Focus on document structure

![Illustration: Markdown Content][illustration-markdown]

-- -- --

### 1: Separate of Concern

There is not much to say about markdown.
Markdown is easy, however there might be a tricky issue.

#### Why Markdown?

> More on Writing Content

While markup focus on document formatting,
markdown focus on document structure.

#### Reading

* [daringfireball.net markdown syntax][link-daringfireball]

#### Common Structure

The common structure for content in Eleventy are:

1. Frontmatter: YAML, or JSON, or Javascript, or even TOML.

2. Content: Markdown or HTML

Th combination is all up to you, your choice, to manage your content.

#### Real Life Structure

I think that, it is easier just to live with this structure,
split the content, and the resource (link and image).

1. Frontmatter: YAML or JSON

2. Content: Markdown

3. Resource: Markdown

{{< highlight markdown >}}
---
layout    : page
---

# Header 1

My content

[My Link][my-image]

-- -- --

## Header 1.1

My sub content

![My Image][mylink]

-- -- --

## Header 1.2

My other sub content

[//]: <> ( -- -- -- links below -- -- -- )

[my-image]:    http://some.site/an-image.jpg
[my-link]:     http://some.site/an-article.html
{{< / highlight >}}

#### Split Resource

Notice, that the markdown divided into two part by using.

{{< highlight markdown >}}
[//]: <> ()
{{< / highlight >}}

It is a good practice to split resource and content.
But do not take that to far.
Simple things do not need to be separated.

-- -- --

### 2: Line Break

`Eleventy` markdown use standard markdown
that require explicit backslash to make a new line break.
So you do not need to worry about different markdown standard.

#### Page Content: Post

As usual, we require example content.

* [gitlab.com/.../views/lyrics/5fdp-wash-it-all-the-way.md][tutor-vc-pa-wash-it]

{{< highlight markdown >}}
---
layout    : post
title     : 5FDP - Wash It All The Way
date      : 2020-02-23 07:35:05
tags      : ["metal", "2010s"]
---

I'm wasting here\
Can anyone\
Wash it all away?
<!--more-->

I won't change for you\
And I can't take the pain\
There's nothing you can do\
And there's nothing you can say
{{< / highlight >}}

![11ty Markdown: Newline in ViM][image-vim-5fdp]

#### Render: With Issue

This will result as:

![11ty Markdown: Newline in Browser][image-newline]

#### Sass

Now you still need to different between paragraph and newline:

* [gitlab.com/.../sass/css/post/_content.sass][tutor-sass-content]

{{< highlight scss >}}
.blog-body p
  margin-top: 0.5rem
  margin-bottom: 0.5rem
{{< / highlight >}}

#### Layout: Nunjucks Post

We need to wrap the `content` in `layout/post.njk` partial,
with this `blog-body` class.

* [gitlab.com/.../views/_includes/layouts/post.njk][tutor-vl-post]

{{< highlight jinja >}}
{% block main %}
  ...

          <div class="blog-content">
            {{ content | safe }}
          </div>

  ...
{% endblock %}
{{< / highlight >}}

-- -- --

### 3: Heading Title

> This is a Bulma specific issue.

#### Markdown Render

Normally markdown render this:

{{< highlight markdown >}}
# My Heading Title
{{< / highlight >}}

Into this:

{{< highlight html >}}
<h1>My Heading Title</h1>
{{< / highlight >}}

#### Bulma Class Issue

Bulma require heading title to be written as this below:

{{< highlight html >}}
<h1 class="title is-1">My Heading Title</h1>
{{< / highlight >}}

Otherwise, heading title rendered without proper size.
This means your heading would not be rendered correctly.

#### Page Content: Post

Again, content for example purpose:

* [gitlab.com/.../views/lyrics/ratm-bulls-on-parade.md][tutor-vc-pa-bulls-on]

{{< highlight markdown >}}
---
layout    : post
title     : RATM - Bulls on Parade
date      : 2018-02-15 07:35:05
tags      : ["nu metal", "90s"]
opengraphimage: "/posts/2018/kiddo-008-s.jpg"
---

Weapons, not food, not homes, not shoes\
Not need, just feed the war cannibal animal
<!--more-->

# Heading 1

## Heading 2

### Example Content

* While arms warehouses fill as quick as the cells

* Rally round tha family, pocket full of shells
{{< / highlight >}}

![11ty Markdown: Title Issue][image-title-issue]

The figure above show that all heading rendered with the same size.

#### Custom SASS: Heading

We can fix the title heading by this SASS.

* [gitlab.com/.../sass/css/post/_title.sass][tutor-sass-title]

{{< highlight scss >}}
h1
  font-size: $size-2

h2
  font-size: $size-3

h3
  font-size: $size-4

h4
  font-size: $size-5

h5
  font-size: $size-6

h6
  font-size: $size-7

h1, h2, h3, h4, h5, h6
  font-weight: $weight-semibold
{{< / highlight >}}

And the result is prettier now.

![11ty Markdown: Title Fixed][image-title-fixed]

-- -- --

### 4: Resources: Link and Image

We can utilize pure markdown to provide link,
or to provide image in `eleventy`.

#### Page Content: Resources in Markdown

Again, content for example purpose:

* [gitlab.com/.../views/lyrics/ratm-bulls-on-parade.md][tutor-vc-pa-bulls-on]

{{< highlight markdown >}}
---
layout    : post
title     : RATM - Bulls on Parade
date      : 2018-02-15 07:35:05
tags      : ["nu metal", "90s"]
---

![Business Card][one-image]

### What's next ?

Our next song would be [Soulfly - Jump Da Fuck Up][local-whats-next]

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]: /lyrics/soulfly-jump-da-fuck-up/

[one-image]:    /assets/images/adverts/one-page.png
{{< / highlight >}}

![11ty Markdown: Image and Link Resources in Markdown][image-resources]

#### Prepare Image Directory

It is, a good practice to have a good structured directory.
Just put all your resources in `assets` directory.
And how you structure you image, javascript, vector and such,
is actually up to you.

{{< highlight bash >}}
❯ tree assets/images assets/posts
assets/images
├── adverts
│   ├── one-page.png
│   ├── oto-spies-01.png
│   └── oto-spies-02.png
├── authors
│   ├── broken-person.png
│   └── epsi-vexel.png
├── cards
│   ├── business-card.png
│   └── one-page.png
├── license
│   ├── cc-0.png
│   ├── cc-by-nc-nd.png
│   ├── cc-by-nc.png
│   ├── cc-by-nc-sa.png
│   ├── cc-by-nd.png
│   ├── cc-by-sa.png
│   └── cc-pdm.png
├── light-grey-terrazzo.png
├── logo-gear-opengraph.png
└── logo-gear.png
assets/posts
└── 2018
    └── kiddo-008-s.jpg

5 directories, 18 files
{{< / highlight >}}

![11ty Source Image: Tree][image-assets-tree]

-- -- --

### 5: Caveat: Include

> Can I use HTML in Markdown?

Although markdown can be preprocessed by nunjucks,
and can also use `include` directive.
It cannot embed any document with `html` tag.

#### Partial: Social Account

Consider this `html` partial.

* [gitlab.com/.../_includes/toc/social-accounts.html][tutor-vi-soc-accounts]

{{< highlight html >}}
  <div class="white hoverable p-t-5 p-b-5">
    <div class="widget-header red lighten-4">

      <strong>Social Accounts</strong>
      <span class="fa fa-user is-pulled-right"></span>

    </div>
    <div class="widget-body red lighten-5">

      <ul class="widget-list">
      <li><span class="fab fa-telegram"></span>
        <a href="https://t.me/cahgentoo">@cahgentoo</a>
      </li>
      <li><span class="fab fa-google-plus"></span>
        <a href="https://plus.google.com/+epsinurwijayadi">+epsinurwijayadi</a>
      </li>
      <li><span class="fab fa-deviantart"></span>
        <a href="http://nurwijayadi.deviantart.com/">nurwijayadi</a>
      </li>
      <li><span class="fab fa-github"></span>
        <a href="https://github.com/epsi-rns">epsi-rns</a>
      </li>
      <li><span class="fab fa-gitlab"></span>
        <a href="https://gitlab.com/epsi-rns">epsi-rns</a>
      </li>
      <li><span class="fab fa-bitbucket"></span>
        <a href="https://bitbucket.org/epsi">epsi</a>
      </li>
      </ul>

    </div>
  </div>
{{< / highlight >}}

Notice the `.html` extension file type.

#### Page Content: Include in HTML

The solution, is to use `html` document instead of markdown.

* [gitlab.com/.../views/pages/about.html][tutor-vc-pa-about]

{{< highlight html >}}
---
layout    : page
title     : About Rescue Mission
---

<p>This was not a rescue mission!....</p>

{% include "toc/social-accounts.html" %}

<p>Let me put to you like this.<br/>
If the secretary wanted me out of there,<br/>
then things are really bad out here.</p>
{{< / highlight >}}

Notice this also use `.html` extension file type.

#### Render in Browser

This will result as:

![11ty HTML: Include HTML in Browser][image-html]

-- -- --

### 6: MathJax

MathJax is a huge topic that deserve their own article series.
But using MathJax is simple and straight away.

This Mathjax can be in both `markdown` and `html`.
So it is not specifically designed for markdown only content.

I have plan to use LaTeX and MathJax extensively.
so I put this MathJax setting in this chapter.
Just in case I forget,
I can find my own example easily.

#### Partial: Nunjucks Scripts

MathJax can be use directly by using setting up this partial below:

* [gitlab.com/.../views/_includes/site/scripts.njk][tutor-vi-s-scripts]

{{< highlight jinja >}}
  {# Additional javaScript here #}

  {% if mathjax %}
    {# include "site/mathjax.njk" #}
    <script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
    <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
  {% endif %}
{{< / highlight >}}

I also put MathJax 2.7 script setting in my repository.

#### Page Content: Post

Again, content for example purpose:

* [gitlab.com/.../views/lyrics/house-of-pain-jump-around.md][tutor-vc-pa-around]

{{< highlight latex >}}
---
layout    : post
title     : House of Pain - Jump Around
...
mathjax   : true
---

Pack it up, pack it in, let me begin\
I came to win, battle me that's a sin
<!--more-->

$$ \lim\limits_{x \to \infty} \exp(-x) = 0 $$

Get up, stand up (c'mon!) see'mon throw your hands up\
If you've got the feeling, jump across the ceiling

$$ \int_0^\infty \mathrm{e}^{-x}\,\mathrm{d} $$

Feelin', funkin', amps in the trunk and I got more rhymes
{{< / highlight >}}

![11ty Markdown: MathJax][image-mathjax]

-- -- --

### What is Next ?

Consider continue reading [ [Eleventy - Content - Shortcode][local-whats-next] ].
We are going to utilize shortcode in markdown content.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/02/03/11ty-content-shortcodes/
[link-daringfireball]:  https://daringfireball.net/projects/markdown/syntax

[tutor-html-master-08]: {{< tutor-11ty-bulma-md >}}/tutor-08/
[illustration-markdown]:{{< assets-ssg >}}/2020/markdown-focus.png

[tutor-configuration]:  {{< tutor-11ty-bulma-md >}}/tutor-08/.eleventy.js
[tutor-vl-post]:        {{< tutor-11ty-bulma-md >}}/tutor-08/views/_includes/layouts/post.njk
[tutor-vi-s-scripts]:   {{< tutor-11ty-bulma-md >}}/tutor-04/views/_includes/site/scripts.njk
[tutor-vc-pa-wash-it]:  {{< tutor-11ty-bulma-md >}}/tutor-08/views/lyrics/5fdp-wash-it-all-the-way.md
[tutor-vc-pa-bulls-on]: {{< tutor-11ty-bulma-md >}}/tutor-08/views/lyrics/ratm-bulls-on-parade.md
[tutor-vc-pa-around]:   {{< tutor-11ty-bulma-md >}}/tutor-08/views/lyrics/house-of-pain-jump-around.md
[tutor-vi-soc-accounts]:{{< tutor-11ty-bulma-md >}}/tutor-08/views/_includes/toc/social-accounts.html
[tutor-vc-pa-about]:    {{< tutor-11ty-bulma-md >}}/tutor-08/views/pages/about.html
[tutor-sass-content]:   {{< tutor-11ty-bulma-md >}}/tutor-08/sass/css/post/_content.sass
[tutor-sass-title]:     {{< tutor-11ty-bulma-md >}}/tutor-08/sass/css/post/_title.sass

[image-newline]:        {{< assets-ssg >}}/2020/02/08-markdown-newline.png
[image-vim-5fdp]:       {{< assets-ssg >}}/2020/02/08-markdown-vim-5fdp.png
[image-title-fixed]:    {{< assets-ssg >}}/2020/02/08-markdown-title-fixed.png
[image-title-issue]:    {{< assets-ssg >}}/2020/02/08-markdown-title-issue.png
[image-resources]:      {{< assets-ssg >}}/2020/02/08-markdown-resources.png
[image-assets-tree]:    {{< assets-ssg >}}/2020/02/08-assets-tree.png
[image-html]:           {{< assets-ssg >}}/2020/02/08-html-include.png
[image-mathjax]:        {{< assets-ssg >}}/2020/02/08-markdown-mathjax.png
