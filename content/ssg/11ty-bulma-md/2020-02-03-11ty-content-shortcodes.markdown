---
type   : post
title  : "Eleventy - Content - Shortcodes"
date   : 2020-02-03T09:17:35+07:00
slug   : 11ty-content-shortcodes
categories: [ssg]
tags      : [11ty, bulma]
keywords  : [markdown, shortcode, parameter argument]
author : epsi
opengraph:
  image: assets-ssg/2020/02/08-markdown-advert.png

toc    : "toc-2020-01-11ty-bulma-md-step"

excerpt:
  Building 11ty Site Step by step, with Bulma MD as stylesheet frontend.
  Using shortcode in markdown.
---

### Preface

> Goal: Using shortcode in markdown.

#### Source Code

This article use [tutor-08][tutor-html-master-08] theme.
We will create it step by step.

-- -- --

### 5: Shortcode: Simple

We can utilize shortcodes to provide link,
or to provide image in `eleventy`.

#### Shortcodes

We need to add a few shortcodes in `.eleventy.js`:

* [gitlab.com/.../.eleventy.js][tutor-configuration]

{{< highlight javascript >}}
  // Internal Path
  eleventyConfig.addShortcode("pathPrefix", function() {
    return config.pathPrefix;
  });

  // External Path
  eleventyConfig.addShortcode("dotfiles", function() {
    return 'https://gitlab.com/epsi-rns/dotfiles/tree/master';
  });

  eleventyConfig.addShortcode("repo", function() {
    return 'https://gitlab.com/epsi-rns/tutor-11ty-bulma-md/tree/master/';
  });
{{< / highlight >}}

We can use this later in markdown as nunjucks code as shown below:

{{< highlight jinja >}}
[local-whats-next]: {% pathPrefix %}lyrics/soulfly-jump-da-fuck-up/
[link-dotfiles]:    {% dotfiles %}/terminal/vimrc/vimrc.bandithijo
{{< / highlight >}}

#### Page Content: Markdown with Shortcut

Again, content for example purpose

* [gitlab.com/.../views/lyrics/ratm-bulls-on-parade.md][tutor-vc-pa-bulls-on]

{{< highlight markdown >}}
---
layout    : post
title     : RATM - Bulls on Parade
date      : 2018-02-15 07:35:05
tags      : ["nu metal", "90s"]
---

[Terminal Dofiles][one-link].

* While arms warehouses fill as quick as the cells

* Rally round tha family, pocket full of shells

![Business Card][one-image]

-- -- --

### Example Dotfiles

ViM RC: [bandithijo][link-dotfiles]

-- -- --

### What's next ?

Our next song would be [Soulfly - Jump Da Fuck Up][local-whats-next]

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]: {% pathPrefix %}lyrics/soulfly-jump-da-fuck-up/
[link-dotfiles]:    {% dotfiles %}/terminal/vimrc/vimrc.bandithijo

[one-image]:    {% pathPrefix %}assets/images/adverts/one-page.png
[one-link]:     {% dotfiles %}/terminal/
{{< / highlight >}}

![11ty Markdown: Image and Link in Markdown using Shortcodes][image-shortcodes]

-- -- --

### 6: Shortcode: Argument

We can also utilize more advance shortcode,
that can be equipped with parameter argument,
to provide `raw HTML` in the middle of content.

#### Shortcode

We need to add a few shortcodes in `.eleventy.js`:

* [gitlab.com/.../.eleventy.js][tutor-configuration]

{{< highlight javascript >}}
  // Multiline Content with Parameter
  eleventyConfig.addShortcode("advert", function(src) {
    return `
  <img alt="advertisement" 
       src="${config.pathPrefix}assets/images/adverts/${src}">
    `;
  });
{{< / highlight >}}

We can use this `advert` shortcode later,
in markdown as nunjucks code as shown below:

{{< highlight jinja >}}
{% advert "oto-spies-01.png" %}
{{< / highlight >}}

#### Interpolation

> Me too

You might curious what this codes meaning.

{{< highlight html >}}
  <img alt="advertisement" 
       src="${config.pathPrefix}assets/images/adverts/${src}">
{{< / highlight >}}

It is all in the official documentation:

* [www.11ty.dev/docs/shortcodes](https://www.11ty.dev/docs/shortcodes/)

#### Page Content: Markdown with Shortcut

Again, content for example purpose

* [gitlab.com/.../views/lyrics/disturbed-stupify.md][tutor-vc-pa-disturbed]

{{< highlight jinja >}}
---
layout    : post
title     : Disturbed - Stupify
date      : 2018-09-13 07:35:05
tags      : ["nu metal", "2000s"]
---

Look in my face, stare in my soul
<!--more-->

{% advert "oto-spies-01.png" %}

Why, do you like playing around with\
My, narrow scope of reality\
I, can feel it all start slipping\
I think I'm breaking down
{{< / highlight >}}

![11ty Markdown: Advertisement in Markdown using Shortcode][image-advert]

Now it is your choice, which one is easier for you.

-- -- --

### What is Next ?

Consider continue reading [ [Eleventy - Syntax Highligting][local-whats-next] ].
We are going to explore syntax highlighting in markdown content.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/02/04/11ty-syntax-highlighting/

[tutor-html-master-08]: {{< tutor-11ty-bulma-md >}}/tutor-08/

[tutor-configuration]:  {{< tutor-11ty-bulma-md >}}/tutor-08/.eleventy.js
[tutor-vc-pa-bulls-on]: {{< tutor-11ty-bulma-md >}}/tutor-08/views/lyrics/ratm-bulls-on-parade.md
[tutor-vc-pa-disturbed]:{{< tutor-11ty-bulma-md >}}/tutor-08/views/lyrics/disturbed-stupify.md

[image-shortcodes]:     {{< assets-ssg >}}/2020/02/08-markdown-shortcodes.png
[image-advert]:         {{< assets-ssg >}}/2020/02/08-markdown-advert.png

