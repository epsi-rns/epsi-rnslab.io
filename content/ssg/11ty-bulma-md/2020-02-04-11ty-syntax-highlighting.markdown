---
type   : post
title  : "Eleventy - Syntax Highlighting"
date   : 2020-02-04T09:17:35+07:00
slug   : 11ty-syntax-highlighting
categories: [ssg]
tags      : [11ty, bulma]
keywords  : [markdown, shortcode, responsive image]
author : epsi
opengraph:
  image: assets-ssg/2020/02/08-highlight-fixed.png

toc    : "toc-2020-01-11ty-bulma-md-step"

excerpt:
  Building 11ty Site Step by step, with Bulma MD as stylesheet frontend.
  A method to highlight code.
---

### Preface

> Goal: A method to highlight code.

#### Source Code

This article use [tutor-08][tutor-html-master-08] theme.
We will create it step by step.

-- -- --

### 1: Highlighting

There are at least two way to highlight in Hexo.
Both should show similar result.

#### Triple Backtick

{{< highlight markdown >}}
```haskell
-- Layout Hook
commonLayout = renamed [Replace "common"]
    $ avoidStruts 
    $ gaps [(U,5), (D,5)] 
    $ spacing 10
    $ Tall 1 (5/100) (1/3)
```
{{< / highlight >}}

#### Highlight Tag Directive

{{< highlight markdown >}}
{% highlight "haskell" %}
-- Layout Hook
commonLayout = renamed [Replace "common"]
    $ avoidStruts 
    $ gaps [(U,5), (D,5)] 
    $ spacing 10
    $ Tall 1 (5/100) (1/3)
{% endhighlight %}
{{< / highlight >}}

#### Configuration

To enable this you should configure `.eleventy.js` first.

* [gitlab.com/.../.eleventy.js][tutor-configuration]

{{< highlight javascript >}}
  // Official plugin
  eleventyConfig.addPlugin(syntaxHighlight);
{{< / highlight >}}

#### Official Documentation

* [www.11ty.dev/docs/plugins/syntaxhighlight](https://www.11ty.dev/docs/plugins/syntaxhighlight/)

-- -- --

### 2: Fixing Bulma Stylesheet Issue

Eleventy's highlight has a `pre .number` class,
that conflict Bulma `.number` class.
This is nobody's fault since `.number` is,
very common english word in CSS world.

### Page Content: Example

Now edit one of your content, and put some code block.

* [gitlab.com/.../views/lyrics/soulfly-jump-da-fuck-up.md][tutor-vc-pa-soulfly]

{{< highlight markdown >}}
---
layout    : post
title     : Soulfly - Jump Da Fuck Up
date      : 2018-09-07 07:35:05
tags      : ["nu metal", "2000s"]
---

{% highlight "haskell" %}
-- Layout Hook
commonLayout = renamed [Replace "common"]
    $ avoidStruts 
    $ gaps [(U,5), (D,5)] 
    $ spacing 10
    $ Tall 1 (5/100) (1/3)
{% endhighlight %}
{{< / highlight >}}

The result in the browser is not what we are expected.

![11ty Syntax Highlighting: Formatting Issue][image-highlight-issue]

The stylesheet is mixed-up with Bulma stylesheet.

#### Reset Number Class in SASS

Bulma is using `.number` element,
while formatting the syntax highlight.
What we need to do is unstyle the element.

#### SASS: Syntax Highlight

* [gitlab.com/.../sass/css/post/_reset-highlight.sass][tutor-sass-highlight]

{{< highlight scss >}}
pre
  .number
    font-size: 100%
    display: inline
    margin-right: 0
    padding: 0 0
    background-color: transparent
{{< / highlight >}}

#### Render: Browser

Now you can see have your expected result in the browser.

![11ty Syntax Highlighting: Formatting Fixed][image-highlight-fixed]

-- -- --

### What is Next ?

We are done with all topics in this article series.

Consider continue reading [ [Eleventy - Summary][local-whats-next] ].
There is at last a summary,
about what Eleventy can do in this article series.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/02/06/11ty-summary/

[tutor-html-master-08]: {{< tutor-11ty-bulma-md >}}/tutor-08/

[tutor-configuration]:  {{< tutor-11ty-bulma-md >}}/tutor-08/.eleventy.js
[tutor-vc-pa-soulfly]:  {{< tutor-11ty-bulma-md >}}/tutor-08/views/lyrics/soulfly-jump-da-fuck-up.md
[tutor-sass-highlight]: {{< tutor-11ty-bulma-md >}}/tutor-08/sass/css/post/_reset-highlight.sass

[image-highlight-fixed]:{{< assets-ssg >}}/2020/02/08-highlight-fixed.png
[image-highlight-issue]:{{< assets-ssg >}}/2020/02/08-highlight-issue.png
