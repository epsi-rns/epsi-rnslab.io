---
type   : post
title  : "Windows - Chocolatey - Hugo"
date   : 2019-01-21T09:17:35+07:00
slug   : windows-choco-hugo
categories: [ssg, frontend]
tags      : [hugo, cli]
keywords  : [static site, windows, chocolatey, cmder]
author : epsi
opengraph:
  image: assets/site/images/topics/hugo.png

toc    : "toc-2019-01-windows-cli"

excerpt:
  Windows can have powerful command line environment using
  chocolatey package manager and cmder terminal.
  Hugo can make use of these tools,
  to make you feels like home.
  Mimicing linux environment.

---

### Preface

> Goal: Using comfortable CLI environment in Windows for Hugo.

-- -- --

### 1: Windows: Setting up CLI Environment

I have written the basic setup in separate article in my other blog.
You should read this before you begin:

*	[Windows: Setting up CLI Environment][github-windows-cli]

-- -- --

### 2: Hugo: Install

Installing is pretty easy.

{{< highlight bash >}}
$ choco install hugo
{{< / highlight >}}

![Windows cmder: choco install hugo][image-ss-choco-install-hugo]

-- -- --

### 3: Hugo: Run Server

Now you can try Hugo as usual, in windows <code>cmder</code>.

{{< highlight bash >}}
$ md d:/cli/test-hugo
$ cd d:/cli/test-hugo/

$ hugo new site .
{{< / highlight >}}

![Windows cmder: hugo new site][image-ss-cmder-hugo-new-site]

Then run server as well

{{< highlight bash >}}
$ cd D:\cli\demo-hugo\
$ hugo server

...

Web Server is available at http://localhost:1313/ (bind address 127.0.0.1)
Press Ctrl+C to stop
{{< / highlight >}}

![Windows cmder: hugo server][image-ss-cmder-hugo-server]

And finally see the result in browser such as example below.

![Browser: hugo localhost][image-ss-hugo-localhost]

-- -- --

### What is Next ?

There is other article, that you might need to read.
Consider continue reading [ [Windows - Chocolatey - Sass][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}frontend/2019/01/23/windows-choco-sass/
[github-windows-cli]:       https://epsi-rns.github.io/system/2019/01/21/windows-cli-environment.html

[image-ss-choco-install-hugo]:  {{< assets-ssg >}}/2019/01/windows-02-choco-install-hugo.png
[image-ss-cmder-hugo-new-site]: {{< assets-ssg >}}/2019/01/windows-02-cmder-hugo-new-site.png
[image-ss-cmder-hugo-server]:   {{< assets-ssg >}}/2019/01/windows-02-cmder-hugo-server.png
[image-ss-hugo-localhost]:      {{< assets-ssg >}}/2019/01/windows-02-windows-hugo-localhost.png

