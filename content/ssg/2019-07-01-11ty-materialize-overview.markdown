---
type   : post
title  : "Eleventy - Materialize - Overview"
date   : 2019-07-01T09:17:35+07:00
slug   : 11ty-materialize-overview
categories: [ssg]
tags      : [11ty]
keywords  : [static site, custom theme, summary, overview]
author : epsi
opengraph:
  image: assets-ssg/2019/07/11ty-materialize-preview.png

toc    : "toc-2020-03-repository"

excerpt:
  Building Eleventy Site Step by step,
  with Materialize as stylesheet frontend.

---

### Preface

> Goal: Explanation of Eleventy Repository Step By Step

I have been very busy lately.
I can't playback yesterday when I have time to write my blog.
So here is the summary of Eleventy material that I have made.

#### Repository

* [Eleventy Materialize Test Drive][repository]

#### Eleventy Materialize Test Drive

An example of 11ty site using Materialize for personal learning purpose.

> 11ty + Nunjucks + Materialize

![Eleventy Materialize: Preview][11ty-materialize-preview]

-- -- --

### Links

#### Demo Site

* <https://eleventy-step.netlify.com/>

#### Eleventy Step By Step

This repository:

* [Eleventy Step by Step Repository][tutorial-11ty]

#### Materialize Step By Step

Additional guidance:

* [Materialize Step by Step Repository][tutorial-materialize]

[tutorial-11ty]:        https://gitlab.com/epsi-rns/tutor-11ty-materialize/
[tutorial-materialize]: https://gitlab.com/epsi-rns/tutor-html-materialize/

-- -- --

### Chapter Step by Step

#### Tutor 01

> Generate Only Pure HTML

* Eleventy Configuration

* Layout: Base, Page, Post, Home, Archives, Index

* Layout: Tags List and Generate Each Tag Name (Using Pagination)

* Basic Content

![Eleventy Materialize: Tutor 01][11ty-materialize-preview-01]

-- -- --

#### Tutor 02

* Add Materialize CSS

* Nice Header and Footer

* Enhance All Layouts with Materialize CSS

![Eleventy Materialize: Tutor 02][11ty-materialize-preview-02]

-- -- --

#### Tutor 03

* Add Custom SASS (Custom Design)

* Apply Two Column Responsive Layout for Most Layout

* Nice Badge in Tags Page

![Eleventy Materialize: Tutor 03][11ty-materialize-preview-03]

-- -- --

#### Tutor 04

* More Content: Lyrics and Quotes. Need this content for demo

* Simplified All Layout Using Nunjucks Template Inheritance

* Configuration Stuff and Helper: Filter, Collection, Shortcodes

* Archives: Simple, By Year, List Tree (By Year and Month)

* Tags: List Tree

* Excerpt (<!--more-->): Read More Separator

* Custom Output: JSON

![Eleventy Materialize: Tutor 04][11ty-materialize-preview-04]

-- -- --

#### Tutor 05

* Multi Column Responsive List: Tag, and Archive

* Widget: Friends, Archives Tree, Tags, Recent Post, Related Post

* Pagination: Adjacent, Indicator, Responsive

* Post: Header, Footer, Navigation

![Eleventy Materialize: Tutor 05][11ty-materialize-preview-05]

-- -- --

#### Tutor 06

> Finishing

* Layout: Service

* More Pagination: Archive

* Post: Markdown Content

* Post: Table of Content

* Post: Responsive Images

* Post: Shortcodes: pathPrefix, image

* Official Plugin: Syntax Highlight, RSS

* Meta: HTML, SEO, Opengraph, Twitter

![Eleventy Materialize: Tutor 06][11ty-materialize-preview-06]

-- -- --

What do you think ?

[//]: <> ( -- -- -- links below -- -- -- )

[repository]:   https://gitlab.com/epsi-rns/tutor-11ty-materialize

[11ty-materialize-preview]:   https://gitlab.com/epsi-rns/tutor-11ty-materialize/raw/master/11ty-materialize-preview.png
[11ty-materialize-preview-01]:https://gitlab.com/epsi-rns/tutor-11ty-materialize/raw/master/tutor-01/11ty-materialize-preview.png
[11ty-materialize-preview-02]:https://gitlab.com/epsi-rns/tutor-11ty-materialize/raw/master/tutor-02/11ty-materialize-preview.png
[11ty-materialize-preview-03]:https://gitlab.com/epsi-rns/tutor-11ty-materialize/raw/master/tutor-03/11ty-materialize-preview.png
[11ty-materialize-preview-04]:https://gitlab.com/epsi-rns/tutor-11ty-materialize/raw/master/tutor-04/11ty-materialize-preview.png
[11ty-materialize-preview-05]:https://gitlab.com/epsi-rns/tutor-11ty-materialize/raw/master/tutor-05/11ty-materialize-preview.png
[11ty-materialize-preview-06]:https://gitlab.com/epsi-rns/tutor-11ty-materialize/raw/master/tutor-06/11ty-materialize-preview.png

