---
type   : post
title  : "Pelican - Bulma MD - Overview"
date   : 2019-12-01T09:17:35+07:00
slug   : pelican-bulma-md-overview
categories: [ssg]
tags      : [pelican]
keywords  : [static site, custom theme, summary, overview]
author : epsi
opengraph:
  image: assets-ssg/2019/11/pelican-bulma-md-preview.png

toc    : "toc-2020-03-repository"

excerpt:
  Building Pelican Site Step by step,
  with Bulma as stylesheet frontend,
  featuring Material Design.

---

### Preface

> Goal: Explanation of Pelican Repository Step By Step

Here is the summary of Pelican material that I have made,
using Jinja2 template.

I'm using Bulma, enhanced with custom SASS containing

* Custom Helper Spacing

* Material Design

#### Repository

* [Pelican Bulma Test Drive][repository]

#### Pelican Bulma Test Drive

An example of pelican site using Bulma for personal learning purpose.

> Pelican + Jinja2 + Bulma + Custom SASS

![Pelican Bulma MD: Preview][pelican-bulma-md-preview]

-- -- --

### Links

#### Pelican Step By Step

This repository:

* [Pelican Step by Step Repository][tutorial-pelican]

#### Frankenbulma (Bulma Material Design) Step By Step

Additional guidance:

* [Bulma Material Design Step by Step Repository][tutorial-bulma-md]

[tutorial-pelican]:      https://gitlab.com/epsi-rns/tutor-pelican-bulma-md/
[tutorial-bulma-md]:    https://gitlab.com/epsi-rns/tutor-html-bulma-md/

-- -- --

### Chapter Step by Step

#### Tutor 01

> Generate Only Pure HTML

* Setup Directory for Minimal Pelican Theme

* General Layout: Base, Page, Article, Index

* Partials: HTML Head, Header, Footer

* Jinja2: Manage Heading Title

* Basic Content: Pages, Posts

![Pelican Bulma MD: Tutor 01][pelican-bulma-md-preview-01]

-- -- --

#### Tutor 02

> Generate Complete Pure HTML Template

* Additional List Layout: Archives, Period, Authors, Categories, Tags

* Additional Single Layout: Author, Category, Tag

* Jinja2: Basic Loop

* More Content: Lyrics. Need this content for demo

![Pelican Bulma MD: Tutor 02][pelican-bulma-md-preview-02]

-- -- --

#### Tutor 03

> Add Bulma CSS Framework

* Add Bulma CSS

* Standard Header (jquery or vue) and Footer

* Enhance All Layouts with Bulma CSS

* Apply Bulma Two Column Responsive Layout for Most Layout

![Pelican Bulma MD: Tutor 03][pelican-bulma-md-preview-03]

-- -- --

#### Tutor 04

> Combine with Custom SASS Material Design

* Add (a bunch of) Custom SASS (Custom Design)

* Nice Header and Footer

* Index Content: Index, Categories, Tags, Periods, Archives By Year and Month

* Enhance All Two Column Responsive Layout with Material Design

* Nice Tag Badge in Tags Page

![Pelican Bulma MD: Tutor 04][pelican-bulma-md-preview-04]

-- -- --

#### Tutor 05

> Loop with Jinja2

* Simplified All Layout Using Jinja2 Template Inheritance

* List Tree: Archives: By Year and Month

* List Tree: Tags and Categories

* Article Index: Blog Posts

* Index Layout: Blog List, Terms, By Chronology

* Home: Landing Page

* Custom Output: Text

![Pelican Bulma MD: Tutor 05][pelican-bulma-md-preview-05]

-- -- --

#### Tutor 06

> Features

* Widget: Friends, Archives Tree, Categories, Tags, Recent Post, Related Post

* Refactoring Template using Capture: Widget

* Pagination: Adjacent, Indicator, Responsive

* Post: Header, Footer, Navigation

* Python Data: Archives, Friends

* Python: Jinja2 Filter

![Pelican Bulma MD: Tutor 06][pelican-bulma-md-preview-06]

-- -- --

#### Tutor 07

> Finishing

* Layout: Service (dummy)

* Post: Markdown Content (test case)

* Post: Table of Content (dynamic include)

* Post: Responsive Images

* Post: Syntax Highlight (sass)

* Post: Macros (include with parameter)

* Official Plugin: Feed

* Meta: HTML, SEO, Opengraph, Twitter

* Python Plugin: Jinja2Content

* Multi Column Responsive List: Categories, Tags, and Archives

![Pelican Bulma MD: Tutor 07][pelican-bulma-md-preview-07]

-- -- --

What do you think ?

[//]: <> ( -- -- -- links below -- -- -- )

[repository]:   https://gitlab.com/epsi-rns/tutor-pelican-bulma-md

[pelican-bulma-md-preview]:     https://gitlab.com/epsi-rns/tutor-pelican-bulma-md/raw/master/pelican-bulma-md-preview.png
[pelican-bulma-md-preview-01]:  https://gitlab.com/epsi-rns/tutor-pelican-bulma-md/raw/master/step-01/pelican-bulma-md-preview.png
[pelican-bulma-md-preview-02]:  https://gitlab.com/epsi-rns/tutor-pelican-bulma-md/raw/master/step-02/pelican-bulma-md-preview.png
[pelican-bulma-md-preview-03]:  https://gitlab.com/epsi-rns/tutor-pelican-bulma-md/raw/master/step-03/pelican-bulma-md-preview.png
[pelican-bulma-md-preview-04]:  https://gitlab.com/epsi-rns/tutor-pelican-bulma-md/raw/master/step-04/pelican-bulma-md-preview.png
[pelican-bulma-md-preview-05]:  https://gitlab.com/epsi-rns/tutor-pelican-bulma-md/raw/master/step-05/pelican-bulma-md-preview.png
[pelican-bulma-md-preview-06]:  https://gitlab.com/epsi-rns/tutor-pelican-bulma-md/raw/master/step-06/pelican-bulma-md-preview.png
[pelican-bulma-md-preview-07]:  https://gitlab.com/epsi-rns/tutor-pelican-bulma-md/raw/master/step-07/pelican-bulma-md-preview.png

