---
type   : post
title  : "Hugo - Bulma MD - Overview"
date   : 2020-03-25T09:17:35+07:00
slug   : hugo-bulma-md-overview
categories: [ssg]
tags      : [hugo]
keywords  : [static site, custom theme, summary, overview]
author : epsi
opengraph:
  image: assets-ssg/2020/03/hugo-bulma-md-preview.png

toc    : "toc-2020-03-repository"

excerpt:
  Building Hugo Site Step by step,
  with Bulma as stylesheet frontend,
  featuring Material Design.

---

### Preface

> Goal: Explanation of Hugo Repository Step By Step

Here is the summary of Hugo with Bulma CSS Framework.
Hugo is using Chroma template.

I'm using Bulma, enhanced with custom SASS containing

* Custom Helper Spacing

* Material Design

#### Hugo Bulma Test Drive

An example of Hugo site using Bulma for personal learning purpose.

> Hugo (Chroma) + Bulma + Custom SASS

![Hugo Bulma MD: Preview][hugo-bulma-md-preview]

-- -- --

### Links

#### Hugo Step By Step

This repository:

* [Hugo Step by Step Repository][tutorial-hugo]

#### Frankenbulma (Bulma Material Design) Step By Step

Additional guidance:

* [Bulma Material Design Step by Step Repository][tutorial-bulma-md]

[tutorial-hugo]:    https://gitlab.com/epsi-rns/tutor-hugo-bulma-md/
[tutorial-bulma-md]:https://gitlab.com/epsi-rns/tutor-html-bulma-md/

-- -- --

### Chapter Step by Step

#### Tutor 01

> Generate Only Pure HTML

* Setup Directory for Minimal Hugo

* General Layout: Base, List, Single

* Partials: HTML Head, Header, Footer

* Additional Layout: Post

* Basic Content

![Hugo Bulma MD: Tutor 01][hugo-bulma-md-preview-01]

-- -- --

#### Tutor 02

* Add Pure Bulma CSS

* Standard Header (jquery or vue) and Footer

* Enhance All Layouts with Bulma CSS

![Hugo Bulma MD: Tutor 02][hugo-bulma-md-preview-02]

-- -- --

#### Tutor 03

* Add Custom SASS (Custom Design)

* Nice Header and Footer

* General Layout: Terms, Taxonomy

* Apply Two Column Responsive Layout for Most Layout

* Nice Tag Badge in Tags Page

![Hugo Bulma MD: Tutor 03][hugo-bulma-md-preview-03]

-- -- --

#### Tutor 04

* More Content: Lyrics and Quotes. Need this content for demo

* Additional Layout: Archives

* Custom Output: YAML, TXT, JSON

* Article Index: By Year, List Tree (By Year and Month)

![Hugo Bulma MD: Tutor 04][hugo-bulma-md-preview-04]

-- -- --

#### Tutor 05

* Post: Header, Footer, Navigation

> Optional Feature

* Blog Pagination: Adjacent, Indicator, Responsive

* Widget: Friends, Archives Tree, Categories, Tags, Recent Post, Related Post

* Article Index: Apply Box Design

![Hugo Bulma MD: Tutor 05][hugo-bulma-md-preview-05]

-- -- --

#### Tutor 06

> Finishing

* Article Index: Apply Multi Column Responsive List

* Layout: Service

* Post: Markdown Content

* Post: Table of Content

* Post: Responsive Images

* Post: Shortcodes

* Post: Syntax Highlight

* Post: Bulma Title: CSS Fix

* Meta: HTML, SEO, Opengraph, Twitter

* RSS: Filter to Show Only Post Kind

* Search: lunr.js

![Hugo Bulma MD: Tutor 06][hugo-bulma-md-preview-06]

-- -- --

What do you think ?

[//]: <> ( -- -- -- links below -- -- -- )

[repository]:   https://gitlab.com/epsi-rns/tutor-hugo-bulma-md

[hugo-bulma-md-preview]:     https://gitlab.com/epsi-rns/tutor-hugo-bulma-md/raw/master/hugo-bulma-md-preview.png
[hugo-bulma-md-preview-01]:  https://gitlab.com/epsi-rns/tutor-hugo-bulma-md/raw/master/step-01/hugo-bulma-md-preview.png
[hugo-bulma-md-preview-02]:  https://gitlab.com/epsi-rns/tutor-hugo-bulma-md/raw/master/step-02/hugo-bulma-md-preview.png
[hugo-bulma-md-preview-03]:  https://gitlab.com/epsi-rns/tutor-hugo-bulma-md/raw/master/step-03/hugo-bulma-md-preview.png
[hugo-bulma-md-preview-04]:  https://gitlab.com/epsi-rns/tutor-hugo-bulma-md/raw/master/step-04/hugo-bulma-md-preview.png
[hugo-bulma-md-preview-05]:  https://gitlab.com/epsi-rns/tutor-hugo-bulma-md/raw/master/step-05/hugo-bulma-md-preview.png
[hugo-bulma-md-preview-06]:  https://gitlab.com/epsi-rns/tutor-hugo-bulma-md/raw/master/step-06/hugo-bulma-md-preview.png


