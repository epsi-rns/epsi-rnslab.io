---
type   : post
title  : "Slides - Concept SSG"
date   : 2020-10-11T09:13:35+07:00
slug   : slides-concept-ssg
categories: [ssg]
tags      : [presentation]
keywords  : [static site, markdown, advantage, disadvantage]
author : epsi
opengraph:
  image: assets-ssg/2020/what-ssg-good-for.png

excerpt:
  Presentation of SSG concept.
  Introduction to static site generator.

---

### The Document

This article contain all article slides.
Slide by slide, so anyone can copy desired slide,
without the need to open the Impress slide, or the PDF version.

![Teaser Preview: Concept SSG][teaser-preview]

I believe, a ready to use slide,
is useful to help people in the group discussion.

#### Original Presentation

I usually draft a presentation in a simple text based,
that I can write down in any text editor.

Then I show in web based for a few moment (actually months).
This give time to append any additional material freely,
create necessary figure, and also correct any typo, or even dead links.

You can watch the presentation here:

* [Presentation - Concept SSG][local-presentation]

This text based presentation is the most authentic version,
with all the links required provided.

#### Impress

This presentation is made in [LibreOffice Impress][libreoffice-impress],
using [candyclone template][candyclone-template].

You can download the Impress document from a page in here:

* [presentation-concept-ssg-diagram.odp][document-impress]

With this downloadable document,
you can use the style of this presentation,
for use with your own presentation works,
either for school material, office works, homeworks, or anything else.

#### PDF

You can also download the exported pdf from a page in here:

* [presentation-concept-ssg-diagram.pdf][document-pdf]

The pdf version cannot contain animation.
So the content might be little different,
compared with the original Impress document.

#### Inkscape

The Impress pages also utilize diagram illustration made in Inkscape.
I also provide the source image in SVG format,
that you can download from a page in here:

* [presentation-concept-ssg-content.svg][document-inkscape]

Here is the preview.

![Inkscape Illustration: Concept SSG][inkscape-thumbs]

Or better, you can get it all from candyclone illustration:

* [template-candyclone-content.svg][candyclone-content]

I intentionally share the source SVG image,
so you can alter the content for your own personal use.
I know there are already so many stock images for presentation,
I just need to share, what I have, with tutorial.
This way, anyone can make pretty presentation easier.
Of course you still have to learn Inkscape,
to suit the illustration for your own use.

#### Template

> What is this candyclone rubbish?

Candyclone is an Impress template that I have made for LibreOffice contest.

* [LibreOffice Impress Template Contest by the Indonesian Community][template-contest]

There are also other free templates as well in `lumbung` repository.

* [Lumbung LibreOffice Indonesia Repository][template-lumbung]
 
This candyclone is just an example template that you can use freely.
With this candyclone example you can also learn how to make your own template.
Your very own template to suit your ecosystem.

#### Disagreement

> What if I do not agree?

The source is available, so you can freely make your own slide.
Feel free to express your thoughts, either with text, or illustration.

-- -- --

### The Slides

Here I represent all the slides.

#### Slide 01: Cover

![Slide - Cover][slide-01]

> Static Site Generator

Introduction to SSG for beginner

#### Slide 02: About The Author

![Slide - About Author][slide-02]

> I have my own blog.

#### Slide 03: About This Material

![Slide - About Material][slide-03]

After watching this, you will understand:

How SSG can be utilized to
make a _fully mature website_
**without** the _burden of complicated backend_.

This material is not really comprehensive.
I still have so much to learn.

#### Slide 04: Chapter Break

![Slide - Preface][slide-04]

#### Slide 05: What is Static Site Generator?

![Slide - Preface: What is SSG?][slide-05]

What is SSG?

* SSG short abbreviation:
  * Static Sites Generator

* Static Sites:
  * Just plain HTML + CSS + JS

* Static:
  * No dynamic server (backend)

* Generated:
  * Built once, on every change
  * Put on server
  * Serve as many request as needed.

#### Slide 06: What is SSG good for?

![Slide - Preface: What is SSG Good for?][slide-06]

What is SSG good for?

* Company Profile
  * or Portfolio.

* Personal Blog
  * Just like mine.

* Official Site
  * Most Linux Distro use SSG.

* Any Static Sites:
  * Such as CSS Tricks.

* JAM Stack:
  * Dynamic sites utilizing javascript,
  * Modern Web Era, in contrast with LAMP stack.

#### Slide 07: Blog Preview

![Slide - Preface: Blog Preview][slide-07]

#### Slide 08: What case is not?

![Slide - Preface: What Case is Not SSG][slide-08]

What case is not?

* Non tech savvy end user
  * should use blogger, or wordpress instead.

* Startup
  * With a lot of infrastructure.

* Dynamic website
  * Anything that require login

* Walk your dog, or feed your cat
  * You still have to do it yourself.

#### Slide 09: Which Generator?

![Slide - Which Generator?][slide-09]

Which Generator?

* Conventional (regular site)
  * Jekyll, Hugo, Hexo, 11ty, Pelican

* Modern (web app)
  * Gatsby, Next, Nuxt

More Reference: [CSS Tricks: Comparing Static Site Generator Build Times][css-tricks]

[css-tricks]: https://css-tricks.com/comparing-static-site-generator-build-times/

#### Slide 10: Chapter Break

![Slide - Preface: Break Why SSG?][slide-10]

#### Slide 11: Why Static Sites?

![Slide - Preface: Why Static Sites][slide-11]

Why Static Sites?

* Markdown:
  * More on Writing Content

* No complex backend:
  * Focus on frontend: Great theme.

* Local Copy:
  * Own Your Own Site
  * Git-able

* Cheap:
  * Free Hosting =💲 murah meriah.

#### Slide 12: Tech Savvy

![Slide - Preface: Tech Savvy][slide-12]

Tech Savvy

* Embrace modern technology 🤠:
  * SPA, JAM stack, serverless, CI/CD.

* OS Agnostic (simpler ecosystem)
  * No racial issue, between Windows, Mac and Linux/BSD user.

* Many SSG Choice Options:
  * Jekyll, Hugo, Hexo, Pelican, Gatsby, 11ty.

* Easier to migrate content between generator.
  * After all, it is just markdown.

* Easier to port tailor made custom theme.
  * No scary language feature.

* Way cooler web developer:
  * Over traditional CMS

#### Slide 13: Other Technical Performance

![Slide - Preface: Other Technical Performance][slide-13]

Other Technical Performance.

* Fast:
  * No server side processing.

* Less Security Issue:
  * SQL Injection and Vulnerablities.

* Cacheable
  * Can be reliaby cached, and maybe served as CDN.

* No scaling issue.
  * It is all just static html and assets.

#### Slide 14: Chapter Break

![Slide - User Tasks][slide-14]

#### Slide 15: Simplified User Tasks

![Slide - User Tasks: Simplified User Tasks][slide-15]

User Tasks: Only consist of this two tasks:

* Design Theme and Layout
* Writing Content, then Deploy

> No complex backend setting.

It means, more focus on the content.
Especially, with 3rd party theme.

#### Slide 16: SSG Caveat

![Slide - User Tasks: Caveat][slide-16]

Even with a ready to use theme,
you still need customization to suit your needs.

#### Slide 17: Install SSG

![Slide - User Tasks: Install][slide-17]

Install

* Some are easy to install.
* Some are harder for non tech-savvy.

> Depend on the choice of SSG.

#### Slide 18: Own Your Own Site

![Slide - User Tasks: Own Your Own Site][slide-18]

You can have it all locally.

* Writing content in your favorite text editor.

* No upload image:
  Edit image directly in your notebook.

* No need to dump database.

> In short: More control.

#### Slide 19: Chapter Break

![Slide - Theming][slide-19]

#### Slide 20: Split HTML Task

![Slide - Theming: Split HTML Task][slide-20]

Split HTML Task:

* Theme:
  * Structure (Layout, and Skeleton)

* Markdown Engine:
  * Formatting

> In short: clear division between these two tasks.

#### Slide 21: Theme Making

![Slide - Theming: Theme Making][slide-21]

Theme Making 🦋

* Template Engine:
  * Block and Basic Coding

* HTML Skeleton:
  * Layout Structure

* Stylesheet:
  * Presenting Nice Looks

#### Slide 22: Template Engine

![Slide - Theming: Template Engine][slide-22]

Template Engine

> liquid, ejs, nunjucks, jinja2, etc...

* Block Building

* Basic Language:
  * Variable, Loop, Conditional

* Static Data Files:
  * from yaml, or toml, or json

* Specific Feature:
  * Pipeline

#### Slide 23: Stylesheet Presentation

![Slide - Theming: Stylesheet][slide-23]

Yet Another Presentation:

* [CSS Frameworks and CSS Tools](/frontend/2019/02/15/concept-css/)

> You might also need to understand CSS Tools.

#### Slide 24: Main Configuration

![Slide - Theming: Main Configuration][slide-24]

> Main Configuration: Contain: Global Stuff.

#### Slide 25: Inside a Page

![Slide - Theming: Page][slide-25]

Inside a Page

* Frontmatter:
  * Per-page configuration

* Content:
  * Markdown or HTML

#### Slide 26: Chapter Break

![Slide - Free Tutorials][slide-26]

#### Slide 27: Step by Step Examples?

![Slide - Tutorial: Step by Step Examples][slide-27]

Free Tutorials!
* [Eleventy Overview (Nunjucks + Bulma MD)](/ssg/2020/01/01/11ty-bulma-md-overview/)
* [Jekyll Overview (Liquid + No Stylesheet)](/ssg/2020/06/01/jekyll-overview/)
* [Hugo Summary (Template + Bulma)](/ssg/2019/04/01/hugo-overview/)
* [Hugo Summary (Template + Bootstrap)](/ssg/2018/09/01/hugo-summary/)
* [Hexo Overview (EJS + Bulma)](/ssg/2019/05/01/hexo-overview/)

> More examples on Repository (11ty, Jekyll, Pelican).

#### Slide 28: Chapter Break

![Slide - Markdown][slide-28]

#### Slide 29: Why Markdown?

![Slide - Markdown: Why Markdown][slide-29]

> More on Writing Content 🤠!

#### Slide 30: Issue with HTML Content

![Slide - Markdown: Issue with HTML Content][slide-30]

Issue with HTML Content

* Time spent on doing formatting.
  * Wasted time = Less creativity.

* Easily get lost of concentration in writing.
  * Dammit, my article is sucks!

* Reformatting:
  * Whenever design changes.

* Error prone.
  * Dad, I forget to close the tag again.

> In short: Markup focus on document formatting.

#### Slide 31: Markdown Content

![Slide - Markdown: Content][slide-31]

Markdown Content

> Recipe: Markdown focus on document structure.

* Easier to write content.
  * Plain Text = Painless Text.

* HTML formatting taken care by markdown engine.
  * Significantly reduce formatting time while writing content.
    Consistent formatting, by not doing manually.

* Inline Code.
  * Parsed, auto format with nice color.

* Text editor is sufficient.
  * No need for sophisticated tools.

#### Slide 32: Markdown Engine

![Slide - Markdown: Engine][slide-32]

> Implementation may vary

Each static site, have their own markdown engine.
_Be aware of slight difference_.

#### Slide 33: HTML Content

![Slide - Markdown: HTML Content][slide-33]

HTML Content

> When you need special formatting.

* HTML Page
  * Such as landing page content.

* Embedded Content in Markdown Page
  * Such as nice looks table of content.

#### Slide 34: Chapter Break

![Slide - Repository][slide-34]

#### Slide 35: Track Changes

![Slide - Repository: Track Changes][slide-35]

Track Changes

* Use Git!
  * Friendly neighbourhood backup platform.

* No weird file naming.
  * _my-revision-content.rev1.final.revagain.txt_

* Time travelling is possible.
  * OMG. I deleted my great idea last month!

* Multiple branch while making themes enhancement
  * branch: Master, development, bootstrapv4, bulma

#### Slide 36: Chapter Break

![Slide - Deploy][slide-36]

#### Slide 37: Deploying Choices

![Slide - Deploy: Deploying Options][slide-37]

Deploying Choices

* Simple setup for CI/CD

* Repository and Host:
  * Gitlab, Github, Bitbucket

* CI/CD:
  * Travis, Circle-CI, Pipelines, Gitlab-CI

* Host and CI/CD:
  * Netlify, Zeit Now, Firebase

#### Slide 38: Configuration Examples

![Slide - Deploy: Configuration Examples][slide-38]

Link:

* [CI/CD - Deploy Overview](/devops/2020/02/10/ci-cd-overview/)

Configuration Examples

* Netlify, Zeit Now (Vercel)

* Manual Git: Refspec, Worktree

* Travis+Github

* CircleCI: Github, Bitbucket

* Github Workflows (Actions)

Both github.io, and gitlab.io, can directly utilize any SSG.

#### Slide 39: Repositories

![Slide - Deploy: Repositories][slide-39]

#### Slide 40: Chapter Break

![Slide - Issues][slide-40]

#### Slide 41: Issues with Static Site?

![Slide - Issue with Static Site][slide-41]

Issue with Static Site

* No commenting system:
  * Except embedded.

* No internal search.

* Local build time,
  *  For site with many article,
     Or theme design with poor logic.

#### Slide 42: Chapter Break

![Slide - Free Tutorials Again][slide-42]

#### Slide 43: Again, Free Tutorials?

![Slide - Repository: Step by Step Examples][slide-43]

Step by Step Examples

* Hugo Repository
  * [Hugo + Template + Bulma MD](https://gitlab.com/epsi-rns/tutor-hugo-bulma-md)
  * [Hugo + Template + Bootstrap](https://gitlab.com/epsi-rns/tutor-hugo-bootstrap)

* Jekyll Repository
  * [Jekyll + Liquid + No Stylesheet](https://gitlab.com/epsi-rns/tutor-jekyll-plain)
  * [Jekyll + Liquid + Bootstrap OC](https://gitlab.com/epsi-rns/tutor-jekyll-bootstrap-oc/)
  * [Jekyll + Liquid + Bulma MD](https://gitlab.com/epsi-rns/tutor-jekyll-bulma-md)

* Pelican Repository
  * [Pelican + Jinja2 + Bulma MD](https://gitlab.com/epsi-rns/tutor-pelican-bulma-md)

* Eleventy Repository
  * [11ty + Nunjucks + Materialize](https://gitlab.com/epsi-rns/tutor-11ty-materialize)
  * [11ty + Nunjucks + Bulma MD](https://gitlab.com/epsi-rns/tutor-11ty-bulma-md)

* Hexo Repository
  * [Hexo + EJS + Bulma](https://gitlab.com/epsi-rns/tutor-hexo-bulma)

#### Slide 44: In Need of a Technical Material?

![Slide - Technical Materials][slide-44]

Technical Materials

* [Building Site with Eleventy](/ssg/2020/02/07/11ty-presentation/)

* [Building Site with Jekyll](/ssg/2020/08/21/jekyll-presentation/)

* [Building Site with Hexo](/ssg/2019/05/30/hexo-presentation/)

#### Slide 45: Chapter Break

![Slide - Summary][slide-45]

#### Slide 46: Summary

![Slide - Summary: Along with Local Communities][slide-46]

Summary: Along with Local Communities

* Static Site Generator
  * [Jekyll](https://t.me/jekyll_id)
  * Pelican
  * [Eleventy (11ty)](https://t.me/id_11ty)
  * [Hugo](https://t.me/gohugoid)
  * [Hexo](https://t.me/hexo_id)
  * [Gatsby](https://t.me/gatsbyjsid)

* Text
  * Markdown
  * reStructured Text

* Template Engine
  * Nunjucks, Jinja2, EJS, Liquid, Template.
  * Local Community: [JAMstackID](https://t.me/JAMstackID/)

* Deploy
  * CVS: Git, Mercurial: [freekelasgithub](https://t.me/freekelasgithub/)
  * Repository: Gitlab, Github, Bitbucket
  * CI/CD: Travis, Circle-CI, Pipelines: [IDDevOps](https://t.me/IDDEvOps/)
  * Static Host: Netlify, Render, Zeit Now.

#### Slide 47: Looking for the right tools?

![Slide - Summary: Blog Development Cycle][slide-47]

Link:

* [Practical Blog Development Cycle](/frontend/2020/03/21/practical-blog-cycle/)

> Learning The Hard Way.

#### Slide 48: What's Next?

![Slide - What's Next][slide-48]

Leverage to JAM stack!
* [staticgen.com](https://www.staticgen.com/)
* [@JAMstackID](https://t.me/JAMstackID)

#### Slide 49: Questions

![Slide - Questions][slide-49]

> Don't be shy!

#### Slide 50: Thank You

![Slide - Thank You][slide-50]

> Thank you for your time.

[//]: <> ( -- -- -- links below -- -- -- )

[teaser-preview]:           {{< assets-ssg >}}/2020/what-ssg-good-for.png
[local-presentation]:       {{< baseurl >}}ssg/2019/02/17/concept-ssg/
[candyclone-template]:      https://epsi-rns.gitlab.io/design/2020/09/21/inkscape-impress-slides-01/
[libreoffice-impress]:      https://www.libreoffice.org/discover/impress/
[document-impress]:         {{< berkas2-blob >}}/impress-presentation/04-concept-ssg/presentation-concept-ssg-diagram.odp
[document-pdf]:             {{< berkas2-blob >}}/impress-presentation/04-concept-ssg/presentation-concept-ssg-diagram.pdf
[document-inkscape]:        {{< berkas2-blob >}}/impress-presentation/04-concept-ssg/presentation-concept-ssg-content.svg
[candyclone-content]:       {{< berkas2-blob >}}/impress-template-candyclone/template-candyclone-content.svg
[inkscape-thumbs]:          {{< assets-ssg >}}/2020/ssg-thumbs-content-mix.png
[template-contest]:         https://blog.documentfoundation.org/blog/2020/10/12/libreoffice-impress-template-contest-by-the-indonesian-community/
[template-lumbung]:         https://lumbung.libreoffice.id/

[slide-01]: {{< assets-ssg >}}/2020/ssg/slide-01-cover.png
[slide-02]: {{< assets-ssg >}}/2020/ssg/slide-02-about-author.png
[slide-03]: {{< assets-ssg >}}/2020/ssg/slide-03-about-material.png
[slide-04]: {{< assets-ssg >}}/2020/ssg/slide-04-preface.png
[slide-05]: {{< assets-ssg >}}/2020/ssg/slide-05-preface-what-is-ssg.png
[slide-06]: {{< assets-ssg >}}/2020/ssg/slide-06-preface-what-is-ssg-good-for.png
[slide-07]: {{< assets-ssg >}}/2020/ssg/slide-07-preface-blog-preview.png
[slide-08]: {{< assets-ssg >}}/2020/ssg/slide-08-preface-what-case-is-not.png
[slide-09]: {{< assets-ssg >}}/2020/ssg/slide-09-which-generator.png
[slide-10]: {{< assets-ssg >}}/2020/ssg/slide-10-preface-break-why-ssg.png
[slide-11]: {{< assets-ssg >}}/2020/ssg/slide-11-preface-why-static-sites.png
[slide-12]: {{< assets-ssg >}}/2020/ssg/slide-12-preface-tech-savvy.png
[slide-13]: {{< assets-ssg >}}/2020/ssg/slide-13-preface-other-technical-preformance.png
[slide-14]: {{< assets-ssg >}}/2020/ssg/slide-14-user-tasks.png
[slide-15]: {{< assets-ssg >}}/2020/ssg/slide-15-user-tasks-simplified-users-tasks.png
[slide-16]: {{< assets-ssg >}}/2020/ssg/slide-16-user-tasks-caveat.png
[slide-17]: {{< assets-ssg >}}/2020/ssg/slide-17-user-tasks-install.png
[slide-18]: {{< assets-ssg >}}/2020/ssg/slide-18-user-tasks-own-your-own-site.png
[slide-19]: {{< assets-ssg >}}/2020/ssg/slide-19-theming.png
[slide-20]: {{< assets-ssg >}}/2020/ssg/slide-20-theming-split-html-task.png
[slide-21]: {{< assets-ssg >}}/2020/ssg/slide-21-theming-theme-making.png
[slide-22]: {{< assets-ssg >}}/2020/ssg/slide-22-theming-template-engine.png
[slide-23]: {{< assets-ssg >}}/2020/ssg/slide-23-theming-stylesheet.png
[slide-24]: {{< assets-ssg >}}/2020/ssg/slide-24-theming-main-configuration.png
[slide-25]: {{< assets-ssg >}}/2020/ssg/slide-25-theming-page.png
[slide-26]: {{< assets-ssg >}}/2020/ssg/slide-26-free-tutorials.png
[slide-27]: {{< assets-ssg >}}/2020/ssg/slide-27-tutorial-step-by-step-examples.png
[slide-28]: {{< assets-ssg >}}/2020/ssg/slide-28-markdown.png
[slide-29]: {{< assets-ssg >}}/2020/ssg/slide-29-markdown-why-markdown.png
[slide-30]: {{< assets-ssg >}}/2020/ssg/slide-30-markdown-issue-with-html-content.png
[slide-31]: {{< assets-ssg >}}/2020/ssg/slide-31-markdown-content.png
[slide-32]: {{< assets-ssg >}}/2020/ssg/slide-32-markdown-engine.png
[slide-33]: {{< assets-ssg >}}/2020/ssg/slide-33-html-content.png
[slide-34]: {{< assets-ssg >}}/2020/ssg/slide-34-repository.png
[slide-35]: {{< assets-ssg >}}/2020/ssg/slide-35-repository-track-changes.png
[slide-36]: {{< assets-ssg >}}/2020/ssg/slide-36-deploy.png
[slide-37]: {{< assets-ssg >}}/2020/ssg/slide-37-deploying-choices.png
[slide-38]: {{< assets-ssg >}}/2020/ssg/slide-38-deploy-configuration-examples.png
[slide-39]: {{< assets-ssg >}}/2020/ssg/slide-39-deploy-repositories.png
[slide-40]: {{< assets-ssg >}}/2020/ssg/slide-40-issues.png
[slide-41]: {{< assets-ssg >}}/2020/ssg/slide-41-issue-with-static-site.png
[slide-42]: {{< assets-ssg >}}/2020/ssg/slide-42-free-tutorials-again.png
[slide-43]: {{< assets-ssg >}}/2020/ssg/slide-43-repository-step-by-step-examples.png
[slide-44]: {{< assets-ssg >}}/2020/ssg/slide-44-technical-material.png
[slide-45]: {{< assets-ssg >}}/2020/ssg/slide-45-summary.png
[slide-46]: {{< assets-ssg >}}/2020/ssg/slide-46-summary-along-with-local-communities.png
[slide-47]: {{< assets-ssg >}}/2020/ssg/slide-47-summary-blog-development-cycle.png
[slide-48]: {{< assets-ssg >}}/2020/ssg/slide-48-whats-next.png
[slide-49]: {{< assets-ssg >}}/2020/ssg/slide-49-questions.png
[slide-50]: {{< assets-ssg >}}/2020/ssg/slide-50-thank-you.png
