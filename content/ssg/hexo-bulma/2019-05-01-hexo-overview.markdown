---
type   : post
title  : "Hexo - Overview"
date   : 2019-05-01T09:17:35+07:00
slug   : hexo-overview
categories: [ssg]
tags      : [hexo]
keywords  : [static site, custom theme, summary, overview]
author : epsi
opengraph:
  image: assets-ssg/2019/05/61-blog-post-all.png

toc    : "toc-2019-05-hexo-bulma-step"

excerpt:
  Building Hexo Site Step by step, with Bulma as stylesheet frontend.
  Overview of article series.

---

### Preface

> Goal: Explain Hexo Step By Step

#### A Journey with Hexo

After using Jekyll for years, and Hugo for more than a year,
I decided to start my third blog using Hexo.
And after a while, I also decide to write this article series,
so that beginner can learn more quickly.

I might write a lot, but never comes deep into conceptual thinking.
This article series is still the same.

However, Let me rewrite the Hugo article, in Hexo case.

#### The Bulma SASS Article Series

I use Bulma for this series.
The CSS part in this article series is
based on that Bulma article series.

Now we have two separate article series.

* [Bulma SASS][local-bulma-overview]: with pure HTML and no SSG.

* This newly Hexo Article Series.

#### Prerequisite

I suggest you to visit the [Bulma SASS][local-bulma-overview],
before you dive in to this long Hexo series.

I'm using <code>Bulma</code> as an example in this guidance.
Of course you can adapt this to any CSS framework that you like,
such as <code>Materialize</code> or <code>Bootstrap</code>.

-- -- --

### About Hexo

Hexo is an SSG (Static Site Generator) that is supported by **gitlab**.
Or used pairly by using **github** with **netlify**.
Hexo is not the only **SSG** (Static Site Generator).

#### Why Static Site Generator?

No need to go deep learning backend.
So you can mock-up your theme quickly.

![What is SSG Good for?][what-ssg-good-for]

#### Why Markdown?

Most SSG is using markdown, instead of HTML.
Markdown reduce the complexity of formatting.
Reduce time wasted on formatting.
Hence concentrate more on content.

#### Why Hexo?

> I do not know.

My first decision to use Hexo is simple,
some of my friends also use Hexo.
Maybe I am just bored and in need to moved on.
At least I have people to talk with.

> I know, I'm cheap.

Hexo doesn't have rich feature as Hugo does.
But it is sufficient for a blog, or personal sites.
In some cases, Hexo has feature that does not available in Hugo.

#### EJS Templating Engine

> EJS gives Hexo strong point

Hexo support more than one templating engine.
You can read about this templating engine,
in Hexo official documentation.

I decide to use EJS for my blog.

#### Why EJS?

> Javascript is very common

Yes, it use plain javascript.
Thus, it is easier to use than Liquid in Jekyll, or Chroma in Hugo.
This EJS support make Hexo **sexier** than Hugo or Jekyll.

#### Source Code

Source code used in this tutorial, is available at:

*	[gitlab.com/epsi-rns/tutor-hexo-bulma][tutor-hexo-master]

Source code for Bulma SASS, is also available at:

*	[gitlab.com/epsi-rns/tutor-html-bulma][tutor-html-master]

#### Demo Hexo

Unfortunately, I do not create Demo site with Bulma.
All I have is this screenshot.

![Hexo: Demo][image-ss-00-demo-hexo]

-- -- --

### The Article Series

#### Making a Theme

Supposed you want to make your own theme.
What would you do first?

* Step One: **Start From the Layout**:
  Although you can create the layout with pure HTML.
  Static site generator can make this job easier.
  This require Sass, for nice looks.

* Step Two: **Custom Pages**:
  From post and page, to archive and taxonomy.
  This require a lot of loop, that we can achieve using EJS.
  And Sass for finishing touch.

* Step Three: **Content**:
  You need to populate with content example.
  Adding layout such as page header, footer, and navigation.
  Also adding feature, such as meta SEO, and commenting system.
  Then a lot of Sass to handle rendered markdown content in post.
  For coder, you need to test syntax highlighting.

* Additional Step: Such as making pagination.

These blog cover most of those four main topics above.
There are additional stuff as well beyond making theme,
such as deployment.

#### Disclaimer

This is would not be the best blog template that you ever have.
Because I only put most common stuff,
to keep the tutorial simple.

After you learn this guidance,
you understand the fundamental skill.
Thus, a base for you, to make your own blog site.
With your imagination, you may continue,
to build your own super duper Hexo site.
Far better than, what I have achieved.

#### Content

A site need content.
I do not want __lorem ipsum__.

> None of Your Buciness

To make this step by step tutorial alive,
I choose most common topic, a fictional love story.

I put some lyrics from great beautiful songs.
I'm not sure exactly what these songs means.
I just find it easier to use tis kind of content.

> Sebuah catatan blog, masih dengan tema-tema perbucinan.

Without the content, you can still make your site anyway.
You do not really need to understand the story behind the content.

#### Table of Content

The table content is available as header on each tutorial.
There, I present a Hexo Tutorial, step by step, for beginners.

-- -- --

### Links

#### Demo Site

* <https://akutidaktahu.netlify.com/>

#### Presentation

* [Hexo - Presentation Slide][hexo-presentation]

#### Hexo Article Series

Consist of more than 30 articles.

* [Hexo - Overview][hexo-overview]

* [Hexo - Summary][hexo-summary]

* [Hexo Step by Step Repository][tutorial-hexo] (this repository)

#### Bulma Article Series

Consist of 10 articles.

* [Bulma - Overview][bulma-overview]

* [Bulma Step by Step Repository][tutorial-bulma]

[hexo-presentation]: https://epsi-rns.gitlab.io/ssg/2019/05/30/hexo-presentation/
[hexo-overview]:     https://epsi-rns.gitlab.io/ssg/2019/05/01/hexo-overview/
[hexo-summary]:      https://epsi-rns.gitlab.io/ssg/2019/05/30/hexo-summary/
[bulma-overview]:    https://epsi-rns.gitlab.io/frontend/2019/03/01/bulma-overview/

[tutorial-hexo]:     https://gitlab.com/epsi-rns/tutor-hexo-bulma/
[tutorial-bulma]:    https://gitlab.com/epsi-rns/tutor-html-bulma/

-- -- --

### Chapter Step by Step

#### Tutor 01

> Generate Only Pure HTML

* Installing Hexo

* Setup Directory for Minimal Hexo

* General Layout: Base, Page, Post, Index

* Basic Content

![Hexo Bulma: Tutor 01][tutor-01]

#### Tutor 02

> Adding stylesheet for content

* Add Bulma CSS

* Standard Header and Footer

* Enhance All Layouts with Bulma CSS

![Hexo Bulma: Tutor 02][tutor-02]

#### Tutor 03

* Add Custom SASS (Custom Design)

* Nice Header and Footer

* Custom Pages: Categories, Tags, Home (landing page)

* Apply Two Column Responsive Layout for Most Layout

* Additional Layout: Category, Tag, Archive

* Template Refactor: Reusable Post List

* EJS: Header Title

* Languages

![Hexo Bulma: Tutor 03][tutor-03]

#### Tutor 04

* More Content: Lyrics and Quotes. Need this content for demo

* Template: Blog Item

* Template: Post List: Simple, By Year, List Tree (By Year and Month)

* Tags and Categories Page: Nice Tag Badge and List Tree

* Multi Column Responsive List: Tags, Categories, and Archive

* Widget: Friends, Archives Tree, Categories, Tags, Recent Post, Related Post

* Custom Output: JSON

![Hexo Bulma: Tutor 04][tutor-04]

#### Tutor 05

> Optional Feature

* Blog Pagination: Adjacent, Indicator, Responsive.

* More Pagination: Tags, Categories, and Archive

![Hexo Bulma: Tutor 05: Indicator Pagination][tutor-05indi]

![Hexo Bulma: Tutor 05: Responsive Pagination][tutor-05resp]

> Finishing

* Post: Header, Footer, Navigation

* Post: Markdown Content

* Post: Table of Content

* Post: Responsive Images

* Syntax Highlight: CSS Fix

* Meta: HTML, SEO, Opengraph, Twitter

![Hexo Bulma: Tutor 06][tutor-06]

What do you think ?
  
[hexo-bulma-preview]:   https://gitlab.com/epsi-rns/tutor-hexo-bulma/raw/master/preview/hexo-bulma-preview.png
[tutor-01]:     https://gitlab.com/epsi-rns/tutor-hexo-bulma/raw/master/preview/13-browser-landing-page.png
[tutor-02]:     https://gitlab.com/epsi-rns/tutor-hexo-bulma/raw/master/preview/25-browser-post.png
[tutor-03]:     https://gitlab.com/epsi-rns/tutor-hexo-bulma/raw/master/preview/38-browser-cats-tree.png
[tutor-04]:     https://gitlab.com/epsi-rns/tutor-hexo-bulma/raw/master/preview/42-section-category.png
[tutor-05indi]: https://gitlab.com/epsi-rns/tutor-hexo-bulma/raw/master/preview/54-hexo-bulma-indicator-animate.gif
[tutor-05resp]: https://gitlab.com/epsi-rns/tutor-hexo-bulma/raw/master/preview/55-hexo-bulma-responsive-animate.gif
[tutor-06]:     https://gitlab.com/epsi-rns/tutor-hexo-bulma/raw/master/preview/61-blog-post-all.png

-- -- --

### Begin The Guidance

Let's get the tutorial started.
Long and short of it, this is the tale.

Consider continue reading [ [Hexo Install][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2019/05/02/hexo-install/

[local-bulma-overview]: {{< baseurl >}}frontend/2019/03/01/bulma-overview/

[tutor-hexo-master]:    {{< tutor-hexo-bulma >}}/
[tutor-html-master]:    {{< tutor-html-bulma >}}/

[image-ss-00-demo-hexo]:    {{< assets-ssg >}}/2019/05/00-hexo-bulma-preview.png
[what-ssg-good-for]:{{< assets-ssg >}}/2020/what-ssg-good-for.png
