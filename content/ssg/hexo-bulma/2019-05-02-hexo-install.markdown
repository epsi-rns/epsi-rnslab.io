---
type   : post
title  : "Hexo - Install"
date   : 2019-05-02T09:17:35+07:00
slug   : hexo-install
categories: [ssg]
tags      : [hexo]
keywords  : [static site, custom theme, install, npm, run server, verbose, port]
author : epsi
opengraph:
  image: assets/site/images/topics/hexo-bulma-markdown.png

toc    : "toc-2019-05-hexo-bulma-step"

excerpt:
  Building Hexo Site Step by step, with Bulma as stylesheet frontend.
  Show Minimal Homepage in Hexo.

---

### Preface

> Goal: Show Minimal Homepage in Hexo

Here, I present a Hexo Tutorial, step by step, for beginners.

-- -- --

### 1: Install Hexo

Hexo run under NodeJS.
It means you can have it in most OS, such as Windows and Linux.

{{< highlight bash >}}
$ npm install hexo-cli -g
{{< / highlight >}}

![Hexo: Install][image-ss-00-install]

Now you can:

{{< highlight bash >}}
$ hexo version
{{< / highlight >}}

![Hexo: Version][image-ss-00-version]

-- -- --

### 2: Create Project

Consider create new Hexo site, or extract the file above,
in your favorites directory

{{< highlight bash >}}
$ cd /media/Works/sites

$ hexo init tutor-hexo-bulma

$ cd tutor-hexo-bulma

$ npm install
{{< / highlight >}}

This will setup a few NPM.

![Hexo: Init][image-ss-00-init]

And the result is these directories:

{{< highlight bash >}}
$ tree -d -I node_modules -L 3
.
├── scaffolds
├── source
│   └── _posts
└── themes
    └── landscape
        ├── languages
        ├── layout
        ├── scripts
        └── source

9 directories
{{< / highlight >}}

![Hexo: Tree][image-ss-00-tree]

Notice the default landscape theme.

_._

-- -- --

### 3: Running for the First Time

#### Run Server

{{< highlight bash >}}
$ hexo server

INFO  Start processing
INFO  Hexo is running at http://localhost:4000 . Press Ctrl+C to stop.
{{< / highlight >}}

![Hexo: Server][image-ss-00-server]

#### Landscape Theme

This will show you a ready to use blog,
with landscape theme as default theme.

![Hexo: Landsacpe Theme][image-ss-00-default]

I suggest you play with content in Hexo directory,
before we continue to the next article.

#### Verbose

If you want you can use more verbose output with `debug` option.

{{< highlight bash >}}
$ hexo server --debug
{{< / highlight >}}

![Hexo: Verbose Server with Debug Option][image-ss-00-debug]

#### Multiple Hexo

It is common to work with multiple Hexo at one time,
for one reason or another.
You can achieve it with using other port.

{{< highlight bash >}}
$ hexo server -p 5000
{{< / highlight >}}

Now you are ready to explore inside Hexo.

-- -- --

### What is Next ?

Consider continue reading [ [Hexo Minimal][local-whats-next] ].
We are going to make theme from scratch,
with pure HTML without any CSS.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2019/05/03/hexo-minimal/

[tutor-html-master]:    {{< tutor-html-bulma >}}/

[image-ss-00-install]:  {{< assets-ssg >}}/2019/05/00-npm-install-hexo-cli.png
[image-ss-00-version]:  {{< assets-ssg >}}/2019/05/00-hexo-version.png
[image-ss-00-init]:     {{< assets-ssg >}}/2019/05/00-hexo-init.png
[image-ss-00-tree]:     {{< assets-ssg >}}/2019/05/00-hexo-tree.png
[image-ss-00-server]:   {{< assets-ssg >}}/2019/05/00-hexo-server.png
[image-ss-00-default]:  {{< assets-ssg >}}/2019/05/00-hexo-landscape.png
[image-ss-00-debug]:    {{< assets-ssg >}}/2019/05/00-hexo-debug.png

