---
type   : post
title  : "Hexo - Minimal"
date   : 2019-05-03T09:17:35+07:00
slug   : hexo-minimal
categories: [ssg]
tags      : [hexo]
keywords  : [static site, custom theme, ejs, layout, configuration, markdown]
author : epsi
opengraph:
  image: assets/site/images/topics/hexo-bulma-markdown.png

toc    : "toc-2019-05-hexo-bulma-step"

excerpt:
  Building Hexo Site Step by step, with Bulma as stylesheet frontend.
  Show Minimal Homepage in Hexo.

---

### Preface

> Goal: Show Minimal Homepage in Hexo

This article use <code>tutor-01</code> theme.
We will create it step by step.

Here, I present a Hexo Tutorial, step by step, for beginners.

#### Artefacts

Our minimal homepage require at least four artefacts.

* <code>_config.yml</code>

* <code>source/index.html</code>

* <code>themes/tutor-01/layout/layout.ejs</code>

* <code>themes/tutor-01/layout/index.ejs</code>

#### Source Code

You can download the source code of this article here.

* [hexo-ejs-bulma-tutor-01.zip][image-ss-01-source]

Extract and run on CLI:

{{< highlight html >}}
$ npm install
{{< / highlight >}}

-- -- --

### 0: Theme Making

Consider to have a look at the big picture below:

![SSG Illustration: Theme Making][theme-making]

-- -- --

### 1: Minimal Configuration

First thing first, 
We need a workable configuration.

The original <code>_config.yml</code> provided is long,
and we need a shorter one so we can learn sep by step.
Consider have a look at this minimalist config:

* <code>_config.yml</code>
  : [gitlab.com/.../_config.yml][tutor-hexo-config]

{{< highlight yaml >}}
# Hexo Configuration

# Site
title: Heartbeats for Bulma
subtitle: Who am I to keep you wait?
description: Too Long. Too Late. So Far Away. So Far Too Long.
keywords: affection, crush, dream, heartbeat, pulse
author: Mataharani
language: en
timezone: Asia/Jakarta

# Extensions
theme: tutor-01
{{< / highlight >}}

![Hexo: ViM Minimalist Configuration][image-ss-01-minimal-config]

We will also make this minimalist <code>tutor-01</code> theme,
right away.

-- -- --

### 2: Minimal Theme

In my early attempt with static site,
it is always trying to understand how the theme works.
The easiest thing to do this is to make a minimal theme,
and modified later.

#### Layout: Tree

Our minimal theme only need two artefacts.

* <code>themes/tutor-01/layout/layout.ejs</code>

* <code>themes/tutor-01/layout/index.ejs</code>

{{< highlight bash >}}
tree themes -I node_modules -L 3
themes
└── tutor-01
    └── layout
        ├── index.ejs
        └── layout.ejs

2 directories, 2 files
{{< / highlight >}}

![Hexo: Minimal Theme][image-ss-01-minimal-theme]

#### Layout: EJS Layout

This layout contains the HTML skeleton of the **whole** web page.

* <code>themes/tutor-01/layout/layout.ejs</code>
  : [gitlab.com/.../layout/layout.ejs][tutor-layout-layout]

{{< highlight html >}}
<html>
<head>
  <meta charset="utf-8">
  <title><%= page.title || config.title%></title>
</head>
<body>
  <p><strong>This is header.</strong></p>
  <div id="content">
    <%- body %>
  </div>
  <p><strong>This is footer.</strong></p>
</body>
</html>
{{< / highlight >}}

All kind of page in Hexo, is based on this skeleton.

Notice that we have our very first EJS template:

{{< highlight javascript >}}
<%= page.title || config.title%></title>
{{< / highlight >}}

and

{{< highlight javascript >}}
<%- body %>
{{< / highlight >}}

#### Layout: EJS Index

This layout contains the content of landing page.
This is usually the index of the blog.

* <code>themes/tutor-01/layout/index.ejs</code>
  : [gitlab.com/.../layout/index.ejs][tutor-layout-index]

{{< highlight html >}}
<h1><%= config.author %></h1>
<p><%= config.subtitle %></p>

<p><strong>This is index.</strong></p>
{{< / highlight >}}

We will alter later, on <code>tutor-04</code>.
Now we need to live with basic template.

-- -- --

### 3: Minimal Homepage

We are finished with minimal homepage.

#### CLI: Run Server

{{< highlight bash >}}
$ hexo server -p 4000 --debug
14:33:03.033 DEBUG Writing database to /home/epsi/tutor-01/db.json
14:33:03.152 DEBUG Hexo version: 3.8.0
14:33:03.153 DEBUG Working directory: ~/tutor-01/
14:33:03.818 DEBUG Config loaded: ~/tutor-01/_config.yml
14:33:03.923 DEBUG Plugin loaded: hexo-generator-archive
14:33:03.927 DEBUG Plugin loaded: hexo-generator-category
14:33:03.934 DEBUG Plugin loaded: hexo-generator-index
14:33:03.937 DEBUG Plugin loaded: hexo-generator-tag
14:33:03.953 DEBUG Plugin loaded: hexo-renderer-ejs
14:33:03.956 DEBUG Plugin loaded: hexo-renderer-stylus
14:33:03.984 DEBUG Plugin loaded: hexo-renderer-marked
14:33:04.242 DEBUG Plugin loaded: hexo-server
14:33:04.280 INFO  Start processing
14:33:04.418 DEBUG Processed: index.html
14:33:04.530 DEBUG Processed: layout/index.ejs
14:33:04.537 DEBUG Processed: layout/layout.ejs
14:33:05.053 DEBUG Generator: page
14:33:05.055 DEBUG Generator: post
14:33:05.058 DEBUG Generator: archive
14:33:05.058 DEBUG Generator: category
14:33:05.059 DEBUG Generator: index
14:33:05.059 DEBUG Generator: tag
14:33:05.060 DEBUG Generator: asset
14:33:05.073 INFO  Hexo is running at http://localhost:4000 . Press Ctrl+C to stop.
{{< / highlight >}}

![Hexo: Server][image-ss-01-server]

#### Render: Browser

Now check the generated page on your favorite browser:

![Hexo: Minimal Homepage on Browser][image-ss-01-minimal-homepage]

#### Render: Source

Consider have a look at HTML source for a while.

{{< highlight html >}}
$ curl localhost:4000
<html>
<head><meta name="generator" content="Hexo 3.8.0">
  <meta charset="utf-8">
  <title>Welcome to Heartbeat</title>
</head>
<body>
  <p><strong>This is header.</strong></p>
  <div id="content">
    <h1>Mataharani</h1>
<p>Who am I to keep you wait?</p>

<p><strong>This is index.</strong></p>

  </div>
  <p><strong>This is footer.</strong></p>
</body>
</html>
{{< / highlight >}}

You can see how clean the output is.

![Hugo: Minimal Homepage on curl][image-ss-01-minimal-curl]

-- -- --

### What is Next ?

Consider continue reading [ [Hexo Layout][local-whats-next] ].
We are going to use theme, with layouts inside each theme.
Out first theme is still pure HTML with no CSS.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2019/05/04/hexo-layout/

[image-ss-01-source]:       {{< assets-ssg >}}/2019/05/hexo-ejs-bulma-tutor-01.zip
[theme-making]:         {{< assets-ssg >}}/2020/theme-making.png

[local-windows-choco]:  {{< baseurl >}}ssg/2019/01/21/windows-choco-hexo/
[local-hexo-minimal]:   {{< baseurl >}}ssg/2018/09/02/hexo-minimal/

[tutor-html-master]:    {{< tutor-html-bulma >}}/

[image-ss-01-minimal-theme]:    {{< assets-ssg >}}/2019/05/11-tree-minimal-theme.png
[image-ss-01-minimal-config]:   {{< assets-ssg >}}/2019/05/11-vim-minimal-config.png
[image-ss-01-minimal-homepage]: {{< assets-ssg >}}/2019/05/11-browser-minimal-homepage.png
[image-ss-01-minimal-curl]:     {{< assets-ssg >}}/2019/05/11-minimal-curl.png

[image-ss-01-server]:   {{< assets-ssg >}}/2019/05/11-hexo-server.png

[tutor-hexo-config]:    {{< tutor-hexo-bulma >}}/_config.yml
[tutor-layout-layout]:  {{< tutor-hexo-bulma >}}/themes/tutor-01/layout/layout.ejs
[tutor-layout-index]:   {{< tutor-hexo-bulma >}}/themes/tutor-01/layout/index.ejs
