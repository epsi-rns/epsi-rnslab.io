---
type   : post
title  : "Hexo - Layout"
date   : 2019-05-04T09:17:35+07:00
slug   : hexo-layout
categories: [ssg]
tags      : [hexo]
keywords  : [static site, minimal theme, ejs, layout, page, refactoring template, layout, landing page]
author : epsi
opengraph:
  image: assets/site/images/topics/hexo-bulma-markdown.png

toc    : "toc-2019-05-hexo-bulma-step"

excerpt:
  Building Hexo Site Step by step, with Bulma as stylesheet frontend.
  Show Minimalist Layout in Hexo.

---

### Preface

This article is intended for beginner.

> Goal: Show Minimalist Layout in Hexo


#### Source Code

You can download the source code of this article here.

* [hexo-ejs-bulma-tutor-01.zip][image-ss-01-source]

Extract and run on CLI:

{{< highlight html >}}
$ npm install
{{< / highlight >}}

-- -- --

### 4: Refactoring Layout

While doing development, code tends to become longer and longer.
It means hard to maintain. This is why we need refactoring.
To make the code modular, shorter, and easy to be read.

#### Layout: EJS Layout

Instead of using our previous code, we need to refactor it first.

* <code>themes/tutor-01/layout/layout.ejs</code>
  : [gitlab.com/.../layout/layout.ejs][tutor-layout-layout]

{{< highlight html >}}
<html>
<%- partial('site/head') %>
<body>
  <%- partial('site/header') %>
  <div id="content">
    <%- body %>
  </div>
  <%- partial('site/footer') %>
</body>
</html>
{{< / highlight >}}

![Hexo: ViM Layout][image-ss-01-vim-layout]

Notice that now we require these three partial artefacts:

* <code>themes/tutor-01/layout/site/head.ejs</code>

* <code>themes/tutor-01/layout/site/header.ejs</code>

* <code>themes/tutor-01/layout/site/footer.ejs</code>

#### Layout: EJS Head

* <code>themes/tutor-01/layout/site/head.ejs</code>
  : [gitlab.com/.../layout/site/head.ejs][tutor-l-site-head]

{{< highlight html >}}
<head>
  <meta charset="utf-8">
  <title><%= page.title || config.title%></title>
</head>
{{< / highlight >}}

#### Layout: EJS Header

* <code>themes/tutor-01/layout/site/header.ejs</code>
  : [gitlab.com/.../layout/site/header.ejs][tutor-l-site-header]

{{< highlight html >}}
<p><strong>My beating heart.</strong></p>
{{< / highlight >}}

#### Layout: EJS Footer

* <code>themes/tutor-01/layout/site/footer.ejs</code>
  : [gitlab.com/.../layout/site/footer.ejs][tutor-l-site-footer]

{{< highlight html >}}
<p><strong>Mind, body, and soul.</strong></p>
{{< / highlight >}}

#### Render: Browser

Consider revisit our last homepage:

![Hexo: Refactored Homepage on Browser][image-ss-01-b-refactored]

Now, whenever there are any changes,
we just need to change the partial EJS template.

-- -- --

### 5: Adding Page

Of course, beside the landing page, we need other page as well.
For this to be happend, we require other artefact.

#### Layout: EJS Page

Consider keep this as simple as possible.

* <code>themes/tutor-01/layout/page.ejs</code>
  : [gitlab.com/.../layout/page.ejs][tutor-layout-page]

{{< highlight html >}}
<h1><%- page.title %></h1>

<%- page.content %>
{{< / highlight >}}

We are going to make it complex later.

#### Content: HTML

Conisder make content other than <code>index.html</code>.
Just like any other static site,
we can setup **frontmatter** before the HTML content.

* <code>source/hello-there.html</code>
  : [gitlab.com/.../source/hello-there.html][tutor-content-hello]

{{< highlight html >}}
---
title: Hello There
# date: only required in landscape theme
---

  <p>Too Long. Too Late.
     So Far Away. So Far Too Long.</p>

  <p>Who am I to keep you wait?</p>
{{< / highlight >}}

Be aware that every theme might have different behaviour.
For example, my theme does not require <code>date</code> field
in frontmatter, but other template might need the field.

#### Render: Browser

Consider visit different page:

* <localhost:4000/hello-there.html>

![Hexo: HTML Page Content on Browser][image-ss-01-b-html-page]

#### Content: Markdown

Just like any other static generator, Hexo also support Markdown.
With markdown you can focus more on content (and document structure).
In contrast, HTML has focus on formatting (and wasting more time).

Consider make content in markdown.

* <code>source/about/longing.md</code>
  : [gitlab.com/.../source/about/longing.md][tutor-content-about]

{{< highlight markdown >}}
---
title: Longing About Bulma
---

It's just pathetic to give up on something
before you even give it a shot.

> I guess that's why you never needed a haircut!!
{{< / highlight >}}

Those Bulma's haircut quote above are from dragonball comic.

#### Render: Browser

Consider visit rendered markdown page in html:

* <localhost:4000/about/longing.html>

![Hexo: Markdown Page Content on Browser][image-ss-01-b-md-page]

-- -- --

### 6: First Post

Hexo has special `page` called `post`.
Just like any other static site generator,
this is useful for blog like page post.

In order to hae this page kind,
we need to create a <code>post.ejs</code> layout.

#### Layout: EJS Post

Consider keep this as simple as possible.
But equipped with post header and post footer,
to be altered later.

* <code>themes/tutor-01/layout/post.ejs</code>
  : [gitlab.com/.../layout/post.ejs][tutor-layout-post]

{{< highlight html >}}
<h1><%- page.title %></h1>
<strong><%= date(page.date, config.date_format) %></strong>

<p><em>Dear Angel,</em></p>

<%- page.content %>

<p><em>Heaven holds a place for you.</em></p>
{{< / highlight >}}

We are also going to make it complex later, as usual

#### Content: Post

As usual, we require example content.

* <code>source/_posts/white-winter.md</code>
  : [gitlab.com/.../source/_posts/white-winter.md][tutor-post-winter]

{{< highlight markdown >}}
---
title     : Surviving White Winter
date      : 2016/11/13 11:02:00
---

It was a frozen winter in cold war era.
<!-- more --> 
We were two lazy men, a sleepy boy, two long haired women,
a husky with attitude, and two shotguns.
After three weeks, we finally figure out, about **css framework**.

But we lost our beloved husky before we finally made it.
Now, every january, we remember our husky,
that helped all of us to survive.
{{< / highlight >}}

I made the story myself.

#### Configuration

We need to setup permalink and stuff.
Add these lines below our configuration.

* <code>_config.yml</code>
  : [gitlab.com/.../_config.yml][tutor-hexo-config]

{{< highlight yaml >}}
# URL
url: http://example/
root: /
permalink: :year/:month/:day/:title/

# Directory
source_dir: source
public_dir: public

# Writing
default_layout: post
{{< / highlight >}}

Restart the server, to apply the new configuration.

#### Render: Browser

Consider visit the post with url as set in permalink:

* <http://localhost:4000/2016/11/13/white-winter/>

![Hexo: Markdown Post Content on Browser][image-ss-01-b-md-post]

#### Frontmatter: Layout

For exercise, you can use page layout in frontmatter.

{{< highlight yaml >}}
---
layout    : page
title     : Surviving White Winter
date      : 2016/11/13 11:02:00
---
{{< / highlight >}}

And revisit the page in browser to see the change.

-- -- --

### 7: Index List

Remember our index page,
now that we already have <code>post.ejs</code>,
we can utilize it to make list (article index) in main page.

#### Layout: EJS Index

This is not simple anymore.

* <code>themes/tutor-01/layout/index.ejs</code>
  : [gitlab.com/.../layout/index.ejs][tutor-layout-index]

{{< highlight html >}}
<h1><%= config.author %></h1>
<p><%= config.subtitle %></p>

<blockquote>I pour the <b>journal</b> as daily letter.</blockquote>

<ul>
  <% page.posts.each(function(post){ %>
    <li>
      <b><a href="<%- url_for(post.path) %>">
        <%= post.title %>
      </a></b>
      <br/>
      
      <p><%= date(post.date, config.date_format) %></p>
      <br/>
      
      <% if(post.excerpt) { %>
      <p><%- post.excerpt %></p>
      <% } %>
    </li>
  <% }) %>
</ul>
{{< / highlight >}}

#### Render: Browser

Consider visit the post with url as set in permalink:

* <http://localhost:4000/>

![Hexo: List of Posts on Browser][image-ss-01-b-list-posts]

#### How does it works?

The loop here:

* <code>themes/tutor-01/layout/index.ejs</code>

{{< highlight html >}}
<ul>
  <% page.posts.each(function(post){ %>
    ...
  <% }) %>
</ul>
{{< / highlight >}}

-- -- --

### 8: Landing Page

What if we don't want list of articles in main page.
I mean by having a landing page, and put the list somewhere else.

#### Content: Index

We need a landing page content.

* <code>source/index.html</code>
  : [gitlab.com/.../source/index.html][tutor-content-index]

{{< highlight html >}}
---
title: Welcome to Heartbeat
---

  <p>Every journey begins with a single step.</p>
{{< / highlight >}}

#### Configuration

We need to setup permalink and stuff.
Add these lines below our configuration.

* <code>_config.yml</code>
  : [gitlab.com/.../_config.yml][tutor-hexo-config]

{{< highlight yaml >}}
# Home page setting
index_generator:
  path: '/blog'
  order_by: -date
{{< / highlight >}}

#### Render: Browser

Consider revisit the main page:

* <http://localhost:4000/>

![Hexo: Landing Page on Browser][image-ss-01-b-landing]

And your index page would be available in

* <http://localhost:4000/blog/>

-- -- --

### Summary

This is how our theme would looks like:

{{< highlight bash >}}
$ hexo new site .tree themes -I node_modules -L 3
themes
└── tutor-01
    ├── layout
    │   ├── index.ejs
    │   ├── layout.ejs
    │   ├── page.ejs
    │   ├── post.ejs
    │   └── site
    └── source
        ├── css
        └── js

6 directories, 4 files
{{< / highlight >}}

As an additional prepration,
I add source <code>source</code> directory,
that contain <code>css</code> and <code>js</code>,
for use with next guidance.

![Hexo: Pure HTML Theme][image-ss-01-pure-html-theme]

-- -- --

### What is Next ?

We are going to use CSS in theme, with Bulma official CSS.
Consider continue reading [ [Hexo - Bulma - CSS Introduction][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2019/05/05/hexo-bulma-css-intro/

[image-ss-01-source]:       {{< assets-ssg >}}/2019/05/hexo-ejs-bulma-tutor-01.zip

[image-ss-01-pure-html-theme]:  {{< assets-ssg >}}/2019/05/13-tree-pure-html-theme.png
[image-ss-01-b-refactored]: {{< assets-ssg >}}/2019/05/13-browser-refactored.png
[image-ss-01-b-html-page]:  {{< assets-ssg >}}/2019/05/13-browser-content-page-html.png
[image-ss-01-b-md-page]:    {{< assets-ssg >}}/2019/05/13-browser-content-page-md.png
[image-ss-01-b-md-post]:    {{< assets-ssg >}}/2019/05/13-browser-content-post-md.png
[image-ss-01-b-list-posts]: {{< assets-ssg >}}/2019/05/13-browser-list-of-posts.png
[image-ss-01-b-landing]:    {{< assets-ssg >}}/2019/05/13-browser-landing-page.png
[image-ss-01-vim-layout]:   {{< assets-ssg >}}/2019/05/13-vim-layout-ejs.png

[tutor-hexo-config]:    {{< tutor-hexo-bulma >}}/_config.yml

[tutor-content-index]:  {{< tutor-hexo-bulma >}}/source/index.html
[tutor-content-hello]:  {{< tutor-hexo-bulma >}}/source/hello-there.html
[tutor-content-about]:  {{< tutor-hexo-bulma >}}/source/about/longing.md
[tutor-post-winter]:    {{< tutor-hexo-bulma >}}/source/_posts/white-winter.md

[tutor-layout-layout]:  {{< tutor-hexo-bulma >}}/themes/tutor-01/layout/layout.ejs
[tutor-layout-page]:    {{< tutor-hexo-bulma >}}/themes/tutor-01/layout/page.ejs
[tutor-layout-post]:    {{< tutor-hexo-bulma >}}/themes/tutor-01/layout/post.ejs
[tutor-layout-index]:   {{< tutor-hexo-bulma >}}/themes/tutor-01/layout/index.ejs

[tutor-l-site-head]:    {{< tutor-hexo-bulma >}}/themes/tutor-01/layout/site/head.ejs
[tutor-l-site-header]:  {{< tutor-hexo-bulma >}}/themes/tutor-01/layout/site/header.ejs
[tutor-l-site-footer]:  {{< tutor-hexo-bulma >}}/themes/tutor-01/layout/site/footer.ejs
