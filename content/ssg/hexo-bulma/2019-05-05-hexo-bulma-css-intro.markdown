---
type   : post
title  : "Hexo - Bulma - CSS Introduction"
date   : 2019-05-05T09:17:35+07:00
slug   : hexo-bulma-css-intro
categories: [ssg]
tags      : [hexo, bulma, css]
keywords  : [static site, custom theme, ejs, layout, refactoring template]
author : epsi
opengraph:
  image: assets/site/images/topics/hexo-bulma-markdown.png

toc    : "toc-2019-05-hexo-bulma-step"

excerpt:
  Building Hexo Site Step by step, with Bulma as stylesheet frontend.
  Explain How to Use Bulma Stylesheet in Hexo.

---

### Preface

This article is part one of Hexo Bulma CSS.

> Goal: Explain How to Use Bulma Stylesheet in Hexo

#### Hexo meet Bulma

	Hexo... This is Bulma.
	Bulma... This is Hexo.
	Now Bulma have options to choose.
	Hexo, Hugo, Jekyll, Zola, or Eleventy. 

#### Source Code

You can download the source code of this article here.

* [hexo-ejs-bulma-tutor-02.zip][image-ss-02-source]

Extract and run on CLI:

{{< highlight html >}}
$ npm install
{{< / highlight >}}

#### Related Article

I wrote about Bulma Navigation Bar that used in this article.
Step by step article, about building Navigation Bar.
You can read here:

* [Bulma Navigation Bar][local-bulma-navbar]

-- -- --

### 1: Preparation

Consider make an ecosystem for bulma first.

#### Create Theme

To avoid messed-up with previous tutorial,
consider make a new theme.

* <code>$ cat _config.yml</code>
  : [gitlab.com/.../_config.yml][tutor-hexo-config]

{{< highlight yaml >}}
# Extensions
theme: tutor-02
{{< / highlight >}}

{{< highlight bash >}}
$ cd themes/tutor-02 
{{< / highlight >}}

#### Copy CSS and JS

Copy `bulma.css` to <code>themes/tutor-02/source/css</code>.

{{< highlight bash >}}
$ tree source/css
source/css
└── bulma.css
{{< / highlight >}}

#### Copy Example Assets

Or even better, use our previous tutorial,
And copy example assets to <code>themes/tutor-02/static</code>.

{{< highlight bash >}}
$ tree themes/tutor-02/source
themes/tutor-02/source
├── css
│   ├── bulma.css
│   ├── bulma.css.map
│   ├── bulma.min.css
│   └── main.css
├── favicon.ico
├── images
│   └── logo-gear.png
└── js
    ├── bulma-burger-jquery.js
    ├── bulma-burger-vue.js
    ├── jquery-slim.min.js
    └── vue.min.js

3 directories, 10 files
{{< / highlight >}}

![Hexo Bulma: Tree][image-ss-02-tree-bulma]

Note that, you might still need map files,
while debugging using object inspector.

**Source**:
	[gitlab.com/.../themes/tutor-02/source][tutor-hexo-source]

#### Theme Configuration

Theme can also have its own configuration as shown below:

* <code>themes/tutor-02/_config.yml</code>
  : [gitlab.com/.../themes/tutor-02/_config.yml][tutor-theme-config]

{{< highlight yaml >}}
# Miscellaneous
favicon: /favicon.ico
{{< / highlight >}}

We can call the config setting later using

{{< highlight html >}}
<%- favicon_tag(theme.favicon) %>
{{< / highlight >}}

Or better favicon tag, using <code>image/x-icon</code>.

{{< highlight html >}}
<link href="<%- theme.favicon %>" rel="shortcut icon" type="image/x-icon" />
{{< / highlight >}}

-- -- --

### 2: Add Bulma

Adding bulma is straightforward.
Just add the stylesheet to <code>site/head.html</code>.

However, there are these artefacts,
to make sure, we have the same copy.

#### Layout: EJS HTML Head

* <code>themes/tutor-02/layout/site/head.ejs</code>
  : [gitlab.com/.../layout/site/head.ejs][tutor-l-site-head]

{{< highlight html >}}
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title><%= page.title || config.title%></title>

  <% if (theme.favicon){ %>
     <%- favicon_tag(theme.favicon) %>
     <link href="<%- theme.favicon %>" rel="shortcut icon" type="image/x-icon" />
  <% } %>

  <%- css('/css/bulma.css') %>
</head>
{{< / highlight >}}

#### Layout: EJS Layout

I'm trying to have this file, as simple as possible.

* <code>layouts/layout.ejs</code>

{{< highlight html >}}
<!DOCTYPE html>
<html lang="en">
<%- partial('site/head') %>
<body>
  <%- body %>
</body>
</html>
{{< / highlight >}}

#### Layout: EJS Page

A slight change, similar to previous tutorial.

* <code>themes/tutor-02/layout/page.ejs</code>
  : [gitlab.com/.../layout/page.ejs][tutor-layout-page]

{{< highlight javascript >}}
<%- page.content %>
{{< / highlight >}}

#### Render: Browser: Landing Page

Open in your favorite browser.
You should see, white background, by now.

* <http://localhost:1313/>

![Hexo Bulma: White Minimal][image-ss-02-browser-minimal]

-- -- --

### 3: Simple Layout

I put colors so you can get the idea,
about this responsive layout clearly.
I also put grid, just as an example.

> Consider refactor by layout

#### Layout: EJS Layout

Consider using semantic HTML5 tag.

* <code>themes/tutor-02/layout/layout.ejs</code>
  : [gitlab.com/.../layout/layout.ejs][tutor-layout-layout]

{{< highlight html >}}
<!DOCTYPE html>
<html lang="en">
<%- partial('site/head') %>
<body>
  <!-- header -->
  <nav class="navbar is-fixed-top is-dark">
    <div class="navbar-brand">
      <a class="navbar-item">
        Home
      </a>
    </div>
  </nav>

  <!-- main -->
  <div class="columns layout-base">
    <main role="main" 
          class="column is-two-thirds has-background-primary">
      <article class="blog-post">
        <h4 class="title is-4"><%= page.title || config.title%></h4>
        <%- body %>
      </article>
    </main>

    <aside class="column is-one-thirds has-background-info">
      <div class="blog-sidebar">
        Side menu
      </div>
    </aside>
  </div>

  <!-- footer -->
  <footer class="site-footer">
    <div class="navbar is-fixed-bottom maxwidth
          is-dark has-text-centered is-vcentered">
      <div class="column">
        &copy; 2019.
      </div>
    </div>
  </footer>

</body>
</html>
{{< / highlight >}}

#### Partial: HTML Head

Add necessary stylesheet. the `main.css` right below `bulma.css`.
You should find the stylesheet on repository.

* <code>themes/tutor-02/layout/site/head.html</code>
  : [gitlab.com/.../layout/site/head.html][tutor-l-site-head]

{{< highlight javascript >}}
<head>
  ...
  
  <%- css(['/css/bulma.css', '/css/main.css']) %>
</head>
{{< / highlight >}}

#### Assets: Custom Stylesheet

This is additional custom decoration stylesheet that required.
Something that does not covered with Bulma CSS Framework.

* <code>themes/tutor-02/css/main.css</code>
  : [gitlab.com/.../css/main.css][tutor-s-css-main]

{{< highlight css >}}
.layout-base {
  padding-top: 5rem;
}

footer.site-footer {
  padding-top: 5rem;
}

h1, h2, h3, h4, h5, h6 {
  font-family: "Playfair Display", Georgia, "Times New Roman", serif;
}

.blog-sidebar,
.blog-post {
  margin-left: 20px;
  margin-right: 20px;
}

.blog-post-meta {
  margin-bottom: 5px;
}

.blog-post p {
  margin-top: 0.5rem;
  margin-bottom: 0.5rem;
}
{{< / highlight >}}

#### Render: Browser

Open in your favorite browser.
You should see, layout in colour, by now.

* <http://localhost:1313/>

![Hexo Bulma: Coloured Layout][image-ss-02-browser-layout]

-- -- --

### 4: Simple Refactor

After minimalist bootstrap above,
the next step is to create the layout.
We do not like long code.
Thus, we are going to refactor.

* <code>layouts/layout.ejs</code>

Into:

* <code>layouts/layout.ejs</code>

* <code>layouts/page.ejs</code>

* <code>layout/site/header.ejs</code>

* <code>layout/site/footer.ejs</code>

#### Layout: EJS Layout

* <code>themes/tutor-02/layout/layout.ejs</code>
  : [gitlab.com/.../layout/layout.ejs][tutor-layout-layout]

{{< highlight html >}}
<!DOCTYPE html>
<html lang="en">
<%- partial('site/head') %>
<body>
<%- partial('site/header') %>

  <div class="columns layout-base">
    <%- body %>
  </div>

<%- partial('site/footer') %>
<%- partial('site/scripts') %>
</body>
</html>
{{< / highlight >}}

I put the header/footer, outside main block.
So we do not need to repeatly writing header/footer in every main block.

#### Layout: EJS Page

A slight change, similar to previous tutorial.

* <code>themes/tutor-02/layout/page.ejs</code>
  : [gitlab.com/.../layout/page.ejs][tutor-layout-page]

{{< highlight html >}}
  <main role="main" 
        class="column is-full has-background-primary">
    <article class="blog-post">
      <h1 class="title is-4"><%- page.title %></h1>
      <%- page.content %>
    </article>
  </main>
{{< / highlight >}}

#### Partial: Header

* <code>themes/tutor-02/layout/site/header.html</code>
  : [gitlab.com/.../layout/site/header.html][tutor-l-site-header]

{{< highlight html >}}
<nav class="navbar is-fixed-top is-dark">
  <div class="navbar-brand">
    <a class="navbar-item">
      Home
    </a>
  </div>
</nav>
{{< / highlight >}}

#### Partial: Footer

* <code>themes/tutor-02/layout/site/footer.html</code>
  : [gitlab.com/.../layout/site/footer.html][tutor-l-site-footer]

{{< highlight html >}}
<footer class="site-footer">
  <div class="navbar is-fixed-bottom maxwidth
        is-dark has-text-centered is-vcentered">
    <div class="column">
      &copy; <%= date(new Date(), 'YYYY') %>.
    </div>
  </div>
</footer>
{{< / highlight >}}

#### Partial: Javascript

It is just an empty template.

* <code>themes/tutor-02/layout/site/scripts.html</code>
  : [gitlab.com/.../layout/site/scripts.html][tutor-l-site-scripts]

{{< highlight html >}}
  <!-- JavaScript -->
  <!-- Placed at the end of the document so the pages load faster -->
{{< / highlight >}}

#### Render: Browser

Open in your favorite browser.
You should see, non-colored homepage, by now.

* <http://localhost:1313/>

![Hexo Bulma: Layout Homepage][image-ss-02-browser-homepage]

-- -- --

### What is Next ?

Consider continue reading next part of this article 
in [ [Hexo - Bulma - CSS Layout][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2019/05/06/hexo-bulma-css-layout/
[local-bulma-navbar]:       {{< baseurl >}}frontend/2019/03/03/bulma-navbar/

[image-ss-02-source]:       {{< assets-ssg >}}/2019/05/hexo-ejs-bulma-tutor-02.zip

[image-ss-02-tree-bulma]:   {{< assets-ssg >}}/2019/05/21-tree-bulma-asset.png
[image-ss-02-browser-minimal]:  {{< assets-ssg >}}/2019/05/21-browser-minimal.png
[image-ss-02-browser-layout]:   {{< assets-ssg >}}/2019/05/22-browser-layout.png
[image-ss-02-browser-homepage]: {{< assets-ssg >}}/2019/05/22-browser-homepage.png

[tutor-hexo-config]:    {{< tutor-hexo-bulma >}}/_config.yml
[tutor-theme-config]:   {{< tutor-hexo-bulma >}}/themes/tutor-02/_config.yml

[tutor-l-site-head]:    {{< tutor-hexo-bulma >}}/themes/tutor-02/layout/site/head.ejs
[tutor-l-site-header]:  {{< tutor-hexo-bulma >}}/themes/tutor-02/layout/site/header.ejs
[tutor-l-site-footer]:  {{< tutor-hexo-bulma >}}/themes/tutor-02/layout/site/footer.ejs
[tutor-l-site-scripts]: {{< tutor-hexo-bulma >}}/themes/tutor-02/layout/site/scripts.ejs

[tutor-layout-layout]:  {{< tutor-hexo-bulma >}}/themes/tutor-02/layout/layout.ejs
[tutor-layout-page]:    {{< tutor-hexo-bulma >}}/themes/tutor-02/layout/page.ejs
[tutor-layout-post]:    {{< tutor-hexo-bulma >}}/themes/tutor-02/layout/post.ejs
[tutor-layout-index]:   {{< tutor-hexo-bulma >}}/themes/tutor-02/layout/index.ejs

[tutor-hexo-source]:    {{< tutor-hexo-bulma >}}/themes/tutor-02/source/

[tutor-s-css-main]:     {{< tutor-hexo-bulma >}}/themes/tutor-02/source/css/main.css

