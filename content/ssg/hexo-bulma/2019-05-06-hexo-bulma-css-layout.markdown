---
type   : post
title  : "Hexo - Bulma - CSS Layout"
date   : 2019-05-06T09:17:35+07:00
slug   : hexo-bulma-css-layout
categories: [ssg]
tags      : [hexo, bulma, css]
keywords  : [static site, custom theme, ejs, layout, navigation bar, page, post, index list]
author : epsi
opengraph:
  image: assets/site/images/topics/hexo-bulma-markdown.png

toc    : "toc-2019-05-hexo-bulma-step"

excerpt:
  Building Hexo Site Step by step, with Bulma as stylesheet frontend.
  Explain How to Use Bulma Stylesheet in Hexo.

---

### Preface

This article is part two of Hexo Bulma CSS.

> Goal: Explain How to Use Bulma Stylesheet in Hexo

#### Source Code

You can download the source code of this article here.

* [hexo-ejs-bulma-tutor-02.zip][image-ss-02-source]

Extract and run on CLI:

{{< highlight html >}}
$ npm install
{{< / highlight >}}

#### Related Article

I wrote about Bulma Navigation Bar that used in this article.
Step by step article, about building Navigation Bar.
You can read here:

* [Bulma Navigation Bar][local-bulma-navbar]

-- -- --

### 5: Modify Layout: Navigation Bar

How about Navigation Bar ?
The next step after create is to enhance the already refactored layout.
I'm going to use Vue version for use with burger button.

* <code>layout/partials/site/header.ejs</code>

* <code>layout/partials/site/scripts.ejs</code>

#### Layout: EJS Javascript

It is just an empty template.

* <code>themes/tutor-02/layout/site/scripts.ejs</code>
  : [gitlab.com/.../layout/site/scripts.ejs][tutor-l-site-scripts]

{{< highlight html >}}
  <!-- JavaScript
  ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <%- js(['/js/vue.min.js', '/js/bulma-burger-vue.js']) %>
{{< / highlight >}}

#### Layout: EJS Header

This is a looong Header, with logo image.

* <code>themes/tutor-02/layout/site/header.ejs</code>
  : [gitlab.com/.../layout/site/header.ejs][tutor-l-site-header]

{{< highlight html >}}
<nav role="navigation" aria-label="main navigation"
     class="navbar is-fixed-top is-dark"
     id="navbar-vue-app">
  <div class="navbar-brand">
    <a class="navbar-item" href="<%- url_for("/") %>">
       <%- image_tag("/images/logo-gear.png", {alt: "Home"}) %>
    </a>
    <a class="navbar-item">
      Blog
    </a>
    
    <a role="button" class="navbar-burger burger" 
       aria-label="menu" aria-expanded="false" data-target="navbarBulma"
       @click="isOpen = !isOpen" v-bind:class="{'is-active': isOpen}">
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
    </a>
  </div>

  <div id="navbarBulma" class="navbar-menu"
       v-bind:class="{'is-active': isOpen}">
    <div class="navbar-start">
      <div class="navbar-item has-dropdown is-hoverable">
        <a class="navbar-link">
          Archives
        </a>

        <div class="navbar-dropdown">
          <a class="navbar-item">
            By Tags
          </a>
          <a class="navbar-item">
            By Category
          </a>
          <hr class="navbar-divider">
          <a class="navbar-item">
            By Date
          </a>
        </div>
      </div>

      <a class="navbar-item">
        About
      </a>
    </div>

    <div class="navbar-end">
      <form class="is-marginless" method="get"
            action="<%- url_for("/pages/search/") %>">
        <div class="navbar-item">
          <input class="" type="text" name="q"
            placeholder="Search..." aria-label="Search">
          &nbsp;
          <button class="button is-light" 
            type="submit">Search</button>
        </div>
      </form>
    </div>

  </div>
</nav>
{{< / highlight >}}

#### Render: Browser

Visit the homepage in your favorite browser.
You should see, a working burgermenu, by now.

* <http://localhost:4000/>

![Hexo Bulma: Layout Navigation Bar][image-ss-02-browser-navbar]

-- -- --

### 6: Further Layout: Page and List.

After minimalist Bulma layout above,
the next step is to create the layout.
We are going to enhance our layout for 

* <code>layout/index.html</code>

* <code>layout/page.html</code>

#### Layout: EJS Index

We should also change the list

* <code>themes/tutor-02/layout/index.ejs</code>
  : [gitlab.com/.../layout/index.ejs][tutor-layout-index]

{{< highlight html >}}
<main role="main" 
      class="column is-full has-background-light">
  <section class="section">

  <h1 class="title is-4"><%= config.author %></h1>
  <h2 class="subtitle is-4"><%= config.subtitle %></h2>

  <ul>
  <% page.posts.each(function(post){ %>
    <li>
      <b><a href="<%- url_for(post.path) %>">
        <%= post.title %>
      </a></b>
      <br/>
      
      <p><%= date(post.date, config.date_format) %></p>
      <br/>
      
      <% if(post.excerpt) { %>
      <p><%- post.excerpt %></p>
      <% } %>
    </li>
  <% }) %>
  </ul>

  </section>
</main>
{{< / highlight >}}

#### Configuration

We need to setup permalink and stuff.
Add these lines below our configuration.

* <code>_config.yml</code>
  : [gitlab.com/.../_config.yml][tutor-hexo-config]

{{< highlight yaml >}}
# Home page setting
index_generator:
  path: '/blog'
  order_by: -date
{{< / highlight >}}

#### Render: Browser: Index

Open in your favorite browser.
You should see, simple index with article list, by now.

* <http://localhost:4000/blog/>

![Hexo Bulma: Layout Index][image-ss-02-browser-index]

#### Layout: EJS Page

This one artefact, for regular content page, including main page.

* <code>themes/tutor-02/layout/page.ejs</code>
  : [gitlab.com/.../layout/page.ejs][tutor-layout-page]

{{< highlight html >}}
  <main role="main" 
        class="column is-two-thirds has-background-primary">
    <article class="blog-post">
      <h1 class="title is-4"><%- page.title %></h1>
      <%- page.content %>
    </article>
  </main>

  <aside class="column is-one-thirds has-background-info">
    <div class="blog-sidebar">
      Side menu
    </div>
  </aside>
{{< / highlight >}}

#### Render: Browser: Page

Open in your favorite browser.
You should see, simple page content, by now.

* <http://localhost:4000/hello-there.html>

![Hexo Bulma: Layout Page][image-ss-02-browser-page]

-- -- --

### 7: Custom Page Posts

> Custom Page is a must have SSG Concept

Consider remake our simple <code>post.ejs</code>,
adding a more useful information, semantic, and bootstrap's classes.

#### Post: Single

This is very similar with `page.ejs`.
The point is you can customize as creative as you want.

* <code>themes/tutor-02/layout/post.ejs</code>
  : [gitlab.com/.../layout/post.ejs][tutor-layout-post]

{{< highlight html >}}
  <main role="main" 
        class="column is-two-thirds">
    <div class="blog-post">
      <section>
        <h1 class="title is-4"><%- page.title %></h1>
        <p  class="blog-post-meta"
            ><%= date(page.date, config.date_format) %> by <a href="#"
            ><%= config.author %></a></p>
      </section>

      <article>
        <%- page.content %>
      </article>
    </div><!-- /.blog-post -->
  </main>

  <aside class="column is-one-thirds has-background-info">
    <div class="blog-sidebar">
      <h4 class="is-size-4 is-italic">About You</h4>
      <p>Are you an angel ?</p>
    </div><!-- /.blog-sidebar -->
  </aside>
{{< / highlight >}}

Most class are in official Bulma documentation.

#### Configuration

We need to setup date formatting and also markdown linebreak setting.
Add these lines below our configuration.

* <code>_config.yml</code>
  : [gitlab.com/.../_config.yml][tutor-hexo-config]

{{< highlight yaml >}}
# Date / Time format
date_format: MMMM DD, YYYY
time_format: HH:mm:ss

# Markdown
marked:
  gfm: true
  breaks: false
{{< / highlight >}}

Restart the server, to apply the new configuration.

#### Assets: Stylesheet

No need any custom stylesheet.
We have already packed the required class, before this section.

#### Render: Browser

Open in your favorite browser.
You should see, a post, with sidebar.

* <http://localhost:4000/2016/11/13/white-winter/>

![Hexo Bulma: Post without Layout][image-ss-02-browser-post]

Note that, this is already using responsive grid.
It has different looks on different screen.
You should try to resize your browser, to get the idea.

![Hexo Bulma: Post with sidebar Layout][image-ss-02-browser-post-grid]

-- -- --

### 7: Summary

So far here are artefacts for our layouts.

{{< highlight conf >}}
$ tree themes/tutor-02/layout
themes/tutor-02/layout
├── index.ejs
├── layout.ejs
├── page.ejs
├── post.ejs
└── site
    ├── footer.ejs
    ├── head.ejs
    ├── header.ejs
    └── scripts.ejs

1 directory, 8 files
{{< / highlight >}}

![Hexo Bulma: Artefacts Summary in Tree][image-ss-02-tree-layouts]

-- -- --

### What is Next ?

Consider continue reading [ [Hexo - Bulma - SASS Introduction][local-whats-next] ].
We are going to use SASS in theme, with custom SASS, all compiled into CSS.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2019/05/07/hexo-bulma-sass-intro/
[local-bulma-navbar]:       {{< baseurl >}}frontend/2019/03/03/bulma-navbar/

[image-ss-02-source]:       {{< assets-ssg >}}/2019/05/hexo-ejs-bulma-tutor-02.zip

[image-ss-02-browser-navbar]:   {{< assets-ssg >}}/2019/05/22-browser-navbar.png
[image-ss-02-browser-index]:    {{< assets-ssg >}}/2019/05/22-browser-index.png
[image-ss-02-browser-page]:     {{< assets-ssg >}}/2019/05/22-browser-page.png
[image-ss-02-tree-layouts]:     {{< assets-ssg >}}/2019/05/23-tree-layouts.png
[image-ss-02-browser-post]:      {{< assets-ssg >}}/2019/05/25-browser-post.png
[image-ss-02-browser-post-grid]: {{< assets-ssg >}}/2019/05/25-browser-post-grid.png

[tutor-hexo-config]:    {{< tutor-hexo-bulma >}}/_config.yml

[tutor-l-site-head]:    {{< tutor-hexo-bulma >}}/themes/tutor-02/layout/site/head.ejs
[tutor-l-site-header]:  {{< tutor-hexo-bulma >}}/themes/tutor-02/layout/site/header.ejs
[tutor-l-site-footer]:  {{< tutor-hexo-bulma >}}/themes/tutor-02/layout/site/footer.ejs
[tutor-l-site-scripts]: {{< tutor-hexo-bulma >}}/themes/tutor-02/layout/site/scripts.ejs

[tutor-layout-layout]:  {{< tutor-hexo-bulma >}}/themes/tutor-02/layout/layout.ejs
[tutor-layout-page]:    {{< tutor-hexo-bulma >}}/themes/tutor-02/layout/page.ejs
[tutor-layout-post]:    {{< tutor-hexo-bulma >}}/themes/tutor-02/layout/post.ejs
[tutor-layout-index]:   {{< tutor-hexo-bulma >}}/themes/tutor-02/layout/index.ejs

[tutor-source]:         {{< tutor-hexo-bulma >}}/themes/tutor-02/source/

[tutor-s-css-main]:     {{< tutor-hexo-bulma >}}/themes/tutor-02/source/css/main.css
