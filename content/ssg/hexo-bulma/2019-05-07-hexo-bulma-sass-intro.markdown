---
type   : post
title  : "Hexo - Bulma - SASS Introduction"
date   : 2019-05-07T09:17:35+07:00
slug   : hexo-bulma-sass-intro
categories: [ssg]
tags      : [hexo, bulma, sass]
keywords  : [static site, custom theme, ejs, layout, compile sass, custom sass]
author : epsi
opengraph:
  image: assets/site/images/topics/hexo-bulma-markdown.png

toc    : "toc-2019-05-hexo-bulma-step"

excerpt:
  Building Hexo Step by step, with Bulma as stylesheet frontend.
  Apply SASS in Hexo theme.

---

### Preface

This article is intended for beginner.

> Goal: Apply SASS in Hexo theme

Custom SASS could make a lot of enhancement,
compared with pure Bulma's CSS.

#### Reading

If you do not have any idea about SASS you should read this first:

* <https://sass-lang.com/>

#### Source Code

You can download the source code of this article here.

* [hexo-ejs-bulma-tutor-03-sass.zip][image-ss-03-source]

Extract and run on CLI:

{{< highlight html >}}
$ npm install
{{< / highlight >}}

#### Related Article

I wrote about Bulma Navigation Bar that used in this article.
Step by step article, about building Navigation Bar.
You can read here:

* [Bulma SASS Introduction][local-bulma-sass-intro]

-- -- --

### 1: Prepare

This preparation is required.

#### Theme

1. Create directory <code>themes/tutor-03/sass</code>

You should see the sass directory,
in this new <code>tutor-03</code> theme:

2. Edit configuration <code>_config.yml</code>, and save.

{{< highlight yaml >}}
# Extensions
theme: tutor-03
{{< / highlight >}}

{{< highlight bash >}}
$ tree -d -I node_modules -L 2
{{< / highlight >}}

![Hexo Bulma SASS: Tree theme][image-ss-03-tree-themes]

3. Have a look at the example in repository.

**Source**:
	[gitlab.com/.../themes/tutor-03][tutor-theme-03]

#### Example Source

Or we can just simplify the step,
by extracting the source code provided above.
You can read the detail of each SASS on 
[Bulma SASS Introduction][local-bulma-sass-intro].

Your <code>sass</code> directory should be looks like this.

{{< highlight conf >}}
$ tree themes/tutor-03/sass/ -L 2
themes/tutor-03/sass/
├── css
│   ├── _blog.sass
│   ├── _box-decoration.sass
│   ├── bulma.sass
│   ├── _decoration.sass
│   ├── _derived-variables.sass
│   ├── _initial-variables.sass
│   ├── _layout.sass
│   └── main.sass
└── vendors
    ├── bulma
    └── bulma.sass
{{< / highlight >}}

![Hexo Bulma SASS: Tree sass][image-ss-03-tree-sass]

**Source**:
	[gitlab.com/.../sass][tutor-sass]

_._

-- -- --

### 2: Compile Sass

There are some tools available.

#### Deprecated

Ruby-sass will be deprecated soon.

* <https://sass-lang.com/install>

As an alternative you can use dart-sass, node-sass.

* [Sass: Common Compiler][local-sass-compiler]

Or any task runner such as gulp or grunt.

* [Sass: Task Runner][local-sass-task-runner]

#### Using SASS

Using <code>themes/tutor-03</code> as working directory,
generating css is as simply as this command below.
The command will map any necessary input scss in <code>sass</code> directory,
into output css in <code>static</code> directory.

{{< highlight bash >}}
$ dart-sass --watch -I sass sass/css:static/css/ --source-map
Compiled sass/css/main.sass to static/css/main.css.
Compiled sass/css/bulma.sass to static/css/bulma.css.
Sass is watching for changes. Press Ctrl-C to stop.
{{< / highlight >}}

To avoid, typing command, over and over again,
cosider using `watch` argument.
This will continuously update as the input change.

![Hexo Bulma SASS: sass update][image-ss-03-sass-update]

**Result**:
	[gitlab.com/.../static/bulma][tutor-css-bulma]

#### Generated CSS

It is using the same place as previous CSS.
So there is no need to change the header.

* <code>/themes/tutor-03/source/css/</code>
  : [gitlab.com/.../source/css/][tutor-source-css].

Now check the browser, to check if there is no error.

#### Bulma SASS

We need to refresh our memory about what is in the `bulma.sass`.

Create <code>bulma.scss</code>
: [gitlab.com/.../sass/bulma.scss][tutor-sass-bulma].

{{< highlight sass >}}
@import "initial-variables"
@import "vendors/bulma"
@import "derived-variables"
{{< / highlight >}}

#### Main SASS

Also the `main.sass`.

Create <code>main.scss</code>
: [gitlab.com/.../sass/css/main.scss][tutor-sass-main].

{{< highlight sass >}}
@import "initial-variables"
@import "vendors/bulma/utilities/initial-variables"
@import "vendors/bulma/utilities/functions"
@import "vendors/bulma/utilities/derived-variables"
// @import "derived-variables"

@import "vendors/bulma/utilities/mixins"
@import "layout"
@import "decoration"
@import "box-decoration"
@import "blog"
{{< / highlight >}}

#### Custom Class

What new class provided in this `main.sass`?
This is the list:

* <code>maxwidth</code>

* <code>box-deco</code>,
  <code>header-deco</code>,
  <code>footer-deco</code>

-- -- --

### 3: Yet Another Design Example

We need to enhance the looks offered by all this new SASS.
So we can differ, from the previous pure Bulma CSS design,
in previous article.

#### Apply Custom Class on Layout

To see the changes we must apply the new class provided in or layout.
Or even better, consider change the design color.

* <code>layouts/layout.ejs</code>

* <code>layouts/index.html</code>

* <code>layouts/partials/site/header.html</code>

* <code>layouts/partials/site/footer.html</code>

#### Layout: EJS Layout

We scan start with the same layout as previous tutorial.

* <code>themes/tutor-03/layout/layout.ejs</code>
  : [gitlab.com/.../layout/layout.ejs][tutor-layout-layout]

{{< highlight html >}}
<!DOCTYPE html>
<html lang="en">
<%- partial('site/head') %>
<body>
<%- partial('site/header') %>

  <div class="columns is-8 layout-base maxwidth">
    <%- body %>
  </div>

<%- partial('site/footer') %>
<%- partial('site/scripts') %>
</body>
</html>
{{< / highlight >}}

#### Layout: EJS Page

* <code>themes/tutor-03/layout/page.ejs</code> (homepage)
  : [gitlab.com/.../layout/page.ejs][tutor-layout-page]
  
{{< highlight html >}}
  <main role="main" 
        class="column is-full blog-column box-deco has-background-white">
    <article class="blog-post">
      <h1 class="title is-4"><%- page.title %></h1>
      <%- page.content %>
    </article>
  </main>
{{< / highlight >}}

#### Partial: Header

* <code>themes/tutor-03/layout/site/header.ejs</code>
  : [gitlab.com/.../layout/site/header.ejs][tutor-l-site-header]

{{< highlight html >}}
<nav role="navigation" aria-label="main navigation"
     class="navbar is-fixed-top is-white maxwidth header-deco"
     id="navbar-vue-app">
...
{{< / highlight >}}

#### Partial: Footer

* <code>themes/tutor-03/layout/site/footer.ejs</code>
  : [gitlab.com/.../layout/site/footer.ejs][tutor-l-site-footer]

{{< highlight html >}}
<footer class="site-footer">
  <div class="navbar is-fixed-bottom maxwidth
        is-white has-text-centered is-vcentered footer-deco">
...
{{< / highlight >}}

#### Render: Browser Page

Open in your favorite browser.
You should see, non-colored homepage, by now.

* <http://localhost:4000/>

![Hexo Bulma: Layout Homepage][image-ss-03-browser-homepage]

-- -- --

### 4: Summary

As a summary, here is the content of scss input:

{{< highlight conf >}}
$ tree sass/css
sass/css
├── _blog.sass
├── _box-decoration.sass
├── bulma.sass
├── _decoration.sass
├── _derived-variables.sass
├── _initial-variables.sass
├── _layout.sass
└── main.sass
{{< / highlight >}}

![Hexo Bulma SASS: Tree Input Summary][image-ss-03-tree-input]

And the content of css output would be:

{{< highlight conf >}}
$ tree static/css 
static/css
├── bulma.css
├── bulma.css.map
├── main.css
└── main.css.map
{{< / highlight >}}

![Hexo Bulma SASS: Tree Output Summary][image-ss-03-tree-output]

With map, you can debug in object inspector,
such as using inspect element menu in firefox based browser.

_._

-- -- --

### What is Next ?

I do not go deep about sass, as this is not a bulma tutorial.

	Create necessary sass, as you need.
	However, still Bulma can save you,
	from writing a lot of CSS code.

Consider continue reading next part of this article 
in [ [Hexo - Bulma - SASS Layout][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2019/05/08/hexo-bulma-sass-layout/
[local-bulma-sass-intro]:   {{< baseurl >}}frontend/2019/03/04/bulma-sass-intro/

[local-sass-compiler]:      {{< baseurl >}}frontend/2019/01/13/sass-common-compiler/
[local-sass-task-runner]:   {{< baseurl >}}frontend/2019/01/15/sass-task-runner/

[image-ss-03-source]:       {{< assets-ssg >}}/2019/05/hexo-ejs-bulma-tutor-03-sass.zip

[image-ss-03-tree-sass]:        {{< assets-ssg >}}/2019/05/31-tree-sass.png
[image-ss-03-tree-themes]:      {{< assets-ssg >}}/2019/05/31-tree-themes-03.png

[image-ss-03-sass-update]:      {{< assets-ssg >}}/2019/05/32-sass-update.png
[image-ss-03-browser-homepage]: {{< assets-ssg >}}/2019/05/32-browser-homepage.png

[image-ss-03-tree-input]:       {{< assets-ssg >}}/2019/04/36-tree-input-summary.png
[image-ss-03-tree-output]:      {{< assets-ssg >}}/2019/04/36-tree-output-summary.png

[tutor-theme-03]:       {{< tutor-hexo-bulma >}}/themes/tutor-03/
[tutor-sass]:           {{< tutor-hexo-bulma >}}/themes/tutor-03/sass/
[tutor-sass-css]:       {{< tutor-hexo-bulma >}}/themes/tutor-03/sass/css/
[tutor-sass-main]:      {{< tutor-hexo-bulma >}}/themes/tutor-03/sass/css/main.scss
[tutor-sass-bulma]:     {{< tutor-hexo-bulma >}}/themes/tutor-03/sass/css/bulma.scss
[tutor-source-css]:     {{< tutor-hexo-bulma >}}/themes/tutor-03/source/css/

[tutor-l-site-head]:    {{< tutor-hexo-bulma >}}/themes/tutor-03/layout/site/head.ejs
[tutor-l-site-header]:  {{< tutor-hexo-bulma >}}/themes/tutor-03/layout/site/header.ejs
[tutor-l-site-footer]:  {{< tutor-hexo-bulma >}}/themes/tutor-03/layout/site/footer.ejs
[tutor-l-site-scripts]: {{< tutor-hexo-bulma >}}/themes/tutor-03/layout/site/scripts.ejs

[tutor-layout-layout]:  {{< tutor-hexo-bulma >}}/themes/tutor-03/layout/layout.ejs
[tutor-layout-page]:    {{< tutor-hexo-bulma >}}/themes/tutor-03/layout/page.ejs
[tutor-layout-post]:    {{< tutor-hexo-bulma >}}/themes/tutor-03/layout/post.ejs
[tutor-layout-index]:   {{< tutor-hexo-bulma >}}/themes/tutor-03/layout/index.ejs



