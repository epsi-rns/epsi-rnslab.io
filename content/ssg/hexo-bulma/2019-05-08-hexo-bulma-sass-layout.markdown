---
type   : post
title  : "Hexo - Bulma - SASS Layout"
date   : 2019-05-08T09:17:35+07:00
slug   : hexo-bulma-sass-layout
categories: [ssg]
tags      : [hexo, bulma, sass]
keywords  : [static site, custom theme, page, index list, custom page, post]
author : epsi
opengraph:
  image: assets/site/images/topics/hexo-bulma-markdown.png

toc    : "toc-2019-05-hexo-bulma-step"

excerpt:
  Building Hexo Step by step, with Bulma as stylesheet frontend.
  Apply SASS in Hexo theme.

---

### Preface

This article is intended for beginner.

> Goal: Apply SASS in Hexo theme

Custom SASS could make a lot of enhancement,
compared with pure Bulma's CSS.

#### Source Code

You can download the source code of this article here.

* [hexo-ejs-bulma-tutor-03-sass.zip][image-ss-03-source]

Extract and run on CLI:

{{< highlight html >}}
$ npm install
{{< / highlight >}}

#### Related Article

I wrote about Bulma Navigation Bar that used in this article.
Step by step article, about building Navigation Bar.
You can read here:

* [Bulma SASS Introduction][local-bulma-sass-intro]

-- -- --

### 5: Regular Page

Consider change a bit the page

#### Layout: EJS Page

We scan start with the same layout as previous tutorial.

* <code>themes/tutor-03/layout/page.ejs</code>
  : [gitlab.com/.../layout/page.ejs][tutor-layout-page]

{{< highlight html >}}
  <main role="main" 
        class="column is-two-thirds blog-column box-deco has-background-white">
    <article class="blog-post">
      <h1 class="title is-4"><%- page.title %></h1>
      <%- page.content %>
    </article>
  </main>

  <aside class="column is-one-thirds box-deco has-background-white">
    <div class="blog-sidebar">
      Side menu
    </div><!-- /.blog-sidebar -->
  </aside>
{{< / highlight >}}

#### Render: Browser Page

Open in your favorite browser.
You should see, non-colored homepage, by now.

* <http://localhost:4000/hello-there.html>

![Hexo Bulma: Layout Page][image-ss-03-browser-page]

This design is already equipped with responsive layout.

![Hexo Bulma: Layout Page Grid][image-ss-03-browser-page-flex]

-- -- --

### 6: Index List

Now comes the Hexo specific parts:

* <code>layouts/_default/single.html</code>

* <code>layouts/_default/list.html</code>

#### Layout: EJS Index

We should also change the list

* <code>themes/tutor-03/layout/index.ejs</code>
  : [gitlab.com/.../layout/index.ejs][tutor-layout-index]

{{< highlight html >}}
<main role="main" 
      class="column is-full box-deco has-background-white">
  <section class="section">

  <h1 class="title is-4"><%= config.author %></h1>
  <h2 class="subtitle is-4"><%= config.subtitle %></h2>

  <ul>
  <% page.posts.each(function(post){ %>
    <li>
      <b><a href="<%- url_for(post.path) %>">
        <%= post.title %>
      </a></b>
      <br/>
      
      <p><%= date(post.date, config.date_format) %></p>
      <br/>
      
      <% if(post.excerpt) { %>
      <p><%- post.excerpt %></p>
      <% } %>
    </li>
  <% }) %>
  </ul>

  </section>
</main>
{{< / highlight >}}

#### Render: Browser: List

Open in your favorite browser.
You should see, simple index with article list, by now.

* <http://localhost:4000/blog/>

![Hexo Bulma: Layout Index][image-ss-03-browser-index]

-- -- --

### 7: Custom Page Posts

> Custom Page is a must have SSG Concept

Here we are again with our simple <code>post.ejs</code>.
Hexo can create as many custom page,
the default one is **post**.
All this custom page use <code>layout.ejs</code> as base skeleton.

#### Layout: EJS Post

This <code>post.ejs</code> is very similar with <code>page.ejs</code>.
The point is you can customize as creative as you want.

* <code>themes/tutor-03/layout/post.ejs</code>
  : [gitlab.com/.../layout/post.ejs][tutor-layout-post]

{{< highlight html >}}
  <main role="main" 
        class="column is-two-thirds">
    <div class="blog-post">
      <section>
        <h1 class="title is-4"><%- page.title %></h1>
        <p  class="blog-post-meta"
            ><%= date(page.date, config.date_format) %> by <a href="#"
            ><%= config.author %></a></p>
      </section>

      <article>
        <%- page.content %>
      </article>
    </div><!-- /.blog-post -->
  </main>

  <aside class="column is-one-thirds has-background-info">
    <div class="blog-sidebar">
      <h4 class="is-size-4 is-italic">About You</h4>
      <p>Are you an angel ?</p>
    </div><!-- /.blog-sidebar -->
  </aside>
{{< / highlight >}}

#### Render: Browser

Open in your favorite browser.
You should see, a post, with sidebar.

* <http://localhost:4000/2016/11/13/white-winter/>

![Hexo Bulma: Post without Layout][image-ss-03-browser-post]

Note that, this is already using responsive flexbox.
It has different looks on different screen.
You should try to resize your browser, to get the idea.

![Hexo Bulma: Post with sidebar Layout][image-ss-03-browser-post-flex]

-- -- --

### What is Next ?

Consider continue reading [ [Hexo Bulma - Section List][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2019/05//10/hugo-ejs-section-list-intro/
[local-bulma-sass-intro]:   {{< baseurl >}}frontend/2019/03/04/bulma-sass-intro/

[image-ss-03-source]:       {{< assets-ssg >}}/2019/05/hexo-ejs-bulma-tutor-03-sass.zip

[image-kitten]:             {{< baseurl >}}/assets/site/images/cats/shoes.jpg

[image-ss-03-browser-index]:     {{< assets-ssg >}}/2019/05/32-browser-index.png

[image-ss-03-browser-page]:      {{< assets-ssg >}}/2019/05/32-browser-page.png
[image-ss-03-browser-page-flex]: {{< assets-ssg >}}/2019/05/32-browser-page-flex.png

[image-ss-03-browser-post]:      {{< assets-ssg >}}/2019/05/35-browser-post.png
[image-ss-03-browser-post-flex]: {{< assets-ssg >}}/2019/05/35-browser-post-flex.png

[tutor-layout-page]:    {{< tutor-hexo-bulma >}}/themes/tutor-03/layout/page.ejs
[tutor-layout-post]:    {{< tutor-hexo-bulma >}}/themes/tutor-03/layout/post.ejs
[tutor-layout-index]:   {{< tutor-hexo-bulma >}}/themes/tutor-03/layout/index.ejs

