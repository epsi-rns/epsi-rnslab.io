---
type   : post
title  : "Hexo - EJS - Section List Introduction"
date   : 2019-05-10T09:17:35+07:00
slug   : hexo-ejs-section-list-intro
categories: [ssg]
tags      : [hexo]
keywords  : [static site, custom theme, ejs, layout, scaffolds, markdown, populate content, archive, category, tag]
author : epsi
opengraph:
  image: assets/site/images/topics/hexo-bulma-markdown.png

toc    : "toc-2019-05-hexo-bulma-step"

excerpt:
  Building Hexo Step by step, with Bulma as stylesheet frontend.
  Produce list for special page sections, Archive, Category, Tags.

---

### Preface

> Goal: Produce list for special page sections: Archive, Category, Tag

Hexo has this four special page sections:

* Index

* Archive

* Category

* Tag

We have already discuss about index in previous section,
and put the index in <code>blog</code> path in url.
Now we are going to discuss about the other three.

#### Source Code

You can download the source code of this article here.

* [hexo-ejs-bulma-tutor-03.zip][image-ss-03-source]

Extract and run on CLI:

{{< highlight html >}}
$ npm install
{{< / highlight >}}

-- -- --

### 1: Head Title

Since we have different kind of page section.
The title should reflect the page section name.
This won't be a complex task, as we have embedded javascript.

#### Javascript: EJS HTML Head

Remember this EJS inside title tag?

{{< highlight html >}}
  <title><%= page.title || config.title%></title>
{{< / highlight >}}

We are going to change. A lot!
As this javascript below.

{{< highlight javascript >}}
  var title = page.title;

  if (is_archive()){
    title = __('archive');

    if (is_month()){
      title += ': ' + page.year + '/' + page.month;
    } else if (is_year()){
      title += ': ' + page.year;
    }
  } else if (is_category()){
    title = __('category') + ': ' + page.category;
  } else if (is_tag()){
    title = __('tag') + ': ' + page.tag;
  }
{{< / highlight >}}

Now that we already have <code>title</code> variable,
we can call it later in HTML view.

{{< highlight html >}}
  <title><%= title || config.title%></title>
{{< / highlight >}}

__.__

#### Section's URL Example

Script above, will parse the URL below:

* <http://localhost:4000/archives/>

* <http://localhost:4000/archives/2015/>

* <http://localhost:4000/archives/2015/05/>

* <http://localhost:4000/categories/lyric/>

* <http://localhost:4000/tags/pop/>

It is all depends on your content.

#### Layout: EJS HTML Head

Now put the javascript inside the EJS for html head.

* <code>themes/tutor-02/layout/site/head.ejs</code>
  : [gitlab.com/.../layout/site/head.ejs][tutor-l-site-head]

{{< highlight html >}}
<%
  ...
_%>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title><%= title || config.title%></title>

  <% if (theme.favicon){ %>
     <link href="<%- theme.favicon %>" rel="shortcut icon" type="image/x-icon" />
  <% } %>

  <%- css(['/css/bulma.css', '/css/main.css']) %>
</head>
{{< / highlight >}}

_._

#### Language

What is this <code>__('archive')</code> means in javascript above?

This will translate the language given.

* <code>themes/tutor-02/languages/en.yml</code>
  : [gitlab.com/.../languages/en.yml][tutor-lang-en]

{{< highlight yaml >}}
archive: Archive
category: Category
tag: Tag
{{< / highlight >}}

* <code>themes/tutor-02/languages/id.yml</code>
  : [gitlab.com/.../languages/id.yml][tutor-lang-id]

{{< highlight yaml >}}
archive: Arsip
category: Kategori
tag: Tagar
{{< / highlight >}}

-- -- --

### 2: Archive

#### Layout: EJS Archive

Remember the page in previous article.

{{< highlight html >}}
  <main role="main" 
        class="column is-two-thirds has-background-white
               blog-column box-deco">
    <article class="blog-post">
      <h1 class="title is-4"><%- page.title %></h1>
      <%- page.content %>
    </article>
  </main>
{{< / highlight >}}

We are going to use this template,
and grow to something more suitable for our archive page.

* <code>themes/tutor-03/layout/archive.ejs</code>

{{< highlight html >}}
<% 
  var section_title = __('archive');

  if (is_month()){
    section_title += ': ' + page.year + '/' + page.month;
  } else if (is_year()){
    section_title += ': ' + page.year;
  }
%>
<main role="main" 
      class="column is-full box-deco has-background-white">
  <section class="section">
    <h4 class="title is-4"><%= section_title %></h4>
  </section>

  <section class="section" id="archive">
  <% page.posts.each(function(post){ %>
    <div class="archive-item">
      <div class="is-pulled-left"><a href="<%- url_for(post.path) %>">
        <%= post.title %>
      </a></div>
      <div class="is-pulled-right has-text-right"><time>
          <%= date(post.date, "DD MMM") %>&nbsp;
          <span class="fa fa-calendar"></span>
      </time></div>
      <div class="is-clearfix"></div>
    </div>
  <% }) %>
  </section>
</main>
{{< / highlight >}}

__.__

#### Render: Browser

Open in your favorite browser.
You should see, our very first section page.
The archive page!

* <http://localhost:4000/archives/2016/11/>

![Hexo Bulma: Archive Page][image-ss-03-browser-archive]

#### The Loop

> How Does It Works?

The simplified range loop, is as below:

{{< highlight javascript >}}
  <% page.posts.each(function(post){ %>
    ...
    <%= post.title %>
    ...
  <% }) %>
{{< / highlight >}}

This will only show page defined post layout (the default one).
This means if you have this frontmatter as below,
the page won't be shown on the archive list.

{{< highlight yaml >}}
layout    : page
{{< / highlight >}}

-- -- --

### 2: Populate the Content

As always, populate content with good reading is,
as hard as giving meaningful variable.
Luckily I have done it for you.

#### Scaffold

I have arrange the scaffold,
so that every post has **tags** and **categories**

* <code>scaffolds/post.md</code>
  : [gitlab.com/.../scaffolds/post.md][tutor-scaffolds-post]

{{< highlight yaml >}}
---
title     : {{ title }}
date      : {{ date }}
tags      :
categories:
---
{{< / highlight >}}

Now you are ready to populate the content.

#### Example Content

* <code>source/_posts/lyrics/bon-jovi-diamond-ring.md</code>
  : [gitlab.com/.../...diamond-ring.md][tutor-content-diamond-ring]

{{< highlight markdown >}}
---
title     : Bon Jovi - Diamond Ring
date      : 2015/03/15 07:35:05
tags      : [rock, 90s]
category  : [lyric]
---

Diamond ring, wear it on your hand  
It's gonna tell the world, I'm your only man  
<!-- more --> 
Diamond ring, diamond ring  
Baby, you're my everything, diamond ring 

You know, I bleed every night you sleep  
'Cause I don't know if I'm in your dreams 
{{< / highlight >}}

#### And The Rest

Consider do the rest for these nine lyrics:

{{< highlight bash >}}
$ tree source
source
├── about
│   └── longing.md
├── hello-there.html
├── index.html
└── _posts
    ├── lyrics
    │   ├── barbara-streisand-the-way-we-were.md
    │   ├── bon-jovi-diamond-ring.md
    │   ├── darius-rucker-it-wont-be-like-this-for-long.md
    │   ├── david-gates-lady-valentine.md
    │   ├── dido-white-flag.md
    │   ├── george-benson-moodys-mood.md
    │   ├── heatwave-all-i-am.md
    │   ├── moody-blues-white-satin.md
    │   ├── natalie-cole-i-wish-you-love.md
    │   ├── norah-jones-shoot-the-moon.md
    │   └── vanessa-williams-love-is.md
    ├── quotes
    │   ├── dead-poets-society.md
    │   ├── every-day.md
    │   ├── fight-club.md
    │   ├── jerry-maguire.md
    │   └── scott-pilgrim.md
    └── white-winter.md

4 directories, 20 files
{{< / highlight >}}

_._

![Hexo: All Content][image-ss-03-tree-content-all]

Also edit the frontmatter as necessary,
for the rest of the content.

-- -- --

### 4: Generic Template

The three pages, Archive, Category and Tag, are very similar.
Thus we can make a generic template.

#### Layout: EJS Post List

I'm going to put the generic template in <code>summary</code> directory.

* <code>themes/tutor-03/layout/summary/post-list.ejs</code>
  : [gitlab.com/.../layout/summary/post-list.ejs][tutor-layout-post-list]

{{< highlight html >}}
<main role="main" 
      class="column is-full box-deco has-background-white">
  <section class="section">
    <h4 class="title is-4"><%= section_title %></h4>
  </section>

  <section class="section" id="archive">
  <ul>
  <% page.posts.each(function(post){ %>
    <li>
      <div class="is-pulled-left">
        <a href="<%- url_for(post.path) %>">
        <%= post.title %>
      </a></div>
      <div class="is-pulled-right has-text-right"><time>
        <%= date(post.date, "DD MMM") %>&nbsp;
      </time></div>
      <div class="is-clearfix"></div>
    </li>
  <% }) %>
  </ul>
  </section>
</main>
{{< / highlight >}}

Now we can use them for

* Archive section page,

* Category section page, and 

* Tag section page.

#### Layout: EJS Archive

* <code>themes/tutor-03/layout/archive.ejs</code>
  : [gitlab.com/.../layout/archive.ejs][tutor-layout-archive]

{{< highlight javascript >}}
<% 
  var section_title = __('archive');

  if (is_month()){
    section_title += ': ' + page.year + '/' + page.month;
  } else if (is_year()){
    section_title += ': ' + page.year;
  }
%>
<%-
  partial('summary/post-list', {
    section_title: section_title
  })
%>
{{< / highlight >}}

![Hexo: Browser Section Archive][image-ss-03-section-archive]

#### Layout: EJS Category

* <code>themes/tutor-03/layout/category.ejs</code>
  : [gitlab.com/.../layout/category.ejs][tutor-layout-cat]

{{< highlight javascript >}}
<%- 
  partial('summary/post-list', {
    section_title: __('category') + ': ' + page.category
  })
%>
{{< / highlight >}}

![Hexo: Browser Section Category][image-ss-03-section-category]

__.__

#### Layout: EJS Tag

* <code>themes/tutor-03/layout/tag.ejs</code>
  : [gitlab.com/.../layout/tag.ejs][tutor-layout-tag]

{{< highlight javascript >}}
<%- 
  partial('summary/post-list', {
    section_title: __('tag') + ': ' + page.tag
  })
%>
{{< / highlight >}}

![Hexo: Browser Section Tag][image-ss-03-section-tag]

__.__

-- -- --

### What is Next ?

Consider continue reading [ [Hexo - Custom Page Introduction][local-whats-next] ].
There are, some interesting topic about <code>Custom Page in Hexo</code>. 

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2019/05/11/hexo-ejs-custom-page-intro/

[image-ss-03-source]:       {{< assets-ssg >}}/2019/05/hexo-ejs-bulma-tutor-03.zip

[image-ss-03-browser-archive]:   {{< assets-ssg >}}/2019/05/36-browser-archive.png
[image-ss-03-tree-content-all]:  {{< assets-ssg >}}/2019/05/37-tree-content-all.png

[image-ss-03-section-archive]:   {{< assets-ssg >}}/2019/05/37-section-archive.png
[image-ss-03-section-category]:  {{< assets-ssg >}}/2019/05/37-section-category.png
[image-ss-03-section-tag]:       {{< assets-ssg >}}/2019/05/37-section-tag.png

[tutor-l-site-head]:    {{< tutor-hexo-bulma >}}/themes/tutor-03/layout/site/head.ejs
[tutor-lang-en]:        {{< tutor-hexo-bulma >}}/themes/tutor-03/languages/en.yml
[tutor-lang-id]:        {{< tutor-hexo-bulma >}}/themes/tutor-03/languages/id.yml
[tutor-scaffolds-post]: {{< tutor-hexo-bulma >}}/scaffolds/post.md
[tutor-content-diamond-ring]:     {{< tutor-hexo-bulma >}}/source/_posts/lyrics/bon-jovi-diamond-ring.md

[tutor-layout-post-list]:   {{< tutor-hexo-bulma >}}/themes/tutor-03/layout/summary/post-list.ejs
[tutor-layout-archive]: {{< tutor-hexo-bulma >}}/themes/tutor-03/layout/archive.ejs
[tutor-layout-cat]:     {{< tutor-hexo-bulma >}}/themes/tutor-03/layout/category.ejs
[tutor-layout-tag]:     {{< tutor-hexo-bulma >}}/themes/tutor-03/layout/tag.ejs
