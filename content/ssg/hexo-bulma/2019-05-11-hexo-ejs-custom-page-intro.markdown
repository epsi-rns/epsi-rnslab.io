---
type   : post
title  : "Hexo - EJS - Custom Page Introduction"
date   : 2019-05-11T09:17:35+07:00
slug   : hexo-ejs-custom-page-intro
categories: [ssg]
tags      : [hexo]
keywords  : [static site, custom theme, ejs, layout, landing page, categories, tags]
author : epsi
opengraph:
  image: assets/site/images/topics/hexo-bulma-markdown.png

toc    : "toc-2019-05-hexo-bulma-step"

excerpt:
  Building Hexo Step by step, with Bulma as stylesheet frontend.
  Create custom page to create landing page, and terms list.

---

### Preface

> Goal: Create custom page to create landing page, and terms list.

Making custom page in Hexo is easy.
The only known limitation is all custom page must follow basic layout.

#### Source Code

You can download the source code of this article here.

* [hexo-ejs-bulma-tutor-03.zip][image-ss-03-source]

Extract and run on CLI:

{{< highlight html >}}
$ npm install
{{< / highlight >}}

-- -- --

### 1: Landing Page

The easier way to understand custom page is to give simple example.
Our previous article has stated that the main page has sidebar,
because it use <code>page.ejs</code>.

We can make special template so that it has no sidebar,
but only full width content.

#### Layout: EJS Landing Page

* <code>themes/tutor-03/layout/kind/landing.ejs</code>
  : [gitlab.com/.../layout/kind/landing.ejs][tutor-layout-landing]

{{< highlight html >}}
<main role="main" 
     class="column is-full blog-column box-deco
            has-background-white has-text-centered">
  <article class="blog-post">
    <h1 class="title is-4"><%= config.author %></h1>
    <h2 class="subtitle is-4"><%= config.subtitle %></h2>
    
    <%- page.content %>
  </article>
</main>
{{< / highlight >}}

#### Content: Index

It is time apply this kind of custom page layout.
All you need to do is add <code>layout</code> in **frontmatter**.

{{< highlight html >}}
layout: kind/landing
{{< / highlight >}}

We also need to modify main page content,
to make it more like a landing page.
I don't know how to make a pretty landing page,
so I put business card instead.

* <code>source/index.html</code>
  : [gitlab.com/.../source/index.html][tutor-content-index]

{{< highlight html >}}
---
layout: kind/landing
title: Welcome to Heartbeat
---

  <div class="justify-content-center">
    <img src="site/images/adverts/one-page.png" 
         class="bio-photo" 
         alt="business card">
  </div>

  <p>Every journey begins with a single step.</p>
{{< / highlight >}}

#### Render: Browser

Open in your favorite browser.
Your landing page is ready.

* <http://localhost:4000/>

![Hexo Bulma: Landing Page][image-ss-03-browser-landing]

As I promised before, nomore sidebar.

-- -- --

### 2: Tags

Remember that Hexo default provide this links ?

* <http://localhost:4000/tags/jazz/>

But why can't we have this summary as one ?

* <http://localhost:4000/tags/>

Consider walking further for a different kind of custom page layout,
that provide list of tags. The URL would be as below:

* <http://localhost:4000/tags.html>

#### Layout: EJS Simple Tags List

* <code>themes/tutor-03/layout/kind/tags.ejs</code>

{{< highlight html >}}
<main role="main" 
      class="column is-full box-deco has-background-white">

  <section class="section">
    <h1 class="title is-4"><%= config.author %></h1>
    <h2 class="subtitle is-4"><%= config.subtitle %></h2>

    <h3 class="title is-3"><%= page.title %></h3>
  </section>

  <% site.tags.each(function(item){ %>
    <div id="<%= item.name %>">
      <p><%= __('tag') %>: 
        <a href="<%- url_for(item.path) %>">
        <%= item.name %></a>
      </p>
    </div>
  <% }) %>
</main>
{{< / highlight >}}

__.__

#### Content: Tags

Custom page is not enough. We need content.

* <code>source/tags.md</code>
  : [gitlab.com/.../source/tags.md][tutor-content-tags]

{{< highlight yaml >}}
---
layout: kind/tags
title: Tags
---
{{< / highlight >}}

#### Render: Browser

Open in your favorite browser.
You should see, a post, with sidebar.

* <http://localhost:4000/tags.html>

![Hexo: Custom Page: Simple Tag List][image-ss-03-browser-tags-only]

#### The Loop

> How Does It Works?

The simplified range loop, is as below:

{{< highlight javascript >}}
  <% site.tags.each(function(item){ %>
    ...
    <%= item.name %>
    ...
  <% }) %>
{{< / highlight >}}

#### Layout: EJS Tags with Tree

Why don't we go further, giving tree view for this tag list.
Every tag should show thier post list.

* <code>themes/tutor-03/layout/kind/tags.ejs</code>
  : [gitlab.com/.../layout/kind/tags.ejs][tutor-layout-tags]

{{< highlight html >}}
<main role="main" 
      class="column is-full box-deco has-background-white">

  <section class="section">
    <h1 class="title is-4"><%= config.author %></h1>
    <h2 class="subtitle is-4"><%= config.subtitle %></h2>

    <h3 class="title is-3"><%= page.title %></h3>
  </section>

  <% site.tags.each(function(item){ %>
  <section class="section">
    <div id="<%= item.name %>">
      <p><%= __('tag') %>: 
      <a href="<%- url_for(item.path) %>">
      <%= item.name %></a></p>
    </div>

    <ul>
    <% item.posts.each(function(post){ %>
      <li>
        <div class="is-pulled-left">
          <a href="<%- url_for(post.path) %>">
          <%= post.title %>
        </a></div>
        <div class="is-pulled-right has-text-right"><time>
          <%= date(post.date, "DD MMM") %>&nbsp;
        </time></div>
        <div class="is-clearfix"></div>
      </li>
    <% }) %>
    </ul>
  </section>
  <% }) %>
</main>
{{< / highlight >}}

__.__

#### Render: Browser

Open in your favorite browser.

* <http://localhost:4000/tags.html>

![Hexo: Custom Page: Tag List with Tree][image-ss-03-browser-tags-tree]

It is still simple, but pretty enough.

#### The Loop

> How Does It Works?

The range loop, is consist of inner and outer loop as below:

{{< highlight javascript >}}
  <% site.tags.each(function(item){ %>
    ...
    <%= item.name %>
    ...
    <% item.posts.each(function(post){ %>
      ...
      <%= post.title %>
      ...
    <% }) %>
    ...
  <% }) %>
{{< / highlight >}}

-- -- --

### 3: Categories

The same method applied to categories.

Does it looks like repetitive, copy paste the same code?
Don't worry it just temporary.
We are going to refactor in the next section.

#### Layout: EJS Categories List with Tree

* <code>themes/tutor-03/layout/kind/categories.ejs</code>
  : [gitlab.com/.../layout/kind/categories.ejs][tutor-layout-cats]

{{< highlight html >}}
<main role="main" 
      class="column is-full box-deco has-background-white">

  <section class="section">
    <h1 class="title is-4"><%= config.author %></h1>
    <h2 class="subtitle is-4"><%= config.subtitle %></h2>

    <h3 class="title is-3"><%= page.title %></h3>
  </section>

  <% site.categories.each(function(item){ %>
  <section class="section box">
    <div id="<%= item.name %>">
      <p><%= __('category') %>: 
        <a href="<%- url_for(item.path) %>"
           class="button is-small">
        <%= item.name %></a>
      </p>
    </div>

    <ul>
    <% item.posts.each(function(post){ %>
      <li>
        <div class="is-pulled-left">
          <a href="<%- url_for(post.path) %>">
          <%= post.title %>
        </a></div>
        <div class="is-pulled-right has-text-right"><time>
          <%= date(post.date, "DD MMM") %>&nbsp;
        </time></div>
        <div class="is-clearfix"></div>
      </li>
    <% }) %>
    </ul>
  </section>
  <% }) %>
</main>
{{< / highlight >}}

__.__

#### Content: Categories

again, custom page is not enough.
We still need content to make this works.

* <code>source/categories.md</code>
  : [gitlab.com/.../source/categories.md][tutor-content-cats]

{{< highlight yaml >}}
---
layout: kind/categories
title: Categories
---
{{< / highlight >}}

#### Render: Browser

Open in your favorite browser.

* <http://localhost:4000/cats.html>

![Hexo: Custom Page: Category List with Tree][image-ss-03-browser-cats-tree]

-- -- --

### What is Next ?

There are, some interesting topic about <code>Custom Page in Hexo</code>.
Consider continue reading [ [Hexo - Custom Page Refactored][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2019/05/12/hexo-ejs-custom-page-layout/

[image-ss-03-source]:       {{< assets-ssg >}}/2019/05/hexo-ejs-bulma-tutor-03.zip

[image-ss-03-browser-landing]:   {{< assets-ssg >}}/2019/05/38-browser-landing.png
[image-ss-03-browser-tags-only]: {{< assets-ssg >}}/2019/05/38-browser-tags-only.png
[image-ss-03-browser-tags-tree]: {{< assets-ssg >}}/2019/05/38-browser-tags-tree.png
[image-ss-03-browser-cats-tree]: {{< assets-ssg >}}/2019/05/38-browser-cats-tree.png

[tutor-layout-landing]: {{< tutor-hexo-bulma >}}/themes/tutor-03/layout/kind/landing.ejs
[tutor-layout-tags]:    {{< tutor-hexo-bulma >}}/themes/tutor-03/layout/kind/tags.ejs
[tutor-layout-cats]:    {{< tutor-hexo-bulma >}}/themes/tutor-03/layout/kind/categories.ejs

[tutor-content-index]:  {{< tutor-hexo-bulma >}}/source/index.html
[tutor-content-tags]:   {{< tutor-hexo-bulma >}}/source/tags.md
[tutor-content-cats]:   {{< tutor-hexo-bulma >}}/source/categories.md
