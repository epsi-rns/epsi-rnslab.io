---
type   : post
title  : "Hexo - EJS - Custom Page Layout"
date   : 2019-05-12T09:17:35+07:00
slug   : hexo-ejs-custom-page-layout
categories: [ssg]
tags      : [hexo]
keywords  : [static site, custom theme, ejs, layout, refactoring terms, responsive]
author : epsi
opengraph:
  image: assets/site/images/topics/hexo-bulma-markdown.png

toc    : "toc-2019-05-hexo-bulma-step"

excerpt:
  Building Hexo Step by step, with Bulma as stylesheet frontend.
  Refactoring custom page, making more layout. 
  And produce nice looks, for categories and tags.

---

### Preface

> Goal: Refactoring custom page, for categories and tags.

Making more layout is fun, especially in refactoring process.

#### Source Code

You can download the source code of this article here.

* [hexo-ejs-bulma-tutor-04.zip][image-ss-04-source]

Extract and run on CLI:

{{< highlight html >}}
$ npm install
{{< / highlight >}}

-- -- --

### 1: Prepare

To achieve pretty looks, we require this iconic Fontawesome.
So I decide to separate it in different tutorial.

#### Theme

This preparation is required.

1. Create theme <code>themes/tutor-04</code>.

You can use source code above, oor from gitlab repository or,
you can also copy paste from <code>themes/tutor-03</code>.

2. Edit configuration <code>_config.yml</code>, and save.

{{< highlight yaml >}}
# Extensions
theme: tutor-04
{{< / highlight >}}

#### Vendors: FontAwesome

Prepare Fontawesome in vendor directory.

{{< highlight bash >}}
$ tree -d themes/tutor-04/sass/ -L 2
themes/tutor-04/sass/
├── css
└── vendors
    ├── bulma
    └── font-awesome-5
{{< / highlight >}}

![Hexo: SASS Tree][image-ss-04-tree-sass]

#### Assets: Custom Stylesheet

And also custom stylesheet.

* <code>themes/tutor-04/sass/css/fontawesome.scss</code>
  : [gitlab.com/.../sass/css/fontawesome.scss][tutor-s-css-fontawesome]

{{< highlight css >}}
// Font Awesome
$fa-font-path:        "https://use.fontawesome.com/releases/v5.2.0/webfonts";

// Import partials from `sass_dir` (defaults to `_sass`)
@import "vendors/font-awesome-5/fontawesome";
@import "vendors/font-awesome-5/solid";
@import "vendors/font-awesome-5/brands";
{{< / highlight >}}

Do not forget to compile the SASS,
with any tools that works for you.

{{< highlight css >}}
$ dart-sass --watch -I sass sass/css:source/css/ --style=compressed --no-source-map
{{< / highlight >}}

-- -- --

#### 2: Header Menu

Real world website building require real links.
Consider complete the previous header,
to test this <code>FontAwesome</code>

#### Layout: EJS Header

This is a looong Header, with logo image.

* <code>themes/tutor-04/layout/site/header.ejs</code>
  : [gitlab.com/.../layout/site/header.ejs][tutor-l-site-header]

{{< highlight html >}}
<nav role="navigation" aria-label="main navigation"
     class="navbar is-fixed-top is-white maxwidth header-deco"
     id="navbar-vue-app">
  <div class="navbar-brand">
    ...

      <div class="navbar-item has-dropdown is-hoverable">
        <a class="navbar-link">
          Archives
        </a>

        <div class="navbar-dropdown">
          <a class="navbar-item"
             href="<%- url_for("/tags.html") %>">
            <span class="fa fa-tags"></span>&nbsp;By Tags
          </a>
          <a class="navbar-item"
            href="<%- url_for("/categories.html") %>">
            <span class="fa fa-folder"></span>&nbsp;By Category
          </a>
          <hr class="navbar-divider">
          <a class="navbar-item"
            href="<%- url_for("/archives/") %>">
            <span class="fa fa-calendar"></span>&nbsp;By Date
          </a>
        </div>
      </div>

    ...
  </div>

  ...
</nav>
{{< / highlight >}}

#### Render: Browser

Open in your favorite browser.

* In any page

![Hexo: Header Menu with FontAwesome][image-ss-04-header-menu]

-- -- --

### 3: Refactoring Terms

Now it is time to go back to custom page as our primarily main focus.
I do like to modularized long code, so it looks more human readable.

#### Artefacts

Our terms require at least four artefacts.

* <code>themes/tutor-04/layout/terms/title.ejs</code>

* <code>themes/tutor-04/layout/terms/list.ejs</code>

* <code>themes/tutor-04/layout/terms/buttons.ejs</code>

* <code>themes/tutor-04/layout/terms/post-items.ejs</code>

We need to put them in <code>layout/terms</code> directory.

#### Terms: Step One: List

Our first attempt of refactoring is as below:

* <code>themes/tutor-03/layout/kind/tags.ejs</code>

{{< highlight javascript >}}
<main role="main" 
      class="column is-full box-deco has-background-white">
  <%- partial('terms/title') %>
  <%- partial('terms/list', 
        {terms: site.tags, taxonomyname: 'Tag' }) %>
</main>
{{< / highlight >}}

While the <code>title.ejs</code> is simply moving that section.

* <code>themes/tutor-04/layout/terms/title.ejs</code>
  : [gitlab.com/.../layout/terms/title.ejs][tutor-terms-title]

{{< highlight html >}}
  <section class="section">
    <h1 class="title is-4"><%= config.author %></h1>
    <h2 class="subtitle is-4"><%= config.subtitle %></h2>

    <h3 class="title is-3"><%= page.title %></h3>
  </section>
{{< / highlight >}}

And the <code>title.ejs</code> is simply moving that section.

* <code>themes/tutor-04/layout/terms/list.ejs</code>

{{< highlight html >}}
  <% terms.each(function(item){ %>
  <section class="section box">
    <div id="<%= item.name %>" class ="anchor-target">
      <p>
        <span class="fa fa-folder"></span>&nbsp;<%= taxonomyname %>: 
        <a href="<%- url_for(item.path) %>"><%= item.name %></a>
      </p>
    </div>

    <ul>
    <% item.posts.each(function(post){ %>
      <li>
        <div class="is-pulled-left">
          <a href="<%- url_for(post.path) %>">
          <%= post.title %>
        </a></div>
        <div class="is-pulled-right has-text-right"><time>
          <%= date(post.date, "DD MMM") %>&nbsp;
        </time></div>
        <div class="is-clearfix"></div>
      </li>
    <% }) %>
    </ul>
  </section>
  <% }) %>
{{< / highlight >}}

Consider render our view in browser:

![Hexo: Refactoring Terms: Tags Step One][image-ss-04-browser-tags-one]

#### Terms: Step Two: Post Item

The thing is, the code above is still too long.
We need more refactoring.

* <code>themes/tutor-04/layout/terms/list.ejs</code>
  : [gitlab.com/.../layout/terms/list.ejs][tutor-terms-list]

{{< highlight html >}}
  <% terms.each(function(item){ %>
  <section class="section box">
    <div id="<%= item.name %>" class ="anchor-target">
      <p>
        <span class="fa fa-folder"></span>&nbsp;<%= taxonomyname %>: 
        <a href="<%- url_for(item.path) %>"><%= item.name %></a>
      </p>
    </div>

    <div class="archive-list">
    <% item.posts.each(function(post){ %>
      <%- partial('terms/post-item', {post: post}) %>
    <% }) %>
    </div>
  </section>
  <% }) %>
{{< / highlight >}}

I make different design for the <code>post-item.ejs</code>,
For practical reason, this template is using,
<code>div</code> instead of <code>ul</code>.

* <code>themes/tutor-04/layout/terms/post-item.ejs</code>
  : [gitlab.com/.../layout/terms/post-item.ejs][tutor-terms-post-item]

{{< highlight html >}}
      <div class="archive-item meta-item">
        <div class="meta_link has-text-right">
          <time class="meta_time is-pulled-right"
                datetime="<%= date(post.date, "YYYY-MM-DD[T]HH:mm:ss.SSS") %>">
            <%= date(post.date, "MMM DD, YYYY") %>
            &nbsp;<span class="fa fa-calendar"></span>
          </time>
        </div>
        <div class="is-pulled-left">
          <a href="<%- url_for(post.path) %>">
            <%= post.title %>
          </a>
        </div>
        <div class="is-clearfix"></div>
      </div>
{{< / highlight >}}

Consider render our view in browser:

![Hexo: Refactoring Terms: Tags Step Two][image-ss-04-browser-tags-two]

#### Tag Buttons

Most CSS frameworks equipped with nice tag button.
This is very useful in our case.

Consider use only buttons in our tags layout as below:

* <code>themes/tutor-03/layout/kind/tags.ejs</code>

{{< highlight html >}}
<main role="main" 
      class="column is-full box-deco has-background-white">
  <%- partial('terms/buttons', {terms: site.tags}) %>
</main>
{{< / highlight >}}

And the button would be

* <code>themes/tutor-03/layout/terms/buttons.ejs</code>
  : [gitlab.com/.../layout/terms/buttons.ejs][tutor-terms-buttons]

{{< highlight html >}}
  <div class="field is-grouped is-grouped-multiline">
    <% terms.each(function(item){ %>
    <div class="tags has-addons">
      <a href="<%- url_for(item.path) %>">
        <div class="tag is-light"><%= item.name %>
        </div><div class="tag is-dark"><%= item.posts.length %></div>
      </a>
    </div>
    &nbsp;
    <% }) %>
    <div class="tags dummy"></div>
  </div>
{{< / highlight >}}

Consider render our view in browser:

![Hexo: Refactoring Terms: Button of Tags][image-ss-04-browser-tags-btn]

#### Final: Categories and Tags

Our final tags page would be like these below.

* <code>themes/tutor-03/layout/kind/tags.ejs</code>
  : [gitlab.com/.../layout/kind/tags.ejs][tutor-layout-tags]

{{< highlight javascript >}}
<main role="main" 
      class="column is-full box-deco has-background-white">
  <%- partial('terms/buttons', {terms: site.tags}) %>
  <%- partial('terms/title') %>
  <%- partial('terms/list', 
        {terms: site.tags, taxonomyname: __('tag')}) %>
</main>
{{< / highlight >}}

* <http://localhost:4000/tags.html>

__.__

And so is the final categorie pages.

* <code>themes/tutor-03/layout/kind/categories.ejs</code>
  : [gitlab.com/.../layout/kind/categories.ejs][tutor-layout-cats]

{{< highlight javascript >}}
<main role="main" 
      class="column is-full box-deco has-background-white">
  <%- partial('terms/buttons', {terms: site.categories}) %>
  <%- partial('terms/title') %>
  <%- partial('terms/list', 
        {terms: site.categories, taxonomyname: __('category')}) %>
</main>
{{< / highlight >}}

* <http://localhost:4000/categories.html>
__.__

Consider render the category page in browser:

![Hexo: Refactoring Terms: Categories][image-ss-04-browser-categories]

-- -- --

### 4: Responsive

Refactoring make our code easier to modify.
Suppose that we want to make a slight design change,
we only need to change specific part.

#### Layout: EJS List

Responsive classes on outer element,
and panel to wrap inner element.

* <code>themes/tutor-04/layout/terms/list-responsive.ejs</code>
  : [gitlab.com/.../layout/terms/list-responsive.ejs][tutor-terms-responsive]

{{< highlight html >}}
  <section class="columns is-multiline" id="archive">
  <% terms.each(function(item){ %>
    <div class="column is-full-mobile 
                is-half-tablet is-one-third-widescreen">

      <section class="panel is-light">
        <div class="panel-header" id="<%= item.name %>">
          <p><%= taxonomyname %>: 
             <a href="<%- url_for(item.path) %>">
             <%= item.name %></a></p>
          <span class="fa fa-folder"></span>
        </div>
        <div class="panel-body has-background-white">

          <div class="archive-list archive-p3">
          <% item.posts.each(function(post){ %>
            <%- partial('terms/post-item', {post: post}) %>
          <% }) %>
          </div>

        </div>

      </section>

    </div>
  <% }) %>
  </section>
{{< / highlight >}}

#### How Does It Works?

These bulma classes below do all the hardworks:

* <code>is-full-mobile</code>

* <code>is-half-tablet</code>

* <code>is-one-third-widescreen</code>

{{< highlight html >}}
  <section class="columns is-multiline" id="archive">
  <% terms.each(function(item){ %>
    <div class="column
                is-full-mobile
                is-half-tablet
                is-one-third-widescreen">
       ...
    </div>
  <% }) %>
  </section>
{{< / highlight >}}

Consider have a look at difference for each screen size.

#### Screen is Mobile: Full

You can test this in your smartphone.

![Hexo: Responsive: is-full-mobile][image-ss-04-browser-full]

#### Screen is Tablet: Half

Consider have a wider screen size.
Such as landscape tablet, or regular PC monitor.

![Hexo: Responsive: is-half-tablet][image-ss-04-browser-half]

#### Screen is Wide: One Third

This take a wide screen flat monitor.

![Hexo: Responsive: is-one-third-widescreen][image-ss-04-browser-third]

I think that's all about this article.

-- -- --

### What is Next ?

There are, some interesting topic about <code>Widget in Hexo</code>. 
Consider continue reading [ [Hexo - Widget][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2019/05/13/hexo-ejs-section-list-layout/

[image-ss-04-source]:       {{< assets-ssg >}}/2019/05/hexo-ejs-bulma-tutor-04.zip

[tutor-s-css-fontawesome]:  {{< tutor-hexo-bulma >}}/themes/tutor-04/sass/css/fontawesome.scss

[tutor-l-site-header]:      {{< tutor-hexo-bulma >}}/themes/tutor-04/layout/site/header.ejs

[image-ss-04-tree-sass]:    {{< assets-ssg >}}/2019/05/41-tree-sass.png
[image-ss-04-header-menu]:  {{< assets-ssg >}}/2019/05/41-header-fontawesome.png

[image-ss-04-browser-tags-one]:  {{< assets-ssg >}}/2019/05/41-browser-tags-step-one.png
[image-ss-04-browser-tags-two]:  {{< assets-ssg >}}/2019/05/41-browser-tags-step-two.png
[image-ss-04-browser-tags-btn]:  {{< assets-ssg >}}/2019/05/41-browser-tags-buttons.png
[image-ss-04-browser-categories]:{{< assets-ssg >}}/2019/05/41-browser-categories.png

[image-ss-04-browser-full]:      {{< assets-ssg >}}/2019/05/41-browser-is-full-mobile.png
[image-ss-04-browser-half]:      {{< assets-ssg >}}/2019/05/41-browser-is-half-tablet.png
[image-ss-04-browser-third]:     {{< assets-ssg >}}/2019/05/41-browser-is-one-third-widescreen.png

[tutor-layout-tags]:    {{< tutor-hexo-bulma >}}/themes/tutor-04/layout/kind/tags.ejs
[tutor-layout-cats]:    {{< tutor-hexo-bulma >}}/themes/tutor-04/layout/kind/categories.ejs

[tutor-terms-list]:             {{< tutor-hexo-bulma >}}/themes/tutor-04/layout/terms/list.ejs
[tutor-terms-title]:            {{< tutor-hexo-bulma >}}/themes/tutor-04/layout/terms/title.ejs
[tutor-terms-buttons]:          {{< tutor-hexo-bulma >}}/themes/tutor-04/layout/terms/buttons.ejs
[tutor-terms-post-item]:        {{< tutor-hexo-bulma >}}/themes/tutor-04/layout/terms/post-item.ejs
[tutor-terms-responsive]:       {{< tutor-hexo-bulma >}}/themes/tutor-04/layout/terms/list-responsive.ejs
