---
type   : post
title  : "Hexo - EJS - Section List Layout"
date   : 2019-05-13T09:17:35+07:00
slug   : hexo-ejs-section-list-layout
categories: [ssg]
tags      : [hexo]
keywords  : [static site, custom theme, ejs, layout, blog index, grouping query, responsive]
author : epsi
opengraph:
  image: assets/site/images/topics/hexo-bulma-markdown.png

toc    : "toc-2019-05-hexo-bulma-step"

excerpt:
  Building Hexo Step by step, with Bulma as stylesheet frontend.
  Refactoring special page, Index, Archive, Category, and Tag.

---

### Preface

> Goal: Refactoring special page, Index, Archive, Category, and Tag.

Hexo has this four special page sections: Index, Archive, Category, Tag.
The last three has very parts in common. So we can redefine as:

* 1: Blog Index

* 2: Section List

#### Source Code

You can download the source code of this article here.

* [hexo-ejs-bulma-tutor-04.zip][image-ss-04-source]

Extract and run on CLI:

{{< highlight html >}}
$ npm install
{{< / highlight >}}

-- -- --

### 1: Blog Index

> Special Layout: Index

Remember our last blog index.
Let me refresh our setup.

#### Configuration

We need to setup permalink and stuff.
Add these lines below our configuration.

* <code>_config.yml</code>
  : [gitlab.com/.../_config.yml][tutor-hexo-config]

{{< highlight yaml >}}
# Home page setting
index_generator:
  path: '/blog'
  order_by: -date
{{< / highlight >}}

#### Layout: EJS Index: Redesigned

We can rewrite blog template.
This time using my own custom class <code>meta-item blog-item</code>.

* <code>themes/tutor-04/layout/index.ejs</code>

{{< highlight html >}}
<main role="main" 
      class="column is-full box-deco has-background-white">
  <section class="section">
    <h1 class="title is-4"><%= config.author %></h1>
    <h2 class="subtitle is-4"><%= config.subtitle %></h2>
  </section>

  <div class="post-list">
  <% page.posts.each(function(post){ %>
    <section class="section" id="archive">
    <div class="meta-item blog-item">
      <a href="<%- url_for(post.path) %>"><%= post.title %></a>
    </div>
    </section>
  <% }) %>
  </div>

</main>
{{< / highlight >}}

#### Render: Browser

Open in your favorite browser.

* <http://localhost:4000/blog/>

![Hexo: Blog Index Redesign][image-ss-04-b-index-01]

#### Layout: EJS Index: Refactored

Things get complex when we design the inner parts.
This why refactoring takes place.

* <code>themes/tutor-04/layout/index.ejs</code>
  : [gitlab.com/.../layout/index.ejs][tutor-layout-index]

{{< highlight html >}}
<main role="main" 
      class="column is-full box-deco has-background-white">
  <section class="section">
    <h1 class="title is-4"><%= config.author %></h1>
    <h2 class="subtitle is-4"><%= config.subtitle %></h2>
  </section>

  <div class="post-list">
  <% page.posts.each(function(post){ %>
    <section class="section" id="archive">
      <%- partial('summary/blog-item', {post: post}) %>
    </section>
  <% }) %>
  </div>

</main>
{{< / highlight >}}

#### Layout: EJS Blog Item

For each item, processed in its own template.

* <code>themes/tutor-04/layout/summary/blog-item.ejs</code>
  : [gitlab.com/.../layout/summary/blog-item.ejs][tutor-layout-blog]

{{< highlight html >}}
    <div class="meta-item blog-item">

      <strong><a class="meta_link" 
        href="<%- url_for(post.path) %>"><%= post.title %>
      </strong></a>

      <div class="meta">
        <div class="meta_time is-pulled-left">
          <i class="fa fa-calendar"></i>
          <time datetime="<%= date(post.date, "YYYY-MM-DD[T]HH:mm:ss.SSS") %>">
          <%= date(post.date, "MMM DD, YYYY") %></time>
        </div>      
        <div class="is-pulled-right" id="meta_tags">
          <% post.tags.forEach(function(tag){ %>
            <a href="<%- url_for(tag.path) %>">
              <span class="tag is-dark">
                <span class="fa fa-tag"></span>
                &nbsp;<%= tag.name %></span></a>
          <% }) %>
        </div>
      </div> 

      <div class="is-clearfix"></div>
      
      <div>
        <% if(post.excerpt) { %>
        <p><%- post.excerpt %></p>
        <% } %>
      </div>

      <div class="read-more is-light">
        <a href="<%- url_for(post.path) %>" 
           class="button is-dark is-small">Read More&nbsp;</a>
      </div>

    </div>
{{< / highlight >}}

Basically, just moving the complex part somewhere else.

#### Render: Browser

Open in your favorite browser.

* <http://localhost:4000/blog/>

![Hexo: Blog Index Refactored][image-ss-04-b-index-02]

-- -- --

### 2: Simple Section

> Special Layout: Archive, Category, Tag

What can be applied to one layout, can be applied to another.
We are going to start with <code>archive</code> layout.

#### Layout: EJS Archive

Consider slight modification of our previous layout.

* <code>themes/tutor-04/layout/archive.ejs</code>
  : [gitlab.com/.../layout/archive.ejs][tutor-layout-archive]

{{< highlight javascript >}}
<% 
  var section_title = __('archive');

  if (is_month()){
    section_title += ': ' + page.year + '/' + page.month;
  } else if (is_year()){
    section_title += ': ' + page.year;
  }
%>
<%-
  partial('summary/post-list-simple', {
    section_title: section_title
  })
%>
{{< / highlight >}}

__.__

#### Layout: EJS Post List: Simple

* <code>themes/tutor-04/layout/summary/post-list-simple.ejs</code>
  : [gitlab.com/.../layout/summary/post-list-simple.ejs][tutor-layout-simple]

{{< highlight html >}}
<main role="main" 
      class="column is-full box-deco has-background-white">
  <section class="section">
    <h4 class="title is-4"><%= section_title %></h4>
  </section>

  <section class="section" id="archive">
  <% page.posts.each(function(post){ %>
    <div class="archive-item">
      <div class="is-pulled-left"><a href="<%- url_for(post.path) %>">
        <%= post.title %>
      </a></div>
      <div class="is-pulled-right has-text-right"><time>
          <%= date(post.date, "DD MMM") %>&nbsp;
          <span class="fa fa-calendar"></span>
      </time></div>
      <div class="is-clearfix"></div>
    </div>
  <% }) %>
  </section>
</main>
{{< / highlight >}}

#### Render: Browser

Open in your favorite browser.
You should see, the modified version of archive page.

* <http://localhost:4000/archives/>

![Hexo Bulma: Archive Page: Simple][image-ss-04-l-archive-s]

#### The Loop

> How Does It Works?

The simplified range loop, is as below:

{{< highlight javascript >}}
  <% page.posts.each(function(post){ %>
    ...
    <%= post.title %>
    ...
  <% }) %>
{{< / highlight >}}

-- -- --

### 3: Grouping Query: By Year

How about grouping the list above by year?
Thi would be complicated in other templating engine.
Luckily we are using embedded javascript.

#### Data Structure

Suppose we have an array of posts:

{{< highlight bash >}}
.
├── [post 01]
│   ├── title
│   ├── name
│   ├── ...
│   └── date
└── [post 01]
    ├── title
    ├── name
    ├── ...
    └── date
{{< / highlight >}}

First we have to insert year as field in every item.
So we have something like this:

{{< highlight bash >}}
.
├── [post 01]
│   ├── title
│   ├── name
│   ├── ...
│   ├── date
│   └── year
└── [post 01]
    ├── title
    ├── name
    ├── ...
    ├── date
    └── year
{{< / highlight >}}

We can achieve this by using mapping in javascript.

{{< highlight javascript >}}
  // grouping by date

  const posts = page.posts.map(post => ({ 
    ...post, year: date(post.date, "YYYY")
  }));
{{< / highlight >}}

As we already have year field,
we can group this data structure by year.

{{< highlight javascript >}}
  const postsByYear = groupBy(posts, 'year');
{{< / highlight >}}

While the <code>groupBy</code> function,
is provided in mozilla site:

* [https://developer.mozilla.org/.../reduce][mozilla-reduce]

#### Javascript: EJS Post List: By Year

The complete javascript source is as below:

{{< highlight javascript >}}
  // Data Model

  // https://developer.mozilla.org/.../Array/reduce
  
  function groupBy(objectArray, property) {
    return objectArray.reduce(function (acc, obj) {
      var key = obj[property];
      if (!acc[key]) {
        acc[key] = [];
      }
      acc[key].push(obj);
      return acc;
    }, {});
  }

  // grouping by date

  const posts = page.posts.map(post => ({ 
    ...post, year: date(post.date, "YYYY")
  }));

  const postsByYear = groupBy(posts, 'year');
{{< / highlight >}}

#### Layout: EJS Post List: By Year

Pour down the javascript above,
between the tag below <code><% ... _%></code>:

* <code>themes/tutor-04/layout/summary/post-list-by-year.ejs</code>
  : [gitlab.com/.../layout/summary/post-list-by-year.ejs][tutor-layout-by-year]

{{< highlight html >}}
<%
  ...
_%>
<main role="main" 
      class="column is-full box-deco has-background-white">
  <section class="section">
    <h4 class="title is-4"><%= section_title %></h4>
  </section>

  <% 
    const postsSorted = Object.keys(postsByYear).sort(date, -1);
    postsSorted.forEach(function (year){ 
  %>
  <section class="section box">
    <div class ="anchor-target archive-year" 
         id="<%= year %>"><%= year %></div>

    <div class="archive-list archive-p3">
      <% postsByYear[year].forEach(function(post){ %>
      <div class="archive-item">
        <div class="is-pulled-left"><a href="<%- url_for(post.path) %>">
          <%= post.title %>
        </a></div>
        <div class="is-pulled-right has-text-right"><time>
            <%= date(post.date, "DD MMM") %>&nbsp;
            <span class="fas fa-calendar"></span>
        </time></div>
        <div class="is-clearfix"></div>
      </div>
      <% }) %>
    </div>
  </section>
  <% }) %>
</main>
{{< / highlight >}}

#### Render: Browser

Open in your favorite browser.
You should see, the archive page grouped by year.

* <http://localhost:4000/archives/>

![Hexo Bulma: Archive Page: By Year][image-ss-04-l-archive-y]

#### The Loop

> How Does It Works?

The simplified range loop, is as below:

{{< highlight javascript >}}
  <% 
    const postsSorted = Object.keys(postsByYear).sort(date, -1);
    postsSorted.forEach(function (year){ 
  %>
    ...
    <%= year %>
    ...
    <% postsByYear[year].forEach(function(post){ %>
      ...
      <%= post.title %>
      ...
    <% }) %>
    ...
  <% }) %>
{{< / highlight >}}

While <code>postsByYear</code> is:

{{< highlight javascript >}}
  const postsByYear = groupBy(posts, 'year');
{{< / highlight >}}

I hope that, this explanation is clear.

#### Update

For unknown reason. Hexo break break my logic.
This `.sort(date, -1)` is no longer working.

The solution is to use `.sort(date, -1).reverse()`.

-- -- --

### 4: Grouping Query: By Month

How about grouping the list above by month?
How complex this would be?

#### Data Structure

Again, suppose we have an array of posts:

{{< highlight bash >}}
.
├── [post 01]
│   ├── title
│   ├── name
│   ├── ...
│   └── date
└── [post 01]
    ├── title
    ├── name
    ├── ...
    └── date
{{< / highlight >}}

First we have to insert both month and year as field in every item.
Wait... why both year and month?
Because we have two loops.
The outer loop would process by year,
and the inner one wouyld process by month.

We have should have something like this:

{{< highlight bash >}}
.
├── [post 01]
│   ├── title
│   ├── name
│   ├── ...
│   ├── date
│   ├── year
│   └── month
└── [post 01]
    ├── title
    ├── name
    ├── ...
    ├── date
    ├── year
    └── month
{{< / highlight >}}

We can achieve this by using mapping in javascript.
Actually this is almost exactly the same as our previous example.
I guess, this is not so complicated, as I thought it was.

{{< highlight javascript >}}
  const posts = page.posts.map(post => ({ 
    ...post,
    year: date(post.date, "YYYY"),
    month: date(post.date, "MMMM")
  }));
{{< / highlight >}}

As we already have year field,
we can group this data structure by year.

{{< highlight javascript >}}
  const postsByYear = groupBy(posts, 'year');
{{< / highlight >}}

Wait..?
Where is the group by month?
Well.... this will be processed in inner loop later.

#### Javascript: EJS Post List: By Year

The complete javascript soource is as below:

{{< highlight javascript >}}
  // Data Model

  // https://developer.mozilla.org/.../Array/reduce
  
  function groupBy(objectArray, property) {
    return objectArray.reduce(function (acc, obj) {
      var key = obj[property];
      if (!acc[key]) {
        acc[key] = [];
      }
      acc[key].push(obj);
      return acc;
    }, {});
  }

  // grouping by date

  const posts = page.posts.map(post => ({ 
    ...post,
    year: date(post.date, "YYYY"),
    month: date(post.date, "MMMM")
  }));

  const postsByYear = groupBy(posts, 'year');
  
  // additional
  
  const yearToday = date(new Date(), 'YYYY');
{{< / highlight >}}

#### Layout: EJS Post List: By Month

Pour down the javascript above,
between the tag below <code><% ... _%></code>:

* <code>themes/tutor-04/layout/summary/post-list-by-month.ejs</code>
  : [gitlab.com/.../layout/summary/post-list-by-month.ejs][tutor-layout-by-month]

{{< highlight html >}}
<%
  ...
_%>

<main role="main" 
      class="column is-full box-deco has-background-white">
  <section class="section">
    <h4 class="title is-4"><%= section_title %></h4>
  </section>

  <% 
    const postsSorted = Object.keys(postsByYear).sort(date, -1);
    postsSorted.forEach(function (year){ 
  %>
  <section class="section box">
    <div class ="anchor-target archive-year" id="<%= year %>">
       <% if (year == yearToday) {%>
         This year's posts (<%= year %>)
       <% } else { %>
         <%= year %>
       <% } %>
    </div>
    <%
      const postsByMonth = groupBy(postsByYear[year], 'month');
      Object.keys(postsByMonth).forEach(function (month){
    %>
    <div class="archive-p4">
      <div class ="archive-month" 
           id="<%= year %>-<%= month %>">
           <%= month %> <%= year %></div>

      <div class="archive-list archive-p3">
        <% postsByMonth[month].forEach(function(post){ %>
        <div class="archive-item">
          <div class="is-pulled-left"><a href="<%- url_for(post.path) %>">
            <%= post.title %>
          </a></div>
          <div class="is-pulled-right has-text-right"><time>
              <%= date(post.date, "DD MMM") %>&nbsp;
              <span class="fas fa-calendar"></span>
          </time></div>
          <div class="is-clearfix"></div>
        </div>
        <% }) %>
      </div>

    </div>
    <% }) %>
  </section>
  <% }) %>
</main>
{{< / highlight >}}

#### Render: Browser

Open in your favorite browser.
You should see, the archive page grouped by month.

* <http://localhost:4000/archives/>

![Hexo Bulma: Archive Page: By Month][image-ss-04-l-archive-m]

#### The Loop

> How Does It Works?

The simplified range loop, is as below:

{{< highlight javascript >}}
  <% 
    const postsSorted = Object.keys(postsByYear).sort(date, -1);
    postsSorted.forEach(function (year){
  %>
    ...
    <%= year %>
    <%
      const postsByMonth = groupBy(postsByYear[year], 'month');
      Object.keys(postsByMonth).forEach(function (month){
    %>
      ...
      <%= month %>
      ...
      <% postsByMonth[month].forEach(function(post){ %>
        ...
        <%= post.title %>
        ...
      <% }) %>
      ...
    <% }) %>
    ...
  <% }) %>
{{< / highlight >}}

#### Update

For unknown reason. Hexo break break my logic.
This `.sort(date, -1)` is no longer working.

The solution is to use `.sort(date, -1).reverse()`.

-- -- --

### 5: Responsive

Doing responsive is just a matter of changine view,
without altering the javascript data model.

#### Layout: EJS Post List: Responsive

Pour down the javascript above,
between the tag below <code><% ... _%></code>:

* <code>themes/tutor-04/layout/summary/post-list-responsive.ejs</code>
  : [gitlab.com/.../layout/summary/post-list-responsive.ejs][tutor-layout-responsive]

{{< highlight html >}}
<%
  ...
_%>
<main role="main" 
      class="column is-full box-deco has-background-white">
  <section class="section">
    <h4 class="title is-4"><%= section_title %></h4>
  </section>

  <% 
    const postsSorted = Object.keys(postsByYear).sort(date, -1);
    postsSorted.forEach(function (year){
  %>
  <section class="section">

  <div class ="anchor-target archive-year" id="<%= year %>">
     <% if (year == yearToday) {%>
       This year's posts (<%= year %>)
     <% } else { %>
       <%= year %>
     <% } %>
  </div>

  <div class="columns is-multiline ">
    <%
      const postsByMonth = groupBy(postsByYear[year], 'month');
      Object.keys(postsByMonth).forEach(function (month){
    %>

    <div class="column is-full-mobile 
                is-half-tablet is-one-third-widescreen">

      <section class="panel is-light">
        <div class="panel-header" id="<%= year %>-<%= month %>">
          <p><%= month %> <%= year %></p>
          <span class="fa fa-archive"></span>
        </div>
        <div class="panel-body has-background-white">

          <div class="archive-list archive-p3">
            <% postsByMonth[month].forEach(function(post){ %>
            <div class="archive-item">
              <div class="is-pulled-left"><a href="<%- url_for(post.path) %>">
                <%= post.title %>
              </a></div>
              <div class="is-pulled-right has-text-right"><time>
                  <%= date(post.date, "DD MMM") %>&nbsp;
                  <span class="fas fa-calendar"></span>
              </time></div>
              <div class="is-clearfix"></div>
            </div>
            <% }) %>
          </div>

        </div>
      </section>

    </div>

    <% }) %>
  </div>
  </section>
  <% }) %>
</main>
{{< / highlight >}}

#### How Does It Works?

These bulma classes below do all the hardworks:

* <code>is-full-mobile</code>

* <code>is-half-tablet</code>

* <code>is-one-third-widescreen</code>

{{< highlight html >}}
  <section class="columns is-multiline" id="archive">
  <% terms.each(function(item){ %>
    <div class="column
                is-full-mobile
                is-half-tablet
                is-one-third-widescreen">
       ...
    </div>
  <% }) %>
  </section>
{{< / highlight >}}

Now we can use them for

* Archive section page,

* Category section page, and 

* Tag section page.

Consider have a look at difference for each screen size.

#### Layout: EJS Archive

* <code>themes/tutor-04/layout/archive.ejs</code>
  : [gitlab.com/.../layout/archive.ejs][tutor-layout-archive]

{{< highlight javascript >}}
<% 
  var section_title = __('archive');

  if (is_month()){
    section_title += ': ' + page.year + '/' + page.month;
  } else if (is_year()){
    section_title += ': ' + page.year;
  }
%>
<%-
  partial('summary/post-list-responsive', {
    section_title: section_title
  })
%>
{{< / highlight >}}

Open in your favorite browser.

* <http://localhost:4000/archives/2018/>

![Hexo: Browser Section Archive][image-ss-04-section-archive]

__.__

#### Layout: EJS Category

* <code>themes/tutor-04/layout/category.ejs</code>
  : [gitlab.com/.../layout/category.ejs][tutor-layout-cat]

{{< highlight javascript >}}
<%- 
  partial('summary/post-list-responsive', {
    section_title: __('category') + ': ' + page.category
  })
%>
{{< / highlight >}}

Open in your favorite browser.

* <http://localhost:4000/categories/lyric/>

![Hexo: Browser Section Category][image-ss-04-section-category]

__.__

#### Layout: EJS Tag

* <code>themes/tutor-04/layout/tag.ejs</code>
  : [gitlab.com/.../layout/tag.ejs][tutor-layout-tag]

{{< highlight javascript >}}
<%- 
  partial('summary/post-list-responsive', {
    section_title: __('tag') + ': ' + page.tag
  })
%>
{{< / highlight >}}

Open in your favorite browser.

* <http://localhost:4000/tags/pop/>

![Hexo: Browser Section Tag][image-ss-04-section-tag]

__.__

I think that's all about this article.

-- -- --

### What is Next ?

There are, some interesting topic about <code>Custom Output in Hexo</code>. 
Consider continue reading [ [Hexo - Custom Output][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2019/05/14/hexo-ejs-custom-output/
[mozilla-reduce]:           https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/reduce

[image-ss-04-source]:       {{< assets-ssg >}}/2019/05/hexo-ejs-bulma-tutor-04.zip

[image-ss-04-b-index-01]:   {{< assets-ssg >}}/2019/05/42-browser-index-redesigned.png
[image-ss-04-b-index-02]:   {{< assets-ssg >}}/2019/05/42-browser-index-refactored.png
[image-ss-04-l-archive-s]:  {{< assets-ssg >}}/2019/05/42-browser-list-simple.png
[image-ss-04-l-archive-y]:  {{< assets-ssg >}}/2019/05/42-browser-list-year.png
[image-ss-04-l-archive-m]:  {{< assets-ssg >}}/2019/05/42-browser-list-month.png

[image-ss-04-section-archive]:   {{< assets-ssg >}}/2019/05/42-section-archive.png
[image-ss-04-section-category]:  {{< assets-ssg >}}/2019/05/42-section-category.png
[image-ss-04-section-tag]:       {{< assets-ssg >}}/2019/05/42-section-tag.png


[tutor-hexo-config]:    {{< tutor-hexo-bulma >}}/_config.yml
[tutor-layout-index]:   {{< tutor-hexo-bulma >}}/themes/tutor-04/layout/index.ejs
[tutor-layout-archive]: {{< tutor-hexo-bulma >}}/themes/tutor-04/layout/archive.ejs
[tutor-layout-cat]:     {{< tutor-hexo-bulma >}}/themes/tutor-04/layout/category.ejs
[tutor-layout-tag]:     {{< tutor-hexo-bulma >}}/themes/tutor-04/layout/tag.ejs

[tutor-layout-blog]:        {{< tutor-hexo-bulma >}}/themes/tutor-04/layout/summary/blog-item.ejs
[tutor-layout-simple]:      {{< tutor-hexo-bulma >}}/themes/tutor-04/layout/summary/post-list-simple.ejs
[tutor-layout-by-year]:     {{< tutor-hexo-bulma >}}/themes/tutor-04/layout/summary/post-list-by-year.ejs
[tutor-layout-by-month]:    {{< tutor-hexo-bulma >}}/themes/tutor-04/layout/summary/post-list-by-month.ejs
[tutor-layout-responsive]:  {{< tutor-hexo-bulma >}}/themes/tutor-04/layout/summary/post-list-responsive.ejs
