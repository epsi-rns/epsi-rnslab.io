---
type   : post
title  : "Hexo - EJS - Custom Content Type"
date   : 2019-05-14T09:17:35+07:00
slug   : hexo-ejs-custom-output
categories: [ssg]
tags      : [hexo]
keywords  : [static site, custom theme, ejs, custom output, json, text, yaml]
author : epsi
opengraph:
  image: assets/site/images/topics/hexo-bulma-markdown.png

toc    : "toc-2019-05-hexo-bulma-step"

excerpt:
  Building Hexo Step by step, with Bulma as stylesheet frontend.
  More about blog archive example, from just html, 
  to custom content output such as json, text and yaml.

---

### Preface

> Goal: Custom content output example: json, text, yaml

There is no such thing as custom output in Hexo.
We cannot just do it natively without plugin.

But there is a trick do it.
These takes a few steps.

#### Source Code

You can download the source code of this article here.

* [hexo-ejs-bulma-tutor-04.zip][image-ss-04-source]

Extract and run on CLI:

{{< highlight html >}}
$ npm install
{{< / highlight >}}

-- -- --

### 1: Inside HTML Template

> This is the ugliest trick.

Supposed you need to extract data from your blog in yaml format,
you can put it inside a <code>pre element</code>.

#### Layout: EJS Archives YAML Inside HTML

* <code>themes/tutor-04/layout/kind/yaml.ejs</code>
  : [gitlab.com/.../layout/kind/yaml.ejs][tutor-layout-yaml]

{{< highlight html >}}
<main role="main" 
     class="column is-full blog-column box-deco has-background-white">
  <article class="blog-post">
    <h1 class="title is-4"><%= config.author %></h1>
    <h2 class="subtitle is-4"><%= config.subtitle %></h2>

<pre>
# Helper for related links
# <%= page.title %>

<% site.posts.each(function(post){ %>
- id: <%= date(post.date, "YYMMDDmm") %>
  title: "<%= post.title %>"
  url: <%= post.path %>
<% }) %>
</pre>

  </article>
</main>
{{< / highlight >}}

#### Content: Archives YAML Inside HTML

It is time apply this kind of custom page layout.
All you need to do is add <code>layout</code> in **frontmatter**.

* <code>source/archives-yaml.html</code>
  : [gitlab.com/.../source/archives-yaml.html][tutor-content-yaml]

{{< highlight yaml >}}
---
layout: kind/yaml
title: Archives
---
{{< / highlight >}}

#### Render: Browser

Open in your favorite browser.

* <http://localhost:4000/archives-yaml.html>

![Hexo Bulma: Archives YAML Inside HTML][image-ss-04-co-yaml]

This will show something similar as below:

{{< highlight yaml >}}
# Helper for related links
# Archives

...

- id: 15051535
  title: "Susan Wong - I Wish You Love"
  url: 2015/05/15/lyrics/natalie-cole-i-wish-you-love/

- id: 18091335
  title: "Norah Jones - Shoot The Moon"
  url: 2018/09/13/lyrics/norah-jones-shoot-the-moon/

...
{{< / highlight >}}

We need to get rid of the HTML tag, and get pure YAML.

-- -- --

### 2: Prepare: Default Template

This preparation is required

#### Layout: EJS Layout

We need to ignore original layout whenever needed.

* <code>themes/tutor-04/layout/layout.ejs</code>
  : [gitlab.com/.../layout/layout.ejs][tutor-layout-layout]

{{< highlight javascript >}}
<%_ 
  var layout='kind/default'

  if ((page.layout=='kind/text') 
  ||  (page.layout=='kind/json')){
    layout=page.layout
  }
_%>
<%- partial(layout) _%>
{{< / highlight >}}

_._

#### Layout: EJS Default

And move the original `layout` to `kind/default`.

* <code>themes/tutor-04/layout/kind/default.ejs</code>
  : [gitlab.com/.../layout/kind/default.ejs][tutor-layout-default]

{{< highlight html >}}
<!DOCTYPE html>
<html lang="en">
<%- partial('site/head') %>
<body>
<%- partial('site/header') %>

  <div class="columns is-8 layout-base maxwidth">
    <%- body %>
  </div>

<%- partial('site/footer') %>
<%- partial('site/scripts') %>
</body>
</html>
{{< / highlight >}}

Your site should work as usual.
Now check your site, if thi default layout is working well.

-- -- --

### 3: Plain Text

We are almost ready to make custom non html output.
But in this step we are still using <code>.html</code> mime type.

#### Layout: EJS Archives YAML Without Markup

* <code>themes/tutor-04/layout/kind/text.ejs</code>
  : [gitlab.com/.../layout/kind/text.ejs][tutor-layout-text]

{{< highlight html >}}
# Helper for related links
# <%= page.title %>

<% site.posts.each(function(post){ %>
- id: <%= date(post.date, "YYMMDDmm") %>
  title: "<%= post.title %>"
  url: <%= post.path %>
<% }) %>
{{< / highlight >}}

#### Content: Archives YAML Without Markup

It is time apply this kind of custom page layout.
All you need to do is add <code>layout</code> in **frontmatter**.

* <code>source/archives-text.html</code>
  : [gitlab.com/.../source/archives-text.html][tutor-content-text]

{{< highlight yaml >}}
---
layout: kind/text
title: Archives
---
{{< / highlight >}}

#### Render: Browser

Open in your favorite browser.

* <http://localhost:4000/archives-text.html>

![Hexo Bulma: Archives YAML Without Markup][image-ss-04-co-text]

Well it still ugly.
Of course, it is still using <code>html</code> mime type.
You have to look at the source.

![Hexo Bulma: Source of Archives YAML][image-ss-04-cos-text]

#### Content: Javascript Mime Type

Alternatively, you can rename <code>source/archives-text.html</code>,
to <code>source/archives-text.js</code>.

This will directly show the text.
But remember, this mime type is javascript, and your content is yaml.

> Still not good!

-- -- --

### 4: JSON

This is the closest thing to custom output so far.
Why not make a custom output in JSON instead of YAML?

#### Layout: EJS Archives JSON as JS Mime

* <code>themes/tutor-04/layout/kind/yaml.ejs</code>
  : [gitlab.com/.../layout/kind/yaml.ejs][tutor-layout-yaml]

{{< highlight javascript >}}
[
<%
  var count = site.posts.length
  var i = 0;
  site.posts.each(function(post){
    i++
_%>
  {
    "id": <%= date(post.date, "YYMMDDmm") %>,
    "title": "<%= post.title %>",
    "url": "<%= post.path %>"
  }<% if (i!=count){%>,<% } %>
<% }) _%>
]
{{< / highlight >}}

_._

#### Content: Archives JSON as JS Mime

It is time apply this kind of custom page layout.
All you need to do is add <code>layout</code> in **frontmatter**.

* <code>source/archives-yaml.html</code>
  : [gitlab.com/.../source/archives-yaml.html][tutor-content-yaml]

{{< highlight yaml >}}
---
layout: kind/json
---
{{< / highlight >}}

#### Render: Browser: Site

Open in your favorite browser.

* <http://localhost:4000/archives-json.js>

![Hexo Bulma: Archives JSON as JS Mime][image-ss-04-co-json]

This will show something similar as below:

{{< highlight json >}}
[
  ...
  {
    "id": 15010108,
    "title": "Dead Poet Society",
    "url": "2015/01/01/quotes/dead-poets-society/"
  },
  {
    "id": 15100308,
    "title": "Every Day",
    "url": "2015/10/03/quotes/every-day/"
  },
  ...
]
{{< / highlight >}}

#### Render: Browser: File

To check the right JSON mime, 
save it somewhere in your computer,
and open it in browser.

* <file:///home/username/tutor-04/source/_data/archives_local.json>

![Hexo Bulma: Archives JSON File][image-ss-04-cof-json]

-- -- --

### 5: Summary

Since we have some more layout kind,
we need to summarize the layout.

{{< highlight bash >}}
$ tree themes/tutor-04/layout/kind 
themes/tutor-04/layout/kind
├── categories.ejs
├── default.ejs
├── json.ejs
├── landing.ejs
├── tags.ejs
├── text.ejs
└── yaml.ejs

0 directories, 7 files
{{< / highlight >}}

![Hexo Layout: Custom Output][image-ss-04-t-kind]

And the related content is

{{< highlight bash >}}
$ ls source/archives-*
source/archives-json.js
source/archives-text.html
source/archives-yaml.html
{{< / highlight >}}

![Hexo Content: Custom Output][image-ss-04-t-content]

-- -- --

### What is Next ?

Consider continue reading [ [Hexo - Widget][local-whats-next] ].
There are, some interesting topic,
about <code>Hexo miscellanous range loop</code>,
such `widget` in a `sidebar`.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2019/05/15/hexo-ejs-widget/

[image-ss-04-source]:   {{< assets-ssg >}}/2019/05/hexo-ejs-bulma-tutor-04.zip

[image-ss-04-co-yaml]:  {{< assets-ssg >}}/2019/05/44-custom-output-yaml.png
[image-ss-04-co-text]:  {{< assets-ssg >}}/2019/05/44-custom-output-text.png
[image-ss-04-cos-text]: {{< assets-ssg >}}/2019/05/44-custom-output-text-source.png
[image-ss-04-co-json]:  {{< assets-ssg >}}/2019/05/44-custom-output-json.png
[image-ss-04-cof-json]: {{< assets-ssg >}}/2019/05/44-custom-output-json-file.png
[image-ss-04-t-kind]:   {{< assets-ssg >}}/2019/05/44-tree-layout-kind.png
[image-ss-04-t-content]:{{< assets-ssg >}}/2019/05/44-tree-content-archives.png

[tutor-data-json]:      {{< tutor-hexo-bulma >}}/source/_data/archives_local.json

[tutor-layout-default]: {{< tutor-hexo-bulma >}}/themes/tutor-04/layout/default.ejs
[tutor-layout-yaml]:    {{< tutor-hexo-bulma >}}/themes/tutor-04/layout/kind/yaml.ejs
[tutor-layout-text]:    {{< tutor-hexo-bulma >}}/themes/tutor-04/layout/kind/text.ejs
[tutor-layout-json]:    {{< tutor-hexo-bulma >}}/themes/tutor-04/layout/kind/json.ejs

[tutor-content-yaml]:   {{< tutor-hexo-bulma >}}/source/archives-yaml.html
[tutor-content-text]:   {{< tutor-hexo-bulma >}}/source/archives-text.html
[tutor-content-json]:   {{< tutor-hexo-bulma >}}/source/archives-json.js
