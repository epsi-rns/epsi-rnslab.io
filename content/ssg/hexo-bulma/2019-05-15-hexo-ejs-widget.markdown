---
type   : post
title  : "Hexo - EJS - Widget"
date   : 2019-05-15T09:17:35+07:00
slug   : hexo-ejs-widget
categories: [ssg, frontend]
tags      : [hexo]
keywords  : [static site, custom theme, widget, recent post, categories, tags, archives]
author : epsi
opengraph:
  image: assets/site/images/topics/hexo-bulma-markdown.png

toc    : "toc-2019-05-hexo-bulma-step"

excerpt:
  Building Hexo Step by step, with Bulma as stylesheet frontend.
  Miscellanous range loop to achieved wordpress like side panel widget.
  Also using bulma.

---

### Preface

> Goal: Miscellanous range loop to achieved wordpress like widget.

We can make a sidebar, so that we can have more information,
just like what common in wordpress site or other blogging platform.
I'm using custom css based on Bulma box to suit example needs,
and of course you can use any class other than this custom css.

#### Source Code

You can download the source code of this article here.

* [hexo-ejs-bulma-tutor-04.zip][image-ss-04-source]

Extract and run on CLI:

{{< highlight html >}}
$ npm install
{{< / highlight >}}

#### Related Article: SASS

The SASS part of this article is already discussed in this two article:

* [Bulma SASS - Custom Class][local-sass-custom]

* [Bulma SASS - Widgets][local-sass-panels]

It is time to reuse the class for real world situation.

-- -- --

### 1: Prepare

#### Theme

Still with `tutor-04`.

* <code>config.toml</code>
  : [gitlab.com/.../config.toml][tutor-hexo-config]

{{< highlight yaml >}}
# Extensions
theme: tutor-04
{{< / highlight >}}

#### Layout: EJS Post

This <code>post.ejs</code> is very similar with <code>page.ejs</code>.
The point is you can customize as creative as you want.

* <code>themes/tutor-04/layout/post.ejs</code>
  : [gitlab.com/.../layout/post.ejs][tutor-layout-post]

{{< highlight html >}}
  <main role="main">
    ...
  </main>

  <aside class="sidebar column is-one-thirds is-paddingless">
    <%- partial('widget/related-posts') %>
  </aside>
{{< / highlight >}}

Now we are using only <code>related-posts</code>.
You can have as many widget as you want as shown below:

{{< highlight html >}}
  <aside class="sidebar column is-one-thirds is-paddingless">
    <%- partial('widget/related-posts') %>
    <%- partial('widget/tags', {view_tag_type: 'before'}) %>
    <%- partial('widget/categories') %>
    <%- partial('widget/archives-grouped') %>
    <%- partial('widget/archives-github') %>
    <%- partial('widget/archives-gitlab') %>
  </aside>
{{< / highlight >}}

We are going to discuss each partial, one by one.

#### Layout: EJS Page

The same method applied to <code>page</code> kind layout.

{{< highlight html >}}
  <aside class="sidebar column is-one-thirds is-paddingless">
    <%- partial('widget/recent-posts') %>
    <%- partial('widget/affiliates') %>
    <%- partial('widget/friends') %>
    <%- partial('widget/archives-simple') %>
  </aside>
{{< / highlight >}}

We are going to discuss each partial, one by one.

#### SASS: List

As already mention in other article,
You can see the code here:

* <code>themes/tutor-04/sass/css/_list.scss</code>
  : [gitlab.com/.../sass/css/_list.scss][tutor-sass-list].

{{< highlight css >}}
// -- -- -- -- --
// _list.scss

...
{{< / highlight >}}

_._

-- -- --

### 2: Simple Example

Consider start with simple example.
We need something without any loop, EJS tag or whatsoever.
Pure HTML with a list of URLs,
that you might need to show on your side panel.

#### Layout: EJS Page

Consider examine only affiliate links:

* <code>themes/tutor-04/layout/page.ejs</code>
  : [gitlab.com/.../layout/page.ejs][tutor-layout-page]

{{< highlight html >}}
  <aside class="sidebar column is-one-thirds is-paddingless">
    <%- partial('widget/affiliates') %>
  </aside>
{{< / highlight >}}

#### Layout: Widget: HTML Affiliates

Notice the `html` file extension.

* <code>themes/tutor-04/layout/widget/affiliates.html</code>
  : [gitlab.com/.../layout/widget/affiliates.html][tutor-widget-affiliates]

{{< highlight html >}}
<section class="panel is-light">
  <div class="panel-header">
    <p>Affiliates</p>
    <span class="fa fa-child"></span>
  </div>
  <div class="panel-body has-background-white">
    <ul class="panel-list">
      <li><a href="http://epsi-rns.github.io/"
            >Linux/BSD Desktop Customization</a></li>
      <li><a href="http://epsi-rns.gitlab.io/"
            >Mobile/Web Development Blog</a></li>
      <li><a href="http://oto-spies.info/"
            >Car Painting and Body Repair.</a></li>
    </ul>
  </div>
</section>
{{< / highlight >}}

#### Render: Browser

Just open one of the quote post:

* <http://localhost:4000/about/longing.html/>

Now you can see the result in the browser.

![Hexo: Panel Affiliates][image-ss-04-widget-affiliates]

This the basic of HTML class required for the rest of this article.

-- -- --

### 3: Recent Post

#### Layout: EJS Page

Consider examine only recent posts:

* <code>themes/tutor-04/layout/page.ejs</code>
  : [gitlab.com/.../layout/page.ejs][tutor-layout-page]

{{< highlight html >}}
  <aside class="sidebar column is-one-thirds is-paddingless">
    <%- partial('widget/recent-posts') %>
  </aside>
{{< / highlight >}}

#### Layout: Widget: EJS Recent Post

Now with the usual `ejs` file extension.

* <code>themes/tutor-04/layout/widget/recent-posts.ejs</code>
  : [gitlab.com/.../layout/widget/recent-posts.ejs][tutor-widget-recent]

{{< highlight html >}}
<section class="panel is-light">
  <div class="panel-header">
    <p>Recent Posts</p>
    <span class="fa fa-newspaper"></span>
  </div>
  <div class="panel-body has-background-white">
    <ul class="panel-list">
      <% site.posts.sort('date', -1).limit(5).each(function(post){ %>
      <li><a href="<%- url_for(post.path) %>"
            ><%= post.title || '(no title)' %></a></li>
      <% }) %>
    </ul>
  </div>
</section>
{{< / highlight >}}

#### The Loop

After sorting the posts array by date,
I limit the result for only first five result.

{{< highlight javascript >}}
      <% site.posts.sort('date', -1).limit(5).each(function(post){ %>
        ...
      <% }) %>
{{< / highlight >}}

#### Render: Browser

Just open one of the page kind:

* <http://localhost:4000/about/longing.html/>

Now you can see the result in the browser.

![Hexo: Panel Recent Posts][image-ss-04-widget-recent-posts]

-- -- --

### 4: Categories and Tags

Both are Taxonomies.
So the layouts is pretty similar.

#### Layout: EJS Post

Now using post kind instead of page kind.

* <code>themes/tutor-04/layout/post.ejs</code>
  : [gitlab.com/.../layout/post.ejs][tutor-layout-post]

{{< highlight html >}}
  <aside class="sidebar column is-one-thirds is-paddingless">
    <%- partial('widget/categories') %>
  </aside>
{{< / highlight >}}

#### Layout: Widget: EJS Categories

Using Bulma's tag class <code>tag is-small is-info</code>:

* <code>themes/tutor-04/layout/widget/categories.ejs</code>
  : [gitlab.com/.../layout/widget/categories.ejs][tutor-widget-categories]

{{< highlight html >}}
<section class="panel is-light">
  <div class="panel-header">
    <p>Categories</p>
    <span class="fa fa-tag"></span>
  </div>
  <div class="panel-body has-background-white">
    <% site.categories.forEach(function(cat){ %>
      <a class="tag is-small is-info" href="<%- url_for(cat.path) %>">
        <%= cat.name %>&nbsp;<span class="fa fa-folder"></span>
      </a>&nbsp;
    <% }) %>
  </div>
</section>
{{< / highlight >}}

#### The Loop

The loop is self explanatory.

{{< highlight javascript >}}
    <% site.categories.forEach(function(cat){ %>
      ...
    <% }) %>
{{< / highlight >}}

#### Render: Browser

Just open one of the quote post:

* <http://localhost:4000/2018/09/07/lyrics/dido-white-flag/>

![Hexo: Panel Categories][image-ss-04-widget-categories]

#### Layout: EJS Post

Consider make the code above a little bit more complex.
We can pass argument to side panels.
Here we can have a choice of tag icon to be shown,
`after` or `before`.

* <code>themes/tutor-04/layout/post.ejs</code>
  : [gitlab.com/.../layout/post.ejs][tutor-layout-post]

{{< highlight html >}}
  <aside class="sidebar column is-one-thirds is-paddingless">
    <%- partial('widget/tags', {view_tag_type: 'before'}) %>
  </aside>
{{< / highlight >}}

Using Bulma's tag class <code>tag is-small is-light</code>:

* <code>themes/tutor-04/layout/widget/tags.ejs</code>
  : [gitlab.com/.../layout/widget/tags.ejs][tutor-widget-tags]

{{< highlight html >}}
<!-- view_tag_type : 'before' or 'after' -->

<section class="panel is-light">
  <div class="panel-header">
    <p>Tags</p>
    <span class="fa fa-tag"></span>
  </div>
  <div class="panel-body has-background-white">
    <% if (view_tag_type == 'after'){ %>
      <% site.tags.forEach(function(tag){ %>
        <a class="tag is-small is-light has-background-white"
           href="<%- url_for(tag.path) %>">
          <%= tag.name %>&nbsp;<span class="fa fa-folder"></span>
        </a>&nbsp;
      <% }) %>
    <% } else { %>
      <% site.tags.forEach(function(tag){ %>
        <a class="tag is-small is-light has-background-white"
           href="<%- url_for(tag.path) %>">
          <span class="fa fa-folder"></span>&nbsp;<%= tag.name %>
        </a>&nbsp;
      <% }) %>
    <% } %>
  </div>
</section>
{{< / highlight >}}

#### The Loop

The loop is still self explanatory.

{{< highlight javascript >}}
      <% site.tags.forEach(function(tag){ %>
      ...
      <% }) %>
{{< / highlight >}}

#### Render: Browser

Now you can see the result in the browser.

![Hexo: Widget Tags][image-ss-04-widget-tags]

-- -- --

### 5: Archives

This archives widget is common in wordpress.

#### Layout: Widget: EJS Archives Simple

Hexo has built in <code>list_archives</code> helper.

* <code>themes/tutor-04/layout/widget/archives-simple.ejs</code>
  : [gitlab.com/.../layout/widget/archives-simple.ejs][tutor-widget-a-simple]

{{< highlight html >}}
<section class="panel is-light">
  <div class="panel-header">
    <p>Archives</p>
    <span class="fa fa-archive"></span>
  </div>
  <div class="panel-body has-background-white">
  <%- list_archives({show_count: theme.show_count, type: theme.archive_type}) %>
  </div>
</section>
{{< / highlight >}}

Now you can see the result in the browser.

![Hexo: Panel Archives Simple][image-ss-04-widget-a-simple]

We need something that we can style better.

#### Javascript: Widget: EJS Archives Grouped

Remember the Archives (section list) tutorial in a previous chapter ?
We can also apply this in similar fashion,
and of course with the same javascript.

{{< highlight javascript >}}
  // Data Model

  // https://developer.mozilla.org/.../reduce
  
  function groupBy(objectArray, property) {
    return objectArray.reduce(function (acc, obj) {
      var key = obj[property];
      if (!acc[key]) {
        acc[key] = [];
      }
      acc[key].push(obj);
      return acc;
    }, {});
  }

  // grouping by date

  const posts = site.posts.map(post => ({ 
    ...post,
    year: date(post.date, "YYYY"),
    month: date(post.date, "MMMM")
  }));

  const postsByYear = groupBy(posts, 'year');
  
  // additional
  
  const pageYear  = date(page.date, 'YYYY');
  const pageMonth = date(page.date, 'MMMM');
{{< / highlight >}}

#### Layout: Widget: EJS Archives Grouped

Now consider pour the javascript to HTML template in EJS artefact below:

* <code>themes/tutor-04/layout/widget/archives-grouped.ejs</code>
  : [gitlab.com/.../layout/widget/archives-grouped.ejs][tutor-widget-a-grouped]

{{< highlight html >}}
<%
  ...
_%>
<section class="panel is-light">
  <div class="panel-header">
    <p>Archives</p>
    <span class="fa fa-archive"></span>
  </div>
  <div class="panel-body has-background-white">
  <% Object.keys(postsByYear).sort(date, -1).forEach(function (year){ %>
    <div class ="archive-year" id="<%= year %>">
      <a href="<%- url_for("/archives/") %>#<%= year %>">
      <%= year %></a>
    </div>
    
    <% if (year == pageYear) {%>
    <ul class="panel-archive">
    <%
      const postsByMonth = groupBy(postsByYear[year], 'month');
      Object.keys(postsByMonth).forEach(function (month){
    %>
      <li class="list-month">
        
        <span id="<%= year %>-<%= month %>">
              <a href="<%- url_for("/archives/") %>#<%= year %>-<%= month %>">
              <%= month %></a> - <%= year %></span>

        <% if (month == pageMonth) {%>
          <ul class="panel-list">
          <% postsByMonth[month].forEach(function(post){ %>

          <li>
            <a href="<%- url_for(post.path) %>">
              <%= post.title %>
            </a>
          </li>
          <% }) %>
          </ul>
        <% } %>

      </li>
      <% }) %>
    </ul>
    <% } %>
  <% }) %>
  </div>
</section>
{{< / highlight >}}

#### The Loop

I know it is complex.
So I write again here as below:

{{< highlight javascript >}}
  <% Object.keys(postsByYear).sort(date, -1).forEach(function (year){ %>
    <%= year %>
    ...

    <% if (year == pageYear) {%>
      <%
        const postsByMonth = groupBy(postsByYear[year], 'month');
        Object.keys(postsByMonth).forEach(function (month){
      %>
      <%= month %> - <%= year %>
      ...

        <% if (month == pageMonth) {%>
          <% postsByMonth[month].forEach(function(post){ %>
          ...
              <%= post.title %>
          ...
          <% }) %>
        <% } %>

      <% }) %>
    <% } %>

  <% }) %>
{{< / highlight >}}

Th effort is worthy.

#### Render: Browser

Now you can see the result in the browser.

![Hexo: Widget Archives Grouped][image-ss-04-widget-a-grouped]

-- -- --

### What is Next ?

Consider continue reading [ [Hexo - Data][local-whats-next] ].
There are, some interesting topic about <code>Example Static Data with Hexo</code>.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2019/05/16/hexo-ejs-data/

[local-sass-custom]:    {{< baseurl >}}frontend/2019/03/06/bulma-sass-custom/
[local-sass-panels]:    {{< baseurl >}}frontend/2019/03/08/bulma-sass-panels/

[image-ss-04-source]:   {{< assets-ssg >}}/2019/05/hexo-ejs-bulma-tutor-04.zip

[image-ss-04-widget-a-grouped]:    {{< assets-ssg >}}/2019/05/43-widget-archives-grouped.png
[image-ss-04-widget-a-simple]:     {{< assets-ssg >}}/2019/05/43-widget-archives-simple.png
[image-ss-04-widget-categories]:   {{< assets-ssg >}}/2019/05/43-widget-categories.png
[image-ss-04-widget-recent-posts]: {{< assets-ssg >}}/2019/05/43-widget-recent-posts.png
[image-ss-04-widget-tags]:         {{< assets-ssg >}}/2019/05/43-widget-tags.png
[image-ss-04-widget-affiliates]:   {{< assets-ssg >}}/2019/05/43-widget-affiliates.png

[tutor-hexo-config]:    {{< tutor-hexo-bulma >}}/_config.yml
[tutor-sass-list]:      {{< tutor-hexo-bulma >}}/themes/tutor-04/sass/css/_list.scss

[tutor-widget-affiliates]:  {{< tutor-hexo-bulma >}}/themes/tutor-04/layout/widget/affiliates.html
[tutor-widget-recent]:      {{< tutor-hexo-bulma >}}/themes/tutor-04/layout/widget/recent-posts.ejs
[tutor-widget-categories]:  {{< tutor-hexo-bulma >}}/themes/tutor-04/layout/widget/categories.ejs
[tutor-widget-tags]:        {{< tutor-hexo-bulma >}}/themes/tutor-04/layout/widget/tags.ejs
[tutor-widget-a-simple]:    {{< tutor-hexo-bulma >}}/themes/tutor-04/layout/widget/archives-simple.ejs
[tutor-widget-a-grouped]:   {{< tutor-hexo-bulma >}}/themes/tutor-04/layout/widget/archives-grouped.ejs

[tutor-layout-page]:    {{< tutor-hexo-bulma >}}/themes/tutor-04/layout/page.ejs
[tutor-layout-post]:    {{< tutor-hexo-bulma >}}/themes/tutor-04/layout/post.ejs


