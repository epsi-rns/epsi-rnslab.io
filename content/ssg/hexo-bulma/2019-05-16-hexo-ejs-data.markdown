---
type   : post
title  : "Hexo - EJS - Data"
date   : 2019-05-16T09:17:35+07:00
slug   : hexo-ejs-data
categories: [ssg, frontend]
tags      : [hexo]
keywords  : [static site, custom theme, data, widget, friends, related post]
author : epsi
opengraph:
  image: assets/site/images/topics/hexo-bulma-markdown.png

toc    : "toc-2019-05-hexo-bulma-step"

excerpt:
  Building Hexo Step by step, with Bulma as stylesheet frontend.
  Using data feature, from simply static to dynamic one.

---

### Preface

> Goal: Using data feature.

There is no such things as database connection in static site generator.

However, we can use static data, to fulfill simple requirement.

#### Source Code

You can download the source code of this article here.

* [hexo-ejs-bulma-tutor-04.zip][image-ss-04-source]

Extract and run on CLI:

{{< highlight html >}}
$ npm install
{{< / highlight >}}

-- -- --

### 1: Outside Link: Friends

#### Static Data

Suppossed, we want to show all blog site, at widget in the sidebar.
Instead of manually coding the html, we can prepare the data.

* <code>source/_data/friends.yml</code>
  : [gitlab.com/.../source/_data/friends.yml][tutor-data-friends]

{{< highlight yaml >}}
# Isle of friends

- title: "BanditHijo (R)-Chive"
  url: http://bandithijo.com/

- title: "Sira Argia (Aflasio)"
  url: https://aflasio.netlify.com/

- title: "Rania Amina"
  url: https://raniaamina.github.io/
{{< / highlight >}}

This might looks stupid,
because we can directly write the result in html.
But in the next example, we can show you how data can be dynamic.

Later we can access this simple data using <code>site.data.friends</code>.

#### Layout: EJS Page

Do not forget to change the <code>page.ejs</code>,
to examine only <code>friends</code> widget:

* <code>themes/tutor-04/layout/page.ejs</code>
  : [gitlab.com/.../layout/page.ejs][tutor-layout-page]

{{< highlight html >}}
  <aside class="sidebar column is-one-thirds is-paddingless">
    <%- partial('widget/friends') %>
  </aside>
{{< / highlight >}}

#### Javascript: Widget: EJS Friends

This require javascript, to shuffle the array.

{{< highlight javascript >}}
/**
 * Randomly shuffle an array
 * https://stackoverflow.com/a/2450976/1293256
 * @param  {Array} array The array to shuffle
 * @return {String}      The first item in the shuffled array
 */
var shuffle = function (array) {

  var currentIndex = array.length;
  var temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {
    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
};
{{< / highlight >}}

* <code>.</code>

#### Layout: Widget: EJS Friends

Now consider pour the javascript to HTML template in EJS artefact below:

* <code>themes/tutor-04/layout/widget/friends.ejs</code>
  : [gitlab.com/.../layout/widget/friends.ejs][tutor-widget-friends]

{{< highlight html >}}
<%
  ...
%>

<section class="panel is-light">
  <div class="panel-header">
    <p>Isle of Friends</p>
    <span class="fa fa-child"></span>
  </div>
  <div class="panel-body has-background-white">
    <ul class="panel-list">
      <% friends = shuffle(site.data.friends).slice(-5) %>
      <% friends.forEach(function(friend){ %>
      <li><a href="<%= friend.url %>"
         ><%= friend.title %></a></li>
      <% }) %>
    </ul>
  </div>
</section>
{{< / highlight >}}

#### The Loop

I also shuffle the result so for each page they shown randomly.

{{< highlight javascript >}}
      <% friends = shuffle(site.data.friends).slice(-5) %>
      <% friends.forEach(function(friend){ %>
        ...
      <% }) %>
{{< / highlight >}}

#### Render: Browser

Now you can see the result in the browser.

![Hexo: Widget Friends][image-ss-04-widget-friends]

-- -- --

### 2: Outside Link: Backlinks

Why not go further?
Instead of just showing your friends domain name,
you can put backlink of external blogs in your widget.
As long as the backlink is relevant to your content, you are safe.

#### Static Data

Yes, we can automate this!

Suppossed, you have different blog,
and you to show articles of your other blog,
at widget in the sidebar.
Instead of manually coding the html,
we can prepare the data.

* <code>source/_data/archives-github</code>
  : [gitlab.com/.../source/_data/archives-github][tutor-data-a-github]
  
or other blog as well

* <code>source/_data/archives-gitlab</code>
  : [gitlab.com/.../source/_data/archives-gitlab][tutor-data-a-gitlab]

{{< highlight yaml >}}
# Helper for related links

- id: 19061119
  title: "Conky with Lua Scripting"
  url: /desktop/2019/06/11/modularized-conky.html

- id: 19040245
  title: "GhostBSD - Ports"
  url: /system/2019/04/02/ghostbsd-ports.html

- id: 19033045
  title: "GhostBSD - Migration from Linux"
  url: /system/2019/03/30/ghostbsd-migration.html
{{< / highlight >}}

All you need are only <code>title</code> and <code>url</code>.
And you can ignore <code>id</code>.
The <code>id</code> serve different purpose for other widget,
as we will discuss later.

We will also discuss how to achieve this external data in other article.

#### Javascript: Widget: EJS Archives Backlink

This require the same javascript,
in <code>friends.ejs</code> as above,
to shuffle the array.

#### Layout: Widget: EJS Archives Backlink

Now consider pour the javascript to HTML template in EJS artefact below:

* <code>themes/tutor-04/layout/widget/archives-github.ejs</code>
  : [gitlab.com/.../layout/widget/archives-github.ejs][tutor-widget-a-github]

{{< highlight html >}}
<%
  ...
%>

<section class="panel is-light">
  <div class="panel-header">
    <p>Linux/BSD Customization</p>
    <span class="fa fa-child"></span>
  </div>
  <div class="panel-body has-background-white">
    <ul class="panel-list">
      <% friends = shuffle(site.data.archives_github).slice(-5) %>
      <% friends.forEach(function(friend){ %>
      <li><a href="http://epsi-rns.github.io<%= friend.url %>"
         ><%= friend.title %></a></li>
      <% }) %>
    </ul>
  </div>
</section>
{{< / highlight >}}

We can access this simple data,
using <code>site.data.archives_github</code>.

We can als setup other blog as well as shown in widget below:

* <code>themes/tutor-04/layout/widget/archives-gitlab.ejs</code>
  : [gitlab.com/.../layout/widget/archives-gitlab.ejs][tutor-widget-a-gitlab]

#### Render: Browser

Now you can see the result in the browser.

![Hexo: Widget Archives epsi-rns.github.io][image-ss-04-widget-a-github]

And with the same method we can setup widget for other blog as well.

![Hexo: Widget Archives epsi-rns.gitlab.io][image-ss-04-widget-a-gitlab]

-- -- --

### 3: Local Link: Related Post

Remember our last archives page ?
Yeah, the **yaml** data one.
We can use it here.

#### Static Data

The data look like here below:

* <code>data/archives.yml</code>
  : [gitlab.com/.../data/archives.yml][tutor-data-archives]

{{< highlight yaml >}}
# Helper for related links
# Archives by Date

...

- id: 15010108
  title: "Dead Poet Society"
  url: 2015/01/01/quotes/dead-poets-society/

- id: 15100308
  title: "Every Day"
  url: 2015/10/03/quotes/every-day/

...
{{< / highlight >}}

![Hexo: Data Archives][image-ss-04-vim-archives]

#### Data Generator

So what is it, the id field ?
Well, our data related posts , need a simple data id.
But since we do not have RDBMS, we can emulate the id.
We are going to use the id from the date field in frontmatter.

Let me remind you how the data generated from script below:

{{< highlight javascript >}}
# Helper for related links
# <%= page.title %>

<% site.posts.each(function(post){ %>
- id: <%= date(post.date, "YYMMDDmm") %>
  title: "<%= post.title %>"
  url: <%= post.path %>
<% }) %>
{{< / highlight >}}

The format "YYMMDDmm", means [year-month-date-minute].
It is rarely, that I blog with the same minute in the same day.
This way, the id would be unique.

#### Manual Download

The <code>data/archives.yml</code> itself is generated.
We have already discuss custom content type in previous chapter.
But there are more steps to do:

* The data should be downloaded manually from
  <http://localhost:4000/archives-yaml.html>  or
  <http://localhost:4000/archives-text.html>

* and copied manually to <code>source/_data/archives.yml</code>
  : [gitlab.com/.../source/_data/archives.yml][tutor-data-archives]

* Or alternatively using <http://localhost:4000/archives-json.js>

* and copied manually to <code>source/_data/archives_local.json</code>
  : [gitlab.com/.../source/_data/archives_local.json][tutor-data-archives-json]

I know, it seems tricky, but it works well so far.

#### Layout: EJS Post

Do not forget to change the <code>post.ejs</code>,
to examine only <code>related-posts</code> widget:

* <code>themes/tutor-04/layout/post.ejs</code>
  : [gitlab.com/.../layout/post.ejs][tutor-layout-post]

{{< highlight html >}}
  <aside class="sidebar column is-one-thirds is-paddingless">
    <%- partial('widget/related-posts') %>
  </aside>
{{< / highlight >}}

#### Layout: Widget: EJS Related Posts

Now that we already have the id, it is time to use the data.

* <code>themes/tutor-04/layout/widget/related-posts.ejs</code>
  : [gitlab.com/.../layout/widget/related-posts.ejs][tutor-widget-related]

{{< highlight html >}}
<% if (page.related_link_ids) { %>
<section class="panel is-light">
  <div class="panel-header">
    <p>Related Posts</p>
    <span class="fa fa-link"></span>
  </div>
  <div class="panel-body has-background-white">
    <ul class="panel-list">
      <% page.related_link_ids.forEach(function (id){ %>
      <li>
        <% site.data.archives.forEach(function(archive){ %>

          <% if (archive.id == id) { %>
          <a href="<%= url_for(archive.url) %>"><%= archive.title %></a>
          <% } %>

        <% }) %>
      </li>
      <% }) %>
    </ul>
  </div>
</section>
<% } %>
{{< / highlight >}}

#### Data Source: Two Options

Code above is using YAML as data source:

* <code>source/_data/archives.yml</code>
  : [gitlab.com/.../source/_data/archives.yml][tutor-data-archives]

Alternatively, you can use JSON as data source:

* <code>source/_data/archives_local.json</code>
  : [gitlab.com/.../source/_data/archives_local.json][tutor-data-archives-json]

And change the data source in the code as below:

{{< highlight html >}}
...
        <% site.data.archives_local.forEach(function(archive){ %>
          ...
        <% }) %>
...
{{< / highlight >}}

### Content: Example

If we want to use it, <code>related_link_ids</code> in frontmatter.
Consider this example.

* <code>source/_posts/lyrics/natalie-cole-i-wish-you-love.md</code>
  : [gitlab.com/.../...i-wish-you-love.md][tutor-content-wish-you]

{{< highlight yaml >}}
---
title     : Susan Wong - I Wish You Love
date      : 2015/05/15 07:35:05
tags      : [pop, 70s]
category  : [lyric]

related_link_ids: 
  - 15051515  # The Way We Were
---

I wish you bluebirds in the spring  
To give your heart a song to sing

<!-- more --> 

I wish you health  
And more than wealth  
I wish you love
{{< / highlight >}}

![Hexo: Content with Data][image-ss-04-vim-susan-wong]

#### Server Output: Browser

Now you can see the result in the browser.

* <http://localhost:4000/2015/05/15/lyrics/natalie-cole-i-wish-you-love/>

![Hexo: Widget Related Posts][image-ss-04-widget-related-post]

-- -- --

### What is Next ?

Consider continue reading [ [Hexo - Pagination - Introduction][local-whats-next] ].
There are, some interesting topic about <code>Pagination in Hexo</code>.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2019/05/17/hexo-pagination-intro/

[image-ss-04-source]:   {{< assets-ssg >}}/2019/05/hexo-ejs-bulma-tutor-04.zip

[image-ss-04-widget-friends]:       {{< assets-ssg >}}/2019/05/43-widget-friends.png
[image-ss-04-widget-a-github]:      {{< assets-ssg >}}/2019/05/43-widget-archives-github.png
[image-ss-04-widget-a-gitlab]:      {{< assets-ssg >}}/2019/05/43-widget-archives-gitlab.png
[image-ss-04-widget-related-post]:  {{< assets-ssg >}}/2019/05/43-widget-related-post.png
[image-ss-04-vim-susan-wong]:       {{< assets-ssg >}}/2019/05/43-vim-susan-wong.png
[image-ss-04-vim-archives]:         {{< assets-ssg >}}/2019/05/43-vim-data-archives-yml.png

[tutor-data-friends]:       {{< tutor-hexo-bulma >}}/source/_data/friends.yml
[tutor-data-archives]:      {{< tutor-hexo-bulma >}}/source/_data/archives.yml
[tutor-data-archives-json]: {{< tutor-hexo-bulma >}}/source/_data//archives_local.json
[tutor-data-a-github]:      {{< tutor-hexo-bulma >}}/source/_data/archives-github.yml
[tutor-data-a-gitlab]:      {{< tutor-hexo-bulma >}}/source/_data/archives-gitlab.yml
[tutor-content-wish-you]:   {{< tutor-hexo-bulma >}}/source/_posts/lyrics/natalie-cole-i-wish-you-love.md

[tutor-layout-related]:     {{< tutor-hexo-bulma >}}/themes/tutor-05/layout/partials/widget/related-posts.html
[tutor-layout-friends]:     {{< tutor-hexo-bulma >}}/themes/tutor-05/layout/partials/widget/friends.html

[tutor-widget-friends]:     {{< tutor-hexo-bulma >}}/themes/tutor-04/layout/widget/archives-friends.ejs
[tutor-widget-a-github]:    {{< tutor-hexo-bulma >}}/themes/tutor-04/layout/widget/archives-gitlab.ejs
[tutor-widget-a-gitlab]:    {{< tutor-hexo-bulma >}}/themes/tutor-04/layout/widget/archives-github.ejs
[tutor-widget-related]:     {{< tutor-hexo-bulma >}}/themes/tutor-04/layout/widget/related-posts.ejs

[tutor-layout-page]:    {{< tutor-hexo-bulma >}}/themes/tutor-04/layout/page.ejs
[tutor-layout-post]:    {{< tutor-hexo-bulma >}}/themes/tutor-04/layout/post.ejs
