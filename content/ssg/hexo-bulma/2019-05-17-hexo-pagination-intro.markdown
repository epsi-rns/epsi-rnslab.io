---
type   : post
title  : "Hexo - Pagination - Introduction"
date   : 2019-05-17T09:17:35+07:00
slug   : hexo-pagination-intro
categories: [ssg, frontend]
tags      : [hexo, navigation, bulma]
keywords  : [static site, custom theme, ejs, pagination, simple]
author : epsi
opengraph:
  image: assets/site/images/topics/hexo-bulma.png

toc    : "toc-2019-05-hexo-bulma-step"

excerpt:
  Building Hexo Step by step, with Bulma as stylesheet frontend.
  An introduction to pagination for dummies.

---

### Preface

> Goal: An introduction to pagination.

Pagination is a whole big topic.
It is rare that common front end web based component require algorithm.
This pagination takes thinking time, instead of just rely on design skill.

#### Source Code

You can download the source code of this article here.

* [hexo-ejs-bulma-tutor-05.zip][image-ss-05-source]

Extract and run on CLI using <code>$ npm install</code>.

-- -- --

### 1: Article Provided

For each step, I made different layout,
and explain the deatil in separate articles.
This article series start from simple pagination layout.

#### Step By Step

We are going to have six step to achieve this pagination.

* Layout 01: Simple

* Layout 02: Number

* Layout 03: Adjacent

* Layout 04: Indicator

* Layout 05: Responsive

* Layout 06: Screen Reader

This is the spoiler, of what we are going to do in this article series.

#### Additional

I also provide other additional article as well:

* Introduction

* Navigation Style (using layout 02 as example)

* Javascript (function helper for handling URL)

* Section

#### Preview

Complete Pagination 

![Hugo Pagination: Indicator Animation][image-ss-05-indicator-gif]

Responsive

![Hugo Pagination: Responsive Animation][image-ss-05-responsive-gif]

-- -- --

### 2: Prepare

This preparation is required.

#### Configuration

We need to setup pagination settings.

* <code>_config.yml</code>
  : [gitlab.com/.../_config.yml][tutor-hexo-config]

{{< highlight yaml >}}
# Home page setting
index_generator:
  path: '/blog'
  per_page    : 2
  order_by: -date
  
# Pagination
pagination_dir: page
{{< / highlight >}}

For live site, I normally have <code>10</code> pages for each pagination.
But for this tutorial, I only have limited number of example article.
This is why I only set <code>2</code> article per page.

#### Theme

Since we have made a lot of changes in previous theme,
consider move on to new theme.

1. Create theme <code>themes/tutor-05</code>.

You can use source code above, oor from gitlab repository or,
you can also copy paste from <code>themes/tutor-04</code>.

2. Edit configuration <code>_config.yml</code>, and save.

{{< highlight yaml >}}
# Extensions
theme: tutor-05
{{< / highlight >}}

#### Content: Example Article

Just make sure you have enough article,
to test all pagination case.

{{< highlight bash >}}
% tree source/_posts
source/_posts
├── lyrics
│   ├── barbara-streisand-the-way-we-were.md
│   ├── bon-jovi-diamond-ring.md
│   ├── darius-rucker-it-wont-be-like-this-for-long.md
│   ├── david-gates-lady-valentine.md
│   ├── dido-white-flag.md
│   ├── george-benson-moodys-mood.md
│   ├── heatwave-all-i-am.md
│   ├── moody-blues-white-satin.md
│   ├── natalie-cole-i-wish-you-love.md
│   ├── norah-jones-shoot-the-moon.md
│   └── vanessa-williams-love-is.md
├── quotes
│   ├── dead-poets-society.md
│   ├── every-day.md
│   ├── fight-club.md
│   ├── jerry-maguire.md
│   └── scott-pilgrim.md
└── white-winter.md

2 directories, 17 files
{{< / highlight >}}

Here we have 17 articles.

![Hexo: Tree Posts Content][image-ss-05-tree-posts]

-- -- --

### 3: Layout

Consider make a slight change,
to inside <code>layout/index.ejs</code>.

#### Partial: Pagination 

Since code can be complex, it is better to put,
the pagination as partial, somewhere else.

{{< highlight html >}}
  <section class="section">
    <%- partial('pagination/01-simple') %>
  </section>
{{< / highlight >}}

#### Layout: EJS Index

The complete code is as below.

* <code>themes/tutor-05/layout/index.ejs</code>
  : [gitlab.com/.../layout/index.ejs][tutor-layout-index]

{{< highlight html >}}
<main role="main" 
      class="column is-full box-deco has-background-white">
  <section class="section">
    <h1 class="title is-4"><%= config.author %></h1>
    <h2 class="subtitle is-4"><%= config.subtitle %></h2>
  </section>

  <section class="section">
    <%- partial('pagination/01-simple') %>
  </section>
  
  <br/>

  <div class="post-list">
  <% page.posts.each(function(post){ %>
    <section class="section" id="archive">
      <%- partial('summary/blog-item', {post: post}) %>
    </section>
  <% }) %>
  </div>

</main>
{{< / highlight >}}

#### All Pagination Options

Since we are going to have six steps,
we may switch the layout often,
so we need a way to __copy-and-paste__ easily.
Here below, is the pagination.
You should only select one of them.

{{< highlight javascript >}}
        partial('pagination/01-simple')
        partial('pagination/02-number')
        partial('pagination/03-adjacent')
        partial('pagination/04-indicator')
        partial('pagination/05-responsive')
        partial('pagination/06-screenreader')
{{< / highlight >}}

-- -- --

### 4: Skeleton

This part is the most basic part,
and would be used in each layout for each step.

#### Layout: EJS Pagination Code

The skeleton is similar as below:

{{< highlight javascript >}}
<!-- helper function -->
<!-- variable initialization -->
<nav class="pagination is-small is-centered" 
     role="navigation"
     aria-label="pagination">

    <ul class="pagination-list">
      <!-- left  navigation code -->
      <!-- middle numbering code -->
      <!-- right navigation code -->
    </ul>

  <% } %>
</nav>
{{< / highlight >}}

Navigation code contain buttons such as:

* Left Pagination: First

* Left Pagination: Previous

* Right Pagination: Next

* Right Pagination: Last

While numbering code is in the middle

* Middle Pagination: Numbering

Notice that, you may easily change the position.
For example, put the middle pagination on right side,
instead of in the middle.
So alternatively you can switch to:

{{< highlight javascript >}}
<!-- helper function -->
<!-- variable initialization -->
<nav class="pagination is-small is-centered" 
     role="navigation"
     aria-label="pagination">

    <!-- navigation code -->

    <ul class="pagination-list">
      <!-- numbering code -->
    </ul>

</nav>
{{< / highlight >}}

#### Minimal Logic

The pagination would only be shown,
if there is more than one pagination page.
No need to show any pagination navigation,
if what we have is only one page.
Hence our minimal pagination logic would be:

{{< highlight javascript >}}
<nav ...>
  <% if (page.total > 1) { %>
    ...
    <!-- pagination code -->

  <% } %>
</nav>
{{< / highlight >}}

Of course this is not enough.
Our minimal pagination should show something.

#### Loop

The loop for each pagination, is simply as below:

{{< highlight javascript >}}
    <% 
  var cursor; 
  for (cursor = 1; cursor <= page.total; cursor++) { 
    ...
    <% } %>
{{< / highlight >}}

I'm using the term <code>cursor</code>.

-- -- --

### 5: Math: Algebra

#### Assumption

Consider an example, this blog post that contain **seventeen** posts.

{{< highlight conf >}}
# CONST

Total Post   = 17
Per Page     = 2

# COMPUTED

page.total   = 9
{{< / highlight >}}

#### Equation

And change the number of page for each pagination,
by number for example:
<code>2</code>, <code>3</code>, <code>5</code>, 
<code>10</code>, and <code>20</code>.

* <code>_config.yml</code>
  : [gitlab.com/.../_config.yml][tutor-hexo-config]

{{< highlight yaml >}}
# Home page setting
index_generator:
  per_page    : 2
{{< / highlight >}}

Restart the server everytime you change the config.

{{< highlight bash >}}
$ hexo server -p 4000 --debug
{{< / highlight >}}

#### Table

Do not worry!
<code>page.total</code> do the math internally in Hexo.

We can also achieve <code>page.total</code> by **ceiling division**.
And the result is on this table below.

{{< highlight conf >}}
# ALGEBRA

+-------------+-------+-------+-------+-------+-------+
| pagination  |   2   |   3   |   5   |  10   |  20   |
+-------------+-------+-------+-------+-------+-------+
| VARIABLE                                            |
| division    |  8.5  |  5.7  |  3.4  |  1.7  |  0.85 |
| page.total  |   9   |   6   |   4   |   2   |  N/A  |
+-------------+-------+-------+-------+-------+-------+
{{< / highlight >}}

Of course, we do not need to show any pagination,
if there is only one page for all result.
That is why we can optionally,
convert <code>1</code> into <code>N/A</code>.

-- -- --

### What is Next ?

Now we are ready to begin this pagination journey.

Consider continue reading [ [Hexo - Pagination - Simple][local-whats-next] ].
There are, some interesting topic about <code>Pagination in Hexo</code>.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2019/05/17/hexo-pagination-simple/

[image-ss-05-source]:   {{< assets-ssg >}}/2019/05/hexo-ejs-bulma-tutor-05.zip

[tutor-hexo-config]:    {{< tutor-hexo-bulma >}}/_config.yml

[image-ss-05-indicator-gif]:    {{< assets-ssg >}}/2019/04/52-04-indicator-bulma-animate.gif
[image-ss-05-responsive-gif]:   {{< assets-ssg >}}/2019/04/52-04-responsive-bulma-animate.gif

[image-ss-05-tree-posts]:   {{< assets-ssg >}}/2019/05/51-tree-posts.png
