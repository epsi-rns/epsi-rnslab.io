---
type   : post
title  : "Hexo - Pagination - Simple"
date   : 2019-05-17T13:17:35+07:00
slug   : hexo-pagination-simple
categories: [ssg, frontend]
tags      : [hexo, navigation, bulma]
keywords  : [static site, custom theme, ejs, pagination, simple]
author : epsi
opengraph:
  image: assets/site/images/topics/hexo-bulma.png

toc    : "toc-2019-05-hexo-bulma-step"

excerpt:
  Building Hexo Step by step, with Bulma as stylesheet frontend.
  Pagination for dummies, a simple start for beginner.

---

### Preface

> Goal: A Simple Pagination.

Every journey start with a single step.

#### Source Code

You can download the source code of this article here.

* [hexo-ejs-bulma-tutor-05.zip][image-ss-05-source]

Extract and run on CLI using <code>$ npm install</code>.

-- -- --

### 1: Preview: General

This is what we want to achieve in this tutorial.

![Hexo Pagination: Simple Positioning Indicator][image-ss-05-combined]

#### Layout: EJS Index

Consider use <code>pagination/01-simple</code> layout, 
in <code>index.ejs</code>

* <code>themes/tutor-05/layout/index.ejs</code>
  : [gitlab.com/.../layout/index.ejs][tutor-layout-index]

#### HTML Preview

The HTML that we want to achieve in this article, is similar as below.

{{< highlight html >}}
<nav class="pagination is-small is-centered"
     role="navigation" 
     aria-label="pagination">

    <!-- First Page. -->   
      <a class="pagination-previous" 
         href="/blog"
         rel="first">First</a>

    <!-- Previous Page. -->
      <a class="pagination-previous" 
         href="/blog/page/8/"
         rel="prev">Previous</a>

    <!-- Indicator Number. -->
    <a class="pagination-link"
       disabled="">Page: 9 of 9</a>

    <!-- Next Page. -->
      <a class="pagination-next" 
         title="This is the last page"
         disabled="">Next</a>

    <!-- Last Page. -->
      <a class="pagination-next" 
         title="This is the last page"
         disabled="">Last</a>

    <!-- Dummy. Do not delete! -->
    <ul class="pagination-list">
    </ul>

</nav>
{{< / highlight >}}

We will achieve this with Hexo code.

-- -- --

### 2: Navigation: Previous and Next

{{< highlight javascript >}}
<nav class="pagination is-small is-centered" 
     role="navigation" aria-label="pagination">
  <% if (page.total > 1) { %>

    <!-- Previous Page. -->
    <% if (page.prev_link) { %>
      <a class="pagination-previous"
         href="<%= url_for(page.prev_link) %>" 
         rel="prev">Previous</a>
    <% } else { %>
      <a class="pagination-previous"
         title="This is the first page"
         disabled>Previous</a>
    <% } %>

    <!-- Next Page. -->
    <% if (page.next_link) { %>
      <a class="pagination-next"
         href="<%= url_for(page.next_link) %>" 
         rel="next">Next</a>
    <% } else { %>
      <a class="pagination-next"
         title="This is the last page"
         disabled>Next</a>
    <% } %>

  <% } %>
</nav>
{{< / highlight >}}

#### Browser: Pagination Preview

Examine any page in blog section.

* <http://localhost:4000/blog/page/9/>

![Hexo Pagination: Simple Previous Next][image-ss-05-prev-next]

#### How does it works ?

This code above rely on 

* Previous Page: <code>page.prev_link</code>, and 

* Next Page: <code>page.next_link</code>.

This should be self explanatory.

#### Stylesheet: Bulma Class

Bulma specific class are these below

* ul: <code>pagination</code>

* a: <code>pagination-previous</code>,
     <code>pagination-next</code>
     
* a: <code>disabled</code>

Bulma doesn't put all pagination class in <code>ul li</code>.

-- -- --

### 3: Navigation: First and Last

Consider also add first page and last page.

{{< highlight javascript >}}
<nav class="pagination is-small is-centered" 
     role="navigation" aria-label="pagination">
  <% if (page.total > 1) { %>

    <!-- First Page. -->
    <% if (page.prev != 0) { %>
      <a class="pagination-previous"
         href="<%= url_for(config.index_generator.path) %>" 
         rel="first">First</a>
    <% } else { %>
      <a class="pagination-previous"
         title="This is the first page"
         disabled>First</a>
    <% } %>

    <!-- Last Page. -->
    <% if (page.next != 0) { %>
    <%
        var lastPage = config.index_generator.path + 
                 '/' + config.pagination_dir + 
                 '/' + page.total;
    %>
      <a class="pagination-next"
         href="<%= url_for(lastPage) %>" 
         rel="last">Last</a>
    <% } else { %>
      <a class="pagination-next"
         title="This is the last page"
         disabled>Last</a>
    <% } %>

  <% } %>
</nav>
{{< / highlight >}}

#### Browser: Pagination Preview

Examine any page in blog section.

* <http://localhost:4000/blog/page/9/>

![Hexo Pagination: Simple First Last][image-ss-05-first-last]

#### How does it works ?

Hexo has a special method,
to show first page and last page:

* First Page: <code>page.prev != 0</code>.

* Last Page: <code>page.next != 0</code>.

#### Javascript: Pagination URL

Noticee this javascript below:

{{< highlight javascript >}}
        var lastPage = config.index_generator.path + 
                 '/' + config.pagination_dir + 
                 '/' + page.total;
{{< / highlight >}}

Later, we are going to make a clean code,
by separating the javascript code, outside the html code.

-- -- --

### 4: Active Page Indicator

And also the pagination positioning in between,
in the middle of those.

{{< highlight javascript >}}
<nav class="pagination is-small is-centered" 
     role="navigation" aria-label="pagination">
  <% if (page.total > 1) { %>

    <!-- Indicator Number. -->
    <a class="pagination-link" 
       disabled>Page: <%= page.current %> of <%= page.total %></a>

  <% } %>
</nav>
{{< / highlight >}}

#### Browser: Pagination Preview

Examine any page in blog section.

* <http://localhost:4000/blog/page/9/>

![Hexo Pagination: Simple Positioning Indicator][image-ss-05-indicator]

#### How does it works ?

This just show:

{{< highlight javascript >}}
Page: <%= page.current %> of <%= page.total %>
{{< / highlight >}}

-- -- --

### 5: Dummy List

All above are pagination indicators.
We will use number later using <code>ul li</code> structure.
We can put it here, just in case we want to add number something later.

{{< highlight javascript >}}
    <!-- Dummy. Do not delete! -->
    <ul class="pagination-list">
    </ul>
{{< / highlight >}}

-- -- --

### 6: Summary

Complete code is here below:

* <code>themes/tutor-05/layout/pagination/01-simple.ejs</code>
  : [gitlab.com/.../layout/pagination/01-simple.ejs][tutor-p-01-simple].

{{< highlight twig >}}
<nav class="pagination is-small is-centered" 
     role="navigation" aria-label="pagination">
  <% if (page.total > 1) { %>
    <!-- First Page. -->
    <% if (page.prev != 0) { %>
      <a class="pagination-previous"
         href="<%= url_for(config.index_generator.path) %>" 
         rel="first">First</a>
        <% } else { %>
      <a class="pagination-previous"
         title="This is the first page"
         disabled>First</a>
    <% } %>

    <!-- Previous Page. -->
    <% if (page.prev_link) { %>
      <a class="pagination-previous"
         href="<%= url_for(page.prev_link) %>" 
         rel="prev">Previous</a>
    <% } else { %>
      <a class="pagination-previous"
         title="This is the first page"
         disabled>Previous</a>
    <% } %>

    <!-- Indicator Number. -->
    <a class="pagination-link" 
       disabled>Page: <%= page.current %> of <%= page.total %></a>

    <!-- Next Page. -->
    <% if (page.next_link) { %>
      <a class="pagination-next"
         href="<%= url_for(page.next_link) %>" 
         rel="next">Next</a>
    <% } else { %>
      <a class="pagination-next"
         title="This is the last page"
         disabled>Next</a>
    <% } %>

    <!-- Last Page. -->
    <% if (page.next != 0) { %>
    <%
        var lastPage = config.index_generator.path + 
                 '/' + config.pagination_dir + 
                 '/' + page.total;
    %>
      <a class="pagination-next"
         href="<%= url_for(lastPage) %>" 
         rel="last">Last</a>
    <% } else { %>
      <a class="pagination-next"
         title="This is the last page"
         disabled>Last</a>
    <% } %>

    <!-- Dummy. Do not delete! -->
    <ul class="pagination-list">
    </ul>

  <% } %>
</nav>
{{< / highlight >}}

#### Browser: Pagination Preview

Examine any page in blog section.

* <http://localhost:4000/blog/page/9/>

![Hexo Pagination: Simple All Indicators Combined][image-ss-05-combined]

-- -- --

### What is Next ?

Consider continue reading [ [Hexo - Pagination - Number][local-whats-next] ].
There are, some interesting topic about <code>Pagination in Hexo</code>.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2019/05/18/hexo-pagination-number/

[image-ss-05-source]:   {{< assets-ssg >}}/2019/05/hexo-ejs-bulma-tutor-05.zip

[image-ss-05-first-last]:   {{< assets-ssg >}}/2019/05/51-first-last.png
[image-ss-05-indicator]:    {{< assets-ssg >}}/2019/05/51-indicator-number.png
[image-ss-05-prev-next]:    {{< assets-ssg >}}/2019/05/51-prev-next.png
[image-ss-05-combined]:     {{< assets-ssg >}}/2019/05/51-combined.png

[tutor-hexo-config]:    {{< tutor-hexo-bulma >}}/_config.yml
[tutor-layout-index]:   {{< tutor-hexo-bulma >}}/themes/tutor-05/layout/index.ejs
[tutor-p-01-simple]:    {{< tutor-hexo-bulma >}}/themes/tutor-05/layout/pagination/01-simple.ejs

