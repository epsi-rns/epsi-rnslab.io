---
type   : post
title  : "Hexo - Pagination - Navigation"
date   : 2019-05-18T13:17:35+07:00
slug   : hexo-pagination-navigation
categories: [ssg, frontend]
tags      : [hexo, navigation, bulma, sass]
keywords  : [static site, custom theme, ejs, pagination, numbering]
author : epsi
opengraph:
  image: assets/site/images/topics/hexo-bulma.png

toc    : "toc-2019-05-hexo-bulma-step"

excerpt:
  Building Hexo Step by step, with Bulma as stylesheet frontend.
  Add Navigation to pagination, along with number inside one list tag.

---

### Preface

> Goal: Add Navigation to pagination, combine with simple numbering.

#### Source Code

You can download the source code of this article here.

* [hexo-ejs-bulma-tutor-05.zip][image-ss-05-source]

Extract and run on CLI using <code>$ npm install</code>.

-- -- --

### 1: Prepare

We are still using layout named <code>pagination/02-number</code>,
in our previous <code>index.ejs</code>

#### Layout: EJS Index

Consider use <code>pagination/02-number</code> layout,
in <code>index.ejs</code>

* <code>themes/tutor-05/layout/index.ejs</code>
  : [gitlab.com/.../layout/index.ejs][tutor-layout-index]
  
-- -- --  

### 2: Navigation: Previous and Next

Consider give it a <code>prev</code> <code>next</code> button.

#### Bulma Pagination

We can utilize example from Bulma official documentation.
Which has a very nice looks.

{{< highlight html >}}
<nav class="pagination is-small is-centered" 
     role="navigation" aria-label="pagination">
  <% if (page.total > 1) { %>
    <!-- First Page. -->
    <% if (page.prev != 0) { %>
      <a class="pagination-previous"
         href="<%= pagination_url(1) %>" 
         rel="first">First</a>
        <% } else { %>
      <a class="pagination-previous"
         title="This is the first page"
         disabled>First</a>
    <% } %>

    <!-- Previous Page. -->
    <% if (page.prev_link) { %>
      <a class="pagination-previous"
         href="<%= url_for(page.prev_link) %>" 
         rel="prev">Previous</a>
    <% } else { %>
      <a class="pagination-previous"
         title="This is the first page"
         disabled>Previous</a>
    <% } %>

    <!-- Next Page. -->
    <% if (page.next_link) { %>
      <a class="pagination-next"
         href="<%= url_for(page.next_link) %>" 
         rel="next">Next</a>
    <% } else { %>
      <a class="pagination-next"
         title="This is the last page"
         disabled>Next</a>
    <% } %>

    <!-- Last Page. -->
    <% if (page.next != 0) { %>
      <a class="pagination-next"
         href="<%= pagination_url(page.total) %>" 
         rel="last">Last</a>
    <% } else { %>
      <a class="pagination-next"
         title="This is the last page"
         disabled>Last</a>
    <% } %>

  <% } %>
</nav>
{{< / highlight >}}

#### Browser: Pagination Preview

Which shown nice pagination.

![Hexo Pagination: Stylesheet Before After][image-ss-05-combined]

#### Inline Navigation: Previous and Next

Alternatively you can wrap them all inside <code>ul li</code> tags.

{{< highlight html >}}
      <!-- Previous Page. -->
      <li class="blog-previous">
      <% if (page.prev_link) { %>
        <a class="pagination-previous"
           href="<%= url_for(page.prev_link) %>" 
           rel="prev">&laquo;&nbsp;</a>
      <% } else { %>
        <a class="pagination-previous"
           title="This is the first page"
           disabled>&laquo;&nbsp;</a>
      <% } %>
      </li>

      <!-- Next Page. -->
      <li class="blog-next">
      <% if (page.next_link) { %>
        <a class="pagination-next"
           href="<%= url_for(page.next_link) %>" 
           rel="next">&nbsp;&raquo;</a>
      <% } else { %>
        <a class="pagination-next"
           title="This is the last page"
           disabled>&nbsp;&raquo;</a>
      <% } %>
      </li>
{{< / highlight >}}

#### Inline Navigation: First Last

With the same method, put them all inside <code>ul li</code> tags.

{{< highlight html >}}
      <!-- First Page. -->
      <li class="first">
      <% if (page.prev != 0) { %>
        <a class="pagination-previous"
           href="<%= pagination_url(1) %>" 
           rel="first">&laquo;&laquo;&nbsp;</a>
      <% } else { %>
        <a class="pagination-previous"
           title="This is the first page"
           disabled>&laquo;&laquo;&nbsp;</a>
      <% } %>
      </li>

      <!-- Last Page. -->
      <li class="last">
      <% if (page.next != 0) { %>
        <a class="pagination-next"
           href="<%= pagination_url(page.total) %>" 
           rel="last">&nbsp;&raquo;&raquo;</a>
      <% } else { %>
        <a class="pagination-next"
           title="This is the last page"
           disabled>&nbsp;&raquo;&raquo;</a>
      <% } %>
      </li>
{{< / highlight >}}

#### Browser: Pagination Preview

Now have a look at this changes.

![Hexo Pagination: Symbol Previous Next][image-ss-05-laquo-raquo]

The choice between the two method is up to you.
You can even have both, functionality and nice looks.

#### Changes

That code above is using:

* symbol <code>&laquo;</code> instead of <code>previous</code> word.

* symbol <code>&raquo;</code> instead of <code>next</code> word.

Notice that, we also have additional stylesheet named 

* <code>blog_previous</code>, and <code>blog_next</code>,

* <code>first</code>, and <code>last</code>.

-- -- --

### 3: Stylesheet: Before and After

Consider define these pagination classes:

* <code>themes/tutor-05/sass/css/_pagination.scss</code>
  : [gitlab.com/.../sass/_pagination.scss][tutor-hexo-sass-pagination].

{{< highlight sass >}}
li.blog-previous a:after
  content: " previous"
li.blog-next a:before
  content: "next "

ul.pagination-list
  li.first,
  li.last
    display: none
{{< / highlight >}}

And add the new scss in main file.

* <code>themes/tutor-05/sass/css/main.scss</code>
  : [gitlab.com/.../sass/main.scss][tutor-hexo-sass-main].

{{< highlight sass >}}
// dart-sass --watch -I sass sass/css:source/css/

...

@import "panel"
@import "blog"
@import "list"
@import "pagination"
{{< / highlight >}}

#### Browser: Pagination Preview

![Hexo Pagination: Stylesheet Before After][image-ss-05-before-after]

-- -- --

### 4: Stylesheet: Responsive

#### Pure CSS

Yes you can do this, and I already make a different article for this.

#### Bulma Mixins

By the help of mixins/breakpoints,
we can write it in bulma style.

{{< highlight sass >}}
+tablet
  li.blog-previous a:after
    content: " previous"
  li.blog-next a:before
    content: "next "

ul.pagination-list
  li.first,
  li.last
    display: none
  +from($tablet)
    li.first,
    li.last
      display: inline-block
{{< / highlight >}}

#### Browser: Pagination Preview

![Hexo Pagination: Stylesheet Responsive][image-ss-05-responsive]

-- -- --

### 5: Summary

To summarized this article, we need a complete code, and preview.

#### Layout: Pagination Number

* <code>themes/tutor-05/layout/pagination/02-number.ejs</code>
  : [gitlab.com/.../layout/pagination/02-number.ejs][tutor-p-02-number].

{{< highlight html >}}
<%
/*
* Helper function
*/

function pagination_url(number) {
  var path = config.index_generator.path;
  
  // dirty quick fix, avoid double (//)
  if (path=='/') { path = '' }

  if (number>1) {
      path = path + '/' + config.pagination_dir + '/' + number;
  }

  return url_for(path);
}
%>
<nav class="pagination is-small is-centered" 
     role="navigation" aria-label="pagination">
  <% if (page.total > 1) { %>

    <!-- First Page. -->
    <% if (page.prev != 0) { %>
      <a class="pagination-previous"
         href="<%= pagination_url(1) %>" 
         rel="first">First</a>
        <% } else { %>
      <a class="pagination-previous"
         title="This is the first page"
         disabled>First</a>
    <% } %>

    <!-- Previous Page. -->
    <% if (page.prev_link) { %>
      <a class="pagination-previous"
         href="<%= url_for(page.prev_link) %>" 
         rel="prev">Previous</a>
    <% } else { %>
      <a class="pagination-previous"
         title="This is the first page"
         disabled>Previous</a>
    <% } %>

    <!-- Next Page. -->
    <% if (page.next_link) { %>
      <a class="pagination-next"
         href="<%= url_for(page.next_link) %>" 
         rel="next">Next</a>
    <% } else { %>
      <a class="pagination-next"
         title="This is the last page"
         disabled>Next</a>
    <% } %>

    <!-- Last Page. -->
    <% if (page.next != 0) { %>
      <a class="pagination-next"
         href="<%= pagination_url(page.total) %>" 
         rel="last">Last</a>
    <% } else { %>
      <a class="pagination-next"
         title="This is the last page"
         disabled>Last</a>
    <% } %>

    <!-- Main Pagination List -->
    <ul class="pagination-list">

      <!-- First Page. -->
      <li class="first">
      <% if (page.prev != 0) { %>
        <a class="pagination-previous"
           href="<%= pagination_url(1) %>" 
           rel="first">&laquo;&laquo;&nbsp;</a>
      <% } else { %>
        <a class="pagination-previous"
           title="This is the first page"
           disabled>&laquo;&laquo;&nbsp;</a>
      <% } %>
      </li>

      <!-- Previous Page. -->
      <li class="blog-previous">
      <% if (page.prev_link) { %>
        <a class="pagination-previous"
           href="<%= url_for(page.prev_link) %>" 
           rel="prev">&laquo;&nbsp;</a>
      <% } else { %>
        <a class="pagination-previous"
           title="This is the first page"
           disabled>&laquo;&nbsp;</a>
      <% } %>
      </li>

      <!-- Page numbers. -->
      <% var cursor; 
         for (cursor = 1; cursor <= page.total; cursor++) { %>
      <li>
        <% if (page.current != cursor) { %>
        <a href="<%= pagination_url(cursor) %>"
           class="pagination-link"
           aria-label="Goto page <%= cursor %>">
          <%= cursor %>
        </a>
        <% } else { %>
        <a class="pagination-link is-current" 
           aria-label="Page <%= page.current %>">
          <%= page.current %>
        </a>
        <% } %>
      </li>
      <% } %>

      <!-- Next Page. -->
      <li class="blog-next">
      <% if (page.next_link) { %>
        <a class="pagination-next"
           href="<%= url_for(page.next_link) %>" 
           rel="next">&nbsp;&raquo;</a>
      <% } else { %>
        <a class="pagination-next"
           title="This is the last page"
           disabled>&nbsp;&raquo;</a>
      <% } %>
      </li>

      <!-- Last Page. -->
      <li class="last">
      <% if (page.next != 0) { %>
        <a class="pagination-next"
           href="<%= pagination_url(page.total) %>" 
           rel="last">&nbsp;&raquo;&raquo;</a>
      <% } else { %>
        <a class="pagination-next"
           title="This is the last page"
           disabled>&nbsp;&raquo;&raquo;</a>
      <% } %>
      </li>

    </ul>
  <% } %>
</nav>
{{< / highlight >}}

* <code>.</code>

#### Browser: Pagination Preview

![Hexo Pagination: Stylesheet Combined][image-ss-05-combined]

-- -- --

### What is Next ?

Do not get confused yet.
Keep calm, just like this cute kitten.
Our pagination tutorial still have some materials to go.

![adorable kitten][image-kitten]

Consider continue reading [ [Hexo - Pagination - Adjacent][local-whats-next] ].
There are, some interesting topic about <code>Pagination in Hexo</code>.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2018/11/19/hexo-pagination-adjacent/
[image-kitten]:             {{< baseurl >}}/assets/site/images/cats/rambu-04.jpg

[image-ss-05-source]:   {{< assets-ssg >}}/2019/05/hexo-ejs-bulma-tutor-05.zip

[image-ss-05-laquo-raquo]:  {{< assets-ssg >}}/2019/05/52-number-laquo-raquo.png
[image-ss-05-before-after]: {{< assets-ssg >}}/2019/05/52-number-before-after.png
[image-ss-05-number-list]:  {{< assets-ssg >}}/2019/05/52-number-list.png
[image-ss-05-responsive]:   {{< assets-ssg >}}/2019/05/52-responsive.png
[image-ss-05-combined]:     {{< assets-ssg >}}/2019/05/52-combined.png
[image-ss-05-fpnl]:         {{< assets-ssg >}}/2019/05/52-fpnl.png

[tutor-hexo-sass-main]:          {{< tutor-hexo-bulma >}}/themes/tutor-05/sass/css/main.scss
[tutor-hexo-sass-pagination]:    {{< tutor-hexo-bulma >}}/themes/tutor-05/sass/css/_pagination.scss

[tutor-hexo-config]:    {{< tutor-hexo-bulma >}}/_config.yml
[tutor-layout-index]:   {{< tutor-hexo-bulma >}}/themes/tutor-05/layout/index.ejs
[tutor-p-02-number]:    {{< tutor-hexo-bulma >}}/themes/tutor-05/layout/pagination/02-number.ejs
