---
type   : post
title  : "Hexo - Pagination - Number"
date   : 2019-05-18T09:17:35+07:00
slug   : hexo-pagination-number
categories: [ssg, frontend]
tags      : [hexo, navigation, bulma, sass]
keywords  : [static site, custom theme, ejs, pagination, numbering]
author : epsi
opengraph:
  image: assets/site/images/topics/hexo-bulma.png

toc    : "toc-2019-05-hexo-bulma-step"

excerpt:
  Building Hexo Step by step, with Bulma as stylesheet frontend.
  Pagination for dummies, list all of page.

---

### Preface

> Goal: Pagination using Number.

#### Source Code

You can download the source code of this article here.

* [hexo-ejs-bulma-tutor-05.zip][image-ss-05-source]

Extract and run on CLI using <code>$ npm install</code>.

-- -- --

### 1: Prepare

Consider use a new layout named <code>pagination/02-number</code>,
in our previous <code>index.ejs</code>

#### Layout: EJS Index

Consider use <code>pagination/02-number</code> layout,
in <code>index.ejs</code>

* <code>themes/tutor-05/layout/index.ejs</code>
  : [gitlab.com/.../layout/index.ejs][tutor-layout-index]

-- -- --

### 2: Preview: General

#### HTML Preview

The HTML that we want to achieve in this article, is similar as below.

{{< highlight html >}}
    <ul class="pagination-list">
      <!-- Page numbers. -->

      <li>
        <a href="/blog" class="pagination-link"
           aria-label="Goto page 1">1</a>
      </li>

      <li>
        <a href="/blog/page/2" class="pagination-link"
           aria-label="Goto page 2">2</a>
      </li>

      ...

      <li>
        <a href="/blog/page/8" class="pagination-link"
           aria-label="Goto page 8">8</a>
      </li>

      <li>
        <a class="pagination-link is-current" 
           aria-label="Page 9">9</a>
      </li>

    </ul>
{{< / highlight >}}

We will achieve this with Hexo code.

-- -- --

### 3: Navigation: Number

Pagination by number is also simple.

#### Pagination URL

The URL that we ant to achieve is as below

* <code>localhost:4000/blog/page/:number</code>

To achieve this URL, all you need to do is put in a <code>href</code>.

{{< highlight javascript >}}
<a href="<%= url_for(config.index_generator.path + '/' + config.pagination_dir + '/' + number) %>
  <%= numberr %>
</a>"
{{< / highlight >}}

#### Javascript: Pagination URL

To make a clean code,
consider to separate the javascript code,
outside the html code.

{{< highlight javascript >}}
function pagination_url(number) {
  var path = config.index_generator.path;
  
  // dirty quick fix, avoid double (//)
  if (path=='/') { path = '' }

  if (number>1) {
      path = path + '/' + config.pagination_dir + '/' + number;
  }

  return url_for(path);
}
{{< / highlight >}}

Just remember our last configuration.

* <code>_config.yml</code>
  : [gitlab.com/.../_config.yml][tutor-hexo-config]

{{< highlight yaml >}}
# Home page setting
index_generator:
  path: '/blog'
  per_page    : 2
  order_by: -date
  
# Pagination
pagination_dir: page
{{< / highlight >}}

Now we can call it with:

{{< highlight javascript >}}
<a href="<%= pagination_url(number) %>">
  <%= number %>
</a>
{{< / highlight >}}

#### Layout: Pagination Number

* <code>themes/tutor-05/layout/pagination/02-number.ejs</code>
  : [gitlab.com/.../layout/pagination/02-number.ejs][tutor-p-02-number].

{{< highlight html >}}
<%
  ...
%>
<nav class="pagination is-small is-centered" 
     role="navigation" aria-label="pagination">
  <% if (page.total > 1) { %>

    <!-- Main Pagination List -->
    <ul class="pagination-list">
      <!-- Page numbers. -->
      <% var cursor; 
         for (cursor = 1; cursor <= page.total; cursor++) { %>
      <li>
        <% if (page.current != cursor) { %>
        <a href="<%= pagination_url(cursor) %>"
           class="pagination-link"
           aria-label="Goto page <%= cursor %>">
          <%= cursor %>
        </a>
        <% } else { %>
        <a class="pagination-link is-current" 
           aria-label="Page <%= page.current %>">
          <%= page.current %>
        </a>
        <% } %>
      </li>
      <% } %>
    </ul>
  <% } %>
</nav>
{{< / highlight >}}

#### Browser: Pagination Preview

![Hexo Pagination: Number List][image-ss-05-number-list]

#### How does it works ?

Just a simple loop:

{{< highlight javascript >}}
      <% var cursor; 
         for (cursor = 1; cursor <= page.total; cursor++) { %>
         ...
      <% } %>
{{< / highlight >}}

And simple conditional:

{{< highlight javascript >}}
        <% if (page.current != cursor) { %>
          <a href="<%= pagination_url(cursor) %>"><%= cursor %></a>
        <% } else { %>
          <a><%= cursor %></a>
        <% } %>
{{< / highlight >}}

No <code>href</code> link for current page.

-- -- --

### What is Next ?

Consider continue reading [ [Hexo - Pagination - Navigation][local-whats-next] ].
There are, some interesting topic about <code>Pagination in Hexo</code>.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2018/11/18/hexo-pagination-navigation/

[image-ss-05-source]:   {{< assets-ssg >}}/2019/05/hexo-ejs-bulma-tutor-05.zip

[image-ss-05-number-list]:  {{< assets-ssg >}}/2019/05/52-number-list.png

[tutor-hexo-layout-list]:       {{< tutor-hexo-bulma >}}/themes/tutor-05/layout/archives/list.html
[tutor-hexo-layout-number]:     {{< tutor-hexo-bulma >}}/themes/tutor-05/layout/partials/pagination-number.html


[tutor-hexo-config]:    {{< tutor-hexo-bulma >}}/_config.yml
[tutor-layout-index]:   {{< tutor-hexo-bulma >}}/themes/tutor-05/layout/index.ejs
[tutor-p-02-number]:    {{< tutor-hexo-bulma >}}/themes/tutor-05/layout/pagination/02-number.ejs
