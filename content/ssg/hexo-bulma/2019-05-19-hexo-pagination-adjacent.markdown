---
type   : post
title  : "Hexo - Pagination - Adjacent"
date   : 2019-05-19T09:17:35+07:00
slug   : hexo-pagination-adjacent
categories: [ssg, frontend]
tags      : [hexo, navigation, bulma]
keywords  : [static site, custom theme, ejs, middle pagination, adjacent, math]
author : epsi
opengraph:
  image: assets/site/images/topics/hexo-bulma.png

toc    : "toc-2019-05-hexo-bulma-step"

excerpt:
  Building Hexo Step by step, with Bulma as stylesheet frontend.
  Advance pagination. Not so for dummies.

---

### Preface

> Goal: Apply Glenn McComb Pagination using Hexo.

#### Source Code

You can download the source code of this article here.

* [hexo-ejs-bulma-tutor-05.zip][image-ss-05-source]

Extract and run on CLI using <code>$ npm install</code>.

-- -- --

### 1: Source

I respect copyright.
The code below copied, and pasted from:

* <https://glennmccomb.com/articles/how-to-build-custom-hugo-pagination/>

I made a slight modification.
But of course the logic remain the same.

I mostly write it down in my blog,
because I do not want to forget what I learn.
An easier place for me to find the modified version of this good code.

-- -- --

### 2: Preview: General

It is not easy to explain by words.
Let me explain what we want to achive by using these images below.
The blue box, is the active page.
since we have 17 articles and 3 page for each pagination,
then we have 9 pages of pagination.
We have from first page (1), to last page (9).

#### Animation: Combined Version

This is the complete version.
We will achieve this later.

![Hexo Pagination: Combined Animation][image-ss-05-combined-animation]

#### Animation: Stripped Version

I'm following **Glenn McComb** code,
combined with my own code,
and this is the result.

![Hexo Pagination: Adjacent Animation][image-ss-05-adjacent-animation]

#### Sample: An Example

Consider get one frame, a sample, because we need an example.
This is what we want to achieve in this tutorial.

![Hexo Pagination: Adjacent Page 5][image-ss-05-stripped-05]

#### HTML Preview

The HTML that we want to achieve in this article, is similar as below.

{{< highlight html >}}
<nav class="pagination is-small is-centered" ...>
    <ul class="pagination-list">
      <li><a class="pagination-link" ...>3</a></li>
      <li><a class="pagination-link" ...>4</a></li>
      <li><a class="pagination-link is-current" >5</a></li>
      <li><a class="pagination-link" ...>6</a></li>
      <li><a class="pagination-link" ...>7</a></li>
    </ul>
</nav>
{{< / highlight >}}

Or in detail, you can breakdown each as below:

{{< highlight html >}}
<nav class="pagination is-small is-centered" 
     role="navigation" 
     aria-label="pagination">

    <ul class="pagination-list">

      <li>
        <a href="/blog/page/3"
           class="pagination-link"
           aria-label="Goto page 3">3
        </a>
      </li>

      <li>
        <a href="/blog/page/4" 
           class="pagination-link" 
           aria-label="Goto page 4">4
        </a>
      </li>

      <li>
        <a class="pagination-link is-current"
           aria-label="Page 5">5
        </a>
      </li>


      <li>
        <a href="/blog/page/6" 
           class="pagination-link" 
           aria-label="Goto page 6">6
        </a>
      </li>


      <li>
        <a href="/blog/page/7" 
           class="pagination-link" 
           aria-label="Goto page 7">7
        </a>
      </li>

    </ul>
</nav>
{{< / highlight >}}

We will achieve this with Hexo code.

#### The Riddle

How do we achieve this ?

-- -- --

### 3: Prepare

This step is required.

#### Configuration

As usual.

{{< highlight yaml >}}
# Home page setting
index_generator:
  path: '/blog'
  per_page    : 2
  order_by: -date

# Pagination
pagination_dir: page
{{< / highlight >}}

#### Layout: EJS Index

Consider use <code>pagination/03-adjacent</code> layout,
in <code>index.ejs</code>

* <code>themes/tutor-05/layout/index.ejs</code>
  : [gitlab.com/.../layout/index.ejs][tutor-layout-index]

#### Layout: Pagination Code Skeleton

This is just skeleton, as we will discuss this later.

* <code>themes/tutor-05/layout/pagination/03-adjacent.ejs</code>
  : [gitlab.com/.../layout/pagination/03-adjacent.ejs][tutor-p-03-adjacent].

{{< highlight javascript >}}
// Variable Initialization.

<nav role="navigation" aria-label="pagination">
  <% if (page.total > 1) { %>
    <ul class="pagination-list">

    <% 
  var cursor; 
  for (cursor = 1; cursor <= page.total; cursor++) { 
    // Variable Initialization

    if (page.total > max_links) {
      // Complex page numbers.

      // Check between these three:
      // * Lower limit pages, or
      // * Upper limit pages, or
      // * Middle pages.

    } else {
      // Simple page numbers.
      ...
    }

    // Show Pager.
    ...

    <% } %>
    </ul>
  <% } %>
</nav>
{{< / highlight >}}

-- -- --

### 4: Math: Basic Algebra

#### Assumption

Consider our previous example,
a blog post contain **seventeen** posts.
This time with two **adjacent**.
It means, two indicators before selected page,
and another two indicators after selected page,

{{< highlight conf >}}
# CONST

Total Post   = 17
Per Page     = 2

adjacent_links = 2

# COMPUTED

page.total   = 9
{{< / highlight >}}

#### Equation

We should manually, do the math.

{{< highlight javascript >}}
EQUATION

max_links   = (adjacent_links * 2) + 1 = 5
lower_limit =  1 + $adjacent_links     = 3
upper_limit =  9 - $adjacent_links     = 7
{{< / highlight >}}

The real code is shown as below:

{{< highlight javascript >}}
const adjacent_links = 2;
var current          = page.current;
var max_links        = (adjacent_links * 2) + 1;
var lower_limit      = 1 + adjacent_links;
var upper_limit      = page.total - adjacent_links;
{{< / highlight >}}

Again, that source above are, **ported** from:

* <https://glennmccomb.com/articles/how-to-build-custom-hexo-pagination/>

#### Table

The result is on this table below.

{{< highlight conf >}}
# ALGEBRA

+-------------+-------+-------+-------+-------+-------+
| pagination  |   2   |   3   |   5   |  10   |  20   |
+-------------+-------+-------+-------+-------+-------+
| VARIABLE                                            |
| page.total  |   9   |   6   |   4   |   2   |  N/A  |
| max_links   |   5   |   5   |   5   |   5   |  N/A  |
| lower_limit |   3   |   3   |   3   |   3   |  N/A  |
| upper_limit |   7   |   4   |   2   |   0   |  N/A  |
+-------------+-------+-------+-------+-------+-------+
{{< / highlight >}}

-- -- --

### 5: Adjacent Code

This utilized <code>show_cursor_flag</code> with complex algorithm.

#### Skeleton 

In every loop, this flag should be check first and use later.

{{< highlight javascript >}}
    <% 
  var cursor; 
  for (cursor = 1; cursor <= page.total; cursor++) { 
    var show_cursor_flag = false;

    // Set Flag: Complex code.
    ...

    // Use Flag: Show Pager.
    ...
    <% } %>
{{< / highlight >}}

#### Checking The Flag

This <code>show_cursor_flag</code> should be checked in every loop.

{{< highlight javascript >}}
    <% 
  var cursor; 
  for (cursor = 1; cursor <= page.total; cursor++) { 
    var show_cursor_flag = false;

    if (page.total > max_links) {
      // Complex page numbers.

      if (current <= lower_limit) {
        // Lower limit pages.
        // If the user is on a page which is in the lower limit.
        if (cursor <= max_links) {
          // If the current loop page is less than max_links.
          show_cursor_flag = true;
        }
      } else if (current >= upper_limit) {
        // Upper limit pages.
        // If the user is on a page which is in the upper limit.
        if (cursor > (page.total - max_links)) {
          // If the current loop page is less than max_links.
          show_cursor_flag = true;
        }
      } else {
        // Middle pages.
        if ( (cursor >= current - adjacent_links) 
        &&   (cursor <= current + adjacent_links) ) {
          show_cursor_flag = true;
        }
      }
    } else {
      // Simple page numbers.
      show_cursor_flag = true;
    }

    // Show Pager.
    ...
    <% } %>
{{< / highlight >}}

#### Using The Flag

And showing the number whenever the <code>show_cursor_flag</code> comes out.
With similar code as previous article.

{{< highlight javascript >}}
    <% 
  var cursor; 
  for (cursor = 1; cursor <= page.total; cursor++) { 
    var show_cursor_flag = false;

    if (page.total > max_links) {
      // Complex page numbers.
      ...
    }

    // Show Pager.
    if (show_cursor_flag) { %>
      <li>
        <% if (current != cursor) { %>
        <a href="<%= pagination_url(cursor) %>"
           class="pagination-link" 
           aria-label="Goto page <%= cursor %>">
          <%= cursor %>
        </a>
        <% } else { %>
        <a class="pagination-link is-current" 
           aria-label="Page <%= current %>">
          <%= current %>
        </a>
        <% } %>
      </li>
      <% } %>
    <% } %>
{{< / highlight >}}

#### The Same Riddle

How does it works ?

> Really! It is confusing.

Luckily, there is explanation for this, for the curious one.

#### Math in Details

I provide separate article with thorough explanation of the logic,
used by this <code>Glenn McComb Pagination</code>.

-- -- --

### What is Next ?

Consider continue reading [ [Hexo - Pagination - Logic][local-whats-next] ].
There are, some interesting topic about <code>Pagination in Hexo</code>.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2019/05/19/hexo-pagination-logic/

[image-ss-05-source]:   {{< assets-ssg >}}/2019/05/hexo-ejs-bulma-tutor-05.zip

[image-ss-05-combined-animation]: {{< assets-ssg >}}/2019/05/53-pagination-combined.gif
[image-ss-05-adjacent-animation]: {{< assets-ssg >}}/2019/05/53-hexo-bulma-adjacent-animate.gif
[image-ss-05-stripped-05]:        {{< assets-ssg >}}/2019/05/53-adjacent-stripped-05.png

[tutor-hexo-config]:    {{< tutor-hexo-bulma >}}/_config.yml
[tutor-layout-index]:   {{< tutor-hexo-bulma >}}/themes/tutor-05/layout/index.ejs
[tutor-p-03-adjacent]:  {{< tutor-hexo-bulma >}}/themes/tutor-05/layout/pagination/03-adjacent.ejs
