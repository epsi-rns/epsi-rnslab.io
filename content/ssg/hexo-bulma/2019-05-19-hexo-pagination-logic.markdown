---
type   : post
title  : "Hexo - Pagination - Logic"
date   : 2019-05-19T13:17:35+07:00
slug   : hexo-pagination-logic
categories: [ssg, frontend]
tags      : [hexo, navigation, bulma]
keywords  : [static site, custom theme, ejs, middle pagination, adjacent, math]
author : epsi
opengraph:
  image: assets/site/images/topics/hexo-bulma.png

toc    : "toc-2019-05-hexo-bulma-step"

excerpt:
  Building Hexo Step by step, with Bulma as stylesheet frontend.
  Reverse engineering of Glenn McComb Pagination using Math and Table.

---

### Preface

> Goal: Explaining Glenn McComb Pagination using Math and Table.

#### Source Code

You can download the source code of this article here.

* [hexo-ejs-bulma-tutor-05.zip][image-ss-05-source]

Extract and run on CLI using <code>$ npm install</code>.

-- -- --

### 1: Preview: Detail

Consider, have a look at the pagination below in a stripped down model.

#### Layout: EJS Index

We are still using <code>pagination/03-adjacent</code> layout,
in <code>index.ejs</code>

* <code>themes/tutor-05/layout/index.ejs</code>
  : [gitlab.com/.../layout/index.ejs][tutor-layout-index]

#### Structure

This will only show one part:

* Middle Pagination: **Glenn McComb**

#### Each Pagination

Consider, have a look at the animation above, frame by frame.
I'm going to do some reverse engineering,
to accomplish better understanding on how this pagination works.

We have from first page (1), to last page (9).

![Hexo Pagination: Adjacent Page 1][image-ss-05-stripped-01]

![Hexo Pagination: Adjacent Page 2][image-ss-05-stripped-02]

![Hexo Pagination: Adjacent Page 3][image-ss-05-stripped-03]

![Hexo Pagination: Adjacent Page 4][image-ss-05-stripped-04]

![Hexo Pagination: Adjacent Page 5][image-ss-05-stripped-05]

![Hexo Pagination: Adjacent Page 6][image-ss-05-stripped-06]

![Hexo Pagination: Adjacent Page 7][image-ss-05-stripped-07]

![Hexo Pagination: Adjacent Page 8][image-ss-05-stripped-08]

![Hexo Pagination: Adjacent Page 9][image-ss-05-stripped-09]

#### Table

We can rewrite the table with additional rows as below.

{{< highlight conf >}}
+-------------+-------+-------+-------+-------+-------+
| pagination  |   2   |   3   |   5   |  10   |  20   |
+-------------+-------+-------+-------+-------+-------+
| VARIABLE                                            |
| page.total  |   9   |   6   |   4   |   2   |  N/A  |
| max_links   |   5   |   5   |   5   |   5   |  N/A  |
| lower_limit |   3   |   3   |   3   |   3   |  N/A  |
| upper_limit |   7   |   4   |   2   |   0   |  N/A  |
+-------------+-------+-------+-------+-------+-------+
| MIDDLE PAGINATION                                   |
| page   = 1  | 1..5  | 1..5  | 1..4  | 1..2  |-------+
| page   = 2  | 1..5  | 1..5  | 1..4  | 1..2  |       |
| page   = 3  | 1..5  | 1..5  | 1..4  |-------+       |
| page   = 4  | 2..6  | 4..6  | 1..4  |               |
| page   = 5  | 3..7  | 4..6  |-------+               |
| page   = 6  | 4..8  | 4..6  |                       |
| page   = 7  | 5..9  |-------+                       |
| page   = 8  | 5..9  |                               |
| page   = 9  | 5..9  |                               |
+-------------+-------+-------------------------------+
{{< / highlight >}}

-- -- --

### 2: Math: Conditional

#### Part: Middle Pages

This is already discussed in, so I won't explain it nomore.

* <https://glennmccomb.com/articles/how-to-build-custom-hexo-pagination/>

{{< highlight javascript >}}
      if (page.current <= lower_limit) {
        // Lower limit pages.
        ...
      } else if (page.current >= upper_limit) {
        // Upper limit pages.
        ...
      } else {
        // Middle pages.
        if ( (cursor >= page.current - adjacent_links) 
        &&   (cursor <= page.current + adjacent_links) ) {
          show_cursor_flag = true;
        }
      }
{{< / highlight >}}

What you need to know is the conditional result in table:

{{< highlight conf >}}
+--------------+-------+
| pagination  |   2   |
| adjacent    |   2   |
| total post  |  17   |
+--------------+-------+
| VARIABLE            |
| page.total  |   9   |
| max_links   |   5   |
| lower_limit |   3   |
| upper_limit |   7   |
+-------------+-------+--+
| selected    | adjacent |
+-------------+----------+
| page   =  1 |   1..3   |
| page   =  2 |   1..4   |
| page   =  3 |   1..5   |
| page   =  4 |   2..6   |
| page   =  5 |   3..7   |
| page   =  6 |   4..8   |
| page   =  7 |   5..9   |
| page   =  8 |   5..9   |
| page   =  9 |   5..9   |
+-------------+----------+
{{< / highlight >}}

#### Part: Lower Limit Pages

Consider stripped more for each part.

{{< highlight javascript >}}
      if (page.current <= lower_limit) {
        // Lower limit pages.
        // If the user is on a page which is in the lower limit.
        if (cursor <= max_links) {
          // If the current loop page is less than max_links.
          show_cursor_flag = true;
        }
      } else if (page.current >= upper_limit) {
        // Upper limit pages.
        ...
      } else {
        // Middle pages.
        ...
      }
{{< / highlight >}}

Notice that there is two part of conditional.

* Outer conditional:
  result true for the first three row,
  as defined by <code>lower_limit</code>.

* Inner conditional: always result 1..5

Thus, the conditional result in table:

{{< highlight conf >}}
+--------------+-------+-------+--------+
| selected     | lower | l max | result |
+--------------+-------+-------+--------+
| page   =  1  |   T   | 1..5  |  1..5  |
| page   =  2  |   T   | 1..5  |  1..5  |
| page   =  3  |   T   | 1..5  |  1..5  |
| page   =  4  |       | 1..5  |        |
| page   =  5  |       | 1..5  |        |
| page   =  6  |       | 1..5  |        |
| page   =  7  |       | 1..5  |        |
| page   =  8  |       | 1..5  |        |
| page   =  9  |       | 1..5  |        |
+--------------+-------+-------+--------+
{{< / highlight >}}

#### Combined: All Conditional

Now we have all the logic combined at once.

{{< highlight conf >}}
+-------------+-------+
| pagination  |   1   |
| adjacent    |   2   |
| totalPost   |  10   |
+-------------+-------+
| VARIABLE            |
| totalPages  |  10   |
| max_links   |   5   |
| lower_limit |   3   |
| upper_limit |   7   |
+-------------+-------+-+-------+-------+-------+-------+
| selected    | adjacent| lower | l max | upper | u max |
+-------------+---------+-------+-------+-------+-------+
| cursor = 1  |  1..3   |   T   | 1..5  |       | 5..9  |
| cursor = 2  |  1..4   |   T   | 1..5  |       | 5..9  |
| cursor = 3  |  1..5   |   T   | 1..5  |       | 5..9  |
| cursor = 4  |  2..6   |       | 1..5  |       | 5..9  |
| cursor = 5  |  3..7   |       | 1..5  |       | 5..9  |
| cursor = 6  |  4..8   |       | 1..5  |       | 5..9  |
| cursor = 7  |  5..9   |       | 1..5  |   T   | 5..9  |
| cursor = 8  |  5..9   |       | 1..5  |   T   | 5..9  |
| cursor = 9  |  5..9   |       | 1..5  |   T   | 5..9  |
+-------------+---------+-------+-------+-------+-------+
{{< / highlight >}}

#### Final Result

As a conclusion table.

{{< highlight conf >}}
+-------------+-------+
| VARIABLE            |
| totalPages  |  10   |
| max_links   |   5   |
| lower_limit |   3   |
| upper_limit |   8   |
+-------------+-------+-------+---------+
| selected    | lower | upper | adjacent|
+-------------+-------+-------+---------+
| cursor =  1 | 1..5  |       |         |
| cursor =  2 | 1..5  |       |         |
| cursor =  3 | 1..5  |       |         |
| cursor =  4 |       |       |  2..6   |
| cursor =  5 |       |       |  3..7   |
| cursor =  6 |       |       |  4..8   |
| cursor =  7 |       | 5..9  |         |
| cursor =  8 |       | 5..9  |         |
| cursor =  9 |       | 5..9  |         |
+-------------+-------+-------+---------+
| selected    | if elsif else | result  |
+-------------+---------------+---------+
| cursor = 1  |               |  1..5   |
| cursor = 2  |               |  1..5   |
| cursor = 3  |               |  1..5   |
| cursor = 4  |               |  2..6   |
| cursor = 5  |               |  3..7   |
| cursor = 6  |               |  4..8   |
| cursor = 7  |               |  5..9   |
| cursor = 8  |               |  5..9   |
| cursor = 9  |               |  5..9   |
+-------------+---------------+---------+
{{< / highlight >}}

-- -- --

### 3: Summary: Navigation: Adjacent

Now you can enjoy the complete adjacent code as below:

* <code>themes/tutor-05/layout/pagination/03-adjacent.ejs</code>
  : [gitlab.com/.../layout/pagination/03-adjacent.ejs][tutor-p-03-adjacent].

{{< highlight javascript >}}
<%
/*
* Helper function
*/

function pagination_url(number) {
  var path = config.index_generator.path;
  
  // dirty quick fix, avoid double (//)
  if (path=='/') { path = '' }

  if (number>1) {
      path = path + '/' + config.pagination_dir + '/' + number;
  }

  return url_for(path);
}

/* 
* Pagination links 
* https://glennmccomb.com/articles/how-to-build-custom-hugo-pagination/
* Adjacent: Number of links either side of the current page
*/

const adjacent_links = 2;
var current          = page.current;
var max_links        = (adjacent_links * 2) + 1;
var lower_limit      = 1 + adjacent_links;
var upper_limit      = page.total - adjacent_links;
%>

<nav class="pagination is-small is-centered"
     role="navigation" aria-label="pagination">
  <% if (page.total > 1) { %>
    <ul class="pagination-list">

    <% 
  var cursor; 
  for (cursor = 1; cursor <= page.total; cursor++) { 
    var show_cursor_flag = false;

    if (page.total > max_links) {
      // Complex page numbers.

      if (current <= lower_limit) {
        // Lower limit pages.
        // If the user is on a page which is in the lower limit.
        if (cursor <= max_links) {
          // If the current loop page is less than max_links.
          show_cursor_flag = true;
        }
      } else if (current >= upper_limit) {
        // Upper limit pages.
        // If the user is on a page which is in the upper limit.
        if (cursor > (page.total - max_links)) {
          // If the current loop page is less than max_links.
          show_cursor_flag = true;
        }
      } else {
        // Middle pages.
        if ( (cursor >= current - adjacent_links) 
        &&   (cursor <= current + adjacent_links) ) {
          show_cursor_flag = true;
        }
      }
    } else {
      // Simple page numbers.
      show_cursor_flag = true;
    }

    // Show Pager.
    if (show_cursor_flag) { %>
      <li>
        <% if (current != cursor) { %>
        <a href="<%= pagination_url(cursor) %>"
           class="pagination-link" 
           aria-label="Goto page <%= cursor %>">
          <%= cursor %>
        </a>
        <% } else { %>
        <a class="pagination-link is-current" 
           aria-label="Page <%= current %>">
          <%= current %>
        </a>
        <% } %>
      </li>
      <% } /* if */ %>
    <% } /* for */ %>

    </ul>
  <% } %>
</nav>
{{< / highlight >}}

Notice that this is not the final code,
as we want to add some indicator and cosmetic later.

* .

-- -- --

### What is Next ?

Consider continue reading [ [Hexo - Pagination - Indicator][local-whats-next] ].
There are, some interesting topic about <code>Pagination in Hexo</code>.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2019/05/20/hexo-pagination-indicator/

[image-ss-05-source]:   {{< assets-ssg >}}/2019/05/hexo-ejs-bulma-tutor-05.zip

[image-ss-05-stripped-01]:        {{< assets-ssg >}}/2019/05/53-adjacent-stripped-01.png
[image-ss-05-stripped-02]:        {{< assets-ssg >}}/2019/05/53-adjacent-stripped-02.png
[image-ss-05-stripped-03]:        {{< assets-ssg >}}/2019/05/53-adjacent-stripped-03.png
[image-ss-05-stripped-04]:        {{< assets-ssg >}}/2019/05/53-adjacent-stripped-04.png
[image-ss-05-stripped-05]:        {{< assets-ssg >}}/2019/05/53-adjacent-stripped-05.png
[image-ss-05-stripped-06]:        {{< assets-ssg >}}/2019/05/53-adjacent-stripped-06.png
[image-ss-05-stripped-07]:        {{< assets-ssg >}}/2019/05/53-adjacent-stripped-07.png
[image-ss-05-stripped-08]:        {{< assets-ssg >}}/2019/05/53-adjacent-stripped-08.png
[image-ss-05-stripped-09]:        {{< assets-ssg >}}/2019/05/53-adjacent-stripped-09.png

[tutor-hexo-config]:    {{< tutor-hexo-bulma >}}/_config.yml
[tutor-layout-index]:   {{< tutor-hexo-bulma >}}/themes/tutor-05/layout/index.ejs
[tutor-p-03-adjacent]:  {{< tutor-hexo-bulma >}}/themes/tutor-05/layout/pagination/03-adjacent.ejs
