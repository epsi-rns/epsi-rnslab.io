---
type   : post
title  : "Hexo - Pagination - Indicator"
date   : 2019-05-20T09:17:35+07:00
slug   : hexo-pagination-indicator
categories: [ssg, frontend]
tags      : [hexo, navigation, bulma, sass]
keywords  : [static site, custom theme, ejs, middle pagination, indicator]
author : epsi
opengraph:
  image: assets/site/images/topics/hexo-bulma.png

toc    : "toc-2019-05-hexo-bulma-step"

excerpt:
  Building Hexo Step by step, with Bulma as stylesheet frontend.
  Middle pagination. Not so for dummies. Combined with Indicator.

---

### Preface

> Goal: Add indicator, and putting all pagination part together.

#### Source Code

You can download the source code of this article here.

* [hexo-ejs-bulma-tutor-05.zip][image-ss-05-source]

Extract and run on CLI using <code>$ npm install</code>.

-- -- --

### 1: Source

I respect copyright.
Most of the code below, especially middle pagination,
copied and pasted from:

* <https://glennmccomb.com/articles/how-to-build-custom-hexo-pagination/>

The rest is my modification.

-- -- --

### 2: Prepare

This step is required.

#### Configuration

As usual in [Hexo - Pagination - Intro][local-pagination-intro].

#### Layout: EJS Index

Consider use <code>pagination/04-indicator</code> layout,
in <code>index.ejs</code>

* <code>themes/tutor-05/layout/index.ejs</code>
  : [gitlab.com/.../layout/index.ejs][tutor-layout-index]

{{< highlight javascript >}}
  <section class="section">
    partial('pagination/04-indicator')
  </section>
{{< / highlight >}}

#### Layout: EJS Index for Screenshot

I omit the section tag for tutorial purpose,
so that I can have enough width,
for all pagination example,
in 480px screen.

{{< highlight javascript >}}
    partial('pagination/04-indicator')
{{< / highlight >}}

#### Layout: Minimal Pagination Code

You should have this minimal code, before you begin.

* <code>themes/tutor-05/layout/pagination/04-indicator.ejs</code>
  : [gitlab.com/.../layout/pagination/04-indicator.ejs][tutor-p-04-indicator].

{{< highlight javascript >}}
<nav class="pagination is-small is-centered"
     role="navigation" aria-label="pagination">
  <% if (page.total > 1) { %>

    <ul class="pagination-list">
    <% if (page.total > max_links) { %>
      ...
    <% } %>
    </ul>

  <% } %>
</nav>
{{< / highlight >}}

#### Javascript: EJS Pagination Indicator

The javascript part is remain the same.

-- -- --

### 3: Preview: General

#### Structure

This consist of at least seven parts:

* Previous Page: **&laquo;**

* First Page: always 1

* Left Indicator

* Middle Pagination: **Glenn McComb**

* Right Indicator

* Last Page: always the same number

* Next Page: **&raquo;**

We will not discuss about **Middle Pagination**,
as it has already been discussed in previous article.

#### HTML Preview

The HTML that we want to achieve is similar as below.

{{< highlight html >}}
<nav class="pagination is-small is-centered" ...>

    <ul class="pagination-list">

      <!-- Previous Page. -->
      <li class="blog-previous"><a class="pagination-previous" ...>«&nbsp;</a></li>

      <!-- First Page. -->
      <li><a class="pagination-link" ...>1</a></li>

      <!-- Early (More Pages) Indicator. -->
      <li><span class="pagination-ellipsis">&hellip;</span></li>

      <li><a class="pagination-link" ...>3</a></li>
      <li><a class="pagination-link" ...>4</a></li>
      <li><a class="pagination-link is-current" >5</a></li>
      <li><a class="pagination-link" ...>6</a></li>
      <li><a class="pagination-link" ...>7</a></li>

      <!-- Late (More Pages) Indicator. -->
      <li><span class="pagination-ellipsis">&hellip;</span></li>

      <!-- Last Page. -->
      <li><a class="pagination-link"  ...>9</a></li>

      <!-- Next Page. -->
      <li class="blog-next"><a class="pagination-next" ...>&nbsp;»</a></li>

    </ul>

</nav>
{{< / highlight >}}

#### Small Preview

This is the complete version.

![Hexo Pagination: Combined Animation][image-ss-05-combined-animation]

#### Wide Preview

Wide version, in responsive context, is slightly different.

![Hexo Pagination: Preview Wide][image-ss-05-preview-wide]

We will use responsive CSS later to achieve this effect.

#### Layout: Pagination Code Skeleton

As usual, the skeleton, to show the complexity.

* <code>themes/tutor-05/layout/pagination/04-indicator.ejs</code>
  : [gitlab.com/.../layout/pagination/04-indicator.ejs][tutor-p-04-indicator].

{{< highlight javascript >}}
// Variable Initialization.

<nav role="navigation" aria-label="pagination">
  <% if (page.total > 1) { %>
    <ul class="pagination-list">

    // Previous Page.
    // First Page.
    // Early (More Pages) Indicator.

    <% 
  var cursor; 
  for (cursor = 1; cursor <= page.total; cursor++) { 
    // Variable Initialization

    if (page.total > max_links) {
      // Complex page numbers.

      // Check between these three:
      // * Lower limit pages, or
      // * Upper limit pages, or
      // * Middle pages.

    } else {
      // Simple page numbers.
      ...
    }

    // Show Pager.
    ...

    <% } %>

    // Late (More Pages) Indicator.
    // Last Page.
    // Next Page.

    </ul>
  <% } %>
</nav>
{{< / highlight >}}

#### Each Pagination

Consider again, have a look at the animation above, frame by frame.

We have from first page (1), to last page (9).

![Hexo Pagination: Indicator Page 1][image-ss-05-combined-01]

![Hexo Pagination: Indicator Page 2][image-ss-05-combined-02]

![Hexo Pagination: Indicator Page 3][image-ss-05-combined-03]

![Hexo Pagination: Indicator Page 4][image-ss-05-combined-04]

![Hexo Pagination: Indicator Page 5][image-ss-05-combined-05]

![Hexo Pagination: Indicator Page 6][image-ss-05-combined-06]

![Hexo Pagination: Indicator Page 7][image-ss-05-combined-07]

![Hexo Pagination: Indicator Page 8][image-ss-05-combined-08]

![Hexo Pagination: Indicator Page 9][image-ss-05-combined-09]

-- -- --

### 4: Variables: Initialization

Consider refresh our memory, about our variables initialization.

#### Javascript: EJS Pagination Indicator

The javascript part is remain similar.
With a slight change, that this pagination is now capable of,
handling many kind of standard pages [archive, category, tag].

{{< highlight javascript >}}
/*
* Helper function
*/

function pagination_url(number) {
  var path;

  // default for index
  var path = config.index_generator.path;
  
  // dirty quick fix, avoid double (//)
  if (path=='/') { path = '' }
  
  if (is_archive()){
    path = '/' + config.archive_dir;

    if (is_month()){
      // trailing zero
      var month = ( page.month < 10 ? '0' + page.month : page.month );
      path += '/' + page.year + '/' + month;
    } else if (is_year()){
      path += '/' + page.year;
    }
  } else if (is_category()){
    path = '/' + config.category_dir + '/' + page.category;
  } else if (is_tag()){
    path = '/' + config.tag_dir + '/' + page.tag;
  }

  if (number>1) {
      path = path + '/' + config.pagination_dir + '/' + number;
  }

  return url_for(path);
}

/* 
* Pagination links 
* https://glennmccomb.com/articles/how-to-build-custom-hugo-pagination/
* Adjacent: Number of links either side of the current page
*/

const adjacent_links = 2;
var current          = page.current;
var max_links        = (adjacent_links * 2) + 1;
var lower_limit      = 1 + adjacent_links;
var upper_limit      = page.total - adjacent_links;
{{< / highlight >}}

* <code>.</code>

-- -- --

### 5: Navigation: Previous and Next

It is similar to our simple pagination.
Except that, now we use dictionary.

#### Navigation: Previous

{{< highlight javascript >}}
      <!-- Previous Page. -->
      <li class="blog-previous">
      <% if (page.prev_link) { %>
        <a class="pagination-previous"
           href="<%= url_for(page.prev_link) %>" 
           rel="prev">&laquo;&nbsp;</a>
      <% } else { %>
        <a class="pagination-previous"
           title="This is the first page"
           disabled>&laquo;&nbsp;</a>
      <% } %>
      </li>
{{< / highlight >}}

#### Navigation: Next

{{< highlight javascript >}}
      <!-- Next Page. -->
      <li class="blog-next">
      <% if (page.next_link) { %>
        <a class="pagination-next"
           href="<%= url_for(page.next_link) %>" 
           rel="next">&nbsp;&raquo;</a>
      <% } else { %>
        <a class="pagination-next"
           title="This is the last page"
           disabled>&nbsp;&raquo;</a>
      <% } %>
      </li>
{{< / highlight >}}

-- -- --

### 6: Navigation: First and Last

It is different to our simple pagination.
Although it is based on the same logic.

#### Navigation: First

This will not be shown, if it is already be shown middle pagination.

{{< highlight javascript >}}
    <% if (page.total > max_links) { %>
      <% if (current - adjacent_links > 1) { %>
      <!-- First Page. -->
      <li>
        <a href="<%= pagination_url(1) %>"
           class="pagination-link" 
           aria-label="Goto page 1"
          >1</a>
      </li>
      <% } %>
    <% } %>
{{< / highlight >}}

#### Navigation: Last

This will not be shown, if it is already be shown middle pagination.

{{< highlight javascript >}}
    <% if (page.total > max_links) { %>
      <% if (current + adjacent_links < page.total) { %>
      <!-- Last Page. -->
      <li>
        <a href="<%= pagination_url(page.total) %>"
           class="pagination-link" 
           aria-label="Goto page <%= page.total %>"
          ><%= page.total %></a>
      </li>
      <% } %>
    <% } %>
{{< / highlight >}}

-- -- --

### 7: Indicator: Left and Right

These will only be shown, only if necessary.

#### Indicator: Left

{{< highlight javascript >}}
    <% if (page.total > max_links) { %>
      <% if (current - adjacent_links > 2) { %>
      <!-- Early (More Pages) Indicator. -->
      <li>
        <span class="pagination-ellipsis">&hellip;</span>
      </li>
      <% } %>
    <% } %>
{{< / highlight >}}

#### Indicator: Right

{{< highlight javascript >}}
    <% if (page.total > max_links) { %>
      <% if (current + adjacent_links < page.total - 1) { %>
      <!-- Late (More Pages) Indicator. -->
      <li>
        <span class="pagination-ellipsis">&hellip;</span>
      </li>
      <% } %>
    <% } %>
{{< / highlight >}}

-- -- --

### 8: Combined Code

It is about the right time to put all the code together.

![Hexo Pagination: Combined Animation][image-ss-05-combined-animation]

* <code>themes/tutor-05/layout/pagination/04-indicator.ejs</code>
  : [gitlab.com/.../layout/pagination/04-indicator.ejs][tutor-p-04-indicator].

{{< highlight javascript >}}
<%
/*
* Helper function
*/

function pagination_url(number) {
  var path;

  // default for index
  var path = config.index_generator.path;
  
  // dirty quick fix, avoid double (//)
  if (path=='/') { path = '' }
  
  if (is_archive()){
    path = '/' + config.archive_dir;

    if (is_month()){
      // trailing zero
      var month = ( page.month < 10 ? '0' + page.month : page.month );
      path += '/' + page.year + '/' + month;
    } else if (is_year()){
      path += '/' + page.year;
    }
  } else if (is_category()){
    path = '/' + config.category_dir + '/' + page.category;
  } else if (is_tag()){
    path = '/' + config.tag_dir + '/' + page.tag;
  }

  if (number>1) {
      path = path + '/' + config.pagination_dir + '/' + number;
  }

  return url_for(path);
}

/* 
* Pagination links 
* https://glennmccomb.com/articles/how-to-build-custom-hugo-pagination/
* Adjacent: Number of links either side of the current page
*/

var current        = page.current;
var adjacent_links = 2;
var max_links      = (adjacent_links * 2) + 1;
var lower_limit    = 1 + adjacent_links;
var upper_limit    = page.total - adjacent_links;
%>

<nav class="pagination is-small is-centered"
     role="navigation" aria-label="pagination">
  <% if (page.total > 1) { %>

    <ul class="pagination-list">

      <!-- Previous Page. -->
      <li class="blog-previous">
      <% if (page.prev_link) { %>
        <a class="pagination-previous"
           href="<%= url_for(page.prev_link) %>" 
           rel="prev">&laquo;&nbsp;</a>
      <% } else { %>
        <a class="pagination-previous"
           title="This is the first page"
           disabled>&laquo;&nbsp;</a>
      <% } %>
      </li>

    <% if (page.total > max_links) { %>
      <% if (current - adjacent_links > 1) { %>
      <!-- First Page. -->
      <li>
        <a href="<%= pagination_url(1) %>"
           class="pagination-link" 
           aria-label="Goto page 1"
          >1</a>
      </li>
      <% } %>

      <% if (current - adjacent_links > 2) { %>
      <!-- Early (More Pages) Indicator. -->
      <li>
        <span class="pagination-ellipsis">&hellip;</span>
      </li>
      <% } %>
    <% } %>

    <% 
  var cursor; 
  for (cursor = 1; cursor <= page.total; cursor++) { 
    var show_cursor_flag = false;


    if (page.total > max_links) {
      // Complex page numbers.

      if (current <= lower_limit) {
        // Lower limit pages.
        // If the user is on a page which is in the lower limit.
        if (cursor <= max_links) {
          // If the current loop page is less than max_links.
          show_cursor_flag = true;
        }
      } else if (current >= upper_limit) {
        // Upper limit pages.
        // If the user is on a page which is in the upper limit.
        if (cursor > (page.total - max_links)) {
          // If the current loop page is less than max_links.
          show_cursor_flag = true;
        }
      } else {
        // Middle pages.
        if ( (cursor >= current - adjacent_links) 
        &&   (cursor <= current + adjacent_links) ) {
          show_cursor_flag = true;
        }
      }
    } else {
      // Simple page numbers.
      show_cursor_flag = true;
    }

    // Show Pager.
    if (show_cursor_flag) { %>
      <li>
        <% if (current != cursor) { %>
        <a href="<%= pagination_url(cursor) %>"
           class="pagination-link" 
           aria-label="Goto page <%= cursor %>">
          <%= cursor %>
        </a>
        <% } else { %>
        <a class="pagination-link is-current" 
           aria-label="Page <%= current %>">
          <%= current %>
        </a>
        <% } %>
      </li>
      <% } /* if */ %>
    <% } /* for */ %>

    <% if (page.total > max_links) { %>
      <% if (current + adjacent_links < page.total - 1) { %>
      <!-- Late (More Pages) Indicator. -->
      <li>
        <span class="pagination-ellipsis">&hellip;</span>
      </li>
      <% } %>

      <% if (current + adjacent_links < page.total) { %>
      <!-- Last Page. -->
      <li>
        <a href="<%= pagination_url(page.total) %>"
           class="pagination-link" 
           aria-label="Goto page <%= page.total %>"
          ><%= page.total %></a>
      </li>
      <% } %>
    <% } %>

      <!-- Next Page. -->
      <li class="blog-next">
      <% if (page.next_link) { %>
        <a class="pagination-next"
           href="<%= url_for(page.next_link) %>" 
           rel="next">&nbsp;&raquo;</a>
      <% } else { %>
        <a class="pagination-next"
           title="This is the last page"
           disabled>&nbsp;&raquo;</a>
      <% } %>
      </li>

    </ul>
  <% } %>
</nav>
{{< / highlight >}}

* <code>.</code>

-- -- --

### 9: Responsive: Simple

Before we continue to the next pagination article about responsive,
consider having this very simple example using bulma.

![Hexo Pagination: Preview Wide][image-ss-05-preview-wide]

This will show the word **previous** and **next** for tablet page.

#### SASS: Custom Pagination

I'm using Bulma breakpoints.

* <code>themes/tutor-05/sass/css/_pagination.scss</code>
  : [gitlab.com/.../sass/_pagination.scss][tutor-hexo-sass-pagination].

{{< highlight css >}}
+tablet
  li.blog-previous a:after
    content: " previous"
  li.blog-next a:before
    content: "next "
{{< / highlight >}}

#### SASS: Bulma Breakpoint Variables.

Bulma 0.7 breakpoints are defined as below.

* <code>themes/tutor-05/sass/vendors/bulma/utilities/initial-variables.sass</code>

{{< highlight sass >}}
// The container horizontal gap,
// which acts as the offset for breakpoints

$gap        : 64px !default
$tablet     : 769px !default
$desktop    : 960px + (2 * $gap) !default
$widescreen : 1152px + (2 * $gap) !default
$fullhd     : 1344px + (2 * $gap) !default
{{< / highlight >}}

-- -- --

### What is Next ?

Consider continue reading [ [Hexo - Pagination - Responsive][local-whats-next] ].
There are, some interesting topic about <code>Pagination in Hexo</code>.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2019/05/21/hexo-pagination-responsive/
[local-pagination-intro]:   {{< baseurl >}}ssg/2019/05/17/hexo-pagination-intro/

[image-ss-05-source]:   {{< assets-ssg >}}/2019/05/hexo-ejs-bulma-tutor-05.zip

[image-ss-05-combined-animation]: {{< assets-ssg >}}/2019/05/54-hexo-bulma-indicator-animate.gif
[image-ss-05-preview-wide]:       {{< assets-ssg >}}/2019/05/54-indicator-wide.png

[image-ss-05-combined-01]:        {{< assets-ssg >}}/2019/05/54-adjacent-combined-01.png
[image-ss-05-combined-02]:        {{< assets-ssg >}}/2019/05/54-adjacent-combined-02.png
[image-ss-05-combined-03]:        {{< assets-ssg >}}/2019/05/54-adjacent-combined-03.png
[image-ss-05-combined-04]:        {{< assets-ssg >}}/2019/05/54-adjacent-combined-04.png
[image-ss-05-combined-05]:        {{< assets-ssg >}}/2019/05/54-adjacent-combined-05.png
[image-ss-05-combined-06]:        {{< assets-ssg >}}/2019/05/54-adjacent-combined-06.png
[image-ss-05-combined-07]:        {{< assets-ssg >}}/2019/05/54-adjacent-combined-07.png
[image-ss-05-combined-08]:        {{< assets-ssg >}}/2019/05/54-adjacent-combined-08.png
[image-ss-05-combined-09]:        {{< assets-ssg >}}/2019/05/54-adjacent-combined-09.png
[image-ss-05-combined-10]:        {{< assets-ssg >}}/2019/05/54-adjacent-combined-10.png


[tutor-hexo-config]:    {{< tutor-hexo-bulma >}}/_config.yml
[tutor-layout-index]:   {{< tutor-hexo-bulma >}}/themes/tutor-05/layout/index.ejs
[tutor-p-04-indicator]: {{< tutor-hexo-bulma >}}/themes/tutor-05/layout/pagination/04-indicator.ejs

[tutor-hexo-sass-main]:          {{< tutor-hexo-bulma >}}/themes/tutor-05/sass/css/main.scss
[tutor-hexo-sass-pagination]:    {{< tutor-hexo-bulma >}}/themes/tutor-05/sass/css/_pagination.scss
