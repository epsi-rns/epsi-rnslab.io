---
type   : post
title  : "Hexo - Pagination - Responsive"
date   : 2019-05-21T09:17:35+07:00
slug   : hexo-pagination-responsive
categories: [ssg, frontend]
tags      : [hexo, navigation, bulma, sass]
keywords  : [static site, custom theme, ejs, pagination, responsive breakpoints]
author : epsi
opengraph:
  image: assets/site/images/topics/hexo-bulma.png

toc    : "toc-2019-05-hexo-bulma-step"

excerpt:
  Building Hexo Step by step, with Bulma as stylesheet frontend.
  Responsive pagination using Bulma. Not so for dummies.

---

### Preface

> Goal: Bringing responsive pagination, using mobile first.

While writing this CSS code,
I can't claim myself as a developer.
Because CSS is not a programming language.

But weird that, responsive design has their own logic.
So yeah, I have to code, a little.

#### Source Code

You can download the source code of this article here.

* [hexo-ejs-bulma-tutor-05.zip][image-ss-05-source]

Extract and run on CLI using <code>$ npm install</code>.

-- -- --

### 1: Source

I respect copyright.
The code below inspired by:

* https://www.timble.net/blog/2015/05/better-pagination-for-jekyll/

* https://github.com/timble/jekyll-pagination

I made a slight modification.
But of course the logic remain the same.

> Of course, this article is talking about SASS

-- -- --

### 2: Prepare

This step is required.

#### Preview: General

This is the complete version.

![Hexo Pagination: Responsive Animation][image-ss-05-responsive-animation]

#### Configuration

As usual in [Hexo - Pagination - Intro][local-pagination-intro].

#### Layout: EJS Index

Consider use <code>pagination/05-responsive</code> layout,
in <code>index.ejs</code>

* <code>themes/tutor-05/layout/index.ejs</code>
  : [gitlab.com/.../layout/index.ejs][tutor-layout-index]

#### SASS: Main

* <code>themes/tutor-05/sass/css/main.scss</code>
  : [gitlab.com/.../sass/main.scss][tutor-hexo-sass-main].

{{< highlight sass >}}
// dart-sass --watch -I sass sass/css:source/css/

...

@import "panel"
@import "blog"
@import "list"
@import "pagination"
{{< / highlight >}}

-- -- --

### 3: Navigation: HTML Class

#### The Final Result.

Consider have a look at the image below.

![Hexo Pagination: Responsive 1][image-ss-05-responsive-01]

The HTML that we want to achieve is similar as below.

{{< highlight html >}}
<nav class="pagination is-small is-centered" ...>

    <ul class="pagination-list">
      <!-- Previous Page. -->
      <li class="blog-previous"><a class="pagination-previous" ...>«&nbsp;</a></li>

      <!-- First Page. -->
      <li class="first"><a class="pagination-link" ...>1</a></li>

      <!-- Early (More Pages) Indicator. -->
      <li class="pages-indicator first"><span class="pagination-ellipsis">&hellip;</span></li>

      <li class="pagination--offset-2"><a class="pagination-link" ...>3</a></li>
      <li class="pagination--offset-1"><a class="pagination-link" ...>4</a></li>
      <li class="pagination--offset-0"><a class="pagination-link is-current" ...>5</a></li>
      <li class="pagination--offset-1"><a class="pagination-link" ...>6</a></li>
      <li class="pagination--offset-2"><a class="pagination-link" ...>7</a></li>

      <!-- Late (More Pages) Indicator. -->
      <li class="pages-indicator last"><span class="pagination-ellipsis">&hellip;</span></li>

      <!-- Last Page. -->
      <li class="last"><a class="pagination-link" ...>9</a></li>

      <!-- Next Page. -->
      <li class="blog-next"><a class="pagination-next" ...>&nbsp;»</a></li>
    </ul>

</nav>
{{< / highlight >}}

#### Middle Pagination

All you need to care is, only these lines.

{{< highlight html >}}
      <li class="pagination--offset-2">...</li>
      <li class="pagination--offset-1">...</li>
      <li class="pagination--offset-0">...</li>
      <li class="pagination--offset-1">...</li>
      <li class="pagination--offset-2">...</li>
{{< / highlight >}}

Our short term goal is,
to put the <code>pagination--offset</code> class.

#### Layout: Pagination Code Skeleton

As usual, the skeleton, to show the complexity.

* <code>themes/tutor-05/layout/pagination/05-responsive.ejs</code>
  : [gitlab.com/.../layout/pagination/05-responsive.ejs][tutor-p-05-responsive].

{{< highlight javascript >}}
// Variable Initialization.

<nav role="navigation" aria-label="pagination">
  <% if (page.total > 1) { %>
    <ul class="pagination-list">

    // Previous Page.
    // First Page.
    // Early (More Pages) Indicator.

    <% 
  var cursor; 
  for (cursor = 1; cursor <= page.total; cursor++) { 
    // Variable Initialization

    if (page.total > max_links) {
      // Complex page numbers.

      // Check between these three:
      // * Lower limit pages, or
      // * Upper limit pages, or
      // * Middle pages.

    } else {
      // Simple page numbers.
      ...
    }
    
    // Calculate Offset Class.
    ...

    // Show Pager.
    ...

    <% } %>

    // Late (More Pages) Indicator.
    // Last Page.
    // Next Page.

    </ul>
  <% } %>
</nav>
{{< / highlight >}}

#### Calculate Offset Value

Notice this part:

{{< highlight javascript >}}
      // Calculate Offset Class.
      var page_offset_class = 'pagination--offset-' 
                            + Math.abs(cursor - current);
{{< / highlight >}}

Notice the new variable called <code>page_offset_class</code>.

#### Using Offset Class

All we need is just adding the offset class.

{{< highlight javascript >}}
      <li class="<%= page_offset_class %>">
        ...
      </li>
{{< / highlight >}}

The real code, is also not very simple.

{{< highlight javascript >}}
      <li class="<%= page_offset_class %>">
        <% if (current != cursor) { %>
        <a href="<%= pagination_url(cursor) %>"
           class="pagination-link" 
           aria-label="Goto page <%= cursor %>">
          <%= cursor %>
        </a>
        <% } else { %>
        <a class="pagination-link is-current" 
           aria-label="Page <%= current %>">
          <%= current %>
        </a>
        <% } %>
      </li>
{{< / highlight >}}

#### Combined Code

{{< highlight javascript >}}
    if (show_cursor_flag) { 
      // Calculate Offset Class.
      var page_offset_class = 'pagination--offset-' 
                            + Math.abs(cursor - current);

      // Show Pager.
      %>
      <li class="<%= page_offset_class %>">
        <% if (current != cursor) { %>
        <a href="<%= pagination_url(cursor) %>"
           class="pagination-link" 
           aria-label="Goto page <%= cursor %>">
          <%= cursor %>
        </a>
        <% } else { %>
        <a class="pagination-link is-current" 
           aria-label="Page <%= current %>">
          <%= current %>
        </a>
        <% } %>
      </li>
      <% } /* if */ %>
    <% } /* for */ %>
{{< / highlight >}}

That is all.
Now that the HTML part is ready, we should go on,
by setting up the responsive breakpoints using SCSS.

* <code>.</code>

-- -- --

### 4: Responsive: Breakpoints

Responsive is easy if you understand the logic.

> It is all about breakpoints.

#### Preview: Each Breakpoint

Consider again, have a look at the animation above, frame by frame.
We have at least five breakpoint as six figures below:

![Hexo Pagination: Responsive 1][image-ss-05-responsive-01]

![Hexo Pagination: Responsive 2][image-ss-05-responsive-02]

![Hexo Pagination: Responsive 3][image-ss-05-responsive-03]

![Hexo Pagination: Responsive 4][image-ss-05-responsive-04]

![Hexo Pagination: Responsive 5][image-ss-05-responsive-05]

![Hexo Pagination: Responsive 6][image-ss-05-responsive-06]

#### SASS: Bulma Custom Breakpoint Variables.

I'm using custom breakpoint, instead of Bulma 7.x breakpoints.

* <code>themes/tutor-05/sass/css/_pagination.scss</code>
  : [gitlab.com/.../sass/_pagination.scss][tutor-hexo-sass-pagination].

{{< highlight sass >}}
// Breakpoint

$xs1: 0
$xs2: 320px
$xs3: 400px
$xs4: 480px
$sm1: 576px
$sm2: 600px
$md:  768px
$lg:  992px
$xl:  1200px
{{< / highlight >}}

The name inspired by Bootsrap,
but it has nothing do with Bootstrap.

#### SASS: Bulma Breakpoint Skeleton

With breakpoint above, we can setup css skeleton, with empty css rules.

{{< highlight sass >}}
ul.pagination-list
  +from($xs1)
  +from($xs2)
  +from($xs3)
  +from($xs4)
  +from($sm1)
  +from($sm2)
  +from($md)
  +from($lg)
  +from($xl)
{{< / highlight >}}

#### SASS: Using Bulma Breakpoint: Simple

We can fill any rules, as below:

{{< highlight sass >}}
+tablet
  li.blog-previous a:after
    content: " previous"
  li.blog-next a:before
    content: "next "
{{< / highlight >}}

#### SASS: Using Custom Breakpoint: Pagination Offset

We can fill any rules, inside <code>pagination</code> class as below:

{{< highlight sass >}}
// Responsiveness

ul.pagination-list
  li.pagination--offset-1,
  li.pagination--offset-2,
  li.pagination--offset-3,
  li.pagination--offset-4,
  li.pagination--offset-5,
  li.pagination--offset-6,
  li.pagination--offset-7
    display: none
  +from($xs3)
    li.pagination--offset-1
      display: inline-block
  +from($xs4)
    li.pagination--offset-2
      display: inline-block
  +from($sm1)
    li.pagination--offset-3
      display: inline-block
  +from($sm2)
    li.pagination--offset-4
      display: inline-block
  +from($md)
    li.pagination--offset-5,
    li.pagination--offset-6
      display: inline-block
  +from($lg)
    li.pagination--offset-7
      display: inline-block
{{< / highlight >}}

This setup breakpoint is actually up to you.
You may change ti suit whatever you need.

#### SASS: Responsive Indicator

You can also add CSS rules for indicator.

{{< highlight sass >}}
// Responsiveness

ul.pagination-list
  li.first,
  li.last,
  li.pages-indicator
    display: none
  +from($xs2)
    li.pages-indicator
      display: inline-block
  +from($sm1)
    li.first,
    li.last
      display: inline-block
{{< / highlight >}}

Short and simple.

#### SASS: Hover Effect

If you desire, a slight enhancement without breaking the original looks,
hover is a good idea.

{{< highlight sass >}}
// hover color

ul.pagination-list li 
  a:hover
    box-shadow: 0 0.5rem 1rem rgba(0,0,0,0.15)
    background-color: $yellow
    color: #000
  a.is-current:hover
    background-color: $gray
    color: #fff
{{< / highlight >}}

#### SASS: Complete Code

Now you can have the complete code as below:

* <code>themes/tutor-05/sass/css/_pagination.scss</code>
  : [gitlab.com/.../sass/_pagination.scss][tutor-hexo-sass-pagination].

{{< highlight sass >}}
+tablet
  li.blog-previous a:after
    content: " previous"
  li.blog-next a:before
    content: "next "

// Breakpoint

$xs1: 0
$xs2: 320px
$xs3: 400px
$xs4: 480px
$sm1: 576px
$sm2: 600px
$md:  768px
$lg:  992px
$xl:  1200px

// Responsiveness

ul.pagination-list
  li.first,
  li.last,
  li.pages-indicator
    display: none
  li.pagination--offset-1,
  li.pagination--offset-2,
  li.pagination--offset-3,
  li.pagination--offset-4,
  li.pagination--offset-5,
  li.pagination--offset-6,
  li.pagination--offset-7
    display: none
  +from($xs1)
  +from($xs2)
    li.pages-indicator
      display: inline-block
  +from($xs3)
    li.pagination--offset-1
      display: inline-block
  +from($xs4)
    li.pagination--offset-2
      display: inline-block
  +from($sm1)
    li.first,
    li.last,
    li.pagination--offset-3
      display: inline-block
  +from($sm2)
    li.pagination--offset-4
      display: inline-block
  +from($md)
    li.pagination--offset-5,
    li.pagination--offset-6
      display: inline-block
  +from($lg)
    li.pagination--offset-7
      display: inline-block
  +from($xl)

// hover color

ul.pagination-list li 
  a:hover
    box-shadow: 0 0.5rem 1rem rgba(0,0,0,0.15)
    background-color: $yellow
    color: #000
  a.is-current:hover
    background-color: $gray
    color: #fff
{{< / highlight >}}

-- -- --

### 5: Summary

You can have a look at our complete code here:

* <code>themes/tutor-05/layout/pagination/05-responsive.ejs</code>
  : [gitlab.com/.../layout/pagination/05-responsive.ejs][tutor-p-05-responsive].
 
{{< highlight javascript >}}
<%
/*
* Helper function
*/

function pagination_url(number) {
  var path;

  // default for index
  var path = config.index_generator.path;
  
  // dirty quick fix, avoid double (//)
  if (path=='/') { path = '' }
  
  if (is_archive()){
    path = '/' + config.archive_dir;

    if (is_month()){
      // trailing zero
      var month = ( page.month < 10 ? '0' + page.month : page.month );
      path += '/' + page.year + '/' + month;
    } else if (is_year()){
      path += '/' + page.year;
    }
  } else if (is_category()){
    path = '/' + config.category_dir + '/' + page.category;
  } else if (is_tag()){
    path = '/' + config.tag_dir + '/' + page.tag;
  }

  if (number>1) {
      path = path + '/' + config.pagination_dir + '/' + number;
  }

  return url_for(path);
}

/* 
* Pagination links 
* https://glennmccomb.com/articles/how-to-build-custom-hugo-pagination/
* Adjacent: Number of links either side of the current page
*/

var current        = page.current;
var adjacent_links = 2;
var max_links      = (adjacent_links * 2) + 1;
var lower_limit    = 1 + adjacent_links;
var upper_limit    = page.total - adjacent_links;
%>

<nav class="pagination is-small is-centered" 
     role="navigation" aria-label="pagination">

  <% if (page.total > 1) { %>
    <ul class="pagination-list">

      <!-- Previous Page. -->
      <li class="blog-previous">
      <% if (page.prev_link) { %>
        <a class="pagination-previous"
           href="<%= url_for(page.prev_link) %>" 
           rel="prev">&laquo;&nbsp;</a>
      <% } else { %>
        <a class="pagination-previous"
           title="This is the first page"
           disabled>&laquo;&nbsp;</a>
      <% } %>
      </li>

    <% if (page.total > max_links) { %>
      <% if (current - adjacent_links > 1) { %>
      <!-- First Page. -->
      <li class="first">
        <a href="<%= pagination_url(1) %>"
           class="pagination-link" 
           aria-label="Goto page 1"
          >1</a>
      </li>
      <% } %>

      <% if (current - adjacent_links > 2) { %>
      <!-- Early (More Pages) Indicator. -->
      <li class="pages-indicator first">
        <span class="pagination-ellipsis">&hellip;</span>
      </li>
      <% } %>
    <% } %>

    <% 
  var cursor; 
  for (cursor = 1; cursor <= page.total; cursor++) { 
    var show_cursor_flag = false;


    if (page.total > max_links) {
      // Complex page numbers.

      if (current <= lower_limit) {
        // Lower limit pages.
        // If the user is on a page which is in the lower limit.
        if (cursor <= max_links) {
          // If the current loop page is less than max_links.
          show_cursor_flag = true;
        }
      } else if (current >= upper_limit) {
        // Upper limit pages.
        // If the user is on a page which is in the upper limit.
        if (cursor > (page.total - max_links)) {
          // If the current loop page is less than max_links.
          show_cursor_flag = true;
        }
      } else {
        // Middle pages.
        if ( (cursor >= current - adjacent_links) 
        &&   (cursor <= current + adjacent_links) ) {
          show_cursor_flag = true;
        }
      }
    } else {
      // Simple page numbers.
      show_cursor_flag = true;
    }

    if (show_cursor_flag) { 
      // Calculate Offset Class.
      var page_offset_class = 'pagination--offset-' 
                            + Math.abs(cursor - current);

      // Show Pager.
      %>
      <li class="<%= page_offset_class %>">
        <% if (current != cursor) { %>
        <a href="<%= pagination_url(cursor) %>"
           class="pagination-link" 
           aria-label="Goto page <%= cursor %>">
          <%= cursor %>
        </a>
        <% } else { %>
        <a class="pagination-link is-current" 
           aria-label="Page <%= current %>">
          <%= current %>
        </a>
        <% } %>
      </li>
      <% } /* if */ %>
    <% } /* for */ %>

    <% if (page.total > max_links) { %>
      <% if (current + adjacent_links < page.total - 1) { %>
      <!-- Late (More Pages) Indicator. -->
      <li class="pages-indicator last">
        <span class="pagination-ellipsis">&hellip;</span>
      </li>
      <% } %>

      <% if (current + adjacent_links < page.total) { %>
      <!-- Last Page. -->
      <li class="last">
        <a href="<%= pagination_url(page.total) %>"
           class="pagination-link" 
           aria-label="Goto page <%= page.total %>"
          ><%= page.total %></a>
      </li>
      <% } %>
    <% } %>

      <!-- Next Page. -->
      <li class="blog-next">
      <% if (page.next_link) { %>
        <a class="pagination-next"
           href="<%= url_for(page.next_link) %>" 
           rel="next">&nbsp;&raquo;</a>
      <% } else { %>
        <a class="pagination-next"
           title="This is the last page"
           disabled>&nbsp;&raquo;</a>
      <% } %>
      </li>
    </ul>
  <% } %>
</nav>
{{< / highlight >}}

We will have to complete the code later, in the next article.
This has been a long article.
We still need a few changes, for screen reader.

* <code>.</code>

#### Browser: Pagination Preview

Finally the animated version.

![Hexo Pagination: Responsive Animation][image-ss-05-responsive-animation]

-- -- --

### What is Next ?

Looks good right?
This kitten, is still with me.
Just like the kitten, do not get rest yet!
Our pagination tutorial still have some materials to go.

![adorable kitten][image-kitten]

Consider continue reading [ [Hexo - Pagination - Screen Reader][local-whats-next] ].
There are, some interesting topic about <code>Pagination in Hexo</code>.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2019/05/22/hexo-pagination-screenreader/
[local-pagination-intro]:   {{< baseurl >}}ssg/2019/05/17/hexo-pagination-intro/
[image-kitten]:             {{< baseurl >}}/assets/site/images/cats/rambu-01.jpg

[image-ss-05-source]:   {{< assets-ssg >}}/2019/05/hexo-ejs-bulma-tutor-05.zip

[image-ss-05-responsive-animation]: {{< assets-ssg >}}/2019/05/55-hexo-bulma-responsive-animate.gif

[image-ss-05-responsive-01]:        {{< assets-ssg >}}/2019/05/55-responsive-01.png
[image-ss-05-responsive-02]:        {{< assets-ssg >}}/2019/05/55-responsive-02.png
[image-ss-05-responsive-03]:        {{< assets-ssg >}}/2019/05/55-responsive-03.png
[image-ss-05-responsive-04]:        {{< assets-ssg >}}/2019/05/55-responsive-04.png
[image-ss-05-responsive-05]:        {{< assets-ssg >}}/2019/05/55-responsive-05.png
[image-ss-05-responsive-06]:        {{< assets-ssg >}}/2019/05/55-responsive-06.png

[tutor-hexo-config]:    {{< tutor-hexo-bulma >}}/_config.yml
[tutor-layout-index]:   {{< tutor-hexo-bulma >}}/themes/tutor-05/layout/index.ejs
[tutor-p-05-responsive]:{{< tutor-hexo-bulma >}}/themes/tutor-05/layout/pagination/05-responsive.ejs

[tutor-hexo-sass-main]:          {{< tutor-hexo-bulma >}}/themes/tutor-05/sass/css/main.scss
[tutor-hexo-sass-pagination]:    {{< tutor-hexo-bulma >}}/themes/tutor-05/sass/css/_pagination.scss
