---
type   : post
title  : "Hexo - Pagination - Screen Reader"
date   : 2019-05-22T09:17:35+07:00
slug   : hexo-pagination-screenreader
categories: [ssg, frontend]
tags      : [hexo, navigation, bulma]
keywords  : [static site, custom theme, ejs, pagination, screen reader]
author : epsi
opengraph:
  image: assets/site/images/topics/hexo-bulma.png

toc    : "toc-2019-05-hexo-bulma-step"

excerpt:
  Building Hexo Step by step, with Bulma as stylesheet frontend.
  Accessability for pagination using Bulma.
  Complete the custom pagination.
  This the summary code for our pagination.

---

### Preface

> Goal: Bringing screen reader accessability in pagination.

#### Source Code

You can download the source code of this article here.

* [hexo-ejs-bulma-tutor-05.zip][image-ss-05-source]

Extract and run on CLI using <code>$ npm install</code>.

-- -- --

### 1: Source

#### Screen Reader Class

I just follow bulma guidance:

* <https://bulma.io/documentation/modifiers/helpers/>

To hidden content visually,
you simply need to add <code>is-sr-only</code>.

{{< highlight html >}}
<span class="is-sr-only">Hidden Content</span>
{{< / highlight >}}

Alternatively you can use fontawesome class that is similar to bootstrap.

{{< highlight html >}}
<span class="sr-only">Hidden Content</span>
{{< / highlight >}}

This content can be read by screenreader.
You can test using <code>inspect element</code>.

#### Aria Label

Instead of class, we can also utilize <code>aria-label</code>.

{{< highlight html >}}
        <a class="pagination-link is-current" 
           aria-label="Page <%= page.current %>"
           aria-current="true">
          <span class="is-sr-only">Page </span><%= page.current %>
        </a>
{{< / highlight >}}

-- -- --

### 2: Prepare

#### Preview: General

There shoud be nomore preview, because this is screen reader.
Luckily, this article meant to provide example of screenreader.
So I show all buttons in this template example.

![Hexo Pagination: Preview All Buttons][image-ss-05-preview-all-buttons]

You may freely change the appearance.
Just remove any button that you do not need.
Change the class, or even the SASS.
Just remember that in this article,
we should focus on accesability, instead the looks.

#### Configuration

As usual in [Hexo - Pagination - Intro][local-pagination-intro].

#### Layout: EJS Index

Consider use <code>pagination/06-screenreader</code> layout,
in <code>index.ejs</code>

* <code>themes/tutor-05/layout/index.ejs</code>
  : [gitlab.com/.../layout/index.ejs][tutor-layout-index]

-- -- --

### 3: Navigation: Previous and Next

Just a slight modification.

#### Navigation: Previous

{{< highlight html >}}
    <!-- Previous Page. -->
    <% if (page.prev_link) { %>
      <a class="pagination-previous"
         href="<%= url_for(page.prev_link) %>" 
         aria-label="Go to previous page"
         rel="prev">
        <span class="is-sr-only">Go to </span>
        Previous
        <span class="is-sr-only"> page</span>
      </a>
    <% } else { %>
      <a class="pagination-previous"
         title="This is the first page"
         aria-label="First page"
         aria-current="true"
         disabled>Previous
        <span class="is-sr-only"> page</span>
      </a>
    <% } %>
{{< / highlight >}}

#### Navigation: Next

{{< highlight html >}}
    <!-- Next Page. -->
    <% if (page.next_link) { %>
      <a class="pagination-next"
         href="<%= url_for(page.next_link) %>" 
         aria-label="Go to next page"
         rel="next">
        <span class="is-sr-only">Go to </span>
        Next
        <span class="is-sr-only"> page</span>
      </a>
    <% } else { %>
      <a class="pagination-next"
         title="This is the last page"
         aria-label="Last page"
         aria-current="true"
         disabled>Next
        <span class="is-sr-only"> page</span>
      </a>
    <% } %>
{{< / highlight >}}

-- -- --

### 4: Navigation: First and Last

Just a slight modification.

#### Navigation: First

{{< highlight html >}}
    <!-- First Page. -->
    <% if (page.prev != 0) { %>
      <a class="pagination-previous"
         href="<%= pagination_url(1) %>" 
         aria-label="Go to first page"
         data-rel="first">
        <span class="is-sr-only">Go to </span>
        First
        <span class="is-sr-only"> page</span>
      </a>
        <% } else { %>
      <a class="pagination-previous"
         title="This is the first page"
         aria-label="First page"
         aria-current="true"
         disabled>First
        <span class="is-sr-only"> page</span>
      </a>
    <% } %>
{{< / highlight >}}

#### Navigation: Last

{{< highlight html >}}
    <!-- Last Page. -->
    <% if (page.next != 0) { %>
      <a class="pagination-next"
         href="<%= pagination_url(page.total) %>" 
         aria-label="Go to last page"
         data-rel="last">
        <span class="is-sr-only">Go to </span>
        Last
        <span class="is-sr-only"> page</span>
      </a>
    <% } else { %>
      <a class="pagination-next"
         title="This is the last page"
         aria-label="Last page"
         aria-current="true"
         disabled>Last
        <span class="is-sr-only"> page</span>
      </a>
    <% } %>
{{< / highlight >}}

-- -- --

### 5: Middle Navigation: Number

{{< highlight html >}}
      <li class="<%= page_offset_class %>">
        <% if (current != cursor) { %>
        <a href="<%= pagination_url(cursor) %>"
           class="pagination-link" 
           aria-label="Go to page <%= cursor %>">
          <span class="is-sr-only">Go to page </span><%= cursor %>
        </a>
        <% } else { %>
        <a class="pagination-link is-current" 
           aria-label="Page <%= current %>"
           aria-current="true">
          <span class="is-sr-only">Page </span><%= current %>
        </a>
        <% } %>
      </li>
{{< / highlight >}}

-- -- --

### 6: Conclusion

Now comes the final part.

#### Inspect Elements

You can test using screen reader <code>inspect element</code>.

![Hexo Pagination: Preview Inspect Elements][image-ss-05-inspect-elements]

#### Complete Code

As a summary of this pagination tutorial,
the complete code is here below:

* <code>themes/tutor-05/layout/pagination/05-screenreader.ejs</code>
  : [gitlab.com/.../layout/pagination/05-screenreader.ejs][tutor-p-06-screenreader].

{{< highlight html >}}
<%
/*
* Helper function
*/

function pagination_url(number) {
  var path;

  // default for index
  var path = config.index_generator.path;
  
  // dirty quick fix, avoid double (//)
  if (path=='/') { path = '' }
  
  if (is_archive()){
    path = '/' + config.archive_dir;

    if (is_month()){
      // trailing zero
      var month = ( page.month < 10 ? '0' + page.month : page.month );
      path += '/' + page.year + '/' + month;
    } else if (is_year()){
      path += '/' + page.year;
    }
  } else if (is_category()){
    path = '/' + config.category_dir + '/' + page.category;
  } else if (is_tag()){
    path = '/' + config.tag_dir + '/' + page.tag;
  }

  if (number>1) {
      path = path + '/' + config.pagination_dir + '/' + number;
  }

  return url_for(path);
}

/* 
* Pagination links 
* https://glennmccomb.com/articles/how-to-build-custom-hugo-pagination/
* Adjacent: Number of links either side of the current page
*/

var current        = page.current;
var adjacent_links = 2;
var max_links      = (adjacent_links * 2) + 1;
var lower_limit    = 1 + adjacent_links;
var upper_limit    = page.total - adjacent_links;
%>

<nav class="pagination is-small is-centered" 
     role="navigation" aria-label="pagination">

  <% if (page.total > 1) { %>

    <!-- First Page. -->
    <% if (page.prev != 0) { %>
      <a class="pagination-previous"
         href="<%= pagination_url(1) %>" 
         aria-label="Go to first page"
         data-rel="first">
        <span class="is-sr-only">Go to </span>
        First
        <span class="is-sr-only"> page</span>
      </a>
        <% } else { %>
      <a class="pagination-previous"
         title="This is the first page"
         aria-label="First page"
         aria-current="true"
         disabled>First
        <span class="is-sr-only"> page</span>
      </a>
    <% } %>

    <!-- Previous Page. -->
    <% if (page.prev_link) { %>
      <a class="pagination-previous"
         href="<%= url_for(page.prev_link) %>" 
         aria-label="Go to previous page"
         rel="prev">
        <span class="is-sr-only">Go to </span>
        Previous
        <span class="is-sr-only"> page</span>
      </a>
    <% } else { %>
      <a class="pagination-previous"
         title="This is the first page"
         aria-label="First page"
         aria-current="true"
         disabled>Previous
        <span class="is-sr-only"> page</span>
      </a>
    <% } %>

    <!-- Next Page. -->
    <% if (page.next_link) { %>
      <a class="pagination-next"
         href="<%= url_for(page.next_link) %>" 
         aria-label="Go to next page"
         rel="next">
        <span class="is-sr-only">Go to </span>
        Next
        <span class="is-sr-only"> page</span>
      </a>
    <% } else { %>
      <a class="pagination-next"
         title="This is the last page"
         aria-label="Last page"
         aria-current="true"
         disabled>Next
        <span class="is-sr-only"> page</span>
      </a>
    <% } %>

    <!-- Last Page. -->
    <% if (page.next != 0) { %>
      <a class="pagination-next"
         href="<%= pagination_url(page.total) %>" 
         aria-label="Go to last page"
         data-rel="last">
        <span class="is-sr-only">Go to </span>
        Last
        <span class="is-sr-only"> page</span>
      </a>
    <% } else { %>
      <a class="pagination-next"
         title="This is the last page"
         aria-label="Last page"
         aria-current="true"
         disabled>Last
        <span class="is-sr-only"> page</span>
      </a>
    <% } %>

    <!-- Main Pagination List -->
    <ul class="pagination-list">

      <!-- Previous Page. -->
      <li class="blog-previous-no-responsive">
      <% if (page.prev_link) { %>
        <a class="pagination-previous"
           href="<%= url_for(page.prev_link) %>" 
           aria-label="Go to previous page"
           rel="prev">&laquo;&nbsp;
           <span class="is-sr-only">Go to previous page</span>
        </a>
      <% } else { %>
        <a class="pagination-previous"
           title="This is the first page"
           aria-label="First page"
           aria-current="true"
           disabled>&laquo;&nbsp;
        <span class="is-sr-only">First page</span>
      </a>
      <% } %>
      </li>

    <% if (page.total > max_links) { %>
      <% if (current - adjacent_links > 1) { %>
      <!-- First Page. -->
      <li class="first">
        <a href="<%= pagination_url(1) %>"
           class="pagination-link" 
           aria-label="Go to page 1">
           <span class="is-sr-only">Go to page </span>1</a>
      </li>
      <% } %>

      <% if (current - adjacent_links > 2) { %>
      <!-- Early (More Pages) Indicator. -->
      <li class="pages-indicator first">
        <span class="pagination-ellipsis">&hellip;</span>
      </li>
      <% } %>
    <% } %>

    <% 
  var cursor; 
  for (cursor = 1; cursor <= page.total; cursor++) { 
    var show_cursor_flag = false;


    if (page.total > max_links) {
      // Complex page numbers.

      if (current <= lower_limit) {
        // Lower limit pages.
        // If the user is on a page which is in the lower limit.
        if (cursor <= max_links) {
          // If the current loop page is less than max_links.
          show_cursor_flag = true;
        }
      } else if (current >= upper_limit) {
        // Upper limit pages.
        // If the user is on a page which is in the upper limit.
        if (cursor > (page.total - max_links)) {
          // If the current loop page is less than max_links.
          show_cursor_flag = true;
        }
      } else {
        // Middle pages.
        if ( (cursor >= current - adjacent_links) 
        &&   (cursor <= current + adjacent_links) ) {
          show_cursor_flag = true;
        }
      }
    } else {
      // Simple page numbers.
      show_cursor_flag = true;
    }

    if (show_cursor_flag) { 
      // Calculate Offset Class.
      var page_offset_class = 'pagination--offset-' 
                            + Math.abs(cursor - current);

      // Show Pager.
      %>
      <li class="<%= page_offset_class %>">
        <% if (current != cursor) { %>
        <a href="<%= pagination_url(cursor) %>"
           class="pagination-link" 
           aria-label="Go to page <%= cursor %>">
          <span class="is-sr-only">Go to page </span><%= cursor %>
        </a>
        <% } else { %>
        <a class="pagination-link is-current" 
           aria-label="Page <%= current %>"
           aria-current="true">
          <span class="is-sr-only">Page </span><%= current %>
        </a>
        <% } %>
      </li>
      <% } /* if */ %>
    <% } /* for */ %>

    <% if (page.total > max_links) { %>
      <% if (current + adjacent_links < page.total - 1) { %>
      <!-- Late (More Pages) Indicator. -->
      <li class="pages-indicator last">
        <span class="pagination-ellipsis">&hellip;</span>
      </li>
      <% } %>

      <% if (current + adjacent_links < page.total) { %>
      <!-- Last Page. -->
      <li class="last">
        <a href="<%= pagination_url(page.total) %>"
           class="pagination-link" 
           aria-label="Go to page <%= page.total %>">
           <span class="is-sr-only">Go to page </span><%= page.total %></a>
      </li>
      <% } %>
    <% } %>

      <!-- Next Page. -->
      <li class="blog-next-no-responsive">
      <% if (page.next_link) { %>
        <a class="pagination-next"
           href="<%= url_for(page.next_link) %>" 
           aria-label="Go to next page"
           rel="next">&nbsp;&raquo;
           <span class="is-sr-only">Go to next page</span>
        </a>
      <% } else { %>
        <a class="pagination-next"
           title="This is the last page"
           aria-label="Last page"
           aria-current="true"
           disabled>&nbsp;&raquo;
        <span class="is-sr-only">Last page</span>
      </a>
      <% } %>
      </li>
    </ul>
  <% } %>
</nav>
{{< / highlight >}}

Now the pagination tutorial is done.

I think this is all for now.

* <code>.</code>

-- -- --

### What is Next ?

Feels like tired, after finishing pagination Tutorial ?
This kitten, need some sleep now..
Just like the kitten, you may need some rest too.
Our pagination tutorial is finished.
But you may continue to explore other challenging topic,
tomorrow, or right away.

![adorable kitten][image-kitten]

Consider continue reading [ [Hexo - Pagination - Handling URL][local-whats-next] ].
There are still, one interesting topic about <code>Pagination in Hexo</code>.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2019/05/23/hexo-pagination-handling-url/
[local-pagination-intro]:   {{< baseurl >}}ssg/2019/05/17/hexo-pagination-intro/
[image-kitten]:             {{< baseurl >}}/assets/site/images/cats/rambu-02.jpg

[image-ss-05-source]:   {{< assets-ssg >}}/2019/05/hexo-ejs-bulma-tutor-05.zip

[image-ss-05-responsive-animation]: {{< assets-ssg >}}/2019/05/56-pagination-responsive.gif
[image-ss-05-preview-all-buttons]:  {{< assets-ssg >}}/2019/05/56-screenreader-all-buttons.png
[image-ss-05-inspect-elements]:     {{< assets-ssg >}}/2019/05/56-screenreader-inspect-elements.png

[tutor-hexo-config]:    {{< tutor-hexo-bulma >}}/_config.yml
[tutor-layout-index]:   {{< tutor-hexo-bulma >}}/themes/tutor-05/layout/index.ejs
[tutor-p-06-screenreader]:{{< tutor-hexo-bulma >}}/themes/tutor-05/layout/pagination/06-screenreader.ejs

[tutor-hexo-sass-main]:          {{< tutor-hexo-bulma >}}/themes/tutor-05/sass/css/main.scss
[tutor-hexo-sass-pagination]:    {{< tutor-hexo-bulma >}}/themes/tutor-05/sass/css/_pagination.scss
