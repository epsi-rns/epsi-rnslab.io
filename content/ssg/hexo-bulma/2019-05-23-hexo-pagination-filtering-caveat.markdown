---
type   : post
title  : "Hexo - Pagination - Filtering Caveat"
date   : 2019-05-23T13:17:35+07:00
slug   : hexo-pagination-filtering-caveat
categories: [ssg, frontend]
tags      : [hexo, navigation, bulma]
keywords  : [static site, custom theme, ejs, pagination, caveat]
author : epsi
opengraph:
  image: assets/site/images/topics/hexo-bulma.png

toc    : "toc-2019-05-hexo-bulma-step"

excerpt:
  Building Hexo Step by step, with Bulma as stylesheet frontend.
  Knowing the limit of this pagination feature..

---

### Preface

> Goal: Knowing the limit of this pagination feature.

Remember our previous handling URL article ?
For these sections to works,
we need make a slight adjustment,
to our summary layout.

With this we will find caveat that we need to know.

#### Source Code

You can download the source code of this article here.

* [hexo-ejs-bulma-tutor-05.zip][image-ss-05-source]

Extract and run on CLI using <code>$ npm install</code>.

-- -- --

### 1: Summary Adjustment

Consider this summary layout.

#### Layout: EJS Post List: By Month

The javascript between the tag below <code><% ... _%></code>,
will be discussed separately.

* <code>themes/tutor-03/layout/summary/post-list-by-month.ejs</code>
  : [gitlab.com/.../layout/summary/post-list-by-month.ejs][tutor-layout-by-month]

{{< highlight html >}}
<%
  ...
_%>
  <% 
    const postsSorted = Object.keys(postsByYear).sort(date, -1);
    postsSorted.forEach(function (year){
  %>

  <div class ="anchor-target archive-year" id="<%= year %>">
     <% if (year == yearToday) {%>
       This year's posts (<%= year %>)
     <% } else { %>
       <%= year %>
     <% } %>
  </div>

  <div class="columns is-multiline ">
    <%
      const postsByMonth = groupBy(postsByYear[year], 'month');
      Object.keys(postsByMonth).forEach(function (month){
    %>

    <div class="column is-full-mobile 
                is-half-tablet is-one-third-widescreen">

      <section class="panel is-light">
        <div class="panel-header" id="<%= year %>-<%= month %>">
          <p><%= month %> <%= year %></p>
          <span class="fa fa-archive"></span>
        </div>
        <div class="panel-body has-background-white">

          <div class="archive-list archive-p3">
            <% postsByMonth[month].forEach(function(post){ %>
            <div class="archive-item">
              <div class="is-pulled-left"><a href="<%- url_for(post.path) %>">
                <%= post.title %>
              </a></div>
              <div class="is-pulled-right has-text-right"><time>
                  <%= date(post.date, "DD MMM") %>&nbsp;
                  <span class="fas fa-calendar"></span>
              </time></div>
              <div class="is-clearfix"></div>
            </div>
            <% }) %>
          </div>

        </div>
      </section>

    </div>

    <% }) %>
  </div>
  <% }) %>
{{< / highlight >}}

#### Javascript: EJS Post List: By Month

Here is the javascript:

{{< highlight javascript >}}
  // Data Model

  // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/reduce
  
  function groupBy(objectArray, property) {
    return objectArray.reduce(function (acc, obj) {
      var key = obj[property];
      if (!acc[key]) {
        acc[key] = [];
      }
      acc[key].push(obj);
      return acc;
    }, {});
  }

  // Posts Array
  const postsLayout = page.posts

  // Grouped by date  
  const postsMapped = postsLayout.map(post => ({ 
    ...post,
    year: date(post.date, "YYYY"),
    month: date(post.date, "MMMM")
  }));

  const postsByYear = groupBy(postsMapped, 'year');
  
  // additional
  
  const yearToday = date(new Date(), 'YYYY');
{{< / highlight >}}

This page use no filtering, and will without any known issue.

-- -- --

### 2: Issue on Filtering

#### Without Filtering

Consider have a look at this line:

{{< highlight javascript >}}
  const postsLayout = page.posts
{{< / highlight >}}

We can ask the page to show only page with specific layout.
But there is no way to filter this in pagination.

It means: one given pagination URL,
could show empty list without article at all,
because there is no article in that page pagination,
is using `post` layout.

![Hexo: Browser Without Filter][image-ss-05-without-filter]

#### Content with Custom Layout

Consider make content with custom layout.

{{< highlight markdown >}}
---
layout    : page
title     : Moody Blues - Nights in White Satin
date      : 2014/03/15 07:35:05
tags      : [pop, 60s]
category  : [lyric]
---

Letters I've written, never meaning to send

<!-- more --> 

Nights in white satin, never reaching the end,  
Letters I've written, never meaning to send  
Beauty I've always missed, with these eyes before  
Just what the truth is, I can't say anymore  

'Cause I love you  
Yes, I love you  
Oh, I love you  
{{< / highlight >}}

All we need to know is this custom line.

{{< highlight yaml >}}
---
layout    : page
---
{{< / highlight >}}

The post does not use `post` kind layout.

#### Filtering Issue

Consider changing the respected line:

{{< highlight javascript >}}
  // Filtered by layout
  // Note: pagination will not be filtered.
  const postsLayout = page.posts
{{< / highlight >}}

We can ask the page to show only page with specific layout.
But there is no way to filter this in pagination.

It means: one given pagination URL,
could show empty list without article at all,
because there is no article in that page pagination,
is using `post` layout.

![Hexo: Browser Filter Caveat][image-ss-05-filter-caveat]

#### Conclusions

We need to carefully consider that,
there is no way to filter post in pagination,
but we can still choose to filter it inside the page.

-- -- --

### What is Next ?

Now we are done with pagination.

Consider continue reading [ [Hexo - Content - Blog Post][local-whats-next] ].
There are, some interesting topic for using <code>Bulma in Hexo Content</code>,
such as <code>Header, Footer, and Navigation</code>.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2019/05/24/hexo-content-post/

[image-ss-05-source]:   {{< assets-ssg >}}/2019/05/hexo-ejs-bulma-tutor-05.zip

[image-ss-05-filter-caveat]:     {{< assets-ssg >}}/2019/05/58-filter-caveat.png
[image-ss-05-without-filter]:    {{< assets-ssg >}}/2019/05/58-without-filter.png

[tutor-hexo-config]:    {{< tutor-hexo-bulma >}}/_config.yml
[tutor-layout-index]:   {{< tutor-hexo-bulma >}}/themes/tutor-05/layout/index.ejs
[tutor-layout-archive]: {{< tutor-hexo-bulma >}}/themes/tutor-05/layout/archive.ejs
[tutor-layout-cat]:     {{< tutor-hexo-bulma >}}/themes/tutor-05/layout/category.ejs
[tutor-layout-tag]:     {{< tutor-hexo-bulma >}}/themes/tutor-05/layout/tag.ejs

[tutor-layout-by-month]:    {{< tutor-hexo-bulma >}}/themes/tutor-05/layout/summary/post-list-by-month.ejs
