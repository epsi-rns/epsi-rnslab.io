---
type   : post
title  : "Hexo - Pagination - Handling URL"
date   : 2019-05-23T09:17:35+07:00
slug   : hexo-pagination-handling-url
categories: [ssg, frontend]
tags      : [hexo, navigation, bulma]
keywords  : [static site, custom theme, ejs, pagination, url]
author : epsi
opengraph:
  image: assets/site/images/topics/hexo-bulma.png

toc    : "toc-2019-05-hexo-bulma-step"

excerpt:
  Building Hexo Step by step, with Bulma as stylesheet frontend.
  Handling URL using Embedded Javascript (EJS).

---

### Preface

> Goal: Handling URL using Embedded Javascript (EJS).

I like EJS.
Compared to other templating,
With EJS, we can also achieve clean code,
separating variable initialization from the template.

#### Source Code

You can download the source code of this article here.

* [hexo-ejs-bulma-tutor-05.zip][image-ss-05-source]

Extract and run on CLI using <code>$ npm install</code>.

-- -- --

### 1: Pagination URL in Hexo

Pagination URL in Hexo, depend on the confguration settings.

#### Configuration

Consider remind the configuration.

* <code>_config.yml</code>
  : [gitlab.com/.../_config.yml][tutor-hexo-config]

{{< highlight yaml >}}
# URL
url           : http://example/

# root: /child/
root          : /

# Home page setting
index_generator:
  path: '/blog'
  per_page    : 2
  order_by: -date
  
# Pagination
pagination_dir: page
{{< / highlight >}}

Restart the server everytime you change the config.

{{< highlight bash >}}
$ hexo server -p 4000 --debug
{{< / highlight >}}

#### Root URL

With this setting we have the root pagination as:

* <http://localhost:4000/blog/>

#### Pagination URL

And consecutively

* <http://localhost:4000/blog/page/2/>

* <http://localhost:4000/blog/page/3/>

* <http://localhost:4000/blog/page/4/>

And so on.

#### Issue

My first attempt is to write the pagination,
uising hexo helper <code>url_for</code> as below:

{{< highlight javascript >}}
<a href="<%= url_for(config.pagination_dir + '/' + cursor) %>">
  <%= cursor %>
</a>
{{< / highlight >}}

But it gave me this url

* <http://localhost:4000/blog/page/2/>

instead of

* <http://localhost:4000/blog/>

So here it is, the <code>pagination_url</code> helper.
Now we can rewrite as:

{{< highlight javascript >}}
<a href="<%= pagination_url(cursor) %>">
  <%= cursor %>
</a>
{{< / highlight >}}

Later the function grown become longer and longer.
We can change the helper, without messing with HTML view.

-- -- --

### URL Helper

We need a helper for pagination.
Consider give the function a meaningful name,
such as <code>pagination_url()</code>.

#### Using The Helper

Simply put the number as an argument.

{{< highlight javascript >}}
      <!-- Page numbers. -->
      <% var cursor; 
         for (cursor = 1; cursor <= page.total; cursor++) { %>
      <li>
        ...
        <a href="<%= pagination_url(cursor) %>"
           class="pagination-link"
           aria-label="Goto page <%= cursor %>">
          <%= cursor %>
        </a>
        ...
      </li>
      <% } %>
{{< / highlight >}}

#### Simple Helper

To have the right url, I must exclude root pagination.

{{< highlight javascript >}}
function pagination_url(number) {
  var path = config.index_generator.path;

  if (number>1) {
      path = path + '/' + config.pagination_dir + '/' + number;
  }

  return url_for(path);
}
{{< / highlight >}}

#### Double Slash Issue

Later I fond that Hexo <code>url_for()</code>,
will treat double slash as file protocol.
Consider this configuration:

{{< highlight yaml >}}
# Home page setting
index_generator:
  path: '/'
{{< / highlight >}}

Instead of having this url

* <http://localhost:4000/blog/page/2/>

We have this result:

* //blog/page/2/

Which lead to

* <file:////blog/page/2>

That is why we need to fix this by this code below:

{{< highlight javascript >}}
function pagination_url(number) {
  var path = config.index_generator.path;
  
  // dirty quick fix, avoid double (//)
  if (path=='/') { path = '' }

  if (number>1) {
      path = path + '/' + config.pagination_dir + '/' + number;
  }

  return url_for(path);
}
{{< / highlight >}}

-- -- --

### 3: Multiple Section

Pagination in Hexo can be used in different layout.

#### Supported Sections

There are sections, that we can put pagination inside it:

* Index: <http://localhost:4000/blog/>

* Archives (root): <http://localhost:4000/archives/>

* Archives (year): <http://localhost:4000/archives/2018/>

* Archives (month): <http://localhost:4000/archives/2018/09/>

* Tags: <http://localhost:4000/tags/pop/>

* Categories: <http://localhost:4000/categories/lyric/>

#### Complex Helper

{{< highlight javascript >}}
function pagination_url(number) {
  var path;

  // default for index
  var path = config.index_generator.path;
  
  // dirty quick fix, avoid double (//)
  if (path=='/') { path = '' }
  
  if (is_archive()){
    path = '/' + config.archive_dir;

    if (is_month()){
      // trailing zero
      var month = ( page.month < 10 ? '0' + page.month : page.month );
      path += '/' + page.year + '/' + month;
    } else if (is_year()){
      path += '/' + page.year;
    }
  } else if (is_category()){
    path = '/' + config.category_dir + '/' + page.category;
  } else if (is_tag()){
    path = '/' + config.tag_dir + '/' + page.tag;
  }

  if (number>1) {
      path = path + '/' + config.pagination_dir + '/' + number;
  }

  return url_for(path);
}
{{< / highlight >}}

-- -- --

### 4: Applying Pagination in Sections

Changing embedded javascript helper is not enough.
We still have to put pagination in each sections.

#### Configuration

Normally I use <code>per_page: 10</code> in configuration.

* <code>_config.yml</code>
  : [gitlab.com/.../_config.yml][tutor-hexo-config]

{{< highlight yaml >}}
# Home page setting
index_generator:
  path: '/blog'
  per_page    : 2
  order_by: -date
  
# Pagination
pagination_dir: page
per_page      : 10
{{< / highlight >}}

For testing the pagination,
with only small amount of articles,
I prefer as lowest number as possible.

{{< highlight yaml >}}
# Pagination
pagination_dir: page
per_page      : 1
{{< / highlight >}}

Restart the server everytime you change the config.

{{< highlight bash >}}
$ hexo server -p 4000 --debug
{{< / highlight >}}

Now we can use them for

* Archive section page,

* Category section page, and 

* Tag section page.

Consider have a look at difference for each screen size.

#### Layout: EJS Archive

* <code>themes/tutor-05/layout/archive.ejs</code>
  : [gitlab.com/.../layout/archive.ejs][tutor-layout-archive]

{{< highlight javascript >}}
<% 
  var section_title = __('archive');

  if (is_month()){
    section_title += ': ' + page.year + '/' + page.month;
  } else if (is_year()){
    section_title += ': ' + page.year;
  }
%>
<main role="main" 
      class="column is-full box-deco has-background-white">
  <section class="section">
    <h4 class="title is-4"><%= section_title %></h4>
  </section>

  <section class="section">
    <%- partial('pagination/06-screenreader') %>
  </section>

  <section class="section" id="archive">  
    <%- partial('summary/post-list-by-month') %>
  </section>
</main>
{{< / highlight >}}

Open in your favorite browser.

* <http://localhost:4000/archives/2018/09/page/2/>

![Hexo: Browser Section Archive][image-ss-05-section-archive]

__.__

#### Layout: EJS Category

* <code>themes/tutor-05/layout/category.ejs</code>
  : [gitlab.com/.../layout/category.ejs][tutor-layout-cat]

{{< highlight javascript >}}
<% 
  var section_title = __('category') + ': ' + page.category;
%>
<main role="main" 
      class="column is-full box-deco has-background-white">
  <section class="section">
    <h4 class="title is-4"><%= section_title %></h4>
  </section>

  <section class="section">
    <%- partial('pagination/06-screenreader') %>
  </section>

  <section class="section" id="archive">  
    <%- partial('summary/post-list-by-month') %>
  </section>
</main>
{{< / highlight >}}

Open in your favorite browser.

* <http://localhost:4000/categories/lyric/page/2/>

![Hexo: Browser Section Category][image-ss-05-section-category]

__.__

#### Layout: EJS Tag

* <code>themes/tutor-05/layout/tag.ejs</code>
  : [gitlab.com/.../layout/tag.ejs][tutor-layout-tag]

{{< highlight javascript >}}
<% 
  var section_title = __('tag') + ': ' + page.tag;
%>
<main role="main" 
      class="column is-full box-deco has-background-white">
  <section class="section">
    <h4 class="title is-4"><%= section_title %></h4>
  </section>

  <section class="section">
    <%- partial('pagination/06-screenreader') %>
  </section>

  <section class="section" id="archive">  
    <%- partial('summary/post-list-by-month') %>
  </section>
</main>
{{< / highlight >}}

Open in your favorite browser.

* <http://localhost:4000/tags/pop/page/2/>

![Hexo: Browser Section Tag][image-ss-05-section-tag]

__.__

#### Layout: EJS Post List: By Month

For these sections to works,
we need make a slight adjustment,
to our summary layout.

* <code>themes/tutor-03/layout/summary/post-list-by-month.ejs</code>
  : [gitlab.com/.../layout/summary/post-list-by-month.ejs][tutor-layout-by-month]

It is a long source code.
No need to put it here.
But if you desire, there is another article for this.

-- -- --

### What is Next ?

Consider continue reading [ [Hexo - Pagination - Filtering Caveat][local-whats-next] ].
There are still, one interesting topic about <code>Pagination in Hexo</code>.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2019/05/24/hexo-pagination-filtering-caveat/

[image-ss-05-source]:   {{< assets-ssg >}}/2019/05/hexo-ejs-bulma-tutor-05.zip

[image-ss-05-section-archive]:   {{< assets-ssg >}}/2019/05/57-section-archive.png
[image-ss-05-section-category]:  {{< assets-ssg >}}/2019/05/57-section-category.png
[image-ss-05-section-tag]:       {{< assets-ssg >}}/2019/05/57-section-tag.png

[tutor-hexo-config]:    {{< tutor-hexo-bulma >}}/_config.yml
[tutor-layout-index]:   {{< tutor-hexo-bulma >}}/themes/tutor-05/layout/index.ejs
[tutor-layout-archive]: {{< tutor-hexo-bulma >}}/themes/tutor-05/layout/archive.ejs
[tutor-layout-cat]:     {{< tutor-hexo-bulma >}}/themes/tutor-05/layout/category.ejs
[tutor-layout-tag]:     {{< tutor-hexo-bulma >}}/themes/tutor-05/layout/tag.ejs

[tutor-layout-by-month]:    {{< tutor-hexo-bulma >}}/themes/tutor-05/layout/summary/post-list-by-month.ejs
