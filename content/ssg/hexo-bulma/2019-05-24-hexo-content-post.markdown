---
type   : post
title  : "Hexo - Content - Blog Post"
date   : 2019-05-24T09:17:35+07:00
slug   : hexo-content-post
categories: [ssg, frontend]
tags      : [hexo, bulma, sass]
keywords  : [static site, custom theme, post header, post footer, post navigation, date time, javascript, timeago]
author : epsi
opengraph:
  image: assets/site/images/topics/hexo-bulma-markdown.png

toc    : "toc-2019-05-hexo-bulma-step"

excerpt:
  Building Hexo Step by step, with Bulma as stylesheet frontend.
  More about blog post content, Header, Footer, and Navigation.

---

### Preface

> Goal: More about blog post content, Header, Footer, and Navigation.

We can save a lot of stylesheet formatting,
by using Bulma, or any other CSS framework.

#### Source Code

You can download the source code of this article here.

* [hexo-ejs-bulma-tutor-05-content.zip][image-ss-05-source]

Extract and run on CLI using <code>$ npm install</code>.

-- -- --

### 1: Prepare

This preparation is required.

#### Theme

To avoid messed-up with previous tutorial, use <code>tutor-05</code>.

* <code>_config.yml</code>
  : [gitlab.com/.../_config.yml][tutor-hexo-config]

{{< highlight yaml >}}
# Extensions
theme: tutor-05
{{< / highlight >}}

#### Preview: General

Consider redesign the looks of blog post.
This is what we want to achieve in this tutorial.

![Hexo Content: All Artefacts][image-ss-05-blog-post-all]

#### Layout: EJS Post

Edit our ancient <code>post.ejs</code> artefact,
that is exist from the very beginning of our tutorial.

* <code>themes/tutor-04/layout/post.ejs</code>
  : [gitlab.com/.../layout/post.ejs][tutor-layout-post]

{{< highlight javascript >}}
  <main role="main" 
        class="column is-two-thirds has-background-white
               blog-column box-deco">
    <div class="blog-post">
      <%- partial('post/header') %>

      <article>
        <%- page.content %>
      </article>
      
      <br/>
      <%- partial('post/navigation') %>
    </div><!-- /.blog-post -->
    
    <%- partial('post/footer') %>
  </main>

  <aside class="sidebar column is-one-thirds is-paddingless">
    <%- partial('widget/related-posts') %>
    ...
  </aside>
{{< / highlight >}}

![Hexo Content: ViM Post Layout][image-ss-05-vim-post-layout]

Talking about schema, you can read this nice site:

* [schema.org/](http://schema.org/)

#### Partials: New Artefacts

The code above require three new partials.

Create three empty artefacts,

* <code>themes/tutor-05/layout/post/header.ejs</code>

* <code>themes/tutor-05/layout/post/footer.ejs</code>

* <code>themes/tutor-05/layout/post/navigation.ejs</code>

And one more artefact that is required in <code>post/header.ejs</code>.

* <code>themes/tutor-05/layout/post/time-elapsed.ejs</code>

#### SASS: Main

And add relevant stylesheet in <code>main.scss</code>.

* <code>themes/tutor-05/sass/css/main.sass</code>
  : [gitlab.com/.../sass/main.sass][tutor-hexo-sass-main].

{{< highlight scss >}}
@import "initial-variables"
...

@import "layout"
...

@import "post"
@import "post-header"
@import "post-content"
@import "post-title"
@import "post-navigation"
@import "post-highlight-hexo"
{{< / highlight >}}

Even with Bulma, we still need adjustment.
For example, you need to reset the font size, in Bulma Heading.

-- -- --

### 2: Header

Consider begin with header.

#### Preview

![Hexo Content: Header][image-ss-05-blog-post-header]

#### Partials: Minimum

The minimum header is simply showing title.

* <code>themes/tutor-05/layout/post/header.ejs</code>
  : [gitlab.com/.../layout/post/header.ejs][tutor-l-post-header]

{{< highlight html >}}
    <section class="box">
      <div class="main_title"> 
        <h4 class="title is-4 blog-post-title" itemprop="name headline">
          <a href="<%- page.path %>"><%- page.title %></a></h4>
      </div>
    </section>
{{< / highlight >}}

#### Partials: Meta

Consider add **meta** data in post header.

* author

* time elapsed (using partial)

* tags

* categories

Here below is my actual code.
I use bulma <code>tag</code>,
with the eye candy Awesome Font.

{{< highlight html >}}
    <section class="box">
      <div class="main_title"> 
        <h4 class="title is-4 blog-post-title" itemprop="name headline">
          <a href="<%- page.path %>"><%- page.title %></a></h4>
      </div>

      <div class="is-clearfix"></div>
      
      <div class="field">
        <% if (config.author){ %>
        <div class="is-pulled-left">
        <span class="meta_author tag is-primary is-small">
            <span class="fa fa-user"></span>
            &nbsp;
            <span itemprop="author"
                  itemscope itemtype="http://schema.org/Person">
            <span itemprop="name"><%- config.author %></span></span>            
        </span>
        &nbsp;
        </div>
        <% } %>

        <div class="is-pulled-left">
          <span class="meta-time tag is-light is-small">
          <%- partial('post/time-elapsed') %>
          </span>
          &nbsp;
        </div>

        <div class="is-pulled-right">
          <% page.tags.forEach(function(tag){ %>
            <a href="<%- url_for(tag.path) %>">
              <span class="tag is-light is-small"><span 
                    class="fa fa-tag"></span>&nbsp;
                    <%- tag.name %></span></a>
          <% }) %>
          &nbsp;
        </div>

        <div class="is-pulled-right">
          <% page.categories.forEach(function(cat){ %>
            <a href="<%- url_for(cat.path) %>">
              <span class="tag is-dark is-small"><span 
                    class="fa fa-folder"></span>&nbsp;
                    <%- cat.name %></span></a>
          <% }) %>
          &nbsp;
        </div>
      </div>

      <div class="is-clearfix"></div>
    </section>
{{< / highlight >}}

-- -- --

### 3: Elapsed Time

As it has been mentioned above, we need special partial for this.

#### Issue

As a static generator,
Hexo build the content whenever there are any changes.
If there are no changes, the generated time remain static.
It means we cannot tell relative time in string such as **three days ago**,
because after a week without changes, the time remain **three days ago**,
not changing into **ten days ago**.

The solution is using javascript.
You can download the script from here

* [timeago.org/](https://timeago.org/)

That way the time ago is updated dynamically as time passes,
as opposed to having to rebuild Hexo every day.

Do not forget to put the script in static directory.

* <code>themes/tutor-05/source/js/timeago.min.js</code>

#### Partial

* <code>themes/tutor-05/layout/post/time-elapsed.ejs</code>
  : [gitlab.com/.../layout/post/time-elapsed.ejs][tutor-l-post-time-elapsed]

{{< highlight javascript >}}
    <time datetime="<%= date(page.date, "YYYY-MM-DD[T]HH:mm:ss.SSS") %>"
          itemprop="datePublished">
    <span datetime="<%= date(page.date, "YYYY-MM-DD HH:mm:ss") %>"
          class="timeago">
    </span></time>
    &nbsp; 

    <%- js('/js/timeago.min.js') %>
    <script type="text/javascript">
      var timeagoInstance = timeago();
      var nodes = document.querySelectorAll('.timeago');
      timeagoInstance.render(nodes, 'en_US');
      timeago.cancel();
      timeago.cancel(nodes[0]);
    </script>
{{< / highlight >}}

-- -- --

### 4: Footer

We already have Header.
Why do not we continue with footer?

#### Preview

![Hexo Content: Footer][image-ss-05-blog-post-footer]

#### Partials

* <code>themes/tutor-05/layout/post/footer.ejs</code>
  : [gitlab.com/.../layout/post/footer.ejs][tutor-l-post-footer]

{{< highlight html >}}
    <footer class="section">
      <div class="columns">

        <div class="column is-narrow has-text-centered">
            <img src="<%- url_for("/site/images/license/cc-by-sa.png") %>" 
                 class="bio-photo" 
                 height="31"
                 width="88"
                 alt="CC BY-SA 4.0"></a>
        </div>

        <div class="column has-text-left">
            This article is licensed under:&nbsp;
            <a href="https://creativecommons.org/licenses/by-sa/4.0/deed" 
               class="text-dark">
                <b>Attribution-ShareAlike</b> 4.0 International (CC BY-SA 4.0)</a>
        </div>

      </div>
    </footer>
{{< / highlight >}}

#### Assets: License

I have collect some image related with **license**,
so that you can use license easily.

* <code>themes/tutor-05/source/site/images/license/</code>
  : [gitlab.com/.../source/site/images/license/][tutor-hexo-license]

-- -- --

### 5: Post Navigation

Each post also need simple navigation.

#### Preview

![Hexo Content: Navigation][image-ss-05-blog-post-nav]

#### Partials

* <code>themes/tutor-05/layout/post/navigation.ejs</code>
  : [gitlab.com/.../layout/post/navigation.ejs][tutor-l-post-navigation]

{{< highlight javascript >}}
<nav class="pagination is-centered" 
     role="navigation" aria-label="pagination">

    <!-- Previous Page. -->
    <% if (page.next) { %>
      <a class="pagination-previous post-previous"
         href="<%- url_for(page.next.path) %>"
         title="<%- page.next.name %>">&laquo;&nbsp;</a>
    <% } %>

    <!-- Next Page. -->
    <% if (page.prev) { %>
      <a class="pagination-next post-next"
         href="<%- url_for(page.prev.path) %>"
         title="<%- page.prev.name %>">&nbsp;&raquo;</a>
    <% } %>
{{< / highlight >}}

This embedded javascript is self explanatory.

#### SASS: Post Navigation

Code above require two more classes

* <code>previous</code>

* <code>next</code>

* <code>themes/tutor-05/sass/css/_post-navigation.sass</code>
  : [gitlab.com/.../sass/_post-navigation.sass][tutor-hexo-sass-navigation].

{{< highlight scss >}}
// -- -- -- -- --
// _post-navigation.sass

a.post-previous:after
  content: " previous"

a.post-next:before
  content: "next "
{{< / highlight >}}

Or better, you can add a simple hover effect.

{{< highlight scss >}}
a.post-previous:hover,
a.post-next:hover
  box-shadow: 0 0.5rem 1rem rgba(0,0,0,0.15)
  background-color: $yellow
  color: #000
{{< / highlight >}}

__.__

-- -- --

### 6: Conclusion

It is enough for now.
There are many part that can be enhanced, in about content area.

After all, it is about imagination.

-- -- --

### What is Next ?

Consider continue reading [ [Hexo - Content - Markdown][local-whats-next] ].
There are, some interesting topic for using <code>Markdown in Hexo Content</code>,
such as configuration and using <code>Hexo Admin</code>.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2019/05/25/hexo-content-markdown/

[image-ss-05-source]:   {{< assets-ssg >}}/2019/05/hexo-ejs-bulma-tutor-05-content.zip

[tutor-hexo-config]:    {{< tutor-hexo-bulma >}}/_config.yml

[image-ss-05-blog-post-all]:    {{< assets-ssg >}}/2019/05/61-blog-post-all.png
[image-ss-05-blog-post-header]: {{< assets-ssg >}}/2019/05/61-blog-post-header.png
[image-ss-05-blog-post-footer]: {{< assets-ssg >}}/2019/05/61-blog-post-footer.png
[image-ss-05-blog-post-nav]:    {{< assets-ssg >}}/2019/05/61-blog-post-navigation.png
[image-ss-05-vim-post-layout]:  {{< assets-ssg >}}/2019/05/62-vim-layout-post-ejs.png

[tutor-layout-post]:            {{< tutor-hexo-bulma >}}/themes/tutor-05/layout/post.ejs
[tutor-l-post-header]:          {{< tutor-hexo-bulma >}}/themes/tutor-05/layout/post/header.ejs
[tutor-l-post-footer]:          {{< tutor-hexo-bulma >}}/themes/tutor-05/layout/post/footer.ejs
[tutor-l-post-navigation]:      {{< tutor-hexo-bulma >}}/themes/tutor-05/layout/post/navigation.ejs
[tutor-l-post-time-elapsed]:    {{< tutor-hexo-bulma >}}/themes/tutor-05/layout/post/time-elapsed.ejs

[tutor-hexo-license]:           {{< tutor-hexo-bulma >}}/source/site/images/license/
[tutor-hexo-sass-main]:         {{< tutor-hexo-bulma >}}/themes/tutor-05/sass/css/main.sass
[tutor-hexo-sass-navigation]:   {{< tutor-hexo-bulma >}}/themes/tutor-05/sass/css/_post-navigation.sass
