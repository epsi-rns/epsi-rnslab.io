---
type   : post
title  : "Hexo - Content - Markdown"
date   : 2019-05-25T09:17:35+07:00
slug   : hexo-content-markdown
categories: [ssg, frontend]
tags      : [hexo, sass]
keywords  : [static site, custom theme, bulma, markdown, shortcode, responsive image]
author : epsi
opengraph:
  image: assets/site/images/topics/hexo-bulma-markdown.png

toc    : "toc-2019-05-hexo-bulma-step"

excerpt:
  Building Hexo Step by step, with Bulma as stylesheet frontend.
  The real markdown content.

---

### Preface

> Goal: Common use of markdown in Hexo content

There is not much to say about markdown.
Markdown is easy, however there might be a tricky issue.

#### Reading

* [daringfireball.net markdown syntax][link-daringfireball]

#### Why Markdown?

	More on Writing Content

While markup focus on document formatting,
markdown focus on document structure.

#### Source Code

You can download the source code of this article here.

* [hexo-ejs-bulma-tutor-05-content.zip][image-ss-05-source]

Extract and run on CLI using <code>$ npm install</code>.

-- -- --

### 0: Markdown Content

> Focus on document structure

![Illustration: Markdown Content][illustration-markdown]

-- -- --

### 1: Split Resource

It is a good practice to split resource and content.

#### Common Structure

The common structure for content in Hexo are:

1. Frontmatter: YAML, or JSON

2. Content: Markdown or HTML

Th combination is all up to you, your choice, to manage your content.

#### Real Life Structure

I think that easier just to live with this structure,
split the content, and the resource (link and image).

1. Frontmatter: YAML or JSON

2. Content: Markdown

3. Resource: Markdown

{{< highlight markdown >}}
---
layout    : page
---

# Header 1

My content

[My Link][my-image]

-- -- --

## Header 1.1

My sub content

![My Image][mylink]

-- -- --

## Header 1.2

My other sub content

[//]: <> ( -- -- -- links below -- -- -- )

[my-image]:    http://some.site/an-image.jpg
[my-link]:     http://some.site/an-article.html
{{< / highlight >}}

Notice, that the markdown divided into two part by using 

{{< highlight markdown >}}
[//]: <> ()
{{< / highlight >}}

-- -- --

### 2: Line Break

Hexo markdown treated each line as newline.
Differently with standard markdown
that require explicit backslash to make a new line break.
This could make your life hard.

#### Content: Post

As usual, we require example content.

* <code>source/_posts/white-winter.md</code>
  : [gitlab.com/.../source/_posts/white-winter.md][tutor-post-winter]

{{< highlight markdown >}}
---
layout    : page
title     : Surviving White Winter
date      : 2016/11/13 11:02:00
---

It was a frozen winter in cold war era.
<!-- more --> 
We were two lazy men, a sleepy boy, two long haired women,
a husky with attitude, and two shotguns.
After three weeks, we finally figure out, about **css framework**.

But we lost our beloved husky before we finally made it.
Now, every january, we remember our husky,
that helped all of us to survive.
{{< / highlight >}}

#### Render: With Issue

This will result as:

![Hexo Markdown: Newline Issue][image-ss-05-with-newline]

#### Configuration

You can standarize using this,
to disable this unused newline feature

* <code>_config.yml</code>
  : [gitlab.com/.../_config.yml][tutor-hexo-config]

{{< highlight yaml >}}
# Markdown
marked:
  gfm: true
  breaks: false
{{< / highlight >}}

Remove <code>db.json</code>, and restart server.

#### Sass

Now you still need to different between paragraph and newline:

* <code>themes/tutor-05/sass/css/_post-content.scss</code>
  : [gitlab.com/.../sass/_post-content.scss][tutor-hexo-sass-content].

{{< highlight scss >}}
.blog-post p
  margin-top: 0.5rem
  margin-bottom: 0.5rem
{{< / highlight >}}

#### Render: Without Issue

This will result as:

![Hexo Markdown: Newline with Issue Fixed][image-ss-05-no-newline]

We have nomore line break issue.

-- -- --

### 3: Heading Title

#### Markdown Render

Normally markdown render this:

{{< highlight markdown >}}
# My Heading Title
{{< / highlight >}}

Into this:

{{< highlight html >}}
<h1>My Heading Title</h1>
{{< / highlight >}}

#### Bulma Class Issue

Bulma require heading title to be written as this below:

{{< highlight html >}}
<h1 class="title is-1">My Heading Title</h1>
{{< / highlight >}}

Otherwise, heading title rendered without proper size.
This means your heading would not be rendered correctly.

#### Content: Post

Again, content for example purpose:

* <code>/source/_posts/lyrics/david-gates-lady-valentine.md</code>
  : [gitlab.com/.../source/...-lady-valentine.md][tutor-post-valentine]

{{< highlight markdown >}}
---
title     : David Gates - Lady Valentine
date      : 2014/03/25 07:35:05
tags      : [jazz, 80s]
category  : [lyric]
---

She's a real look  
cause she's my heart my inspiration  
Lucky is the man who find the child of love

<!-- more --> 

# Heading 1

## Heading 2

### Example Content

Your the music your my song  
Here with me is where you belong

Your the rythm, your my ryhme  
Keep me right into the time  
And I'm so happy when you're here
{{< / highlight >}}

![Hexo Markdown: Bulma Heading Class Issue][image-ss-05-heading-plain]

The figure above show that all heading rendered with the same size.

#### Custom SASS: Heading

We can fix the title heading by this SASS.

* <code>themes/tutor-05/sass/css/_post-title.scss</code>
  : [gitlab.com/.../sass/_post-title.scss][tutor-hexo-sass-title].

{{< highlight scss >}}
h1
  font-size: $size-2

h2
  font-size: $size-3

h3
  font-size: $size-4

h4
  font-size: $size-5

h5
  font-size: $size-6

h6
  font-size: $size-7

h1, h2, h3, h4, h5, h6
  font-weight: $weight-semibold
{{< / highlight >}}

And the result is prettier now.

![Hexo Markdown: Fix Bulma Heading Class Size][image-ss-05-heading-styled]

-- -- --

### 4: Link and Image

There are two methods to provide link and image in Hexo.
But still, I cannot find an easy way to make things simpler in Hexo.

#### Content: Pure Markdown

Again, content for example purpose

* <code>/source/_posts/lyrics/david-gates-lady-valentine.md</code>
  : [gitlab.com/.../source/...-lady-valentine.md][tutor-post-valentine]

{{< highlight markdown >}}
---
title     : David Gates - Lady Valentine
date      : 2014/03/25 07:35:05
tags      : [jazz, 80s]
category  : [lyric]
---

She's a real look  
cause she's my heart my inspiration  
Lucky is the man who find the child of love

<!-- more -->

[Terminal Dofiles][one-link].

Your the music your my song  
Here with me is where you belong

![Business Card][one-image]

Your the rythm, your my ryhme  
Keep me right into the time  
And I'm so happy when you're here

[//]: <> ( -- -- -- links below -- -- -- )

[one-image]:    /site/images/adverts/one-page.png
[one-link]:     https://gitlab.com/epsi-rns/dotfiles/tree/master/terminal/
{{< / highlight >}}

![Hexo Markdown: Image and Link using Pure Markdown][image-ss-05-image-pure]

#### Content: Bulma Helper

However, there is another way, using hexo's helper.

* [gitlab.com/.../source/...-lady-valentine.md][tutor-post-valentine]

{{< highlight markdown >}}
---
title     : David Gates - Lady Valentine
date      : 2014/03/25 07:35:05
tags      : [jazz, 80s]
category  : [lyric]
---

She's a real look  
cause she's my heart my inspiration  
Lucky is the man who find the child of love

<!-- more -->

{% link Terminal Dofiles "https://gitlab.com/epsi-rns/dotfiles/tree/master/terminal/" %}.

Your the music your my song  
Here with me is where you belong

Your the rythm, your my ryhme  
Keep me right into the time  
And I'm so happy when you're here

-- -- --

### What's next ?

Our next song would be {% post_link lyrics/heatwave-all-i-am %}.
{{< / highlight >}}

![Hexo Markdown: Image and Link using Hexo's Helper][image-ss-05-image-helper]

#### Prepare Image Directory

It is, a good practice to have a good structured directory.
Just put all your resources in <code>assets</code> directory.
And how you structure you image, javascript, vector and such,
is actually up to you.

{{< highlight bash >}}
$ tree source/site source/posts
source/site
└── images
    ├── adverts
    │   └── one-page.png
    ├── authors
    │   └── epsi-vexel.png
    └── license
        ├── cc-0.png
        ├── cc-by-nc-nd.png
        ├── cc-by-nc.png
        ├── cc-by-nc-sa.png
        ├── cc-by-nd.png
        ├── cc-by-sa.png
        └── cc-pdm.png
source/posts
└── 2018
    ├── kiddo-007.jpg
    └── kiddo-008-s.jpg
{{< / highlight >}}

![Hexo Source Image: Tree][image-ss-06-tree]

-- -- --

### 5: Hexo Admin

There are though, a markdown editor.
Not as fancy as <code>ViM</code>.
But this is useful to test markdown.

* [github.com/jaredly/hexo-admin](https://github.com/jaredly/hexo-admin)

It is also very easy to setup.
All you need to know is already,
in the <code>README.md</code> of that repository.
And access it in:

* <http://localhost:4000/admin/>

#### View

Here is the list of the posts.

![Hexo Admin: List View][image-ss-05-admin-list-view]

#### Edit

You can edit one by one.

![Hexo Admin: editor][image-ss-05-admin-editor]

#### Setting

And you can also edit the frontmatter.

![Hexo Admin: frontmatter setting][image-ss-05-admin-setting]

Now we are done with markdown content.
Your content has been served.

#### Note

This hexo admin is only for local,
and does not have any capability to write to git repository.

But still, this is inspiring.
And maybe also useful for client,
who does not familiar with static site.

-- -- --

### What is Next ?

Consider continue reading [ [Hexo - Content - Raw HTML and Services][local-whats-next] ].
There are, some interesting topic for using <code>HTML in Hexo Content</code>,
such as inserting <code>Table of Content</code>.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2019/05/26/hexo-content-html/
[link-daringfireball]:      https://daringfireball.net/projects/markdown/syntax
[illustration-markdown]:{{< assets-ssg >}}/2020/markdown-focus.png

[image-ss-05-source]:   {{< assets-ssg >}}/2019/05/hexo-ejs-bulma-tutor-05-content.zip

[tutor-hexo-config]:    {{< tutor-hexo-bulma >}}/_config.yml
[image-ss-06-tree]:     {{< assets-ssg >}}/2019/05/62-tree-source.png

[image-ss-05-thunar-resource]:   {{< assets-ssg >}}/2018/11/62-thunar-resource.png
[image-ss-05-kiddo-example]:     {{< assets-ssg >}}/2018/11/62-kiddo-example.png


[image-ss-05-with-newline]:     {{< assets-ssg >}}/2019/05/62-markdown-with-newline.png
[image-ss-05-no-newline]:       {{< assets-ssg >}}/2019/05/62-markdown-no-newline.png
[image-ss-05-heading-plain]:    {{< assets-ssg >}}/2019/05/62-heading-plain.png
[image-ss-05-heading-styled]:   {{< assets-ssg >}}/2019/05/62-heading-styled.png
[image-ss-05-image-pure]:       {{< assets-ssg >}}/2019/05/62-image-pure-markdown.png
[image-ss-05-image-helper]:     {{< assets-ssg >}}/2019/05/62-image-helper-hexo.png

[image-ss-05-admin-editor]:     {{< assets-ssg >}}/2019/05/65-hexo-admin-editor.png
[image-ss-05-admin-setting]:    {{< assets-ssg >}}/2019/05/65-hexo-admin-setting.png
[image-ss-05-admin-list-view]:  {{< assets-ssg >}}/2019/05/65-hexo-admin-list-view.png

[tutor-hexo-sass-content]:  {{< tutor-hexo-bulma >}}/themes/tutor-05/sass/css/_post-content.scss
[tutor-hexo-sass-title]:    {{< tutor-hexo-bulma >}}/themes/tutor-05/sass/css/_post-title.sass

[tutor-post-winter]:        {{< tutor-hexo-bulma >}}/source/_posts/white-winter.md
[tutor-post-valentine]:     {{< tutor-hexo-bulma >}}/source/_posts/lyrics/david-gates-lady-valentine.md
