---
type   : post
title  : "Hexo - Content - Raw HTML"
date   : 2019-05-26T09:17:35+07:00
slug   : hexo-content-html
categories: [ssg, frontend]
tags      : [hexo, bulma, sass]
keywords  : [static site, custom theme, raw html, table of content]
author : epsi
opengraph:
  image: assets/site/images/topics/hexo-bulma-markdown.png

toc    : "toc-2019-05-hexo-bulma-step"

excerpt:
  Building Hexo Step by step, with Bulma as stylesheet frontend.
  Inserting Raw HTML content. Before, after, and in the middle.

---

### Preface

> Goal: Inserting Raw HTML before content.

Sometimes we need to add raw HTML content.
We can do it before or after, the content.
And in the middle of content using shortcodes.

#### Source Code

You can download the source code of this article here.

* [hexo-ejs-bulma-tutor-05-content.zip][image-ss-05-source]

Extract and run on CLI using <code>$ npm install</code>.

-- -- --

### 1: Services

Most static site generator has their own function helper
to provide internet services such as google analytics,
google adsense or discuss comments.
This helper, is very useful for beginner.
For other case, it is more flexible to render your own service layout.

#### Layout: EJS Post

Your can put your service anywhere you want.
For example this <code>post.ejs</code> artefact.

* <code>themes/tutor-05/layout/post.ejs</code>
  : [gitlab.com/.../layout/post.ejs][tutor-layout-post]

{{< highlight javascript >}}
  <main role="main" 
        class="column is-two-thirds has-background-white
               blog-column box-deco">
    <div class="blog-post">
      <%- partial('post/header') %>

      <article>
        <%- partial('service/google-adsense') %>
        <%- page.content %>
      </article>
      
      <br/>
      <%- partial('post/navigation') %>
    </div><!-- /.blog-post -->
    
    <%- partial('post/footer') %>
    <%- partial('service/disqus-comments') %>
  </main>

  <aside class="sidebar column is-one-thirds is-paddingless">
    <%- partial('widget/related-posts') %>
    ...
  </aside>
{{< / highlight >}}

#### Layout: EJS Default

Or <code>layout.ejs</code> artefact, for side wide analytic

* <code>themes/tutor-05/layout/layout.ejs</code>
  : [gitlab.com/.../layout/kind/default.ejs][tutor-layout-default]

{{< highlight html >}}
<!DOCTYPE html>
<html lang="en">
<%- partial('site/head') %>
<body>
<%- partial('service/google-analytics') %>
<%- partial('site/header') %>

  <div class="columns is-8 layout-base maxwidth">
    <%- body %>
  </div>

<%- partial('site/footer') %>
<%- partial('site/scripts') %>
</body>
</html>
{{< / highlight >}}

#### Layout: EJS Services

Now you can have your services here

* <code>themes/tutor-05/layout/service/disqus-comments.html</code>

{{< highlight html >}}
<!-- put your disqus code here -->
{{< / highlight >}}

I do not give example of this service code.
There is already examples on the internet.

#### Service Article

For detail about setting up services,
you might want to read my Hugo article service here:

* [Hugo Service][local-hugo-service]

-- -- --

### 2: Before Content: Table of Content

Sometimes we need to show recurring content, 
such as table of content in article series.
For some article series, we only need one TOC.
And we do not want to repeat ourself,
writing it over and over again.

To solve this case, we need help form frontmatter.

#### Layout: EJS Post

We are going to insert TOC, before the content.

* <code>themes/tutor-05/layout/post.ejs</code>
  : [gitlab.com/.../layout/post.ejs][tutor-layout-post]

{{< highlight javascript >}}
        <article class="main-content" itemprop="articleBody">
          <% if (page.toc) {%>
            <%- partial(page.toc) %>
          <% } %>

          <%- partial('service/google-adsense') %>

          <%- page.content %>
        </article>
{{< / highlight >}}

### Content: Example

Now here it is, the <code>toc</code> in frontmatter,
as shown in this example below.

* <code>source/_posts/lyrics/natalie-cole-i-wish-you-love.md</code>
  : [gitlab.com/.../...i-wish-you-love.md][tutor-content-wish-you]

{{< highlight yaml >}}
---
title     : Susan Wong - I Wish You Love
date      : 2015/05/15 07:35:05
tags      : [pop, 70s]
category  : [lyric]
toc       : "toc/2015-05-susan-wong"

related_link_ids: 
  - 15051515  # The Way We Were
---

I wish you bluebirds in the spring  
To give your heart a song to sing

<!-- more --> 

I wish you health  
And more than wealth  
I wish you love
{{< / highlight >}}

#### Layout: EJS TOC

Now you can have your services here.

* <code>themes/tutor-05/layout/toc/2015-05-susan-wong.html</code>
  : [gitlab.com/.../layout/toc/2015-05-susan-wong.html][tutor-layout-susan-wong]

{{< highlight html >}}
<section class="panel is-light">
  <div class="panel-header">
    <p>Table of Content</p>
    <span class="fa fa-archive"></span>
  </div>
  <div class="panel-body has-background-white">
    <ul class="panel-list">
      <li><a href="<%= url_for("/2015/05/15/lyrics/natalie-cole-i-wish-you-love/") %>"
            >Susan Wong - I Wish You Love</a></li>
      <li><a href="<%= url_for("/2015/05/15/lyrics/barbara-streisand-the-way-we-were/") %>"
            >Susan Wong - The Way We Were</a></li>
    </ul>
  </div>
</section>
{{< / highlight >}}

Notice the <code>.html</code> file extension.

#### Render: Browser

Now you can see the result in the browser.

![Hexo Raw HTML: Table of Content][image-ss-05-toc]

-- -- --

### What is Next ?

Consider continue reading [ [Hexo - Syntax Highlighting][local-whats-next] ].
There are, some interesting topic
about <code>Syntax Highlighting in Hexo Content</code>.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2019/05/27/hexo-syntax-highlighting/
[local-hugo-service]:       {{< baseurl >}}ssg/2018/12/10/hugo-service/

[image-ss-05-source]:   {{< assets-ssg >}}/2019/05/hexo-ejs-bulma-tutor-05-content.zip

[image-ss-05-toc]:          {{< assets-ssg >}}/2019/05/63-toc-susan-wong.png

[tutor-layout-default]:     {{< tutor-hexo-bulma >}}/themes/tutor-05/layout/kind/default.ejs
[tutor-layout-post]:        {{< tutor-hexo-bulma >}}/themes/tutor-05/layout/post.ejs

[tutor-content-wish-you]:   {{< tutor-hexo-bulma >}}/source/_posts/lyrics/natalie-cole-i-wish-you-love.md
[tutor-layout-susan-wong]:  {{< tutor-hexo-bulma >}}/themes//tutor-05/layout/toc/2015-05-susan-wong.html


