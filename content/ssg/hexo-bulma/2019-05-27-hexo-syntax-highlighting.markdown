---
type   : post
title  : "Hexo - Content - Syntax Highlighting"
date   : 2019-05-27T09:17:35+07:00
slug   : hexo-syntax-highlighting
categories: [ssg, frontend]
tags   : [hexo, css, js]
keywords  : [static site, custom theme, javascript, syntax highlighting, highlight sass]
author : epsi
opengraph:
  image: assets/site/images/topics/hexo-bulma-markdown.png

toc    : "toc-2019-05-hexo-bulma-step"

excerpt:
  Building Hexo Step by step, with Bulma as stylesheet frontend.
  A few method to highlight code.

---

### Preface

> Goal: A few method to highlight code.

From default, triple backtick, to prismjs.

#### Source Code

You can download the source code of this article here.

* [hexo-ejs-bulma-tutor-05-content.zip][image-ss-05-source]

Extract and run on CLI using <code>$ npm install</code>.

-- -- --

### 1: Highlighting

There are at least two way to highlight in Hexo.
Both should show similar result.

#### Triple Backtick

{{< highlight markdown >}}
```haskell
-- Layout Hook
commonLayout = renamed [Replace "common"]
    $ avoidStruts 
    $ gaps [(U,5), (D,5)] 
    $ spacing 10
    $ Tall 1 (5/100) (1/3)
```
{{< / highlight >}}

#### Code Block

{{< highlight markdown >}}
{% codeblock lang:haskell %}
-- Layout Hook
commonLayout = renamed [Replace "common"]
    $ avoidStruts 
    $ gaps [(U,5), (D,5)] 
    $ spacing 10
    $ Tall 1 (5/100) (1/3)
{% endcodeblock %}
{{< / highlight >}}

-- -- --

### 2: Stylesheet Issue

Code block is not <code>pygment</code> compatible

### Content: Example

Now edit one of your content, and put some code block.

* <code>source/_posts/lyrics/dido-white-flag.md</code>
  : [gitlab.com/.../dido-white-flag.md][tutor-content-white-flag]

#### Plain

Plain without language type should not have any stylesheet issue.

{{< highlight markdown >}}
{% codeblock %}
-- Layout Hook
commonLayout = renamed [Replace "common"]
    $ avoidStruts 
    $ gaps [(U,5), (D,5)] 
    $ spacing 10
    $ Tall 1 (5/100) (1/3)
{% endcodeblock %}
{{< / highlight >}}

The result in the browser is just fine

![Hexo Syntax Highlighting: Plain][image-ss-05-plain]

#### Formatting

Adding language type would result in html formatting.

{{< highlight markdown >}}
{% codeblock lang:haskell %}
-- Layout Hook
commonLayout = renamed [Replace "common"]
    $ avoidStruts 
    $ gaps [(U,5), (D,5)] 
    $ spacing 10
    $ Tall 1 (5/100) (1/3)
{% endcodeblock %}
{{< / highlight >}}

The result in the browser is not what we are expected.

![Hexo Syntax Highlighting: Formatting][image-ss-05-format]

The stylesheet is mixed-up with Bulma stylesheet.

-- -- --

### 3: Reset Class in Figure Element

Hexo code block is using <code>figure</code> element,
while formatting the syntax highlight.
What we need to do is unstyle,
the class inside <code>figure</code> element.

#### SASS: Syntax Highlight

* <code>themes/tutor-05/sass/css/_post-highlight-hexo.sass</code>
  : [gitlab.com/.../sass/_post-highlight-hexo.sass][tutor-hexo-sass-highlight].

{{< highlight scss >}}
figure.highlight
  table
    span.line
      .title, .number, .type, .string
        font-size: unset
        padding:   unset
        margin:    unset
        height:    unset
        min-width: unset
{{< / highlight >}}

I also style the layout.
So long code won't occupy the whole page.

{{< highlight scss >}}
figure.highlight
  table
    max-height: 350px
{{< / highlight >}}

You may also add necessary nice color.

{{< highlight scss >}}
figure.highlight
  table
    span.line
      .title
        color: #1976d2 // blue 700        
      .comment
        color: #9e9e9e // grey 500
        font-style: oblique
      .number
        color: #0097a7 // cyan 700
        font-weight: 500
      .type
        color: #3f51b5 // indigo 500
        text-decoration: underline dotted
      .string
        color: #004d40 // teal 900
        font-style: italic
        text-decoration: underline wavy
      :hover
        background-color: $yellow
        box-shadow: 0 0.5rem 1rem rgba(0,0,0,0.15)
    tbody
      box-shadow: 0 0.5rem 1rem rgba(0,0,0,0.15)
    tbody:hover
      box-shadow: 0 1rem 3rem rgba($black, .175)
{{< / highlight >}}

This is just an example.
Of course you can add your own style.
Such as adding horizontal scrollbar or else.

#### Render: Browser

Now you can see the result in the browser.

![Hexo Syntax Highlighting: Reset Class in Figure Element][image-ss-05-unstyled]

-- -- --

### What is Next ?

Consider continue reading [ [Hexo - Meta - SEO][local-whats-next] ].
There are, some interesting topic about using <code>Meta in Hexo</code>,
such as <code>Opengraph</code> and <code>Twitter</code>.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2019/05/28/hexo-meta-seo/

[image-ss-05-source]:   {{< assets-ssg >}}/2019/05/hexo-ejs-bulma-tutor-05-content.zip

[image-ss-05-plain]:        {{< assets-ssg >}}/2019/05/64-highlight-plain.png
[image-ss-05-format]:       {{< assets-ssg >}}/2019/05/64-highlight-format.png
[image-ss-05-unstyled]:     {{< assets-ssg >}}/2019/05/64-highlight-unstyled.png

[tutor-content-white-flag]: {{< tutor-hexo-bulma >}}/source/_posts/lyrics/dido-white-flag.md

[tutor-hexo-sass-main]:          {{< tutor-hexo-bulma >}}/themes/tutor-05/sass/css/main.sass
[tutor-hexo-sass-highlight]:     {{< tutor-hexo-bulma >}}/themes/tutor-05/sass/css/_post-highlight-hexo.sass
