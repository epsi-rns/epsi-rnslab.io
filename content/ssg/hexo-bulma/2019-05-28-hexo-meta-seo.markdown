---
type   : post
title  : "Hexo - Meta - SEO"
date   : 2019-05-28T09:17:35+07:00
slug   : hexo-meta-seo
categories: [ssg, frontend]
tags      : [hexo, meta]
keywords  : [static site, custom theme, seo, meta name, meta property, keywords, og, opengraph, twitter]
author : epsi
opengraph:
  image: assets/site/images/topics/hexo-bulma-markdown.png

toc    : "toc-2019-05-hexo-bulma-step"

excerpt:
  Building Hexo Step by step, with Bulma as stylesheet frontend.
  Flexible meta SEO with Hexo, w3c, opengraph and twitter,
  that can be set with params.

---

### Preface

Content is not just for human that using web browser,
but also for machine that read this page, such as search engine,
or social media that read your opengraph.

> Goal: Flexible SEO with Hexo, w3c, opengraph and twitter.

Don't you love a preview on your site?
I mean, that you can customize differently, based on the content.

![Hexo Meta: Telegram Desktop Opengraph][image-ss-05-tele-opengraph]

You can see at above figure, each post has different image.

#### Source Code

You can download the source code of this article here.

* [hexo-ejs-bulma-tutor-05-content.zip][image-ss-05-source]

Extract and run on CLI using <code>$ npm install</code>.

-- -- --

### 1: Prepare

There are few artefacts changes.

#### Artefacts

Edit

* <code>_config.yml</code>.

* <code>themes/tutor-05/layout/site/head.ejs</code>.

New

* <code>themes/tutor-05/layout/meta/html.ejs</code>.

* <code>themes/tutor-05/layout/meta/seo.ejs</code>.

* <code>themes/tutor-05/layout/meta/opengraph.ejs</code>.

* <code>themes/tutor-05/layout/meta/twitter.ejs</code>.

Example Frontmatter

* <code>source/_posts/lyrics/moody-blues-white-satin.md</code>.

-- -- --

### 2: Refactoring: Head Tag

Our first attempt is simple,
consider move the <code>meta tag</code> to different template.

#### Skeleton

To avoid complicated code, we are going to us partial for meta tags.
This including opengraph and twitter.

{{< highlight javascript >}}
<%
  // embedded javascript code here
  ...
%>
<head>
  <%- partial('meta/html') %>
  ...

  <%- partial('meta/seo', {description: description}) %>  
  <%- partial('meta/opengraph', {title: title, description: description}) %>
  <%- partial('meta/twitter', {title: title, description: description}) %>
</head>

{{< / highlight >}}

#### SEO: HTML Preview

Don't you love that your site is ready for Search Engine Optimization ?
The HTML that we want to achieve is similar as example page below:

{{< highlight html >}}
<head>
...

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <meta name="HandheldFriendly" content="True">
  <meta name="MobileOptimized" content="320">
  
  <meta name="generator" content="Karimata 1.2.3 Media Engine" />  
  <meta name="theme-color" content="#2980b9">  

  <!--w3c-->
  <meta name="description" content="Letters I’ve written, never meaning to send">
  <meta name="keywords" content="lyric, pop, 60s, poetic, tragedy, truth, love">
  <meta name="author" content="Mataharani">  

  <!--opengraph-->
  <meta property="og:locale" content="en">
  <meta property="og:title" content="Moody Blues - Nights in White Satin">
  <meta property="og:type" content="article">
  <meta property="og:description" content="Letters I’ve written, never meaning to send">
  <meta property="og:url" content="http://example/2014/03/15/lyrics/moody-blues-white-satin/">
  <meta property="og:site_name" content="Heartbeats for Bulma">
  <meta property="og:image" content="http://example/site/images/adverts/one-page.png">
  <meta property="og:latitude" content="-6.193665">
  <meta property="og:longitude" content="106.848558">
  <meta property="og:locality" content="Jakarta">
  <meta property="og:country-name" content="Indonesia">

  <!--twitter-->
  <meta name="twitter:title" content="Moody Blues - Nights in White Satin">
  <meta name="twitter:description" content="Letters I’ve written, never meaning to send">
...
</head>
{{< / highlight >}}

![Hexo Meta: SEO][image-ss-05-seo]

You can also optimize twitter card if you want.

#### Javascript: EJS Head

Remember the javascript part, from last tutorial.
  
{{< highlight javascript >}}
  var title = page.title;

  if (is_archive()){
    title = __('archive');

    if (is_month()){
      title += ': ' + page.year + '/' + page.month;
    } else if (is_year()){
      title += ': ' + page.year;
    }
  } else if (is_category()){
    title = __('category') + ': ' + page.category;
  } else if (is_tag()){
    title = __('tag') + ': ' + page.tag;
  }
{{< / highlight >}}

This is just a reminder of the last javascript that we use__.__
We still need this javascript.

#### Javascript: Description

Have a look at partial calls,
all three partial takes <code>description</code> as paramater.

{{< highlight javascript >}}
  <%- partial('meta/seo', {description: description}) %>  
  <%- partial('meta/opengraph', {title: title, description: description}) %>
  <%- partial('meta/twitter', {title: title, description: description}) %> 
{{< / highlight >}}

This <code>description</code> will be set by embedded javascript.
Now add this embedded javascript.
  
{{< highlight javascript >}}
  var description = '';

  if (page.description) {
    description = page.description;
  } else if (page.excerpt) {
    description = page.excerpt
                      .replace(/<[^>]*>?/gm, ' ')
                      .substring(0, 155)
                      .trim();
  }
{{< / highlight >}}

We need to limit the length of the description,
and alow remove trailing white space.

-- -- --

### 3: Meta HTML

Consider move important meta to special <code>meta-html.html</code>.
This should be an easy task, since it is only HTML.

#### Layout: EJS Head

* <code>themes/tutor-05/layout/site/head.ejs</code>
  : [gitlab.com/.../layout/site/head.ejs][tutor-l-site-head]
  
{{< highlight javascript >}}
<%
  ...
%>
<head>
  <%- partial('meta/html') %>  
  <title><%= title || config.title %></title>

  <% if (theme.favicon){ %>
     <link href="<%- theme.favicon %>" rel="shortcut icon" type="image/x-icon" />
  <% } %>

  <%- css(['/css/bulma.css', '/css/fontawesome.css', '/css/main.css']) %>
</head>
{{< / highlight >}}

Notice this <code>partial('meta/html')</code> code.

#### Layout: EJS Meta HTML

Remember that, some of these metas must come first in the head tag.

* <code>themes/tutor-05/layout/meta/html.ejs</code>
  : [gitlab.com/.../layout/meta/html.ejs][tutor-l-meta-html]

{{< highlight html >}}
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <meta name="HandheldFriendly" content="True">
  <meta name="MobileOptimized" content="320">
{{< / highlight >}}

Or better, we can add more meta html tag.

{{< highlight html >}}
  <%/* Let's Fake the Generator */%>
  <meta name="generator" content="Karimata 1.2.3 Media Engine" />

  <%/* Chrome, Firefox OS and Opera */%>
  <meta name="theme-color" content="#2980b9">
{{< / highlight >}}

* .

-- -- --

### 4: SEO: W3C

The next step is, put valid **w3c** SEO related meta.

We have at least these three meta tags:

* Description

* Keywords

* Author

#### Keywords

The easiest thing to do make keywords is to utilize
built in <code>tags</code> and <code>categories</code> in frontmatter.
For example in a page we can set the frontmatter as below:

{{< highlight yaml >}}
tags      : [pop, 60s]
category  : [lyric]
{{< / highlight >}}

This will result as below:

{{< highlight html >}}
  <meta name="keywords" content="lyric, pop, 60s">
{{< / highlight >}}

If these two are not enough we can add <code>keywords</code> field
in frontmatter. So we can have more keywords without ruining,
<code>tags</code> and <code>categories</code> in page.
For example:

{{< highlight yaml >}}
tags      : [pop, 60s]
category  : [lyric]
keywords  : [poetic, tragedy, truth, love]
{{< / highlight >}}

These three frontmatter setting will result as below:

{{< highlight html >}}
  <meta name="keywords" content="lyric, pop, 60s, poetic, tragedy, truth, love">
{{< / highlight >}}

#### Layout: EJS Meta SEO

And here the powerful embedded javascript.
It is very easy to do array manipulation using embedded javascript.

* <code>themes/tutor-05/layout/meta/seo.ejs</code>
  : [gitlab.com/.../layout/meta/seo.ejs][tutor-l-meta-seo-w3c]

{{< highlight javascript >}}
<%
  var keywords = '';

  var terms = [];

  if (page.categories) {
    page.categories.each(function(item){
      terms.push(item.name);
    })
  }

  if (page.tags) {
    page.tags.each(function(item){
      terms.push(item.name);
    })
  }

  if (page.keywords) {
    page.keywords.forEach(function(keyword){
      terms.push(keyword);
    })
  }

  if (terms) {
    keywords = terms.join(', ');
  }
%>

  <meta name="description"
        content="<%= description || config.description %>">
  <meta name="keywords"
        content="<%= keywords || config.keywords %>">
  <meta name="author"
        content="<%= page.author || config.author %>">
{{< / highlight >}}

Just be aware, that the <code>keywords</code> is using <code>forEach</code>,
while <code>tags</code> and <code>categories</code> using <code>each</code>.
The differences is because <code>keywords</code> is simply array,
while the other two processed internally by **Hexo**.

#### How Does Keywords Works?

Merge array into one string require <code>join</code> function.

{{< highlight javascript >}}
  if (terms) {
    keywords = terms.join(', ');
  }
{{< / highlight >}}

#### Content: Example

{{< highlight markdown >}}
---
layout    : page
title     : Moody Blues - Nights in White Satin
date      : 2014/03/15 07:35:05
tags      : [pop, 60s]
category  : [lyric]
keywords  : [poetic, tragedy, truth, love]
---

Letters I've written, never meaning to send

<!-- more --> 

Nights in white satin, never reaching the end,  
Letters I've written, never meaning to send  
Beauty I've always missed, with these eyes before  
Just what the truth is, I can't say anymore  

'Cause I love you  
Yes, I love you  
Oh, I love you  
{{< / highlight >}}

This will result as below:

{{< highlight html >}}
  <meta name="description" content="Letters I’ve written, never meaning to send">
  <meta name="keywords" content="lyric, pop, 60s, poetic, tragedy, truth, love">
  <meta name="author" content="Mataharani">  
{{< / highlight >}}

-- -- --

### 5: SEO: Opengraph

The Structure of opengraph can be read from official page:

* [The Open Graph protocol](http://ogp.me/)

#### HTML Preview

The HTML that we want to achieve is similar as below.

{{< highlight html >}}
  ...
  <meta property="og:title" content="Moody Blues - Nights in White Satin">
  <meta property="og:description" content="Letters I’ve written, never meaning to send">
  <meta property="og:url" content="http://example/2014/03/15/lyrics/moody-blues-white-satin/">
  <meta property="og:site_name" content="Heartbeats for Bulma">
  ...
  <meta property="og:image" content="http://example/site/images/adverts/one-page.png">
  ...
  <meta property="og:locality" content="Jakarta">
  ...
{{< / highlight >}}

#### Structure

	How to achieve ?

There are three parts:

* Common properties: using conditional.

* Image: using default image, depend on page type.

* Location: set in theme configuration

#### Javascript: Opengraph Image

To make life easier, consider separate code and view.
Javascript in code, and html in view.

This would takes some conditional, but it self explanatory.
If there is no frontmatter setting,
then use sitewide configuration setting.
If neither set, then fallback to default image.

{{< highlight javascript >}}
  var ogi; 
  if (page.opengraphimage) {
    ogi = page.opengraphimage;
  } else if(config.opengraphimage) {
    ogi = config.opengraphimage;
  } else {
    ogi = 'images/broken-person.png';
  }
  ogi = config.url + url_for(ogi);
{{< / highlight >}}

Now we have three possibility

* Set in frontmatter.

* General: set in <code>_config.yml</code>

* Fallback: hardcoded as <code>images/broken-person.png</code>

You can use any image, whatever you like,
rather than just hardcoded example.

#### Content: Frontmatter Example

{{< highlight yaml >}}
title     : Moody Blues - Nights in White Satin
date      : 2014/03/15 07:35:05
opengraphimage: site/images/adverts/one-page.png
{{< / highlight >}}

This will result as below:

{{< highlight html >}}
  <meta property="og:image" content="http://example/site/images/adverts/one-page.png">
{{< / highlight >}}

#### Config: YAML Example

We can set parameters in configuration, as below:

* <code>_config.yml</code>
  : [gitlab.com/.../_config.yml][tutor-hexo-config]

{{< highlight yaml >}}
# Custom
opengraphimage : "images/logo-gear-opengraph.png"
{{< / highlight >}}

#### Theme Configuration

To avoid hardcoded layout,
theme can also have its own configuration as shown below:

* <code>themes/tutor-05/_config.yml</code>
  : [gitlab.com/.../themes/tutor-05/_config.yml][tutor-theme-config]

{{< highlight yaml >}}
# Opengraph

og_latitude     : "-6.193665"
og_longitude    : "106.848558"
og_locality     : Jakarta
og_country_name : Indonesia
{{< / highlight >}}

The value in this theme configuration can be called in template.

#### Layout: EJS Meta Opengraph

Now the view part.

* <code>themes/tutor-05/layout/meta/opengraph.ejs</code>
  : [gitlab.com/.../layout/meta/opengraph.ejs][tutor-l-meta-og]

{{< highlight html >}}
<%  
  ...
%>

  <meta property="og:locale"
        content="<%= config.language || "en" %>">
  <meta property="og:title"
        content="<%= title || config.title %>">
<% if (is_post()) {%>
  <meta property="og:type"
        content="article">
<% } %>
<% if (page.excerpt) {%>
  <meta property="og:description"  
        content="<%= description || config.description %>">
<% } %>
  <meta property="og:url"          content="<%= page.permalink %>">
  <meta property="og:site_name"    content="<%= config.title %>">
  <meta property="og:image"        content="<%= ogi %>">
  <meta property="og:latitude"     content="<%= theme.og_latitude %>"/>
  <meta property="og:longitude"    content="<%= theme.og_longitude %>"/>
  <meta property="og:locality"     content="<%= theme.og_locality %>"/>
  <meta property="og:country-name" content="<%= theme.og_country_name %>"/>
{{< / highlight >}}

-- -- --

### 6: SEO: Twitter

The twitter version is much more simple.
Because, I do not really pay attention to twitter.

* <code>themes/tutor-05/layout/meta/twitter.ejs</code>
  : [gitlab.com/.../layout/meta/twitter.ejs][tutor-l-meta-twitter]

{{< highlight html >}}
  <meta name="twitter:title"
        content="<%= title || config.title %>">

<% if (page.excerpt) {%>
  <meta name="twitter:description"
        content="<%= description || config.description %>">
<% } %>

<% if (site.data.owner) {%>
  <meta name="twitter:site"
        content="@<%= site.data.owner.twitter %>">
<% } %>
{{< / highlight >}}

I have an example using Jekyll,
if you need example about twitter card.

-- -- --

### 7: Preview: Social Media

How does it looks on social media?
Enough with code, now see figure below.

#### Telegram on Smartphone

![Hexo Meta: Opengraph Telegram Smartphone][image-ss-05-opengraph-slack]

#### Slack on Smartphone

![Hexo Meta: Opengraph Slack Smartphone][image-ss-05-opengraph-tele]

The thing is, every social media interpret differently.
In fact smartphone view and desktop,
show have different result of opengraph image,
based on image size and screen width,
as you can see in inkscape article above.

-- -- --

### What is Next ?

Consider continue reading [ [Netlify - Hosting][local-whats-next] ].
There are, some interesting topic about, how to make your blog site live.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2019/05/29/deploy-netlify/

[image-ss-05-source]:   {{< assets-ssg >}}/2019/05/hexo-ejs-bulma-tutor-05-content.zip

[image-ss-05-seo]:          {{< assets-ssg >}}/2019/05/59-meta-seo.png
[image-ss-05-tele-opengraph]:   {{< assets-ssg >}}/2019/05/59-tele-desktop-opengraph.png
[image-ss-05-opengraph-slack]:  {{< assets-ssg >}}/2019/05/59-opengraph-slack.png
[image-ss-05-opengraph-tele]:   {{< assets-ssg >}}/2019/05/59-opengraph-telegram.png

[tutor-hexo-config]:    {{< tutor-hexo-bulma >}}/_config.yml
[tutor-theme-config]:   {{< tutor-hexo-bulma >}}/themes/tutor-05/_config.yml
[tutor-l-site-head]:    {{< tutor-hexo-bulma >}}/themes/tutor-05/layout/site/head.ejs
[tutor-l-meta-html]:    {{< tutor-hexo-bulma >}}/themes/tutor-05/layout/meta/html.ejs
[tutor-l-meta-seo-w3c]: {{< tutor-hexo-bulma >}}/themes/tutor-05/layout/meta/seo.ejs
[tutor-l-meta-og]:      {{< tutor-hexo-bulma >}}/themes/tutor-05/layout/meta/opengraph.ejs
[tutor-l-meta-twitter]: {{< tutor-hexo-bulma >}}/themes/tutor-05/layout/meta/twitter.ejs
