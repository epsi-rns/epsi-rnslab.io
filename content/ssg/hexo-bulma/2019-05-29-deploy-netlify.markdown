---
type   : post
title  : "Netlify - Deploying"
date   : 2019-05-29T09:17:35+07:00
slug   : deploy-netlify
categories: [ssg]
tags      : [hexo, git]
keywords  : [static site, github, gitlab, netlify]
author : epsi
opengraph:
  image: assets/site/images/topics/git.png

toc    : "toc-2019-05-hexo-bulma-step"

excerpt:
  Deploying Pages on Netlify for Dummies.

---

### Preface

This article is intended for beginner.

> Goal: Hosting on Netlify Step By Step

#### Related Article

I also wrote about Git Hosting.
Step by step article, that you can read here:

* [Making Github/ Gitlab Pages Step By Step][local-git-hosting]

You may also want to read the first mention above,
before continue reading this article.

#### Choice: Deploying Method

How many ways to deploy your static site?
I found at least ten ways to deploy SSG site.

![Illustration: Deploy Options][illustration-deploy]

-- -- --

### 1: Prepare Your Repository

Netlify only build repo to static site,
but doesn't host the repository itself.
So you need to create your own repositry.

#### Where ?

You can create git repository using

* <https://github.com/>

* <https://about.gitlab.com/>

* <https://bitbucket.org/>

#### Practice

Now you can make repository, with any name, for example:

* <https://github.com/epsi-rns/akutidaktahu>

Note that, this is just an example,
you should use your username account.
and you should use your own repository.

![Github: Project Page Repository][image-ss-github-akutidaktahu]

-- -- --

### 2: Using Netlify

This step doesn't require coding at all.
Using figures below as an explanation would be sufficient.

#### New Site

Create a <code>netlify</code>, and then create new site

![Netlify: New Site][image-ss-netlify-00-new-site]

#### Continuous Deployment

Netlify is using __Continuous Deployment__.
You should pick the source of your repository.

![Netlify: CI CD][image-ss-netlify-01-cd]

In this example, I choose github.

#### Repository Application

Now you should pick one of the repo in your github

![Netlify: Pick][image-ss-netlify-02-pick]

You might need to setup authorization first,
for your repository, so that netlify can access it.

#### Build Settings

Once you pick a repo, netlify is smart enough to,
determine static site that you have.
In this case Hexo.

![Netlify: Build][image-ss-netlify-03-build]

#### Deploy

After click the deploy button,
netlify will give a random name.

![Netlify: Overview][image-ss-netlify-04-overview]

Tis takes a few seconds to delploy,
depend on your project complexity.

#### Settings

You can preview your setting, and change whatever you might need.

![Netlify: Settings][image-ss-netlify-05-settings]

#### Change Subdomain Name

I mostly change the site name,
with something that easy to remember.

![Netlify: Name][image-ss-netlify-06-name]

#### Live Site

Now it is live. Served in this address:

* [akutidaktahu.netlify.com](https://akutidaktahu.netlify.com/)

![Netlify: Live][image-ss-netlify-07-live]

-- -- --

### What is Next ?

Consider continue reading [ [Hexo - Summary][local-whats-next] ].
There is at last in this article series,
a summary about what Hexo can do.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:             {{< baseurl >}}ssg/2019/05/30/hexo-summary/
[local-git-hosting]:            {{< baseurl >}}devops/2018/08/15/git-hosting/

[image-ss-github-akutidaktahu]: {{< assets-ssg >}}/2019/05/66-github-akutidaktahu.png
[image-ss-netlify-00-new-site]: {{< assets-ssg >}}/2019/05/66-netlify-00-new-site.png
[image-ss-netlify-01-cd]:       {{< assets-ssg >}}/2019/05/66-netlify-01-cd.png
[image-ss-netlify-02-pick]:     {{< assets-ssg >}}/2019/05/66-netlify-02-pick.png
[image-ss-netlify-03-build]:    {{< assets-ssg >}}/2019/05/66-netlify-03-build.png
[image-ss-netlify-04-overview]: {{< assets-ssg >}}/2019/05/66-netlify-04-overview.png
[image-ss-netlify-05-settings]: {{< assets-ssg >}}/2019/05/66-netlify-05-settings.png
[image-ss-netlify-06-name]:     {{< assets-ssg >}}/2019/05/66-netlify-06-name.png
[image-ss-netlify-07-live]:     {{< assets-ssg >}}/2019/05/66-netlify-07-live.png

[illustration-deploy]:  {{< assets-ssg >}}/2020/deploy-options.png
