---
type   : post
title  : "Hexo - Summary"
date   : 2019-05-30T09:09:35+07:00
slug   : hexo-summary
categories: [ssg]
tags      : [hexo]
keywords  : [static site, custom theme, summary, overview]
author : epsi
opengraph:
  image: assets-ssg/2019/05/toc-hexo-all.png

toc    : "toc-2019-05-hexo-bulma-step"

excerpt:
  Building Hexo Site Step by step, with Bulma as stylesheet frontend.
  Summary of article series.

---

### Preface

> Goal: What Hexo can do in this article series

Making theme consist of these steps:

* Preparing general layout

* Making necessary custom pages

* Adding stylesheet for content

* Optional feature, such as pagination


-- -- --

### 1: Layout

#### Minimal Theme

In my early attempt with static site,
it is always trying to understand how the theme works.
The easiest thing to do this is to make a minimal theme,
and modified later. Our minimal theme only need two artefacts.
It is <code>layout.ejs</code> and <code>index.ejs</code>

![Hexo: Minimal Theme][image-ss-01-minimal-theme]

Of course you need better than these two.
From header, footer, to nice looks stylesheet.

![Hexo: Pure HTML Theme][image-ss-01-pure-html-theme]

**article**: [Hexo - Minimal][local-minimal]

#### Layout Template

Your typical layout would contain header and footer
This is an example of typical layout in a theme.

* <code>themes/tutor-01/layout/layout.ejs</code>

{{< highlight html >}}
<html>
<%- partial('site/head') %>
<body>
  <%- partial('site/header') %>
  <div id="content">
    <%- body %>
  </div>
  <%- partial('site/footer') %>
</body>
</html>
{{< / highlight >}}

![Hexo: ViM Layout][image-ss-01-vim-layout]

And your typical content would be,
a markdown file (or html file) with frontmatter on top.

* <code>source/index.html</code>

{{< highlight html >}}
---
title: Welcome to Heartbeat
---

  <p>Every journey begins with a single step.</p>
{{< / highlight >}}

With very humble result as:

![Hexo: Landing Page on Browser][image-ss-01-b-landing]

**article**: [Hexo - Layout][local-layout]

#### Adding Assets

You can add any necessary assets such as:
images, css frameworks, and javascript frameworks.

![Hexo Bulma: Tree][image-ss-02-tree-bulma]

And see the result in action:

![Hexo Bulma: Layout Homepage][image-ss-02-browser-homepage]

**article**: [Hexo - Bulma - CSS - Part One][local-css-intro]

#### CSS Frameworks

Using CSS framework such as Bulma or Bootstrap,
can make your life easier.
Here is an example of Bulma top navigation in Hexo:

![Hexo Bulma: Layout Navigation Bar][image-ss-02-browser-navbar]

**article**: [Hexo - Bulma - CSS - Part Two][local-css-layout]

#### Using SASS

SASS is a must have skill for theme maker.
Every CSS frameworks built on CSS preprocessor.
Long code tends to get messy.
You need to get organized.

![Hexo Bulma SASS: Tree Sass][image-ss-03-tree-sass]

Now you can have a nicer looks in browser:

![Hexo Bulma: Layout Homepage][image-ss-03-browser-homepage]

With your own colourshceme, to match your company logo,
or your personal portfolio.

**article**: [Hexo - Bulma - SASS - Part One][local-sass-intro]

#### More Layout

Or further, you can utilize stylesheet,
to control your responsive space, such as padding and margin.
Then custom component, such as box effect that control:
border, shadow, rounded.

![Hexo Bulma: Post with sidebar Layout][image-ss-03-browser-post-flex]

**article**: [Hexo - Bulma - SASS - Part Two][local-sass-layout]

Now we are done with Hexo's layout.

-- -- --

### 2: Custom Pages

Hexo has this four special page sections:

* Index

* Archive

* Category

* Tag

#### Default Pages

The URL looks like these below:

* <http://localhost:4000/blog/>

* <http://localhost:4000/archives/>

* <http://localhost:4000/archives/2015/>

* <http://localhost:4000/archives/2015/05/>

* <http://localhost:4000/categories/lyric/>

* <http://localhost:4000/tags/pop/>

For example, this is a tag page: 

![Hexo: Browser Section Tag][image-ss-03-section-tag]

**article**: [Hexo - EJS - Section List - Part One][local-section-list-intro]

#### Custom Layouts Pages

However you still have to make your own page for summary:

* <http://localhost:4000/categories.html>

* <http://localhost:4000/tags.html>

Or even landing Page

* <http://localhost:4000/>

#### Styling Layout

Instead of plain looks, you can style custom layout.

![Hexo: Browser Section Tag][image-ss-04-section-tag]

With any looks, as creative as you can.

![Hexo: Blog Index Refactored][image-ss-04-b-index-02]

**article**: [Hexo - EJS - Section List - Part Two][local-section-list-layout]

#### Simple Summary Page

Your attempt to make summary page should be just a simple loop.

![Hexo: Custom Page: Simple Tag List][image-ss-03-browser-tags-only]

And then you can make a nice looks,
from that simple loop above,
using bulma's button class.

![Hexo: Refactoring Terms: Button of Tags][image-ss-04-browser-tags-btn]

**article**: [Hexo - EJS - Custom Page - Part One][local-custom-page-intro]

#### Complex Summary Page

You can go further by showing tree of pages in this summary page.

![Hexo: Refactoring Terms: Categories][image-ss-04-browser-categories]

**article**: [Hexo - EJS - Custom Page - Part Two][local-custom-page-layout]

#### Custom Output

Without adding any plugin, Hexo cannot generate custom output.
But we can render any content to <code>.html</code>,
or <code>.js</code>, as shown in local <code>json</code> file below:

* file:///home/username/.../archives_local.json

![Hexo Bulma: Archives JSON File][image-ss-04-cof-json]

This is useful when you need to generate data.

**article**: [Hexo - EJS - Custom Output][local-custom-output]

#### Widget

Hexo is not just about page and post,
you can put anything in your layout as creative as you are.
For example is this archive tree below:

![Hexo: Widget Archives Grouped][image-ss-04-widget-a-grouped]

**article**: [Hexo - EJS - Widget][local-widget]

#### Data

You can also utilize data to generate content,
such as backlink, internal link, or other stuff.

{{< highlight yaml >}}
# Helper for related links
# Archives by Date

...

- id: 15010108
  title: "Dead Poet Society"
  url: 2015/01/01/quotes/dead-poets-society/

- id: 15100308
  title: "Every Day"
  url: 2015/10/03/quotes/every-day/

...
{{< / highlight >}}

![Hexo: Data Archives][image-ss-04-vim-archives]

Now consider link the data from this content below:

{{< highlight yaml >}}
---
title     : Susan Wong - I Wish You Love
date      : 2015/05/15 07:35:05
tags      : [pop, 70s]
category  : [lyric]

related_link_ids: 
  - 15051515  # The Way We Were
---

I wish you bluebirds in the spring  
To give your heart a song to sing

<!-- more --> 

I wish you health  
And more than wealth  
I wish you love
{{< / highlight >}}

![Hexo: Content with Data][image-ss-04-vim-susan-wong]

With that article, now we can see one or more links in a widget:

![Hexo: Widget Related Posts][image-ss-04-widget-related-post]

**article**: [Hexo - EJS - Data][local-data]

-- -- --

### 3: Content

After you are done with basic layout and cutom pages,
now you can work in detail with content.

#### Layout: EJS Post

Your typical <code>post.ejs</code> should contain header,
and optionally footer (such as license) and navigation.
Or even more, services such as google adsense and disqus comment.

* <code>themes/tutor-04/layout/post.ejs</code>

{{< highlight javascript >}}
  <main role="main" 
        class="column is-two-thirds has-background-white
               blog-column box-deco">
    <div class="blog-post">
      <%- partial('post/header') %>

      <article>
        <%- page.content %>
      </article>
      
      <br/>
      <%- partial('post/navigation') %>
    </div><!-- /.blog-post -->
    
    <%- partial('post/footer') %>
  </main>

  <aside class="sidebar column is-one-thirds is-paddingless">
    <%- partial('widget/related-posts') %>
    ...
  </aside>
{{< / highlight >}}

![Hexo Content: ViM Post Layout][image-ss-05-vim-post-layout]

A complete post can be seen in preview below:

![Hexo Content: All Artefacts][image-ss-05-blog-post-all]

**article**: [Hexo - Content - Blog Post][local-content-post]

#### Markdown

Be aware of markdown configuration,
as it takes tweak to make it follow standard practice.

* <code>_config.yml</code>

{{< highlight yaml >}}
# Markdown
marked:
  gfm: true
  breaks: false
{{< / highlight >}}

Your typical markdown is shown as below:

{{< highlight markdown >}}
---
layout    : page
...
---

# Header 1

My content

[My Link][my-image]

-- -- --

## Header 1.1

My sub content

![My Image][mylink]

-- -- --

## Header 1.2

My other sub content

[//]: <> ( -- -- -- links below -- -- -- )

[my-image]:    http://some.site/an-image.jpg
[my-link]:     http://some.site/an-article.html
{{< / highlight >}}

There is also hexo admin to help you work with markdown.

* [github.com/jaredly/hexo-admin](https://github.com/jaredly/hexo-admin)

Just be aware for Bulma user, that you need to make an adjustment,
to make heading works in Hexo.

![Hexo Markdown: Fix Bulma Heading Class Size][image-ss-05-heading-styled]

**article**: [Hexo - Content - Markdown][local-content-markdown]

#### Layout: Raw HTML

You can also add raw html layout instead of EJS.
This is useful when you want to add service,
or just plain html such as TOC in figure below:

![Hexo Raw HTML: Table of Content][image-ss-05-toc]

**article**: [Hexo - Content - Raw HTML][local-content-raw-html]

#### Syntax Highlighting

Syntax highlighting in Hexo is using <code>figure</code> tag.
Sometimes you need to fix the stylesheet. It is an easy task.
Then later you can add color and stuff to make it looks nicer.
After all it depends on your creativity.

![Hexo Syntax Highlighting: Reset Class in Figure Element][image-ss-05-unstyled]

**article**: [Hexo - Content - Syntax Highlighting][local-syntax-highlighting]

#### Meta SEO

Content is not just for human, but also for machine that read this page.
Especially search engine and social media.
Opengraph is a must nowadays.
Your article should have preview as shown in telegram chat below.

![Hexo Meta: Telegram Desktop Opengraph][image-ss-05-tele-opengraph]

**article**: [Hexo - Meta - SEO][local-meta-seo]

-- -- --

### 4: Pagination

Sometimes you need a special feature that require algorithm.
Yes, this pagination is common, but a complex task to do.
Luckily I have made a step by step guidance,
on how to achieve it with Hexo.

#### Numbering

Using EJS and Bulma stylesheet, a common pagination could be achieved.

![Hexo Pagination: Combined Animation][image-ss-05-combined-animation]

**article**: [Hexo - Pagination - Indicator][local-pagination-indicator]

#### Responsive

Using EJS custom stylesheet, a responsive pagination also could be achieved.

![Hexo Pagination: Responsive Animation][image-ss-05-responsive-animation]

**article**: [Hexo - Pagination - Responsive][local-pagination-responsive]

#### Handling URL

There are sections, that we can put pagination inside it.
We need to handle these URL correctly with EJS function.

* Index: <http://localhost:4000/blog/>

* Archives (root): <http://localhost:4000/archives/>

* Archives (year): <http://localhost:4000/archives/2018/>

* Archives (month): <http://localhost:4000/archives/2018/09/>

* Tags: <http://localhost:4000/tags/pop/>

* Categories: <http://localhost:4000/categories/lyric/>

Pagination can be placed in many places.

![Hexo: Browser Section Tag][image-ss-05-section-tag]

**article**: [Hexo - Pagination - Handling URL][local-pagination-handling-url]

-- -- --

### Conclusion

I think that is all.

This media can also be read in a nice web presentation.
Consider continue reading [ [Hexo - Presentation Slide][local-whats-next] ].

What do you think ?

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2019/05/30/hexo-presentation/

[local-bulma-overview]: {{< baseurl >}}frontend/2019/03/01/bulma-overview/

[tutor-hexo-master]:    {{< tutor-hexo-bulma >}}/
[tutor-html-master]:    {{< tutor-html-bulma >}}/

[local-hexo-minimal]:   {{< baseurl >}}ssg/2018/09/02/hexo-minimal/

[tutor-html-master]:    {{< tutor-html-bulma >}}/

[image-ss-01-minimal-theme]:    {{< assets-ssg >}}/2019/05/11-tree-minimal-theme.png
[image-ss-01-pure-html-theme]:  {{< assets-ssg >}}/2019/05/13-tree-pure-html-theme.png
[image-ss-01-b-landing]:        {{< assets-ssg >}}/2019/05/13-browser-landing-page.png
[image-ss-01-vim-layout]:       {{< assets-ssg >}}/2019/05/13-vim-layout-ejs.png
[image-ss-02-tree-bulma]:       {{< assets-ssg >}}/2019/05/21-tree-bulma-asset.png
[image-ss-02-browser-homepage]: {{< assets-ssg >}}/2019/05/22-browser-homepage.png
[image-ss-02-browser-navbar]:   {{< assets-ssg >}}/2019/05/22-browser-navbar.png
[image-ss-03-tree-sass]:        {{< assets-ssg >}}/2019/05/31-tree-sass.png
[image-ss-03-browser-homepage]: {{< assets-ssg >}}/2019/05/32-browser-homepage.png
[image-ss-03-browser-post]:     {{< assets-ssg >}}/2019/05/35-browser-post.png
[image-ss-03-browser-post-flex]:{{< assets-ssg >}}/2019/05/35-browser-post-flex.png

[image-ss-03-section-tag]:      {{< assets-ssg >}}/2019/05/37-section-tag.png
[image-ss-04-section-tag]:      {{< assets-ssg >}}/2019/05/42-section-tag.png
[image-ss-04-b-index-02]:       {{< assets-ssg >}}/2019/05/42-browser-index-refactored.png
[image-ss-03-browser-tags-only]:{{< assets-ssg >}}/2019/05/38-browser-tags-only.png
[image-ss-04-browser-tags-btn]: {{< assets-ssg >}}/2019/05/41-browser-tags-buttons.png
[image-ss-04-browser-categories]:   {{< assets-ssg >}}/2019/05/41-browser-categories.png
[image-ss-04-cof-json]:             {{< assets-ssg >}}/2019/05/44-custom-output-json-file.png
[image-ss-04-widget-a-grouped]:     {{< assets-ssg >}}/2019/05/43-widget-archives-grouped.png
[image-ss-04-widget-related-post]:  {{< assets-ssg >}}/2019/05/43-widget-related-post.png
[image-ss-04-vim-susan-wong]:       {{< assets-ssg >}}/2019/05/43-vim-susan-wong.png
[image-ss-04-vim-archives]:         {{< assets-ssg >}}/2019/05/43-vim-data-archives-yml.png

[image-ss-05-blog-post-all]:    {{< assets-ssg >}}/2019/05/61-blog-post-all.png
[image-ss-05-heading-styled]:   {{< assets-ssg >}}/2019/05/62-heading-styled.png
[image-ss-05-vim-post-layout]:  {{< assets-ssg >}}/2019/05/62-vim-layout-post-ejs.png
[image-ss-05-toc]:              {{< assets-ssg >}}/2019/05/63-toc-susan-wong.png
[image-ss-05-unstyled]:         {{< assets-ssg >}}/2019/05/64-highlight-unstyled.png
[image-ss-05-tele-opengraph]:   {{< assets-ssg >}}/2019/05/59-tele-desktop-opengraph.png

[image-ss-05-combined-animation]:   {{< assets-ssg >}}/2019/05/54-hexo-bulma-indicator-animate.gif
[image-ss-05-responsive-animation]: {{< assets-ssg >}}/2019/05/55-hexo-bulma-responsive-animate.gif
[image-ss-05-section-tag]:          {{< assets-ssg >}}/2019/05/57-section-tag.png


[local-overview]:                   {{< baseurl >}}ssg/2019/05/01/hexo-overview/
[local-install]:                    {{< baseurl >}}ssg/2019/05/02/hexo-install/
[local-minimal]:                    {{< baseurl >}}ssg/2019/05/03/hexo-minimal/
[local-layout]:                     {{< baseurl >}}ssg/2019/05/04/hexo-layout/
[local-css-intro]:                  {{< baseurl >}}ssg/2019/05/05/hexo-bulma-css-intro/
[local-css-layout]:                 {{< baseurl >}}ssg/2019/05/06/hexo-bulma-css-layout/
[local-sass-intro]:                 {{< baseurl >}}ssg/2019/05/07/hexo-bulma-sass-intro/
[local-sass-layout]:                {{< baseurl >}}ssg/2019/05/08/hexo-bulma-sass-layout/
[local-section-list-intro]:         {{< baseurl >}}ssg/2019/05/10/hexo-ejs-section-list-intro/
[local-section-list-layout]:        {{< baseurl >}}ssg/2019/05/13/hexo-ejs-section-list-layout/
[local-custom-page-intro]:          {{< baseurl >}}ssg/2019/05/11/hexo-ejs-custom-page-intro/
[local-custom-page-layout]:         {{< baseurl >}}ssg/2019/05/12/hexo-ejs-custom-page-layout/
[local-custom-output]:              {{< baseurl >}}ssg/2019/05/14/hexo-ejs-custom-output/
[local-widget]:                     {{< baseurl >}}ssg/2019/05/15/hexo-ejs-widget/
[local-data]:                       {{< baseurl >}}ssg/2019/05/16/hexo-ejs-data/
[local-pagination-intro]:           {{< baseurl >}}ssg/2019/05/17/hexo-pagination-intro/
[local-pagination-simple]:          {{< baseurl >}}ssg/2019/05/17/hexo-pagination-simple/
[local-pagination-number]:          {{< baseurl >}}ssg/2019/05/18/hexo-pagination-number/
[local-pagination-navigation]:      {{< baseurl >}}ssg/2019/05/18/hexo-pagination-navigation/
[local-pagination-adjacent]:        {{< baseurl >}}ssg/2019/05/19/hexo-pagination-adjacent/
[local-pagination-logic]:           {{< baseurl >}}ssg/2019/05/19/hexo-pagination-logic/
[local-pagination-indicator]:       {{< baseurl >}}ssg/2019/05/20/hexo-pagination-indicator/
[local-pagination-responsive]:      {{< baseurl >}}ssg/2019/05/21/hexo-pagination-responsive/
[local-pagination-screenreader]:    {{< baseurl >}}ssg/2019/05/22/hexo-pagination-screenreader/
[local-pagination-handling-url]:    {{< baseurl >}}ssg/2019/05/23/hexo-pagination-handling-url/
[local-pagination-filering-caveat]: {{< baseurl >}}ssg/2019/05/23/hexo-pagination-filtering-caveat/
[local-content-post]:               {{< baseurl >}}ssg/2019/05/24/hexo-content-post/
[local-content-markdown]:           {{< baseurl >}}ssg/2019/05/25/hexo-content-markdown/
[local-content-raw-html]:           {{< baseurl >}}ssg/2019/05/26/hexo-content-html/
[local-syntax-highlighting]:        {{< baseurl >}}ssg/2019/05/27/hexo-syntax-highlighting/
[local-meta-seo]:                   {{< baseurl >}}ssg/2019/05/28/hexo-meta-seo/
[local-deploy-netlify]:             {{< baseurl >}}ssg/2019/05/29/deploy-netlify/

