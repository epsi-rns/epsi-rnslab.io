---
type   : post
title  : "Hugo - Summary"
date   : 2018-09-01T09:17:35+07:00
slug   : hugo-summary
categories: [ssg]
tags      : [hugo]
keywords  : [static site, custom theme, summary, overview]
author : epsi
opengraph:
  image: assets/site/images/topics/hugo-bootstrap-markdown.png

toc    : "toc-2018-09-hugo-bootstrap-step"

excerpt:
  Building Hugo Site Step by step, with Bootstrap as stylesheet frontend.
  Summary of article series.

---

### Preface

This article series is intended for beginner.

> Goal: Explain Hugo Step By Step

I'm using <code>Bootstrap</code> as an example in this guidance.
Of course you can use any CSS framework that you like,
such as <code>Materialize</code> or <code>Bulma</code>.

#### About Hugo

Hugo is an SSG (Static Site Generator) that is supported by **gitlab**.
Or used pairly by using **github** with **netlify**.
Hugo is not the only **SSG** (Static Site Generator).

#### Why Hugo ?

> Hugo is Fast.

After blogging time to time, after 194 article,
my Jekyll become so slugish.
I have to move some articles to new Blog.
Now it is done.

#### Part of Hugo

There are these two part that you need to know.

* Black Friday: Markdown Parser for Hugo

* Chroma: Templating Language for Hugo

#### Source Code

Source code used in this tutorial, is available at:

*	[gitlab.com/epsi-rns/tutor-hugo-bootstrap][tutor-hugo-master]

#### Disclaimer

This is would not be the best blog template that you ever have.
Because I only put most common stuff,
to keep the tutorial simple.

After you learn this guidance,
you understand the fundamental skill.
Thus, a base for you, to make your own blog site.
With your imagination, you may continue,
to build your own super duper Hugo site.
Far better than, what I have achieved.

#### Demo Hugo

I also made a site, intended as a Demo.
It is based on this tutorial, but in a more enhanced way,
to face real life, blog building situation.

*	[Demo Hugo](https://epsi-rns.gitlab.io/demo-hugo/)

And add some aestethic appeal, with more custom stylesheet.

![Hugo: Demo][image-ss-07-demo-hugo]

#### Table of Content

The table content is available as header on each tutorial.
There, I present a Hugo Tutorial, step by step, for beginners.

-- -- --

### Begin The Guidance

Let's get the tutorial started.

Consider continue reading [ [Hugo Minimal][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2018/09/02/hugo-minimal/

[tutor-hugo-master]:    {{< tutor-hugo-bootstrap >}}/

[image-ss-07-demo-hugo]:    {{< assets-ssg >}}/2018/12/67-demo-hugo.png
