---
type   : post
title  : "Hugo - Minimal"
date   : 2018-09-02T09:17:35+07:00
slug   : hugo-minimal
categories: [ssg]
tags      : [hugo]
keywords  : [static site, custom theme, configuration, markdown, layout]
author : epsi
opengraph:
  image: assets/site/images/topics/hugo-bootstrap-markdown.png

toc    : "toc-2018-09-hugo-bootstrap-step"

excerpt:
  Building Hugo Step by step, with Bootstrap as stylesheet frontend.
  Show Minimal Homepage in Hugo.

---

### Preface

This article is intended for beginner.

> Goal: Show Minimal Homepage in Hugo

When you are done with install, this is what to do.

#### Source Code

You can download the source code of this article here.

* [tutor-hugo-minimal.tar][image-ss-00-source]

Here, I present a Hugo Tutorial, step by step, for beginners.

-- -- --

### 0: Install Hugo

Yeps, array start from zero.

Installing Hugo is easy.
No dependency issue, as go compile it with static linking.
Most distro has Hugo package.
This should not be hard for most people.
Just follow the official documentation:

* <https://gohugo.io/getting-started/installing/>

Do not worry about screenshot below.
This is just illustration, in my linux box.
You can also use Hugo with Windows.

![Hugo: Install][image-ss-00-install-hugo]

After install, let's get the guidance started.

-- -- --

### 1: Source Structure

It only takes a few steps.

#### 1. First

After install, make sure that, Hugo run from CLI.

{{< highlight bash >}}
$ hugo version
Hugo Static Site Generator v0.42.1-4172A835E544B867F292579019D0597439E596BA linux/amd64 BuildDate: 
{{< / highlight >}}

![Hugo: Version][image-ss-01-version]

#### 2. Second

Make a directory for your project.

{{< highlight bash >}}
$ mkdir tutor-hugo
$ cd tutor-hugo
{{< / highlight >}}

#### 3. Other Step

Make a skeleton for your project.

{{< highlight bash >}}
$ hugo new site .
Congratulations! Your new Hugo site is created in /media/Works/sites/tutor-hugo.
{{< / highlight >}}

![Hugo: New Site][image-ss-01-new-site]

The result of that command above is
a newly created directory and files as below.

<code>
$ tree
</code>

{{< highlight conf >}}
.
├── archetypes
│  └── default.md
├── config.toml
├── content
├── data
├── layouts
├── static
└── themes
{{< / highlight >}}

![Hugo: New Site Tree][image-ss-01-tree]

#### 4. See what is in there

All these directories and files are empty, except these two artefacts:
<code>config.toml</code>, and
<code>archetypes/default.md</code>.

* <code>config.toml</code>
  : [gitlab.com/.../config.toml][tutor-hugo-config]

{{< highlight toml >}}
baseURL = "http://example.org/"
languageCode = "en-us"
title = "My New Hugo Site"
{{< / highlight >}}

![Hugo: Config][image-ss-01-config]

As a beginner, you only need to care about <code>config.toml</code>.
But there are other file, as well as below.

* <code>archetypes/default.md</code>
  : [gitlab.com/.../archetypes/default.md][tutor-hugo-archetypes-default]

{{< highlight yaml >}}
---
title: "{{ replace .TranslationBaseName "-" " " | title }}"
date: {{ .Date }}
draft: true
---
{{< / highlight >}}

![Hugo: archetypes][image-ss-01-archetypes]

I rarely use archetypes.

-- -- --

### 2: Generated Structure

Hugo use in memory mapping, in contrast with Jekyll,
that use <code>_site</code> directory for server.
Jekyll have to copy images for each change,
it has performance impact, if you have a lot of images.
Hugo use some kind of memory mapping, thus no performance impact.

#### Public Directory

Sometimes, you need to debug.
As an SSG, Hugo can dump the generated output to a directory.
Hugo use <code>public</code> directory as default.

{{< highlight conf >}}
$ hugo

                   | EN  
+------------------+----+
  Pages            |  3  
  Paginator pages  |  0  
  Non-page files   |  0  
  Static files     |  0  
  Processed images |  0  
  Aliases          |  0  
  Sitemaps         |  1  
  Cleaned          |  0  

Total in 16 ms
{{< / highlight >}}

![Hugo: generate public][image-ss-02-hugo-public]

#### Default XML

The command above will generate, static site.

{{< highlight conf >}}
$ exa public --tree
public
├── categories
│  └── index.xml
├── index.xml
├── sitemap.xml
└── tags
   └── index.xml
{{< / highlight >}}

![Hugo: tree public][image-ss-02-tree-public]

It has no <code>index.html</code> yet.
Because we have not define them yet.
Hugo, as default, generate a few <code>.xml</code> files.

![Hugo: index.xml][image-ss-02-b-index-xml]

#### High Performance Server

On daily basis you issue <code>hugo server</code>,
instead of just <code>hugo</code> command.

{{< highlight conf >}}
$ hugo server
                   | EN  
+------------------+----+
  Pages            |  3  
  Paginator pages  |  0  
  Non-page files   |  0  
  Static files     |  0  
  Processed images |  0  
  Aliases          |  0  
  Sitemaps         |  1  
  Cleaned          |  0  

Total in 6 ms
Watching for changes in /media/Works/sites/tutor-hugo/{content,data,layouts,static}
Watching for config changes in /media/Works/sites/tutor-hugo/config.toml
Serving pages from memory
Running in Fast Render Mode. For full rebuilds on change: hugo server --disableFastRender
Web Server is available at http://localhost:1313/ (bind address 127.0.0.1)
Press Ctrl+C to stop
{{< / highlight >}}

![Hugo: hugo server][image-ss-02-hugo-server]

#### Check the Site

Now, the Hugo server is running.
But no content yet.
You can check the site using <code>curl</code>

<code>
$ curl http://localhost:1313/
</code>

{{< highlight html >}}
<pre>
</pre>
{{< / highlight >}}

![Hugo: localhost:1313][image-ss-02-c-localhost]

-- -- --

### 3: Minimal Homepage

In order to have a working index.html you need this two artefacts.

* <code>content/_index.html</code>
  : [gitlab.com/.../content/_index.html][tutor-hugo-content-index]

* <code>layout/index.html</code>

#### Content

Consider this is, as our very first content.

{{< highlight html >}}
$ cat content/_index.html

  <p>There are so many things to say.
  I don't want to live in regrets.
  So I wrote this for my love.</p>
{{< / highlight >}}

Or alternatively:

{{< highlight markdown >}}
$ cat content/_index.md

There are so many things to say.
I don't want to live in regrets.
So I wrote this for my love.
{{< / highlight >}}

You can choose either of these two above format.
But you can not have both index.

#### Layout

Here we have our very first <code>Go html/template/code> tag.

{{< highlight html >}}
$ cat layouts/index.html

<p>Dear love,</p>

{{ .Content }}

<p>Sincerely yours.</p>
{{< / highlight >}}

Sorry for my english, It is just an example.

#### Server Output: Browser

Now check on browser

![Hugo: Page on Browser][image-ss-03-page-minimal]

#### Compare Source And Generated

You can examine how this works,
by comparing the source directory, and generated directory.

{{< highlight html >}}
$ exa --tree
.
├── archetypes
│  └── default.md
├── config.toml
├── content
│  └── _index.html
├── data
├── layouts
│  └── index.html
├── public
│  ├── categories
│  │  └── index.xml
│  ├── index.html
│  ├── index.xml
│  ├── sitemap.xml
│  └── tags
│     └── index.xml
├── static
└── themes
{{< / highlight >}}

![Hugo: Tree to compare directory][image-ss-03-tree-minimal]

_._

#### Server Output: HTML Source

Check also the source with <code>curl</code>.
And compare with above figure.

![Hugo: curl on terminal][image-ss-03-curl-minimal]

#### Remove Temporary Example

Now we are safely to remove layouts/index.html in root.
We need to remove to avoid conflict with the next part,
the layout inside theme.
We are going to use make all layouts in theme.

{{< highlight bash >}}
$ rm layouts/index.html
{{< / highlight >}}

-- -- --

### What is Next ?

Consider continue reading [ [Hugo Layout][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2018/09/03/hugo-layout/

[image-ss-00-source]:       {{< assets-ssg >}}/2018/09/tutor-hugo-minimal.tar

[image-ss-00-install-hugo]: {{< assets-ssg >}}/2018/09/00-hugo-install.png

[image-ss-01-new-site]:     {{< assets-ssg >}}/2018/09/01-hugo-new-site.png
[image-ss-01-version]:      {{< assets-ssg >}}/2018/09/01-hugo-version.png
[image-ss-01-tree]:         {{< assets-ssg >}}/2018/09/01-tree.png
[image-ss-01-config]:       {{< assets-ssg >}}/2018/09/01-cat-config-toml.png
[image-ss-01-archetypes]:   {{< assets-ssg >}}/2018/09/01-cat-archetypes-default.png

[image-ss-02-b-index-xml]:  {{< assets-ssg >}}/2018/09/02-browser-index-xml.png
[image-ss-02-c-localhost]:  {{< assets-ssg >}}/2018/09/02-curl-localhost.png
[image-ss-02-hugo-public]:  {{< assets-ssg >}}/2018/09/02-hugo.png
[image-ss-02-hugo-server]:  {{< assets-ssg >}}/2018/09/02-hugo-server.png
[image-ss-02-tree-public]:  {{< assets-ssg >}}/2018/09/02-tree-public.png

[image-ss-03-page-minimal]: {{< assets-ssg >}}/2018/09/03-browser-minimal-layout.png
[image-ss-03-curl-minimal]: {{< assets-ssg >}}/2018/09/03-curl-minimal-layout.png
[image-ss-03-tree-minimal]: {{< assets-ssg >}}/2018/09/03-minimal-layout.png

[tutor-hugo-content-index]: {{< tutor-hugo-bootstrap >}}/content/_index.html
[tutor-hugo-archetypes-default]: {{< tutor-hugo-bootstrap >}}/archetypes/default.md
[tutor-hugo-config]:        {{< tutor-hugo-bootstrap >}}/config.toml
