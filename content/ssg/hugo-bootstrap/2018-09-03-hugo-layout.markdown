---
type   : post
title  : "Hugo - Layout"
date   : 2018-09-03T09:17:35+07:00
slug   : hugo-layout
categories: [ssg]
tags      : [hugo]
keywords  : [static site, minimal theme, section, taxonomy, custom page, refactoring template, layout]
author : epsi
opengraph:
  image: assets/site/images/topics/hugo-bootstrap-markdown.png

toc    : "toc-2018-09-hugo-bootstrap-step"

excerpt:
  Building Hugo Step by step, with Bootstrap as stylesheet frontend.
  Show Minimalist Layout in Hugo.

---

### Preface

This article is intended for beginner.

> Goal: Show Minimalist Layout in Hugo

In this guidance, there are two context:

* <code>content</code> refer to root directory, and

* <code>layout</code> refer to <code>themes</code> directory.

#### Markdown

Hugo content can use either html or markdown.
You can read about Markdown format here:

* <https://daringfireball.net/projects/markdown/syntax>

#### Source Code

You can download the source code of this article here.

* [tutor-hugo-01.tar][image-ss-01-source]

-- -- --

### 1: Minimal Theme

In order to make a step by step guidance,
we need to move the layout to theme.
Of cource you can use site layout instead of theme layout.
It is just easier to maintain for this tutorial case.

#### New Theme

Consider make our very first new theme.

{{< highlight bash >}}
$ hugo new theme tutor-01
Creating theme at /media/Works/sites/tutor-hugo/themes/tutor-01
{{< / highlight >}}

This will generate skeleton of your theme.

{{< highlight bash >}}
$ tree themes
themes
└── tutor-01
   ├── archetypes
   │  └── default.md
   ├── layouts
   │  ├── 404.html
   │  ├── _default
   │  │  ├── baseof.html
   │  │  ├── list.html
   │  │  └── single.html
   │  ├── index.html
   │  └── partials
   │     ├── footer.html
   │     └── header.html
   ├── LICENSE
   ├── static
   │  ├── css
   │  └── js
   └── theme.toml
{{< / highlight >}}

![Hugo: New Theme][image-ss-01-new-theme]

**Source**:
	[gitlab.com/.../themes/tutor-01][tutor-hugo-tutor-01]
_._

#### Config: Theme

Consider changing <code>config.toml</code>,
to include newly created theme.

{{< highlight bash >}}
baseURL      = "http://example.org/"
languageCode = "en-us"
title        = "Letters to my Beloved"
theme        = "tutor-01"
{{< / highlight >}}

> As the config changed, the server should be restarted.

#### Default: Baseof Template

All html files are empty, with zero based size,
except <code>/layouts/_default/baseof.html</code>.

{{< highlight twig >}}
<html>
    {{- partial "head.html" . -}}
    <body>
        {{- partial "header.html" . -}}
        <div id="content">
        {{- block "main" . }}{{- end }}
        </div>
        {{- partial "footer.html" . -}}
    </body>
</html>
{{< / highlight >}}

**Source**:
	[gitlab.com/.../layouts/_default/baseof.html][tutor-hugo-layouts-baseof]

#### Theme: Homepage

Consider change our layout, in <code>themes/tutor-01/</code>, in
<code>layouts/index.html</code> with block definition.

{{< highlight twig >}}
{{ define "main" }}
<p>Dear love,</p>

{{ .Content }}

<p>Sincerely yours.</p>
{{ end }}
{{< / highlight >}}

**Source**:
	[gitlab.com/.../layouts/index.html][tutor-hugo-layouts-index]

#### Partial: HTML Head

We are not done yet, in <code>themes/tutor-01/</code>,
we need to make <code>partials/head.html</code>.

{{< highlight html >}}
<head>
  <title>{{ .Site.Title }}</title>
</head>
{{< / highlight >}}

**Source**:
	[gitlab.com/.../layouts/partials/head.html][tutor-hugo-layouts-head]

#### Server Output: HTML Source

And the result is:

<code>
$ curl localhost:1313 | less
</code>

{{< highlight html >}}
<html>
<head>
  <meta name="generator" content="Hugo 0.42.1" />
  <title>Letters to my Beloved</title>
</head>
<body><div id="content">
<p>Dear love,</p>

<p>There are so many things to say.
  I don't want to live in regrets.
  So I wrote this for my love.</p>


<p>Sincerely yours.</p>

        </div><script data-no-instant>document.write('<script src="/livereload.js?port=1313&mindelay=10"></' + 'script>')</script></body>
</html>
{{< / highlight >}}

![Hugo: _default/baseof][image-ss-01-baseof]

-- -- --

### 2: Refactor

We can refactor a common layout into these three artefacts.:

* <code>layouts/index.html</code>
  : [gitlab.com/.../layouts/index.html][tutor-hugo-layouts-index]

* <code>layouts/partials/header</code>
  : [gitlab.com/.../layouts/partials/header.html][tutor-hugo-layouts-header]

* <code>layouts/partials/footer</code>
  : [gitlab.com/.../layouts/partials/footer.html][tutor-hugo-layouts-footer]

#### Homepage

* <code>layouts/index.html</code>

{{< highlight twig >}}
{{ define "main" }}
{{ .Content }}
{{ end }}
{{< / highlight >}}

#### Partial: Header

* <code>layouts/partials/header</code>

{{< highlight html >}}
<p><i>My one, and only dream,</i></p>
{{< / highlight >}}

#### Partial: Footer

* <code>layouts/partials/footer</code>

{{< highlight html >}}
<p><i>Wishful thinking.</i></p>
{{< / highlight >}}

#### Server Output: HTML Source

The result is similar:

![Hugo: Browser Homepage][image-ss-02-page-homepage]

{{< highlight html >}}
$ curl localhost:1313 | less
<html>
<head>
   <meta name="generator" content="Hugo 0.42.1" />
  <title>Letters to my Beloved</title>
</head>
<body><p><i>My one, and only dream,</i></p>
<div id="content">
<p>There are so many things to say.
  I don't want to live in regrets.
  So I wrote this for my love.</p>


        </div><p><i>Wishful thinking.</i></p>
<script data-no-instant>document.write('<script src="/livereload.js?port=1313&mindelay=10"></' + 'script>')</script></body>
</html>
{{< / highlight >}}

![Hugo: curl Example: Reuse][image-ss-02-curl-reuse]

Now you can reuse parts as below.

-- -- --

### 3: Reuse

Now we can reuse single page.
We need these four artefacts:

* <code>themes/tutor-01/layouts/partials/head</code>
  : [gitlab.com/.../layouts/partials/head.html][tutor-hugo-layouts-head]

* <code>themes/tutor-01/layouts/_default/single</code>
  : [gitlab.com/.../layouts/_default/single.html][tutor-hugo-layouts-single]

* <code>content/duty.md</code>
  : [gitlab.com/.../content/duty.html][tutor-hugo-content-duty]

* <code>content/letters/hello.html</code>
  : [gitlab.com/.../content/letters/hello.html][tutor-hugo-content-hello]

#### Partial: Head

* <code>themes/tutor-01/layouts/partials/head</code>

{{< highlight html >}}
<head>
  <title>{{ .Page.Title | default .Site.Title }}</title>
</head>
{{< / highlight >}}

You should see different title header at the top of browser later on.

#### Default: Single

* <code>themes/tutor-01/layouts/_default/single</code>

{{< highlight twig >}}
{{ define "main" }}
<blockquote>I pour the <b>journal</b> as daily letter.</blockquote>

{{ .Content }}
{{ end }}
{{< / highlight >}}

#### Example Content: Hello

You can put the post in **any directory**.

* <code>content/letters/hello.html</code>

{{< highlight html >}}
+++
title = "Hello There"
+++
  <p>I wish you are okay over there.
     I just need to say hello.</p>
{{< / highlight >}}

and open it later in

* http://localhost:1313/letters/hello/

![Hugo: Browser Example: Hello][image-ss-03-page-hello]

#### Example Content: Duty

You can use **markdown format** as well

* <code>content/duty.md</code>

{{< highlight markdown >}}
+++
title = "My Apology"
+++
We still have duty calls to be done.
I can't go home anytime soon.
{{< / highlight >}}

![Hugo: Browser Example: Duty][image-ss-03-page-duty]

-- -- --

### 4: Section

We can make a list of each section easily.

#### Example Content: Winter

I also put other content in letters directory.

* <code>content/letters/winter.md</code>

{{< highlight markdown >}}
+++
title = "Surviving White Winter"
+++

It was a frozen winter in cold war era.
We are two men, a boy, two women, a husky, and two shotguns.
After three weeks, we finally configure dzen2.

But we lost our beloved husky before we finally made it.
Now, every january, we remember our husky,
that helped all of us to survive.
{{< / highlight >}}

![Hugo: Browser Example: Winter][image-ss-04-page-winter]

#### Real Directory

Consider examine the tree.

{{< highlight conf >}}
$ tree content

content
├── _index.html
├── duty.md
└── letters
   ├── hello.html
   └── winter.md
{{< / highlight >}}

![Hugo: Tree Content][image-ss-04-tree-content]

_._

#### Section List

By using <code>/layouts/_default/list.html</code>,
you can access <http://localhost:1313/letters/>.

{{< highlight twig >}}
{{ define "main" }}
  <h1>{{ .Section }}</h1>

  {{ .Content }}

  <ul>
    {{ range .Data.Pages }}
    <li>
      <a href="{{ .URL }}">{{ .Title }}</a>
    </li>
    {{ end }}
  </ul>
{{ end }}
{{< / highlight >}}

Note that, this list only show the list of letters folder.

![Hugo: Browser List][image-ss-04-page-list]

**Source**:
	[gitlab.com/.../layouts/_default/list.html][tutor-hugo-layouts-list]

-- -- --

### 5: Taxonomy

Hugo has amazing taxonomy that almost tears my eye.
Earlier we have list of section.
Now we can setup categories and tags easily.

#### Prepare Config

We need to configure the <code>config.toml</code>.

{{< highlight toml >}}
baseURL      = "http://example.org/"
languageCode = "en-us"
title        = "Letters to my Beloved"
theme        = "tutor-01"

[taxonomies]
  category = "categories"
  tag = "tags"
{{< / highlight >}}

As we change the config.toml,
we must also restart the server.

#### Apply in Frontmatter

And add taxonomies into

* <code>content/letters/hello.html</code>
  : [gitlab.com/.../content/letters/hello.html][tutor-hugo-content-hello]

{{< highlight html >}}
+++
title  = "Hello There"
tags   = ["wishful", "story"]
categories = ["love"]
+++
  <p>I wish you are okay over there.
     I just need to say hello.</p>
{{< / highlight >}}

and

* <code>content/letters/winter.md</code>
  : [gitlab.com/.../content/letters/winter.md][tutor-hugo-content-winter]


{{< highlight markdown >}}
+++
title  = "Surviving White Winter"
tags   = ["duty", "story"]
categories = ["love"]
+++

It was a frozen winter in cold war era.
We are two men, a boy, two women, a husky, and two shotguns.
After three weeks, we finally configure dzen2.

But we lost our beloved husky before we finally made it.
Now, every january, we remember our husky,
that helped all of us to survive.
{{< / highlight >}}

#### Taxonomy Terms: Category

We can check taxonomy terms:

![Hugo: Browser Taxonomy][image-ss-05-page-cats]

#### Taxonomy Item: Category

Now we can access:

* <http://localhost:1313/categories/love/>

![Hugo: Browser Taxonomy][image-ss-05-page-item]

#### Taxonomy Item: Tags

We can add any taxonomy terms as well.

![Hugo: Browser Taxonomy][image-ss-05-page-tags]

-- -- --

### 6: Custom Page

We can make custom pages other than the default one.
For example, we make a custom post, with slug and date.

#### Custom Page: Single

For this to achieve, we need to make this one artefact.

* <code>themes/tutor-01/layouts/post/single.html</code>
  : [gitlab.com/.../layouts/post/single.html][tutor-hugo-layouts-post-single]

{{< highlight twig >}}
{{ define "main" }}
<b>{{ .Date.Format "2006, 02 January" }}</b>

<p>Dear Angel,</p>

{{ .Content }}

<p>With deep love in my heart.</p>
{{ end }}
{{< / highlight >}}

#### Example Content: Everyday

Consider make this one post as an example

* <code>content/posts/everyday.md</code>

{{< highlight markdown >}}
+++
type   = "post"
title  = "Everyday I Ask"
date   = 2015-10-03T08:08:15+07:00
slug   = "daily-wish"
tags   = ["wishful", "story"]
categories = ["love"]
+++

You know, some people makes mistakes,
and have to bear the consequences, for the rest of their life.
I made, one good decision when I was far younger than you are.
Later I know it was the right decision.
But I also have to bear the consquences, for the rest of my life.

> I wish you a good family, and happiness.
{{< / highlight >}}

#### Permalink

And also make new permalink format in config.toml.
Restart Hugo server, as the config changing.

Consider this URL to get the new permalink.

* <http://localhost:1313/posts/>

Using above list, you will get:

* <http://localhost:1313/posts/2015/10/03/daily-wish/>

![Hugo: Browser Custom Page][image-ss-06-page-custom]

#### Real Life Blog

> Let's get organized.

Real blog may contain hundreds of articles.
We can put articles in separate sections.
this way, we can maintain article easily.

{{< highlight conf >}}
content
├── _index.html
├── backend
├── code
├── design
├── frontend
├── pages
└── ssg
{{< / highlight >}}

![Hugo: Section Tree][image-ss-06-section-tree]

I also use Jekyll style file naming,
sorting by date, to make the article easier to find.

![Hugo: File Naming][image-ss-06-file-naming]

_._

-- -- --

### What is Next ?

There are, some interesting topic for <code>Hugo</code>,
such as <code>Layout</code> using <code>Semantic HTML5 tag</code>.
Consider continue reading [ [Hugo Bootstrap - Layout][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2018/09/05/hugo-bootstrap-layout/

[image-ss-01-source]:       {{< assets-ssg >}}/2018/09/tutor-hugo-01.tar

[image-ss-01-baseof]:        {{< assets-ssg >}}/2018/09/11-default-base-of.png
[image-ss-01-new-theme]:     {{< assets-ssg >}}/2018/09/11-theme-tutor-01.png

[image-ss-02-page-homepage]: {{< assets-ssg >}}/2018/09/12-browser-homepage.png
[image-ss-02-curl-reuse]:    {{< assets-ssg >}}/2018/09/12-curl-reuse.png

[image-ss-03-page-duty]:     {{< assets-ssg >}}/2018/09/13-browser-reuse-duty.png
[image-ss-03-page-hello]:    {{< assets-ssg >}}/2018/09/13-browser-reuse-hello.png

[image-ss-04-page-list]:     {{< assets-ssg >}}/2018/09/14-browser-list-letters.png
[image-ss-04-page-winter]:   {{< assets-ssg >}}/2018/09/14-browser-nouse-winter.png
[image-ss-04-tree-content]:  {{< assets-ssg >}}/2018/09/14-tree-content.png

[image-ss-05-page-cats]:     {{< assets-ssg >}}/2018/09/15-browser-categories.png
[image-ss-05-page-item]:     {{< assets-ssg >}}/2018/09/15-browser-categories-love.png
[image-ss-05-page-tags]:     {{< assets-ssg >}}/2018/09/15-browser-tags.png

[image-ss-06-page-custom]:   {{< assets-ssg >}}/2018/09/16-browser-custom-page.png
[image-ss-06-section-tree]:  {{< assets-ssg >}}/2018/09/16-section-tree.png
[image-ss-06-file-naming]:   {{< assets-ssg >}}/2018/09/16-file-naming.png

[tutor-hugo-tutor-01]:       {{< tutor-hugo-bootstrap >}}/themes/tutor-01/

[tutor-hugo-layouts-baseof]: {{< tutor-hugo-bootstrap >}}/themes/tutor-01/layouts/_default/baseof.html
[tutor-hugo-layouts-single]: {{< tutor-hugo-bootstrap >}}/themes/tutor-01/layouts/_default/single.html
[tutor-hugo-layouts-list]:   {{< tutor-hugo-bootstrap >}}/themes/tutor-01/layouts/_default/list.html

[tutor-hugo-layouts-index]:  {{< tutor-hugo-bootstrap >}}/themes/tutor-01/layouts/index.html
[tutor-hugo-layouts-head]:   {{< tutor-hugo-bootstrap >}}/themes/tutor-01/layouts/partials/head.html
[tutor-hugo-layouts-header]: {{< tutor-hugo-bootstrap >}}/themes/tutor-01/layouts/partials/header.html
[tutor-hugo-layouts-footer]: {{< tutor-hugo-bootstrap >}}/themes/tutor-01/layouts/partials/footer.html

[tutor-hugo-content-duty]:   {{< tutor-hugo-bootstrap >}}/content/duty.md
[tutor-hugo-content-hello]:  {{< tutor-hugo-bootstrap >}}/content/letters/hello.html
[tutor-hugo-content-winter]: {{< tutor-hugo-bootstrap >}}/content/letters/winter.md
[tutor-hugo-content-every]:  {{< tutor-hugo-bootstrap >}}/content/posts/everyday.md

[tutor-hugo-layouts-post-single]:   {{< tutor-hugo-bootstrap >}}/themes/tutor-01/layouts/post/single.html
