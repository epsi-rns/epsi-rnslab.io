---
type   : post
title  : "Hugo - Bootstrap - Layout"
date   : 2018-09-05T09:17:35+07:00
slug   : hugo-bootstrap-layout
categories: [ssg, frontend]
tags      : [hugo, bootstrap, css]
keywords  : [static site, custom theme, layout]
author : epsi
opengraph:
  image: assets/site/images/topics/hugo-bootstrap-markdown.png

toc    : "toc-2018-09-hugo-bootstrap-step"

excerpt:
  Building Hugo Step by step, with Bootstrap as stylesheet frontend.
  Explain How to Use Bootstrap in Hugo.

---

### Preface

This article is intended for beginner.

> Goal: Explain How to Use Bootstrap in Hugo

-- -- --

### 1: Preparation

Consider make an ecosystem for bootstrap first.

#### Create Theme

To avoid messed-up with previous tutorial,
consider make a new theme.

{{< highlight bash >}}
$ hugo new theme tutor-02
Creating theme at /media/Works/sites/tutor-hugo/themes/tutor-02
{{< / highlight >}}

{{< highlight bash >}}
$ cat config.toml
theme        = "tutor-02"
{{< / highlight >}}

{{< highlight bash >}}
$ cd themes/tutor-02 
{{< / highlight >}}

#### Copy CSS and JS

Copy bootstrap dist to <code>themes/tutor-02/static</code>.

{{< highlight bash >}}
$ tree
static
├── css
├── dist
│  ├── css
│  │  ├── bootstrap-grid.css
│  │  ├── bootstrap-grid.min.css
│  │  ├── bootstrap-reboot.css
│  │  ├── bootstrap-reboot.min.css
│  │  ├── bootstrap.css
│  │  └── bootstrap.min.css
│  └── js
│     ├── bootstrap.bundle.js
│     ├── bootstrap.bundle.min.js
│     ├── bootstrap.js
│     └── bootstrap.min.js
└── js
{{< / highlight >}}

![Hugo Bootstrap: Tree][image-ss-01-tree-bootstrap]

Note that, you might still need map files,
while debugging using object inspector.

**Source**:
	[gitlab.com/.../themes/tutor-02/static][tutor-hugo-static]

-- -- --

### 2: Add Bootstrap

Adding bootstrap is straightforward.
Just add the stylesheet to <code>site-head.html</code>.

However, there are these artefacts,
to make sure, we have the same copy.

#### Partial: HTML Head

* <code>themes/tutor-02/layouts/partials/site-head.html</code>
  : [gitlab.com/.../layouts/partials/site-head.html][tutor-hugo-layouts-head]

{{< highlight twig >}}
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>{{ .Page.Title | default .Site.Title }}</title>
  
    <link rel="stylesheet" type="text/css" href="{{ "dist/css/bootstrap.css" | relURL }}">
</head>
{{< / highlight >}}

#### Layout: Homepage

No change, very similar to previous tutorial.
You can just copy from <code>tutor-01</code> to <code>tutor-02</code>.

* <code>themes/tutor-02/layouts/index.html</code>
  : [gitlab.com/.../layouts/index.html][tutor-hugo-layouts-index]

{{< highlight twig >}}
{{ define "main" }}
{{ .Content }}
{{ end }}
{{< / highlight >}}

#### Default: Baseof Template

I'm trying to have this file, as simple as possible.

* <code>layouts/_default/baseof.html</code>

{{< highlight twig >}}
<!DOCTYPE html>
<html lang="en">
{{ partial "site-head.html" . }}
<body>
  {{ .Content }}
</body>
</html>
{{< / highlight >}}

#### Server Output: Browser

Open in your favorite browser.
You should see, white background, by now.

* <http://localhost:1313/>

![Hugo Bootstrap: White Minimal][image-ss-02-browser-minimal]

-- -- --

### 3: Layout

I put colors so you can get the idea,
about this responsive layout clearly.
I also put grid, just as an example.

> Consider refactor by layout

#### Default: Baseof Template

Consider using semantic HTML5 tag.

* <code>themes/tutor-02/layouts/_default/baseof.html</code>

{{< highlight html >}}
<!DOCTYPE html>
<html lang="en">
{{ partial "site-head.html" . }}
<body>
  <header>
    <nav class="bg-dark text-light">
      <a href="{{ .Site.BaseURL }}" class="text-light">Home</a>
    </nav>
  </header>

  <div class="container-fluid">

    <div class="row layout-base">
      <main role="main" class="container-fluid col-md-8 bg-primary">
        <section>
          <h4>{{ .Title | default .Site.Title }}</h4>
        </section>

        <article class="bg-light">
          {{ .Content }}
        </article>
      </main>
      
      <aside class="col-md-4 bg-info">
        Side menu
      </aside>

    </div><!-- .row -->

  </div><!-- .container-fluid -->

  <footer class="container-fluid bg-dark text-light text-center">
    &copy; {{ now.Year }}.
  </footer>

</body>
</html>
{{< / highlight >}}

#### Server Output: Browser

Open in your favorite browser.
You should see, layout in color, by now.

* <http://localhost:1313/>

![Hugo Bootstrap: Colored Layout][image-ss-03-browser-layout]

-- -- --

### 4: Refactor

After minimalist bootstrap above,
the next step is to create the layout.
We are going to refactor

* <code>layouts/_default/baseof.html</code>

Into:

* <code>layouts/_default/baseof.html</code>

* <code>layouts/_default/single.html</code>

* <code>layouts/index.html</code>

* <code>layouts/partials/site-header.html</code>

* <code>layouts/partials/site-footer.html</code>

#### Default: Baseof Template

* <code>themes/tutor-02/layouts/_default/baseof.html</code>
  : [gitlab.com/.../layouts/_default/baseof.html][tutor-hugo-layouts-baseof]

{{< highlight twig >}}
<!DOCTYPE html>
<html lang="en">
{{ partial "site-head.html" . }}
<body>
{{ partial "site-header.html" . }}

  <div class="container-fluid">

    <div class="row layout-base">
      {{- block "main" . }}
      {{ .Content }}
      {{- end }}

      {{- block "aside" . }}{{- end }}
    </div><!-- .row -->

  </div><!-- .container-fluid -->

{{ partial "site-footer.html" . }}
{{ partial "site-scripts.html" . }}
</body>
</html>
{{< / highlight >}}

I put the header/footer, outside main block.
So we do not need to repeatly writing header/footer in every main block.

#### Default: Single

Both these two artefacts.

* <code>themes/tutor-02/layouts/index.html</code> (homepage)

* <code>themes/tutor-02/layouts/_default/single.html</code>
  : [gitlab.com/.../layouts/_default/single.html][tutor-hugo-layouts-single]

{{< highlight html >}}
$ cat layouts/_default/single.html

{{ define "main" }}
  <main role="main" class="container-fluid bg-light">
    <section>
      <h4>{{ .Title | default .Site.Title }}</h4>
    </section>

    <article>
      {{ .Content }}
    </article>
  </main>
{{ end }}
{{< / highlight >}}

#### Partial: Header

* <code>themes/tutor-02/layouts/partials/site-header.html</code>

{{< highlight html >}}
<header>
  <nav class="bg-dark text-light">
    <a href="{{ .Site.BaseURL }}" class="text-light">Home</a>
  </nav>
</header>
{{< / highlight >}}

#### Partial: Footer

* <code>themes/tutor-02/layouts/partials/site-footer.html</code>

{{< highlight html >}}
<footer class="container-fluid bg-dark text-light text-center">
  &copy; {{ now.Year }}.
</footer>
{{< / highlight >}}

#### Partial: Javascript

It is just an empty template.

* <code>themes/tutor-02/layouts/partials/site-scripts.html</code>
  : [gitlab.com/.../layouts/partials/site-scripts.html][tutor-hugo-layouts-scripts]

{{< highlight html >}}
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
{{< / highlight >}}

#### Server Output: Browser

Open in your favorite browser.
You should see, non-colored homepage, by now.

* <http://localhost:1313/>

![Hugo Bootstrap: Layout Homepage][image-ss-03-browser-homepage]

#### Default: List

We should also change the list

* <code>themes/tutor-02/layouts/_default/list.html</code>
  : [gitlab.com/.../layouts/_default/list.html][tutor-hugo-layouts-list]

{{< highlight twig >}}
{{ define "main" }}
<main role="main" class="container-fluid bg-light">
  <section>
    <h4>{{ .Section }}</h4>
  </section>

  {{ .Content }}
  
  <ul>
    {{ range .Data.Pages }}
    <li>
      <a href="{{ .URL }}">{{ .Title }}</a>
    </li>
    {{ end }}
  </ul>
</main>
{{ end }}
{{< / highlight >}}

#### Server Output: Browser

Open in your favorite browser.
You should see, simple index with article list, by now.

* <http://localhost:1313/letters/>

![Hugo Bootstrap: Layout List][image-ss-03-browser-list]

-- -- --

### Summary

So far here are artefacts for our layouts.

{{< highlight conf >}}
$ tree
layouts
├── _default
│  ├── baseof.html
│  ├── list.html
│  └── single.html
├── index.html
└── partials
   ├── site-footer.html
   ├── site-head.html
   ├── site-header.html
   └── site-scripts.html
{{< / highlight >}}

![Hugo Bootstrap: Artefacts Summary in Tree][image-ss-03-tree-layouts]

-- -- --

### What is Next ?

There are, some interesting topic for using <code>Bootstrap in Hugo</code>,
such as <code>Navigation, Header, and Footer</code>.
And also exotic page <code>Blog</code> and <code>Landing Page</code>. 
Consider continue reading [ [Hugo Bootstrap - Components][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2018/09/06/hugo-bootstrap-components/

[image-ss-01-tree-bootstrap]:   {{< assets-ssg >}}/2018/09/21-tree-bootstrap-asset.png
[image-ss-02-browser-minimal]:  {{< assets-ssg >}}/2018/09/21-browser-minimal.png
[image-ss-03-browser-layout]:   {{< assets-ssg >}}/2018/09/22-browser-layout.png
[image-ss-03-browser-homepage]: {{< assets-ssg >}}/2018/09/22-browser-homepage.png
[image-ss-03-browser-list]:     {{< assets-ssg >}}/2018/09/22-browser-list.png
[image-ss-03-tree-layouts]:     {{< assets-ssg >}}/2018/09/23-tree-layouts.png

[tutor-hugo-layouts-index]:     {{< tutor-hugo-bootstrap >}}/themes/tutor-01/layouts/index.html

[tutor-hugo-static]:            {{< tutor-hugo-bootstrap >}}/themes/tutor-02/static/
[tutor-hugo-layouts-baseof]:    {{< tutor-hugo-bootstrap >}}/themes/tutor-02/layouts/_default/baseof.html
[tutor-hugo-layouts-single]:    {{< tutor-hugo-bootstrap >}}/themes/tutor-02/layouts/_default/single.html
[tutor-hugo-layouts-list]:      {{< tutor-hugo-bootstrap >}}/themes/tutor-02/layouts/_default/list.html

[tutor-hugo-layouts-head]:      {{< tutor-hugo-bootstrap >}}/themes/tutor-02/layouts/partials/site-head.html
[tutor-hugo-layouts-header]:    {{< tutor-hugo-bootstrap >}}/themes/tutor-02/layouts/partials/site-header.html
[tutor-hugo-layouts-footer]:    {{< tutor-hugo-bootstrap >}}/themes/tutor-02/layouts/partials/site-footer.html
[tutor-hugo-layouts-scripts]:   {{< tutor-hugo-bootstrap >}}/themes/tutor-02/layouts/partials/site-scripts.html
