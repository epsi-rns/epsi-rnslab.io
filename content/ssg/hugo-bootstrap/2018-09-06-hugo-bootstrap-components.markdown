---
type   : post
title  : "Hugo - Bootstrap - Components"
date   : 2018-09-06T09:17:35+07:00
slug   : hugo-bootstrap-components
categories: [ssg, frontend]
tags      : [hugo, bootstrap, css]
keywords  : [static site, custom theme, navigation, header, footer, posts, landing page]
author : epsi
opengraph:
  image: assets/site/images/topics/hugo-bootstrap-markdown.png

excerpt:
  Building Hugo Step by step, with Bootstrap as stylesheet frontend.
  Use Bootstrap Features in Hugo Layout.

---


### Preface

This article is intended for beginner.

> Goal: Use Bootstrap Features in Hugo Layout

All these examples below based on official bootstrap examples:

* <http://getbootstrap.com/docs/4.1/examples>

-- -- --

### 1: Navigation, Header, and Footer

Based on:

* <http://getbootstrap.com/docs/4.1/examples/sticky-footer-navbar/>

#### Server Output: Browser

This section is long,
before you get lost, 
this is what we want to achieve.

> A homepage, with fixed top taskbar.

![Hugo Bootstrap: Homepage with Navigation Bar][image-ss-04-browser-homepage]

#### Assets: Stylesheet

Add necessary stylesheet.
You should find the stylesheet on official site.
This guidance is using Bootstrap v4.1.

* <code>themes/tutor-02/layouts/partials/site-head.html</code>
  : [gitlab.com/.../layouts/partials/head.html][tutor-hugo-layouts-head]

{{< highlight twig >}}
    <link rel="stylesheet" type="text/css" href="{{ "css/sticky-footer-navbar.css" | relURL }}">
    <link rel="stylesheet" type="text/css" href="{{ "css/bootstrap-custom.css" | relURL }}">
{{< / highlight >}}

#### Assets: Javascript

Bootstrap menu require JQuery.
You can also find in Bootstrap official repository.

* <code>themes/tutor-02/layouts/partials/site-scripts.html</code>
  : [gitlab.com/.../layouts/partials/scripts.html][tutor-hugo-layouts-scripts]

{{< highlight twig >}}
    <script src="{{ "js/jquery-slim.min.js" | relURL }}"></script>
    <script src="{{ "dist/js/bootstrap.min.js" | relURL }}"></script>
{{< / highlight >}}

#### Assets: Logo

You are free to use your custom logo.
I actually made some logo,
and here we can use it as an example for this guidance.

* <code>themes/tutor-02/static/images/logo-gear.png</code>
  : [gitlab.com/.../static/images/logo-gear.png][tutor-hugo-images-logo-gear]

![Hugo Bootstrap: Custom Logo][image-ss-04-logo-gear]

* Size [width x height]: 96px * 96px

#### Assets: Custom Stylesheet

* <code>themes/tutor-02/static/css/bootstrap-custom.css</code>
  : [gitlab.com/.../static/css/bootstrap-custom.css][tutor-hugo-css-bootstrap-custom]

{{< highlight css >}}
.layout-base {
  padding-top: 70px;
}

.navbar-brand img {
  width: 48px;
  height: 48px;
  margin-top:    -10px;
  margin-bottom: -10px;
}
{{< / highlight >}}

#### Partial: HTML Head

* <code>themes/tutor-02/layouts/partials/site-header.html</code>
  : [gitlab.com/.../layouts/partials/header.html][tutor-hugo-layouts-header]

{{< highlight html >}}
<header>
  <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="{{ .Site.BaseURL }}">
       <img src="{{ "images/logo-gear.png" | absURL }}"
           alt="Logo" />
    </a>
    <button class="navbar-toggler" type="button" 
        data-toggle="collapse" data-target="#navbarCollapse" 
        aria-controls="navbarCollapse" aria-expanded="false" 
        aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="{{ .Site.BaseURL }}"
             >Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Archives</a>
        </li>
      </ul>
      <form class="form-inline mt-2 mt-md-0">
        <input class="form-control mr-sm-2" type="text" 
          placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-light my-2 my-sm-0" 
          type="submit">Search</button>
      </form>
    </div>
  </nav>
</header>
{{< / highlight >}}

#### Partial: Footer

* <code>themes/tutor-02/layouts/partials/site-footer.html</code>
  : [gitlab.com/.../layouts/partials/footer.html][tutor-hugo-layouts-footer]

{{< highlight html >}}
<footer class="bg-dark text-light">
  <div class="container text-center">
    <span class="text-muted">&copy; {{ now.Year }}.</span>
  </div>
</footer>
{{< / highlight >}}

#### Server Output: Browser

Open in your favorite browser.
You should see, a section list, with fixed top taskbar.

* <http://localhost:1313/letters/>

![Hugo Bootstrap: Index List with Navigation Bar][image-ss-04-browser-list]

-- -- --

### 2: Posts

Consider remake our simple <code>single.html</code>,
adding a more useful information, semantic, and bootstrap's classes.

#### Post: Single

* <code>themes/tutor-02/layouts/post/single.html</code>
  : [gitlab.com/.../layouts/post/single.html][tutor-hugo-post-single]

{{< highlight html >}}
{{ define "main" }}
  <main role="main" class="container-fluid bg-light">
    <div class="blog-main">
      <div class="blog-post">
        <section>
          <h2 class="blog-post-title"
              >{{ .Title | default .Site.Title }}</h2>
          <p  class="blog-post-meta"
              >{{ .Page.Date.Format "January 02, 2006" }} by <a href="#">epsi</a></p>
        </section>

        <article>
          {{ .Content }}
        </article>
      </div><!-- /.blog-post -->
    </div><!-- /.blog-main -->
  </main>
{{ end }}
{{< / highlight >}}

Most tags are in official bootstrap documentation.

#### Assets: Stylesheet

We can also apply, custom bootstrap stylesheet, taken from examples:

* <code>themes/tutor-02/layouts/partials/site-head.html</code>
  : [gitlab.com/.../layouts/partials/head.html][tutor-hugo-layouts-head]

{{< highlight html >}}
    <link rel="stylesheet" type="text/css" href="{{ "css/sticky-footer-navbar.css" | relURL }}">
    <link rel="stylesheet" type="text/css" href="{{ "css/blog.css" | relURL }}">
    <link rel="stylesheet" type="text/css" href="{{ "css/bootstrap-custom.css" | relURL }}">
{{< / highlight >}}

#### Server Output: Browser

Open in your favorite browser.
You should see, an ordinary post.

* <http://localhost:1313/posts/>

![Hugo Bootstrap: Post without Layout][image-ss-05-browser-post]

#### Post: Single, Again

Why not go further with bootstrap grid ?

* <code>themes/tutor-02/layouts/post/single.html</code>
  : [gitlab.com/.../layouts/post/single.html][tutor-hugo-post-single]

{{< highlight html >}}
{{ define "main" }}
  <main role="main" class="col-md-8">
    <div class="blog-main p-3 mb-3 bg-light rounded">
      <div class="blog-post">
        <section>
          <h2 class="blog-post-title"
              >{{ .Title | default .Site.Title }}</h2>
          <p  class="blog-post-meta"
              >{{ .Page.Date.Format "January 02, 2006" }} by <a href="#">epsi</a></p>
        </section>

        <article>
          {{ .Content }}
        </article>
      </div><!-- /.blog-post -->
    </div><!-- /.blog-main -->
  </main>
{{ end }}

{{ define "aside" }}
  <aside class="col-md-4 blog-sidebar">
    <div class="p-3 mb-3 bg-light rounded">
      <h4 class="font-italic">About You</h4>
      Are you an angel ?
    </div>
  </aside>
{{ end }}
{{< / highlight >}}

#### Server Output: Browser

Open in your favorite browser.
You should see, a post, with sidebar.

* <http://localhost:1313/posts/>

![Hugo Bootstrap: Post with sidebar Layout][image-ss-05-browser-post-grid]

Note that, this is already using responsive grid.
It has different looks on different screen.
You should try to resize your browser, to get the idea.

-- -- --

### 3: Landing Page

This guidance is not the place to dump all my blog template.
So this is the last example on this article.

#### Layout: Homepage

We need more aesthetic accent for our landing page.

* <code>themes/tutor-02/layouts/index.html</code>
  : [gitlab.com/.../layouts/index.html][tutor-hugo-layouts-index]

{{< highlight html >}}
{{ define "main" }}
  <main role="main" class="container-fluid">
      <div class="jumbotron">
        <div class="container bg-light text-dark">
    <section>
      <h2 class="cover-heading">{{ .Title | default .Site.Title }}</h2>
    </section>

    <article>
      {{ .Content }}
    </article>
        </div>
      </div>
  </main>
{{ end }}
{{< / highlight >}}

#### Content: Homepage

Just add <code>lead</code> class, as cosmetic.

* <code>themes/tutor-02/content/_index.html</code>
  : [gitlab.com/.../content/_index.html][tutor-hugo-content-index]

{{< highlight html >}}
  <p class="lead">There are so many things to say.
  I don't want to live in regrets.
  So I wrote this for my love.</p>
{{< / highlight >}}

#### Assets: Custom Stylesheet

* <code>themes/tutor-02/static/css/bootstrap-custom.css</code>
  : [gitlab.com/.../static/css/bootstrap-custom.css][tutor-hugo-css-bootstrap-custom]

{{< highlight css >}}
.cover-heading,
.blog-post-title {
  text-shadow: 0 .05rem .1rem rgba(0, 0, 0, .5);
}
{{< / highlight >}}

#### Server Output: Browser

Open in your favorite browser.
You should see, homepage, with jumbotron, and nice font.

* <http://localhost:1313/>

![Hugo Bootstrap: Nice Landing Page][image-ss-05-browser-homepage]

-- -- --

### What is Next ?

Before we continue, consider have a break, and look at this cute kitten.
Relax for a while, make another cup of coffe, and resume our tutorial.

![adorable kitten][image-kitten]

There are, some interesting topic about <code>Using SASS in Bootstrap in Hugo</code>. 
Consider continue reading [ [Hugo Bootstrap - SASS][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2018/09/07/hugo-bootstrap-sass/
[image-kitten]:             {{< baseurl >}}/assets/site/images/cats/kitten.jpg

[image-ss-04-logo-gear]:         {{< assets-ssg >}}/2018/09/logo-gear.png
[image-ss-04-browser-homepage]:  {{< assets-ssg >}}/2018/09/24-browser-homepage.png
[image-ss-04-browser-list]:      {{< assets-ssg >}}/2018/09/24-browser-list.png
[image-ss-05-browser-post]:      {{< assets-ssg >}}/2018/09/25-browser-post.png
[image-ss-05-browser-post-grid]: {{< assets-ssg >}}/2018/09/25-browser-post-grid.png
[image-ss-05-browser-homepage]:  {{< assets-ssg >}}/2018/09/25-browser-homepage.png

[tutor-hugo-content-index]:      {{< tutor-hugo-bootstrap >}}/content/_index.html

[tutor-hugo-layouts-index]:      {{< tutor-hugo-bootstrap >}}/themes/tutor-02/layouts/index.html

[tutor-hugo-layouts-head]:       {{< tutor-hugo-bootstrap >}}/themes/tutor-02/layouts/partials/site-head.html
[tutor-hugo-layouts-header]:     {{< tutor-hugo-bootstrap >}}/themes/tutor-02/layouts/partials/site-header.html
[tutor-hugo-layouts-footer]:     {{< tutor-hugo-bootstrap >}}/themes/tutor-02/layouts/partials/site-footer.html
[tutor-hugo-layouts-scripts]:    {{< tutor-hugo-bootstrap >}}/themes/tutor-02/layouts/partials/site-scripts.html

[tutor-hugo-images-logo-gear]:   {{< tutor-hugo-bootstrap >}}/themes/tutor-02/static/images/logo-gear.png
[tutor-hugo-css-bootstrap-custom]: {{< tutor-hugo-bootstrap >}}/themes/tutor-02/static/css/bootstrap-custom.css

[tutor-hugo-layouts-single]:     {{< tutor-hugo-bootstrap >}}/themes/tutor-02/layouts/_default/single.html
[tutor-hugo-post-single]:        {{< tutor-hugo-bootstrap >}}/themes/tutor-02/layouts/post/single.html
