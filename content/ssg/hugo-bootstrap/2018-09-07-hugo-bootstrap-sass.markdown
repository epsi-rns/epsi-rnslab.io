---
type   : post
title  : "Hugo - Bootstrap - SASS"
date   : 2018-09-07T09:17:35+07:00
slug   : hugo-bootstrap-sass
categories: [ssg, frontend]
tags      : [hugo, bootstrap, sass]
keywords  : [static site, custom theme, custom color variable]
author : epsi
opengraph:
  image: assets/site/images/topics/hugo-bootstrap-markdown.png

toc    : "toc-2018-09-hugo-bootstrap-step"

excerpt:
  Building Hugo Step by step, with Bootstrap as stylesheet frontend.
  Changing Bootstrap Color with Sass in Hugo theme.

---


### Preface

This article is intended for beginner.

> Goal: Changing Bootstrap Color with Sass in Hugo theme

All these examples below based on official bootstrap examples:

* <https://sass-lang.com/>

-- -- --

### 1: Prepare

This preparation is required.

#### Theme

With your file manager, copy manually 

1. From <code>themes/tutor-02/*</code> to <code>themes/tutor-03</code>.

Remove completely:

2. <code>themes/tutor-03/static/dist/css/</code>

Create directory
3. <code>themes/tutor-03/sass</code>

You should see the sass directory,
in this new <code>tutor-03</code> theme:

4. Edit <code>config.toml</code>, and save.

{{< highlight toml >}}
$ cat config.toml
theme        = "tutor-03"
{{< / highlight >}}

![Hugo Bootstrap SASS: Tree theme][image-ss-01-tree-themes]

5. Have a look at the example in repository.

**Source**:
	[gitlab.com/.../themes/tutor-03][tutor-hugo-tutor-03]

#### Boostrap Source

You can download your bootstrap using

{{< highlight bash >}}
$ wget https://github.com/twbs/bootstrap/archive/v4.1.3.zip
{{< / highlight >}}

With your file manager, using newly downloaded bootstrap directory,
copy manually:

* From <code>bootstrap-4.1.3/scss/*</code> 
  to <code>themes/tutor-03/sass/bootstrap</code>.

Change working directory

{{< highlight bash >}}
$ cd /themes/tutor-03
{{< / highlight >}}

Your <code>sass</code> directory should be looks like this.

{{< highlight conf >}}
$ tree -d sass
sass
└── bootstrap
    ├── mixins
    └── utilities

3 directories
{{< / highlight >}}

![Hugo Bootstrap SASS: Tree sass][image-ss-01-tree-sass]

**Source**:
	[gitlab.com/.../sass/bootstrap][tutor-hugo-sass-bootstrap]

-- -- --

### 2: Bootstrap Sass

#### Install

{{< highlight bash >}}
$ gem install sass
...

Successfully installed sass-3.5.7
Parsing documentation for sass-3.5.7
Installing ri documentation for sass-3.5.7
Done installing documentation for sass after 13 seconds
1 gem installed
{{< / highlight >}}

![Hugo Bootstrap SASS: gem install sass][image-ss-02-gem-install]

#### Deprecated

Ruby-sass will be deprecated soon.

* <https://sass-lang.com/install>

As an alternative you can use dart-sass, node-sass.

* [Sass: Common Compiler][local-sass-compiler]

Or any task runner such as gulp or grunt.

* [Sass: Task Runner][local-sass-task-runner]

#### Using SASS

Using SASS in CLI is easy.
However, you need to read the manual page first.

{{< highlight bash >}}
$ sass --help
{{< / highlight >}}

You don't need to create this directory,
as sass will create necessary directory.

{{< highlight bash >}}
$ mkdir static/bootstrap/
{{< / highlight >}}

#### Generate CSS

Using <code>themes/tutor-03</code> as working directory,
generating css is as simply as this command below.
The command will map any necessary input scss in <code>sass</code> directory,
into output css in <code>static</code> directory.

{{< highlight bash >}}
$ sass --update sass:static
      write static/bootstrap/bootstrap-grid.css
      write static/bootstrap/bootstrap-grid.css.map
      write static/bootstrap/bootstrap-reboot.css
      write static/bootstrap/bootstrap-reboot.css.map
      write static/bootstrap/bootstrap.css
      write static/bootstrap/bootstrap.css.map
{{< / highlight >}}

![Hugo Bootstrap SASS: sass update][image-ss-02-sass-update]

**Result**:
	[gitlab.com/.../static/bootstrap][tutor-hugo-css-bootstrap]

#### Continuously

To avoid, typing command above over, and over again.
Consider to this command to continuously update as the input change.

{{< highlight bash >}}
$ sass --watch sass:static
>>> Sass is watching for changes. Press Ctrl-C to stop.
{{< / highlight >}}

![Hugo Bootstrap SASS: sass watch][image-ss-02-sass-watch]

#### Assets: Stylesheet

We need an adjustment,
as we use different bootstrap stylesheet.

* <code>themes/tutor-03/layouts/partials/site-head.html</code>
  : [gitlab.com/.../layouts/partials/site-head.html][tutor-hugo-layouts-head]

{{< highlight twig >}}
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>{{ .Page.Title | default .Site.Title }}</title>
  
    <link rel="stylesheet" type="text/css" href="{{ "bootstrap/bootstrap.css" | relURL }}">
</head>
{{< / highlight >}}

Now check the browser, to check if there is no error.

-- -- --

### 3: Custom SCSS

Now we are also going to move <code>static/css/</code>.

#### Create Custom CSS

Rename these three artefacts:

* <code>themes/tutor-03/static/css/blog.css</code>
  to <code>sass/css/_blog.scss</code>

* <code>themes/tutor-03/static/css/bootstrap-custom.css</code>
  to <code>sass/css/_bootstrap-custom.scss</code>

* <code>themes/tutor-03/static/css/sticky-footer-navbar.css</code>
  to <code>sass/css/_sticky-footer-navbar.scss</code>

**source**
: [gitlab.com/.../sass/css/][tutor-hugo-sass-css]

#### Watch The Changes

{{< highlight bash >}}
$ sass --watch sass:static
>>> Sass is watching for changes. Press Ctrl-C to stop.
>>> New template detected: sass/css/_blog.scss
>>> New template detected: sass/css/_bootstrap-custom.scss
>>> New template detected: sass/css/_sticky-footer-navbar.scss
{{< / highlight >}}

![Hugo Bootstrap SASS: rename css to scss][image-ss-03-sass-css]

**Result**:
	[gitlab.com/.../static/css][tutor-hugo-css-css]

#### Generate CSS

Create <code>main.scss</code>
: [gitlab.com/.../sass/main.scss][tutor-hugo-sass-main].

{{< highlight scss >}}
@import
    "sticky-footer-navbar",
    "blog",
    "bootstrap-custom"
;
{{< / highlight >}}

You should see the change in the terminal.

{{< highlight conf >}}
$ sass --watch sass:static
>>> Sass is watching for changes. Press Ctrl-C to stop.
>>> New template detected: sass/css/main.scss
      write static/css/main.css
      write static/css/main.css.map
>>> Change detected to: sass/css/main.scss
      write static/css/main.css
      write static/css/main.css.map
{{< / highlight >}}

![Hugo Bootstrap SASS: main.css][image-ss-03-sass-main]

#### Result

The content of <code>themes/tutor-03/static/css</code> should be as below:

{{< highlight conf >}}
$ tree static/css 
static/css
├── main.css
└── main.css.map
{{< / highlight >}}

![Hugo Bootstrap SASS: content of css directory][image-ss-03-tree-css]

**Result**:
	[gitlab.com/.../static/css/main.css][tutor-hugo-css-main]

It is, a looong stylesheet, automatically crafted by **sass**.

#### Assets: Stylesheet

Don't forget to add necessary stylesheet.

* <code>themes/tutor-03/layouts/partials/site-head.html</code>
  : [gitlab.com/.../layouts/partials/site-head.html][tutor-hugo-layouts-head]

{{< highlight twig >}}
<head>
    ...
  
    <link rel="stylesheet" type="text/css" href="{{ "boostrap/bootstrap.css" | relURL }}">
    <link rel="stylesheet" type="text/css" href="{{ "css/main.css" | relURL }}">
</head>
{{< / highlight >}}

#### Server Output: Browser

Check the browser again, to check if there is no error.

* <http://localhost:1313/>

![Hugo Bootstrap SASS: content of css directory][image-ss-03-browser-homepage]

-- -- --

### 4: Bootswatch (as an example)

You can change bootstrap predefined variable.
You can make your own variable override.


#### Resources

Luckily, you do not to reinvent the wheel,
there are already good color schemes,
in the internet.

* http://colormind.io/bootstrap/

* https://bootswatch.com/

* https://mdbootstrap.com/

* And some more sites

I took one example, the bootswatch.

#### Prepare

Clone

{{< highlight bash >}}
$ git clone https://github.com/thomaspark/bootswatch
{{< / highlight >}}

Copy from <code>bootswatch/dist/*</code>
to <code>themes/tutor-03/sass/bootswatch/</code>

You should see, many theme in that directory.

{{< highlight bash >}}
$ ls sass/bootswatch
cerulean  flatly   lux      sandstone  solar      yeti
cosmo     journal  materia  simplex    spacelab
cyborg    litera   minty    sketchy    superhero
darkly    lumen    pulse    slate      united
{{< / highlight >}}

Remove unnecessary files.

{{< highlight bash >}}
$ find themes/tutor-03/sass/bootswatch -name "bootstrap*" -exec rm {} \;
{{< / highlight >}}

This will remove any all compiled bootstrap.css.
Saving some space.

**source**
: [gitlab.com/.../sass/bootswatch/][tutor-hugo-sass-bootswatch]

#### Generate CSS

Create <code>bootswatch.scss</code>
: [gitlab.com/.../sass/bootswatch.scss][tutor-hugo-sass-bootswatch].

{{< highlight scss >}}
@import
    "bootswatch/cerulean/variables",
    "bootstrap/bootstrap",
    "bootswatch/cerulean/bootswatch"
;
{{< / highlight >}}

Again, you should see the change in the terminal.

{{< highlight conf >}}
$ sass --watch sass:static
>>> Sass is watching for changes. Press Ctrl-C to stop.
>>> New template detected: sass/css/bootswatch.scss
      write static/css/bootswatch.css
      write static/css/bootswatch.css.map
{{< / highlight >}}

![Hugo Bootstrap SASS: Generate Bootswatch CSS][image-ss-04-bootswatch-watch]

**Result**:
	[gitlab.com/.../static/css/main.css][tutor-hugo-css-main]

#### Assets: Stylesheet

Don't forget to make an adjustment,
as we use bootstrap stylesheet from bootswatch.

* <code>themes/tutor-03/layouts/partials/site-head.html</code>
  : [gitlab.com/.../layouts/partials/site-head.html][tutor-hugo-layouts-head]

{{< highlight twig >}}
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>{{ .Page.Title | default .Site.Title }}</title>
  
    <link rel="stylesheet" type="text/css" href="{{ "css/bootswatch.css" | relURL }}">
    <link rel="stylesheet" type="text/css" href="{{ "css/main.css" | relURL }}">
</head>
{{< / highlight >}}

#### Switch Theme

You may switch to other template.
There is already some free swatch such as: cerulean, cosmo, cyborg, 
darkly, flatly, journal, litera, lumen, lux, materia, minty, pulse, 
sandstone, simplex, sketchy, slate, solar, spacelab, superhero, 
united, yeti.

{{< highlight scss >}}
$ cat sass/css/bootswatch.scss

@import
    "bootswatch/flatly/variables",
    "bootstrap/bootstrap",
    "bootswatch/flatly/bootswatch"
;
{{< / highlight >}}

But sometimes you need some color adjustment, to match.
Here I remove the text-mute class from the span tag.

{{< highlight twig >}}
<footer class="footer bg-dark text-light">
  <div class="container text-center">
    <span>&copy; {{ now.Year }}.</span>
  </div>
</footer>
{{< / highlight >}}

#### Server Output: Browser

Open in your favorite browser.
You should see, a homepage, with different theme color.

* <http://localhost:1313/>

![Hugo Bootstrap SASS: Bootswatch Flatly Theme][image-ss-04-browser-flatly]

-- -- --

### 5: Custom Color Variables

If you want, you can create your own custom color.
Supposed you want a <code>pink</code> feminine navigation bar,
you can just adjust the color.

* <code>themes/tutor-03/sass/css/_variables.scss</code>
  : [gitlab.com/.../sass/_variables.scss][tutor-hugo-sass-variables].

{{< highlight scss >}}
// Variables

$pink:  #e83e8c !default;
$primary:  darken($pink, 10%) !default;
{{< / highlight >}}

Note that, <code>darken</code> is sass function.

#### Generate CSS

Do not forget to make the <code>bootstrap.scss</code>.

* <code>themes/tutor-03/sass/css/bootstrap.scss</code>
  : [gitlab.com/.../sass/bootstrap.scss][tutor-hugo-sass-custom].

{{< highlight scss >}}
@import
    "bootstrap/functions",
    "variables",
    "bootstrap/bootstrap"
;
{{< / highlight >}}

You should see the change in the terminal, as usual.

{{< highlight conf >}}
$ sass --watch sass:static
>>> Sass is watching for changes. Press Ctrl-C to stop.
>>> New template detected: sass/css/_variables.scss
>>> New template detected: sass/css/bootstrap.scss
      write static/css/bootstrap.css
      write static/css/bootstrap.css.map
{{< / highlight >}}

![Hugo Bootstrap SASS: Generate Custom Bootstrap CSS][image-ss-05-boostrap-watch]

#### Assets: Stylesheet

Also change the <code>site-head.html</code>.

* <code>themes/tutor-03/layouts/partials/site-head.html</code>
  : [gitlab.com/.../layouts/partials/site-head.html][tutor-hugo-layouts-head]

{{< highlight twig >}}
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>{{ .Page.Title | default .Site.Title }}</title>
  
    <link rel="stylesheet" type="text/css" href="{{ "css/bootstrap.css" | relURL }}">
    <link rel="stylesheet" type="text/css" href="{{ "css/main.css" | relURL }}">
</head>
{{< / highlight >}}

#### Partial: HTML Footer

And some adjustment,
from <code>bg-dark</code> to <code>bg-primary</code>,
so the <code>pink</code> can also affect the hyperlink.

* <code>themes/tutor-03/layouts/partials/site-footer.html</code>
  : [gitlab.com/.../layouts/partials/site-footer.html][tutor-hugo-layouts-footer]

{{< highlight html >}}
<footer class="footer bg-primary text-light">
  <div class="container text-center">
    <span>&copy; {{ now.Year }}.</span>
  </div>
</footer>
{{< / highlight >}}

#### Partial: HTML Header

I also simplified the <code>logo</code>, to match the pink color.

* <code>themes/tutor-03/layouts/partials/site-header.html</code>
  : [gitlab.com/.../layouts/partials/site-header.html][tutor-hugo-layouts-header]

{{< highlight html >}}
<header>
  <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-primary">
    <a class="navbar-brand" href="{{ .Site.BaseURL }}">
       <img src="{{ "images/logo-gear-pink.png" | absURL }}"
           alt="Logo" />
    </a>
...
{{< / highlight >}}

#### Server Output: Browser

Open in your favorite browser.
You should see, a homepage, with <code>pink</code> navigation bar.

* <http://localhost:1313/>

![Hugo Bootstrap SASS: Feminine Navigation Bar][image-ss-05-browser-pink]

-- -- --

### Summary

As a summary, here is the content of scss input:

{{< highlight conf >}}
$ tree sass/css
sass/css
├── _blog.scss
├── _bootstrap-custom.scss
├── bootstrap.scss
├── bootswatch.scss
├── main.scss
├── _sticky-footer-navbar.scss
└── _variables.scss
{{< / highlight >}}

![Hugo Bootstrap SASS: Tree Input Summary][image-ss-06-tree-input]

_._

And the content of css output would be:

{{< highlight conf >}}
$ tree static/css 
static/css
├── bootstrap.css
├── bootstrap.css.map
├── bootswatch.css
├── bootswatch.css.map
├── main.css
└── main.css.map
{{< / highlight >}}

![Hugo Bootstrap SASS: Tree Output Summary][image-ss-06-tree-output]

With map, you can debug in object inspector.

-- -- --

### What is Next ?

I do not go deep about sass, as this is not a bootstrap tutorial.

There are, some interesting topic about <code>Using SASS in Bootstrap in Hugo</code>. 
Consider continue reading [ [Hugo Bootstrap - SASS Customization][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2018/09/08/hugo-bootstrap-sass-custom/
[local-sass-compiler]:      {{< baseurl >}}frontend/2019/01/13/sass-common-compiler/
[local-sass-task-runner]:   {{< baseurl >}}frontend/2019/01/15/sass-task-runner/

[image-ss-01-tree-sass]:         {{< assets-ssg >}}/2018/09/31-tree-sass-bootstrap.png
[image-ss-01-tree-themes]:       {{< assets-ssg >}}/2018/09/31-tree-themes-03.png

[image-ss-02-gem-install]:       {{< assets-ssg >}}/2018/09/32-gem-install-sass.png
[image-ss-02-sass-update]:       {{< assets-ssg >}}/2018/09/32-sass-bootstrap-update.png
[image-ss-02-sass-watch]:        {{< assets-ssg >}}/2018/09/32-sass-bootstrap-watch.png

[image-ss-03-sass-css]:          {{< assets-ssg >}}/2018/09/33-sass-css-watch.png
[image-ss-03-sass-main]:         {{< assets-ssg >}}/2018/09/33-sass-main-watch.png
[image-ss-03-tree-css]:          {{< assets-ssg >}}/2018/09/33-tree-static-css.png
[image-ss-03-browser-homepage]:  {{< assets-ssg >}}/2018/09/25-browser-homepage.png

[image-ss-04-browser-flatly]:    {{< assets-ssg >}}/2018/09/34-browser-bootswatch-flatly.png
[image-ss-04-bootswatch-watch]:  {{< assets-ssg >}}/2018/09/34-sass-bootswatch-watch.png

[image-ss-05-browser-pink]:      {{< assets-ssg >}}/2018/09/35-browser-bootswatch-pink.png
[image-ss-05-boostrap-watch]:    {{< assets-ssg >}}/2018/09/35-sass-custom-bootstrap-watch.png

[image-ss-06-tree-input]:        {{< assets-ssg >}}/2018/09/36-tree-input-summary.png
[image-ss-06-tree-output]:       {{< assets-ssg >}}/2018/09/36-tree-output-summary.png

[tutor-hugo-tutor-03]:           {{< tutor-hugo-bootstrap >}}/themes/tutor-03/
[tutor-hugo-sass-bootstrap]:     {{< tutor-hugo-bootstrap >}}/themes/tutor-03/sass/bootstrap/
[tutor-hugo-sass-bootswatch]:    {{< tutor-hugo-bootstrap >}}/themes/tutor-03/sass/bootswatch/
[tutor-hugo-sass-css]:           {{< tutor-hugo-bootstrap >}}/themes/tutor-03/sass/css/
[tutor-hugo-sass-main]:          {{< tutor-hugo-bootstrap >}}/themes/tutor-03/sass/css/main.scss
[tutor-hugo-sass-bootswatch]:    {{< tutor-hugo-bootstrap >}}/themes/tutor-03/sass/css/bootswatch.scss
[tutor-hugo-css-bootstrap]:      {{< tutor-hugo-bootstrap >}}/themes/tutor-03/static/bootstrap/
[tutor-hugo-css-css]:            {{< tutor-hugo-bootstrap >}}/themes/tutor-03/static/css/
[tutor-hugo-css-main]:           {{< tutor-hugo-bootstrap >}}/themes/tutor-03/static/css/main.css
[tutor-hugo-css-bootswatch]:     {{< tutor-hugo-bootstrap >}}/themes/tutor-03/static/css/bootswatch.css
[tutor-hugo-sass-variables]:     {{< tutor-hugo-bootstrap >}}/themes/tutor-03/sass/css/_variables.scss
[tutor-hugo-sass-custom]:        {{< tutor-hugo-bootstrap >}}/themes/tutor-03/sass/css/bootstrap.scss

[tutor-hugo-layouts-head]:       {{< tutor-hugo-bootstrap >}}/themes/tutor-03/layouts/partials/site-head.html
[tutor-hugo-layouts-header]:     {{< tutor-hugo-bootstrap >}}/themes/tutor-03/layouts/partials/site-header.html
[tutor-hugo-layouts-footer]:     {{< tutor-hugo-bootstrap >}}/themes/tutor-03/layouts/partials/site-footer.html
