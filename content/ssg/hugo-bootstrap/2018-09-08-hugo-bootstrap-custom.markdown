---
type   : post
title  : "Hugo - Bootstrap - SASS Customization"
date   : 2018-09-08T09:17:35+07:00
slug   : hugo-bootstrap-sass-custom
categories: [ssg, frontend]
tags      : [hugo, bootstrap, sass]
keywords  : [static site, custom theme, maximum width, border, rounded, shadow]
author : epsi
opengraph:
  image: assets/site/images/topics/hugo-bootstrap-markdown.png

toc    : "toc-2018-09-hugo-bootstrap-step"

excerpt:
  Building Hugo Step by step, with Bootstrap as stylesheet frontend.
  Add common bootstrap sass customization

---

### Preface

This article is intended for beginner.

> Goal: Add common bootstrap sass customization.

Before going futher with Hugo,
we need to clean up our sass,
because we next chapter require a nice looks.

This chapter is more like a Bootstrap guidance rather than Hugo.
In fact this chapter doesn't require Hugo at all.
This chapter is completely out of topic.

-- -- --

### 1: Prepare

This whole chapter is nice to have.

#### Theme

As usual, with your file manager, copy manually 

1. From <code>themes/tutor-03/*</code> to <code>themes/tutor-04</code>
   : [gitlab.com/.../themes/tutor-04][tutor-hugo-tutor-04].

2. Edit <code>config.toml</code>, and save.

{{< highlight toml >}}
$ cat config.toml
theme        = "tutor-04"
{{< / highlight >}}

#### Watch Both

As usual, run <code>Hugo</code>.

{{< highlight bash >}}
$ hugo server
{{< / highlight >}}

And do not forget, using other terminal,
run the good <code>SASS</code> in parallel:

{{< highlight bash >}}
$ cd themes/tutor-04
$ sass --watch sass:static
{{< / highlight >}}

#### Stylesheet

For starter, I put a nice background, using subtle pattern.

* <code>themes/tutor-04/sass/css/_decoration.scss</code>
  : [gitlab.com/.../sass/_decoration.scss][tutor-hugo-sass-decoration].

{{< highlight scss >}}
// -- -- -- -- --
// _decoration.scss

body {
    background-image: url("../images/funky-lines.png");
}
{{< / highlight >}}

And empty stylesheet file

* <code>themes/tutor-04/sass/css/_layout.scss</code>
  : [gitlab.com/.../sass/_layout.scss][tutor-hugo-sass-layout].

{{< highlight scss >}}
// -- -- -- -- --
// _layout.scss
{{< / highlight >}}

And finally restucture all stylesheets in <code>main.scss</code>.

* <code>themes/tutor-04/sass/css/main.scss</code>
  : [gitlab.com/.../sass/main.scss][tutor-hugo-sass-main].

{{< highlight scss >}}
$ cat sass/css/main.scss

@import
  // taken from bootstrap
    "sticky-footer-navbar",
    "blog",
    "bootstrap-custom",

  // variables
    "bootstrap/functions",
    "variables",
    "bootstrap/variables",

  // custom
    "layout",
    "decoration"
;
{{< / highlight >}}

I also change the pin color to blue. Note that, actually,
you do not need to use the <code>!default</code> directive at all.

* <code>themes/tutor-04/sass/css/_variables.scss</code>
  : [gitlab.com/.../sass/_variables.scss][tutor-hugo-sass-variables].

{{< highlight scss >}}
// Variables

$blue:  #007bff;
$dark:  darken($blue, 35%);
{{< / highlight >}}

![Hugo Bootstrap: Subtle Pattern Background][image-ss-01-maxwidth-small]

-- -- --

### 2: Maxwidth

I have a smartphone, tablet, sometimes medium screen,
and mostly I'm working with large screen.
What is good for my regular screen, looks ugly in large screen.
My solution is to create maxwidth,
so my content would not be stretched horizontally.
Unfortunately, bootstrap maxwidth, does not suit my needs.

There are at least two options:

* first: using bootstrap,

* second: custom css class

#### Using Bootstrap

Using bootstrap is simply 
add these classes <code>offset-lg-2 col-lg-8</code>.
or even for medium screen <code>offset-md-2 col-md-8</code>.
And put these classes as below:

* <code>themes/tutor-04/layouts/_default/baseof.html</code>
  : [gitlab.com/.../layouts/_default/baseof.html][tutor-hugo-layouts-baseof]

{{< highlight scss >}}
<!DOCTYPE html>
...
  <div class="container-fluid offset-lg-2 col-lg-8">
...
{{< / highlight >}}

This will move the column to right.
For illustration below, I put border, so you can see clearly,
how the responsive goes in medium screen.

![Hugo Bootstrap: Maxwidth Using Bootstrap][image-ss-02-maxwidth-medium]

Notice that you do not need to balance by writing as this below:

{{< highlight scss >}}
  <div class="container-fluid offset-lg-2 col-lg-8 offset-lg-2">
{{< / highlight >}}

#### Custom CSS Maxwidth Class

The other way is using custom stylesheet.
Instead of relying on bootstrap,
I'd rather lock the design with fixed width in pixel.
 
The <code>maxwidth</code> class is as simply as below.
You can use whatever pixel width that suit you most.

* <code>themes/tutor-04/sass/css/_layout.scss</code>
  : [gitlab.com/.../sass/_layout.scss][tutor-hugo-sass-layout].

{{< highlight scss >}}
// -- -- -- -- --
// _layout.scss

.maxwidth {
   margin-right: auto;
   margin-left: auto;
   max-width: 1366px;
}
{{< / highlight >}}

Add this <code>maxwidth</code> class.
And put it as below:

* <code>layouts/_default/baseof.html</code>
  : [gitlab.com/.../layouts/_default/baseof.html][tutor-hugo-layouts-baseof]

{{< highlight html >}}
<!DOCTYPE html>
...
  <div class="container-fluid maxwidth">
...
{{< / highlight >}}

And the header

* <code>layouts/partials/site-header.html</code>
  : [gitlab.com/.../layouts/partials/site-header.html][tutor-hugo-layouts-header]

{{< highlight html >}}
<header>
  <nav class="navbar navbar-expand-md navbar-dark fixed-top
              bg-dark maxwidth">
  ...
{{< / highlight >}}

Also the footer

* <code>layouts/partials/site-footer.html</code>
  : [gitlab.com/.../layouts/partials/site-footer.html][tutor-hugo-layouts-footer]

{{< highlight html >}}
<footer class="footer bg-transparent">
  <div class="container text-center 
              bg-dark text-light maxwidth">
    <span>&copy; {{ now.Year }}.</span>
  </div>
</footer>
{{< / highlight >}}

You can see the different look as below.

![Hugo Bootstrap: Maxwidth Custom CSS][image-ss-02-maxwidth-large]

I guess it is more consistent than using mix 
of: <code>md</code>, <code>lg</code>, and <code>xl</code>.
Not everything should be using bootstrap.

-- -- --

### 3: Decoration: Border, Rounded, Shadow

In order to have a better looks,
I like to add these three bootstrap classes:

* Rounded

* Border

* Shadow

#### Decoration with Bootstrap

We need to add some custom variable here,
altering the default variable.

* <code>themes/tutor-04/sass/css/_variables.scss</code>
  : [gitlab.com/.../sass/_variables.scss][tutor-hugo-sass-variables].

{{< highlight scss >}}
// Variables

$blue:  #007bff;
$dark:  darken($blue, 35%);

$border-radius: 4px;
$border-color:  $dark; // no default
{{< / highlight >}}

Since we have border,
we should also adjust <code>footer</code> related stylesheet.

* <code>themes/tutor-04/sass/css/_sticky-footer-navbar.scss</code>
  : [gitlab.com/.../sass/_sticky-footer-navbar.scss][tutor-hugo-sass-sticky].

{{< highlight scss >}}
.footer {
  ...
  bottom: 1px;
  ...
}
{{< / highlight >}}

Change the header

* <code>themes/tutor-04/layouts/partials/site-header.html</code>
  : [gitlab.com/.../layouts/partials/site-header.html][tutor-hugo-layouts-header]

{{< highlight html >}}
<header>
  <nav class="navbar navbar-expand-md navbar-dark fixed-top
              bg-dark maxwidth rounded-bottom shadow
              border border-top-0 border-light">
  ...
{{< / highlight >}}

And the footer

* <code>themes/tutor-04/layouts/partials/site-footer.html</code>
  : [gitlab.com/.../layouts/partials/site-footer.html][tutor-hugo-layouts-footer]

{{< highlight html >}}
<footer class="footer bg-transparent">
  <div class="container
              text-center bg-dark text-light
              maxwidth rounded-top shadow
              border border-bottom-0 border-light">
    <span>&copy; {{ now.Year }}.</span>
  </div>
</footer>
{{< / highlight >}}

Also these two:

* <code>themes/tutor-04/layouts/_default/single.html</code>

* <code>themes/tutor-04/layouts/_default/list.html</code>
  : [gitlab.com/.../layouts/_default/list.html][tutor-hugo-layouts-list]

{{< highlight html >}}
{{ define "main" }}
<main role="main" class="container-fluid 
            bg-light rounded border border-dark shadow">
{{< / highlight >}}

But this one is, a little bit different, since it has layout.

* <code>themes/tutor-04/layouts/post/single.html</code>
  : [gitlab.com/.../layouts/post/single.html][tutor-hugo-layouts-post-single]

{{< highlight html >}}
{{ define "main" }}
  <main role="main" class="col-md-8">
    <div class="blog-main p-3 mb-3
                bg-light rounded border border-dark shadow">
      <div class="blog-post">
        <section>
          <h2 class="blog-post-title"
              >{{ .Title | default .Site.Title }}</h2>
          <p  class="blog-post-meta"
              >{{ .Page.Date.Format "January 02, 2006" }} by <a href="#">epsi</a></p>
        </section>

        <article>
          {{ .Content }}
        </article>
      </div><!-- /.blog-post -->
    </div><!-- /.blog-main -->
  </main>
{{ end }}

{{ define "aside" }}
  <aside class="col-md-4 blog-sidebar">
    <div class="p-3 mb-3
                bg-light rounded border border-dark shadow">
      <h4 class="font-italic">About You</h4>
      Are you an angel ?
    </div>
  </aside>
{{ end }}
{{< / highlight >}}

![Hugo Bootstrap: Decoration with Bootstrap][image-ss-03-decoration]

Actually, this is just an example.
You should write, what suit you most.

#### Custom CSS Hover Class

Not everything can be solved using bootstrap.
For example, hover would not work,
because of this <code>!important</code> directive.

* <code>themes/tutor-04/sass/bootstrap/utilities/_shadows.scss</code>

{{< highlight scss >}}
// stylelint-disable declaration-no-important

.shadow-sm { box-shadow: $box-shadow-sm !important; }
.shadow { box-shadow: $box-shadow !important; }
.shadow-lg { box-shadow: $box-shadow-lg !important; }
.shadow-none { box-shadow: none !important; }
{{< / highlight >}}

To solve this situation,
you need to create your own css.

* <code>themes/tutor-04/sass/css/_decoration.scss</code>
  : [gitlab.com/.../sass/_decoration.scss][tutor-hugo-sass-decoration].

{{< highlight scss >}}
// -- -- -- -- --
// _decoration.scss

...

.shadow-hover { box-shadow: $box-shadow; }

.shadow-hover:hover {
    box-shadow: 0 1rem 3rem rgba($black, .175);
    transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
}
{{< / highlight >}}

Rename all shadow class to <code>shadow-hover</code>.
And you are done. Ready for next chapter.

-- -- --

### What is Next ?

	Create necessary css, as you need.
	However, still Bootstrap can save you,
	from writing a lot of CSS code.

-- -- --

Need some rest ? Consider relax,
and look at this adorable kitten in my shoes.
Have a break for a while, prepare your playlist,
and resume our tutorial.

![adorable kitten][image-kitten]

There are, some interesting topic
about <code>Populating Content in Hugo using Archetypes</code>. 
Consider continue reading [ [Hugo - Section List][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2018/09/10/hugo-section-list/
[image-kitten]:             {{< baseurl >}}/assets/site/images/cats/shoes.jpg

[image-ss-01-maxwidth-small]:    {{< assets-ssg >}}/2018/09/41-maxwidth-small-screen.png
[image-ss-02-maxwidth-large]:    {{< assets-ssg >}}/2018/09/41-maxwidth-large-screen.png
[image-ss-02-maxwidth-medium]:   {{< assets-ssg >}}/2018/09/41-maxwidth-offset.png
[image-ss-03-decoration]:        {{< assets-ssg >}}/2018/09/41-border-rounded-shadow.png

[tutor-hugo-tutor-04]:           {{< tutor-hugo-bootstrap >}}/themes/tutor-04/
[tutor-hugo-sass-main]:          {{< tutor-hugo-bootstrap >}}/themes/tutor-04/sass/css/main.scss
[tutor-hugo-sass-decoration]:    {{< tutor-hugo-bootstrap >}}/themes/tutor-04/sass/css/_decoration.scss
[tutor-hugo-sass-layout]:        {{< tutor-hugo-bootstrap >}}/themes/tutor-04/sass/css/_layout.scss
[tutor-hugo-sass-variables]:     {{< tutor-hugo-bootstrap >}}/themes/tutor-04/sass/css/_variables.scss
[tutor-hugo-sass-sticky]:        {{< tutor-hugo-bootstrap >}}/themes/tutor-04/sass/css/_sticky-footer-navbar.scss

[tutor-hugo-layouts-baseof]:     {{< tutor-hugo-bootstrap >}}/themes/tutor-04/layouts/_default/baseof.html
[tutor-hugo-layouts-header]:     {{< tutor-hugo-bootstrap >}}/themes/tutor-04/layouts/partials/site-header.html
[tutor-hugo-layouts-footer]:     {{< tutor-hugo-bootstrap >}}/themes/tutor-04/layouts/partials/site-footer.html

[tutor-hugo-layouts-list]:       {{< tutor-hugo-bootstrap >}}/themes/tutor-04/layouts/_default/list.html
[tutor-hugo-layouts-post-single]:   {{< tutor-hugo-bootstrap >}}/themes/tutor-04/layouts/post/single.html
