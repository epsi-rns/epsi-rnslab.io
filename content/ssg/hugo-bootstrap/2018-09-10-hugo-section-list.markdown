---
type   : post
title  : "Hugo - Section List"
date   : 2018-09-10T09:17:35+07:00
slug   : hugo-section-list
categories: [ssg]
tags      : [hugo]
keywords  : [static site, custom theme, go html/template, archetypes, markdown, populate content, list by month]
author : epsi
opengraph:
  image: assets/site/images/topics/hugo-bootstrap-markdown.png

toc    : "toc-2018-09-hugo-bootstrap-step"

excerpt:
  Building Hugo Step by step, with Bootstrap as stylesheet frontend.
  Building content, and produce list for section.

---


### Preface

This article is intended for beginner.

> Goal: Building content, and produce list for section.

-- -- --

### 1: Section

I have been looking for a good content for these tutorial.
And believe me it is as hard as giving a good variable naming.
I look for simple content, such as churchill quotes,
also navier-stokes explanation,
and finally end up with quotes song lyric.

So yes, I decide to create a section named quotes.

#### Archetypes

Since every quotes having similar frontmatter,
we can make a frontmatter template,
so we do not have to type each time we make a new document.

Consider this archetypes for quotes.

* <code>themes/tutor-04/archetypes/quotes.md</code>
  : [gitlab.com/.../archetypes/quotes.md][tutor-hugo-archetypes-quotes]

{{< highlight markdown >}}
+++
type       = "post"
title      = "{{ replace .TranslationBaseName "-" " " | title }}"
date       = {{ .Date }}
categories = []
tags       = []
slug       = ""
author     = "epsi"
draft      = true
+++
{{< / highlight >}}

#### Permalink

For this quotes section, we can also set the permalink behaviour.

* <code>config.toml</code>
  : [gitlab.com/.../config.toml][tutor-hugo-config]

{{< highlight toml >}}
[permalinks]
  posts    = ":section/:year/:month/:day/:slug"
  quotes   = ":section/:year/:month/:day/:slug"
{{< / highlight >}}

-- -- --

### 2: Populate the Content

Now you can easily make ten content with archetypes.

#### Example Content

{{< highlight bash >}}
$ hugo new quotes/bruce-springsteen-the-river
/media/Works/sites/tutor-hugo/content/quotes/bruce-springsteen-the-river created
{{< / highlight >}}

![Hugo: Example Content][image-ss-02-hugo-new-river]

The result will be

* <code>content/quotes/bruce-springsteen-the-river.md</code>
  : [gitlab.com/.../...the-river.md][tutor-hugo-content-the-river]

{{< highlight markdown >}}
+++
type       = "post"
title      = "Bruce Springsteen the River"
date       = 2018-09-15T07:35:05+07:00
categories = ["lyric"]
tags       = []
slug       = ""
author     = "epsi"
draft      = true
+++
{{< / highlight >}}

Edit the frontmatter and add content,
so that we have something similar as below:

{{< highlight markdown >}}
+++
type       = "post"
title      = "Bruce Springsteen - The River"
date       = 2014-03-15T07:35:05+07:00
categories = ["lyric"]
tags       = [springsteen, 80s]
slug       = "bruce-springsteen-the-river"
author     = "epsi"
+++

Is a dream a lie 
if it don't come true? 
Or is it something worse?
{{< / highlight >}}

#### And The Rest

Consider do the rest for these nine lyrics:

{{< highlight bash >}}
$ for i in quotes/{\
"bruce-springsteen-one-step-up",\
"joni-mitchell-both-sides-now",\
"john-mayer-slow-dancing-in-a-burning-room",\
"julian-baker-something",\
"sarah-mclachlan-angel",\
"avenged-sevenfold-so-far-away",\
"michael-jackson-gone-too-soon",\
"mothers-no-crying-in-baseball",\
"blink-182-adams-song"}.md;\
do hugo new "$i"; done

/media/Works/sites/tutor-hugo/content/quotes/bruce-springsteen-one-step-up.md created
/media/Works/sites/tutor-hugo/content/quotes/joni-mitchell-both-sides-now.md created
/media/Works/sites/tutor-hugo/content/quotes/john-mayer-slow-dancing-in-a-burning-room.md created
/media/Works/sites/tutor-hugo/content/quotes/julian-baker-something.md created
/media/Works/sites/tutor-hugo/content/quotes/sarah-mclachlan-angel.md created
/media/Works/sites/tutor-hugo/content/quotes/avenged-sevenfold-so-far-away.md created
/media/Works/sites/tutor-hugo/content/quotes/michael-jackson-gone-too-soon.md created
/media/Works/sites/tutor-hugo/content/quotes/mothers-no-crying-in-baseball.md created
/media/Works/sites/tutor-hugo/content/quotes/blink-182-adams-song.md created
{{< / highlight >}}

![Hugo: All Content][image-ss-02-hugo-new-all]

Also edit the frontmatter and add content,
for the rest of the nine lyrics.

#### Result

Now we can see the list:

* <http://localhost:1313/quotes/>

![Hugo: Simple List in Browser][image-ss-02-list-simple]

Now we need to design a more informative
template layout for this content list.

-- -- --

### 3: Listing

Consider change this artefact:

* <code>themes/tutor-04/layouts/_default/list.html</code>
  : [gitlab.com/.../layouts/_default/list.html][tutor-hugo-layouts-list]

Instead of all kind of pages,
we filter it to show only post type.

{{< highlight twig >}}
{{ define "main" }}
<main role="main" 
      class="container-fluid m-3 m-sm-0 p-3
             bg-light rounded border border-dark shadow-hover">
  <header>
    <h4>{{ .Section }}</h4>
  </header>

  {{ .Content }}

  {{ $posts := where .Data.Pages "Type" "post" }}
  <section id="archive">
    {{ range ($posts.GroupByDate "2006") }}
      {{ partial "summary-by-year.html" . }}
    {{ end }}
  </section>
</main>
{{ end }}
{{< / highlight >}}

#### Listing By Year

Instead of simple loop, we can group the article by year.
The detail of the partial layout can bee seen here.

* <code>themes/tutor-04/layouts/partials/summary-by-year.html</code>
 : [gitlab.com/.../partials/summary-by-year.html][tutor-hugo-layouts-year]

{{< highlight twig >}}
      <div class ="anchor-target archive-year" 
           id="{{ .Key }}">{{ .Key }}</div>

      {{ range .Pages }}
      <div class="d-flex flex-row-reverse archive-list">
        <div class="text-right "><time>
            {{ .Date.Format "02 Jan" }}&nbsp;</time></div>
        <div class="mr-auto"><a href="{{ .URL | absURL }}">
          {{ .Title }}
        </a></div>
      </div>
      {{ end }}
{{< / highlight >}}

Add custom scss, so the list can have better looks.

* <code>themes/tutor-04/sass/css/_decoration.scss</code>
  : [gitlab.com/.../sass/_decoration.scss][tutor-hugo-sass-decoration].

{{< highlight scss >}}
// -- -- -- -- --
// _list.scss

.archive-list {
  position: relative;
}

.archive-list a:before {
  position: absolute;
  left: -15px;
  content: "\00BB";
  color: $dark;
}

.archive-list:hover {
  background-color: $yellow;
}
{{< / highlight >}}

_._

And do not forget to put the newly add custom scss to the main one.

* <code>themes/tutor-04/sass/css/main.scss</code>
  : [gitlab.com/.../sass/main.scss][tutor-hugo-sass-main].

{{< highlight scss >}}
@import
  ...

  // custom
    "layout",
    "decoration",
    "list"
;
{{< / highlight >}}

![Hugo: List by Year in Browser][image-ss-03-list-by-year]

#### Listing By Month

Of course, you can make, a more sophisticated listing,
by changing this artefact:

* <code>themes/tutor-04/layouts/_default/list.html</code>
  : [gitlab.com/.../layouts/_default/list.html][tutor-hugo-layouts-list]

{{< highlight twig >}}
{{ define "main" }}
<main role="main" 
  ...

  <section id="archive">
    {{ range ($posts.GroupByDate "2006") }}
      {{ partial "summary-by-month.html" . }}
    {{ end }}
  </section>
</main>
{{ end }}
{{< / highlight >}}

We use multiple loop stage. Outer: Group the article by year.
And inner: Group Article by month.

* <code>themes/tutor-04/layouts/partials/summary-by-month.html</code>
 : [gitlab.com/.../partials/summary-by-month.html][tutor-hugo-layouts-month]

{{< highlight twig >}}
  {{ $year := .Key }}
  <div class ="anchor-target archive-year" 
       id="{{ .Key }}">{{ $year }}</div>

  {{ range (.Pages.GroupByDate "January") }}
  {{ $month := .Key }}
  <div class="px-3">
    <span class ="anchor-target archive-month" 
          id="{{ $year }}-{{ $month }}">
          {{ $month }} - {{ $year }}</span>

    <div class="px-3">
      {{ range .Pages }}
      <div class="d-flex flex-row-reverse archive-list">
        <div class="text-right text-nowrap"><time>
            {{ .Date.Format "02 Jan" }}&nbsp;</time></div>
        <div class="mr-auto"><a href="{{ .URL | absURL }}">
          {{ .Title }}
        </a></div>
       </div>
      {{ end }}
     </div>

  </div>
  {{ end }}
{{< / highlight >}}

![Hugo: List by Month in Browser][image-ss-03-list-by-month]

-- -- --

### What is Next ?

There are, some interesting topic about <code>Taxonomy in Hugo</code>. 
Consider continue reading [ [Hugo - Taxonomy][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2018/09/11/hugo-taxonomy/

[image-ss-02-hugo-new-all]:      {{< assets-ssg >}}/2018/09/42-hugo-new-quote-all.png
[image-ss-02-hugo-new-river]:    {{< assets-ssg >}}/2018/09/42-hugo-new-quote-river.png
[image-ss-02-list-simple]:       {{< assets-ssg >}}/2018/09/42-list-simple.png
[image-ss-03-list-by-month]:     {{< assets-ssg >}}/2018/09/43-list-by-month.png
[image-ss-03-list-by-year]:      {{< assets-ssg >}}/2018/09/43-list-by-year.png

[tutor-hugo-archetypes-quotes]:  {{< tutor-hugo-bootstrap >}}/themes/tutor-04/archetypes/quotes.md
[tutor-hugo-config]:             {{< tutor-hugo-bootstrap >}}/config.toml
[tutor-hugo-content-the-river]:  {{< tutor-hugo-bootstrap >}}/content/quotes/bruce-springsteen-the-river.md

[tutor-hugo-layouts-list]:       {{< tutor-hugo-bootstrap >}}/themes/tutor-04/layouts/_default/list.html
[tutor-hugo-sass-main]:          {{< tutor-hugo-bootstrap >}}/themes/tutor-04/sass/css/main.scss
[tutor-hugo-sass-decoration]:    {{< tutor-hugo-bootstrap >}}/themes/tutor-04/sass/css/_decoration.scss

[tutor-hugo-layouts-month]:      {{< tutor-hugo-bootstrap >}}/themes/tutor-04/layouts/partials/summary-by-month.html
[tutor-hugo-layouts-year]:       {{< tutor-hugo-bootstrap >}}/themes/tutor-04/layouts/partials/summary-by-year.html
