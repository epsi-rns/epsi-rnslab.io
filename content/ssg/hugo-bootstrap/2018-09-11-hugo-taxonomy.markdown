---
type   : post
title  : "Hugo - Taxonomy"
date   : 2018-09-11T09:17:35+07:00
slug   : hugo-taxonomy
categories: [ssg]
tags      : [hugo]
keywords  : [static site, custom theme, configuration, go html/template, taxonomy, terms]
author : epsi
opengraph:
  image: assets/site/images/topics/hugo-bootstrap-markdown.png

toc    : "toc-2018-09-hugo-bootstrap-step"

excerpt:
  Building Hugo Step by step, with Bootstrap as stylesheet frontend.
  Create custom taxonomy terms layout and also taxonomy detail layout.

---


### Preface

This article is intended for beginner.

> Goal: Create custom taxonomy terms layout and also taxonomy detail layout.

-- -- --

### 1: Taxonomy Terms

Remember our last <code>config.toml</code>.
We already have these taxonomies:

* <code>config.toml</code>
  : [gitlab.com/.../config.toml][tutor-hugo-config]

{{< highlight toml >}}
# An example of Hugo site for tutorial purpose.

baseURL      = "http://example.org/"
languageCode = "en-us"
title        = "Letters to my Beloved"
theme        = "tutor-04"

[taxonomies]
  category = "categories"
  tag = "tags"

[permalinks]
  posts    = ":section/:year/:month/:day/:slug"
  quotes   = ":section/:year/:month/:day/:slug"
{{< / highlight >}}

Instead of the standard terms.html,
we can make it prettier, by making our own custom terms.html.

* <code>themes/tutor-04/layouts/_default/terms.html</code>
  : [gitlab.com/.../layouts/_default/terms.html][tutor-hugo-layouts-terms]

{{< highlight twig >}}
{{ define "main" }}
<main role="main" 
      class="container-fluid m-3 m-sm-0 p-3
             bg-light rounded border border-dark shadow-hover">
  <header>
    <h4>{{ .Section }}</h4>
  </header>

  {{ .Content }}

  <section class="pl-3" id="archive">
    {{ range $key, $value := .Data.Terms }}
      <div class ="anchor-target archive-list" id="{{ $key }}">
        {{ $.Data.Singular | humanize }}: 
        <a href="{{ "/" | relLangURL }}{{ $.Data.Plural | urlize }}/{{ $key | urlize }}">
        {{ $key }}</a>
      </div>
    {{ end }}
  </section>
</main>
{{ end }}
{{< / highlight >}}

* <http://localhost:1313/categories/>

![Hugo: Simple Taxonomy Terms][image-ss-04-terms-simple]

-- -- --

Why not go further ?
Do not let beaurocracy stop you, let's dump all the link here.
And also give nice tag list using boostrap badge above the header.

* <code>themes/tutor-04/layouts/_default/terms.html</code>
  : [gitlab.com/.../layouts/_default/terms.html][tutor-hugo-layouts-terms]

{{< highlight twig >}}
{{ define "main" }}
<main role="main" 
      class="container-fluid m-3 m-sm-0 p-3
             bg-light rounded border border-dark shadow-hover">

  <div class="">
    {{ range $key, $value := .Data.Terms }}
      {{ $postCount := len $value.Pages  }}
      <a href="{{ "/" | relLangURL }}{{ $.Data.Plural | urlize }}/{{ $key | urlize }}">
      <span class="badge badge-dark">{{ $key }} 
      <span class="badge badge-light">{{ $postCount }}</span></span></a>
    {{ end }}
  </div>
  <br/>

  <header>
    <h4>{{ .Section }}</h4>
  </header>

  {{ .Content }}

  <section class="pl-3" id="archive">
    {{ range $key, $value := .Data.Terms }}
      <div class ="anchor-target" id="{{ $key }}">
        {{ $.Data.Singular | humanize }}: 
        <a href="{{ "/" | relLangURL }}{{ $.Data.Plural | urlize }}/{{ $key | urlize }}">
        {{ $key }}</a>
      </div>

      <div class="pl-3">
      {{ range $value.Pages }}
      <div class="d-flex flex-row-reverse archive-list">
        <div class="text-right text-nowrap">
          <time datetime="{{ .Date.Format "2006-01-02T15:04:05Z07:00" }}">
            {{ .Date.Format "Jan 02, 2006" }}&nbsp;</time></div>
        <div class="mr-auto"><a href="{{ .URL | absURL }}">
          {{ .Title }}
        </a></div>
      </div>
      {{ end }}
      </div>

    {{ end }}
  </section>
</main>
{{ end }}
{{< / highlight >}}

* <code>http://localhost:1313/categories/</code>

![Hugo: Complex Taxonomy Terms][image-ss-04-terms-complex]

-- -- --

### 2: Taxonomy Detail

When you click the love badge or lyric badge,
an url will be given:

* <http://localhost:1313/categories/lyric/>

Which will be using the <code>layouts/_default/list.html</code>.
Which is somehow having the wrong result,
because the <code>.Pages</code> is filtered for only post kind page.

![Hugo: Taxonomy Detail Using List][image-ss-05-taxonomy-list]

Of course we can make our own taxonomy.html to show taxonomy detail.

* <code>themes/tutor-04/layouts/_default/taxonomy.html</code>
  : [gitlab.com/.../layouts/_default/taxonomy.html][tutor-hugo-layouts-taxonomy]

{{< highlight twig >}}
{{ define "main" }}
<main role="main" 
      class="container-fluid m-3 m-sm-0 p-3
             bg-light rounded border border-dark shadow-hover">
  <header>
    <h4>{{ .Section }}</h4>
  </header>

  {{ .Content }}
  
  {{ $posts := .Data.Pages }}
  <section id="archive">
   {{ range ($posts.GroupByDate "2006") }}
     {{ partial "summary-by-month.html" . }}
   {{ end }}
  </section>
</main>
{{ end }}
{{< / highlight >}}

![Hugo: Simple Taxonomy Detail][image-ss-05-taxonomy-custom]

-- -- --

### What is Next ?

There are, some interesting topic about <code>Blog Archive in Hugo</code>. 
Consider continue reading [ [Hugo - Archive][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2018/09/12/hugo-archive/

[image-ss-04-terms-complex]:     {{< assets-ssg >}}/2018/09/44-terms-complex-categories.png
[image-ss-04-terms-simple]:      {{< assets-ssg >}}/2018/09/44-terms-simple-categories.png
[image-ss-05-taxonomy-custom]:   {{< assets-ssg >}}/2018/09/44-taxonomy-custom-layout.png
[image-ss-05-taxonomy-list]:     {{< assets-ssg >}}/2018/09/44-taxonomy-using-list.png

[tutor-hugo-config]:             {{< tutor-hugo-bootstrap >}}/config.toml
[tutor-hugo-layouts-terms]:      {{< tutor-hugo-bootstrap >}}/themes/tutor-04/layouts/_default/terms.html
[tutor-hugo-layouts-taxonomy]:   {{< tutor-hugo-bootstrap >}}/themes/tutor-04/layouts/_default/taxonomy.html
