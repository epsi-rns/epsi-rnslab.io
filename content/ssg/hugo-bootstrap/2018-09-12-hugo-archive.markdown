---
type   : post
title  : "Hugo - Archive"
date   : 2018-09-12T09:17:35+07:00
slug   : hugo-archive
categories: [ssg]
tags      : [hugo]
keywords  : [static site, custom theme, go html/template, archive, chronogical list]
author : epsi
opengraph:
  image: assets/site/images/topics/hugo-bootstrap-markdown.png

toc    : "toc-2018-09-hugo-bootstrap-step"

excerpt:
  Building Hugo Step by step, with Bootstrap as stylesheet frontend.
  Create blog archive, a site wide content list,
  for all specific page kind named post.

---


### Preface

This article is intended for beginner.

> Goal: Create blog archive, a site wide content list for all posts.

-- -- --

### 1: Archive Page by Group

We can make a page dedicated for archiving all posts.
I always have this page, 
to show a shortcut to all article links.

#### Prepare All Artefacts

{{< highlight bash >}}
$ mkdir themes/tutor-04/layouts/archives
$ touch themes/tutor-04/layouts/archives/single.html

$ mkdir content/pages
$ touch content/pages/archives.md
{{< / highlight >}}

#### Content: Single

Just an empty fronmatter without the need of any content.

* <code>content/pages/archives.md</code>
  : [gitlab.com/.../pages/archives.md][tutor-hugo-content-archives]

{{< highlight toml >}}
+++
type  = "archives"
title = "Archives by Date"
+++
{{< / highlight >}}

#### Layout: Single

* <code>themes/tutor-04/layouts/archives/single.html</code>
  : [gitlab.com/.../layouts/archives/single.html][tutor-hugo-layouts-single]

{{< highlight twig >}}
{{ define "main" }}
<main role="main" 
      class="container-fluid m-3 m-sm-0 p-3
             bg-light rounded border border-dark shadow-hover">
  <header>
    <h4>{{ .Title | default .Site.Title }}</h4>
  </header>

  <article>
    {{ .Content }}
  </article>
  
  {{ $posts := where .Site.Pages "Type" "post" }}
  <section id="archive">
    {{ range ($posts.GroupByDate "2006") }}
      {{ partial "summary-by-month.html" . }}
    {{ end }}
  </section>
</main>
{{ end }}
{{< / highlight >}}

#### Partial: Summary

Already discussed in previous article.

#### Server Output: Browser

And see the result

* <http://localhost:1313/pages/archives/>

![Hugo: Archive by Group][image-ss-06-archive-single]

The different of this page and the taxonomy page is:
the list filtered for specific page kind named post.

-- -- --

### 2: Chronogical Archive List

And you can also have page list with chronological order.
Later we will combine this with nice pagination.

#### Prepare All Artefacts

{{< highlight bash >}}
$ touch themes/tutor-04/layouts/archives/list.html
$ touch themes/tutor-04/layouts/partials/summary-blog-list.html
$ touch content/pages/_index.md
{{< / highlight >}}

#### Content: List

Also no content required. Frontmatter is sufficient.

* <code>content/pages/_index.md</code>
  : [gitlab.com/.../pages/_index.md][tutor-hugo-content-index]

{{< highlight markdown >}}
+++
type  = "archives"
title = "Blog List"
+++
{{< / highlight >}}

#### Layout: List

* <code>themes/tutor-04/layouts/archives/list.html</code>
  : [gitlab.com/.../layouts/archives/list.html][tutor-hugo-layouts-list]

{{< highlight twig >}}
{{ define "main" }}
<main role="main" 
      class="container-fluid m-3 m-sm-0 p-3
             bg-light rounded border border-dark shadow-hover">
  <header>
    <h4>{{ .Title | default .Site.Title }}</h4>
  </header>

  <article>
    {{ .Content }}
  </article>

  {{ $posts := where .Site.Pages "Type" "post" }}
  <section id="archive">
  <div class="post-list">
    {{ range $posts }}
      {{ partial "summary-blog-list.html" . }}
    {{ end }}
  </div>
  </section>
</main>
{{ end }}
{{< / highlight >}}

#### Partial: Summary

Where the summary layout template is similar as code below:

* <code>themes/tutor-04/layouts/partials/summary-blog-list.html</code>
 : [gitlab.com/.../partials/summary-blog-list.html][tutor-hugo-layouts-blog]

{{< highlight twig >}}
    <div class="p-3">

      <strong><a href="{{ .Permalink }}">{{ .Title }}</strong></a>

      <div>
        <div class="float-left">
          <time datetime="{{ .Date.Format "2006-01-02T15:04:05Z07:00" }}">
          {{ .Date.Format "Jan 2, 2006" }}</time>
        </div>      
        <div class="float-right" id="meta_tags">
          {{ range .Params.tags }}
            <a href="{{ printf "tags/%s" . | absURL }}">
              <span class="badge badge-dark">{{ . }}</span></a>
          {{ end }}
        </div>
      </div> 

      <div class="clearfix"></div>
      
      <div>
        {{ if .Params.Excerpt -}}
          {{ .Params.Excerpt }}
        {{- else -}}
          {{ .Summary }}
        {{- end }}
      </div>

      <div class="read-more">
        <a href="{{ .Permalink }}" 
           class="btn btn-dark btn-sm">Read More</a>
      </div>

    </div>
{{< / highlight >}}

I'm using bootstrap badge feature.

#### Server Output: Browser

Consider see the result as usual.

* <http://localhost:1313/pages/>

![Hugo: Chronological Blog Archive][image-ss-06-archive-list]

-- -- --

### 3: Real World Navigation

Now that our archives layout and taxonomy pages has complete,
Consider change the header.

![Hugo: Wide Navigation][image-ss-06-header-wide]

* <code>themes/tutor-04/layouts/partials/site-header.html</code>
  : [gitlab.com/.../layouts/partials/site-header.html][tutor-hugo-layouts-header]

{{< highlight html >}}
<header>
  <nav class="navbar navbar-expand-md navbar-dark fixed-top
              bg-dark maxwidth rounded-bottom shadow-hover
              border border-top-0 border-light">
    <a class="navbar-brand" href="{{ .Site.BaseURL }}">
       <img src="{{ "images/logo-gear-blue.png" | absURL }}"
           alt="Home" />
    </a>
    <button class="navbar-toggler" type="button" 
        data-toggle="collapse" data-target="#navbarCollapse" 
        aria-controls="navbarCollapse" aria-expanded="false" 
        aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="{{ "pages" | absURL }}"
             >Blog <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item active dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" 
             role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
             Archives
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="{{ "tags" | absURL }}">By Tag</a>
            <a class="dropdown-item" href="{{ "categories" | absURL }}">By Category</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="{{ "pages/archives" | absURL }}">By Date</a>
          </div>
        </li>
      </ul>
      <form class="form-inline mt-2 mt-md-0">
        <input class="form-control mr-sm-2" type="text" 
          placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-light my-2 my-sm-0" 
          type="submit">Search</button>
      </form>
    </div>
  </nav>
</header>
{{< / highlight >}}

#### Server Output: Browser

Now you can see a usable navigation bar in your smartphone.

![Hugo: Small Navigation][image-ss-06-header-small]

We will add nice pagination later.

-- -- --

### What is Next ?

Tired ? Consider relax, and look at this innocent kitten in my kitchen.
I rescued her after almost drowned in sewer.
Have a break for a while, make another cup of coffe,
eat your veggies, and resume our tutorial.

![adorable kitten][image-kitten]

There are, some interesting topic about <code>Hugo Custom Content Type</code>.
Consider continue reading [ [Hugo - Custom Content Type][local-whats-next] ].


Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2018/09/13/hugo-custom-output/
[image-kitten]:             {{< baseurl >}}/assets/site/images/cats/cemong.jpg

[image-ss-06-archive-list]:      {{< assets-ssg >}}/2018/09/46-archives-list.png
[image-ss-06-archive-single]:    {{< assets-ssg >}}/2018/09/46-archives-single.png
[image-ss-06-header-small]:      {{< assets-ssg >}}/2018/09/46-layout-header-small.png
[image-ss-06-header-wide]:       {{< assets-ssg >}}/2018/09/46-layout-header-wide.png

[tutor-hugo-content-archives]:   {{< tutor-hugo-bootstrap >}}/content/pages/archives.md
[tutor-hugo-content-index]:      {{< tutor-hugo-bootstrap >}}/content/pages/_index.md
[tutor-hugo-layouts-single]:     {{< tutor-hugo-bootstrap >}}/themes/tutor-04/layouts/archives/single.html
[tutor-hugo-layouts-list]:       {{< tutor-hugo-bootstrap >}}/themes/tutor-04/layouts/archives/list.html
[tutor-hugo-layouts-blog]:       {{< tutor-hugo-bootstrap >}}/themes/tutor-04/layouts/partials/summary-blog-list.html
[tutor-hugo-layouts-header]:     {{< tutor-hugo-bootstrap >}}/themes/tutor-04/layouts/partials/site-header.html
