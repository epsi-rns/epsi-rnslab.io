---
type   : post
title  : "Hugo - Widget"
date   : 2018-09-14T09:17:35+07:00
slug   : hugo-sidebar
categories: [ssg, frontend]
tags      : [hugo]
keywords  : [static site, custom theme, widget, recent post, categories, tags, archives]
author : epsi
opengraph:
  image: assets/site/images/topics/hugo-bootstrap-markdown.png

toc    : "toc-2018-09-hugo-bootstrap-step"

excerpt:
  Building Hugo Step by step, with Bootstrap as stylesheet frontend.
  Miscellanous range loop to achieved wordpress like sidebar.
  Also using bootstrap.

---

### Preface

This article is intended for beginner.

> Goal: Miscellanous range loop to achieved wordpress like sidebar.

We can make a sidebar, so that we can have more information,
just like what common in wordpress site or other blogging platform.
I'm using Bootstrap card,
and of course you can use view other than bootstrap card.

-- -- --

### 1: Prepare

#### Copy Theme

Wwith your file manager, copy manually
<code>themes/tutor-04</code> to <code>themes/tutor-05</code>.

* <code>config.toml</code>
  : [gitlab.com/.../config.toml][tutor-hugo-config]

{{< highlight toml >}}
theme        = "tutor-05"
{{< / highlight >}}

#### Layout: Single

Our final layout would be:

* <code>themes/tutor-05/layouts/post/single.html</code>
  : [gitlab.com/.../layouts/post/single.html][tutor-hugo-layouts-post-single]

{{< highlight twig >}}
{{ define "main" }}
...
{{ end }}

{{ define "aside" }}
  <aside class="col-md-4">
    {{ partial "card-recent-posts.html" . }}
    {{ partial "card-archives.html" . }}

    {{ partial "card-categories.html" . }}
    {{ partial "card-tags.html" . }}

    {{ partial "card-friends.html" . }}    
    {{ partial "card-related-posts.html" . }}
  </aside>
{{ end }}
{{< / highlight >}}

But we are going to discuss one by one.

#### SASS: List

I'm using Bootstrap card.
So I do not need to setup my own css.

However, I still need a few changes, to make it a little bit pretty.

* <code>themes/tutor-05/sass/css/_list.scss</code>
  : [gitlab.com/.../sass/_list.scss][tutor-hugo-sass-list].

{{< highlight css >}}
// -- -- -- -- --
// _list.scss

div.archive-list {
  position: relative;
}

div.archive-list a:before {
  position: absolute;
  left: -15px;
  content: "\00BB";
  color: $dark;
}

div.archive-list:hover {
  background-color: lighten($yellow, 10%);
}

ul.archive-list,
ul.archive-list ul, {
  position: relative;
  list-style: none;
}

ul.archive-list li:before {
  position: relative;
  left: -15px;
  content: "\00BB";
  color: $dark;
}

ul.archive-list li:hover {
  background-color: lighten($yellow, 10%);
}

ul.archive-list li li:hover {
  background-color: lighten($yellow, 30%);
}
{{< / highlight >}}

Of course you can use view other than bootstrap card.

_._

-- -- --

### 2: Recent Post

#### Layout: Single

Consider examine only recent posts:

* <code>themes/tutor-05/layouts/post/single.html</code>
  : [gitlab.com/.../layouts/post/single.html][tutor-hugo-layouts-post-single]

{{< highlight twig >}}
{{ define "aside" }}
  <aside class="col-md-4">
    {{ partial "card-recent-posts.html" . }}
  </aside>
{{ end }}
{{< / highlight >}}

#### Partial: Widget

* <code>themes/tutor-05/layouts/partials/card-recent-posts.html</code>
  : [gitlab.com/.../layouts/partials/card-recent-posts.html][tutor-hugo-layouts-recent]

{{< highlight twig >}}
<div class="card hidden-sm hidden-xs mb-3">
  <div class="card-header bg-dark text-light pb-0">
    <p class="card-title">Recent Posts</p>
  </div>
  <div class="card-body py-2">
    <ul class="archive-list">
      {{ range first 5 (where .Site.Pages "Type" "post") }}
      <li><a href="{{ .URL | absURL }}">{{ .Title }}</a></li>
      {{ end }}
    </ul>
  </div>
</div>
{{< / highlight >}}

#### Server Output: Browser

Now you can see the result in the browser.
Due to the wide image, I crop only the card part.

![Hugo: Card Recent Posts][image-ss-01-card-recent-posts]

-- -- --

### 3: Categories and Tags

Both are Taxonomies.
So the layouts is pretty similar.

#### Partial: Widget: Categories

* <code>themes/tutor-05/layouts/partials/card-categories.html</code>
  : [gitlab.com/.../layouts/partials/card-categories.html][tutor-hugo-layouts-categories]

{{< highlight twig >}}
<div class="card mb-3">
  <div class="card-header bg-dark text-light pb-0">
    <p class="card-title">Categories</p>
  </div>
  <div class="card-body py-2">
    {{ range $name, $taxonomy := .Site.Taxonomies.categories }}
      <a href="{{ printf "categories/%s" $name | absURL }}">
        <span class="badge badge-primary">{{ $name }}</span>
      </a>&nbsp;
    {{ end }}
  </div>
</div> 
{{< / highlight >}}

#### Server Output: Browser

Now you can see the result in the browser.
Due to the wide image, I crop only the card part.

![Hugo: Card Categories][image-ss-01-card-categories]

#### Partial: Widget: Tags

* <code>themes/tutor-05/layouts/partials/card-tags.html</code>
  : [gitlab.com/.../layouts/partials/card-tags.html][tutor-hugo-layouts-tags]

{{< highlight twig >}}
<div class="card mb-3">
  <div class="card-header bg-dark text-light pb-0">
    <p class="card-title">Tags</p>
  </div>
  <div class="card-body py-2">
    {{ range $name, $taxonomy := .Site.Taxonomies.tags }}
      <a href="{{ printf "tags/%s" $name | absURL }}">
        <span class="badge badge-secondary">{{ $name }}</span>
      </a>&nbsp;
    {{ end }}
  </div>
</div> 
{{< / highlight >}}

#### Server Output: Browser

Now you can see the result in the browser.
Due to the wide image, I crop only the card part.

![Hugo: Card Tags][image-ss-01-card-tags]

-- -- --

### 4: Archives

Remember the Archives tutorial in a previous chapter ?
We can also apply this in similar fashion.

#### Partial: Widget

* <code>themes/tutor-05/layouts/partials/card-archives.html</code>
  : [gitlab.com/.../layouts/partials/card-archives.html][tutor-hugo-layouts-archives]

{{< highlight twig >}}
<div class="card mb-3">
  <div class="card-header bg-dark text-light pb-0">
    <p class="card-title">Archives</p>
  </div>
  <div class="card-body py-2">

  {{ $posts := where .Site.Pages "Type" "post" }}
  {{ $page_year   := .Page.Date.Format "2006" }}
  {{ $page_month  := .Page.Date.Format "January" }}
    
  {{ range ($posts.GroupByDate "2006") }}  
    {{ $year := .Key }}
      <div class ="archive-year" id="{{ $year }}">
        <a href="{{ "pages/archives" | absURL }}#{{ $year }}">
        {{ $year }}</a>
      </div>

    {{ if eq $year $page_year }}
      <ul class="archive-list">
      {{ range (.Pages.GroupByDate "January") }}
      {{ $month := .Key }}
      <li>
        
        <span id="{{ $year }}-{{ $month }}">
              <a href="{{ "pages/archives" | absURL }}#{{ $year }}-{{ $month }}">
              {{ $month }}</a> - {{ $year }}</span>

        {{ if eq $month $page_month }}
          <ul>
          {{ range $post_month := .Pages }}

          <li>
            <a href="{{ $post_month.URL | absURL }}">
              {{ $post_month.Title }}
            </a>
          </li>
          {{ end }}
          </ul>
        {{ end }}

      </li>
      {{ end }}
      </ul>
    {{ end }}

  {{ end }}
  </div>
</div>
{{< / highlight >}}

#### Server Output: Browser

Now you can see the result in the browser.
Due to the wide image, I crop only the card part.

![Hugo: Card Archives][image-ss-01-card-archives]

-- -- --

### What is Next ?

There are, some interesting topic about <code>Example Static Data with Hugo</code>.
Consider continue reading [ [Hugo - Data][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2018/09/15/hugo-data/

[image-ss-01-card-archives]:     {{< assets-ssg >}}/2018/09/51-card-archives.png
[image-ss-01-card-categories]:   {{< assets-ssg >}}/2018/09/51-card-categories.png
[image-ss-01-card-recent-posts]: {{< assets-ssg >}}/2018/09/51-card-recent-post.png
[image-ss-01-card-tags]:         {{< assets-ssg >}}/2018/09/51-card-tags.png

[tutor-hugo-config]:             {{< tutor-hugo-bootstrap >}}/config.toml
[tutor-hugo-layouts-post-single]: {{< tutor-hugo-bootstrap >}}/themes/tutor-05/layouts/post/single.html

[tutor-hugo-sass-list]:          {{< tutor-hugo-bootstrap >}}/themes/tutor-05/sass/css/_list.scss
[tutor-hugo-layouts-recent]:     {{< tutor-hugo-bootstrap >}}/themes/tutor-05/layouts/partials/card-recent-posts.html
[tutor-hugo-layouts-archives]:   {{< tutor-hugo-bootstrap >}}/themes/tutor-05/layouts/partials/card-archives.html
[tutor-hugo-layouts-categories]: {{< tutor-hugo-bootstrap >}}/themes/tutor-05/layouts/partials/card-categories.html
[tutor-hugo-layouts-tags]:       {{< tutor-hugo-bootstrap >}}/themes/tutor-05/layouts/partials/card-tags.html



