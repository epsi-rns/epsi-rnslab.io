---
type   : post
title  : "Hugo - Pagination - Simple"
date   : 2018-11-17T09:17:35+07:00
slug   : hugo-pagination-simple
categories: [ssg, frontend]
tags      : [hugo, navigation, bootstrap]
keywords  : [static site, custom theme, pagination, simple]
author : epsi
opengraph:
  image: assets/site/images/topics/hugo-bootstrap.png

toc    : "toc-2018-09-hugo-bootstrap-step"

excerpt:
  Building Hugo Step by step, with Bootstrap as stylesheet frontend.
  Pagination for dummies, a simple introduction.

---

### Preface

> Goal: A Simple Pagination.

![Hugo Pagination: Combined Animation][image-ss-02-combined-animation]


-- -- --

### 1: Prepare

Pagination can be set in config.toml

* <code>config.toml</code>
  : [gitlab.com/.../config.toml][tutor-hugo-config]

{{< highlight toml >}}
Paginate     = 7
{{< / highlight >}}

In layout we are going to use

{{< highlight twig >}}
{{ $paginator := .Paginate .Site.Pages }}
{{< / highlight >}}

Since we only show specific page kind named post, 
we should filter the pages using where.

{{< highlight twig >}}
{{ $paginator := .Paginate (where .Site.Pages "Type" "post") }}
{{< / highlight >}}

While debugging, or development process,
we can do this to override sitewide <code>paginate</code> configuration.
Here we use <code>5</code> articles for each pagination page.

{{< highlight twig >}}
{{ $paginator := .Paginate (where .Site.Pages "Type" "post") 5 }}
{{< / highlight >}}

This is a very helpful trick for me.

-- -- --

### 2: Preview: General

This is what we want to achieve in this tutorial.

![Hugo Pagination: Simple Positioning Indicator][image-ss-02-simple-indicator]

#### HTML Preview

The HTML that we want to achieve in this article, is similar as below.

{{< highlight html >}}
<ul class="pagination justify-content-center">
  <li class="page-item disabled">
    <span class="page-link">First</span>
  </li>
  <li class="page-item disabled">
    <span class="page-link">Previous</span>
  </li>
   <li class="page-item disabled">
    <span class="page-link">Page: 1 of 3</span>
  </li>
  <li class="page-item">
    <a class="page-link" href="/pages/page/2/" rel="next">Next</a>
  </li>
  <li class="page-item">
    <a class="page-link" href="/pages/page/3/" rel="last">Last</a>
  </li>
</ul>
{{< / highlight >}}

We will achieve this with Hugo code.

-- -- --

### 3: Layout

Change our previous blog list

* <code>themes/tutor-05/layouts/archives/list.html</code>.

{{< highlight twig >}}
  {{ $posts := where .Site.Pages "Type" "post" }}
  <section id="archive">
  <div class="post-list">
    {{ range $posts }}
      {{ partial "summary-blog-list.html" . }}
    {{ end }}
  </div>
  </section>
{{< / highlight >}}

to use paginator

{{< highlight twig >}}
  {{ $paginator := .Paginate (where .Site.Pages "Type" "post") }}
  {{ partial "pagination-simple.html" (dict "p" $paginator) }}
  <section id="archive">
  <div class="post-list">
    {{ range $paginator.Pages }}
      {{ partial "summary-blog-list.html" . }}
    {{ end }}
  </div>
  </section>
{{< / highlight >}}

#### Layout: List

The complete file is here below:

* <code>themes/tutor-05/layouts/archives/list.html</code>
  : [gitlab.com/.../layouts/archives/list.html][tutor-hugo-layouts-list].

{{< highlight twig >}}
{{ define "main" }}
<main role="main" 
      class="container-fluid m-3 m-sm-0 p-3
             bg-light rounded border border-dark shadow-hover">
  <header>
    <h4>{{ .Title | default .Site.Title }}</h4>
  </header>

  <article>
    {{ .Content }}
  </article>

  {{ $paginator := .Paginate (where .Site.Pages "Type" "post") }}
  {{ partial "pagination-simple.html" (dict "p" $paginator) }}
  <section id="archive">
  <div class="post-list">
    {{ range $paginator.Pages }}
      {{ partial "summary-blog-list.html" . }}
    {{ end }}
  </div>
  </section>
</main>
{{ end }}
{{< / highlight >}}

#### Dictionary

In order to pass variable from list layout to pagination partial,
we utilize dictionary: <code>(dict "p" $paginator)</code>.

{{< highlight twig >}}
  {{ $paginator := .Paginate (where .Site.Pages "Type" "post") }}
  {{ partial "pagination-simple.html" (dict "p" $paginator) }}
{{< / highlight >}}

-- -- --

### 4: Navigation: Previous and Next

No need to show any pagination navigation,
if we what we have is only one page.
Hence our minimal pagination logic would be:

{{< highlight twig >}}
<nav aria-label="Page navigation">

  {{ if gt .p.TotalPages 1 }}
  <ul class="pagination justify-content-center">
  ...
  </ul>
  {{ end }}
</nav>
{{< / highlight >}}

Of course this is not enough.
Our minimal pagination should show something.

#### Partial: Pagination Simple

* <code>themes/tutor-05/layouts/partials/pagination-simple.html</code>
 : [gitlab.com/.../partials/pagination-simple.html][tutor-hugo-layouts-simple].

{{< highlight twig >}}
<nav aria-label="Page navigation">

  {{ if gt .p.TotalPages 1 }}
  <ul class="pagination justify-content-center">

    <!-- Previous Page. -->
    {{ if .p.HasPrev }}
      <li class="page-item">
        <a class="page-link" href="{{ .p.Prev.URL }}"
           rel="prev">Previous</a>
      </li>
    {{ else }}
      <li class="page-item disabled">
        <span class="page-link">Previous</span>
      </li>
    {{ end }}

    <!-- Next Page. -->
    {{ if .p.HasNext }}
      <li class="page-item">
        <a class="page-link" href="{{ .p.Next.URL }}"
           rel="next">Next</a>
      </li>
    {{ else }}
      <li class="page-item disabled">
        <span class="page-link">Next</span>
      </li>
    {{ end }}

  </ul>
  {{ end }}

</nav>
{{< / highlight >}}

#### Browser: Pagination Preview

![Hugo Pagination: Simple Previous Next][image-ss-02-simple-prev-next]

#### How does it works ?

This code above rely on 

* Previous Page: <code>if .HasPrev</code>, and 

* Next Page: <code>if .HasNext</code>.

This should be self explanatory.

#### Stylesheet: Bootstrap Class

Bootstrap specific class are these below

* ul: <code>pagination</code>

* li: <code>page-item</code>

* a, span: <code>page-link</code>

* li: <code>disabled</code>

-- -- --

### 5: Navigation: First and Last

Consider also add first page and last page.

{{< highlight twig >}}
<nav aria-label="Page navigation">

  {{ if gt .p.TotalPages 1 }}
  <ul class="pagination justify-content-center">

    <!-- First Page. -->
    {{ if not (eq .p.PageNumber 1) }}
      <li class="page-item">
        <a class="page-link" href="{{ .p.First.URL }}"
           rel="first">First</a>
      </li>
    {{ else }}
      <li class="page-item disabled">
        <span class="page-link">First</span>
      </li>
    {{ end }}

    ...

    <!-- Last Page. -->
    {{ if not (eq .p.PageNumber .p.TotalPages) }}
      <li class="page-item">
        <a class="page-link" href="{{ .p.Last.URL }}"
           rel="last">Last</a>
      </li>
    {{ else }}
      <li class="page-item disabled">
        <span class="page-link">Last</span>
      </li>
    {{ end }}
  </ul>
  {{ end }}

</nav>
{{< / highlight >}}

#### Browser: Pagination Preview

![Hugo Pagination: Simple First Last][image-ss-02-simple-first-last]

#### How does it works ?

Since there is no equivalent
for <code>.HasPrev</code> and <code>.HasNext</code>
to show first page and last page,
we utilize:

* First Page: <code>.PageNumber == 1</code>.

* Last Page: <code>.PageNumber == .TotalPages</code>.

-- -- --

### 6: Active Page Indicator

And also the pagination positioning in between,
in the middle of those.

{{< highlight twig >}}
<nav aria-label="Page navigation">

  {{ if gt .p.TotalPages 1 }}
  <ul class="pagination justify-content-center">
    ...

    <!-- Indicator Number. -->
    <li class="page-item">
      <span class="page-link">
        Page: {{ .p.PageNumber }} of {{ .p.TotalPages }}</span>
    </li>

    ...
  </ul>
  {{ end }}

</nav>
{{< / highlight >}}

#### Browser: Pagination Preview

![Hugo Pagination: Simple Positioning Indicator][image-ss-02-simple-indicator]

#### How does it works ?

This just show:

{{< highlight twig >}}
Page: {{ .p.PageNumber }} of {{ .p.TotalPages }}
{{< / highlight >}}

-- -- --

### 7: Summary

Complete code is here below:

* <code>themes/tutor-05/layouts/partials/pagination-simple.html</code>
 : [gitlab.com/.../partials/pagination-simple.html][tutor-hugo-layouts-simple].


{{< highlight twig >}}
<nav aria-label="Page navigation">

  {{ if gt .p.TotalPages 1 }}
  <ul class="pagination justify-content-center">

    <!-- First Page. -->
    {{ if not (eq .p.PageNumber 1) }}
      <li class="page-item">
        <a class="page-link" href="{{ .p.First.URL }}" 
           rel="first">First</a>
      </li>
    {{ else }}
      <li class="page-item disabled">
      <span class="page-link">First</span>
    {{ end }}

    <!-- Previous Page. -->
    {{ if .p.HasPrev }}
      <li class="page-item">
        <a class="page-link" href="{{ .p.Prev.URL }}" 
           rel="prev">Previous</a>
      </li>
    {{ else }}
      <li class="page-item disabled">
        <span class="page-link">Previous</span>
      </li>
    {{ end }}

    <!-- Indicator Number. -->
    <li class="page-item disabled">
      <span class="page-link">
        Page: {{ .p.PageNumber }} of {{ .p.TotalPages }}</span>
    </li>

    <!-- Next Page. -->
    {{ if .p.HasNext }}
      <li class="page-item">
        <a class="page-link" href="{{ .p.Next.URL }}" 
           rel="next">Next</a>
      </li>
    {{ else }}
      <li class="page-item disabled">
        <span class="page-link">Next</span>
      </li>
    {{ end }}

    <!-- Last Page. -->
    {{ if not (eq .p.PageNumber .p.TotalPages) }}
      <li class="page-item">
        <a class="page-link" href="{{ .p.Last.URL }}" 
           rel="last">Last</a>
      </li>
    {{ else }}
      <li class="page-item disabled">
        <span class="page-link">Last</span>
      </li>
    {{ end }}
  </ul>
  {{ end }}

</nav>
{{< / highlight >}}

-- -- --

### What is Next ?

Just like this adorable kitten, you may feel excited.
Consider resume our tutorial.

![adorable kitten][image-kitten]

There are, some interesting topic about <code>Pagination in Hugo</code>.
Consider continue reading [ [Hugo - Pagination - Number][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2018/11/18/hugo-pagination-number/
[image-kitten]:             {{< baseurl >}}/assets/site/images/cats/rambu-03.jpg

[image-ss-02-combined-animation]: {{< assets-ssg >}}/2018/11/52-pagination-combined.gif

[image-ss-02-simple-first-last]: {{< assets-ssg >}}/2018/11/52-simple-first-last.png
[image-ss-02-simple-indicator]:  {{< assets-ssg >}}/2018/11/52-simple-indicator-number.png
[image-ss-02-simple-prev-next]:  {{< assets-ssg >}}/2018/11/52-simple-prev-next.png

[tutor-hugo-config]:             {{< tutor-hugo-bootstrap >}}/config.toml
[tutor-hugo-layouts-list]:       {{< tutor-hugo-bootstrap >}}/themes/tutor-05/layouts/archives/list.html
[tutor-hugo-layouts-simple]:     {{< tutor-hugo-bootstrap >}}/themes/tutor-05/layouts/partials/pagination-simple.html
