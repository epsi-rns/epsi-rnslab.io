---
type   : post
title  : "Hugo - Pagination - Number"
date   : 2018-11-18T09:17:35+07:00
slug   : hugo-pagination-number
categories: [ssg, frontend]
tags      : [hugo, navigation, bootstrap, sass]
keywords  : [static site, custom theme, pagination, numbering]
author : epsi
opengraph:
  image: assets/site/images/topics/hugo-bootstrap.png

toc    : "toc-2018-09-hugo-bootstrap-step"

excerpt:
  Building Hugo Step by step, with Bootstrap as stylesheet frontend.
  Pagination for dummies, list all of page.

---

### Preface

> Goal: Pagination using Number.

-- -- --

### 1: Prepare

Consider use a new partial named <code>pagination-number.html</code>,
in our previous <code>list.html</code>

{{< highlight twig >}}
  {{ $paginator := .Paginate (where .Site.Pages "Type" "post") }}
  {{ partial "pagination-number.html" (dict "p" $paginator) }}
{{< / highlight >}}

#### Layout: List

The code would looks similar as below:

* <code>themes/tutor-05/layouts/archives/list.html</code>
  : [gitlab.com/.../layouts/archives/list.html][tutor-hugo-layouts-list].

{{< highlight twig >}}
  {{ $paginator := .Paginate (where .Site.Pages "Type" "post") }}
  {{ partial "pagination-number.html" (dict "p" $paginator) }}
  <section id="archive">
  <div class="post-list">
    {{ range $paginator.Pages }}
      {{ partial "summary-blog-list.html" . }}
    {{ end }}
  </div>
  </section>
{{< / highlight >}}


-- -- --

### 2: Preview: General

This is what we want to achieve in this tutorial.

![Hugo Pagination: Stylesheet Before After][image-ss-02-scss-before-after]

#### HTML Preview

The HTML that we want to achieve in this article, is similar as below.

{{< highlight html >}}
<ul class="pagination justify-content-center">
  <li class="page-item blog_previous disabled">
    <span class="page-link">«</span>
  </li>
  <li class="page-item  active">
    <span class="page-link">1</span>
  </li>
  <li class="page-item ">
    <a href="/pages/page/2/" class="page-link">2</a>      
  </li>
  <li class="page-item ">
    <a href="/pages/page/3/" class="page-link">3</a>
  </li>
  <li class="page-item blog_next">
    <a class="page-link" href="/pages/page/2/" rel="next">»</a>
  </li>
</ul>
{{< / highlight >}}

We will achieve this with Hugo code.

-- -- --

### 3: Navigation: Number

Pagination by number is also simple.

#### Partial: Pagination Number

* <code>themes/tutor-05/layouts/partials/pagination-number.html</code>
 : [gitlab.com/.../partials/pagination-number.html][tutor-hugo-layouts-number].

{{< highlight twig >}}
<nav aria-label="Page navigation">

  {{ if gt .p.TotalPages 1 }}
  <ul class="pagination justify-content-center">

    <!-- Page numbers. -->
    {{ $pagenumber := .p.PageNumber }}

    {{ range .p.Pagers }}
    <li class="page-item {{ if eq $pagenumber .PageNumber }} active{{ end }}">
      {{ if not (eq $pagenumber .PageNumber) }} 
      <a href="{{ .URL }}" class="page-link">
        {{ .PageNumber }}
      </a>
      {{ else }}
      <span class="page-link>
        {{ .PageNumber }}
      </span>
      {{ end }}
    </li>
    {{ end }}

  </ul>
  {{ end }}

</nav>
{{< / highlight >}}

#### Browser: Pagination Preview

![Hugo Pagination: Number List][image-ss-02-number-list]

#### How does it works ?

Just a simple loop.

{{< highlight twig >}}
  {{ range .p.Pagers }}
    {{ .PageNumber }}
  {{ end }}
{{< / highlight >}}

-- -- --

### 4: Navigation: Previous and Next

Consider give it a prev next button.

* <code>themes/tutor-05/layouts/partials/pagination-number.html</code>
 : [gitlab.com/.../partials/pagination-number.html][tutor-hugo-layouts-number].

{{< highlight twig >}}
<nav aria-label="Page navigation">

  {{ if gt .p.TotalPages 1 }}
  <ul class="pagination justify-content-center">

    <!-- Previous Page. -->
    {{ if .p.HasPrev }}
      <li class="page-item blog_previous">
        <a class="page-link" href="{{ .p.Prev.URL }}" rel="prev">&laquo;</a>
      </li>
    {{ else }}
      <li class="page-item blog_previous disabled">
        <span class="page-link">&laquo;</span>
      </li>
    {{ end }}

    ...

    <!-- Next Page. -->
    {{ if .p.HasNext }}
      <li class="page-item blog_next">
        <a class="page-link" href="{{ .p.Next.URL }}" rel="next">&raquo;</a>
      </li>
    {{ else }}
      <li class="page-item blog_next disabled">
        <span class="page-link">&raquo;</span>
      </li>
    {{ end }}
  </ul>
  {{ end }}

</nav>
{{< / highlight >}}

#### Browser: Pagination Preview

![Hugo Pagination: Symbol Previous Next][image-ss-02-number-laquo-raquo]

#### Changes

That code above is using:

* symbol <code>&laquo;</code> instead of <code>previous</code> word.

* symbol <code>&raquo;</code> instead of <code>next</code> word.

Notice that, we also have additional stylesheet named 

* blog_previous, and 

* blog_next

-- -- --

### 5: Stylesheet: Before and After

Consider define these pagination classes:

* <code>themes/tutor-05/sass/css/_pagination.scss</code>
  : [gitlab.com/.../sass/_pagination.scss][tutor-hugo-sass-pagination].

{{< highlight scss >}}
.blog_previous {
  span:after,
  a:after {
    content: " previous"
  }
}

.blog_next {
  span:before,
  a:before {
    content: "next "
  }
}
{{< / highlight >}}

And add the new scss in main file.

* <code>themes/tutor-05/sass/css/main.scss</code>
  : [gitlab.com/.../sass/main.scss][tutor-hugo-sass-main].

{{< highlight scss >}}
@import
  ...

  // custom
    "layout",
    "decoration",
    "list",
    "pagination"
;
{{< / highlight >}}

#### Browser: Pagination Preview

![Hugo Pagination: Stylesheet Before After][image-ss-02-scss-before-after]

-- -- --

### 6: Stylesheet: Responsive

#### Pure CSS

You can make this responsive by using standard css

{{< highlight scss >}}
@media only screen and (min-width: 768px) {
  ...
}
{{< / highlight >}}

* <code>themes/tutor-05/sass/css/_pagination.scss</code>
  : [gitlab.com/.../sass/_pagination.scss][tutor-hugo-sass-pagination].

{{< highlight scss >}}
@media only screen and (min-width: 768px) {

  .blog_previous {
    span:after,
    a:after {
      content: " previous"
    }
  }

  .blog_next {
    span:before,
    a:before {
      content: "next "
    }
  }

}
{{< / highlight >}}

#### Bootstrap Mixins

By the help of mixins/breakpoints,
we can rewrite it in bootstrap style.

{{< highlight scss >}}
@include media-breakpoint-up(md) {
  ...
}
{{< / highlight >}}

* <code>themes/tutor-05/sass/css/_pagination.scss</code>
  : [gitlab.com/.../sass/_pagination.scss][tutor-hugo-sass-pagination].

{{< highlight scss >}}
@include media-breakpoint-up(md) {

  .blog_previous {
    span:after,
    a:after {
      content: " previous"
    }
  }

  .blog_next {
    span:before,
    a:before {
      content: "next "
    }
  }

}
{{< / highlight >}}

Do not forget to add mixins/breakpoints.
So we have the complete main.scss as below.

* <code>themes/tutor-05/sass/css/main.scss</code>
  : [gitlab.com/.../sass/main.scss][tutor-hugo-sass-main].

{{< highlight scss >}}
@import
  // taken from bootstrap
    "sticky-footer-navbar",
    "blog",
    "bootstrap-custom",

  // variables
    "bootstrap/functions",
    "variables",
    "bootstrap/variables",
    "bootstrap/mixins/breakpoints",

  // custom
    "layout",
    "decoration",
    "list",
    "pagination"
;
{{< / highlight >}}

-- -- --

### 7: Math: Algebra

#### Assumption

Consider an example, a blog post contain **ten** posts.

{{< highlight conf >}}
# CONST

$totalPost   = 10
{{< / highlight >}}

#### Equation

And change the number of page for each pagination,
by number for example:
<code>1</code>, <code>2</code>, <code>5</code>, 
<code>7</code>, and <code>10</code>.

{{< highlight twig >}}
  {{ $paginator := .Paginate (where .Site.Pages "Type" "post") 2 }}
  {{ partial "pagination-number.html" (dict "p" $paginator) }}
{{< / highlight >}}

Do not worry!
<code>$paginator</code> do the math internally.

#### Table

We can also achieve <code>$totalPages</code> by **ceiling division**.
And the result is on this table below.

{{< highlight conf >}}
# ALGEBRA

+--------------+-------+-------+-------+-------+-------+
| $pagination  |   1   |   2   |   5   |   7   |  10   |
+--------------+-------+-------+-------+-------+-------+
| VARIABLE                                             |
| $division    | 10.0  |  5.0  |  2.0  |  1.4  |   1   |
| $totalPages  |  10   |   5   |   2   |   2   |  N/A  |
+--------------+-------+-------+-------+-------+-------+
{{< / highlight >}}

Of course, we do not need to show any pagination,
if there is only one page for all result.
That is why we can optionally,
convert <code>1</code> into <code>N/A</code>.

-- -- --

### What is Next ?

Do not get confused yet.
Keep calm, just like this cute kitten.
Our pagination tutorial still have some materials to go.

![adorable kitten][image-kitten]

There are, some interesting topic about <code>Pagination in Hugo</code>.
Consider continue reading [ [Hugo - Pagination - Adjacent][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2018/11/19/hugo-pagination-adjacent/
[image-kitten]:             {{< baseurl >}}/assets/site/images/cats/rambu-04.jpg

[image-ss-02-scss-before-after]:  {{< assets-ssg >}}/2018/11/52-number-before-after.png
[image-ss-02-number-laquo-raquo]: {{< assets-ssg >}}/2018/11/52-number-laquo-raquo.png
[image-ss-02-number-list]:        {{< assets-ssg >}}/2018/11/52-number-list.png

[tutor-hugo-layouts-list]:       {{< tutor-hugo-bootstrap >}}/themes/tutor-05/layouts/archives/list.html
[tutor-hugo-layouts-number]:     {{< tutor-hugo-bootstrap >}}/themes/tutor-05/layouts/partials/pagination-number.html

[tutor-hugo-sass-main]:          {{< tutor-hugo-bootstrap >}}/themes/tutor-05/sass/css/main.scss
[tutor-hugo-sass-pagination]:    {{< tutor-hugo-bootstrap >}}/themes/tutor-05/sass/css/_pagination.scss
