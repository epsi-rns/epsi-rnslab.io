---
type   : post
title  : "Hugo - SASS - Pagination - Responsive"
date   : 2018-11-21T09:17:35+07:00
slug   : hugo-pagination-responsive
categories: [ssg, frontend]
tags      : [hugo, navigation, bootstrap, sass]
keywords  : [static site, custom theme, pagination, responsive breakpoints]
author : epsi
opengraph:
  image: assets/site/images/topics/hugo-bootstrap.png

toc    : "toc-2018-09-hugo-bootstrap-step"

excerpt:
  Building Hugo Step by step, with Bootstrap as stylesheet frontend.
  Responsive pagination using Bootstrap. Not so for dummies.

---

### Preface

> Goal: Bringing responsive pagination, using mobile first.

While writing this CSS code,
I can't claim myself as a developer.
Because CSS is not a programming language.

But weird that, responsive design has their own logic.
So yeah, I have to code, a little.

-- -- --

### 1: Source

I respect copyright.
The code below inspired by:

* https://www.timble.net/blog/2015/05/better-pagination-for-jekyll/

* https://github.com/timble/jekyll-pagination

I made a slight modification.
But of course the logic remain the same.

-- -- --

### 2: Prepare

#### Preview: General

This is the complete version.

![Hugo Pagination: Responsive Animation][image-ss-02-responsive-animation]

#### Layout: List

As usual, change the script to use responsive partial.

{{< highlight twig >}}
  {{ $paginator := .Paginate (where .Site.Pages "Type" "post") }}
  {{ partial "pagination-responsive.html" (dict "p" $paginator "s" .Scratch) }}
{{< / highlight >}}

#### Partial: Minimal Pagination Code

You should have this artefact, before you begin.

* <code>themes/tutor-05/layouts/partials/pagination-responsive.html</code>
 : [gitlab.com/.../partials/pagination-responsive.html][tutor-hugo-layouts-responsive].

{{< highlight twig >}}
<nav aria-label="Page navigation">
  {{ $s := .s }}
  {{ $p := .p }}

  {{ if gt $p.TotalPages 1 }}
  <ul class="pagination justify-content-center">

  ...

  </ul>
  {{ end }}

</nav>
{{< / highlight >}}

#### SASS: Main

* <code>themes/tutor-05/sass/css/main.scss</code>
  : [gitlab.com/.../sass/main.scss][tutor-hugo-sass-main].

{{< highlight scss >}}
@import
  // taken from bootstrap
  // ...

  // variables
    "bootstrap/functions",
    "variables",
    "bootstrap/variables",
    "bootstrap/mixins/breakpoints",

  // custom
  // ...
    "pagination"
;
{{< / highlight >}}

#### SASS: Custom Pagination

I'm using Bootstrap 4 grid breakpoints.

* <code>themes/tutor-05/sass/css/_pagination.scss</code>
  : [gitlab.com/.../sass/_pagination.scss][tutor-hugo-sass-pagination].

{{< highlight css >}}
@include media-breakpoint-up(md) {
  ...
}
{{< / highlight >}}

-- -- --

### 3: Navigation: HTML Class

#### The Final Result.

Consider have a look at the image below.

![Hugo Pagination: Responsive 1][image-ss-02-responsive-01]

The HTML that we want to achieve is similar as below.

{{< highlight html >}}
  <ul class="pagination justify-content-center">
      <li class="page-item blog_previous">...</li>
      <li class="page-item first">...</li>
      <li class="pages-indicator first disabled">...</li>
      <li class="page-item pagination--offset-2">...</li>
      <li class="page-item pagination--offset-1">...</li>
      <li class="page-item pagination--offset-0 active ">...</li>
      <li class="page-item pagination--offset-1">...</li>
      <li class="page-item pagination--offset-2">...</li>
      <li class="pages-indicator last disabled">...</li>
      <li class="page-item last">...</li>
      <li class="page-item blog_next">...</li>
  </ul>
{{< / highlight >}}

#### Middle Pagination

All you need to care is, only these lines.

{{< highlight html >}}
      <li class="page-item pagination--offset-2">...</li>
      <li class="page-item pagination--offset-1">...</li>
      <li class="page-item pagination--offset-0 active ">...</li>
      <li class="page-item pagination--offset-1">...</li>
      <li class="page-item pagination--offset-2">...</li>
{{< / highlight >}}

Our short term goal is,
to put the <code>pagination--offset</code> class.

#### Partial: Pagination Code Skeleton

As usual, the skeleton, to show the complexity.

* <code>themes/tutor-05/layouts/partials/pagination-responsive.html</code>
 : [gitlab.com/.../partials/pagination-responsive.html][tutor-hugo-layouts-responsive].

{{< highlight twig >}}
<nav aria-label="Page navigation">
  {{ $s := .s }}
  {{ $p := .p }}

  {{ if gt $p.TotalPages 1 }}
  <ul class="pagination justify-content-center">

    <!-- Variable Initialization. -->

    <!-- Previous Page. -->
    <!-- First Page. -->
    <!-- Early (More Pages) Indicator. -->

    {{- range $p.Pagers -}}
      {{ $s.Set "page_number_flag" false }}
      {{ $s.Set "page_offset" false }}

      <!-- Complex page numbers. -->
      {{ if gt $p.TotalPages $max_links }}
        <!-- Lower limit pages. -->
        <!-- Upper limit pages. -->
        <!-- Middle pages. -->

      <!-- Simple page numbers. -->
      {{ else }}.
        ...
      {{ end }}
      
      <!-- Calculate Offset Class. -->

      <!-- Show Pager. -->
      {{- if eq ($s.Get "page_number_flag") true -}}
      ...
      {{- end -}}
    {{ end }}

    <!-- Late (More Pages) Indicator. -->
    <!-- Last Page. -->
    <!-- Next Page. -->

  </ul>
  {{ end }}

</nav>
{{< / highlight >}}

Notice the new variable called <code>page_offset</code>.

#### Partial: Pagination Code: Long Version

Before you begin, consider copy-and-paste from our last tutorial.

* <code>themes/tutor-05/layouts/partials/pagination-indicator.html</code>
 : [gitlab.com/.../partials/pagination-indicator.html][tutor-hugo-layouts-indicator].

#### Calculate Offset Value

It is just a matter of difference, between current page and pager.

{{< highlight twig >}}
      <!-- Calculate Offset Class. -->
      {{- if eq ($s.Get "page_number_flag") true -}}
          {{ $s.Set "page_offset" (sub .PageNumber $p.PageNumber) }}
      {{- end -}}
{{< / highlight >}}

#### Calculate Offset Class

We require to calculate absolute value, turn the negative value to positive.
The issue is that Hugo does not come with <code>abs()</code> function,
so the next part is a little bit cryptic.

{{< highlight twig >}}
      <!-- Calculate Offset Class. -->
      {{- if eq ($s.Get "page_number_flag") true -}}
          {{ $s.Set "page_offset" (sub .PageNumber $p.PageNumber) }}

          {{ $s.Set "page_offset_class" "" }}
          {{- if ge ($s.Get "page_offset") 0 -}}
              {{ $s.Set "page_offset_class" (print "pagination--offset-" ($s.Get "page_offset") ) }}
          {{- else -}}
              {{ $s.Set "page_offset_class" (print "pagination--offset" ($s.Get "page_offset") ) }}
          {{- end -}}
      {{- end -}}
{{< / highlight >}}

#### Using Offset Class

All we need is just adding the offset class.

{{< highlight twig >}}
      <li class="page-item {{ $s.Get "page_offset_class" }}">
        ...
      </li>
{{< / highlight >}}

The real code, is also not very simple.

{{< highlight twig >}}
      <!-- Show Pager. -->
      {{- if eq ($s.Get "page_number_flag") true -}}
      <li class="page-item{{ if eq $pagenumber .PageNumber }} active{{ end }} {{ $s.Get "page_offset_class" }}">
        {{ if not (eq $pagenumber .PageNumber) }} 
          <a href="{{ .URL }}" class="page-link">{{ .PageNumber }}</a>
        {{ else }}
          <span class="page-link page-item">{{ .PageNumber }}</span>
        {{ end }}
      </li>
      {{- end -}}
{{< / highlight >}}

#### Combined Code

{{< highlight twig >}}
      {{- if eq ($s.Get "page_number_flag") true -}}
      <!-- Calculate Offset Class. -->
        {{ $s.Set "page_offset" (sub .PageNumber $p.PageNumber) }}

        {{ $s.Set "page_offset_class" "" }}
        {{- if ge ($s.Get "page_offset") 0 -}}
          {{ $s.Set "page_offset_class" (print "pagination--offset-" ($s.Get "page_offset") ) }}
        {{- else -}}
          {{ $s.Set "page_offset_class" (print "pagination--offset" ($s.Get "page_offset") ) }}
        {{- end -}}

      <!-- Show Pager. -->
      <li class="page-item{{ if eq $pagenumber .PageNumber }} active{{ end }} {{ $s.Get "page_offset_class" }}">
        {{ if not (eq $pagenumber .PageNumber) }} 
          <a href="{{ .URL }}" class="page-link">{{ .PageNumber }}</a>
        {{ else }}
          <span class="page-link page-item">{{ .PageNumber }}</span>
        {{ end }}
      </li>
      {{- end -}}
{{< / highlight >}}

That is all.
Now that the HTML part is ready, we should go on,
by setting up the responsive breakpoints using SCSS.

-- -- --

### 4: Responsive: Breakpoints

Responsive is easy if you understand the logic.

> It is all about breakpoints.

#### Preview: Each Breakpoint

Consider again, have a look at the animation above, frame by frame.
We have at least five breakpoint as six figures below:

![Hugo Pagination: Responsive 1][image-ss-02-responsive-01]

![Hugo Pagination: Responsive 2][image-ss-02-responsive-02]

![Hugo Pagination: Responsive 3][image-ss-02-responsive-03]

![Hugo Pagination: Responsive 4][image-ss-02-responsive-04]

![Hugo Pagination: Responsive 5][image-ss-02-responsive-05]

![Hugo Pagination: Responsive 6][image-ss-02-responsive-06]

#### SASS: Bootstrap Grid Breakpoint Variables.

Bootstrap 4 grid breakpoints are defined as below.

* <code>themes/tutor-05/sass/bootstrap/_variables.scss</code>

{{< highlight scss >}}
// Grid breakpoints
//
// Define the minimum dimensions at which your layout will change,
// adapting to different screen sizes, for use in media queries.

$grid-breakpoints: (
  xs: 0,
  sm: 576px,
  md: 768px,
  lg: 992px,
  xl: 1200px
) !default;
{{< / highlight >}}

#### SASS: Bootstrap Breakpoint Skeleton

With breakpoint above, we can setup css skeleton, with empty css rules.

{{< highlight scss >}}
ul.pagination {
  @include media-breakpoint-up(xs) {}  
  @include media-breakpoint-up(sm) {}
  @include media-breakpoint-up(md) {}
  @include media-breakpoint-up(lg) {}
  @include media-breakpoint-up(xl) {}
}
{{< / highlight >}}

#### SASS: Using Bootstrap Breakpoint: Simple

We can fill any rules, as below:

{{< highlight scss >}}
@include media-breakpoint-up(md) {

  .blog_previous {
    span.page-link:after,
    a.page-link:after {
      content: " previous"
    }
  }

  .blog_next {
    span.page-link:before,
    a.page-link:before {
      content: "next "
    }
  }

}
{{< / highlight >}}

#### SASS: Using Bootstrap Breakpoint: Pagination Offset

We can fill any rules, inside <code>pagination</code> class as below:

{{< highlight scss >}}
ul.pagination {

  li.pagination--offset-1,
  li.pagination--offset-2,
  li.pagination--offset-3,
  li.pagination--offset-4,
  li.pagination--offset-5,
  li.pagination--offset-6,
  li.pagination--offset-7 {
    display: none;
  }

  @include media-breakpoint-up(xs) {
  }
  
  @include media-breakpoint-up(sm) {
    li.pagination--offset-1,
    li.pagination--offset-2 {
      display: inline-block;
    }
  }

  @include media-breakpoint-up(md) {
    li.pagination--offset-3,
    li.pagination--offset-4 {
      display: inline-block;
    }
  }

  @include media-breakpoint-up(lg) {
    li.pagination--offset-5,
    li.pagination--offset-6,
    li.pagination--offset-7 {
      display: inline-block;
    }
  }

  @include media-breakpoint-up(xl) {
  }

}
{{< / highlight >}}

#### SASS: Responsive Indicator

You can also add CSS rules for indicator.

{{< highlight scss >}}
ul.pagination {

  li.first,
  li.last,
  li.pages-indicator {
    display: none;
  }

  @include media-breakpoint-up(xs) {
  }
  
  @include media-breakpoint-up(sm) {
    li.pages-indicator {
      display: inline-block;
    }
  }

  @include media-breakpoint-up(md) {
    li.first,
    li.last {
      display: inline-block;
    }
  }

  @include media-breakpoint-up(lg) {
  }

  @include media-breakpoint-up(xl) {
  }

}
{{< / highlight >}}

#### SASS: Enhanced Bootstrap Breakpoint

If you desire smoother transition effect,
you can create your own breakpoint, beyond bootstrap.

{{< highlight scss >}}
$grid-breakpoints-custom: (
  xs:  0,
  xs2: 320px,
  xs3: 400px,
  xs4: 480px,
  sm:  576px,
  sm2: 600px,
  md:  768px,
  lg:  992px,
  xl:  1200px
) !default;
{{< / highlight >}}

And later, add css rule as code below:

{{< highlight scss >}}
ul.pagination {
  @include media-breakpoint-up(xs2, $grid-breakpoints-custom) {
    li.pages-indicator {
      display: inline-block;
    }
  }
}
{{< / highlight >}}

#### SASS: Complete Code

Now you can have the complete code as below:

* <code>themes/tutor-05/sass/css/_pagination.scss</code>
  : [gitlab.com/.../sass/_pagination.scss][tutor-hugo-sass-pagination].

{{< highlight scss >}}
@include media-breakpoint-up(md) {

  .blog_previous {
    span.page-link:after,
    a.page-link:after {
      content: " previous"
    }
  }

  .blog_next {
    span.page-link:before,
    a.page-link:before {
      content: "next "
    }
  }

}

$grid-breakpoints-custom: (
  xs:  0,
  xs2: 320px,
  xs3: 400px,
  xs4: 480px,
  sm:  576px,
  sm2: 600px,
  md:  768px,
  lg:  992px,
  xl:  1200px
) !default;

ul.pagination {

  li.first,
  li.last,
  li.pages-indicator {
    display: none;
  }

  li.pagination--offset-1,
  li.pagination--offset-2,
  li.pagination--offset-3,
  li.pagination--offset-4,
  li.pagination--offset-5,
  li.pagination--offset-6,
  li.pagination--offset-7 {
    display: none;
  }

  @include media-breakpoint-up(xs, $grid-breakpoints-custom) {
  }
  
  @include media-breakpoint-up(xs2, $grid-breakpoints-custom) {
    li.pages-indicator {
      display: inline-block;
    }
  }

  @include media-breakpoint-up(xs3, $grid-breakpoints-custom) {
    li.pagination--offset-1 {
      display: inline-block;
    }
  }
  
  @include media-breakpoint-up(xs4, $grid-breakpoints-custom) {
    li.pagination--offset-2 {
      display: inline-block;
    }
  }

  @include media-breakpoint-up(sm, $grid-breakpoints-custom) {
    li.first,
    li.last,
    li.pagination--offset-3 {
      display: inline-block;
    }
  }
  
  @include media-breakpoint-up(sm2, $grid-breakpoints-custom) {
    li.pagination--offset-4 {
      display: inline-block;
    }
  }

  @include media-breakpoint-up(md, $grid-breakpoints-custom) {
    li.pagination--offset-5,
    li.pagination--offset-6 {
      display: inline-block;
    }
  }

  @include media-breakpoint-up(lg, $grid-breakpoints-custom) {
    li.pagination--offset-7 {
      display: inline-block;
    }
  }

  @include media-breakpoint-up(xl, $grid-breakpoints-custom) {
  }

}

{{< / highlight >}}

-- -- --

### 5: Summary

You can have a look at our complete code here:

* <code>themes/tutor-05/layouts/partials/pagination-responsive.html</code>
 : [gitlab.com/.../partials/pagination-responsive.html][tutor-hugo-layouts-responsive].

We will have to complete the code later, in the next article.
This has been a long article.
We still need a few changes, for screen reader.

-- -- --

### What is Next ?

Looks good right?
This kitten, is still with me.
Just like the kitten, do not get rest yet!
Our pagination tutorial still have some materials to go.

![adorable kitten][image-kitten]

There are, some interesting topic about <code>Pagination in Hugo</code>.
Consider continue reading [ [Hugo - Pagination - Screen Reader][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2018/11/22/hugo-pagination-screenreader/
[image-kitten]:             {{< baseurl >}}/assets/site/images/cats/rambu-01.jpg

[image-ss-02-responsive-animation]: {{< assets-ssg >}}/2018/11/52-pagination-responsive.gif

[image-ss-02-responsive-01]:        {{< assets-ssg >}}/2018/11/52-responsive-01.png
[image-ss-02-responsive-02]:        {{< assets-ssg >}}/2018/11/52-responsive-02.png
[image-ss-02-responsive-03]:        {{< assets-ssg >}}/2018/11/52-responsive-03.png
[image-ss-02-responsive-04]:        {{< assets-ssg >}}/2018/11/52-responsive-04.png
[image-ss-02-responsive-05]:        {{< assets-ssg >}}/2018/11/52-responsive-05.png
[image-ss-02-responsive-06]:        {{< assets-ssg >}}/2018/11/52-responsive-06.png

[tutor-hugo-layouts-list]:          {{< tutor-hugo-bootstrap >}}/themes/tutor-05/layouts/archives/list.html
[tutor-hugo-layouts-indicator]:     {{< tutor-hugo-bootstrap >}}/themes/tutor-05/layouts/partials/pagination-indicator.html
[tutor-hugo-layouts-responsive]:    {{< tutor-hugo-bootstrap >}}/themes/tutor-05/layouts/partials/pagination-responsive.html

[tutor-hugo-sass-main]:             {{< tutor-hugo-bootstrap >}}/themes/tutor-05/sass/css/main.scss
[tutor-hugo-sass-pagination]:       {{< tutor-hugo-bootstrap >}}/themes/tutor-05/sass/css/_pagination.scss
