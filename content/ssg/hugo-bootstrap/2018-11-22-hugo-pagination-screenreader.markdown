---
type   : post
title  : "Hugo - Pagination - Screen Reader"
date   : 2018-11-22T09:17:35+07:00
slug   : hugo-pagination-screenreader
categories: [ssg, frontend]
tags      : [hugo, navigation, bootstrap]
keywords  : [static site, custom theme, pagination, screen reader]
author : epsi
opengraph:
  image: assets/site/images/topics/hugo-bootstrap.png

toc    : "toc-2018-09-hugo-bootstrap-step"

excerpt:
  Building Hugo Step by step, with Bootstrap as stylesheet frontend.
  Accessability for pagination using Bootstrap.
  Complete the custom pagination.
  This the summary code for our pagination.

---

### Preface

> Goal: Bringing screen reader accessability in pagination.

-- -- --

### 1: Source

I just follow bootstrap guidance:

* https://getbootstrap.com/docs/4.0/components/pagination/

To hidden content visually,
you simply need to add <code>sr-only</code>.

{{< highlight twig >}}
<span class="sr-only">Hidden Content</span>
{{< / highlight >}}

This content can be read by screenreader.

-- -- --

### 2: Prepare

#### Preview: General

Nomore preview.
This is screen reader.

#### Layout: List

As usual, change the script to use responsive partial.

* <code>themes/tutor-05/layouts/archives/list.html</code>
  : [gitlab.com/.../layouts/archives/list.html][tutor-hugo-layouts-list].

{{< highlight twig >}}
  {{ $paginator := .Paginate (where .Site.Pages "Type" "post") }}
  {{ partial "pagination-screenreader.html" (dict "p" $paginator "s" .Scratch) }}
{{< / highlight >}}

-- -- --

### 3: Navigation: Previous and Next

Just a slight modification.

#### Navigation: Previous

{{< highlight html >}}
    {{ if $p.HasPrev }}
      <li class="page-item blog_previous">
        <a class="page-link" href="{{ $p.Prev.URL }}" 
           aria-label="Previous" rel="prev">
          <span aria-hidden="true">&laquo;</span>
          <span class="sr-only">Previous</span>
        </a>
      </li>
    {{ else }}
      <li class="page-item blog_previous disabled">
        <span class="page-link">&laquo;</span>
      </li>
    {{ end }}
{{< / highlight >}}

#### Navigation: Next

{{< highlight html >}}
    {{ if $p.HasNext }}
      <li class="page-item blog_next">
        <a class="page-link" href="{{ $p.Next.URL }}"
           aria-label="Next" rel="next">
          <span aria-hidden="true">&raquo;</span>
          <span class="sr-only">Next</span>
        </a>
      </li>
    {{ else }}
      <li class="page-item blog_next disabled">
        <span class="page-link">&raquo;</span>
      </li>
    {{ end }}
{{< / highlight >}}

-- -- --

### 4: Navigation: First and Last

Just a slight modification.

#### Navigation: First

{{< highlight html >}}
      {{ if gt (sub $p.PageNumber $adjacent_links) 1 }}
        <li class="page-item first">
          <a class="page-link" href="{{ $p.First.URL }}">
            <span class="sr-only">Page </span>1</a>
        </li>
      {{ end }}
{{< / highlight >}}

#### Navigation: Last

{{< highlight html >}}
      {{ if lt (add $p.PageNumber $adjacent_links) $p.TotalPages }}
        <li class="page-item last">
          <a class="page-link" href="{{ $p.Last.URL }}">
            <span class="sr-only">Page </span>{{ $p.TotalPages }}</a>
        </li>
      {{ end }}
{{< / highlight >}}

-- -- --

### 5: Middle Navigation: Number

{{< highlight html >}}
      <li class="page-item{{ if eq $pagenumber .PageNumber }} active{{ end }} {{ $s.Get "page_offset_class" }}">
        {{ if not (eq $pagenumber .PageNumber) }} 
          <a href="{{ .URL }}" class="page-link">
            <span class="sr-only">Page </span>{{ .PageNumber }}</a>
        {{ else }}
          <span class="page-link page-item">
            <span class="sr-only">Page </span>{{ .PageNumber }}</span>
        {{ end }}
      </li>
{{< / highlight >}}

-- -- --

### 6: Conclusion

As a summary of this pagination tutorial,
the complete code is here below:

* <code>themes/tutor-05/layouts/partials/pagination-screenreader.html</code>
 : [gitlab.com/.../partials/pagination-screenreader.html][tutor-hugo-layouts-sr-only].

{{< highlight twig >}}
<nav aria-label="Page navigation">
  {{ $s := .s }}
  {{ $p := .p }}

  {{ if gt $p.TotalPages 1 }}
  <ul class="pagination justify-content-center">

    <!-- Page numbers. -->
    {{- $pagenumber := $p.PageNumber -}}

    <!-- Number of links either side of the current page. -->
    {{ $adjacent_links := 2 }}

    <!-- $max_links = ($adjacent_links * 2) + 1 -->
    {{ $max_links := (add (mul $adjacent_links 2) 1) }}

    <!-- $lower_limit = 1 + $adjacent_links -->
    {{ $lower_limit := (add 1 $adjacent_links) }}

    <!-- $upper_limit = $paginator.TotalPages - $adjacent_links -->
    {{ $upper_limit := (sub $p.TotalPages $adjacent_links) }}

    <!-- Previous Page. -->
    {{ if $p.HasPrev }}
      <li class="page-item blog_previous">
        <a class="page-link" href="{{ $p.Prev.URL }}" 
           aria-label="Previous" rel="prev">
          <span aria-hidden="true">&laquo;</span>
          <span class="sr-only">Previous</span>
        </a>
      </li>
    {{ else }}
      <li class="page-item blog_previous disabled">
        <span class="page-link">&laquo;</span>
      </li>
    {{ end }}

    {{ if gt $p.TotalPages $max_links }}
      <!-- First Page. -->
      {{ if gt (sub $p.PageNumber $adjacent_links) 1 }}
        <li class="page-item first">
          <a class="page-link" href="{{ $p.First.URL }}">
            <span class="sr-only">Page </span>1</a>
        </li>
      {{ end }}

      <!-- Early (More Pages) Indicator. -->
      {{ if gt (sub $p.PageNumber $adjacent_links) 2 }}
        <li class="pages-indicator first disabled">
          <span class="page-link">...</span>
        </li>
      {{ end }}
    {{ end }}

    {{- range $p.Pagers -}}
      {{ $s.Set "page_number_flag" false }}
      {{ $s.Set "page_offset" false }}

      <!-- Complex page numbers. -->
      {{ if gt $p.TotalPages $max_links }}

        <!-- Lower limit pages. -->
        <!-- If the user is on a page which is in the lower limit.  -->
        {{ if le $p.PageNumber $lower_limit }}

          <!-- If the current loop page is less than max_links. -->
          {{ if le .PageNumber $max_links }}
            {{ $s.Set "page_number_flag" true }}
          {{ end }}

        <!-- Upper limit pages. -->
        <!-- If the user is on a page which is in the upper limit. -->
        {{ else if ge $p.PageNumber $upper_limit }}

          <!-- If the current loop page is greater than total pages minus $max_links -->
          {{ if gt .PageNumber (sub .TotalPages $max_links) }}
            {{ $s.Set "page_number_flag" true }}
          {{ end }}

        <!-- Middle pages. -->
        {{ else }}
          
          {{ if and ( ge .PageNumber (sub $p.PageNumber $adjacent_links) ) ( le .PageNumber (add $p.PageNumber $adjacent_links) ) }}
            {{ $s.Set "page_number_flag" true }}
          {{ end }}

        {{ end }}

      <!-- Simple page numbers. -->
      {{ else }}

        {{ $s.Set "page_number_flag" true }}
      {{ end }}

      {{- if eq ($s.Get "page_number_flag") true -}}
      <!-- Calculate Offset Class. -->
        {{ $s.Set "page_offset" (sub .PageNumber $p.PageNumber) }}

        {{ $s.Set "page_offset_class" "" }}
        {{- if ge ($s.Get "page_offset") 0 -}}
          {{ $s.Set "page_offset_class" (print "pagination--offset-" ($s.Get "page_offset") ) }}
        {{- else -}}
          {{ $s.Set "page_offset_class" (print "pagination--offset" ($s.Get "page_offset") ) }}
        {{- end -}}

      <!-- Show Pager. -->
      <li class="page-item{{ if eq $pagenumber .PageNumber }} active{{ end }} {{ $s.Get "page_offset_class" }}">
        {{ if not (eq $pagenumber .PageNumber) }} 
          <a href="{{ .URL }}" class="page-link">
            <span class="sr-only">Page </span>{{ .PageNumber }}</a>
        {{ else }}
          <span class="page-link page-item">
            <span class="sr-only">Page </span>{{ .PageNumber }}</span>
        {{ end }}
      </li>
      {{- end -}}
    {{ end }}

    {{ if gt $p.TotalPages $max_links }}
      <!-- Late (More Pages) Indicator. -->
      {{ if lt (add $p.PageNumber $adjacent_links) (sub $p.TotalPages 1) }}
        <li class="pages-indicator last disabled">
          <span class="page-link">...</span>
        </li>
      {{ end }}

      <!-- Last Page. -->
      {{ if lt (add $p.PageNumber $adjacent_links) $p.TotalPages }}
        <li class="page-item last">
          <a class="page-link" href="{{ $p.Last.URL }}">
            <span class="sr-only">Page </span>{{ $p.TotalPages }}</a>
        </li>
      {{ end }}
    {{ end }}

    <!-- Next Page. -->
    {{ if $p.HasNext }}
      <li class="page-item blog_next">
        <a class="page-link" href="{{ $p.Next.URL }}"
           aria-label="Next" rel="next">
          <span aria-hidden="true">&raquo;</span>
          <span class="sr-only">Next</span>
        </a>
      </li>
    {{ else }}
      <li class="page-item blog_next disabled">
        <span class="page-link">&raquo;</span>
      </li>
    {{ end }}

  </ul>
  {{ end }}

</nav>
{{< / highlight >}}

Now the pagination tutorial is done.

I think this is all for now.

-- -- --

### What is Next ?

Feels like tired, after finishing pagination Tutorial ?
This kitten, need some sleep now..
Just like the kitten, you may need some rest too.
Our pagination tutorial is finished.
But you may continue to explore other challenging topic,
tomorrow, or right away.

![adorable kitten][image-kitten]

There are, some interesting topic for using <code>Bootstrap in Hugo Content</code>,
such as <code>Header, Footer, and Navigation</code>.
Consider continue reading [ [Hugo - Bootstrap - Blog Post][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2018/11/24/hugo-bootstrap-content/
[image-kitten]:             {{< baseurl >}}/assets/site/images/cats/rambu-02.jpg

[image-ss-02-responsive-animation]: {{< assets-ssg >}}/2018/11/52-pagination-responsive.gif

[image-ss-02-responsive-01]:        {{< assets-ssg >}}/2018/11/52-responsive-01.png
[image-ss-02-responsive-02]:        {{< assets-ssg >}}/2018/11/52-responsive-02.png
[image-ss-02-responsive-03]:        {{< assets-ssg >}}/2018/11/52-responsive-03.png
[image-ss-02-responsive-04]:        {{< assets-ssg >}}/2018/11/52-responsive-04.png
[image-ss-02-responsive-05]:        {{< assets-ssg >}}/2018/11/52-responsive-05.png
[image-ss-02-responsive-06]:        {{< assets-ssg >}}/2018/11/52-responsive-06.png

[tutor-hugo-layouts-list]:          {{< tutor-hugo-bootstrap >}}/themes/tutor-05/layouts/archives/list.html
[tutor-hugo-layouts-sr-only]:       {{< tutor-hugo-bootstrap >}}/themes/tutor-05/layouts/partials/pagination-screenreader.html
