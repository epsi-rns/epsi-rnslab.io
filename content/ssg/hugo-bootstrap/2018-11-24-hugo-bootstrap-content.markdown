---
type   : post
title  : "Hugo - Bootstrap - Blog Post"
date   : 2018-11-24T09:17:35+07:00
slug   : hugo-bootstrap-content
categories: [ssg, frontend]
tags      : [hugo, bootstrap, sass]
keywords  : [static site, custom theme, post header, post footer, post navigation, date time, javascript, timeago]
author : epsi
opengraph:
  image: assets/site/images/topics/hugo-bootstrap-markdown.png

toc    : "toc-2018-09-hugo-bootstrap-step"

excerpt:
  Building Hugo Step by step, with Bootstrap as stylesheet frontend.
  More about blog post content, Header, Footer, and Pagination.

---

### Preface

> Goal: More about blog post content, Header, Footer, and Pagination.

We can save a lot of stylesheet formatting,
by using Bootstrap, or any other CSS framework.

-- -- --

### 1: Prepare

This preparation is required.

#### Theme

To avoid messed-up with previous tutorial,
copy manually, with your file manager

1. From <code>themes/tutor-05/*</code> to <code>themes/tutor-06</code>.

2. Edit <code>config.toml</code>, and save.

{{< highlight toml >}}
$ cat config.toml
theme        = "tutor-06"
{{< / highlight >}}

#### Preview: General

Consider redesign the looks of blog post.
This is what we want to achieve in this tutorial.

![Hugo Content: All Artefacts][image-ss-01-blog-post-all]

#### Layouts: Single

Edit our ancient artefact <code>single.html</code>,
that is exist from the very beginning of our tutorial.

* <code>themes/tutor-06/layouts/post/single.html</code>
  : [gitlab.com/.../layouts/post/single.html][tutor-hugo-layouts-post-single]

{{< highlight twig >}}
{{ define "main" }}
  <main role="main" class="col-md-8">
    <div class="blog-main p-3 mb-3
                bg-light rounded border border-dark shadow-hover">
      <div class="blog-post" 
           itemscope itemtype="http://schema.org/BlogPosting">
        {{ partial "post-header.html" . }}

        <article class="main-content" itemprop="articleBody">
          {{ .Content }}
        </article>

        {{ partial "post-navigation.html" . }}
        {{ partial "post-footer.html" . }}
      </div><!-- /.blog-post -->
    </div><!-- /.blog-main -->
  </main>
{{ end }}

{{ define "aside" }}
  <aside class="col-md-4">   
    {{ partial "card-related-posts.html" . }}
  </aside>
{{ end }}
{{< / highlight >}}

Talking about schema, you can read this nice site:

* [schema.org/](http://schema.org/)

#### Partials: New Artefacts

The code above require three new partials.

Create three empty artefacts,

* <code>themes/tutor-06/layouts/partials/post-header.html</code>

* <code>themes/tutor-06/layouts/partials/post-footer.html</code>

* <code>themes/tutor-06/layouts/partials/post-navigation.html</code>

And one more artefact that is required in <code>post-header.html</code>.

* <code>themes/tutor-06/layouts/partials/post-time-elapsed.html</code>

-- -- --

### 2: Header

Consider begin with header.

#### Preview

![Hugo Content: Header][image-ss-01-blog-post-header]

#### Partials: Minimum

The minimum header is simply showing title.

* <code>themes/tutor-06/layouts/partials/post-header</code>
  : [gitlab.com/.../layouts/partials/post-header.html][tutor-hugo-partials-header]

{{< highlight html >}}
    <header class="rounded shadow border p-2 mb-4"> 
      <h2 itemprop="name headline">
          <a class="text-dark" href="{{ .Permalink }}"
          >{{ .Page.Title }}</a></h2>
    </header>
{{< / highlight >}}

#### Partials: Meta

Consider add **meta** data in post header.

* author

* time elapsed (using partial)

* tags

* categories

My actual code is using Awesome Font.
To reduce complexity, here I use bootstrap <code>badge</code>,
without the eye candy Awesome Font.

{{< highlight html >}}
    <header class="rounded shadow border p-2 mb-4"> 
      <h2 itemprop="name headline">
          <a class="text-dark" href="{{ .Permalink }}"
          >{{ .Page.Title }}</a></h2>

      <div class="clearfix"></div>
      <div class="container-fluid">
        {{ if .Params.author }}
        <span class="badge badge-primary mr-2">
            <span itemprop="author" itemscope itemtype="http://schema.org/Person">
            <span itemprop="name">{{ .Params.author }}</span></span>
        </span>
        {{ end }}  
        <div class="float-left">
            <span class="badge badge-secondary mr-2">
            {{ partial "post-time-elapsed.html" . }}
            </span>
        </div>
        <div class="float-right mr-2">
          {{ range .Params.tags }}
            <a href="{{ printf "tags/%s" . | absURL }}">
              <span class="badge badge-dark">{{ . }}</span></a>
          {{ end }}
        </div>
        <div class="float-right mr-2">
          {{ range .Params.categories }}
            <a href="{{ printf "categories/%s" . | absURL }}">
              <span class="badge badge-info">{{ . }}</span></a>
          {{ end }}
          &nbsp;
        </div>
      </div>
    </header>
{{< / highlight >}}

-- -- --

### 3: Elapsed Time

As it has been mentioned above, we need special partial for this.

#### Issue

As a static generator, Hugo compiled the content whenever there are any changes.
If there are no changes, the generated time remain static.
It means we cannot tell relative time in string such as **three days ago**,
because after a week without changes, the time remain **three days ago**,
not changing into **ten days ago**.

The solution is using javascript.
You can download the script from here

* [timeago.org/](https://timeago.org/)

That way the time ago is updated dynamically as time passes,
as opposed to having to rebuild Hugo every day.

Do not forget to put the script in static directory.

* <code>themes/tutor-06/static/js/timeago.min.js</code>

#### Partial

* <code>themes/tutor-06/layouts/partials/post-time-elapsed.html</code>
  : [gitlab.com/.../layouts/partials/post-time-elapsed.html][tutor-hugo-partials-time-elapsed]

{{< highlight javascript >}}
    <time datetime="{{ .Page.Date.Format "2006-01-02T15:04:05Z07:00" }}" 
          itemprop="datePublished">
    <span class="timeago" datetime="{{ .Page.Date.Format "2006-01-02 15:04:05" }}">
    </span></time>
    &nbsp; 

    <script src="{{ "js/timeago.min.js" | relURL }}"></script>
    <script type="text/javascript">
      var timeagoInstance = timeago();
      var nodes = document.querySelectorAll('.timeago');
      timeagoInstance.render(nodes, 'en_US');
      timeago.cancel();
      timeago.cancel(nodes[0]);
    </script>
{{< / highlight >}}

-- -- --

### 4: Footer

We already have Header.
Why do not we continue with footer?

#### Preview

![Hugo Content: Footer][image-ss-01-blog-post-footer]

#### Partials

* <code>themes/tutor-06/layouts/partials/post-footer.html</code>
  : [gitlab.com/.../layouts/partials/post-footer.html][tutor-hugo-partials-footer]

{{< highlight html >}}
    <footer class="rounded shadow border p-2 mb-4">
        <div class="d-flex flex-row justify-content-center align-items-center">

        <div class="p-2 text-center">
            <img src="/assets/site/images/license/cc-by-sa.png" 
                 class="bio-photo" 
                 height="31"
                 width="88"
                 alt="CC BY-SA 4.0"></a>
        </div>

        <div class="p-2">
            This article is licensed under:<br/>
            <a href="https://creativecommons.org/licenses/by-sa/4.0/deed" 
               class="text-dark">
                <b>Attribution-ShareAlike</b> 4.0 International (CC BY-SA 4.0)</a>
        </div>

        </div>
    </footer>
{{< / highlight >}}

#### Assets: License

I have collect some image related with **license**,
so that you can use license easily.

* <code>themes/tutor-06/assets/site/images/license/</code>
  : [gitlab.com/.../assets/site/images/license/][tutor-hugo-license]

-- -- --

### 5: Post Navigation

Each post also need simple navigation.

#### Preview

![Hugo Content: Navigation][image-ss-01-blog-post-nav]

#### Partials

* <code>themes/tutor-06/layouts/partials/post-navigation.html</code>
  : [gitlab.com/.../layouts/partials/post-navigation.html][tutor-hugo-partials-navigation]

{{< highlight twig >}}
  <ul class="pagination justify-content-between" 
      role="navigation" aria-labelledby="pagination-label">

      {{ if .Page.Next }}
      <li class="page-item previous">
        <a class="page-link" 
           href="{{ .Page.Next.URL | absURL }}" 
           title="{{ .Page.Next.Title }}">&laquo; </a>
      </li>
      {{ end }}

      {{ if .Page.Prev }}
      <li class="page-item next">
        <a class="page-link" 
           href="{{ .Page.Prev.URL | absURL }}" 
           title="{{ .Page.Prev.Title }}"> &raquo;</a>
      </li>
      {{ end }}

  </ul>
{{< / highlight >}}

#### SASS: Post Navigation

Code above require two more classes

* <code>previous</code>

* <code>next</code>

* <code>themes/tutor-06/sass/css/_post-navigation.scss</code>
  : [gitlab.com/.../sass/_post-navigation.scss][tutor-hugo-sass-navigation].

{{< highlight scss >}}
// -- -- -- -- --
// _post-navigation.scss

.previous {
  a.page-link:after {
    content: " previous"
  }
}

.next {
  a.page-link:before {
    content: "next "
  }
}
{{< / highlight >}}

#### SASS: Main

And add relevant stylesheet in <code>main.scss</code>.

* <code>themes/tutor-06/sass/css/main.scss</code>
  : [gitlab.com/.../sass/main.scss][tutor-hugo-sass-main].

{{< highlight scss >}}
@import
  ...

  // custom
    "layout",
    "decoration",
    "list",
    "pagination",
    "post-navigation"
;
{{< / highlight >}}

-- -- --

### 6: Conclusion

It is enough for now.
There are many part that can be enhanced, in about content area.

After all, it is about imagination.

-- -- --

### What is Next ?

There are, some interesting topic for using <code>Markdown in Hugo Content</code>,
such as using <code>shortcodes</code>.
Consider continue reading [ [Hugo - Content - Markdown][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2018/11/25/hugo-markdown-content/

[image-ss-01-blog-post-all]:     {{< assets-ssg >}}/2018/11/61-blog-post-all.png
[image-ss-01-blog-post-header]:  {{< assets-ssg >}}/2018/11/61-blog-post-header.png
[image-ss-01-blog-post-footer]:  {{< assets-ssg >}}/2018/11/61-blog-post-footer.png
[image-ss-01-blog-post-nav]:     {{< assets-ssg >}}/2018/11/61-blog-post-navigation.png

[tutor-hugo-layouts-post-single]:   {{< tutor-hugo-bootstrap >}}/themes/tutor-06/layouts/post/single.html
[tutor-hugo-partials-header]:       {{< tutor-hugo-bootstrap >}}/themes/tutor-06/layouts/partials/post-header.html
[tutor-hugo-partials-footer]:       {{< tutor-hugo-bootstrap >}}/themes/tutor-06/layouts/partials/post-footer.html
[tutor-hugo-partials-navigation]:   {{< tutor-hugo-bootstrap >}}/themes/tutor-06/layouts/partials/post-navigation.html
[tutor-hugo-partials-time-elapsed]: {{< tutor-hugo-bootstrap >}}/themes/tutor-06/layouts/partials/post-time-elapsed.html

[tutor-hugo-license]:            {{< tutor-hugo-bootstrap >}}/static/assets/site/images/license/
[tutor-hugo-sass-main]:          {{< tutor-hugo-bootstrap >}}/themes/tutor-06/sass/css/main.scss
[tutor-hugo-sass-navigation]:    {{< tutor-hugo-bootstrap >}}/themes/tutor-06/sass/css/_post-navigation.scss
