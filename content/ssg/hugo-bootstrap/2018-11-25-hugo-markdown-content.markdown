---
type   : post
title  : "Hugo - Content - Markdown"
date   : 2018-11-25T09:17:35+07:00
slug   : hugo-markdown-content
categories: [ssg, frontend]
tags      : [hugo, sass]
keywords  : [static site, custom theme, bootstrap, markdown, shortcode, responsive image]
author : epsi
opengraph:
  image: assets/site/images/topics/hugo-bootstrap-markdown.png

toc    : "toc-2018-09-hugo-bootstrap-step"

excerpt:
  Building Hugo Step by step, with Bootstrap as stylesheet frontend.
  The real markdown content.

---

### Preface

> Goal: Common use of markdown in Hugo content

There is not much to say about markdown.
Markdown is easy, however there might be a tricky issue.

#### Reading

* [daringfireball.net markdown syntax][link-daringfireball]

#### Why Markdown?

	More on Writing Content

While markup focus on document formatting,
markdown focus on document structure.

-- -- --

### 1: Split Resource

It is a good practice to split resource and content.

#### Common Structure

The common structure for content in Hugo are:

1. Frontmatter: YAML, TOML or JSON

2. Content: Markdown or HTML

Th combination is all up to you, your choice, to manage your content.

#### Real Life Structure

I think that easier just to live with this structure,
split the content, and the resource (link and image).

1. Frontmatter: YAML or TOML

2. Content: Markdown

3. Resource: Markdown

{{< highlight markdown >}}
+++
type       = "post"
+++

# Header 1

My content

[My Link][my-image]

-- -- --

## Header 1.1

My sub content

![My Image][mylink]

-- -- --

## Header 1.2

My other sub content

[//]: <> ( -- -- -- links below -- -- -- )

[my-image]:    http://some.site/an-image.jpg
[my-link]:     http://some.site/an-article.html
{{< / highlight >}}

Notice, that the markdown divided into two part by using 

{{< highlight markdown >}}
[//]: <> ()
{{< / highlight >}}

-- -- --

### 2: Shortcode: Link

#### Shortcode Issue

The issue is, Hugo does not allow any Chroma code in markdown content.
But Hugo can utilize shortcode in markdown content

* [gohugo.io shortcodes/](https://gohugo.io/content-management/shortcodes/)

#### BaseURL

Consider create shortcodes

* <code>themes/tutor-06/layouts/shortcodes/baseurl.html</code>.
  : [gitlab.com/.../layouts/shortcodes/baseurl.html][tutor-hugo-layouts-baseurl]

{{< highlight twig >}}
{{- .Page.Site.BaseURL -}}
{{< / highlight >}}

#### Local Link

Now we can use the <code>baseurl</code> shortcode,
to solve link issue.

{{< highlight markdown >}}
[local-whats-next]: {{ < baseurl > }}quotes/2018/09/07/julian-baker-something/
{{< / highlight >}}

And call the link later in markdown format.

{{< highlight markdown >}}
### What's next ?

Our next song would be [Julian Baker][local-whats-next]
{{< / highlight >}}

#### Outside Link

I often use shortcodes for reccuring link.
For example for my blog, I use dotfiles a lot.

Consider create shortcodes for <code>dotfiles</code>.

* <code>layouts/shortcodes/dotfiles.html</code>.
  : [gitlab.com/.../layouts/shortcodes/dotfiles.html][tutor-hugo-layouts-dotfiles]

{{< highlight twig >}}
{{- "https://gitlab.com/epsi-rns/dotfiles/tree/master" -}}
{{< / highlight >}}

Again, we can use the <code>baseurl</code> shortcode,
to solve recurring link issue.

{{< highlight markdown >}}
[link-dotfiles]:    {{ < dotfiles > }}/terminal/vimrc/vimrc.bandithijo
{{< / highlight >}}

And call the link later in markdown format.

{{< highlight markdown >}}
### Example Dotfiles

ViM RC: [bandithijo][link-dotfiles]
{{< / highlight >}}

-- -- --

### 3: Shortcode: Image

#### Prepare Image Directory

It is, a good practice to have a good structured directory.
Just put all your resources in <code>assets</code> directory.
And how you structure you image, javascript, vector and such,
is actually up to you.

I respect copyright, so I put my own old photographs, as an example.

* <code>static/assets/posts/2018/kiddo-007.jpg</code>
  : [gitlab.com/.../assets/posts/2018/kiddo-007.jpg][tutor-hugo-assets-kiddo]

![Hugo Content: Image Directory][image-ss-02-thunar-resource]

Other example might be as complex as:
using section, categories, year and month.

* <code>static/assets/posts/ssg/2018/11/thunar-03.png</code>

#### Image Link

Again, now we can use the <code>baseurl</code> shortcode,
to solve image link issue.

{{< highlight markdown >}}
[image-kiddo]:    {{ < baseurl > }}assets/posts/2018/kiddo-007.jpg
{{< / highlight >}}

And show image later in markdown format.

{{< highlight markdown >}}
![A Thinking Kiddo][image-kiddo]
{{< / highlight >}}

-- -- --

### 4: Responsive: Image

We are not finished yet.
We need the image to shrink the image,
so it always follow the size of the container.

#### Layouts: Single

Remember our previous <code>single.html</code>.

* <code>themes/tutor-06/layouts/post/single.html</code>
  : [gitlab.com/.../layouts/post/single.html][tutor-hugo-layouts-post-single]

{{< highlight twig >}}
        <article class="main-content" itemprop="articleBody">
          {{ .Content }}
        </article>
{{< / highlight >}}

We have already had a class called <code>main-content</code>.

#### SASS: Content

Now we can implement a bootstrap mixin in this content:

* <code>themes/tutor-06/sass/css/_content.scss</code>
  : [gitlab.com/.../sass/_content.scss][tutor-hugo-sass-content].

{{< highlight scss >}}
.main-content img {
    @include img-fluid;
}
{{< / highlight >}}

#### SASS: Main

{{< highlight scss >}}
  // variables
    "bootstrap/mixins/image",

  // custom
    "content"
{{< / highlight >}}

And the complete SASS is here

* <code>themes/tutor-06/sass/css/main.scss</code>
  : [gitlab.com/.../sass/main.scss][tutor-hugo-sass-main].

{{< highlight scss >}}
@import
  // taken from bootstrap
    "sticky-footer-navbar",
    "blog",
    "bootstrap-custom",

  // variables
    "bootstrap/functions",
    "variables",
    "bootstrap/variables",
    "bootstrap/mixins/breakpoints",
    "bootstrap/mixins/image",

  // custom
    "layout",
    "decoration",
    "list",
    "pagination",
    "post-navigation",
    "content"
;
{{< / highlight >}}

-- -- --

### 5: Summary

#### Example Content

Consider have a look at one of our song quotes, written in markdown.

* <code>content/quotes/john-mayer-slow-dancing-in-a-burning-room.md</code>
  : [gitlab.com/.../content/quotes/...room.md][tutor-hugo-content-dancing]

{{< highlight markdown >}}
+++
type       = "post"
title      = "John Mayer - Slow Dancing in a Burning Room"
date       = 2018-02-15T07:35:05+07:00
categories = ["lyric"]
tags       = ["rock", "2010s"]
slug       = "john-mayer-slow-dancing-in-a-burning-room"
author     = "epsi"
+++

We're going down.
And you can see it, too.

We're going down.
And you know that we're doomed.

My dear, we're slow dancing in a burning room.

![A Thinking Kiddo][image-kiddo]

-- -- --

### Example Dotfiles

ViM RC: [bandithijo][link-dotfiles]

-- -- --

### What's next ?

Our next song would be [Julian Baker][local-whats-next]

[//]: <> ( -- -- -- links below -- -- -- )

[image-kiddo]:      {{ < baseurl > }}assets/posts/2018/kiddo-007.jpg

[local-whats-next]: {{ < baseurl > }}quotes/2018/09/07/julian-baker-something/
[link-dotfiles]:    {{ < dotfiles > }}/terminal/vimrc/vimrc.bandithijo

{{< / highlight >}}

![Hugo Content: Shortcode Example: Link and Image][image-ss-02-kiddo-example]

-- -- --

### What is Next ?

There are, some interesting topic for using <code>HTML in Hugo Content</code>,
such as inserting <code>Table of Content</code>.
Consider continue reading [ [Hugo - Table of Content][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2018/11/26/hugo-html-content/
[link-daringfireball]:      https://daringfireball.net/projects/markdown/syntax

[image-ss-02-thunar-resource]:   {{< assets-ssg >}}/2018/11/62-thunar-resource.png
[image-ss-02-kiddo-example]:     {{< assets-ssg >}}/2018/11/62-kiddo-example.png

[tutor-hugo-layouts-post-single]:   {{< tutor-hugo-bootstrap >}}/themes/tutor-06/layouts/post/single.html
[tutor-hugo-sass-main]:          {{< tutor-hugo-bootstrap >}}/themes/tutor-06/sass/css/main.scss
[tutor-hugo-sass-content]:       {{< tutor-hugo-bootstrap >}}/themes/tutor-06/sass/css/_content.scss
[tutor-hugo-layouts-baseurl]:    {{< tutor-hugo-bootstrap >}}/themes/tutor-06/layouts/shortcodes/baseurl.html
[tutor-hugo-layouts-dotfiles]:   {{< tutor-hugo-bootstrap >}}/layouts/shortcodes/dotfiles.html
[tutor-hugo-assets-kiddo]:       {{< tutor-hugo-bootstrap >}}/static/assets/posts/2018/kiddo-007.jpg

[tutor-hugo-content-dancing]:    {{< tutor-hugo-bootstrap >}}/content/quotes/john-mayer-slow-dancing-in-a-burning-room.md
