---
type   : post
title  : "Hugo - Content - Raw HTML"
date   : 2018-11-26T09:17:35+07:00
slug   : hugo-html-content
categories: [ssg, frontend]
tags      : [hugo, bootstrap, sass]
keywords  : [static site, custom theme, raw html, table of content]
author : epsi
opengraph:
  image: assets/site/images/topics/hugo-bootstrap-markdown.png

toc    : "toc-2018-09-hugo-bootstrap-step"

excerpt:
  Building Hugo Step by step, with Bootstrap as stylesheet frontend.
  Inserting Raw HTML content. Before, after, and in the middle.

---

### Preface

> Goal: Inserting Raw HTML before content.

Sometimes we need to add raw HTML content.
We can do it before or after, the content.
And in the middle of content using shortcodes.

-- -- --

### 1: Before Content: Table of Content

We are going to insert TOC, before the content.

#### Preview

This is what we want to achieve.

![Hugo Content: Table of Content: Card][image-ss-02-toc-card]

#### Raw HTML

I'm using bootstrap card. This below is our raw HTML.

* layouts/partials/toc-2014-03-springsteen.html
  : [gitlab.com/.../layouts/partials/toc-2014-03-springsteen.html][tutor-hugo-layouts-toc]

{{< highlight html >}}
<div class="card">
  <div class="card-header bg-dark text-light">
    <p class="card-title float-left">Table of Content</p>
    <div class="clearfix"></div>
  </div>
  <div class="card-body">

  <p>Springsteen Related Articles.</p>
  <ul>
    <li><a href="{{ "/quotes/2014/03/25/bruce-springsteen-one-step-up/" | relURL }}"
          >Bruce Springsteen - One Step Up</a></li>

    <li><a href="{{ "/quotes/2014/03/15/bruce-springsteen-the-river/" | relURL }}"
          >Bruce Springsteen - The River</a></li>
  </ul>
  </div>
</div>
{{< / highlight >}}

You can use any custom design that you like,
instead of that bootstrap card above.

Notice the filename <code>toc-2014-03-springsteen</code>.
We are going to use that later.

#### Layouts: Single

Edit our previous <code>single.html</code>.

Add code below, right before the content.

{{< highlight twig >}}
          {{- with .Page.Params.toc -}}
          {{ partial (print . ".html") . }}
          <br/>
          {{- end -}}
{{< / highlight >}}

This is the complete artefact.

* <code>themes/tutor-06/layouts/post/single.html</code>
  : [gitlab.com/.../layouts/post/single.html][tutor-hugo-layouts-post-single]

{{< highlight twig >}}
{{ define "main" }}
  <main role="main" class="col-md-8">
    <div class="blog-main p-3 mb-3
                bg-light rounded border border-dark shadow-hover">
      <div class="blog-post" 
           itemscope itemtype="http://schema.org/BlogPosting">
        {{ partial "post-header.html" . }}

        <article class="main-content" itemprop="articleBody">
          {{- with .Page.Params.toc -}}
          {{ partial (print . ".html") . }}
          <br/>
          {{- end -}}

          {{ .Content }}
        </article>

        {{ partial "post-navigation.html" . }}
        {{ partial "post-footer.html" . }}
      </div><!-- /.blog-post -->
    </div><!-- /.blog-main -->
  </main>
{{ end }}

{{ define "aside" }}
  <aside class="col-md-4">   
    {{ partial "card-related-posts.html" . }}
  </aside>
{{ end }}
{{< / highlight >}}

#### Content

There are two content affected

* The River

* One Step Up

For each frontmatter, add this line.

{{< highlight toml >}}
toc        = "toc-2014-03-springsteen"
{{< / highlight >}}

That line refer to html filename in partials directory.

#### Result

![Hugo Content: Table of Content: Page][image-ss-02-toc-page]

-- -- --

### 2: Inside Content: Advertisement

Still in content context, we are going to produce,
raw HTML using shortcodes.

#### Reading

* [gohugo.io shortcode-templates](https://gohugo.io/templates/shortcode-templates/)

#### Shortcode: Advert

No. I won't use adense here.
But I use my own real business cardname instead.

* <code>themes/tutor-06/layouts/shortcodes/advert.html</code>
  : [gitlab.com/.../layouts/shortcodes/advert.html][tutor-hugo-layouts-advert]

{{< highlight twig >}}
<div class="card card-primary">
  <div class="card-body">
    <img alt="advertisement" 
         src="{{- .Page.Site.BaseURL -}}assets/site/images/adverts/{{.Get "src" }}">
  </div>
</div>
{{< / highlight >}}

You can use later on your document as:

{{< highlight twig >}}
{{ < advert src="oto-spies-01.png" > }}
{{< / highlight >}}

#### Content: Using The Advert Shortcode

Conisder make this one as an example/.

* <code>content/quotes/bruce-springsteen-one-step-up.md</code>
  : [gitlab.com/.../content/quotes/...one-step-up.md][tutor-hugo-content-one-step-up]

{{< highlight markdown >}}
+++
type       = "post"
title      = "Bruce Springsteen - One Step Up"
date       = 2014-03-25T07:35:05+07:00
categories = ["lyric"]
tags       = ["springsteen", "80s"]
slug       = "bruce-springsteen-one-step-up"
author     = "epsi"

toc        = "toc-2014-03-springsteen"

related_link_ids = [
  "14031535"  # The River
]

+++

When I look at myself I don't see.
The man I wanted to be.
Somewhere along the line I slipped off track.
I'm caught movin' one step up and two steps back.

{{ < advert src="oto-spies-01.png" > }}

There's a girl across the bar.
I get the message she's sendin'.
Mmm she ain't lookin' too married.
And me well honey I'm pretending.

{{ < advert src="oto-spies-02.png" > }}

Last night I dreamed I held you in my arms.
The music was never-ending.
We danced as the evening sky faded to black.
One step up and two steps back.
{{< / highlight >}}

#### Result

![Hugo Content: Advertisement: Oto Spies][image-ss-02-advert]

-- -- --

### What is Next ?

There are, some interesting topic
about <code>Syntax Highlighting in Hugo Content</code>
Consider continue reading [ [Hugo - Syntax Highlighting][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2018/12/05/hugo-syntax-highlighting/

[image-ss-02-toc-card]:  {{< assets-ssg >}}/2018/11/62-table-of-content-card.png
[image-ss-02-toc-page]:  {{< assets-ssg >}}/2018/11/62-table-of-content-page.png
[image-ss-02-advert]:    {{< assets-ssg >}}/2018/11/62-advert-oto-spies.png


[tutor-hugo-layouts-post-single]:   {{< tutor-hugo-bootstrap >}}/themes/tutor-06/layouts/post/single.html
[tutor-hugo-layouts-advert]:        {{< tutor-hugo-bootstrap >}}/themes/tutor-06/layouts/shortcodes/advert.html
[tutor-hugo-layouts-toc]:   {{< tutor-hugo-bootstrap >}}/layouts/partials/toc-2014-03-springsteen.html

[tutor-hugo-content-one-step-up]:   {{< tutor-hugo-bootstrap >}}/content/quotes/bruce-springsteen-one-step-up.md
