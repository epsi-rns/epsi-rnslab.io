---
type   : post
title  : "Hugo - Syntax Highlighting"
date   : 2018-12-05T09:17:35+07:00
slug   : hugo-syntax-highlighting
categories: [ssg, frontend]
tags   : [hugo, css, js]
keywords  : [static site, custom theme, javascript, syntax highlighting, pygment, PrismJS, highlight sass]
author : epsi
opengraph:
  image: assets/site/images/topics/hugo-bootstrap-markdown.png

toc    : "toc-2018-09-hugo-bootstrap-step"

excerpt:
  Building Hugo Step by step, with Bootstrap as stylesheet frontend.
  A few method to highlight code.

---

### Preface

> Goal: A few method to highlight code.

From default, triple backtick, to prismjs.

-- -- --

### 1: Built-in: Pygment

#### Reading

* [gohugo.io syntax-highlighting/](https://gohugo.io/content-management/syntax-highlighting/)

#### Shortcode

Syntax highlight in Hugo is easy,
just put it under <code>syntax</code> shortcode.
For example, this Haskell code:

{{< highlight html "linenos=table">}}
{{ < highlight haskell > }}
-- Layout Hook
commonLayout = renamed [Replace "common"]
    $ avoidStruts 
    $ gaps [(U,5), (D,5)] 
    $ spacing 10
    $ Tall 1 (5/100) (1/3)
{{ < / highlight > }}
{{< / highlight >}}

Example provided here, between the lyric:

* <code>content/quotes/julian-baker-something.md</code>
  : [gitlab.com/.../content/quotes/julian-baker-something.md][tutor-hugo-content-something]

#### Preview: Dark

Hugo will show nice default dark color on web browser.

![Hugo Highlight: Pygment: Default][tutor-hugo-pygments-default]

#### Theme

Highlight theme in Hugo can be set in config.

* <code>config.toml</code>
  : [gitlab.com/.../config.toml][tutor-hugo-config]

{{< highlight toml >}}
PygmentsCodeFences = true
PygmentsStyle      = "tango"
{{< / highlight >}}

#### Preview: Bright

Hugo will show nice bright color on web browser.

![Hugo Highlight: Pygment: Tango][tutor-hugo-pygments-tango]

#### Highlight Theme Gallery

* [help.farbox.com/pygments.html](https://help.farbox.com/pygments.html)

-- -- --

### 2: Triple Backtick

There is another way, using triple backtick.

{{< highlight markdown >}}
```haskell
-- Layout Hook
commonLayout = renamed [Replace "common"]
    $ avoidStruts 
    $ gaps [(U,5), (D,5)] 
    $ spacing 10
    $ Tall 1 (5/100) (1/3)
```
{{< / highlight >}}

* <code>content/quotes/joni-mitchell-both-sides-now.md</code>
  : [gitlab.com/.../content/quotes/joni-mitchell-both-sides-now.md][tutor-hugo-content-both-sides]

#### Server Output: Browser

Open in your favorite browser.
You should see, highlighted code.

![Hugo Highlight: Backtick: Haskell][tutor-hugo-backtick-haskell]

-- -- --

### 3: PrismJS

You can also, format the result later using PrismJS.

But you need to change these artefacts:

* <code>themes/tutor-06/layouts/partials/site-head.html</code>

* <code>themes/tutor-06/layouts/partials/site-scripts.html</code>

#### Reading

* [prismjs.com/](https://prismjs.com/)

#### CSS

* <code>themes/tutor-06/layouts/partials/site-head.html</code>
  : [gitlab.com/.../layouts/partials/site-head.html][tutor-hugo-layouts-head].

{{< highlight twig >}}
    <link rel="stylesheet" type="text/css" href="{{ "css/prism.css" | relURL }}">
{{< / highlight >}}

#### Javascript

* <code>themes/tutor-06/layouts/partials/site-scripts.html</code>
  : [gitlab.com/.../layouts/partials/site-scripts.html][tutor-hugo-layouts-scripts].

{{< highlight twig >}}
    <script src="{{ "js/prism.js" | relURL }}"></script>
{{< / highlight >}}

#### Server Output: Browser

Open in your favorite browser.
You should see, highlighted code.

![Hugo Highlight: PrismJS: Haskell][tutor-hugo-prismjs-haskell]

-- -- --

### 4: Highlight Stylesheet

There is also this useful <code>pygmentsUseClasses</code> directive
that you can set in <code>config.toml</code>.

So you can use this:
* [github.com/richleland/pygments-css](https://github.com/richleland/pygments-css)

With <code>highlight</code> class renamed to <code>chroma</code>.

But I never explore it.
Since Hugo and PrismJS, are more than enough for me.

#### Chromastyles

You can also use this useful command line.

{{< highlight bash >}}
hugo gen chromastyles --style=perldoc > perldoc.css
{{< / highlight >}}

I think this is enough for now.

-- -- --

### What is Next ?

I hope that ypu are still there.
Consider relax, and look at this cat in my workshop.
Have a break for a while, but do not give up yet.
We still have a few materials to go.

![adorable cat][image-kitten]

There are, some interesting topic about using <code>Meta in Hugo</code>,
such as <code>Opengraph</code> and <code>Twitter</code>.
Consider continue reading [ [Hugo - Meta - Opengraph][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2018/12/07/hugo-meta-opengraph/
[image-kitten]:             {{< baseurl >}}/assets/site/images/cats/bengkel.jpg
[link-pygment-css]:         https://github.com/richleland/pygments-css

[tutor-hugo-pygments-default]:    {{< assets-ssg >}}/2018/12/63-pygments-default.png
[tutor-hugo-pygments-tango]:      {{< assets-ssg >}}/2018/12/63-pygments-tango.png
[tutor-hugo-backtick-haskell]:    {{< assets-ssg >}}/2018/12/63-backtick-haskell.png
[tutor-hugo-prismjs-haskell]:     {{< assets-ssg >}}/2018/12/63-prismjs-haskell.png

[tutor-hugo-config]:              {{< tutor-hugo-bootstrap >}}/config.toml
[tutor-hugo-content-something]:   {{< tutor-hugo-bootstrap >}}/content/quotes/julian-baker-something.md
[tutor-hugo-content-both-sides]:  {{< tutor-hugo-bootstrap >}}/content/quotes/joni-mitchell-both-sides-now.md

[tutor-hugo-layouts-head]:        {{< tutor-hugo-bootstrap >}}/themes/tutor-06/layouts/partials/site-head.html
[tutor-hugo-layouts-scripts]:     {{< tutor-hugo-bootstrap >}}/themes/tutor-06/layouts/partials/site-scripts.html

