---
type   : post
title  : "Hugo - Service"
date   : 2018-12-10T09:17:35+07:00
slug   : hugo-service
categories: [ssg, frontend]
tags      : [hugo]
keywords  : [static site, custom theme, service, google analytic, disqus comments]
author : epsi
opengraph:
  image: assets/site/images/topics/hugo-bootstrap-markdown.png

toc    : "toc-2018-09-hugo-bootstrap-step"

excerpt:
  Building Hugo Step by step, with Bootstrap as stylesheet frontend.
  Example of common service.

---

### Preface

> Goal: Setup Google Analytic and Disqus Comments.

#### Reading

* [gohugo.io/templates/internal/](https://gohugo.io/templates/internal/)

-- -- --

### 1: Google Analytics

#### Get The Code

You should setup google analytic before you begin.
And get the code.

![Hugo: Google Analytic: Get The Code][tutor-hugo-google-analytic]

#### Partial: Service

Copy and Paste directly: 

* <code>themes/tutor-06/layouts/partials/service-google-analytics.html</code>
  : [gitlab.com/.../layouts/partials/service-google-analytics.html][tutor-hugo-service-analytic]

{{< highlight javascript >}}
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-78098670-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-78098670-2');
</script>
{{< / highlight >}}

#### Default: Baseof Template

Consider go back to our ancient artefact <code>baseof.html</code>.
And add this partial:

{{< highlight twig >}}
  {{ partial "service-google-analytics.html" . }}
{{< / highlight >}}

* <code>/layouts/_default/baseof.html</code>.
  : [gitlab.com/.../layouts/_default/baseof.html][tutor-hugo-layouts-baseof]

{{< highlight twig >}}
<!DOCTYPE html>
<html lang="en">
{{ partial "site-head.html" . }}
<body>
{{ partial "service-google-analytics.html" . }}
{{ partial "site-header.html" . }}

  <div class="container-fluid maxwidth">

    <div class="row layout-base">
      {{- block "main" . }}
      {{ .Content }}
      {{- end }}

      {{- block "aside" . }}{{- end }}
    </div><!-- .row -->

  </div><!-- .container-fluid -->

{{ partial "site-footer.html" . }}
{{ partial "site-scripts.html" . }}
</body>
</html>
{{< / highlight >}}

#### Get The Chart

I mostly watch the chart for result.

![Hugo: Google Analytic: Get The Chart][tutor-hugo-analytic-chart]

-- -- --

### 2: Disqus Comment

#### Get The Code

You should setup disqus account before you begin.

#### Partial: Service

Copy and Paste the code:

* <code>themes/tutor-06/layouts/partials/service-disqus-comments.html</code>
  : [gitlab.com/.../layouts/partials/service-disqus-comments.html][tutor-hugo-service-disqus]

{{< highlight html >}}
<div id="disqus_thread"></div>
<script type="text/javascript">

(function() {
    // Don't ever inject Disqus on localhost--it creates unwanted
    // discussions from 'localhost:1313' on your Disqus account...
    if (window.location.hostname == "localhost")
        return;

    var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
    var disqus_shortname = 'epsirnsgithubio';
    dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
})();
</script>
<noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
<a href="http://disqus.com/" class="dsq-brlink">comments powered by <span class="logo-disqus">Disqus</span></a>
{{< / highlight >}}

Notice this line, if you do not want to bother with config setting.

{{< highlight twig >}}
    var disqus_shortname = 'epsirnsgithubio';
{{< / highlight >}}

#### Layouts: Single

Consider go back to our ancient artefact <code>single.html</code>.
And add this partial:

{{< highlight twig >}}
  {{ partial "service-disqus-comments.html" . }}
{{< / highlight >}}

* <code>themes/tutor-06/layouts/post/single.html</code>
  : [gitlab.com/.../layouts/post/single.html][tutor-hugo-layouts-post-single]

{{< highlight twig >}}
{{ define "main" }}
  <main role="main" class="col-md-8">
    <div class="blog-main p-3 mb-3
                bg-light rounded border border-dark shadow-hover">
      <div class="blog-post" 
           itemscope itemtype="http://schema.org/BlogPosting">
        {{ partial "post-header.html" . }}

        <article class="main-content" itemprop="articleBody">
          {{- with .Page.Params.toc -}}
          {{ partial (print . ".html") . }}
          <br/>
          {{- end -}}

          {{ .Content }}
        </article>

        {{ partial "post-navigation.html" . }}
        {{ partial "post-footer.html" . }}
      </div><!-- /.blog-post -->
    </div><!-- /.blog-main -->

    {{ partial "service-disqus-comments.html" . }}
  </main>
{{ end }}

{{ define "aside" }}
  <aside class="col-md-4">   
    {{ partial "card-related-posts.html" . }}
  </aside>
{{ end }}
{{< / highlight >}}

#### Server Output: Browser

Open in your favorite browser.
You should see, the disqus comments, already served.

![Hugo Highlight: Disqus Comments][tutor-hugo-disqus-comments]

-- -- --

### What is Next ?

There are, some interesting topic about setting up <code>Search in Hugo</code>,
using <code>lunr js</code>.
Consider continue reading [ [Hugo - Javacript - lunr Search][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2018/12/15/hugo-javascript-search/

[tutor-hugo-google-analytic]:   {{< assets-ssg >}}/2018/12/64-google-analytic.png
[tutor-hugo-analytic-chart]:    {{< assets-ssg >}}/2018/12/64-google-analytic-chart.png
[tutor-hugo-disqus-comments]:    {{< assets-ssg >}}/2018/12/64-disqus-comments.png

[tutor-hugo-layouts-baseof]:    {{< tutor-hugo-bootstrap >}}/themes/tutor-06/layouts/_default/baseof.html
[tutor-hugo-service-analytic]:  {{< tutor-hugo-bootstrap >}}/themes/tutor-06/layouts/partials/service-google-analytics.html
[tutor-hugo-service-disqus]:    {{< tutor-hugo-bootstrap >}}/themes/tutor-06/layouts/partials/service-disqus-comments.html
[tutor-hugo-layouts-post-single]:   {{< tutor-hugo-bootstrap >}}/themes/tutor-06/layouts/post/single.html
