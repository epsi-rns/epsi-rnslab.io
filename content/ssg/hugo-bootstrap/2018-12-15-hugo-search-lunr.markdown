---
type   : post
title  : "Hugo - Javacript - lunr Search"
date   : 2018-12-15T09:17:35+07:00
slug   : hugo-javascript-search
categories: [ssg, frontend]
tags      : [hugo, js]
keywords  : [static site, custom theme, lunr search, javascript, jquery, json]
author : epsi
opengraph:
  image: assets/site/images/topics/hugo-bootstrap-markdown.png

toc    : "toc-2018-09-hugo-bootstrap-step"

excerpt:
  Building Hugo Step by step, with Bootstrap as stylesheet frontend.
  Javascript example in Hugo. Search Page with lunrjs.

---

### Preface

> Goal: Javascript Search in Hugo, using lunrjs

-- -- --

### 1: Prepare

#### Reading

* [lunrjs.com/](https://lunrjs.com/)

#### Source

I respect copyright.
The javascript code below, copied and pasted from:

* [rayhightower.com: How to Make lunr.js and Jekyll Work Together (with Gotchas)](http://rayhightower.com/blog/2016/01/04/how-to-make-lunrjs-jekyll-work-together/)

* [learn.cloudcannon.com: Jekyll search using lunr.js](https://learn.cloudcannon.com/jekyll/jekyll-search-using-lunr-js/)

Although both are intented to be used with Jekyll,
the Javascript part reamin the same with Hugo.

I also made a slight modification, and a little enhancement.
But of course the logic for lunr part remain the same.

#### Artefacts

This is the list of the Arteftacfts that we require.

Edit Artefact:

* <code>themes/tutor-06/layouts/partials/site-scripts.html</code>

* <code>themes/tutor-06/layouts/_default/baseof.html</code>

* <code>themes/tutor-06/layouts/partials/site-header.html</code>

New Artefact:

* <code>themes/tutor-06/layouts/search/single.html</code>

* <code>content/pages/search.md</code>

* <code>themes/tutor-06/static/js/jquery.min.js</code>

* <code>themes/tutor-06/static/js/lunr-0.7.1.min.js</code>

* <code>themes/tutor-06/static/js/search-lunr-0.7.1.js</code>

-- -- --

### 2: Search Page

The search page consist of this two artefacts:

* <code>themes/tutor-06/layouts/search/single.html</code>

* <code>content/pages/search.md</code>

#### Layout: Single

First we need to create newly layout for search

* <code>themes/tutor-06/layouts/search/single.html</code>
  : [gitlab.com/.../layouts/search/single.html][tutor-hugo-layouts-single]

{{< highlight twig >}}
{{ define "main" }}
<div class="p-2">
  <form action="get" id="site_search">
    <label for="search_box">Search</label>
    <input type="text" id="search_box" name="query">
    <input type="submit" value="search">
  </form>

  <ul id="search_results" class="list-unstyled"></ul>
</div>
{{ end }}
{{< / highlight >}}

#### Content: Search

* <code>content/pages/search.md</code>
  : [gitlab.com/.../content/pages/search.md][tutor-hugo-content-search]

{{< highlight twig >}}
+++
type  = "search"
title = "Search Blog"
+++
{{< / highlight >}}

#### Server Output: Browser

Open in your favorite browser.
You should see, search form in search page.

* <code>http://localhost:1313/pages/search/</code>

![Hugo Search: No Javascript][image-ss-05-search-nojs]

We do not use javascript yet.

-- -- --

### 3: Navigation Bar

Consider have a look at our ancient artefact,
that exist from the very beginning of the tutorial.

We need to update navigation Bar.
Change the form part.

* <code>themes/tutor-06/layouts/partials/site-header.html</code>
  : [gitlab.com/.../layouts/partials/site-header.html][tutor-hugo-layouts-header]

{{< highlight twig >}}
      <form class="form-inline mt-2 mt-md-0" action="/pages/search/" method="get">
        <input class="form-control mr-sm-2" type="text" name="q"
          placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-light my-2 my-sm-0" 
          type="submit">Search</button>
      </form>
{{< / highlight >}}

#### Server Output: Browser

Open in your favorite browser.
You should see, search form in navigation bar.

* <code>http://localhost:1313/</code>

![Hugo Search: Navigation Bar][image-ss-05-navigation-bar]

#### What does it do ?

If you type a word to be searched,
such as for example <code>time</code>,
and click the <code>search</code> button.
The page will be redirect to 

* <code>http://localhost:1313/pages/search/?q=time</code>

-- -- --

#### 4: Javascript Layout

#### Riddle ?

We need to forward the url request such as:

* <code>http://localhost:1313/pages/search/?q=time</code>

To the form in search page, using javacript.
So the form will show the word <code>time</code>.

We need to put a specific javascript in this serach page.

#### Layout: baseof

How do we manage javascript for specific page ?

We should have look at our ancient artefact.
All we need is to add <code>block</code>.

* <code>themes/tutor-06/layouts/_default/baseof.html</code>
  : [gitlab.com/.../layouts/_default/baseof.html][tutor-hugo-layouts-baseof]

{{< highlight twig >}}
{{ partial "site-scripts.html" . }}
{{ block "custom-javascript" . }}{{ end }}
{{< / highlight >}}

The complete code is here

{{< highlight twig >}}
<!DOCTYPE html>
<html lang="en">
{{ partial "site-head.html" . }}
<body>
{{/* partial "service-google-analytics.html" . */}}
{{ partial "site-header.html" . }}

  <div class="container-fluid maxwidth">

    <div class="row layout-base">
      {{- block "main" . }}
      {{ .Content }}
      {{- end }}

      {{- block "aside" . }}{{- end }}
    </div><!-- .row -->

  </div><!-- .container-fluid -->

{{ partial "site-footer.html" . }}
{{ partial "site-scripts.html" . }}
{{ block "custom-javascript" . }}{{ end }}
</body>
</html>
{{< / highlight >}}

#### Layout: Single

And apply the block in layout.

* <code>themes/tutor-06/layouts/search/single.html</code>
  : [gitlab.com/.../layouts/search/single.html][tutor-hugo-layouts-single]

{{< highlight twig >}}
{{ define "main" }}
<div class="p-2">
  <form action="get" id="site_search">
    <label for="search_box">Search</label>
    <input type="text" id="search_box" name="query">
    <input type="submit" value="search">
  </form>

  <ul id="search_results" class="list-unstyled"></ul>
</div>
{{ end }}

{{ define "custom-javascript" }}
    <script src="{{ "js/search-form-example.js" | relURL }}"></script>
{{ end }}
{{< / highlight >}}

#### Javascript: Example

And the content of the Javascript example is:

* <code>themes/tutor-06/static/js/search-form-example.js</code>
  : [gitlab.com/.../static/js/search-form-example.js][tutor-hugo-javascript-form]

{{< highlight javascript >}}
jQuery(function() {
  // Get search results if q parameter is set in querystring
  if (getParameterByName('q')) {
      var query = decodeURIComponent(getParameterByName('q'));
      $("#search_box").val(query);
      
      window.data.then(function(loaded_data){
        $('#site_search').trigger('submit'); 
      });
  }

});

 /* ==========================================================================
    Helper functions
    ========================================================================== */

/**
 * Gets query string parameter - taken from 
 * http://stackoverflow.com/questions/901115/how-can-i-get-query-string-values-in-javascript
 * @param {String} name 
 * @return {String} parameter value
 */
function getParameterByName(name) {
    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}
{{< / highlight >}}

#### Server Output: Browser

Open in your favorite browser.
You should see, search form respond to url query.

* <code>http://localhost:1313/pages/search/?q=time</code>

![Hugo Search: Search Form JQuery][image-ss-05-search-form-time]

-- -- --

### 5: JQuery Requirement

Our lunr search require JQuery, instead of just JQuery Slim.

#### Download JQuery

And put in JS directory under static assets.

* <code>themes/tutor-06/static/js/jquery.min.js</code>

#### Partial: Javascript

It is just an empty template.

* <code>themes/tutor-06/layouts/partials/site-scripts.html</code>
  : [gitlab.com/.../layouts/partials/scripts.html][tutor-hugo-layouts-scripts]

{{< highlight html >}}
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{{ "js/jquery.min.js" | relURL }}"></script>
    <script src="{{ "dist/js/bootstrap.min.js" | relURL }}"></script>
    <script src="{{ "js/prism.js" | relURL }}"></script>
{{< / highlight >}}

-- -- --

### 6: Search Data Source

Remember our last JSON source ?
Consider refresh our last tutorial.

* [Hugo - Custom Content Type][local-custom-output]

#### Server Output: Browser

Now you can see JSON output in your favorite browser.

* <code>http://localhost:1313/pages/archives/index.json</code>

This will produce something similar as shown below:

{{< highlight json >}}
{
  
    "/quotes/2018/09/13/mothers-no-crying-in-baseball/": {
      ...
    },
    "/quotes/2018/09/07/julian-baker-something/": {
      "title": "Julian Baker - Something",
      "content": "I knew I was wasting my time. Keep myself awake at night.",
      "url": "http://localhost:1313/quotes/2018/09/07/julian-baker-something/",
      "author": "epsi",
      "category": "quotes"
    },
    ...
}
{{< / highlight >}}

Modern browser automatically format,
the JSON output in nice view collapsable view.

![Hugo: JSON Content Type][image-ss-07-browser-json]

-- -- --

### 7: lunr Search

This is the main component.

#### Download lunrjs.

I haven't update my skill lately.
I'm still using the old lunrjs code.

* [github.com lunr.js v0.7.1](https://github.com/olivernn/lunr.js/tree/v0.7.1)

And put it in JS directory.

* <code>themes/tutor-06/static/js/lunr-0.7.1.min.js</code>

#### Layout: Single

And apply the block in layout.

* <code>themes/tutor-06/layouts/search/single.html</code>
  : [gitlab.com/.../layouts/search/single.html][tutor-hugo-layouts-single]

{{< highlight twig >}}
{{ define "custom-javascript" }}
    <script src="{{ "js/lunr-0.7.1.min.js" | relURL }}"></script>
    <script src="{{ "js/search-data-example.js" | relURL }}"></script>
{{ end }}
{{< / highlight >}}

#### Javascript: Loading Lunr

And the content of the Javascript example is:

* <code>themes/tutor-06/static/js/search-data-example.js</code>
  : [gitlab.com/.../static/js/search-data-example.js][tutor-hugo-javascript-data]

{{< highlight javascript >}}
jQuery(function() {

  // Initalize lunr with the fields it will be searching on. I've given title
  // a boost of 10 to indicate matches on this field are more important.
  window.idx = lunr(function () {
    this.ref('id');
    this.field('title');
    this.field('content', { boost: 10 });
    this.field('author');
    this.field('category');
  });

  // Download the data from the JSON file we generated
  window.data = $.getJSON('/pages/archives/index.json');

  // Wait for the data to load and add it to lunr
  window.data.then(function(loaded_data){
    $.each(loaded_data, function(index, value){
      window.idx.add(
        $.extend({ "id": index }, value)
      );
    });
  });

  // Event when the form is submitted
  $("#site_search").submit(function(event){
      event.preventDefault();
      var query = $("#search_box").val(); // Get the value for the text field
      var results = window.idx.search(query); // Get lunr to perform a search
      display_search_results(results); // Hand the results off to be displayed
  });

  function display_search_results(results) {
    console.log(results);
  }

});
{{< / highlight >}}

Notice the data source is that we call

{{< highlight javascript >}}
window.data = $.getJSON('/pages/archives/index.json');
{{< / highlight >}}

#### Server Output: Browser

Open in your favorite browser.
Put any text in search form, and click the search button.
You should see, console contain array, as respond to button click.

* <code>http://localhost:1313/pages/search/</code>

![Hugo Search: lunr Data in Console][image-ss-05-search-console]

In json object, this would be as below:

{{< highlight json >}}
[
  {
    "ref": "/quotes/2018/09/07/julian-baker-something/",
    "score": 0.1154962866960379
  }
]
{{< / highlight >}}

-- -- --

### 8: Showing The Result

We are almost finished.

#### Javascript: Loading Lunr

Showing the result is simply changing the <code>display_search_results</code>.

* <code>themes/tutor-06/static/js/search-lunr-0.7.1.js</code>
  : [gitlab.com/.../static/js/search-lunr-0.7.1.js][tutor-hugo-javascript-lunr]

{{< highlight javascript >}}
  function display_search_results(results) {
    var $search_results = $("#search_results");

    // Wait for data to load
    window.data.then(function(loaded_data) {

      // Are there any results?
      if (results.length) {
        $search_results.empty(); // Clear any old results

        // Iterate over the results
        results.forEach(function(result) {
          var item = loaded_data[result.ref];

          // Build a snippet of HTML for this result
          var appendString = '<li><a href="' + item.url + '">' + item.title + '</a></li>';

          // Add it to the results
          $search_results.append(appendString);
        });
      } else {
        $search_results.html('<li>No results found</li>');
      }
    });
  }
{{< / highlight >}}

#### Layout: Single

Since we change the javascript name again,
we must also update the block layout in this artefact.

* <code>themes/tutor-06/layouts/search/single.html</code>
  : [gitlab.com/.../layouts/search/single.html][tutor-hugo-layouts-single]

{{< highlight twig >}}
{{ define "main" }}
<div class="p-2">
  <form action="get" id="site_search">
    <label for="search_box">Search</label>
    <input type="text" id="search_box" name="query">
    <input type="submit" value="search">
  </form>

  <ul id="search_results" class="list-unstyled"></ul>
</div>
{{ end }}

{{ define "custom-javascript" }}
    <script src="{{ "js/lunr-0.7.1.min.js" | relURL }}"></script>
    <script src="{{ "js/search-lunr-0.7.1.js" | relURL }}"></script>
{{ end }}
{{< / highlight >}}

#### Server Output: Browser

Open in your favorite browser.
Put any text in search form, and click the search button.
You should see, the html result. as text in browser,
as respond to button click.

* <code>http://localhost:1313/pages/search/</code>

![Hugo Search: lunr Result][image-ss-05-search-result]

-- -- --

### 9: Summary

Now it is about the right time to put it all together.

* <code>themes/tutor-06/static/js/search-lunr-0.7.1.js</code>
  : [gitlab.com/.../static/js/search-lunr-0.7.1.js][tutor-hugo-javascript-lunr]

{{< highlight javascript >}}
// http://rayhightower.com/blog/2016/01/04/how-to-make-lunrjs-jekyll-work-together/
// https://learn.cloudcannon.com/jekyll/jekyll-search-using-lunr-js/

jQuery(function() {

  // Initalize lunr with the fields it will be searching on. I've given title
  // a boost of 10 to indicate matches on this field are more important.
  window.idx = lunr(function () {
    this.ref('id');
    this.field('title');
    this.field('content', { boost: 10 });
    this.field('author');
    this.field('category');
  });

  // Download the data from the JSON file we generated
  window.data = $.getJSON('/pages/archives/index.json');

  // Wait for the data to load and add it to lunr
  window.data.then(function(loaded_data){
    $.each(loaded_data, function(index, value){
      window.idx.add(
        $.extend({ "id": index }, value)
      );
    });
  });

  function display_search_results(results) {
    var $search_results = $("#search_results");

    // Wait for data to load
    window.data.then(function(loaded_data) {

      // Are there any results?
      if (results.length) {
        $search_results.empty(); // Clear any old results

        // Iterate over the results
        results.forEach(function(result) {
          var item = loaded_data[result.ref];

          // Build a snippet of HTML for this result
          var appendString = '<li><a href="' + item.url + '">' + item.title + '</a></li>';

          // Add it to the results
          $search_results.append(appendString);
        });
      } else {
        $search_results.html('<li>No results found</li>');
      }
    });
  }

  // Event when the form is submitted
  $("#site_search").submit(function(event){
      event.preventDefault();
      var query = $("#search_box").val(); // Get the value for the text field
      var results = window.idx.search(query); // Get lunr to perform a search
      display_search_results(results); // Hand the results off to be displayed
  });

  // Get search results if q parameter is set in querystring
  if (getParameterByName('q')) {
      var query = decodeURIComponent(getParameterByName('q'));
      $("#search_box").val(query);
      
      window.data.then(function(loaded_data){
        $('#site_search').trigger('submit'); 
      });
  }

});

 /* ==========================================================================
    Helper functions
    ========================================================================== */

/**
 * Gets query string parameter - taken from 
 * http://stackoverflow.com/questions/901115/how-can-i-get-query-string-values-in-javascript
 * @param {String} name 
 * @return {String} parameter value
 */
function getParameterByName(name) {
    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}
{{< / highlight >}}

-- -- --

### What is Next ?

Feel sleepy ? 
Although it is a good idea to finish your tutorial.
Just like this sleepy cat, you may sleep anytime you want.
Have a break for a while, feed your pet,
and resume our tutorial.

![adorable cat][image-kitten]

There is this our last article, how to make your blog site live.
This is actually, the first article that I made, before I start the whole tutorial.
Consider continue reading [ [Git Pages - Deploying][local-whats-next] ].

Thank you for reading.

Farewell.
We shall meet again.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}devops/2018/08/15/git-hosting/
[local-custom-output]:      {{< baseurl >}}ssg/2018/09/13/hugo-custom-output/
[image-kitten]:             {{< baseurl >}}/assets/site/images/cats/fatboy.jpg

[image-ss-07-browser-json]:      {{< assets-ssg >}}/2018/09/47-browser-json.png
[image-ss-05-search-nojs]:       {{< assets-ssg >}}/2018/12/65-search-page-nojs.png
[image-ss-05-navigation-bar]:    {{< assets-ssg >}}/2018/12/65-navigation-bar.png
[image-ss-05-search-form-time]:  {{< assets-ssg >}}/2018/12/65-search-form-time.png
[image-ss-05-search-console]:    {{< assets-ssg >}}/2018/12/65-search-console.png
[image-ss-05-search-result]:     {{< assets-ssg >}}/2018/12/65-search-result.png

[tutor-hugo-content-search]:    {{< tutor-hugo-bootstrap >}}/content/pages/search.md
[tutor-hugo-layouts-baseof]:    {{< tutor-hugo-bootstrap >}}/themes/tutor-06/layouts/_default/baseof.html
[tutor-hugo-layouts-header]:    {{< tutor-hugo-bootstrap >}}/themes/tutor-06/layouts/partials/site-header.html
[tutor-hugo-layouts-scripts]:   {{< tutor-hugo-bootstrap >}}/themes/tutor-06/layouts/partials/site-scripts.html
[tutor-hugo-layouts-single]:    {{< tutor-hugo-bootstrap >}}/themes/tutor-06/layouts/search/single.html

[tutor-hugo-javascript-form]:   {{< tutor-hugo-bootstrap >}}/themes/tutor-06/static/js/search-form-example.js
[tutor-hugo-javascript-data]:   {{< tutor-hugo-bootstrap >}}/themes/tutor-06/static/js/search-data-example.js
[tutor-hugo-javascript-lunr]:   {{< tutor-hugo-bootstrap >}}/themes/tutor-06/static/js/search-lunr-0.7.1.js
