---
type   : post
title  : "Hugo - Overview"
date   : 2019-04-01T09:17:35+07:00
slug   : hugo-overview
categories: [ssg]
tags      : [hugo]
keywords  : [static site, custom theme, summary, overview]
author : epsi
opengraph:
  image: assets-ssg/2019/04/61-blog-post-all.png

toc    : "toc-2019-04-hugo-bulma-step"

excerpt:
  Building Hugo Site Step by step, with Bulma as stylesheet frontend.
  Summary of article series.

---

### Preface

This article series is intended for beginner.

> Goal: Explain Hugo Step By Step

I might write a lot, but never comes deep into conceptual thinking.
This article series is still the same.

However, Let me start over, by rewrite the old article.

#### Issue with last year Tutorial.

I made a Hugo article series last year,
it is basically a step by step Hugo using Bootstrap.

* [Hugo Step By Step with Bootstrap][local-hugo-summary]

After a while, there are some feedback.
Some reader has difficulty with that article,
because reader have to learn SASS at the same time as using Hugo.

> A sequel

I decide to rewrite the Tutorial, with the same material,
and using Bulma as stylesheet frontend instead of Bootstrap,
so that I can feel that there is still added value in writing this.

The early article of this article series would be similar to bootstrap one.
But the rest would be different. Nomore SASS explanation.

#### The Bulma SASS Article Series

Now we have two separate article series.

* [Bulma SASS][local-bulma-overview]: with pure HTML and no SSG.

* This newly Hugo Article Series.

#### Prerequisite

You have to read the [Bulma SASS][local-bulma-overview] ,
before reading this Hugo article series.

I'm using <code>Bulma</code> as an example in this guidance.
Of course you can adapt this to any CSS framework that you like,
such as <code>Materialize</code> or <code>Bootstrap</code>.

-- -- --

### About Hugo

Hugo is an SSG (Static Site Generator) that is supported by **gitlab**.
Or used pairly by using **github** with **netlify**.
Hugo is not the only **SSG** (Static Site Generator).

#### Why Hugo ?

> Hugo is Fast.

Sure I have compared Hugo with other SSG.
There is other fast Static site generator,
but not with as much feature as Hugo.

#### Part of Hugo

There are these two part that you need to know.

* Black Friday: Markdown Parser for Hugo

* Chroma: Templating Language for Hugo

Hugo content can use either html or markdown.
You can read about Markdown format here:

* <https://daringfireball.net/projects/markdown/syntax>

#### Source Code

Source code used in this tutorial, is available at:

*	[gitlab.com/epsi-rns/tutor-hugo-bulma][tutor-hugo-master]

Source code for Bulma SASS, is also available at:

*	[gitlab.com/epsi-rns/tutor-html-bulma][tutor-html-master]

#### Demo Hugo

Unfortunately, I do not create Demo site with Bulma,
but you can still use this old Demo in Bootstrap.
It is based on this tutorial, but in a more enhanced way,
to face real life, blog building situation.

*	[Demo Hugo](https://epsi-rns.gitlab.io/demo-hugo/)

And add some aestethic appeal, with more custom stylesheet.

![Hugo: Demo][image-ss-00-demo-hugo]

-- -- --

### The Article Series

#### Disclaimer

This is would not be the best blog template that you ever have.
Because I only put most common stuff,
to keep the tutorial simple.

After you learn this guidance,
you understand the fundamental skill.
Thus, a base for you, to make your own blog site.
With your imagination, you may continue,
to build your own super duper Hugo site.
Far better than, what I have achieved.

#### Content

A site need content.
I do not want __lorem ipsum__.

> None of Your Buciness

To make this step by step tutorial alive,
I choose most common topic, a fictional love story,
about Hugo, Bulma, and lightyears between.

I put some lyrics from great beautiful songs.
I'm not sure exactly what these songs means,
but I think most is about falling in love with someone unexpected.

> Sebuah catatan blog dengan tema-tema perbucinan.

Without the content, you can still make your site anyway.
You do not really need to understand the story behind the content.

#### Table of Content

The table content is available as header on each tutorial.
There, I present a Hugo Tutorial, step by step, for beginners.

-- -- --

### Links

#### Hugo Article Series

Consist of more than 30 articles.

* [Hugo - Overview][hugo-overview]

* [Hugo Step by Step Repository][tutorial-hugo] (this repository)

#### Bulma Article Series

Consist of 10 articles.

* [Bulma - Overview][bulma-overview]

* [Bulma Step by Step Repository][tutorial-bulma]

[hugo-overview]:     https://epsi-rns.gitlab.io/ssg/2019/04/01/hugo-overview/
[bulma-overview]:    https://epsi-rns.gitlab.io/frontend/2019/03/01/bulma-overview/

[tutorial-hugo]:  https://gitlab.com/epsi-rns/tutor-hugo-bulma/
[tutorial-bulma]: https://gitlab.com/epsi-rns/tutor-html-bulma/

-- -- --

### Chapter Step by Step

#### Tutor 01

> Generate Only Pure HTML

* Setup Directory for Minimal Hugo

* General Layout: Base, List, Single

* Partials: HTML Head, Header, Footer

* Additional Layout: Post

* Basic Content

![Hugo Bulma : Tutor 01][tutor-01]

Archive: [Tutor 01][archive-01]

-- -- --

#### Tutor 02

* Add Bulma CSS

* Standard Header (jquery or vue) and Footer

* Enhance All Layouts with Bulma CSS

![Hugo Bulma : Tutor 02][tutor-02]

Archive: [Tutor 02][archive-02]

-- -- --

#### Tutor 03

* Add Custom SASS (Custom Design)

* Nice Header and Footer

* General Layout: Terms, Taxonomy

* Apply Two Column Responsive Layout for Most Layout

* Nice Tag Badge in Tags Page

![Hugo Bulma : Tutor 03][tutor-03]

Archive: [Tutor 03][archive-03]

-- -- --

#### Tutor 04

* More Content: Lyrics and Quotes. Need this content for demo

* Additional Layout: Archives

* Article Index: By Year, List Tree (By Year and Month)

* Custom Output: YAML, TXT, JSON

![Hugo Bulma : Tutor 04][tutor-04]

> Optional Feature

* Widget: Friends, Archives Tree, Categories, Tags, Recent Post, Related Post

![Hugo Bulma : Tutor 04][tutor-04-widget]

Archive: [Tutor 04][archive-04]

-- -- --

#### Tutor 05

* Article Index: Apply Multi Column Responsive List

* Post: Header, Footer, Navigation

> Optional Feature

* Blog Pagination: Adjacent, Indicator, Responsive

![Hugo Bulma: Tutor 05: Indicator Pagination][tutor-05-indi]

![Hugo Bulma: Tutor 05: Responsive Pagination][tutor-05-resp]

> Finishing

* Layout: Service

* Post: Markdown Content

* Post: Table of Content

* Post: Responsive Images

* Post: Shortcodes

* Post: Syntax Highlight

* Post: Bulma Title: CSS Fix

* Meta: HTML, SEO, Opengraph, Twitter

* RSS: Filter to Show Only Post Kind

![Hugo Bulma : Tutor 05: Post][tutor-05-post]

Archive: [Tutor 05][archive-05]

-- -- --

What do you think ?
  
[hugo-bulma-preview]:   https://gitlab.com/epsi-rns/tutor-hugo-bulma/raw/master/preview/hugo-bulma-preview.png
[tutor-01]:         https://gitlab.com/epsi-rns/tutor-hugo-bulma/raw/master/preview/14-browser-nouse-winter.png
[tutor-02]:         https://gitlab.com/epsi-rns/tutor-hugo-bulma/raw/master/preview/25-browser-post.png
[tutor-03]:         https://gitlab.com/epsi-rns/tutor-hugo-bulma/raw/master/preview/35-browser-post.png
[tutor-04]:         https://gitlab.com/epsi-rns/tutor-hugo-bulma/raw/master/preview/44-browser-terms-tree.png
[tutor-04-widget]:  https://gitlab.com/epsi-rns/tutor-hugo-bulma/raw/master/preview/51-panel-archives.png
[tutor-05-indi]:    https://gitlab.com/epsi-rns/tutor-hugo-bulma/raw/master/preview/52-04-indicator-bulma-animate.gif
[tutor-05-resp]:    https://gitlab.com/epsi-rns/tutor-hugo-bulma/raw/master/preview/52-04-responsive-bulma-animate.gif
[tutor-05-post]:    https://gitlab.com/epsi-rns/tutor-hugo-bulma/raw/master/preview/61-blog-post-all.png

[archive-01]:       https://gitlab.com/epsi-rns/tutor-hugo-bulma/raw/master/preview/tutor-hugo-01.tar
[archive-02]:       https://gitlab.com/epsi-rns/tutor-hugo-bulma/raw/master/preview/tutor-hugo-02.tar
[archive-03]:       https://gitlab.com/epsi-rns/tutor-hugo-bulma/raw/master/preview/tutor-hugo-03.tar
[archive-04]:       https://gitlab.com/epsi-rns/tutor-hugo-bulma/raw/master/preview/tutor-hugo-04.tar
[archive-05]:       https://gitlab.com/epsi-rns/tutor-hugo-bulma/raw/master/preview/tutor-hugo-05.tar


-- -- --

### Begin The Guidance

Let's get the tutorial started.
Long and short of it, this is the tale.

Consider continue reading [ [Hugo Minimal][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2019/04/02/hugo-minimal/

[local-hugo-summary]:   {{< baseurl >}}ssg/2018/09/01/hugo-summary/
[local-bulma-overview]: {{< baseurl >}}frontend/2019/03/01/bulma-overview/

[tutor-hugo-master]:    {{< tutor-hugo-bulma >}}/
[tutor-html-master]:    {{< tutor-html-bulma >}}/

[image-ss-00-demo-hugo]:    {{< assets-ssg >}}/2019/04/00-hugo-bulma-preview.png
