---
type   : post
title  : "Hugo - Minimal"
date   : 2019-04-02T09:17:35+07:00
slug   : hugo-minimal
categories: [ssg]
tags      : [hugo]
keywords  : [static site, custom theme, configuration, markdown, layout]
author : epsi
opengraph:
  image: assets/site/images/topics/hugo-bulma-markdown.png

toc    : "toc-2019-04-hugo-bulma-step"

excerpt:
  Building Hugo Site Step by step, with Bulma as stylesheet frontend.
  Show Minimal Homepage in Hugo.

---

### Preface

> Goal: Show Minimal Homepage in Hugo

#### Related Article

I had wrote about Minimal Hugo and you can read here:

* [Hugo Minimal][local-hugo-minimal]

The only different is, the content.
And I also add nice favicon.

#### Source Code

You can download the source code of this article here.

* [tutor-hugo-minimal.tar][tar-00-source]

-- -- --

### 1: Which Hugo to Install

Most distribution, have Hugo in repository.
Even Windows can have it with `chocolatey` package manager.

* Windows Environment: [Chocolatey - Hugo][local-windows-choco]

But after a while, you may need multiversion of Hugo.
Or you might need extended verio of Hugo.
You can download here, and put it somewhere in your OS.

* [github.com/gohugoio/hugo/releases](https://github.com/gohugoio/hugo/releases)

![Hugo: Binary Version][image-ss-01-hugo-bin]

In my cases, I prefer to uninstall Hugo package from distribution,
and replace with whatever binary I need with alias in `.bashrc` or `.zshrc`.
For example:

{{< highlight bash >}}
alias hugo='/media/Works/bin/hugo-54'
{{< / highlight >}}

-- -- --

### 2: Running for the First Time.

#### Create Project

Consider create new Hugo site, or extract the file above,
in your favorites directory

{{< highlight bash >}}
$ cd /media/Works/sites

$ mkdir tutor-hugo-bulma

$ cd tutor-hugo-bulma

$ hugo new site .
Congratulations! Your new Hugo site is created in /media/Works/sites/tutor-hugo-bulma.

Just a few more steps and you're ready to go:

1. Download a theme into the same-named folder.
   Choose a theme from https://themes.gohugo.io/, or
   create your own with the "hugo new theme <THEMENAME>" command.
2. Perhaps you want to add some content. You can add single files
   with "hugo new <SECTIONNAME>/<FILENAME>.<FORMAT>".
3. Start the built-in live server via "hugo server".

Visit https://gohugo.io/ for quickstart guide and full documentation.
{{< / highlight >}}

#### Run Server

{{< highlight bash >}}
$ hugo server

                   | EN  
+------------------+----+
  Pages            |  3  
  Paginator pages  |  0  
  Non-page files   |  0  
  Static files     |  0  
  Processed images |  0  
  Aliases          |  0  
  Sitemaps         |  1  
  Cleaned          |  0  

Total in 6 ms
Watching for changes in /media/Works/sites/tutor-hugo-bulma/{content,data,layouts,static}
Watching for config changes in /media/Works/sites/tutor-hugo-bulma/config.toml
Serving pages from memory
Running in Fast Render Mode. For full rebuilds on change: hugo server --disableFastRender
Web Server is available at http://localhost:1313/ (bind address 127.0.0.1)
Press Ctrl+C to stop
{{< / highlight >}}

This will show you nothing in your browser.

<code>
$ curl localhost:1313
</code>

{{< highlight bash >}}
<pre>
</pre>
{{< / highlight >}}

-- -- --

### 3: Minimal Homepage

#### Directory Structure

Consider have look for each file:

<code>
$ tree
</code>

{{< highlight html >}}
.
├── archetypes
│   └── default.md
├── config.toml
├── content
│   └── _index.html
├── layouts
│   └── index.html
├── static
└── themes
{{< / highlight >}}

_._

#### Config

Now you can change your newly created config artefact,
and change the title to whatever suitable for your site,
and then add any description that you want.

* <code>config.toml</code>
  : [gitlab.com/.../config.toml][tutor-hugo-config]

{{< highlight toml >}}
# An example of Hugo site using Bulma for personal learning purpose.

baseURL      = "http://example/"
languageCode = "en-us"
title        = "Hugo's Letter to Bulma"

[params]
  description = """\
    Embrace, extend, and extinguish: Open Source with Daily Genuine Experience.
    From Coding, Design, Package Management Tweak to Desktop Customization.
    """
{{< / highlight >}}

#### Minimal Homepage

In order to have a working index.html you need this two artefacts.

* <code>content/_index.html</code>
  : [gitlab.com/.../content/_index.html][tutor-content-index]

The only different is, the content.

{{< highlight html >}}
  <p>There are so many things to say,
    to Bulma, Chici, and that Android 18.
    Hugo don't want to live in regrets.
    But Hugo have to go. So Hugo wrote this.</p>
{{< / highlight >}}

* <code>layouts/index.html</code>
  : [gitlab.com/.../layouts/index.html][tutor-layouts-index]

{{< highlight html >}}
<p>Dear Bulma,</p>

{{ .Content }}

<p>Yours sincerely.</p>
{{< / highlight >}}

Notice your first `Go html/template` language:

* `{{ .Content }}`.

#### Archetypes

You don't really need this `archetypes` artefact,
unless you create article from CLI.

Tips: Most of the times, I did copy paste from other articles,
using file manager. And never use this archetypes.

#### Server Output: HTML Source

Consider see the content again.

{{< highlight html >}}
$ curl localhost:1313
<p>Dear Bulma,</p>

<p>There are so many things to say,
    to Bulma, Chici, and that Android 18.
    Hugo don't want to live in regrets.
    But Hugo have to go. So Hugo wrote this.</p>


<p>Yours sincerely.</p>
{{< / highlight >}}

#### Server Output: Browser

Now check the generated page on your favorite browser:

![Hugo: Page on Browser][image-ss-01-minimal]

#### Tips: Multiple Sites

As a blogger, I can open two or more local Hugo sites at once.
In order to do this, I use different port such as:

{{< highlight bash>}}
$ hugo server --disableFastRender --port="1515"
{{< / highlight >}}

#### More Complete Article

I had wrote about Minimal Hugo and you can read here:

* [Hugo Minimal][local-hugo-minimal]

-- -- --

### What is Next ?

We are going to use theme, with layouts inside each theme.
Out first theme is still pure HTML with no CSS.
Consider continue reading [ [Hugo Layout][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2019/04/03/hugo-layout/

[tar-00-source]:        {{< assets-ssg >}}/2019/04/tutor-hugo-minimal.tar

[local-windows-choco]:  {{< baseurl >}}ssg/2019/01/21/windows-choco-hugo/
[local-hugo-minimal]:   {{< baseurl >}}ssg/2018/09/02/hugo-minimal/

[tutor-html-master]:    {{< tutor-html-bulma >}}/

[image-ss-01-hugo-bin]: {{< assets-ssg >}}/2019/04/01-hugo-binary-extended.png
[image-ss-01-minimal]:  {{< assets-ssg >}}/2019/04/01-minimal-homepage.png

[tutor-hugo-config]:    {{< tutor-hugo-bulma >}}/config.toml
[tutor-content-index]:  {{< tutor-hugo-bulma >}}/content/_index.html
[tutor-layouts-index]:  {{< tutor-hugo-bulma >}}/layouts/index.html
