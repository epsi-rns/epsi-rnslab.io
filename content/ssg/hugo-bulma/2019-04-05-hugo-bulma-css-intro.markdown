---
type   : post
title  : "Hugo - Bulma - CSS Introduction"
date   : 2019-04-05T09:17:35+07:00
slug   : hugo-bulma-css-intro
categories: [ssg]
tags      : [hugo, bulma, css]
keywords  : [static site, custom theme, layout, maximum width]
author : epsi
opengraph:
  image: assets/site/images/topics/hugo-bulma-markdown.png

toc    : "toc-2019-04-hugo-bulma-step"

excerpt:
  Building Hugo Site Step by step, with Bulma as stylesheet frontend.
  Explain How to Use Bulma Stylesheet in Hugo.

---

### Preface

This article is part one of Hugo Bulma CSS.

> Goal: Explain How to Use Bulma Stylesheet in Hugo

#### Hugo meet Bulma

	Hugo... This is Bulma.
	Bulma... This is Hugo.
	Don't be shy, both of you.

#### Source Code

You can download the source code of this article here.

* [tutor-hugo-02.tar][image-ss-02-source]

#### Related Article

I wrote about Bulma Navigation Bar that used in this article.
Step by step article, about building Navigation Bar.
You can read here:

* [Bulma Navigation Bar][local-bulma-navbar]

-- -- --

### 1: Preparation

Consider make an ecosystem for bulma first.

#### Create Theme

To avoid messed-up with previous tutorial,
consider make a new theme.

{{< highlight bash >}}
$ hugo new theme tutor-02
Creating theme at /media/Works/sites/tutor-hugo-bulma/themes/tutor-02
{{< / highlight >}}

{{< highlight bash >}}
$ cat config.toml
theme        = "tutor-02"
{{< / highlight >}}

{{< highlight bash >}}
$ cd themes/tutor-02 
{{< / highlight >}}

#### Copy CSS and JS

Copy `bulma.css` to <code>themes/tutor-02/static/css</code>.

{{< highlight bash >}}
$ tree static/css
static/css
└── bulma.css
{{< / highlight >}}

#### Copy Example Assets

Or even better, use our previous tutorial,
And copy example assets to <code>themes/tutor-02/static</code>.

{{< highlight bash >}}
$ tree static
static
├── css
│   ├── bulma.css
│   ├── bulma.css.map
│   ├── bulma.min.css
│   └── main.css
├── images
│   └── logo-gear.png
└── js
    ├── bulma-burger-jquery.js
    ├── bulma-burger-vue.js
    ├── jquery-slim.min.js
    └── vue.min.js
{{< / highlight >}}

![Hugo Bulma: Tree][image-ss-02-tree-bulma]

Note that, you might still need map files,
while debugging using object inspector.

**Source**:
	[gitlab.com/.../themes/tutor-02/static][tutor-hugo-static]

-- -- --

### 2: Add Bulma

Adding bulma is straightforward.
Just add the stylesheet to <code>site/head.html</code>.

However, there are these artefacts,
to make sure, we have the same copy.

#### Partial: HTML Head

* <code>themes/tutor-02/layouts/partials/site/head.html</code>
  : [gitlab.com/.../layouts/partials/site/head.html][tutor-hugo-layouts-head]

{{< highlight twig >}}
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>{{ .Page.Title | default .Site.Title }}</title>
  
    <link rel="stylesheet" type="text/css" href="{{ "css/bulma.css" | relURL }}">
</head>
{{< / highlight >}}

#### Layout: Homepage

No change, very similar to previous tutorial.
You can just copy from <code>tutor-01</code> to <code>tutor-02</code>.

* <code>themes/tutor-02/layouts/index.html</code>
  : [gitlab.com/.../layouts/index.html][tutor-hugo-layouts-index]

{{< highlight twig >}}
{{ define "main" }}
{{ .Content }}
{{ end }}
{{< / highlight >}}

#### Default: Baseof Template

I'm trying to have this file, as simple as possible.

* <code>layouts/_default/baseof.html</code>

{{< highlight twig >}}
<!DOCTYPE html>
<html lang="en">
{{ partial "site/head.html" . }}
<body>
  {{ .Content }}
</body>
</html>
{{< / highlight >}}

#### Server Output: Browser: Landing Page

Open in your favorite browser.
You should see, white background, by now.

* <http://localhost:1313/>

![Hugo Bulma: White Minimal][image-ss-02-browser-minimal]

-- -- --

### 3: Simple Layout

I put colors so you can get the idea,
about this responsive layout clearly.
I also put grid, just as an example.

> Consider refactor by layout

#### Default: Baseof Template

Consider using semantic HTML5 tag.

* <code>themes/tutor-02/layouts/_default/baseof.html</code>

{{< highlight html >}}
<!DOCTYPE html>
<html lang="en">
{{ partial "site/head.html" . }}
<body>
  <!-- header -->
  <nav class="navbar is-fixed-top is-dark">
    <div class="navbar-brand">
      <a class="navbar-item">
        Home
      </a>
    </div>
  </nav>

  <!-- main -->
  <div class="columns layout-base">
    <main role="main" 
          class="column is-two-thirds has-background-primary">
      <article class="blog-post">
        <h4 class="title is-4">{{ .Title | default .Site.Title }}</h4>
        {{ .Content }}
      </article>
    </main>

    <aside class="column is-one-thirds has-background-info">
      <div class="blog-sidebar">
        Side menu
      </div>
    </aside>
  </div>

  <!-- footer -->
  <footer class="site-footer">
    <div class="navbar is-fixed-bottom maxwidth
          is-dark has-text-centered is-vcentered">
      <div class="column">
        &copy; 2019.
      </div>
    </div>
  </footer>

</body>
</html>
{{< / highlight >}}

#### Partial: HTML Head

Add necessary stylesheet. the `main.css` right below `bulma.css`.
You should find the stylesheet on repository.

* <code>themes/tutor-02/layouts/partials/site/head.html</code>
  : [gitlab.com/.../layouts/partials/site/head.html][tutor-hugo-layouts-head]

{{< highlight twig >}}
<head>
    ...
  
    <link rel="stylesheet" type="text/css" href="{{ "css/bulma.css" | relURL }}">
    <link rel="stylesheet" type="text/css" href="{{ "css/main.css" | relURL }}">
</head>
{{< / highlight >}}

#### Assets: Custom Stylesheet

This is additional custom decoration stylesheet that required.
Something that does not covered with Bulma CSS Framework.

* <code>themes/tutor-02/css/main.css</code>
  : [gitlab.com/.../css/main.css][tutor-hugo-css-main]

{{< highlight css >}}
.layout-base {
  padding-top: 5rem;
}

footer.site/footer {
  padding-top: 5rem;
}

h1, h2, h3, h4, h5, h6 {
  font-family: "Playfair Display", Georgia, "Times New Roman", serif;
}

.blog-sidebar,
.blog-post {
  margin-left: 20px;
  margin-right: 20px;
}

.blog-post-meta {
  margin-bottom: 5px;
}
{{< / highlight >}}

#### Server Output: Browser

Open in your favorite browser.
You should see, layout in colour, by now.

* <http://localhost:1313/>

![Hugo Bulma: Coloured Layout][image-ss-02-browser-layout]

-- -- --

### 4: Simple Refactor

After minimalist bootstrap above,
the next step is to create the layout.
We are going to refactor

* <code>layouts/_default/baseof.html</code>

Into:

* <code>layouts/_default/baseof.html</code>

* <code>layouts/index.html</code>

* <code>layouts/partials/site/header.html</code>

* <code>layouts/partials/site/footer.html</code>

#### Default: Baseof Template

* <code>themes/tutor-02/layouts/_default/baseof.html</code>
  : [gitlab.com/.../layouts/_default/baseof.html][tutor-hugo-layouts-baseof]

{{< highlight twig >}}
<!DOCTYPE html>
<html lang="en">
{{ partial "site/head.html" . }}
<body>
{{ partial "site/header.html" . }}

  {{- block "general" . }}
    <div class="columns layout-base">
      {{- block "main" . }}
      {{ .Content }}
      {{- end }}

      {{- block "aside" . }}{{- end }}
    </div><!-- columns -->
  {{- end }}

{{ partial "site/footer.html" . }}
{{ partial "site/scripts.html" . }}
</body>
</html>
{{< / highlight >}}

I put the header/footer, outside main block.
So we do not need to repeatly writing header/footer in every main block.

#### Layout: Index

* <code>themes/tutor-02/layouts/index.html</code> (homepage)
  : [gitlab.com/.../layouts/index.html][tutor-hugo-layouts-index]
  
{{< highlight html >}}
{{ define "main" }}
  <main role="main" 
        class="column is-full has-background-primary">
    <article class="blog-post">
      <h4 class="title is-4">{{ .Title | default .Site.Title }}</h4>
      {{ .Content }}
    </article>
  </main>
{{ end }}
{{< / highlight >}}

#### Partial: Header

* <code>themes/tutor-02/layouts/partials/site/header.html</code>
  : [gitlab.com/.../layouts/partials/site/header.html][tutor-hugo-layouts-header]

{{< highlight html >}}
<nav class="navbar is-fixed-top is-dark">
  <div class="navbar-brand">
    <a class="navbar-item">
      Home
    </a>
  </div>
</nav>
{{< / highlight >}}

#### Partial: Footer

* <code>themes/tutor-02/layouts/partials/site/footer.html</code>
  : [gitlab.com/.../layouts/partials/site/footer.html][tutor-hugo-layouts-footer]

{{< highlight html >}}
<footer class="site-footer">
  <div class="navbar is-fixed-bottom
        is-white has-text-centered is-vcentered">
    <div class="column">
      &copy; {{ now.Year }}.
    </div>
  </div>
</footer>
{{< / highlight >}}

#### Partial: Javascript

It is just an empty template.

* <code>themes/tutor-02/layouts/partials/site/scripts.html</code>
  : [gitlab.com/.../layouts/partials/site/scripts.html][tutor-hugo-layouts-scripts]

{{< highlight html >}}
    <!-- JavaScript -->
    <!-- Placed at the end of the document so the pages load faster -->
{{< / highlight >}}

#### Server Output: Browser

Open in your favorite browser.
You should see, non-colored homepage, by now.

* <http://localhost:1313/>

![Hugo Bulma: Layout Homepage][image-ss-02-browser-homepage]

-- -- --

### What is Next ?

Consider continue reading next part of this article 
in [ [Hugo - Bulma - CSS Layout][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2019/04/06/hugo-bulma-css-layout/
[local-bulma-navbar]:       {{< baseurl >}}frontend/2019/03/03/bulma-navbar/

[image-ss-02-source]:       {{< assets-ssg >}}/2019/04/tutor-hugo-02.tar

[image-ss-02-tree-bulma]:   {{< assets-ssg >}}/2019/04/21-tree-bulma-asset.png
[image-ss-02-browser-minimal]:  {{< assets-ssg >}}/2019/04/21-browser-minimal.png
[image-ss-02-browser-layout]:   {{< assets-ssg >}}/2019/04/22-browser-layout.png
[image-ss-02-browser-homepage]: {{< assets-ssg >}}/2019/04/22-browser-homepage.png

[tutor-hugo-css-main]:          {{< tutor-hugo-bulma >}}/themes/tutor-02/css/main.css

[tutor-hugo-layouts-index]:     {{< tutor-hugo-bulma >}}/themes/tutor-02/layouts/index.html

[tutor-hugo-static]:            {{< tutor-hugo-bulma >}}/themes/tutor-02/static/
[tutor-hugo-layouts-baseof]:    {{< tutor-hugo-bulma >}}/themes/tutor-02/layouts/_default/baseof.html
[tutor-hugo-layouts-single]:    {{< tutor-hugo-bulma >}}/themes/tutor-02/layouts/_default/single.html
[tutor-hugo-layouts-list]:      {{< tutor-hugo-bulma >}}/themes/tutor-02/layouts/_default/list.html

[tutor-hugo-layouts-head]:      {{< tutor-hugo-bulma >}}/themes/tutor-02/layouts/partials/site/head.html
[tutor-hugo-layouts-header]:    {{< tutor-hugo-bulma >}}/themes/tutor-02/layouts/partials/site/header.html
[tutor-hugo-layouts-footer]:    {{< tutor-hugo-bulma >}}/themes/tutor-02/layouts/partials/site/footer.html
[tutor-hugo-layouts-scripts]:   {{< tutor-hugo-bulma >}}/themes/tutor-02/layouts/partials/site/scripts.html


