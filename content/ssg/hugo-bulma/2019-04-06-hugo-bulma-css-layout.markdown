---
type   : post
title  : "Hugo - Bulma - CSS Layout"
date   : 2019-04-06T09:17:35+07:00
slug   : hugo-bulma-css-layout
categories: [ssg]
tags      : [hugo, bulma, css]
keywords  : [static site, custom theme, navigation, header, footer, page, post]
author : epsi
opengraph:
  image: assets/site/images/topics/hugo-bulma-markdown.png

toc    : "toc-2019-04-hugo-bulma-step"

excerpt:
  Building Hugo Site Step by step, with Bulma as stylesheet frontend.
  Explain How to Use Bulma Stylesheet in Hugo.

---

### Preface

This article is part two of Hugo Bulma CSS.

> Goal: Explain How to Use Bulma Stylesheet in Hugo

#### Source Code

You can download the source code of this article here.

* [tutor-hugo-02.tar][image-ss-02-source]

#### Related Article

I wrote about Bulma Navigation Bar that used in this article.
Step by step article, about building Navigation Bar.
You can read here:

* [Bulma Navigation Bar][local-bulma-navbar]

-- -- --

### 5: Modify Layout: Navigation Bar

How about Navigation Bar ?
The next step after create is to enhance the already refactored layout.
I'm going to use Vue version for use with burger button.

* <code>layouts/partials/site/header.html</code>

* <code>layouts/partials/site/scripts.html</code>

#### Partial: Javascript

It is just an empty template.

* <code>themes/tutor-02/layouts/partials/site/scripts.html</code>
  : [gitlab.com/.../layouts/partials/site/scripts.html][tutor-hugo-layouts-scripts]

{{< highlight html >}}
    <!-- JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{{ "js/vue.min.js" | relURL }}"></script>
    <script src="{{ "js/bulma-burger-vue.js" | relURL }}"></script>
{{< / highlight >}}

#### Partial: Header

This is a loong Header, with logo image.

* <code>themes/tutor-02/layouts/partials/site/header.html</code>
  : [gitlab.com/.../layouts/partials/site/header.html][tutor-hugo-layouts-header]

{{< highlight html >}}
<nav role="navigation" aria-label="main navigation"
     class="navbar is-fixed-top is-dark"
     id="navbar-vue-app">
  <div class="navbar-brand">
    <a class="navbar-item" href="{{ .Site.BaseURL }}">
       <img src="{{ "images/logo-gear.png" | absURL }}"
           alt="Home" />
    </a>
    <a class="navbar-item">
      Home
    </a>
    <a class="navbar-item">
      Blog
    </a>
    
    <a role="button" class="navbar-burger burger" 
       aria-label="menu" aria-expanded="false" data-target="navbarBulma"
       @click="isOpen = !isOpen" v-bind:class="{'is-active': isOpen}">
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
    </a>
  </div>

  <div id="navbarBulma" class="navbar-menu"
       v-bind:class="{'is-active': isOpen}">
    <div class="navbar-start">
      <div class="navbar-item has-dropdown is-hoverable">
        <a class="navbar-link">
          Archives
        </a>

        <div class="navbar-dropdown">
          <a class="navbar-item">
            By Tags
          </a>
          <a class="navbar-item">
            By Category
          </a>
          <hr class="navbar-divider">
          <a class="navbar-item">
            By Date
          </a>
        </div>
      </div>

      <a class="navbar-item">
        About
      </a>
    </div>

    <div class="navbar-end">
      <form class="is-marginless" action="/pages/search/" method="get">
        <div class="navbar-item">
          <input class="" type="text" name="q"
            placeholder="Search..." aria-label="Search">
          &nbsp;
          <button class="button is-light" 
            type="submit">Search</button>
        </div>
      </form>
    </div>

  </div>
</nav>
{{< / highlight >}}

#### Server Output: Browser

Open in your favorite browser.
You should see, non-colored homepage, by now.

* <http://localhost:1313/>

![Hugo Bulma: Layout Navigation Bar][image-ss-02-browser-navbar]

-- -- --

### 6: Further Layout: Page and List.

After minimalist Bulma layout above,
the next step is to create the layout.
We are going to enhance our layout for 

* <code>layouts/_default/single.html</code>

* <code>layouts/_default/list.html</code>

#### Default: List

We should also change the list

* <code>themes/tutor-02/layouts/_default/list.html</code>
  : [gitlab.com/.../layouts/_default/list.html][tutor-hugo-layouts-list]

{{< highlight twig >}}
{{ define "main" }}
<main role="main" 
      class="column is-full has-background-light">
  <section class="section">
  <h4 class="title is-4">{{ .Section }}</h4>

  {{ .Content }}

  <ul>
    {{ range .Data.Pages }}
    <li>
      <a href="{{ .URL }}">{{ .Title }}</a>
    </li>
    {{ end }}
  </ul>
  </section>
</main>
{{ end }}
{{< / highlight >}}

#### Server Output: Browser: List

Open in your favorite browser.
You should see, simple index with article list, by now.

* <http://localhost:1313/letters/>

![Hugo Bulma: Layout List][image-ss-02-browser-list]

#### Default: Single, as I am

This one artefact, for content page, or post page.

* <code>themes/tutor-02/layouts/_default/single.html</code>
  : [gitlab.com/.../layouts/_default/single.html][tutor-hugo-layouts-single]

{{< highlight html >}}
{{ define "main" }}
  <main role="main" 
        class="column is-two-thirds has-background-primary">
    <article class="blog-post">
      <h4 class="title is-4">{{ .Title | default .Site.Title }}</h4>
      {{ .Content }}
    </article>
  </main>
{{ end }}

{{ define "aside" }}
  <aside class="column is-one-thirds has-background-info">
    <div class="blog-sidebar">
      Side menu
    </div>
  </aside>
{{ end }}
{{< / highlight >}}

#### Server Output: Browser: Page

Open in your favorite browser.
You should see, simple page content, by now.

* <http://localhost:1313/letters/winter/>

![Hugo Bulma: Layout Single][image-ss-02-browser-single]

-- -- --

### 7: Custom Page Posts

> Custom Page is a must have SSG Concept

Consider remake our simple <code>single.html</code>,
adding a more useful information, semantic, and bootstrap's classes.

#### Post: Single

This is very similar with `_default`.
The point is you can customize as creative as you want.

* <code>themes/tutor-02/layouts/post/single.html</code>
  : [gitlab.com/.../layouts/post/single.html][tutor-hugo-post-single]

{{< highlight html >}}
{{ define "main" }}
  <main role="main" 
        class="column is-two-thirds">
    <div class="blog-post">
      <section>
        <h4 class="title is-4">{{ .Title | default .Site.Title }}</h4>
        <p  class="blog-post-meta"
            >{{ .Page.Date.Format "January 02, 2006" }} by <a href="#">epsi</a></p>
      </section>

      <article>
        {{ .Content }}
      </article>
    </div><!-- /.blog-post -->
  </main>
{{ end }}

{{ define "aside" }}
  <aside class="column is-one-thirds has-background-info">
    <div class="blog-sidebar">
      <h4 class="is-size-4 is-italic">About You</h4>
      <p>Are you an angel ?</p>
    </div><!-- /.blog-sidebar -->
  </aside>
{{ end }}

{{< / highlight >}}

Most class are in official Bulma documentation.

#### Assets: Stylesheet

No need any custom stylesheet.
We have already packed the required class, before this section.

#### Example Content: Everyday

Consider make this one post as an example

* <code>content/posts/everyday.md</code>

{{< highlight markdown >}}
+++
type   = "post"
title  = "Everyday I Ask"
date   = 2015-10-03T08:08:15+07:00
slug   = "daily-wish"
tags   = ["wishful", "story"]
categories = ["love"]
+++

You know, some people makes mistakes,
and have to bear the consequences, for the rest of their life.
I made, one good decision when I was far younger than you are.
Later I know it was the right decision.
But I also have to bear the consquences, for the rest of my life.

> I wish you a good family, and happiness.
{{< / highlight >}}

#### Server Output: Browser

Open in your favorite browser.
You should see, a post, with sidebar.

* <http://localhost:1313/posts/>

![Hugo Bulma: Post without Layout][image-ss-02-browser-post]

Note that, this is already using responsive grid.
It has different looks on different screen.
You should try to resize your browser, to get the idea.

![Hugo Bulma: Post with sidebar Layout][image-ss-02-browser-post-grid]

-- -- --

### 7: Summary

So far here are artefacts for our layouts.

{{< highlight conf >}}
$ tree themes/tutor-02/layouts
themes/tutor-02/layouts
├── _default
│   ├── baseof.html
│   ├── list.html
│   └── single.html
├── index.html
└── partials
    └── site
        ├── footer.html
        ├── header.html
        ├── head.html
        └── scripts.html

3 directories, 8 files
{{< / highlight >}}

![Hugo Bulma: Artefacts Summary in Tree][image-ss-02-tree-layouts]

-- -- --

### What is Next ?

I feel you. Sometimes human get scared of new thing.
You might think you are alone  somewhere in a middle of jungle.
Do not give up. Do not be a quitter.
Yes you can finish until the end of the journey.

![confused cat][image-feral]

We are going to use SASS in theme, with custom SASS, all compiled into CSS.
Consider continue reading [ [Hugo - Bulma - SASS Introduction][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2019/04/07/hugo-bulma-sass-intro/
[local-bulma-navbar]:       {{< baseurl >}}frontend/2019/03/03/bulma-navbar/

[image-ss-02-source]:       {{< assets-ssg >}}/2019/04/tutor-hugo-02.tar
[image-feral]:              {{< baseurl >}}/assets/site/images/cats/ownerless.jpg

[image-ss-02-browser-navbar]:   {{< assets-ssg >}}/2019/04/22-browser-navbar.png
[image-ss-02-browser-list]:     {{< assets-ssg >}}/2019/04/22-browser-list.png
[image-ss-02-browser-single]:   {{< assets-ssg >}}/2019/04/22-browser-single.png
[image-ss-02-tree-layouts]:     {{< assets-ssg >}}/2019/04/23-tree-layouts.png
[image-ss-02-browser-post]:      {{< assets-ssg >}}/2019/04/25-browser-post.png
[image-ss-02-browser-post-grid]: {{< assets-ssg >}}/2019/04/25-browser-post-grid.png

[tutor-hugo-static]:            {{< tutor-hugo-bulma >}}/themes/tutor-02/static/
[tutor-hugo-layouts-baseof]:    {{< tutor-hugo-bulma >}}/themes/tutor-02/layouts/_default/baseof.html
[tutor-hugo-layouts-single]:    {{< tutor-hugo-bulma >}}/themes/tutor-02/layouts/_default/single.html
[tutor-hugo-layouts-list]:      {{< tutor-hugo-bulma >}}/themes/tutor-02/layouts/_default/list.html

[tutor-hugo-layouts-head]:      {{< tutor-hugo-bulma >}}/themes/tutor-02/layouts/partials/site/head.html
[tutor-hugo-layouts-header]:    {{< tutor-hugo-bulma >}}/themes/tutor-02/layouts/partials/site/header.html
[tutor-hugo-layouts-footer]:    {{< tutor-hugo-bulma >}}/themes/tutor-02/layouts/partials/site/footer.html
[tutor-hugo-layouts-scripts]:   {{< tutor-hugo-bulma >}}/themes/tutor-02/layouts/partials/site/scripts.html

[tutor-hugo-post-single]:       {{< tutor-hugo-bulma >}}/themes/tutor-02/layouts/post/single.html
