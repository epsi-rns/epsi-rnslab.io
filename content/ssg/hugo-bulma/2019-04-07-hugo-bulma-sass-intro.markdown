---
type   : post
title  : "Hugo - Bulma - SASS Introduction"
date   : 2019-04-07T09:17:35+07:00
slug   : hugo-bulma-sass-intro
categories: [ssg]
tags      : [hugo, bulma, sass]
keywords  : [static site, custom theme, compile sass, custom sass]
author : epsi
opengraph:
  image: assets/site/images/topics/hugo-bulma-markdown.png

toc    : "toc-2019-04-hugo-bulma-step"

excerpt:
  Building Hugo Step by step, with Bulma as stylesheet frontend.
  Apply SASS in Hugo theme.

---

### Preface

This article is intended for beginner.

> Goal: Apply SASS in Hugo theme

Custom SASS could make a lot of enhancement,
compared with pure Bulma's CSS.

#### Reading

If you do not have any idea about SASS you should read this first:

* <https://sass-lang.com/>

#### Source Code

You can download the source code of this article here.

* [tutor-hugo-03.tar][image-ss-03-source]

#### Related Article

I wrote about Bulma Navigation Bar that used in this article.
Step by step article, about building Navigation Bar.
You can read here:

* [Bulma SASS Introduction][local-bulma-sass-intro]

-- -- --

### 1: Prepare

This preparation is required.

#### Theme

Create directory

1. <code>themes/tutor-03/sass</code>

You should see the sass directory,
in this new <code>tutor-03</code> theme:

2. Edit <code>config.toml</code>, and save.

{{< highlight toml >}}
$ cat config.toml
theme        = "tutor-03"
{{< / highlight >}}

![Hugo Bulma SASS: Tree theme][image-ss-01-tree-themes]

3. Have a look at the example in repository.

**Source**:
	[gitlab.com/.../themes/tutor-03][tutor-hugo-tutor-03]

#### Example Source

Or we can just simplify the step,
by extracting the source code provided above.
You can read the detail of each SASS on 
[Bulma SASS Introduction][local-bulma-sass-intro].

Your <code>sass</code> directory should be looks like this.

{{< highlight conf >}}
$ tree themes/tutor-03/sass/ -L 2
themes/tutor-03/sass/
├── css
│   ├── _blog.sass
│   ├── _box-decoration.sass
│   ├── bulma.sass
│   ├── _decoration.sass
│   ├── _derived-variables.sass
│   ├── _initial-variables.sass
│   ├── _layout.sass
│   └── main.sass
└── vendors
    ├── bulma
    └── bulma.sass

{{< / highlight >}}

![Hugo Bulma SASS: Tree sass][image-ss-01-tree-sass]

**Source**:
	[gitlab.com/.../sass][tutor-hugo-sass]
_._

-- -- --

### 2: Compile Sass

There are some tools available.

#### Deprecated

Ruby-sass will be deprecated soon.

* <https://sass-lang.com/install>

As an alternative you can use dart-sass, node-sass.

* [Sass: Common Compiler][local-sass-compiler]

Or any task runner such as gulp or grunt.

* [Sass: Task Runner][local-sass-task-runner]

#### Using SASS

Using <code>themes/tutor-03</code> as working directory,
generating css is as simply as this command below.
The command will map any necessary input scss in <code>sass</code> directory,
into output css in <code>static</code> directory.

{{< highlight bash >}}
$ dart-sass --watch -I sass sass/css:static/css/ --source-map
Compiled sass/css/main.sass to static/css/main.css.
Compiled sass/css/bulma.sass to static/css/bulma.css.
Sass is watching for changes. Press Ctrl-C to stop.
{{< / highlight >}}

To avoid, typing command, over and over again,
cosider using `watch` argument.
This will continuously update as the input change.

![Hugo Bulma SASS: sass update][image-ss-03-sass-update]

**Result**:
	[gitlab.com/.../static/bulma][tutor-hugo-css-bulma]

#### Generated CSS

It is using the same place as previous CSS.
So there is no need to change the header.

<code>/themes/tutor-03/static/css/</code>
: [gitlab.com/.../static/css/][tutor-hugo-css-css].

Now check the browser, to check if there is no error.

#### Bulma SASS

We need to refresh our memory about what is in the `bulma.sass`.

Create <code>bulma.scss</code>
: [gitlab.com/.../sass/bulma.scss][tutor-hugo-sass-bulma].

{{< highlight scss >}}
@import "initial-variables"
@import "vendors/bulma"
@import "derived-variables"
{{< / highlight >}}

#### Main SASS

Also the `main.sass`.

Create <code>main.scss</code>
: [gitlab.com/.../sass/main.scss][tutor-hugo-sass-main].

{{< highlight scss >}}

@import "initial-variables"
@import "vendors/bulma/utilities/initial-variables"
@import "vendors/bulma/utilities/functions"
@import "vendors/bulma/utilities/derived-variables"
// @import "derived-variables"

@import "vendors/bulma/utilities/mixins"
@import "layout"
@import "decoration"
@import "box-decoration"
@import "blog"
{{< / highlight >}}

#### Custom Class

What new class provided in this `main.sass`?
This is the list:

* <code>maxwidth</code>

* <code>box-deco</code>,
  <code>header-deco</code>,
  <code>footer-deco</code>

-- -- --

### 3: Yet Another Design Example

We need to enhance the looks offered by all this new SASS.
So we can differ, from the previous pure Bulma CSS design,
in previous article.

#### Apply Custom Class on Layout

To see the changes we must apply the new class provided in or layout.
Or even better, consider change the design color.

* <code>layouts/_default/baseof.html</code>

* <code>layouts/index.html</code>

* <code>layouts/partials/site/header.html</code>

* <code>layouts/partials/site/footer.html</code>

#### Default: Baseof Template

* <code>themes/tutor-03/layouts/_default/baseof.html</code>
  : [gitlab.com/.../layouts/_default/baseof.html][tutor-hugo-layouts-baseof]

{{< highlight twig >}}
...
    <div class="columns is-8 layout-base maxwidth">
      {{- block "main" . }}
      {{ .Content }}
      {{- end }}

      {{- block "aside" . }}{{- end }}
    </div><!-- columns -->
...
{{< / highlight >}}

#### Layout: Index

* <code>themes/tutor-03/layouts/index.html</code> (homepage)
  : [gitlab.com/.../layouts/index.html][tutor-hugo-layouts-index]
  
{{< highlight html >}}
...
  <main role="main" 
        class="column is-full blog-column box-deco
               has-background-white has-text-centered">
...
{{< / highlight >}}

#### Partial: Header

* <code>themes/tutor-03/layouts/partials/site/header.html</code>
  : [gitlab.com/.../layouts/partials/site/header.html][tutor-hugo-layouts-header]

{{< highlight html >}}
<nav role="navigation" aria-label="main navigation"
     class="navbar is-fixed-top is-white maxwidth header-deco"
     id="navbar-vue-app">
...
{{< / highlight >}}

#### Partial: Footer

* <code>themes/tutor-03/layouts/partials/site/footer.html</code>
  : [gitlab.com/.../layouts/partials/site/footer.html][tutor-hugo-layouts-footer]

{{< highlight html >}}
<footer class="site-footer">
  <div class="navbar is-fixed-bottom maxwidth
        is-white has-text-centered is-vcentered footer-deco">
...
{{< / highlight >}}

#### Server Output: Browser

Open in your favorite browser.
You should see, non-colored homepage, by now.

* <http://localhost:1313/>

![Hugo Bulma: Layout Homepage][image-ss-03-browser-homepage]

-- -- --

### 4: Summary

As a summary, here is the content of scss input:

{{< highlight conf >}}
$ tree sass/css
sass/css
├── _blog.sass
├── _box-decoration.sass
├── bulma.sass
├── _decoration.sass
├── _derived-variables.sass
├── _initial-variables.sass
├── _layout.sass
└── main.sass
{{< / highlight >}}

![Hugo Bulma SASS: Tree Input Summary][image-ss-03-tree-input]

-._

And the content of css output would be:

{{< highlight conf >}}
$ tree static/css 
static/css
├── bulma.css
├── bulma.css.map
├── main.css
└── main.css.map
{{< / highlight >}}

![Hugo Bulma SASS: Tree Output Summary][image-ss-03-tree-output]

With map, you can debug in object inspector,
such as using inspect element menu in firefox based browser.

-- -- --

### What is Next ?

I do not go deep about sass, as this is not a bulma tutorial.

	Create necessary sass, as you need.
	However, still Bulma can save you,
	from writing a lot of CSS code.

Consider continue reading next part of this article 
in [ [Hugo - Bulma - SASS Layout][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2019/04/08/hugo-bulma-sass-layout/
[local-bulma-sass-intro]:   {{< baseurl >}}frontend/2019/03/04/bulma-sass-intro/

[local-sass-compiler]:      {{< baseurl >}}frontend/2019/01/13/sass-common-compiler/
[local-sass-task-runner]:   {{< baseurl >}}frontend/2019/01/15/sass-task-runner/

[image-ss-03-source]:       {{< assets-ssg >}}/2019/04/tutor-hugo-03.tar

[image-ss-01-tree-sass]:         {{< assets-ssg >}}/2019/04/31-tree-sass.png
[image-ss-01-tree-themes]:       {{< assets-ssg >}}/2018/09/31-tree-themes-03.png

[image-ss-03-sass-update]:       {{< assets-ssg >}}/2019/04/32-sass-update.png
[image-ss-03-browser-homepage]:  {{< assets-ssg >}}/2019/04/32-browser-homepage.png

[image-ss-03-tree-input]:        {{< assets-ssg >}}/2019/04/36-tree-input-summary.png
[image-ss-03-tree-output]:       {{< assets-ssg >}}/2019/04/36-tree-output-summary.png

[tutor-hugo-tutor-03]:           {{< tutor-hugo-bulma >}}/themes/tutor-03/
[tutor-hugo-sass]:               {{< tutor-hugo-bulma >}}/themes/tutor-03/sass/
[tutor-hugo-sass-main]:          {{< tutor-hugo-bulma >}}/themes/tutor-03/sass/css/main.scss
[tutor-hugo-sass-bulma]:         {{< tutor-hugo-bulma >}}/themes/tutor-03/sass/css/bulma.scss
[tutor-hugo-sass-bootswatch]:    {{< tutor-hugo-bulma >}}/themes/tutor-03/sass/css/bootswatch.scss
[tutor-hugo-css-bulma]:          {{< tutor-hugo-bulma >}}/themes/tutor-03/static/bulma/
[tutor-hugo-css-css]:            {{< tutor-hugo-bulma >}}/themes/tutor-03/static/css/
[tutor-hugo-css-main]:           {{< tutor-hugo-bulma >}}/themes/tutor-03/static/css/main.css
[tutor-hugo-css-bootswatch]:     {{< tutor-hugo-bulma >}}/themes/tutor-03/static/css/bootswatch.css
[tutor-hugo-sass-variables]:     {{< tutor-hugo-bulma >}}/themes/tutor-03/sass/css/_variables.scss

[tutor-hugo-layouts-baseof]:     {{< tutor-hugo-bulma >}}/themes/tutor-03/layouts/_default/baseof.html
[tutor-hugo-layouts-index]:      {{< tutor-hugo-bulma >}}/themes/tutor-03/layouts/index.html
[tutor-hugo-layouts-head]:       {{< tutor-hugo-bulma >}}/themes/tutor-03/layouts/partials/site/head.html
[tutor-hugo-layouts-header]:     {{< tutor-hugo-bulma >}}/themes/tutor-03/layouts/partials/site/header.html
[tutor-hugo-layouts-footer]:     {{< tutor-hugo-bulma >}}/themes/tutor-03/layouts/partials/site/footer.html


