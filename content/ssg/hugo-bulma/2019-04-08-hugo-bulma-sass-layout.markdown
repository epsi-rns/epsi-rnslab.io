---
type   : post
title  : "Hugo - Bulma - SASS Layout"
date   : 2019-04-08T09:17:35+07:00
slug   : hugo-bulma-sass-layout
categories: [ssg]
tags      : [hugo, bulma, sass]
keywords  : [static site, custom theme, page, list, custom page, post]
author : epsi
opengraph:
  image: assets/site/images/topics/hugo-bulma-markdown.png

toc    : "toc-2019-04-hugo-bulma-step"

excerpt:
  Building Hugo Step by step, with Bulma as stylesheet frontend.
  Apply SASS in Hugo theme.

---

### Preface

This article is intended for beginner.

> Goal: Apply SASS in Hugo theme

Custom SASS could make a lot of enhancement,
compared with pure Bulma's CSS.

#### Source Code

You can download the source code of this article here.

* [tutor-hugo-03.tar][image-ss-03-source]

#### Related Article

I wrote about Bulma Navigation Bar that used in this article.
Step by step article, about building Navigation Bar.
You can read here:

* [Bulma SASS Introduction][local-bulma-sass-intro]

-- -- --

### 5: Page and List.

Now comes the Hugo specific parts:

* <code>layouts/_default/single.html</code>

* <code>layouts/_default/list.html</code>

#### Default: List

We should also change the list

* <code>themes/tutor-03/layouts/_default/list.html</code>
  : [gitlab.com/.../layouts/_default/list.html][tutor-hugo-layouts-list]

{{< highlight twig >}}
{{ define "main" }}
<main role="main" 
      class="column is-full box-deco has-background-white">
...
{{ end }}
{{< / highlight >}}

#### Server Output: Browser: List

Open in your favorite browser.
You should see, simple index with article list, by now.

* <http://localhost:1313/letters/>

![Hugo Bulma: Layout List][image-ss-03-browser-list]

#### Default: Single, as I am

This one artefact, for content page, or post page.

* <code>themes/tutor-03/layouts/_default/single.html</code>
  : [gitlab.com/.../layouts/_default/single.html][tutor-hugo-layouts-single]

{{< highlight html >}}
{{ define "main" }}
  <main role="main" 
        class="column is-two-thirds blog-column box-deco has-background-white">
    <article class="blog-post">
      <h4 class="title is-4">{{ .Title | default .Site.Title }}</h4>
      {{ .Content }}
    </article>
  </main>
{{ end }}

{{ define "aside" }}
  <aside class="column is-one-thirds box-deco has-background-white">
    <div class="blog-sidebar">
      Side menu
    </div><!-- /.blog-sidebar -->
  </aside>
{{ end }}
{{< / highlight >}}

#### Server Output: Browser: Page

Open in your favorite browser.
You should see, simple page content, by now.

* <http://localhost:1313/letters/winter/>

![Hugo Bulma: Layout Single][image-ss-03-browser-single]

Now, already equipped with responsive layout.

-- -- --

### 6: Custom Page Posts

> Custom Page is a must have SSG Concept

Here we are again with our simple <code>single.html</code>.

#### Post: Single

This is very similar with `_default`.
The point is you can customize as creative as you want.

* <code>themes/tutor-03/layouts/post/single.html</code>
  : [gitlab.com/.../layouts/post/single.html][tutor-hugo-post-single]

{{< highlight html >}}
{{ define "main" }}
  <main role="main" 
        class="column is-two-thirds blog-column box-deco has-background-white">
    <div class="blog-post">
      <section>
        <h4 class="title is-4 blog-post-title">{{ .Title | default .Site.Title }}</h4>
        <p  class="blog-post-meta"
            >{{ .Page.Date.Format "January 02, 2006" }} by <a href="#">epsi</a></p>
      </section>

      <article>
        {{ .Content }}
      </article>
    </div><!-- /.blog-post -->
  </main>
{{ end }}

{{ define "aside" }}
  <aside class="column is-one-thirds box-deco has-background-white">
    <div class="blog-sidebar">
      <h4 class="is-size-4 is-italic">About You</h4>
      <p>Are you an angel ?</p>
    </div><!-- /.blog-sidebar -->
  </aside>
{{ end }}
{{< / highlight >}}

#### Server Output: Browser

Open in your favorite browser.
You should see, a post, with sidebar.

* <http://localhost:1313/posts/>

![Hugo Bulma: Post without Layout][image-ss-03-browser-post]

Note that, this is already using responsive grid.
It has different looks on different screen.
You should try to resize your browser, to get the idea.

![Hugo Bulma: Post with sidebar Layout][image-ss-03-browser-post-grid]

-- -- --

### What is Next ?

I think that's enough for today.
Correct me If I wrong.
But I think human need some rest too.

Consider relax, and look at this adorable kitten in my shoes.
Have a break for a while, prepare your playlist,
and resume our tutorial.

![adorable kitten][image-kitten]

Consider continue reading [ [Hugo Bulma - Section List][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2019/04/10/hugo-section-list/
[local-bulma-sass-intro]:   {{< baseurl >}}frontend/2019/03/04/bulma-sass-intro/

[image-ss-03-source]:       {{< assets-ssg >}}/2019/04/tutor-hugo-03.tar
[image-kitten]:             {{< baseurl >}}/assets/site/images/cats/shoes.jpg

[image-ss-03-browser-list]:     {{< assets-ssg >}}/2019/04/32-browser-list.png
[image-ss-03-browser-single]:   {{< assets-ssg >}}/2019/04/32-browser-single.png

[image-ss-03-browser-post]:      {{< assets-ssg >}}/2019/04/35-browser-post.png
[image-ss-03-browser-post-grid]: {{< assets-ssg >}}/2019/04/35-browser-post-grid.png

[tutor-hugo-layouts-single]:    {{< tutor-hugo-bulma >}}/themes/tutor-03/layouts/_default/single.html
[tutor-hugo-layouts-list]:      {{< tutor-hugo-bulma >}}/themes/tutor-03/layouts/_default/list.html
[tutor-hugo-post-single]:       {{< tutor-hugo-bulma >}}/themes/tutor-03/layouts/post/single.html
