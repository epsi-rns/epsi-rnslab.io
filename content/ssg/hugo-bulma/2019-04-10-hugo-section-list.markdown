---
type   : post
title  : "Hugo - Section List"
date   : 2019-04-10T09:17:35+07:00
slug   : hugo-section-list
categories: [ssg]
tags      : [hugo]
keywords  : [static site, custom theme, go html/template, archetypes, markdown, populate content, range loop, list by month]
author : epsi
opengraph:
  image: assets/site/images/topics/hugo-bulma-markdown.png

toc    : "toc-2019-04-hugo-bulma-step"

excerpt:
  Building Hugo Step by step, with Bulma as stylesheet frontend.
  Building content, and produce list for section.

---

### Preface

Again I rewrite this article, so beginner can understand.

> Goal: Building content, and produce list for section.

#### Source Code

You can download the source code of this article here.

* [tutor-hugo-04.tar][image-ss-04-source]

#### Related Article

I also wrote about Hugo Bootstrap Section List.
Step by step article, that you can read here:

* [Hugo - Bootstrap - Section List][local-hugo-section-list]

-- -- --

### 1: Section

I have been looking for a good content for these tutorial.
And believe me it is as hard as giving a good variable naming.
I look for simple content, such as churchill quotes,
also navier-stokes explanation,
and finally end up with quotes song lyric.

So yes, I decide to create a section, named quotes.

{{< highlight bash >}}
$ tree -d content
content
├── letters
├── pages
├── posts
└── quotes

4 directories
{{< / highlight >}}

> Distance between couples always a touchy tale.

#### Archetypes

Since every `quotes` having similar frontmatter,
we can make a frontmatter template,
so we do not have to type each time we make a new document.

Consider this archetypes for quotes.

* <code>themes/tutor-04/archetypes/quotes.md</code>
  : [gitlab.com/.../archetypes/quotes.md][tutor-hugo-archetypes-quotes]

{{< highlight markdown >}}
+++
type       = "post"
title      = "{{ replace .TranslationBaseName "-" " " | title }}"
date       = {{ .Date }}
categories = []
tags       = []
slug       = ""
author     = "epsi"
+++
{{< / highlight >}}

#### Permalink

For this quotes section, we can also set the permalink behaviour.

* <code>config.toml</code>
  : [gitlab.com/.../config.toml][tutor-hugo-config]

{{< highlight toml >}}
[permalinks]
  posts    = ":section/:year/:month/:day/:slug"
  quotes   = ":section/:year/:month/:day/:slug"
{{< / highlight >}}

-- -- --

### 2: Populate the Content

Now you can easily make ten content with archetypes.

#### Example Content

{{< highlight bash >}}
$ hugo new quotes/cranberries-dreaming-my-dreams.md
/home/epsi/tutor-hugo-bulma-04/content/quotes/cranberries-dreaming-my-dreams.md created
{{< / highlight >}}

![Hugo: Example Content][image-ss-04-hugo-my-dreams]

The result will be

* <code>content/quotes/cranberries-dreaming-my-dreams.md</code>
  : [gitlab.com/.../...my-dreams.md][tutor-hugo-content-my-dreams]

{{< highlight markdown >}}
+++
type       = "post"
title      = "Cranberries Dreaming My Dreams"
date       = 2019-05-08T04:34:02+07:00
categories = []
tags       = []
slug       = ""
author     = "epsi"
+++
{{< / highlight >}}

Edit the frontmatter and add content,
so that we have something similar as below:

{{< highlight markdown >}}
+++
type       = "post"
title      = "Cranberries - Dreaming my Dreams"
date       = 2015-03-15T07:35:05+07:00
categories = ["lyric"]
tags       = ["alternative", "90s"]
slug       = "cranberries-dreaming-my-dreams"
author     = "epsi"
+++

All the things you said to me today
Changed my perspective in every way
These things count to mean so much to me
Into my faith you and your baby

I'll be dreaming my dreams with you
I'll be dreaming my dreams with you
And there's no other place that I'd lay down my face
I'll be dreaming my dreams with you
{{< / highlight >}}

#### And The Rest

Consider do the rest for these nine lyrics:

{{< highlight bash >}}
$ for i in quotes/{\
"darius-rucker-think-about-it",\
"dashboard-confessional-stolen",\
"dashboard-confessional-vindicated",\
"julian-baker-sprained-ankle",\
"lisa-loeb-how",\
"m2m-the-day-you-went-away",\
"natalie-cole-i-wish-you-love",\
"paul-simon-jonah",\
"pixies-where-is-my-mind",\
"the-used-hard-to-say"}.md;\
do hugo new "$i"; done

/home/epsi/tutor-hugo-bulma-04/content/quotes/darius-rucker-think-about-it.md created
/home/epsi/tutor-hugo-bulma-04/content/quotes/dashboard-confessional-stolen.md created
/home/epsi/tutor-hugo-bulma-04/content/quotes/dashboard-confessional-vindicated.md created
/home/epsi/tutor-hugo-bulma-04/content/quotes/julian-baker-sprained-ankle.md created
/home/epsi/tutor-hugo-bulma-04/content/quotes/lisa-loeb-how.md created
/home/epsi/tutor-hugo-bulma-04/content/quotes/m2m-the-day-you-went-away.md created
/home/epsi/tutor-hugo-bulma-04/content/quotes/natalie-cole-i-wish-you-love.md created
/home/epsi/tutor-hugo-bulma-04/content/quotes/paul-simon-jonah.md created
/home/epsi/tutor-hugo-bulma-04/content/quotes/pixies-where-is-my-mind.md created
/home/epsi/tutor-hugo-bulma-04/content/quotes/the-used-hard-to-say.md created
{{< / highlight >}}

![Hugo: All Content][image-ss-04-hugo-new-all]

Also edit the frontmatter and add content,
for the rest of the nine lyrics.

-- -- --

### Layout: List: Simple Range

#### The Loop

The range is as simple as this loop below.

{{< highlight twig >}}
  {{ range .Data.Pages }}
    ...
  {{ end }}
{{< / highlight >}}

#### Simple Example

Consider begin with simple `list` layouts.

{{< highlight twig >}}
{{ define "main" }}
<main role="main" 
      class="column is-full box-deco has-background-white">
  <section class="section">
  <h4 class="title is-4">{{ .Section }}</h4>

  {{ .Content }}

  {{ range .Data.Pages }}
    <div><a href="{{ .URL | absURL }}">
      {{ .Title }}
    </a></div>
  {{ end }}
  </section>
</main>
{{ end }}
{{< / highlight >}}

#### Server Output: Browser

Now we can see the list:

* <http://localhost:1313/quotes/>

![Hugo: Simple List in Browser][image-ss-04-list-simple]

Now we need to design a more informative
template layout for this content list.

#### Custom Stylesheet

Add custom sass, so the list can have better looks.

* <code>themes/tutor-04/sass/css/_list.sass</code>
  : [gitlab.com/.../sass/css/_list.sass][tutor-hugo-sass-list].

{{< highlight scss >}}
// -- -- -- -- --
// _list.sass

div.archive-month,
div.archive-item
  position: relative

div.archive-item a:before
  position: absolute
  left: -15px

  font-family: "Font Awesome 5 Free"
  content: '\f054'
  color: $gray

div.archive-month:before
  position: absolute
  left: -20px

  font-family: "Font Awesome 5 Free"
  content: '\f073'
  color: $black

.archive-item:hover:before 
  color: $dark
{{< / highlight >}}

#### The Loop

The range loop, is still the same

{{< highlight twig >}}
  {{ range .Data.Pages }}
    ...
  {{ end }}
{{< / highlight >}}

_._

#### Styled Example

And customize with this custom stylesheet.

{{< highlight twig >}}
  {{ range .Data.Pages }}
    <div class="archive-item">
      <div class="is-pulled-left"><a href="{{ .URL | absURL }}">
        {{ .Title }}
      </a></div>
      <div class="is-pulled-right has-text-right"><time>
          {{ .Date.Format "02 Jan" }}&nbsp;
          <span class="fa fa-calendar"></span>
      </time></div>
      <div class="is-clearfix"></div>
    </div>
  {{ end }}
{{< / highlight >}}

#### Server Output: Browser

Now we can see the list:

* <http://localhost:1313/quotes/>

![Hugo: Styled List in Browser][image-ss-04-list-styled]

Now we need to design a more informative
template layout for this content list.

-- -- --

### Layout: List: Grouped Range: by Year (simple)

#### The Outer Loop

The range is as a little bit complex.

Instead of all kind of pages,
we filter it to show only post type.

{{< highlight twig >}}
  {{ $posts := where .Data.Pages "Type" "post" }}

  {{ range ($posts.GroupByDate "2006") }}
    ...
    {{ range .Pages }}
      ...
      {{ .Title }}
      ...
    {{ end }}
  {{ end }}
{{< / highlight >}}

#### Layout: Default List

Consider change this artefact:

* <code>themes/tutor-04/layouts/_default/list.html</code>
  : [gitlab.com/.../layouts/_default/list.html][tutor-hugo-layouts-list]

{{< highlight twig >}}
{{ define "main" }}
<main role="main" 
      class="column is-full box-deco has-background-white">
  <section class="section">
    <h4 class="title is-4">{{ .Section }}</h4>
  
    {{ .Content }}
  </section>

  {{ $posts := where .Data.Pages "Type" "post" }}
  
  {{ range ($posts.GroupByDate "2006") }}
    <section class="section" id="archive">
      {{ partial "summary/by-year.html" . }}
    </section>
  {{ end }}
</main>
{{ end }}
{{< / highlight >}}

#### Listing By Year

Instead of simple loop, we can group the article by year.
The detail of the partial layout can be seen here.

* <code>themes/tutor-04/layouts/partials/summary-by-year.html</code>
 : [gitlab.com/.../partials/summary-by-year.html][tutor-hugo-layouts-year]

{{< highlight twig >}}
  <div class ="anchor-target archive-year" 
       id="{{ .Key }}">{{ .Key }}</div>

  <div class="archive-list archive-p3">
    {{ range .Pages }}
    <div class="archive-item">
      <div class="is-pulled-left"><a href="{{ .URL | absURL }}">
        {{ .Title }}
      </a></div>
      <div class="is-pulled-right has-text-right"><time>
          {{ .Date.Format "02 Jan" }}&nbsp;
          <span class="fa fa-calendar"></span>
      </time></div>
      <div class="is-clearfix"></div>
    </div>
    {{ end }}
  </div>
{{< / highlight >}}

#### The Inner Loop

We move the inner range as this loop below.

{{< highlight twig >}}
  {{ range .Pages }}
    ...
    {{ .Title }}
    ...
  {{ end }}
{{< / highlight >}}

#### Custom Stylesheet

Add custom scss, so the list can have better looks.

* <code>themes/tutor-04/sass/css/_list.sass</code>
  : [gitlab.com/.../sass/css/_list.sass][tutor-hugo-sass-list].

{{< highlight scss >}}
// -- -- -- -- --
// _list.sass

.archive-p3
  padding-left: 15px

.archive-p4
  padding-left: 20px
  
// -- -- -- -- --
// Components: Tags

.archive-year
  font-weight: bold

// -- -- -- -- --
// Flip Flop

.archive-list
  .archive-item:nth-child(even)
    background-color: darken($white, 3%)

  .archive-item:nth-child(odd)
    background-color: darken($white, 5%)

  .archive-item:hover
    background-color: lighten($yellow, 10%)
{{< / highlight >}}

I won't go deep into sass,
as we have already an article covering sass in Bulma.

_._

#### Server Output: Browser

![Hugo: List by Year in Browser][image-ss-04-list-by-year]

-- -- --

### Layout: List: Grouped Range: by Year and Month

It is basically the same with the previous one.

#### Listing By Month

Of course, you can make, a more sophisticated listing,
by changing this artefact:

* <code>themes/tutor-04/layouts/_default/list.html</code>
  : [gitlab.com/.../layouts/_default/list.html][tutor-hugo-layouts-list]

{{< highlight twig >}}
{{ define "main" }}
<main role="main" 
  ...

  <section id="archive">
    {{ range ($posts.GroupByDate "2006") }}
      {{ partial "summary-by-month.html" . }}
    {{ end }}
  </section>
</main>
{{ end }}
{{< / highlight >}}

We use multiple loop stage. Outer: Group the article by year.
And inner: Group Article by month.

* <code>themes/tutor-04/layouts/partials/summary-by-month.html</code>
 : [gitlab.com/.../partials/summary-by-month.html][tutor-hugo-layouts-month]

{{< highlight twig >}}
  {{ $year := .Key }}
  <div class ="anchor-target archive-year" 
       id="{{ $year }}">
       {{- if eq .Key (now.Format "2006") -}}
         This year's posts ({{ $year }})
       {{- else -}}
         {{ .Key }}
       {{- end -}}
       </div>

  {{ range (.Pages.GroupByDate "January") }}
  {{ $month := .Key }}
  <div class="archive-p4">
    <div class ="archive-month" 
         id="{{ $year }}-{{ $month }}">
         {{ $month }} - {{ $year }}</div>

    <div class="archive-list archive-p3">
      {{ range .Pages }}
      <div class="archive-item">
        <div class="is-pulled-left"><a href="{{ .URL | absURL }}">
          {{ .Title }}
        </a></div>
        <div class="is-pulled-right has-text-right"><time>
            {{ .Date.Format "02 Jan" }}&nbsp;
            <span class="fas fa-calendar"></span></time></div>
        <div class="is-clearfix"></div>
      </div>
      {{ end }}
    </div>
  </div>
  {{ end }}
{{< / highlight >}}

#### The Inner Loop

This require nested loops as below:

{{< highlight twig >}}
  {{ range (.Pages.GroupByDate "January") }}
    {{ range .Pages }}
      ...
      {{ .Title }}
      ...
    {{ end }}
  {{ end }}
{{< / highlight >}}

#### Server Output: Browser

![Hugo: List by Month in Browser][image-ss-04-list-by-month]

-- -- --

### What is Next ?

There are, some interesting topic about <code>Taxonomy in Hugo</code>. 
Consider continue reading [ [Hugo - Taxonomy][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2019/04/11/hugo-taxonomy/
[local-hugo-section-list]:  {{< baseurl >}}ssg/2018/09/10/hugo-section-list/

[image-ss-04-source]:       {{< assets-ssg >}}/2019/04/tutor-hugo-04.tar

[image-ss-04-hugo-new-all]:      {{< assets-ssg >}}/2019/04/42-hugo-new-quote-all.png
[image-ss-04-hugo-my-dreams]:    {{< assets-ssg >}}/2019/04/42-hugo-new-quote-dreams.png
[image-ss-04-list-simple]:       {{< assets-ssg >}}/2019/04/42-list-simple.png
[image-ss-04-list-styled]:       {{< assets-ssg >}}/2019/04/42-list-styled.png
[image-ss-04-list-by-month]:     {{< assets-ssg >}}/2019/04/43-list-by-month.png
[image-ss-04-list-by-year]:      {{< assets-ssg >}}/2019/04/43-list-by-year.png

[tutor-hugo-archetypes-quotes]:  {{< tutor-hugo-bulma >}}/themes/tutor-04/archetypes/quotes.md
[tutor-hugo-config]:             {{< tutor-hugo-bulma >}}/config.toml
[tutor-hugo-content-my-dreams]:  {{< tutor-hugo-bulma >}}/content/quotes/cranberries-dreaming-my-dreams.md

[tutor-hugo-layouts-list]:       {{< tutor-hugo-bulma >}}/themes/tutor-04/layouts/_default/list.html
[tutor-hugo-sass-main]:          {{< tutor-hugo-bulma >}}/themes/tutor-04/sass/css/main.sass
[tutor-hugo-sass-list]:          {{< tutor-hugo-bulma >}}/themes/tutor-04/sass/css/_list.sass

[tutor-hugo-layouts-month]:      {{< tutor-hugo-bulma >}}/themes/tutor-04/layouts/partials/summary-by-month.html
[tutor-hugo-layouts-year]:       {{< tutor-hugo-bulma >}}/themes/tutor-04/layouts/partials/summary-by-year.html
