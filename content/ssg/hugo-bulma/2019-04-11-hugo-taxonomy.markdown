---
type   : post
title  : "Hugo - Taxonomy"
date   : 2019-04-11T09:17:35+07:00
slug   : hugo-taxonomy
categories: [ssg]
tags      : [hugo]
keywords  : [static site, custom theme, configuration, go html/template, taxonomy, terms]
author : epsi
opengraph:
  image: assets/site/images/topics/hugo-bulma-markdown.png

toc    : "toc-2019-04-hugo-bulma-step"

excerpt:
  Building Hugo Step by step, with Bulma as stylesheet frontend.
  Create custom taxonomy terms layout and also taxonomy detail layout.

---

### Preface

This feature in Hugo almost tears my eyes.
Consider see how it works.

> Goal: Create custom taxonomy terms layout and also taxonomy detail layout.

TThere are two artefacts required, `terms.html` and `taxonomy.html`.
We need to concentrate on the loop for both artefacts.

#### Source Code

You can download the source code of this article here.

* [tutor-hugo-04.tar][image-ss-04-source]

#### Related Article

I also wrote about Hugo Bootstrap Taxonomy.
Step by step article, that you can read here:

* [Hugo - Bootstrap - Taxonomy][local-hugo-taxonomy]

Again I rewrite this article.
A slight modification for Bulma.

-- -- --

### 1: Prepare

Remember our last <code>config.toml</code>.
We already have these taxonomies:

* <code>config.toml</code>
  : [gitlab.com/.../config.toml][tutor-hugo-config]

{{< highlight toml >}}
# An example of Hugo site for tutorial purpose.

baseURL      = "http://example.org/"
languageCode = "en-us"
title        = "Letters to my Beloved"
theme        = "tutor-04"

[taxonomies]
  category = "categories"
  tag = "tags"

[permalinks]
  posts    = ":section/:year/:month/:day/:slug"
  quotes   = ":section/:year/:month/:day/:slug"
{{< / highlight >}}

In fact.

I already put both `terms.html` and `taxonomy.html` in `tutor-03`.

-- -- --

### 2: Taxonomy Terms

Instead of the standard terms.html,
we can make it prettier, by making our own custom `terms.html`.

#### The Loop

The range is as simple as this loop below.

{{< highlight twig >}}
  {{ range $key, $value := .Data.Terms }}
    ...
  {{ end }}
{{< / highlight >}}

#### Default: Terms: Simple

Create this artefact:

* <code>themes/tutor-03/layouts/_default/terms.html</code>

{{< highlight twig >}}
{{ define "main" }}
<main role="main" 
      class="column is-full box-deco has-background-white">

  <section class="section">
    <h4 class="title is-4">{{ .Section }}</h4>
  
    {{ .Content }}
  </section>

  <section id="archive">
    {{ range $key, $value := .Data.Terms }}
      <div id="{{ $key }}">
        {{ $.Data.Singular | humanize }}: 
        <a href="{{ "/" | relLangURL }}{{ $.Data.Plural | urlize }}/{{ $key | urlize }}">
        {{ $key }}</a>
      </div>
    {{ end }}
  </section>
</main>
{{ end }}
{{< / highlight >}}

#### Server Output: Browser: Terms: Simple

Open in your favorite browser.
You should see, a page, with tags.

* <http://localhost:1313/tags/>

![Hugo Bulma: Terms][image-ss-03-browser-terms]

#### Default: Terms: with Stylesheet

Or even better, consider apply Bulma stylesheet to this `terms` layouts.

Consider to change this artefact:

* <code>themes/tutor-03/layouts/_default/terms.html</code>
  : [gitlab.com/.../layouts/_default/terms.html][tutor-hugo-layouts-terms]

{{< highlight twig >}}
{{ define "main" }}
<main role="main" 
      class="column is-full box-deco has-background-white">

  <section class="section">
    <h4 class="title is-4">{{ .Section }}</h4>
  
    {{ .Content }}
  </section>

  <div class="field is-grouped is-grouped-multiline">
    {{ range $key, $value := .Data.Terms }}
    <div class="tags has-addons">
      {{ $posts := where $value.Pages "Type" "post" }}
      {{ $postCount := len $posts -}}
      <a href="{{ "/" | relLangURL }}{{ $.Data.Plural | urlize }}/{{ $key | urlize }}">
        <div class="tag is-light">{{ $key }}
        </div><div class="tag is-dark">{{ $postCount }}
        </div>
      </a>
    </div>
    &nbsp;
    {{- end }}
    <div class="tags dummy"></div>
  </div>
</main>
{{ end }}
{{< / highlight >}}

Notice that all equipped with Bulma Class.

#### The Loop

The range is as simple as this loop below.
But be aware of the count.
We need to filter them only for `post` kind page.

{{< highlight twig >}}
  {{ range $key, $value := .Data.Terms }}
    ...
    {{ $posts := where $value.Pages "Type" "post" }}
    {{ $postCount := len $posts -}}
    ...
  {{ end }}
{{< / highlight >}}

#### Server Output: Browser: Terms: with Stylesheet

Open in your favorite browser.
You should see, a page, with tags.

* <http://localhost:1313/tags/>

![Hugo Bulma: Terms with Stylesheet][image-ss-03-browser-styled]

#### Default: Terms: with Tree

It would be nice if your `terms` list,
also contain a list of each article,
but filter the result to contain only **post** type,
and excluding the other page kind.

Again consider to change this artefact:

* <code>themes/tutor-03/layouts/_default/terms.html</code>
  : [gitlab.com/.../layouts/_default/terms.html][tutor-hugo-layouts-terms]

{{< highlight twig >}}
{{ define "main" }}
<main role="main" 
      class="column is-full box-deco has-background-white">

  <div class="field is-grouped is-grouped-multiline">
    {{ range $key, $value := .Data.Terms }}
    <div class="tags has-addons">
      {{ $posts := where $value.Pages "Type" "post" }}
      {{ $postCount := len $posts -}}
      <a href="{{ "/" | relLangURL }}{{ $.Data.Plural | urlize }}/{{ $key | urlize }}">
        <div class="tag is-light">{{ $key }}
        </div><div class="tag is-dark">{{ $postCount }}
        </div>
      </a>
    </div>
    &nbsp;
    {{- end }}
    <div class="tags dummy"></div>
  </div>

  <section class="section">
    <h4 class="title is-4">{{ .Section }}</h4>
  
    {{ .Content }}
  </section>

  <section class="archive-p3" id="archive">
    {{ range $key, $value := .Data.Terms }}
      <div id="{{ $key }}" class ="anchor-target">
        <span class="fa fa-folder"></span> 
        {{ $.Data.Singular | humanize }}: 
        <a href="{{ "/" | relLangURL }}{{ $.Data.Plural | urlize }}/{{ $key | urlize }}">
        {{ $key }}</a>
      </div>

      <div class="archive-list archive-p3">
      {{ range where $value.Pages "Type" "post" }}
      <div class="archive-item meta-item">
        <div class="meta_link has-text-right">
          <time class="meta_time is-pulled-right"
                datetime="{{ .Date.Format "2006-01-02T15:04:05Z07:00" }}">
            {{ .Date.Format "Jan 02, 2006" }}
            &nbsp;<span class="fa fa-calendar"></span></time></div>
        <div class="is-pulled-left"><a href="{{ .URL | absURL }}">
          {{ .Title }}
        </a></div>
        <div class="is-clearfix"></div>
      </div>
      {{ end }}
      </div>

    {{ end }}
  </section>
</main>
{{ end }}
{{< / highlight >}}

Notice that, I have added FontAwesome for comfort.

#### The Loop

We also need to filter them only for `post` kind page.
This require nested loops as below:

{{< highlight twig >}}
  {{ range $key, $value := .Data.Terms }}
    ...
    {{ range where $value.Pages "Type" "post" }}
      ...
      {{ .Title }}
      ...
    {{ end }}
    ...
  {{ end }}
{{< / highlight >}}

#### Server Output: Browser: Terms: with Tree

Open in your favorite browser.
You should see, a page, with tags.

* <http://localhost:1313/categories/>

![Hugo Bulma: Terms with Tree][image-ss-04-browser-tree]

#### Test

I remove the post type from this frontmatter.

{{< highlight toml >}}
+++
title      = "M2M - The Day You Went Away"
date       = 2014-03-15T07:35:05+07:00
categories = ["lyric"]
tags       = ["pop", "90s"]
slug       = "m2m-the-day-you-went-away"
author     = "epsi"
+++
{{< / highlight >}}

This article won't show up nomore as post in either `terms.html`,
and `taxonomy.html`.

-- -- --

### 3: Taxonomy Detail

You can click for each `terms` and have the detail,
such as love badge or lyric badge:

* <http://localhost:1313/categories/lyric/>

Which will be using the <code>layouts/_default/list.html</code>
Of course we can make our own `taxonomy.html` to show taxonomy detail.
Now we need to make the `taxonomy.html` artefact.

#### The Loop

The range needs tob filtered,
to include only `post` kind page as this loop below.

{{< highlight twig >}}
  {{ range where .Data.Pages "Type" "post" }}
    ...
  {{ end }}
{{< / highlight >}}

#### Default: Taxonomy: Simple

Consider to create this artefact:

* <code>themes/tutor-04/layouts/_default/taxonomy.html</code>
  : [gitlab.com/.../layouts/_default/taxonomy.html][tutor-hugo-layouts-taxonomy]

{{< highlight twig >}}
{{ define "main" }}
<main role="main" 
      class="column is-full box-deco has-background-white">
  <section class="section">
    <h4 class="title is-4">{{ .Section }}</h4>
  
    {{ .Content }}
  </section>

  <section class="section" id="archive">
  {{ range where .Data.Pages "Type" "post" }}
    <div class="archive-item">
      <div class="is-pulled-left"><a href="{{ .URL | absURL }}">
        {{ .Title }}
      </a></div>
      <div class="is-pulled-right has-text-right"><time>
          {{ .Date.Format "02 Jan" }}&nbsp;
          <span class="fa fa-calendar"></span>
      </time></div>
      <div class="is-clearfix"></div>
    </div>
  {{ end }}
  </section>

</main>
{{ end }}
{{< / highlight >}}

#### Server Output: Browser: Taxonomy: Simple

Open in your favorite browser.
You should see, a page, contain link.

* <http://localhost:1313/categories/lyric/>

![Hugo Bulma: Simple Taxonomy][image-ss-04-taxonomy-simple]

#### Default: Taxonomy: Summary by Year

Just like `list`, we can have the summary by year as below:

* <code>themes/tutor-04/layouts/_default/taxonomy.html</code>
  : [gitlab.com/.../layouts/_default/taxonomy.html][tutor-hugo-layouts-taxonomy]

{{< highlight twig >}}
{{ define "main" }}
<main role="main" 
      class="column is-full box-deco has-background-white">
  <section class="section">
    <h4 class="title is-4">{{ .Section }}</h4>
  
    {{ .Content }}
  </section>
  
  {{ $posts := where .Data.Pages "Type" "post" }}

  {{ range ($posts.GroupByDate "2006") }}
    <section class="section" id="archive">
      {{ partial "summary/by-year.html" . }}
    </section>
  {{ end }}
</main>
{{ end }}
{{< / highlight >}}

The detail of the partial layout can be seen here.

* <code>themes/tutor-04/layouts/partials/summary-by-year.html</code>
 : [gitlab.com/.../partials/summary-by-year.html][tutor-hugo-layouts-year]
 
As already discussed in previous article.

#### The Outer Loop

Again, the range is as a little bit complex.
As already discussed in previous article.

{{< highlight twig >}}
  {{ $posts := where .Data.Pages "Type" "post" }}

  {{ range ($posts.GroupByDate "2006") }}
    ...
      {{ partial "summary/by-year.html" . }}
    ...
  {{ end }}
{{< / highlight >}}

#### Server Output: Browser: Taxonomy: Year

And summary by month as below:

![Hugo Bulma: Taxonomy by Year][image-ss-04-taxonomy-year]

#### Default: Taxonomy: Summary by Month

* <code>themes/tutor-04/layouts/_default/taxonomy.html</code>
  : [gitlab.com/.../layouts/_default/taxonomy.html][tutor-hugo-layouts-taxonomy]

{{< highlight twig >}}
{{ define "main" }}
<main role="main" 
      class="column is-full box-deco has-background-white">
  <section class="section">
    <h4 class="title is-4">{{ .Section }}</h4>
  
    {{ .Content }}
  </section>
  
  {{ $posts := where .Data.Pages "Type" "post" }}

  {{ range ($posts.GroupByDate "2006") }}
    <section class="section" id="archive">
      {{ partial "summary/by-month.html" . }}
    </section>
  {{ end }}
</main>
{{ end }}
{{< / highlight >}}

The detail of the partial layout can be seen here.

* <code>themes/tutor-04/layouts/partials/summary-by-month.html</code>
 : [gitlab.com/.../partials/summary-by-month.html][tutor-hugo-layouts-month]

As already discussed in previous article.

#### Server Output: Browser: Taxonomy: Month

![Hugo Bulma: Taxonomy by Month][image-ss-04-taxonomy-month]

-- -- --

### What is Next ?

Feel sleepy ? 
Although it is a good idea to finish your tutorial.
Just like this kitten, you may sleep anytime you want.
Have a break for a while, feed your pet,
and resume our tutorial.

![adorable kitten][image-kitten]

There are, some interesting topic about <code>Blog Archive in Hugo</code>. 
Consider continue reading [ [Hugo - Archive][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2019/04/12/hugo-archive/
[local-hugo-taxonomy]:  {{< baseurl >}}ssg/2018/09/11/hugo-taxonomy/

[image-ss-04-source]:   {{< assets-ssg >}}/2019/04/tutor-hugo-04.tar
[image-kitten]:             {{< baseurl >}}/assets/site/images/cats/rambu-02.jpg

[image-ss-03-browser-terms]:    {{< assets-ssg >}}/2019/04/37-browser-terms.png
[image-ss-03-browser-styled]:   {{< assets-ssg >}}/2019/04/37-browser-terms-styled.png
[image-ss-03-browser-taxonomy]: {{< assets-ssg >}}/2019/04/37-browser-taxonomy.png
[image-ss-04-browser-tree]:     {{< assets-ssg >}}/2019/04/44-browser-terms-tree.png
[image-ss-04-taxonomy-simple]:  {{< assets-ssg >}}/2019/04/44-browser-taxonomy-simple.png
[image-ss-04-taxonomy-year]:    {{< assets-ssg >}}/2019/04/44-browser-taxonomy-year.png
[image-ss-04-taxonomy-month]:   {{< assets-ssg >}}/2019/04/44-browser-taxonomy-month.png

[tutor-03-layouts-terms]:        {{< tutor-hugo-bulma >}}/themes/tutor-03/layouts/_default/terms.html
[tutor-03-layouts-taxonomy]:     {{< tutor-hugo-bulma >}}/themes/tutor-03/layouts/_default/taxonomy.html

[tutor-hugo-config]:             {{< tutor-hugo-bulma >}}/config.toml
[tutor-hugo-layouts-terms]:      {{< tutor-hugo-bulma >}}/themes/tutor-04/layouts/_default/terms.html
[tutor-hugo-layouts-taxonomy]:   {{< tutor-hugo-bulma >}}/themes/tutor-04/layouts/_default/taxonomy.html

[tutor-hugo-layouts-month]:      {{< tutor-hugo-bulma >}}/themes/tutor-04/layouts/partials/summary-by-month.html
[tutor-hugo-layouts-year]:       {{< tutor-hugo-bulma >}}/themes/tutor-04/layouts/partials/summary-by-year.html
