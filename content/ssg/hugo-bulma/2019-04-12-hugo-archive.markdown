---
type   : post
title  : "Hugo - Archive"
date   : 2019-04-12T09:17:35+07:00
slug   : hugo-archive
categories: [ssg]
tags      : [hugo]
keywords  : [static site, custom theme, go html/template, archive, chronogical list]
author : epsi
opengraph:
  image: assets/site/images/topics/hugo-bulma-markdown.png

toc    : "toc-2019-04-hugo-bulma-step"

excerpt:
  Building Hugo Step by step, with Bulma as stylesheet frontend.
  Create blog archive, a site wide content list,
  for all specific page kind named post.

---

### Preface

> Goal: Create blog archive, a site wide content list for all posts.

#### Source Code

You can download the source code of this article here.

* [tutor-hugo-04.tar][image-ss-04-source]

#### Related Article

I also wrote about Hugo Bootstrap Archive.
Step by step article, that you can read here:

* [Hugo - Bootstrap - Archive][local-hugo-archive]

Again I rewrite this article.
A slight modification for Bulma.

-- -- --

### 1: Archive Page by Group

We can make a page dedicated for archiving all posts.
I always have this page, 
to show a shortcut to all article links.

#### Prepare All Artefacts

{{< highlight bash >}}
$ mkdir themes/tutor-04/layouts/archives
$ touch themes/tutor-04/layouts/archives/single.html

$ mkdir content/pages
$ touch content/pages/archives.md
{{< / highlight >}}

#### Content: Single

Just an empty fronmatter without the need of any content.

* <code>content/pages/archives.md</code>
  : [gitlab.com/.../pages/archives.md][tutor-hugo-content-archives]

{{< highlight toml >}}
+++
type  = "archives"
title = "Archives by Date"
+++
{{< / highlight >}}

#### Layout: Single

* <code>themes/tutor-04/layouts/archives/single.html</code>
  : [gitlab.com/.../layouts/archives/single.html][tutor-hugo-layouts-single]

{{< highlight twig >}}
{{ define "main" }}
<main role="main" 
      class="column is-full box-deco has-background-white">
  <section class="section">
    <h4 class="title is-4">{{ .Title | default .Site.Title }}</h4>
  
    {{ .Content }}
  </section>

  {{ $posts := where .Site.Pages "Type" "post" }}

  {{ range ($posts.GroupByDate "2006") }}
    <section class="section" id="archive">
      {{ partial "summary/by-month.html" . }}
    </section>
  {{ end }}
</main>
{{ end }}
{{< / highlight >}}

#### Partial: Summary

Already discussed in previous article.

#### Server Output: Browser

And see the result

* <http://localhost:1313/pages/archives/>

![Hugo: Archive by Group][image-ss-04-archive-single]

The different of this page and the taxonomy page is:
the list filtered for specific page kind named post.

-- -- --

### 2: Chronogical Archive List

And you can also have page list with chronological order.
Later we will combine this with nice pagination.

#### Prepare All Artefacts

{{< highlight bash >}}
$ touch themes/tutor-04/layouts/archives/list.html
$ touch themes/tutor-04/layouts/partials/summary-blog-list.html
$ touch content/pages/_index.md
{{< / highlight >}}

#### Content: List

Also no content required. Frontmatter is sufficient.

* <code>content/pages/_index.md</code>
  : [gitlab.com/.../pages/_index.md][tutor-hugo-content-index]

{{< highlight markdown >}}
+++
type  = "archives"
title = "Blog List"
+++
{{< / highlight >}}

#### The Loop

The range is as this loop below.

{{< highlight twig >}}
  {{ range where .Site.Pages "Type" "post" }}
    ...
  {{ end }}
{{< / highlight >}}

There is no need for nested loop.
But we need to put the details to partials to reduce complexity.

#### Layout: List

* <code>themes/tutor-04/layouts/archives/list.html</code>
  : [gitlab.com/.../layouts/archives/list.html][tutor-hugo-layouts-list]

{{< highlight twig >}}
{{ define "main" }}
<main role="main" 
      class="column is-full box-deco has-background-white">
  <section class="section">
    <h4 class="title is-4">{{ .Section }}</h4>
  
    {{ .Content }}
  </section>

  {{ $posts := where .Site.Pages "Type" "post" }}

  <div class="post-list">
  {{ range $posts }}
    <section class="section" id="archive">
      {{ partial "summary/blog-list.html" . }}
    </section>
  {{ end }}
  </div>

</main>
{{ end }}
{{< / highlight >}}

#### Partial: Summary

Where the summary layout template is similar as code below:

* <code>themes/tutor-04/layouts/partials/summary-blog-list.html</code>
 : [gitlab.com/.../partials/summary-blog-list.html][tutor-hugo-layouts-blog]

{{< highlight twig >}}
    <div class="meta-item blog-item">

      <strong><a class="meta_link" 
        href="{{ .Permalink }}">{{ .Title }}
      </strong></a>

      <div class="meta">
        <div class="meta_time is-pulled-left">
          <i class="fa fa-calendar"></i>
          <time datetime="{{ .Date.Format "2006-01-02T15:04:05Z07:00" }}">
          {{ .Date.Format "Jan 2, 2006" }}</time>
        </div>      
        <div class="is-pulled-right" id="meta_tags">
          {{ range .Params.tags }}
            <a href="{{ printf "tags/%s" . | absURL }}">
              <span class="tag is-dark"><span class="fa fa-tag"></span>{{ . }}</span></a>
          {{ end }}
        </div>
      </div> 

      <div class="is-clearfix"></div>
      
      <div>
        {{ if .Params.Excerpt -}}
          {{ .Params.Excerpt }}
        {{- else -}}
          {{ .Summary }}
        {{- end }}
      </div>

      <div class="read-more is-light">
        <a href="{{ .Permalink }}" 
           class="button is-dark is-small">Read More&nbsp;</a>
      </div>

    </div>
{{< / highlight >}}

I'm using bulma with FontAwesome to have pretty tag icons.

#### Server Output: Browser

Consider see the result as usual.

* <http://localhost:1313/pages/>

![Hugo: Chronological Blog Archive][image-ss-04-archive-list]

-- -- --

### 3: Real World Navigation

Now that our archives layout and taxonomy pages has complete,
Consider change the header.

![Hugo: Wide Navigation][image-ss-04-header-wide]

* <code>themes/tutor-04/layouts/partials/site-header.html</code>
  : [gitlab.com/.../layouts/partials/site/header.html][tutor-hugo-layouts-header]

{{< highlight html >}}
<nav role="navigation" aria-label="main navigation"
     class="navbar is-fixed-top is-white maxwidth header-deco"
     id="navbar-vue-app">
  <div class="navbar-brand">
    <a class="navbar-item" href="{{ .Site.BaseURL }}">
       <img src="{{ "images/logo-gear.png" | absURL }}"
           alt="Home" />
    </a>
    <a class="navbar-item" href="{{ "pages" | absURL }}">
      Blog
    </a>
    
    <a role="button" class="navbar-burger burger" 
       aria-label="menu" aria-expanded="false" data-target="navbarBulma"
       @click="isOpen = !isOpen" v-bind:class="{'is-active': isOpen}">
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
    </a>
  </div>

  <div id="navbarBulma" class="navbar-menu"
       v-bind:class="{'is-active': isOpen}">
    <div class="navbar-start">
      <div class="navbar-item has-dropdown is-hoverable">
        <a class="navbar-link">
          Archives
        </a>

        <div class="navbar-dropdown">
          <a class="navbar-item" href="{{ "tags" | absURL }}">
            By Tags
          </a>
          <a class="navbar-item" href="{{ "categories" | absURL }}">
            By Category
          </a>
          <hr class="navbar-divider">
          <a class="navbar-item" href="{{ "pages/archives" | absURL }}">
            By Date
          </a>
        </div>
      </div>

      <a class="navbar-item">
        About
      </a>
    </div>

    <div class="navbar-end">
      <form class="is-marginless" action="/pages/search/" method="get">
        <div class="navbar-item">
          <input class="" type="text" name="q"
            placeholder="Search..." aria-label="Search">
          &nbsp;
          <button class="button is-light" 
            type="submit">Search</button>
        </div>
      </form>
    </div>

  </div>
</nav>
{{< / highlight >}}

#### Server Output: Browser

Now you can see a usable navigation bar in your smartphone.

![Hugo: Small Navigation][image-ss-04-header-small]

We will add nice pagination later.

-- -- --

### What is Next ?

There are, some interesting topic about <code>Hugo Custom Content Type</code>.
Consider continue reading [ [Hugo - Custom Content Type][local-whats-next] ].


Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2019/04/13/hugo-custom-output/
[local-hugo-archive]:   {{< baseurl >}}ssg/2018/09/12/hugo-archive/

[image-ss-04-source]:   {{< assets-ssg >}}/2019/04/tutor-hugo-04.tar

[image-ss-04-archive-list]:      {{< assets-ssg >}}/2019/04/46-archives-list.png
[image-ss-04-archive-single]:    {{< assets-ssg >}}/2019/04/46-archives-single.png
[image-ss-04-header-small]:      {{< assets-ssg >}}/2019/04/46-layout-header-small.png
[image-ss-04-header-wide]:       {{< assets-ssg >}}/2019/04/46-layout-header-wide.png

[tutor-hugo-content-archives]:   {{< tutor-hugo-bulma >}}/content/pages/archives.md
[tutor-hugo-content-index]:      {{< tutor-hugo-bulma >}}/content/pages/_index.md
[tutor-hugo-layouts-single]:     {{< tutor-hugo-bulma >}}/themes/tutor-04/layouts/archives/single.html
[tutor-hugo-layouts-list]:       {{< tutor-hugo-bulma >}}/themes/tutor-04/layouts/archives/list.html
[tutor-hugo-layouts-blog]:       {{< tutor-hugo-bulma >}}/themes/tutor-04/layouts/partials/summary-blog-list.html
[tutor-hugo-layouts-header]:     {{< tutor-hugo-bulma >}}/themes/tutor-04/layouts/partials/site-header.html
