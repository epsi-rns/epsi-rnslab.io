---
type   : post
title  : "Hugo - Custom Content Type"
date   : 2019-04-13T09:17:35+07:00
slug   : hugo-custom-output
categories: [ssg]
tags      : [hugo]
keywords  : [static site, custom theme, go html/template, custom output, json, text, yaml]
author : epsi
opengraph:
  image: assets/site/images/topics/hugo-bulma-markdown.png

toc    : "toc-2019-04-hugo-bulma-step"

excerpt:
  Building Hugo Step by step, with Bulma as stylesheet frontend.
  More about blog archive example, from just html, 
  to custom content output such as json, text and yaml.

---

### Preface

> Goal: Custom content output example: json, text, yaml

#### Source Code

You can download the source code of this article here.

* [tutor-hugo-04.tar][image-ss-04-source]

#### Related Article

I also wrote about Hugo Bootstrap Custom Output.
Step by step article, that you can read here:

* [Hugo - Bootstrap - Custom Content][local-hugo-custom]

I do not rewrite this article.
It is exactly the same as the previous one,
only with different content.

-- -- --

### 1: Prepare

We can make a custom content for archiving.

#### Content: Archives

Remember our last archive page ?

* <code>content/pages/archives.md</code>
  : [gitlab.com/.../pages/archives.md][tutor-hugo-content-archives]

{{< highlight toml >}}
+++
type  = "archives"
title = "Archives by Date"
+++
{{< / highlight >}}

Instead of just html page, we will enhanced this archive,
to output different custom content type.

{{< highlight toml >}}
+++
type  = "archives"
title = "Archives by Date"
outputs = ["html", "txt", "yml", "json"]
+++
{{< / highlight >}}

Why do we even need these custom outputs ?

* The HTML, we already seen in previous chapter

* The JSON artefact as a search data source for lunr json.

* The YAML artefact to build data source for related pages,
  something that you usually seen in wordpress posts.

* The plain text artefact, is just another demo.
  Because not every browser can handle yaml type,
  I use the plain fo easiness reason

#### Main Configuration

In order to do this,
we need to make a little adjustment in our beloved config file.
Making some new definition in config.toml.

* <code>config.toml</code>
  : [gitlab.com/.../config.toml][tutor-hugo-config]

{{< highlight toml >}}
# An example of Hugo site using Bulma for personal learning purpose.

...

[mediaTypes]
  [mediaTypes."text/yaml"]
    suffixes = ["yml"]

[outputFormats]
  [outputFormats.yml]
    baseName    = "index"
    mediaType   = "text/yaml"
    isPlainText = true
  [outputFormats.txt]
    baseName    = "index"
    mediaType   = "text/plain"
    isPlainText = true
{{< / highlight >}}

* JSON is already a standard output type.
  So we don't need to change anything.

* On the other hand, we need to define YAML mediaTypes.

* However, we still need to define txt output formats.

Note that, newer Hugo require <code>suffixes</code>,
instead of <code>suffix</code>.

-- -- --

### 2: JSON

#### Reading

* <https://forestry.io/blog/build-a-json-api-with-hugo/>

#### Preparation

We need these two artefacts.

{{< highlight bash >}}
$ touch themes/tutor-04/layouts/archives/baseof.json.json
$ touch themes/tutor-04/layouts/archives/single.json.json
{{< / highlight >}}

We have already this archives.md, no need to recreate.

* <code>content/pages/archives.md</code>
  : [gitlab.com/.../pages/archives.md][tutor-hugo-content-archives]

{{< highlight bash >}}
$ cat content/pages/archives.md
{{< / highlight >}}

#### Layout: Baseof: JSON

Our base is as simply as this below.

* <code>themes/tutor-04/layouts/archives/baseof.json.json</code>
  : [gitlab.com/.../layouts/archives/baseof.json.json][tutor-hugo-layouts-baseof-json]


{{< highlight twig >}}
{{ block "main" .}}{{ end }}
{{< / highlight >}}

#### Layout: Single: JSON

The detail is in 

* <code>themes/tutor-04/layouts/archives/single.json.json</code>
  : [gitlab.com/.../layouts/archives/single.json.json][tutor-hugo-layouts-single-json]

{{< highlight twig >}}
{{ define "main" }}
{{- $posts := where .Site.Pages "Type" "post" -}}
{{- $postCount := len $posts -}}
{
  {{ range $i, $e := $posts }}
    "{{ .URL | urlize }}": {
      "title": {{ jsonify $e.Title }},
      "content": {{ jsonify ($e.Params.excerpt | default $e.Summary) }},
      "url": {{ jsonify .Permalink }},
      "author": {{ jsonify $e.Params.author }},
      "category": {{ jsonify $e.Section  }}
    }
    {{- if not (eq (add $i 1) $postCount) }},{{ end -}}
  {{ end }}
}
{{ end }}
{{< / highlight >}}

#### Server Output: Browser

Now you can see JSON output in your favorite browser.

* <code>http://localhost:1313/pages/archives/index.json</code>

This will produce something similar as shown below:

{{< highlight json >}}
{
    "/quotes/2014/03/25/the-used-hard-to-say/": {
      ...
    },
    "/quotes/2014/01/15/paul-simon-jonah/": {
      "title": "Paul Simon - Jonah",
      "content": "No one gives their dreams away too lightly They hold them tightly Warm against cold\nI know Jonah He was swallowed by a song",
      "url": "http://localhost:1313/quotes/2014/01/15/paul-simon-jonah/",
      "author": "epsi",
      "category": "quotes"
    }
}
{{< / highlight >}}

Modern browser automatically format,
the JSON output in nice view collapsable view.

![Hugo: JSON Content Type][image-ss-04-browser-json]

-- -- --

### 3: Text and YAML

#### Preparation

We need these two artefacts.

{{< highlight bash >}}
$ touch themes/tutor-04/layouts/archives/baseof.yml.yml
$ touch themes/tutor-04/layouts/archives/single.yml.yml
$ touch themes/tutor-04/layouts/archives/baseof.txt.txt
$ touch themes/tutor-04/layouts/archives/single.txt.txt
{{< / highlight >}}

We have already the archives.md.

#### Layout: Baseof: Text and YAML

Our base for both yml and txt, have the same content.

* <code>themes/tutor-04/layouts/archives/baseof.yml.yml</code>
  : [gitlab.com/.../layouts/archives/baseof.yml.yml][tutor-hugo-layouts-baseof-yml]
  
* <code>themes/tutor-04/layouts/archives/baseof.txt.txt</code>
  : [gitlab.com/.../layouts/archives/baseof.txt.txt][tutor-hugo-layouts-baseof-txt]

Our base for both yml and txt, are as simply as this below.

{{< highlight twig >}}
{{ block "main" .}}{{ end }}
{{< / highlight >}}

#### Layout: Single: Text and YAML

The detail are also the same for both

* <code>themes/tutor-04/layouts/archives/single.yml.yml</code>
  : [gitlab.com/.../layouts/archives/single.yml.yml][tutor-hugo-layouts-single-yml]
  
* <code>themes/tutor-04/layouts/archives/single.txt.txt</code>
  : [gitlab.com/.../layouts/archives/single.txt.txt][tutor-hugo-layouts-single-txt]

You can create anything that suit your own needs.

{{< highlight twig >}}
{{- define "main" -}}
# Helper for related links
# {{ .Title }}

{{ range (where .Site.Pages "Type" "post") }}
  {{- if .Date }}
- id: {{ .Date.Format "06010204" }}
  title: "{{ .Title }}"
  url: {{ .URL }}
  {{ end -}}
{{ end }}

{{- end -}}
{{< / highlight >}}

#### Server Output: Browser: Text

Now you can see plain text output in your favorite browser.

* <code>http://localhost:1313/pages/archives/index.txt</code>

This will produce something like:

{{< highlight yaml >}}
# Helper for related links
# Archives by Date

- id: 18091335
  title: "Mothers - No Crying in Baseball"
  url: /quotes/2018/09/13/mothers-no-crying-in-baseball/
  
- id: 18090735
  title: "Julian Baker - Something"
  url: /quotes/2018/09/07/julian-baker-something/
{{< / highlight >}}

![Hugo: Text Content Type][image-ss-04-browser-text]

So we can easily copy paste the output to any text editor.

#### Server Output: Browser: YAML

Now you can download YAML output in your favorite browser.

* <code>http://localhost:1313/pages/archives/index.yml</code>

Since no handle for text/yaml in browser,
the browser will ask if it should be downloaded

![Hugo: YAML Content Type][image-ss-04-browser-yaml]

So we can easily copy downloaded files using any file manager.

-- -- --

### 4: Summary

Since we have four type, we have four single layouts,
as shown in tree below.

{{< highlight bash >}}
$ tree themes/tutor-04/layouts/archives/
themes/tutor-04/layouts/archives/
├── baseof.json.json
├── baseof.txt.txt
├── baseof.yml.yml
├── list.html
├── single.html
├── single.json.json
├── single.txt.txt
└── single.yml.yml
{{< / highlight >}}

![Hugo: Archive Summary][image-ss-04-summary-archive]

-- -- --

### What is Next ?

There are, some interesting topic,
about <code>Hugo miscellanous range loop</code>,
such in a `sidebar`.
Consider continue reading [ [Hugo - Side Panel][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2019/04/14/hugo-widget/
[local-hugo-custom]:    {{< baseurl >}}ssg/2018/09/13/hugo-custom-output/

[image-ss-04-source]:   {{< assets-ssg >}}/2019/04/tutor-hugo-04.tar

[image-ss-04-summary-archive]:   {{< assets-ssg >}}/2019/04/47-archives-summary.png
[image-ss-04-browser-json]:      {{< assets-ssg >}}/2019/04/47-browser-json.png
[image-ss-04-browser-text]:      {{< assets-ssg >}}/2019/04/47-browser-txt.png
[image-ss-04-browser-yaml]:      {{< assets-ssg >}}/2019/04/47-browser-yml.png

[tutor-hugo-config]:             {{< tutor-hugo-bulma >}}/config.toml
[tutor-hugo-content-archives]:   {{< tutor-hugo-bulma >}}/content/pages/archives.md

[tutor-hugo-layouts-single-json]: {{< tutor-hugo-bulma >}}/themes/tutor-04/layouts/archives/single.json.json
[tutor-hugo-layouts-baseof-json]: {{< tutor-hugo-bulma >}}/themes/tutor-04/layouts/archives/baseof.json.json
[tutor-hugo-layouts-single-yml]:  {{< tutor-hugo-bulma >}}/themes/tutor-04/layouts/archives/single.yml.yml
[tutor-hugo-layouts-baseof-yml]:  {{< tutor-hugo-bulma >}}/themes/tutor-04/layouts/archives/baseof.yml.yml
[tutor-hugo-layouts-single-txt]:  {{< tutor-hugo-bulma >}}/themes/tutor-04/layouts/archives/single.txt.txt
[tutor-hugo-layouts-baseof-txt]:  {{< tutor-hugo-bulma >}}/themes/tutor-04/layouts/archives/baseof.txt.txt
