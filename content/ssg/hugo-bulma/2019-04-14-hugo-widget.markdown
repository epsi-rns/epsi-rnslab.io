---
type   : post
title  : "Hugo - Widget"
date   : 2019-04-14T09:17:35+07:00
slug   : hugo-widget
categories: [ssg, frontend]
tags      : [hugo]
keywords  : [static site, custom theme, widget, recent post, categories, tags, archives]
author : epsi
opengraph:
  image: assets/site/images/topics/hugo-bulma-markdown.png

toc    : "toc-2019-04-hugo-bulma-step"

excerpt:
  Building Hugo Step by step, with Bulma as stylesheet frontend.
  Miscellanous range loop to achieved wordpress like side panel widget.
  Also using bulma.

---

### Preface

> Goal: Miscellanous range loop to achieved wordpress like widget.

We can make a sidebar, so that we can have more information,
just like what common in wordpress site or other blogging platform.
I'm using custom css based on Bulma box to suit example needs,
and of course you can use any class other than this custom css.

#### Source Code

You can download the source code of this article here.

* [tutor-hugo-04.tar][image-ss-04-source]

#### Related Article: SASS

The SASS part of this article is already discussed in this two article:

* [Bulma SASS - Custom Class][local-sass-custom]

* [Bulma SASS - Panels][local-sass-panels]

It is time to reuse the class for real world situation.

#### Related Article: Hugo

I also wrote about Hugo Bootstrap Aside.
Step by step article, that you can read here:

* [Hugo - Bootstrap - Custom Content][local-hugo-custom]

Again, a rewrite.

-- -- --

### 1: Prepare

#### Theme

Still with `tutor-04`.

* <code>config.toml</code>
  : [gitlab.com/.../config.toml][tutor-hugo-config]

{{< highlight toml >}}
theme        = "tutor-04"
{{< / highlight >}}

#### Layout: Single

Our final layout would be:

* <code>themes/tutor-04/layouts/post/single.html</code>
  : [gitlab.com/.../layouts/post/single.html][tutor-hugo-layouts-post-single]

{{< highlight twig >}}
{{ define "main" }}
...
{{ end }}

{{ define "aside" }}
  <aside class="col-md-4">
    {{ partial "widget/recent-post.html" . }}
  </aside>
{{ end }}
{{< / highlight >}}

The beauty of this Go html/template,
is that we can define other template in the same file artefact,
even when we do not really use it.
For example I put this `aside-long`, in the same file artefact,
so that I can copy-paste to `aside` template whenever I need.

{{< highlight twig >}}
{{ define "aside-long" }}
  <aside class="col-md-4">
    {{ partial "widget/recent-post.html" . }}
    {{ partial "widget/archives.html" . }}

    {{ partial "widget/categories.html" . }}
    {{ partial "widget/tags.html" . }}

    {{ partial "widget/friends.html" . }}    
    {{ partial "widget/related-posts.html" . }}
  </aside>
{{ end }}
{{< / highlight >}}

We are going to discuss each partial, one by one.

#### SASS: List

As already mention in other article,
You can see the code here:

* <code>themes/tutor-04/sass/css/_list.scss</code>
  : [gitlab.com/.../sass/css/_list.scss][tutor-hugo-sass-list].

{{< highlight css >}}
// -- -- -- -- --
// _list.scss

...
{{< / highlight >}}

-._

-- -- --

### 2: Simple Example

Consider start with simple example.
We need something without any loop, `go html/template` tag or whatsoever.
Pure HTML with a list of URLs,
that you might need to show on your side panel.

#### Layout: Single

Consider examine only recent posts:

* <code>themes/tutor-04/layouts/post/single.html</code>
  : [gitlab.com/.../layouts/post/single.html][tutor-hugo-layouts-post-single]

{{< highlight twig >}}
{{ define "aside" }}
  <aside class="side column is-one-thirds is-paddingless">
    {{ partial "widget/affiliates.html" . }}
  </aside>
{{ end }}
{{< / highlight >}}

#### Partial: Widget

* <code>themes/tutor-04/layouts/partials/widget/recent-posts.html</code>
  : [gitlab.com/.../layouts/partials/widget/recent-posts.html][tutor-hugo-layouts-recent]

{{< highlight html >}}
<section class="panel is-light">
  <div class="panel-header">
    <p>Affiliates</p>
    <span class="fa fa-child"></span>
  </div>
  <div class="panel-body has-background-white">
    <ul class="panel-list">
      <li><a href="http://epsi-rns.github.io/"
            >Linux/BSD Desktop Customization</a></li>
      <li><a href="http://epsi-rns.gitlab.io/"
            >Mobile/Web Development Blog</a></li>
      <li><a href="http://oto-spies.info/"
            >Car Painting and Body Repair.</a></li>
    </ul>
  </div>
</section>
{{< / highlight >}}

#### Server Output: Browser

Just open one of the quote post:

* <http://localhost:1313/quotes/2015/03/15/cranberries-dreaming-my-dreams/>

Now you can see the result in the browser.

![Hugo: Widget Affiliates][image-ss-01-widget-affiliates]

This the basic of HTML class required for the rest of this article.

-- -- --

### 3: Recent Post

#### Layout: Single

Consider examine only recent posts:

* <code>themes/tutor-04/layouts/post/single.html</code>
  : [gitlab.com/.../layouts/post/single.html][tutor-hugo-layouts-post-single]

{{< highlight twig >}}
{{ define "aside" }}
  <aside class="side column is-one-thirds is-paddingless">
    {{ partial "widget/recent-post.html" . }}
  </aside>
{{ end }}
{{< / highlight >}}

#### Partial: Widget

* <code>themes/tutor-04/layouts/partials/widget/recent-posts.html</code>
  : [gitlab.com/.../layouts/partials/widget/recent-posts.html][tutor-hugo-layouts-recent]

{{< highlight twig >}}
<section class="panel is-light">
  <div class="panel-header">
    <p>Recent Post</p>
    <span class="fa fa-newspaper"></span>
  </div>
  <div class="panel-body has-background-white">
    <ul class="panel-list">
      {{ range first 5 (where .Site.Pages "Type" "post") }}
      <li><a href="{{ .URL | absURL }}">{{ .Title }}</a></li>
      {{ end }}
    </ul>
  </div>
</section>
{{< / highlight >}}

#### The Loop

I limit the result for only first five result.

{{< highlight twig >}}
      {{ range first 5 (where .Site.Pages "Type" "post") }}
        ...
      {{ end }}
{{< / highlight >}}

#### Server Output: Browser

Now you can see the result in the browser.

![Hugo: Widget Recent Posts][image-ss-01-widget-recent-posts]

-- -- --

### 4: Categories and Tags

Both are Taxonomies.
So the layouts is pretty similar.

#### Partial: Widget: Categories

* <code>themes/tutor-04/layouts/partials/widget/categories.html</code>
  : [gitlab.com/.../layouts/partials/widget/categories.html][tutor-hugo-layouts-categories]

{{< highlight twig >}}
<section class="panel is-light">
  <div class="panel-header">
    <p>Categories</p>
    <span class="fa fa-tag"></span>
  </div>
  <div class="panel-body has-background-white">
    {{ range $name, $taxonomy := .Site.Taxonomies.categories }}
      <a class="tag is-small is-info" href="{{ printf "categories/%s" $name | absURL }}">
        {{ $name }}&nbsp;<span class="fa fa-folder"></span>
      </a>&nbsp;
    {{ end }}
  </div>
</section>
{{< / highlight >}}

#### The Loop

The loop is self explanatory.

{{< highlight twig >}}
    {{ range $name, $taxonomy := .Site.Taxonomies.categories }}
      ...
    {{ end }}
{{< / highlight >}}

#### Server Output: Browser

Now you can see the result in the browser.

![Hugo: Widget Categories][image-ss-01-widget-categories]

#### Partial: Widget: Tags

Consider make the code above a little bit more complex.
We can pass argument to side panels.
Here we can have a choice of tag icon to be shown,
`after` or `before`.

* <code>themes/tutor-04/layouts/post/single.html</code>
  : [gitlab.com/.../layouts/post/single.html][tutor-hugo-layouts-post-single]

{{< highlight twig >}}
{{ define "aside" }}
  <aside class="side column is-one-thirds is-paddingless">
    {{ partial "widget/tags.html" (dict "taxonomies" .Site.Taxonomies "view_tag_type" "before") }}
  </aside>
{{ end }}
{{< / highlight >}}

* <code>themes/tutor-04/layouts/partials/widget/tags.html</code>
  : [gitlab.com/.../layouts/partials/widget/tags.html][tutor-hugo-layouts-tags]

{{< highlight twig >}}
{{/* view_tag_type : 'before' or 'after' */}}

<section class="panel is-light">
  <div class="panel-header">
    <p>Tags</p>
    <span class="fa fa-tag"></span>
  </div>
  <div class="panel-body has-background-white">
    {{ if eq .view_tag_type "after" }}
      {{ range $name, $taxonomy := .taxonomies.tags }}
        <a class="tag is-small is-light has-background-white"
           href="{{ printf "tags/%s" $name | absURL }}">
          {{ $name }}&nbsp;<span class="fa fa-folder"></span>
        </a>&nbsp;
      {{ end }}
    {{ else }}
      {{ range $name, $taxonomy := .taxonomies.tags }}
        <a class="tag is-small is-light has-background-white"
           href="{{ printf "tags/%s" $name | absURL }}">
          <span class="fa fa-folder"></span>&nbsp;{{ $name }}
        </a>&nbsp;
      {{ end }}
    {{ end }}
  </div>
</section>
{{< / highlight >}}

* _._

#### The Loop

The loop is still self explanatory.

{{< highlight twig >}}
    {{ range $name, $taxonomy := .Site.Taxonomies.tags }}
      ...
    {{ end }}
{{< / highlight >}}

#### Server Output: Browser

Now you can see the result in the browser.

![Hugo: Widget Tags][image-ss-01-widget-tags]

-- -- --

### 5: Archives

Remember the Archives tutorial in a previous chapter ?
We can also apply this in similar fashion.

#### Partial: Widget

* <code>themes/tutor-04/layouts/partials/widget/archives.html</code>
  : [gitlab.com/.../layouts/partials/widget/archives.html][tutor-hugo-layouts-archives]

{{< highlight twig >}}
<section class="panel is-light">
  <div class="panel-header">
    <p>Archives</p>
    <span class="fa fa-archive"></span>
  </div>
  <div class="panel-body has-background-white">
  {{ $posts := where .Site.Pages "Type" "post" }}
  {{ $page_year   := .Page.Date.Format "2006" }}
  {{ $page_month  := .Page.Date.Format "January" }}
    
  {{ range ($posts.GroupByDate "2006") }}  
    {{ $year := .Key }}
      <div class ="archive-year" id="{{ $year }}">
        <a href="{{ "pages/archives" | absURL }}#{{ $year }}">
        {{ $year }}</a>
      </div>

    {{ if eq $year $page_year }}
      <ul class="panel-archive">
      {{ range (.Pages.GroupByDate "January") }}
      {{ $month := .Key }}
      <li class="list-month">
        
        <span id="{{ $year }}-{{ $month }}">
              <a href="{{ "pages/archives" | absURL }}#{{ $year }}-{{ $month }}">
              {{ $month }}</a> - {{ $year }}</span>

        {{ if eq $month $page_month }}
          <ul class="panel-list">
          {{ range $post_month := .Pages }}

          <li>
            <a href="{{ $post_month.URL | absURL }}">
              {{ $post_month.Title }}
            </a>
          </li>
          {{ end }}
          </ul>
        {{ end }}

      </li>
      {{ end }}
      </ul>
    {{ end }}

  {{ end }}
  </div>
</section>
{{< / highlight >}}

#### The Loop

I know it is complex.

{{< highlight twig >}}
  {{ range ($posts.GroupByDate "2006") }}  
    {{ $year := .Key }}
    ...

    {{ if eq $year $page_year }}
      {{ range (.Pages.GroupByDate "January") }}
      {{ $month := .Key }}
      ...

        {{ if eq $month $page_month }}
          {{ range $post_month := .Pages }}
          ...
              {{ $post_month.Title }}
          ...
          {{ end }}
        {{ end }}

      {{ end }}

    {{ end }}

  {{ end }}
{{< / highlight >}}

Th effort is worthy.

#### Server Output: Browser

Now you can see the result in the browser.

![Hugo: Widget Archives][image-ss-01-widget-archives]

-- -- --

### What is Next ?

There are, some interesting topic about <code>Example Static Data with Hugo</code>.
Consider continue reading [ [Hugo - Data][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2019/04/15/hugo-data/
[local-hugo-custom]:    {{< baseurl >}}ssg/2018/09/14/hugo-sidebar/
[local-sass-custom]:    {{< baseurl >}}frontend/2019/03/06/bulma-sass-custom/
[local-sass-panels]:    {{< baseurl >}}frontend/2019/03/08/bulma-sass-panels/

[image-ss-04-source]:   {{< assets-ssg >}}/2019/04/tutor-hugo-04.tar

[image-ss-01-widget-archives]:     {{< assets-ssg >}}/2019/04/51-panel-archives.png
[image-ss-01-widget-categories]:   {{< assets-ssg >}}/2019/04/51-panel-categories.png
[image-ss-01-widget-recent-posts]: {{< assets-ssg >}}/2019/04/51-panel-recent-post.png
[image-ss-01-widget-tags]:         {{< assets-ssg >}}/2019/04/51-panel-tags.png
[image-ss-01-widget-affiliates]:   {{< assets-ssg >}}/2019/04/51-panel-affiliates.png

[tutor-hugo-config]:             {{< tutor-hugo-bulma >}}/config.toml
[tutor-hugo-layouts-post-single]: {{< tutor-hugo-bulma >}}/themes/tutor-04/layouts/post/single.html

[tutor-hugo-sass-list]:          {{< tutor-hugo-bulma >}}/themes/tutor-04/sass/css/_list.scss
[tutor-hugo-layouts-recent]:     {{< tutor-hugo-bulma >}}/themes/tutor-04/layouts/partials/widget/recent-posts.html
[tutor-hugo-layouts-archives]:   {{< tutor-hugo-bulma >}}/themes/tutor-04/layouts/partials/widget/archives.html
[tutor-hugo-layouts-categories]: {{< tutor-hugo-bulma >}}/themes/tutor-04/layouts/partials/widget/categories.html
[tutor-hugo-layouts-tags]:       {{< tutor-hugo-bulma >}}/themes/tutor-04/layouts/partials/widget/tags.html



