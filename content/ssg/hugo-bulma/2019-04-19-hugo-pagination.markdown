---
type   : post
title  : "Hugo - Pagination"
date   : 2019-04-19T09:17:35+07:00
slug   : hugo-bulma-pagination
categories: [ssg, frontend]
tags      : [hugo, navigation, bulma]
keywords  : [static site, custom theme, pagination, adjacent, responsive breakpoints, screen reader]
author : epsi
opengraph:
  image: assets/site/images/topics/hugo-bulma.png

toc    : "toc-2019-04-hugo-bulma-step"

excerpt:
  A step by step creation process for dummies,
  to achieve custom Hugo pagination,
  with Bulma as stylesheet frontend.

---

### Preface

> Goal: Step by step demo of custom pagination using Bulma

I have already made an article about Bootstrap pagination in Hugo.
I do not want to talk about Bulma pagination in very detail fashioned.
This would be a brief summary on how I built the pagination

![Hugo Pagination: Indicator Animation][image-ss-05-indicator-animation]

![Hugo Pagination: Responsive Animation][image-ss-05-responsive-animation]

#### Source Code

You can download the source code of this article here.

* [tutor-hugo-05.tar][image-ss-05-source]

#### Related Articles

I also wrote about Hugo Bootstrap Pagination.
Step by step article, that you can read here:

{{< ref-2018-11-hugo-pagination >}}

-- -- --

### 1: Prepare

Prepare this sitewide configuration.

#### Theme

Still with `tutor-05`.

* <code>config.toml</code>
  : [gitlab.com/.../config.toml][tutor-hugo-config]

{{< highlight toml >}}
theme        = "tutor-05"
{{< / highlight >}}

#### Pagination Limit

Pagination can be set in config.toml

{{< highlight toml >}}
Paginate     = 7
{{< / highlight >}}

#### SASS

We require this custom SASS

* <code>themes/tutor-05/sass/css/_pagination.sass</code>
  : [gitlab.com/.../sass/css/_pagination][tutor-hugo-sass-pagination].

{{< highlight scss >}}
+tablet
  li.blog-previous a:after
    content: " previous"
  li.blog-next a:before
    content: "next "
...
{{< / highlight >}}

-- -- --

### 2: Paginator

We will this `paginator` object throughout this long article.
This `paginator` would stay in `archives/list.html` layout.

#### Paginator Filtering

In layout we are going to use

{{< highlight twig >}}
{{ $paginator := .Paginate .Site.Pages }}
{{< / highlight >}}

Since we only show specific page kind named post, 
we should filter the pages using where.

{{< highlight twig >}}
{{ $paginator := .Paginate (where .Site.Pages "Type" "post") }}
{{< / highlight >}}

#### Layout: List

The complete file would be as follow:

* <code>themes/tutor-05/layouts/archives/list.html</code>
  : [gitlab.com/.../layouts/archives/list.html][tutor-hugo-layouts-list].

{{< highlight twig >}}
{{ define "main" }}
<main role="main" 
      class="column is-full box-deco has-background-white">
  <header>
    <h4 class="title is-4">{{ .Title | default .Site.Title }}</h4>
  </header>

  <section class="section">
    {{ $paginator := .Paginate (where .Site.Pages "Type" "post") }}
    {{ partial "pagination/01-simple.html" (dict "p" $paginator) }}
  </section>

  <div class="post-list">
  {{ range $paginator.Pages }}
    <section class="section" id="archive">
      {{ partial "summary/blog-list.html" . }}
    </section>
  {{ end }}
  </div>
{{ end }}
</main>
{{< / highlight >}}

I also put the code for each process in the same file artefact.
So you can easliy switch between one of the pagination.

{{< highlight twig >}}
{{ define "pagination-steps" }}
{{/*
  You may choose one of our pagination:

  {{ partial "pagination/01-simple.html" (dict "p" $paginator) }}
  {{ partial "pagination/02-number.html" (dict "p" $paginator) }}
  {{ partial "pagination/03-adjacent.html" (dict "p" $paginator "s" .Scratch) }}
  {{ partial "pagination/04-indicator.html" (dict "p" $paginator "s" .Scratch) }}
  {{ partial "pagination/05-responsive.html" (dict "p" $paginator "s" .Scratch) }}
  {{ partial "pagination/06-screenreader.html" (dict "p" $paginator "s" .Scratch) }}
*/}}
{{ end }}
{{< / highlight >}}

#### Dictionary

In order to pass variable from list layout to pagination partial,
we utilize dictionary: <code>(dict "p" $paginator)</code>.

{{< highlight twig >}}
  {{ $paginator := .Paginate (where .Site.Pages "Type" "post") }}
  {{ partial "pagination-simple.html" (dict "p" $paginator) }}
{{< / highlight >}}

#### Testing Pagination

While debugging, or development process,
we can do this to override sitewide <code>paginate</code> configuration.
Here we can use <code>5</code> articles for each pagination page.

{{< highlight twig >}}
{{ $paginator := .Paginate (where .Site.Pages "Type" "post") 5 }}
{{< / highlight >}}

This is a very helpful trick for me.

#### Content

To make it easy to count, I'm going to use exactly ten pages,
so we can have exactly ten pagination.
We can achieve this using

{{< highlight twig >}}
{{ $paginator := .Paginate (where .Site.Pages "Type" "post") 1 }}
{{< / highlight >}}

Make sure you have only ten content with `post` kind.
Comment the type just in case you need.

{{< highlight toml >}}
+++
# type   = "post"
title  = "Everyday I Ask"
... 
+++
{{< / highlight >}}

-- -- --

### 3: Simple

> Goal: Pagination for dummies, a simple introduction.

This is what we want to achieve in this tutorial.

![Hugo Pagination: Simple][image-ss-05-simple-480]

We will achieve this with Hugo code.

#### Layout: List

Change our previous blog list to use this paginator below:

* <code>themes/tutor-05/layouts/archives/list.html</code>.

{{< highlight twig >}}
  {{ $paginator := .Paginate (where .Site.Pages "Type" "post") }}
  {{ partial "pagination/01-simple.html" (dict "p" $paginator) }}
{{< / highlight >}}

#### Partial: Pagination Simple

Complete code is here below:

* <code>themes/tutor-05/layouts/partials/pagination/01-simple.html</code>
 : [gitlab.com/.../partials/pagination/01-simple.html][tutor-hugo-layouts-simple].

{{< highlight twig >}}
<nav class="pagination is-small is-centered" 
     role="navigation" aria-label="pagination">
  {{ if gt .p.TotalPages 1 }}
    <!-- First Page. -->
    {{ if not (eq .p.PageNumber 1) }}
      <a class="pagination-previous" href="{{ .p.First.URL }}" 
         rel="first">First</a>
    {{ else }}
      <a class="pagination-previous" title="This is the first page"
         disabled>First</a>
    {{ end }}

    <!-- Previous Page. -->
    {{ if .p.HasPrev }}
      <a class="pagination-previous" href="{{ .p.Prev.URL }}" 
         rel="prev">Previous</a>
    {{ else }}
      <a class="pagination-previous" title="This is the first page"
         disabled>Previous</a>
    {{ end }}

    <!-- Indicator Number. -->
    <a class="pagination-link" 
       disabled>Page: {{ .p.PageNumber }} of {{ .p.TotalPages }}</a>

    <!-- Next Page. -->
    {{ if .p.HasNext }}
      <a class="pagination-next" href="{{ .p.Next.URL }}" 
         rel="next">Next</a>
    {{ else }}
      <a class="pagination-next" title="This is the last page"
         disabled>Next</a>
    {{ end }}

    <!-- Last Page. -->
    {{ if not (eq .p.PageNumber .p.TotalPages) }}
      <a class="pagination-next" href="{{ .p.Last.URL }}" 
         rel="last">Last</a>
    {{ else }}
      <a class="pagination-next" title="This is the last page"
         disabled>Last</a>
    {{ end }}

    <!-- Dummy. Do not delete! -->
    <ul class="pagination-list">
    </ul>

  {{ end }}
</nav>
{{< / highlight >}}

#### Browser: Pagination Preview

Consider check out the result:

* 480px screen wide

![Hugo Pagination: Simple][image-ss-05-simple-480]

* 640px screen wide

![Hugo Pagination: Simple][image-ss-05-simple-640]

* 800px screen wide

![Hugo Pagination: Simple][image-ss-05-simple-800]

#### How does it works ?

You can see the detail in bootstrap article.

* [Hugo - Pagination - Simple][local-pagination-simple]

-- -- --

### 4: Number

> Goal: Pagination for dummies, list all of page.

This is what we want to achieve in this tutorial.

![Hugo Pagination: Number][image-ss-05-number-480]

We will achieve this with Hugo code.

#### Layout: List

Change our previous blog list to use this paginator below:

* <code>themes/tutor-05/layouts/archives/list.html</code>.

{{< highlight twig >}}
  {{ $paginator := .Paginate (where .Site.Pages "Type" "post") }}
  {{ partial "pagination/02-number.html" (dict "p" $paginator) }}
{{< / highlight >}}

#### Partial: Pagination Number

Complete code is here below:

* <code>themes/tutor-05/layouts/partials/pagination/02-number.html</code>
 : [gitlab.com/.../partials/pagination/02-number.html][tutor-hugo-layouts-number].

{{< highlight twig >}}
<nav class="pagination is-small is-centered" 
     role="navigation" aria-label="pagination">
  {{ if gt .p.TotalPages 1 }}
    <!-- First Page. -->
    {{ if not (eq .p.PageNumber 1) }}
      <a class="pagination-previous" href="{{ .p.First.URL }}" 
         rel="first">First</a>
    {{ else }}
      <a class="pagination-previous" title="This is the first page"
         disabled>First</a>
    {{ end }}

    <!-- Previous Page. -->
    {{ if .p.HasPrev }}
      <a class="pagination-previous" href="{{ .p.Prev.URL }}" 
         rel="prev">Previous</a>
    {{ else }}
      <a class="pagination-previous" title="This is the first page"
         disabled>Previous</a>
    {{ end }}

    <!-- Next Page. -->
    {{ if .p.HasNext }}
      <a class="pagination-next" href="{{ .p.Next.URL }}" 
         rel="next">Next</a>
    {{ else }}
      <a class="pagination-next" title="This is the last page"
         disabled>Next</a>
    {{ end }}

    <!-- Last Page. -->
    {{ if not (eq .p.PageNumber .p.TotalPages) }}
      <a class="pagination-next" href="{{ .p.Last.URL }}" 
         rel="last">Last</a>
    {{ else }}
      <a class="pagination-next" title="This is the last page"
         disabled>Last</a>
    {{ end }}

    <ul class="pagination-list">
      <!-- Page numbers. -->
      {{ $pagenumber := .p.PageNumber }}

      {{ range .p.Pagers }}
      <li>
        {{ if not (eq $pagenumber .PageNumber) }} 
        <a href="{{ .URL }}" class="pagination-link" 
           aria-label="Goto page {{ .PageNumber }}">
          {{ .PageNumber }}
        </a>
        {{ else }}
        <a class="pagination-link is-current" 
           aria-label="Page {{ .PageNumber }}">
          {{ .PageNumber }}
        </a>
        {{ end }}
      </li>
      {{ end }}
    </ul>
  {{ end }}
</nav>
{{< / highlight >}}

#### Browser: Pagination Preview

Consider check out the result:

* 480px screen wide

![Hugo Pagination: Number][image-ss-05-number-480]

* 640px screen wide

![Hugo Pagination: Number][image-ss-05-number-640]

* 800px screen wide

![Hugo Pagination: Number][image-ss-05-number-800]

#### How does it works ?

You can see the detail in bootstrap article.

* [Hugo - Pagination - Number][local-pagination-number]

-- -- --

### 5: Adjacent

This is actually, an advance pagination. Not so for dummies.

> Goal: Explaining Glenn McComb Pagination using Math and Table.

#### Source

I respect copyright.
The code below copied, and pasted from:

* <https://glennmccomb.com/articles/how-to-build-custom-hugo-pagination/>

I made a slight modification.
But of course the logic remain the same.

I mostly write it down in my blog,
because I do not want to forget what I learn.
An easier place for me to find the modified version of this good code.

#### Layout: List

Change our previous blog list to use this paginator below:

* <code>themes/tutor-05/layouts/archives/list.html</code>.

{{< highlight twig >}}
  {{ $paginator := .Paginate (where .Site.Pages "Type" "post") }}
  {{ partial "pagination/03-adjacent.html" (dict "p" $paginator "s" .Scratch) }}
{{< / highlight >}}

#### Partial: Pagination Adjacent

The code is complex, but interesting for coder.
Complete code is here below:

* <code>themes/tutor-05/layouts/partials/pagination/03-adjacent.html</code>
 : [gitlab.com/.../partials/pagination/03-adjacent.html][tutor-hugo-layouts-adjacent].

{{< highlight twig >}}
{{/* 

Pagination links 
* https://glennmccomb.com/articles/how-to-build-custom-hugo-pagination/

*/}}
<nav class="pagination is-small is-centered"
     role="navigation" aria-label="pagination">
  {{ $s := .s }}
  {{ $p := .p }}

  {{ if gt .p.TotalPages 1 }}
    <ul class="pagination-list">

    <!-- Page numbers. -->
    {{- $pagenumber := $p.PageNumber -}}

    <!-- Number of links either side of the current page. -->
    {{ $adjacent_links := 2 }}

    <!-- $max_links = ($adjacent_links * 2) + 1 -->
    {{ $max_links := (add (mul $adjacent_links 2) 1) }}

    <!-- $lower_limit = 1 + $adjacent_links -->
    {{ $lower_limit := (add 1 $adjacent_links) }}

    <!-- $upper_limit = $paginator.TotalPages - $adjacent_links -->
    {{ $upper_limit := (sub $p.TotalPages $adjacent_links) }}

    {{- range $p.Pagers -}}
      {{ $s.Set "page_number_flag" false }}

      <!-- Complex page numbers. -->
      {{ if gt $p.TotalPages $max_links }}

        <!-- Lower limit pages. -->
        <!-- If the user is on a page which is in the lower limit.  -->
        {{ if le $p.PageNumber $lower_limit }}

          <!-- If the current loop page is less than max_links. -->
          {{ if le .PageNumber $max_links }}
            {{ $s.Set "page_number_flag" true }}
          {{ end }}

        <!-- Upper limit pages. -->
        <!-- If the user is on a page which is in the upper limit. -->
        {{ else if ge $p.PageNumber $upper_limit }}

          <!-- If the current loop page is greater than total pages minus $max_links -->
          {{ if gt .PageNumber (sub .TotalPages $max_links) }}
            {{ $s.Set "page_number_flag" true }}
          {{ end }}

        <!-- Middle pages. -->
        {{ else }}
          
          {{ if and ( ge .PageNumber (sub $p.PageNumber $adjacent_links) ) ( le .PageNumber (add $p.PageNumber $adjacent_links) ) }}
            {{ $s.Set "page_number_flag" true }}
          {{ end }}

        {{ end }}

      <!-- Simple page numbers. -->
      {{ else }}

        {{ $s.Set "page_number_flag" true }}
      {{ end }}

      <!-- Show Pager. -->
      {{- if eq ($s.Get "page_number_flag") true -}}
      <li>
        {{ if not (eq $pagenumber .PageNumber) }} 
        <a href="{{ .URL }}" class="pagination-link" 
           aria-label="Goto page {{ .PageNumber }}">
          {{ .PageNumber }}
        </a>
        {{ else }}
        <a class="pagination-link is-current" 
           aria-label="Page {{ .PageNumber }}">
          {{ .PageNumber }}
        </a>
        {{ end }}
      </li>
      {{- end -}}
    {{ end }}

    </ul>
  {{ end }}
</nav>
{{< / highlight >}}

#### Browser: Pagination Preview

Consider check out the result:

![Hugo Pagination: Adjacent][image-ss-05-adjacent-480]

This would result the same in any screen width.

#### How does it works ?

You can see the detail in bootstrap article.

* [Hugo - Pagination - Adjacent][local-pagination-adjacent]

-- -- --

### 6: Indicator

> Goal: Add indicator, and putting all pagination part together.

This is what we want to achieve in this tutorial.

![Hugo Pagination: Indicator Animation][image-ss-05-indicator-animation]

We will achieve this with Hugo code.

#### Layout: List

Change our previous blog list to use this paginator below:

* <code>themes/tutor-05/layouts/archives/list.html</code>.

{{< highlight twig >}}
  {{ $paginator := .Paginate (where .Site.Pages "Type" "post") }}
  {{ partial "pagination/04-indicator.html" (dict "p" $paginator "s" .Scratch) }}
{{< / highlight >}}

#### Partial: Pagination Indicator

Now comes, the long code.
Complete code is here below:

* <code>themes/tutor-05/layouts/partials/pagination/04-indicator.html</code>
 : [gitlab.com/.../partials/paginatio/04-indicator.html][tutor-hugo-layouts-indicator].

{{< highlight twig >}}
<nav class="pagination is-small is-centered"
     role="navigation" aria-label="pagination">
  {{ $s := .s }}
  {{ $p := .p }}

  {{ if gt .p.TotalPages 1 }}
    <!-- Previous Page. -->
    {{ if .p.HasPrev }}
      <a class="pagination-previous" href="{{ .p.Prev.URL }}" 
         rel="prev">Previous</a>
    {{ else }}
      <a class="pagination-previous" title="This is the first page"
         disabled>Previous</a>
    {{ end }}

    <!-- Next Page. -->
    {{ if .p.HasNext }}
      <a class="pagination-next" href="{{ .p.Next.URL }}" 
         rel="next">Next</a>
    {{ else }}
      <a class="pagination-next" title="This is the last page"
         disabled>Next</a>
    {{ end }}

    <ul class="pagination-list">

    <!-- Page numbers. -->
    {{- $pagenumber := $p.PageNumber -}}

    <!-- Number of links either side of the current page. -->
    {{ $adjacent_links := 2 }}

    <!-- $max_links = ($adjacent_links * 2) + 1 -->
    {{ $max_links := (add (mul $adjacent_links 2) 1) }}

    <!-- $lower_limit = 1 + $adjacent_links -->
    {{ $lower_limit := (add 1 $adjacent_links) }}

    <!-- $upper_limit = $paginator.TotalPages - $adjacent_links -->
    {{ $upper_limit := (sub $p.TotalPages $adjacent_links) }}

    {{ if gt $p.TotalPages $max_links }}
      <!-- First Page. -->
      {{ if gt (sub $p.PageNumber $adjacent_links) 1 }}
      <li>
        <a href="{{ $p.First.URL }}" class="pagination-link" 
           aria-label="Goto page 1">1</a>
      </li>
      {{ end }}

      <!-- Early (More Pages) Indicator. -->
      {{ if gt (sub $p.PageNumber $adjacent_links) 2 }}
      <li>
        <span class="pagination-ellipsis">&hellip;</span>
      </li>
      {{ end }}
    {{ end }}

    {{- range $p.Pagers -}}
      {{ $s.Set "page_number_flag" false }}

      <!-- Complex page numbers. -->
      {{ if gt $p.TotalPages $max_links }}

        <!-- Lower limit pages. -->
        <!-- If the user is on a page which is in the lower limit.  -->
        {{ if le $p.PageNumber $lower_limit }}

          <!-- If the current loop page is less than max_links. -->
          {{ if le .PageNumber $max_links }}
            {{ $s.Set "page_number_flag" true }}
          {{ end }}

        <!-- Upper limit pages. -->
        <!-- If the user is on a page which is in the upper limit. -->
        {{ else if ge $p.PageNumber $upper_limit }}

          <!-- If the current loop page is greater than total pages minus $max_links -->
          {{ if gt .PageNumber (sub .TotalPages $max_links) }}
            {{ $s.Set "page_number_flag" true }}
          {{ end }}

        <!-- Middle pages. -->
        {{ else }}
          
          {{ if and ( ge .PageNumber (sub $p.PageNumber $adjacent_links) ) ( le .PageNumber (add $p.PageNumber $adjacent_links) ) }}
            {{ $s.Set "page_number_flag" true }}
          {{ end }}

        {{ end }}

      <!-- Simple page numbers. -->
      {{ else }}

        {{ $s.Set "page_number_flag" true }}
      {{ end }}

      <!-- Show Pager. -->
      {{- if eq ($s.Get "page_number_flag") true -}}
      <li>
        {{ if not (eq $pagenumber .PageNumber) }} 
        <a href="{{ .URL }}" class="pagination-link" 
           aria-label="Goto page {{ .PageNumber }}">
          {{ .PageNumber }}
        </a>
        {{ else }}
        <a class="pagination-link is-current" 
           aria-label="Page {{ .PageNumber }}">
          {{ .PageNumber }}
        </a>
        {{ end }}
      </li>
      {{- end -}}
    {{ end }}

    {{ if gt $p.TotalPages $max_links }}
      <!-- Late (More Pages) Indicator. -->
      {{ if lt (add $p.PageNumber $adjacent_links) (sub $p.TotalPages 1) }}
      <li>
        <span class="pagination-ellipsis">&hellip;</span>
      </li>
      {{ end }}

      <!-- Last Page. -->
      {{ if lt (add $p.PageNumber $adjacent_links) $p.TotalPages }}
      <li>
        <a href="{{ $p.Last.URL }}" class="pagination-link" 
           aria-label="Goto page {{ $p.TotalPages }}">{{ $p.TotalPages }}</a>
      </li>
      {{ end }}
    {{ end }}
    </ul>
  {{ end }}
</nav>
{{< / highlight >}}

#### Browser: Pagination Preview

Consider check out the result:

* 480px screen wide

![Hugo Pagination: Indicator][image-ss-05-indicator-480]

* 640px screen wide

![Hugo Pagination: Indicator][image-ss-05-indicator-640]

* 800px screen wide

![Hugo Pagination: Indicator][image-ss-05-indicator-800]

#### How does it works ?

You can see the detail in bootstrap article.

* [Hugo - Pagination - Indicator][local-pagination-indicator]

-- -- --

### 7: Responsive

> Goal: Bringing responsive pagination, using mobile first.

While writing this CSS code,
I can't claim myself as a developer.
Because CSS is not a programming language.

But weird that, responsive design has their own logic.
So yeah, I have to code, a little.

This is what we want to achieve in this tutorial.

![Hugo Pagination: Responsive Animation][image-ss-05-responsive-animation]

We will achieve this with Hugo code.

#### Layout: List

Change our previous blog list to use this paginator below:

* <code>themes/tutor-05/layouts/archives/list.html</code>.

{{< highlight twig >}}
  {{ $paginator := .Paginate (where .Site.Pages "Type" "post") }}
  {{ partial "pagination/05-responsive.html" (dict "p" $paginator "s" .Scratch) }}
{{< / highlight >}}

#### SASS

We require this custom SASS

* <code>themes/tutor-05/sass/css/_pagination.sass</code>
  : [gitlab.com/.../sass/css/_pagination][tutor-hugo-sass-pagination].

{{< highlight scss >}}
+tablet
  li.blog-previous a:after
    content: " previous"
  li.blog-next a:before
    content: "next "

// Breakpoint

$xs1: 0
$xs2: 320px
$xs3: 400px
$xs4: 480px
$sm1: 576px
$sm2: 600px
$md:  768px
$lg:  992px
$xl:  1200px

// Responsiveness

ul.pagination-list
  li.first,
  li.last,
  li.pages-indicator
    display: none
  li.pagination--offset-1,
  li.pagination--offset-2,
  li.pagination--offset-3,
  li.pagination--offset-4,
  li.pagination--offset-5,
  li.pagination--offset-6,
  li.pagination--offset-7
    display: none
  +from($xs1)
  +from($xs2)
    li.pages-indicator
      display: inline-block
  +from($xs3)
    li.pagination--offset-1
      display: inline-block
  +from($xs4)
    li.pagination--offset-2
      display: inline-block
  +from($sm1)
    li.first,
    li.last,
    li.pagination--offset-3
      display: inline-block
  +from($sm2)
    li.pagination--offset-4
      display: inline-block
  +from($md)
    li.pagination--offset-5,
    li.pagination--offset-6
      display: inline-block
  +from($lg)
    li.pagination--offset-7
      display: inline-block
  +from($xl)
{{< / highlight >}}

#### Partial: Pagination Responsive

Complete code is here below:

* <code>themes/tutor-05/layouts/partials/pagination/05-responsive.html</code>
 : [gitlab.com/.../partials/pagination/05-responsive.html][tutor-hugo-layouts-responsive].

{{< highlight twig >}}
<nav class="pagination is-small is-centered" 
     role="navigation" aria-label="pagination">
  {{ $s := .s }}
  {{ $p := .p }}

  {{ if gt .p.TotalPages 1 }}
    <ul class="pagination-list">

    <!-- Page numbers. -->
    {{- $pagenumber := $p.PageNumber -}}

    <!-- Number of links either side of the current page. -->
    {{ $adjacent_links := 2 }}

    <!-- $max_links = ($adjacent_links * 2) + 1 -->
    {{ $max_links := (add (mul $adjacent_links 2) 1) }}

    <!-- $lower_limit = 1 + $adjacent_links -->
    {{ $lower_limit := (add 1 $adjacent_links) }}

    <!-- $upper_limit = $paginator.TotalPages - $adjacent_links -->
    {{ $upper_limit := (sub $p.TotalPages $adjacent_links) }}

      <!-- Previous Page. -->
      <li class="blog-previous">
      {{ if .p.HasPrev }}
        <a class="pagination-previous"
           href="{{ .p.Prev.URL }}" 
           rel="prev">&laquo;&nbsp;</a>
      {{ else }}
        <a class="pagination-previous"
           title="This is the first page"
           disabled>&laquo;&nbsp;</a>
      {{ end }}
      </li>

    {{ if gt $p.TotalPages $max_links }}
      <!-- First Page. -->
      {{ if gt (sub $p.PageNumber $adjacent_links) 1 }}
      <li class="first">
        <a href="{{ $p.First.URL }}" class="pagination-link" 
           aria-label="Goto page 1">1</a>
      </li>
      {{ end }}

      <!-- Early (More Pages) Indicator. -->
      {{ if gt (sub $p.PageNumber $adjacent_links) 2 }}
      <li class="pages-indicator first">
        <span class="pagination-ellipsis">&hellip;</span>
      </li>
      {{ end }}
    {{ end }}

    {{- range $p.Pagers -}}
      {{ $s.Set "page_number_flag" false }}

      <!-- Complex page numbers. -->
      {{ if gt $p.TotalPages $max_links }}

        <!-- Lower limit pages. -->
        <!-- If the user is on a page which is in the lower limit.  -->
        {{ if le $p.PageNumber $lower_limit }}

          <!-- If the current loop page is less than max_links. -->
          {{ if le .PageNumber $max_links }}
            {{ $s.Set "page_number_flag" true }}
          {{ end }}

        <!-- Upper limit pages. -->
        <!-- If the user is on a page which is in the upper limit. -->
        {{ else if ge $p.PageNumber $upper_limit }}

          <!-- If the current loop page is greater than total pages minus $max_links -->
          {{ if gt .PageNumber (sub .TotalPages $max_links) }}
            {{ $s.Set "page_number_flag" true }}
          {{ end }}

        <!-- Middle pages. -->
        {{ else }}
          
          {{ if and ( ge .PageNumber (sub $p.PageNumber $adjacent_links) ) ( le .PageNumber (add $p.PageNumber $adjacent_links) ) }}
            {{ $s.Set "page_number_flag" true }}
          {{ end }}

        {{ end }}

      <!-- Simple page numbers. -->
      {{ else }}

        {{ $s.Set "page_number_flag" true }}
      {{ end }}

      {{- if eq ($s.Get "page_number_flag") true -}}
      <!-- Calculate Offset Class. -->
        {{ $s.Set "page_offset" (sub .PageNumber $p.PageNumber) }}

        {{ $s.Set "page_offset_class" "" }}
        {{- if ge ($s.Get "page_offset") 0 -}}
          {{ $s.Set "page_offset_class" (print "pagination--offset-" ($s.Get "page_offset") ) }}
        {{- else -}}
          {{ $s.Set "page_offset_class" (print "pagination--offset" ($s.Get "page_offset") ) }}
        {{- end -}}

      <!-- Show Pager. -->
      <li class="{{ $s.Get "page_offset_class" }}">
        {{ if not (eq $pagenumber .PageNumber) }} 
        <a href="{{ .URL }}" class="pagination-link" 
           aria-label="Goto page {{ .PageNumber }}">
          {{ .PageNumber }}
        </a>
        {{ else }}
        <a class="pagination-link is-current" 
           aria-label="Page {{ .PageNumber }}">
          {{ .PageNumber }}
        </a>
        {{ end }}
      </li>
      {{- end -}}
    {{ end }}

    {{ if gt $p.TotalPages $max_links }}
      <!-- Late (More Pages) Indicator. -->
      {{ if lt (add $p.PageNumber $adjacent_links) (sub $p.TotalPages 1) }}
      <li class="pages-indicator last">
        <span class="pagination-ellipsis">&hellip;</span>
      </li>
      {{ end }}

      <!-- Last Page. -->
      {{ if lt (add $p.PageNumber $adjacent_links) $p.TotalPages }}
      <li class="last">
        <a href="{{ $p.Last.URL }}" class="pagination-link" 
           aria-label="Goto page {{ $p.TotalPages }}">{{ $p.TotalPages }}</a>
      </li>
      {{ end }}
    {{ end }}

      <!-- Next Page. -->
      <li class="blog-next">
      {{ if .p.HasNext }}
        <a class="pagination-next"
           href="{{ .p.Next.URL }}" 
           rel="next">&nbsp;&raquo;</a>
      {{ else }}
        <a class="pagination-next"
           title="This is the last page"
           disabled>&nbsp;&raquo;</a>
      {{ end }}
      </li>
    </ul>
  {{ end }}
</nav>
{{< / highlight >}}

#### Browser: Pagination Preview

Consider check out the result:

* 480px screen wide

![Hugo Pagination: Responsive][image-ss-05-responsive-480]

* 640px screen wide (cropped)

![Hugo Pagination: Responsive][image-ss-05-responsive-640c]

* 800px screen wide (cropped)

![Hugo Pagination: Responsive][image-ss-05-responsive-800c]

#### How does it works ?

You can see the detail in bootstrap article.

* [Hugo - Pagination - Responsive][local-pagination-responsive]

-- -- --

### 8: Screenreader

> Goal: Bringing screen reader accessability in pagination.

There will be no such change for the look.
It will remain the same as `responsive` pagination.

#### Screenreader Class

I just follow bulma guidance:

* <https://bulma.io/documentation/modifiers/helpers/>

To hidden content visually,
you simply need to add <code>is-sr-only</code>.

{{< highlight twig >}}
<span class="is-sr-only">Hidden Content</span>
{{< / highlight >}}

Alternatively you can use fontawesome class that is similar to bootstrap.

{{< highlight twig >}}
<span class="sr-only">Hidden Content</span>
{{< / highlight >}}

This content can be read by screenreader.

#### Layout: List

Change our previous blog list to use this paginator below:

* <code>themes/tutor-05/layouts/archives/list.html</code>.

{{< highlight twig >}}
  {{ $paginator := .Paginate (where .Site.Pages "Type" "post") }}
  {{ partial "pagination/06-screenreader.html" (dict "p" $paginator "s" .Scratch) }}
{{< / highlight >}}

#### Partial: Pagination Screenreader

Complete code is here below:

* <code>themes/tutor-05/layouts/partials/pagination/06-screenreader.html</code>
 : [gitlab.com/.../partials/pagination/06-screenreader.html][tutor-hugo-layouts-screenreader].

{{< highlight twig >}}
<nav class="pagination is-small is-centered" 
     role="navigation" aria-label="pagination">
  {{ $s := .s }}
  {{ $p := .p }}

  {{ if gt .p.TotalPages 1 }}
    <ul class="pagination-list">

    <!-- Page numbers. -->
    {{- $pagenumber := $p.PageNumber -}}

    <!-- Number of links either side of the current page. -->
    {{ $adjacent_links := 2 }}

    <!-- $max_links = ($adjacent_links * 2) + 1 -->
    {{ $max_links := (add (mul $adjacent_links 2) 1) }}

    <!-- $lower_limit = 1 + $adjacent_links -->
    {{ $lower_limit := (add 1 $adjacent_links) }}

    <!-- $upper_limit = $paginator.TotalPages - $adjacent_links -->
    {{ $upper_limit := (sub $p.TotalPages $adjacent_links) }}

      <!-- Previous Page. -->
      <li class="blog-previous">
      {{ if .p.HasPrev }}
        <a class="pagination-previous"
           href="{{ .p.Prev.URL }}" 
           rel="prev">&laquo;&nbsp;
           <span class="is-sr-only">Previous</span>
        </a>
      {{ else }}
        <a class="pagination-previous"
           title="This is the first page"
           disabled>&laquo;&nbsp;</a>
      {{ end }}
      </li>

    {{ if gt $p.TotalPages $max_links }}
      <!-- First Page. -->
      {{ if gt (sub $p.PageNumber $adjacent_links) 1 }}
      <li class="first">
        <a href="{{ $p.First.URL }}" class="pagination-link" 
           aria-label="Goto page 1">
           <span class="is-sr-only">Goto page </span>1</a>
      </li>
      {{ end }}

      <!-- Early (More Pages) Indicator. -->
      {{ if gt (sub $p.PageNumber $adjacent_links) 2 }}
      <li class="pages-indicator first">
        <span class="pagination-ellipsis">&hellip;</span>
      </li>
      {{ end }}
    {{ end }}

    {{- range $p.Pagers -}}
      {{ $s.Set "page_number_flag" false }}

      <!-- Complex page numbers. -->
      {{ if gt $p.TotalPages $max_links }}

        <!-- Lower limit pages. -->
        <!-- If the user is on a page which is in the lower limit.  -->
        {{ if le $p.PageNumber $lower_limit }}

          <!-- If the current loop page is less than max_links. -->
          {{ if le .PageNumber $max_links }}
            {{ $s.Set "page_number_flag" true }}
          {{ end }}

        <!-- Upper limit pages. -->
        <!-- If the user is on a page which is in the upper limit. -->
        {{ else if ge $p.PageNumber $upper_limit }}

          <!-- If the current loop page is greater than total pages minus $max_links -->
          {{ if gt .PageNumber (sub .TotalPages $max_links) }}
            {{ $s.Set "page_number_flag" true }}
          {{ end }}

        <!-- Middle pages. -->
        {{ else }}
          
          {{ if and ( ge .PageNumber (sub $p.PageNumber $adjacent_links) ) ( le .PageNumber (add $p.PageNumber $adjacent_links) ) }}
            {{ $s.Set "page_number_flag" true }}
          {{ end }}

        {{ end }}

      <!-- Simple page numbers. -->
      {{ else }}

        {{ $s.Set "page_number_flag" true }}
      {{ end }}

      {{- if eq ($s.Get "page_number_flag") true -}}
      <!-- Calculate Offset Class. -->
        {{ $s.Set "page_offset" (sub .PageNumber $p.PageNumber) }}

        {{ $s.Set "page_offset_class" "" }}
        {{- if ge ($s.Get "page_offset") 0 -}}
          {{ $s.Set "page_offset_class" (print "pagination--offset-" ($s.Get "page_offset") ) }}
        {{- else -}}
          {{ $s.Set "page_offset_class" (print "pagination--offset" ($s.Get "page_offset") ) }}
        {{- end -}}

      <!-- Show Pager. -->
      <li class="{{ $s.Get "page_offset_class" }}">
        {{ if not (eq $pagenumber .PageNumber) }} 
        <a href="{{ .URL }}" class="pagination-link" 
           aria-label="Goto page {{ .PageNumber }}">
          <span class="is-sr-only">Goto page </span>{{ .PageNumber }}
        </a>
        {{ else }}
        <a class="pagination-link is-current" 
           aria-label="Page {{ .PageNumber }}">
          <span class="is-sr-only">Page </span>{{ .PageNumber }}
        </a>
        {{ end }}
      </li>
      {{- end -}}
    {{ end }}

    {{ if gt $p.TotalPages $max_links }}
      <!-- Late (More Pages) Indicator. -->
      {{ if lt (add $p.PageNumber $adjacent_links) (sub $p.TotalPages 1) }}
      <li class="pages-indicator last">
        <span class="pagination-ellipsis">&hellip;</span>
      </li>
      {{ end }}

      <!-- Last Page. -->
      {{ if lt (add $p.PageNumber $adjacent_links) $p.TotalPages }}
      <li class="last">
        <a href="{{ $p.Last.URL }}" class="pagination-link" 
           aria-label="Goto page {{ $p.TotalPages }}">
           <span class="is-sr-only">Goto page </span>{{ $p.TotalPages }}</a>
      </li>
      {{ end }}
    {{ end }}

      <!-- Next Page. -->
      <li class="blog-next">
      {{ if .p.HasNext }}
        <a class="pagination-next"
           href="{{ .p.Next.URL }}" 
           rel="next">&nbsp;&raquo;
           <span class="is-sr-only">Next</span>
        </a>
      {{ else }}
        <a class="pagination-next"
           title="This is the last page"
           disabled>&nbsp;&raquo;</a>
      {{ end }}
      </li>
    </ul>
  {{ end }}
</nav>
{{< / highlight >}}

#### Browser: Pagination Preview

Consider check out the result:

* 480px screen wide

![Hugo Pagination: Screenreader][image-ss-05-screenreader-480]

#### How does it works ?

You can see the detail in bootstrap article.

* [Hugo - Pagination - Screen Reader][local-pagination-screenreader]

-- -- --

### What is Next ?

Now the pagination tutorial is done.
I think this is all for now.

Just like this adorable kitten, you may feel excited.
Consider resume our tutorial.

![adorable kitten][image-kitten]

There are still some interesting topic,
about <code>Content in Hugo</code> such as Page Post.
Consider continue reading [ [Hugo - Content][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2019/04/21/hugo-content/
[local-pagination-simple]:  {{< baseurl >}}ssg/2018/11/17/hugo-pagination-simple/
[local-pagination-number]:  {{< baseurl >}}ssg/2018/11/18/hugo-pagination-number/
[local-pagination-adjacent]:     {{< baseurl >}}ssg/2018/11/18/hugo-pagination-adjacent/
[local-pagination-indicator]:    {{< baseurl >}}ssg/2018/11/20/hugo-pagination-indicator/
[local-pagination-responsive]:   {{< baseurl >}}ssg/2018/11/21/hugo-pagination-responsive/
[local-pagination-screenreader]: {{< baseurl >}}ssg/2018/11/22/hugo-pagination-screenreader/

[image-ss-05-source]:   {{< assets-ssg >}}/2019/04/tutor-hugo-05.tar
[image-kitten]:             {{< baseurl >}}/assets/site/images/cats/rambu-03.jpg

[image-ss-05-indicator-animation]:  {{< assets-ssg >}}/2019/04/52-04-indicator-bulma-animate.gif
[image-ss-05-responsive-animation]: {{< assets-ssg >}}/2019/04/52-04-responsive-bulma-animate.gif

[tutor-hugo-sass-pagination]:       {{< tutor-hugo-bulma >}}/themes/tutor-05/sass/css/_pagination.scss

[image-ss-05-simple-480]:   {{< assets-ssg >}}/2019/04/52-01-simple-480px.png
[image-ss-05-simple-640]:   {{< assets-ssg >}}/2019/04/52-01-simple-640px.png
[image-ss-05-simple-800]:   {{< assets-ssg >}}/2019/04/52-01-simple-800px.png
[image-ss-05-number-480]:   {{< assets-ssg >}}/2019/04/52-02-number-480px.png
[image-ss-05-number-640]:   {{< assets-ssg >}}/2019/04/52-02-number-640px.png
[image-ss-05-number-800]:   {{< assets-ssg >}}/2019/04/52-02-number-800px.png
[image-ss-05-adjacent-480]: {{< assets-ssg >}}/2019/04/52-03-adjacent-480px.png
[image-ss-05-indicator-480]:    {{< assets-ssg >}}/2019/04/52-04-indicator-480px.png
[image-ss-05-indicator-640]:    {{< assets-ssg >}}/2019/04/52-04-indicator-640px.png
[image-ss-05-indicator-800]:    {{< assets-ssg >}}/2019/04/52-04-indicator-800px.png
[image-ss-05-responsive-480]:   {{< assets-ssg >}}/2019/04/52-05-responsive-480px.png
[image-ss-05-responsive-640]:   {{< assets-ssg >}}/2019/04/52-05-responsive-640px.png
[image-ss-05-responsive-640c]:  {{< assets-ssg >}}/2019/04/52-05-responsive-640px-cropped.png
[image-ss-05-responsive-800]:   {{< assets-ssg >}}/2019/04/52-05-responsive-800px.png
[image-ss-05-responsive-800c]:  {{< assets-ssg >}}/2019/04/52-05-responsive-800px-cropped.png
[image-ss-05-screenreader-480]: {{< assets-ssg >}}/2019/04/52-06-screenreader-480px.png

[tutor-hugo-config]:             {{< tutor-hugo-bulma >}}/config.toml
[tutor-hugo-layouts-list]:       {{< tutor-hugo-bulma >}}/themes/tutor-05/layouts/archives/list.html
[tutor-hugo-layouts-simple]:     {{< tutor-hugo-bulma >}}/themes/tutor-05/layouts/partials/pagination/01-simple.html
[tutor-hugo-layouts-number]:     {{< tutor-hugo-bulma >}}/themes/tutor-05/layouts/partials/pagination/02-number.html
[tutor-hugo-layouts-adjacent]:      {{< tutor-hugo-bulma >}}/themes/tutor-05/layouts/partials/pagination/03-adjacent.html
[tutor-hugo-layouts-indicator]:     {{< tutor-hugo-bulma >}}/themes/tutor-05/layouts/partials/pagination/04-indicator.html
[tutor-hugo-layouts-responsive]:    {{< tutor-hugo-bulma >}}/themes/tutor-05/layouts/partials/pagination/05-responsive.html
[tutor-hugo-layouts-screenreader]:  {{< tutor-hugo-bulma >}}/themes/tutor-05/layouts/partials/pagination/06-screenreader.html
