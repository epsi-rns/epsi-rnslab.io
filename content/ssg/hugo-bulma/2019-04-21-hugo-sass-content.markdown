---
type   : post
title  : "Hugo - Blog Post"
date   : 2019-04-21T09:17:35+07:00
slug   : hugo-sass-content
categories: [ssg, frontend]
tags      : [hugo, bulma, sass]
keywords  : [static site, custom theme, post header, post footer, post navigation, date time, javascript, timeago]
author : epsi
opengraph:
  image: assets/site/images/topics/hugo-bulma-markdown.png

toc    : "toc-2019-04-hugo-bulma-step"

excerpt:
  Building Hugo Step by step, with Bulma as stylesheet frontend.
  More about blog post content, Header, Footer, and Pagination.

---

### Preface

> Goal: More about blog post content, Header, Footer, and Pagination.

We can save a lot of stylesheet formatting,
by using Bulma, or any other CSS framework.

#### Source Code

You can download the source code of this article here.

* [tutor-hugo-05.tar][image-ss-05-source]

-- -- --

### 1: Prepare

This preparation is required.

#### Theme

We are still in `tutor-05`.

{{< highlight toml >}}
$ cat config.toml
theme        = "tutor-05"
{{< / highlight >}}

#### Preview: General

Consider redesign the looks of blog post.
This is what we want to achieve in this tutorial.

![Hugo Content: All Artefacts][image-ss-01-blog-post-all]

#### Layouts: Single

Edit our ancient artefact <code>single.html</code>,
that is exist from the very beginning of our tutorial.

* <code>themes/tutor-05/layouts/post/single.html</code>
  : [gitlab.com/.../layouts/post/single.html][tutor-hugo-layouts-post-single]

{{< highlight twig >}}
{{ define "main" }}
  <main role="main" 
        class="column is-two-thirds blog-column box-deco has-background-white">
    <div class="blog-post" itemscope itemtype="http://schema.org/BlogPosting">
      {{ partial "post/header.html" . }}

      <article class="main-content" itemprop="articleBody">
        {{ .Content }}
      </article>

      <br/>
      {{ partial "post/navigation.html" . }}
    </div><!-- /.blog-post -->
    
    {{ partial "post/footer.html" . }}
  </main>
{{ end }}

{{ define "aside" }}
  <aside class="sidebar column is-one-thirds is-paddingless">
    {{ partial "side/recent-post.html" . }}
    {{ partial "side/categories.html" . }}
  </aside>
{{ end }}
{{< / highlight >}}

Talking about schema, you can read this nice site:

* [schema.org/](http://schema.org/)

#### Partials: New Artefacts

The code above require three new partials.

Create three empty artefacts,

* <code>themes/tutor-05/layouts/partials/post/header.html</code>

* <code>themes/tutor-05/layouts/partials/post/footer.html</code>

* <code>themes/tutor-05/layouts/partials/post/navigation.html</code>

And one more artefact that is required in <code>post/header.html</code>.

* <code>themes/tutor-05/layouts/partials/post/time-elapsed.html</code>

-- -- --

### 2: Header

Consider begin with header.

#### Preview

![Hugo Content: Header][image-ss-01-blog-post-header]

#### Partials: Minimum

The minimum header is simply showing title.

* <code>themes/tutor-05/layouts/partials/post/header</code>
  : [gitlab.com/.../layouts/partials/post/header.html][tutor-hugo-partials-header]

{{< highlight html >}}
    <section class="box">
      <div class="main_title"> 
        <h4 class="title is-4 blog-post-title" itemprop="name headline">
          <a href="{{ .Permalink }}">{{ .Page.Title }}</a></h4>
      </div>
    </section>
{{< / highlight >}}

#### Partials: Meta

Consider add **meta** data in post header.

* author

* time elapsed (using partial)

* tags

* categories

Here I use bulma <code>button</code>,
with the eye candy Awesome Font.

{{< highlight html >}}
    <section class="box">
      <div class="main_title"> 
        <h4 class="title is-4 blog-post-title" itemprop="name headline">
          <a href="{{ .Permalink }}">{{ .Page.Title }}</a></h4>
      </div>

      <div class="is-clearfix"></div>
      
      <div class="field">
        {{ if .Params.author }}
        <div class="is-pulled-left">
        <span class="meta_author tag is-primary is-small">
            <span class="fa fa-user"></span>
            &nbsp;
            <span itemprop="author" itemscope itemtype="http://schema.org/Person">
            <span itemprop="name">{{ .Params.author }}</span></span>            
        </span>
        &nbsp;
        </div>
        {{ end }}

        <div class="is-pulled-right">
          {{ range .Params.tags }}
            <a href="{{ printf "tags/%s" . | absURL }}">
              <span class="tag is-light is-small"><span class="fa fa-tag"></span>&nbsp;{{ . }}</span></a>
          {{ end }}
          &nbsp;
        </div>

        <div class="is-pulled-right">
          {{ range .Params.categories }}
            <a href="{{ printf "categories/%s" . | absURL }}">
              <span class="tag is-dark is-small"><span class="fa fa-folder"></span>&nbsp;{{ . }}</span></a>
          {{ end }}
          &nbsp;
        </div>
      </div>

      <div class="is-clearfix"></div>
    </section>
{{< / highlight >}}

-- -- --

### 3: Elapsed Time

As it has been mentioned above, we need special partial for this.

#### Issue

As a static generator, Hugo compiled the content whenever there are any changes.
If there are no changes, the generated time remain static.
It means we cannot tell relative time in string such as **three days ago**,
because after a week without changes, the time remain **three days ago**,
not changing into **ten days ago**.

The solution is using javascript.
You can download the script from here

* [timeago.org/](https://timeago.org/)

That way the time ago is updated dynamically as time passes,
as opposed to having to rebuild Hugo every day.

Do not forget to put the script in static directory.

* <code>themes/tutor-05/static/js/timeago.min.js</code>

#### Partial

* <code>themes/tutor-05/layouts/partials/post/time-elapsed</code>
  : [gitlab.com/.../layouts/partials/post/time-elapsed.html][tutor-hugo-partials-time-elapsed]

{{< highlight javascript >}}
    <time datetime="{{ .Page.Date.Format "2006-01-02T15:04:05Z07:00" }}" 
          itemprop="datePublished">
    <span class="timeago" datetime="{{ .Page.Date.Format "2006-01-02 15:04:05" }}">
    </span></time>
    &nbsp; 

    <script src="{{ "js/timeago.min.js" | relURL }}"></script>
    <script type="text/javascript">
      var timeagoInstance = timeago();
      var nodes = document.querySelectorAll('.timeago');
      timeagoInstance.render(nodes, 'en_US');
      timeago.cancel();
      timeago.cancel(nodes[0]);
    </script>
{{< / highlight >}}

-- -- --

### 4: Footer

We already have Header.
Why do not we continue with footer?

#### Preview

![Hugo Content: Footer][image-ss-01-blog-post-footer]

#### Partials

* <code>themes/tutor-05/layouts/partials/post/footer</code>
  : [gitlab.com/.../layouts/partials/post/footer.html][tutor-hugo-partials-footer]

{{< highlight html >}}
    <footer class="section">
      <div class="columns">

        <div class="column is-narrow has-text-centered">
            <img src="/site/images/license/cc-by-sa.png" 
                 class="bio-photo" 
                 height="31"
                 width="88"
                 alt="CC BY-SA 4.0"></a>
        </div>

        <div class="column has-text-left">
            This article is licensed under:&nbsp;
            <a href="https://creativecommons.org/licenses/by-sa/4.0/deed" 
               class="text-dark">
                <b>Attribution-ShareAlike</b> 4.0 International (CC BY-SA 4.0)</a>
        </div>

      </div>
    </footer>
{{< / highlight >}}

#### Assets: License

I have collect some image related with **license*,
so that you can use license easily.

* <code>static/assets/site/images/license/</code>
  : [gitlab.com/.../assets/site/images/license/][tutor-hugo-license]

-- -- --

### 5: Post Navigation

Each post also need simple navigation.
Be aware of this, Hugo as some deprecation code.
This could lead to wrong page,
if you use the deprecated version on newer Hugo.

#### Preview

![Hugo Content: Navigation][image-ss-01-blog-post-nav]

#### Partials: Hugo 0.5x

* <code>themes/tutor-05/layouts/partials/post/navigation</code>
  : [gitlab.com/.../layouts/partials/post/navigation.html][tutor-hugo-partials-navigation]

{{< highlight twig >}}
<nav class="pagination is-centered" 
     role="navigation" aria-label="pagination">

    <!-- Previous Page. -->
    {{ if .Page.Prev }}
      <a class="pagination-previous post-previous"
         href="{{ .Page.Prev.URL | absURL }}"
         title="{{ .Page.Prev.Title }}">&laquo;&nbsp;</a>
    {{ end }}

    {{ if .Page.Next }}
      <a class="pagination-next post-next"
         href="{{ .Page.Next.URL | absURL }}" 
         title="{{ .Page.Next.Title }}">&nbsp;&raquo;</a>
    {{ end }}
</nav>
{{< / highlight >}}


#### Partials: Hugo 0.4x

If you are using old Hugo, you can use this instead:

{{< highlight twig >}}
<nav class="pagination is-centered" 
     role="navigation" aria-label="pagination">

    <!-- Previous Page. -->
    {{ if .Page.Next }}
      <a class="pagination-previous post-previous"
         href="{{ .Page.Next.URL | absURL }}"
         title="{{ .Page.Next.Title }}">&laquo;&nbsp;</a>
    {{ end }}

    {{ if .Page.Prev }}
      <a class="pagination-next post-next"
         href="{{ .Page.Prev.URL | absURL }}" 
         title="{{ .Page.Prev.Title }}">&nbsp;&raquo;</a>
    {{ end }}
</nav>
{{< / highlight >}}

#### SASS: Post Navigation

Code above require two more classes

* <code>previous</code>

* <code>next</code>

* <code>themes/tutor-05/sass/css/_post-navigation.scss</code>
  : [gitlab.com/.../sass/_post-navigation.scss][tutor-hugo-sass-navigation].

{{< highlight scss >}}
// -- -- -- -- --
// _post-navigation.sass

a.post-previous:after
    content: " previous"

a.post-next:before
    content: "next "
{{< / highlight >}}

-- -- --

### What is Next ?

Looks good right?
This kitten, is still with me.
Just like the kitten, do not get rest yet!
Our pagination tutorial still have some materials to go.

![adorable kitten][image-kitten]

There are still, a list of some interesting topic for using <code>Hugo</code>.
Consider continue reading [ [Hugo - Conclusion][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2019/04/25/hugo-conclusion/

[image-ss-05-source]:   {{< assets-ssg >}}/2019/04/tutor-hugo-05.tar
[image-kitten]:             {{< baseurl >}}/assets/site/images/cats/rambu-01.jpg

[image-ss-01-blog-post-all]:     {{< assets-ssg >}}/2019/04/61-blog-post-all.png
[image-ss-01-blog-post-header]:  {{< assets-ssg >}}/2019/04/61-blog-post-header.png
[image-ss-01-blog-post-footer]:  {{< assets-ssg >}}/2019/04/61-blog-post-footer.png
[image-ss-01-blog-post-nav]:     {{< assets-ssg >}}/2019/04/61-blog-post-navigation.png

[tutor-hugo-layouts-post-single]:   {{< tutor-hugo-bulma >}}/themes/tutor-05/layouts/post/single.html
[tutor-hugo-partials-header]:       {{< tutor-hugo-bulma >}}/themes/tutor-05/layouts/partials/post/header.html
[tutor-hugo-partials-footer]:       {{< tutor-hugo-bulma >}}/themes/tutor-05/layouts/partials/post/footer.html
[tutor-hugo-partials-navigation]:   {{< tutor-hugo-bulma >}}/themes/tutor-05/layouts/partials/post/navigation.html
[tutor-hugo-partials-time-elapsed]: {{< tutor-hugo-bulma >}}/themes/tutor-05/layouts/partials/post/time-elapsed.html

[tutor-hugo-license]:            {{< tutor-hugo-bulma >}}/static/assets/site/images/license/
[tutor-hugo-sass-main]:          {{< tutor-hugo-bulma >}}/themes/tutor-05/sass/css/main.scss
[tutor-hugo-sass-navigation]:    {{< tutor-hugo-bulma >}}/themes/tutor-05/sass/css/_post-navigation.scss
