---
type   : post
title  : "Hugo - Conclusion"
date   : 2019-04-25T09:17:35+07:00
slug   : hugo-conclusion
categories: [ssg]
tags      : [hugo]
keywords  : [static site, custom theme, conclusion]
author : epsi
opengraph:
  image: assets/site/images/topics/hugo-bulma-markdown.png

toc    : "toc-2019-04-hugo-bulma-step"

excerpt:
  Building Hugo Step by step, with Bulma as stylesheet frontend.
  Conclusion of all the articles.
  The journey end here.

---

### Preface

> Goal: Complete the article series.

There are at least six more articles to go.
And I decide not to rewrite,
because the rest six article have little to do,
with Bootstrap nor Bulma.

#### Source Code

There are still things to explore.
I have already bake them into `tutor-05` source code.
You can download the source code of this article here.

* [tutor-hugo-05.tar][image-ss-05-source]

It is mostly the same with th Bootstrap counterpart,
with a few enhancement.

Have fun with exploring.

#### Related Articles

If you want to read, you can continue to these old articles.

{{< ref-2018-12-hugo-rest >}}

-- -- --

### Table of Content

#### Content - Markdown

* 1: Split Resource

* 2: Shortcode: Link

* 3: Shortcode: Image

* 4: Responsive: Image

#### Content - Raw HTML

* 1: Before Content: Table of Content

* 2: Inside Content: Advertisement

#### Syntax Highlighting

* 1: Built-in: Pygment

* 2: Triple Backtick

* 3: PrismJS

* 4: Highlight Stylesheet

#### Meta - Opengraph

* 1: Prepare Artefacts

* 2: Head Tag

* 3: Opengraph

* 4: Twitter

#### Service

* Preface: Table of Content

* 1: Google Analytics

* 2: Disqus Comment

#### Javacript - lunr Search

* 1: Prepare Artefacts

* 2: Search Page

* 3: Navigation Bar

* 4: Javascript Layout

* 5: JQuery Requirement

* 6: Search Data Source

* 7: lunr Search

* 8: Showing The Result

* 9: Summary

-- -- --

### What is Next ?

We are finished with this Hugo article series.
Consider going back to [ [Hugo - Overview][local-whats-next] ].

With confidence. Thank you for reading.
The journey end here. It is always hard to say goodbye.
It is up to you now to pair `Hugo` and `Bulma`,
or pair each with other framework.

Feels like tired, after finishing these article series?
This sleepy cat, need some sleep now..
Just like the sleepy cat, you may need some rest too.
Our article series is finished.
But you may continue to explore other challenging topic,
tomorrow, or right away.

![adorable kitten][image-kitten]

At the end of the day,
there is always people in need of help.
There is always another material to explore.
There is always another article series to be done.

Farewell.
We shall meet again.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:         {{< baseurl >}}ssg/2019/04/01/hugo-overview/

[image-ss-05-source]:   {{< assets-ssg >}}/2019/04/tutor-hugo-05.tar
[image-kitten]:             {{< baseurl >}}/assets/site/images/cats/fatboy.jpg
