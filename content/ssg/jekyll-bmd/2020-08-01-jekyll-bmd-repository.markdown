---
type   : post
title  : "Jekyll Bulma MD - Repository"
date   : 2020-08-01T09:17:35+07:00
slug   : jekyll-bmd-repository
categories: [ssg]
tags      : [jekyll, bulma]
keywords  : [static site, custom theme, summary, overview]
author : epsi
opengraph:
  image: assets-ssg/2020/08/jekyll-bulma-md-preview.png

toc    : "toc-2020-06-jekyll-step"

excerpt:
  Building Jekyll Site Step by step,
  using Bulma MD as stylesheet.
---

### Preface

> Goal: Overview of Jekyll Repository Step By Step.

An example of pure Jekyll site with Bulma MD as stylesheet
for personal learning purpose.

> Build: using Bulma MD as stylesheet.

This repository is based on Jekyll Plain without any stylesheet.
We put on Bulma stylesheet as clothes using different repository,
and face some adjustment in almost all `_includes` views.

#### Jekyll Version

Since this repository is still using Jekyll 3.8, instead of Jekyll 4.0,
you might need to run `bundle install`, depend on the situation.

{{< highlight bash >}}
$ bundle install
{{< / highlight >}}

## Chapter Step by Step

### Tutor 08

> Add Bulma CSS Framework

* Add Bulma CSS

* Standard Header (jquery or vue or native) and Footer

* Enhance All Layouts with Bulma CSS

* Apply Bulma Two Column Responsive Layout for Most Layout

![Jekyll Bulma: Tutor 08][jekyll-bulma-preview-08]

-- -- --

### Tutor 09

> Combine with Custom SASS Material Design

* Add (a bunch of) Custom SASS (Custom Design)

* Nice Header and Footer

* Enhance All Two Column Responsive Layout with Material Design

* Nice Tag Badge in Tags Page

![Jekyll Bulma: Tutor 09][jekyll-bulma-preview-09]

-- -- --

### Tutor 10

> Loop with Liquid

* More Content: Quotes. Need this content for demo

* Simplified All Layout Using Liquid Template Inheritance

* Article Index: By Year, List Tree (By Year and Month)

![Jekyll Bulma: Tutor 10][jekyll-bulma-preview-10]

-- -- --

### Tutor 11

> Features

* Widget: Friends, Archives Tree, Categories, Tags, Recent Post, Related Post

* Refactoring Template using Capture: Widget

* Pagination (v1): Adjacent, Indicator, Responsive

* Pagination (v2): Adjacent, Indicator, Responsive

* Post: Header, Footer, Navigation

![Jekyll Bulma: Tutor 11][jekyll-bulma-preview-11]

-- -- --

### Tutor 12

> Finishing

* Layout: Service (dummy)

* Post: Markdown Content (test case)

* Post: Table of Content (dynamic include)

* Post: Responsive Images

* Post: Syntax Highlight (sass)

* Post: Shortcodes (include with parameter)

* Official Plugin: Feed

* Meta: HTML, SEO, Opengraph, Twitter

* Multi Column Responsive List: Categories, Tags, and Archives

![Jekyll Bulma: Tutor 12][jekyll-bulma-preview-12]

-- -- --

### Tutor 13

> Jekyll Plugin with Ruby

* Filter: Year Text, Term Array, Pagination Links, Link Offset Flag, Keywords

* Generator: Tag Names

![Jekyll Bulma: Tutor 13][jekyll-bulma-preview-13]

-- -- --

### Tutor 14

> Jekyll Theme with Assets and SASS

* Bundling Gem

* Using Theme

![Jekyll Bulma: Tutor 14][jekyll-bulma-preview-14]

-- -- --

### What is Next?

Consider continue reading [ [Jekyll Bulma - CSS Intro][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/08/02/jekyll-bmd-css-intro/

[jekyll-bulma-preview]:     https://gitlab.com/epsi-rns/tutor-jekyll-bulma-md/raw/master/jekyll-bulma-md-preview.png
[jekyll-bulma-preview-08]:  https://gitlab.com/epsi-rns/tutor-jekyll-bulma-md/raw/master/tutor-08/jekyll-bulma-md-preview.png
[jekyll-bulma-preview-09]:  https://gitlab.com/epsi-rns/tutor-jekyll-bulma-md/raw/master/tutor-09/jekyll-bulma-md-preview.png
[jekyll-bulma-preview-10]:  https://gitlab.com/epsi-rns/tutor-jekyll-bulma-md/raw/master/tutor-10/jekyll-bulma-md-preview.png
[jekyll-bulma-preview-11]:  https://gitlab.com/epsi-rns/tutor-jekyll-bulma-md/raw/master/tutor-11/jekyll-bulma-md-preview.png
[jekyll-bulma-preview-12]:  https://gitlab.com/epsi-rns/tutor-jekyll-bulma-md/raw/master/tutor-12/jekyll-bulma-md-preview.png
[jekyll-bulma-preview-13]:  https://gitlab.com/epsi-rns/tutor-jekyll-bulma-md/raw/master/tutor-13/jekyll-bulma-md-preview.png
[jekyll-bulma-preview-14]:  https://gitlab.com/epsi-rns/tutor-jekyll-bulma-md/raw/master/tutor-14/jekyll-bulma-md-preview.png
