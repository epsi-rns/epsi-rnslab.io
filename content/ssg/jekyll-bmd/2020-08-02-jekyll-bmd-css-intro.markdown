---
type   : post
title  : "Jekyll Bulma - CSS Intro"
date   : 2020-08-02T09:17:35+07:00
slug   : jekyll-bmd-css-intro
categories: [ssg]
tags      : [jekyll, bulma]
keywords  : [assets, liquid, layout, refactoring template]
author : epsi
opengraph:
  image: assets-ssg/2020/08/bulma/08-vim-default.png

toc    : "toc-2020-06-jekyll-step"

excerpt:
  Building Jekyll Site Step by step, using Bulma MD as stylesheet.
  Revamp the looks, of current Jekyll layout.
---

### Preface

> Goal: Revamp the looks,
> of current Jekyll layout, with Bulma Stylesheet.

#### Previous Tutorial

This article is direct chain of previous tutorial.

* [Jekyll Plain - Theme Bundling][local-whats-theme]

#### Source Code

This article use [tutor-08][tutor-html-master-08] theme.

#### Not a CSS Tutorial

This article is not a CSS tutorial,
but rather than applying simple CSS theme,
into `Jekyll` layout.
This CSS theme is a based on a SASS theme,
that we are going to discuss later.

#### Layout Preview for Tutor 08

![Liquid: Layout Preview for Tutor 08][template-inheritance-08]

[template-inheritance-08]:  {{< assets-ssg >}}/2020/06/08-template-inheritance.png

-- -- --

### 1: About Bulma Stylesheet

> Use Stylesheet in Jekyll

In previous articles,
we have already refactor pages into a few layouts.
All done without stylesheet,
so you have understanding of Jekyll layout, 
without the burden of learning stylesheet.

#### The Choice

Now it is a good time to give this site a good looks.
There are many options for this such as:
`Bulma`, `Materialize CSS`, `Semantic UI`, `Bulma`,
or even better custom tailor made without CSS Frameworks,
or `Tailwind CSS`.

#### Prerequiste

> Use Bulma Stylesheet in Jekyll

My choice comes to Bulma and Bootstrap,
because I have already make a few articles about Bulma and Bootstrap,
with a lot of details explanation:

* [Bulma - Overview][local-bulma]

This article rely on Bulma stylesheet on the article above.
You should give a fast reading of above Bulma article first,
before continue reading this article.

#### Responsive Design

Our base layout is based on responsive design.
This below is general preview of,
what responsive page that we want to achieve.

![Jekyll: General Preview of Responsive Page Design][image-png-layout-page]

Different website might have different preview.
Source image is available in inkscape SVG format,
so you can modify, and make your own preview quickly.

* [Responsive Page: Image Source][image-svg-layout-page]

This responsive design has already discussed in Bulma article series.

#### Related Article: CSS

I wrote about Bulma Navigation Bar that used in this article.
Step by step article, about building Navigation Bar.
You can read here:

* [Bulma - Navigation Bar][local-bulma-navbar]

* [Bulma SASS - Responsive Layout][local-bulma-layout]

#### Reference

* [bulma.io/documentation](https://bulma.io/documentation/)

-- -- --

### 2: Prepare: Assets

#### Directory Tree: Assets

There is no need to change any configuration.
Any directory without `_` prefix would be rendered.
To make your assets tidy, consider put them all in one directory,
such as `./assets`.

{{< highlight bash >}}
$ tree assets
assets
├── css
│   ├── bulma.css
│   └── main.css
├── images
│   ├── light-grey-terrazzo.png
│   └── logo-gear.png
└── js
    └── bulma-burger-plain.js

3 directories, 5 files
{{< / highlight >}}

Actually, I made three different javascript choices.
`jquery`, `vue`, and `plain native vanilla`.
For simplicity reason, and also neutrality reason,
for the rest of the chapter, I use `plain` javascript.

![Jekyll: Tree: Assets][image-08-tree-assets]

#### Stylesheet: Main

The `main.css` assets is the only custom stylesheet.

* [gitlab.com/.../assets/css/main.css][tutor-css-main]

{{< highlight css >}}
html {
  height: 100%;
  overflow-y: auto;
}

body {
  background-image: url("../images/light-grey-terrazzo.png");
  min-height: 100%;
}

.maxwidth {
  margin-right: auto;
  margin-left: auto;
}

@media screen and (min-width: 1216px) {
  .maxwidth {
    max-width: 1216px;
  }
}

.layout-base {
  padding-top: 5rem;
}

footer.site-footer {
  padding-top: 4rem;
}

.blog-post p {
  margin-top: 0.5rem;
  margin-bottom: 0.5rem;
}

main,
aside {
  margin-bottom: 20px;
}

@media screen and (min-width: 769px), print {
  main {
    margin-right: 10px;
    margin-left: 10px;
  }

  aside {
    margin-left: 10px;
    margin-right: 10px;
  }
}

@media screen and (min-width: 1280px) {
  main {
    margin-left: 0px;
  }

  aside {
    margin-right: 0px;
  }
}
{{< / highlight >}}

We have already discussed the stylesheet in Bulma article.

* [Bulma - Overview][local-bulma]

-- -- --

### 3: Layout: Refactoring Base

> Put on clothes on HTML body.

We should start over again from the very fundamental layout.

#### Layout: Liquid Default

* [gitlab.com/.../_layouts/default.html][tutor-vl-default]

{{< highlight jinja >}}
<!DOCTYPE html>
<html>

<head>
  {% include site/head.html %}
</head>

<body>
  <!-- header -->
  {% include site/header.html %}

  <!-- main -->
  <div class="columns is-8 layout-base maxwidth">
    {{ content }}
  </div>

  <!-- footer -->
  {% include site/footer.html %}
  {% include site/scripts.html %}
</body>

</html>
{{< / highlight >}}

No we have four `include`s:

* `head`,

* `header`,

* `footer`,

* `script` (new).

For flexibility reason,
I move two `element`s to child layout:

* `main`, and

* `aside`.

![Jekyll: Base Layout][image-08-vim-default]

#### Partial Liquid: Head

* [gitlab.com/.../_includes/site/head.html][tutor-vi-head]

There is a few additional changes here.
Now the header contain stylesheets, meta tag, and icons.

{{< highlight html >}}
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible"
        content="IE=edge,chrome=1">
  <meta name="viewport"
        content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>{{ (page.title | default: site.title) | escape }}</title>

  <link rel="stylesheet" type="text/css"
        href="{{ site.baseurl }}/assets/css/bulma.css">
  <link rel="stylesheet" type="text/css"
        href="{{ site.baseurl }}/assets/css/main.css">

  <link rel="shortcut icon" type="image/vnd.microsoft.icon"
        href="{{ site.baseurl }}/favicon.ico"/>

  <script src="{{ site.baseurl }}/assets/js/bulma-burger-plain.js"></script>
{{< / highlight >}}

We will add meta SEO later.
Here we start from simple thing.

#### Partial Liquid: Header

It is a long header, so I crop the code.
You can get the complete code in the repository.

* [gitlab.com/.../_includes/site/header.html][tutor-vi-header]

{{< highlight html >}}
  <nav role="navigation" aria-label="main navigation"
       class="navbar is-fixed-top is-dark maxwidth">
    <div class="navbar-brand">
      <a class="navbar-item"
         href="{{ site.baseurl | prepend:site.url }}">
        <img src="{{ site.baseurl }}/assets/images/logo-gear.png" 
           alt="Home" />
      </a>
      <a class="navbar-item" href="{{ site.baseurl }}/pages/">Blog</a>

      <a role="button" class="navbar-burger burger" ...>
        ...
      </a>
    </div>

    <div id="navbarBulma" class="navbar-menu">
      <div class="navbar-start">
        ...
      </div>

      <div class="navbar-end">
        ...
      </div>

    </div>
  </nav>
{{< / highlight >}}

![Jeykll: Navigation Bar: Header][image-vc-pa-header]

This is an example of header using `plain` .
You can switch to `jquery` or `vue` with example,
in `html-bulma` repository.

#### Partial Liquid: Scripts

There is nothing here yet.

{{< highlight html >}}
  {% comment %}<!-- JavaScript at end of body. -->{% endcomment %}
{{< / highlight >}}

You can have the code here:

* [gitlab.com/.../_includes/site/scripts.html][tutor-vi-scripts]

#### Layout Liquid: Footer

* [gitlab.com/.../_includes/site/footer.html][tutor-vi-footer]

{{< highlight html >}}
  <footer class="site-footer">
    <div class="navbar is-fixed-bottom maxwidth
          is-dark has-text-centered is-vcentered">
      <div class="column">
        &copy; {{ site.time | date: '%Y' }}
      </div>
    </div>
  </footer>
{{< / highlight >}}

-- -- --

### 4: Layout: Home

Consider use `home` layout` to begin,
and all other layout later.

#### Layout Liquid: Home

> This is a single column design.

* [gitlab.com/.../_layouts/home.html][tutor-vl-home]

`liquid`'s template inheritance in `jekyll`,
set in `frontmatter`.

{{< highlight jinja >}}
---
layout : default
---

  <main role="main" 
        class="column is-full">
    <article class="blog-post box">
      <h4 class="title is-4"
        >{{ page.title | default: site.title }}</h4>
      {{ content }}
    </article>

    <div class="box">
      This is a home kind layout,
      to show landing page.
    </div>
  </main>
{{< / highlight >}}

#### Page Content: index

It is the same old story.

* [gitlab.com/.../index.md][tutor-vc-index]

{{< highlight html >}}
---
layout    : home
---

To have, to hold, to love,
cherish, honor, and protect?
  
To shield from terrors known and unknown?
To lie, to deceive?

To live a double life,
to fail to prevent her abduction,
erase her identity, 
force her into hiding,
take away all she has known.
{{< / highlight >}}

#### Render: Browser

Open in your favorite browser.
You should see, a simple homepage, by now.

* <http://localhost:4000/>

Well, I cut the content a little to make this screenshot below:

![Jeykll: Page Content: Home][image-vc-pa-home]

Notice that this is a single column Bulma page.
The other page is double columns,
and deserve a its own explanation.

-- -- --

### 5: Layout: Page, Post

Our Page Kind, and Post Kind is simply,
a double columns version of above layout.

#### Layout Liquid: Page

* [gitlab.com/.../_layouts/page.html][tutor-vl-page]

{{< highlight jinja >}}
---
layout: default
aside_message : This is a page kind layout.
---

  <main role="main" 
        class="column is-two-thirds">
    <article class="blog-post box">
      <h4 class="title is-4"
        >{{ page.title | default: site.title }}</h4>
      {{ content }}
    </article>
  </main>

  <aside class="column is-one-thirds">
    <div class="box">
      {{ layout.aside_message }}
    </div>
  </aside>
{{< / highlight >}}

![Jekyll: Extending Default: Page][image-parent-page]

We have two block `element`s here:

* `main`,

* `aside`.

#### Page Content: pages/about

No difference with previous chapter.

* [gitlab.com/.../pages/about.md][tutor-vc-pa-about]

{{< highlight markdown >}}
---
layout    : page
title     : Rescue Mission
---

This was not a rescue mission!

Let me put to you like this.
If the secretary wanted me out of there,
then things are really bad out here
{{< / highlight >}}

We can say that this content wear `page` layout.

#### Render: Browser

Open in your favorite browser.
You should see, a black and white about page, by now.

![Jekyll: Page Content: pages/about][image-vc-about]

#### Layout Liquid: Post

* [gitlab.com/.../_layouts/post.html][tutor-vl-post]

{{< highlight jinja >}}
---
layout: default
aside_message : This is a post kind layout.
---

  <main role="main" 
        class="column is-two-thirds">
    <article class="blog-post box">
      <h4 class="title is-4"
        >{{ page.title | default: site.title }}</h4>
      <p><strong>{{ page.date | date: "%B %d, %Y" }}</strong></p>
      {{ content }}
    </article>
  </main>

  <aside class="column is-one-thirds">
    <div class="box">
      {{ layout.aside_message }}
    </div>
  </aside>
{{< / highlight >}}

We also have two block `element`s here:

* `main`,

* `aside`.

#### Post Content: winter.md

No difference with previous chapter.

* [gitlab.com/.../_posts/2016-01-01-winter.md][tutor-vc-po-winter]

{{< highlight markdown >}}
---
layout  : post
title   : Surviving White Winter
date    : 2016-01-01 08:08:15 +0700
tags      : ['sleepy', 'husky']
---

It was a frozen winter in cold war era.
We were two lazy men, a sleepy boy, two long haired women,
a husky with attitude, and two shotguns.
After three weeks, we finally configure javascript.

But we lost our beloved husky before we finally made it.
Now, every january, we remember our husky,
that helped all of us to survive.
{{< / highlight >}}

#### Render: Browser

Open in your favorite browser.
You should see, a black and white post, by now.

![Jekyll: Post Content: _posts/winter][image-vc-winter]

-- -- --

### What is Next?

Consider continue reading [ [Jekyll Bulma - CSS Layout][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/08/03/jekyll-bmd-css-layout/
[local-whats-theme]:    {{< baseurl >}}ssg/2020/06/24/jekyll-plain-theme-bundling/

[local-bulma]:          {{< baseurl >}}frontend/2019/03/01/bulma-overview/
[local-bulma-navbar]:   {{< baseurl >}}frontend/2019/03/03/bulma-navbar/
[local-bulma-layout]:   {{< baseurl >}}frontend/2019/03/07/bulma-sass-layout/

[tutor-html-master-08]: {{< tutor-jekyll-bmd >}}/tutor-08/

[tutor-css-main]:       {{< tutor-jekyll-bmd >}}/tutor-08/assets/css/main.css

[tutor-vl-default]:     {{< tutor-jekyll-bmd >}}/tutor-08/_layouts/default.html
[tutor-vl-home]:        {{< tutor-jekyll-bmd >}}/tutor-08/_layouts/home.html
[tutor-vl-page]:        {{< tutor-jekyll-bmd >}}/tutor-08/_layouts/page.html
[tutor-vl-post]:        {{< tutor-jekyll-bmd >}}/tutor-08/_layouts/post.html
[tutor-vi-head]:        {{< tutor-jekyll-bmd >}}/tutor-08/_includes/site/head.html
[tutor-vi-header]:      {{< tutor-jekyll-bmd >}}/tutor-08/_includes/site/header.html
[tutor-vi-footer]:      {{< tutor-jekyll-bmd >}}/tutor-08/_includes/site/footer.html
[tutor-vi-scripts]:     {{< tutor-jekyll-bmd >}}/tutor-08/_includes/site/scripts.html
[tutor-vc-index]:       {{< tutor-jekyll-bmd >}}/tutor-08/index.md
[tutor-vc-pa-about]:    {{< tutor-jekyll-bmd >}}/tutor-08/pages/about.md
[tutor-vc-po-winter]:   {{< tutor-jekyll-bmd >}}/tutor-08/_posts/2016-01-01-winter.md

[image-png-layout-page]:    {{< baseurl >}}assets/posts/frontend/2019/12/layout-page.png
[image-svg-layout-page]:    {{< baseurl >}}assets/posts/frontend/2019/12/layout-page.svg

[image-08-tree-assets]: {{< assets-ssg >}}/2020/08/bulma/08-tree-assets.png
[image-08-vim-default]: {{< assets-ssg >}}/2020/08/bulma/08-vim-default.png
[image-vc-pa-home]:     {{< assets-ssg >}}/2020/08/bulma/08-pages-home.png
[image-vc-pa-header]:   {{< assets-ssg >}}/2020/08/bulma/08-pages-header.png
[image-parent-page]:    {{< assets-ssg >}}/2020/08/bulma/08-parent-page.png
[image-parent-post]:    {{< assets-ssg >}}/2020/08/bulma/08-parent-post.png
[image-vc-about]:       {{< assets-ssg >}}/2020/08/bulma/08-pages-about.png
[image-vc-winter]:      {{< assets-ssg >}}/2020/08/bulma/08-posts-winter.png
