---
type   : post
title  : "Jekyll Bulma - Layout - Index List"
date   : 2020-08-07T09:17:35+07:00
slug   : jekyll-bmd-layout-index-list
categories: [ssg]
tags      : [jekyll, bulma]
keywords  : [liquid, layout, columns, color, multiline, responsive]
author : epsi
opengraph:
  image: assets-ssg/2020/08/bulma/10-by-month.png

toc    : "toc-2020-06-jekyll-step"

excerpt:
  Building Jekyll Site Step by step, using Bulma MD as stylesheet.
  Reponsive column for index and colors in content with Liquid Layout.
---

### Preface

> Goal: Reponsive column for index and colors in content with Liquid Layout.

#### Source Code

This article use [tutor-10][tutor-html-master-10] theme for colors,
and additionaly [tutor-12][tutor-html-master-12] theme
for multiline responsive content.
We will create it step by step.

-- -- --

### 1: Colors: Blog Index

> How about content?

Previously, we have manage color for parent layout.
And we can actually, manage color for its content too.

#### Partial Liquid: Blog List

> This is the Bulma View Part.

To enhance the view,
consider give more meta tag details,
and separate it into partial.

We have one partial HTML view for this layout.

* [gitlab.com/.../_includes/index/blog-list.html][tutor-vi-blog-list]

{{< highlight html >}}
  {% assign color_main  = page.color_main  | default: layout.color_main %}

  <div class="post-list">
  {% for post in posts %}
    <div class="archive pb-2">
    <div class="meta-item px-2 py-2 hoverable">

      <strong><a class="meta_link {{ color_main }}-text text-darken-3" 
        href="{{ post.url | prepend: site.baseurl }}"
        >{{ post.title }}
      </strong></a>

      {% include index/blog-each-meta.html %}

      <div>{{ post.excerpt }}</div>

      <div class="read-more is-light">
        <a href="{{ post.url | prepend: site.baseurl }}" 
           class="button is-dark is-small {{ color_main }} z-depth-1 hoverable"
          >Read More&nbsp;</a>
      </div>

    </div></div>
  {% endfor %}
  </div>
{{< / highlight >}}

#### Partial Liquid: Index/ Blog Each Meta

You might notice on above script:

{{< highlight jinja >}}
{% include index/blog-each-meta.html %}
{{< / highlight >}}

The `blog-each-meta` partial is a long detail:

* [gitlab.com/.../_includes/index/blog-each-meta.html][tutor-vi-blog-meta]

{{< highlight jinja >}}

  <div class="meta">
    <div class="meta_time is-pulled-left">
      <i class="fa fa-calendar"></i>
      <time datetime="{{ post.date | date_to_xmlschema }}">
      {{ post.date | date: "%b %-d, %Y" }}</time>
    </div>      
    <div class="meta_tags is-pulled-right">
      {% for tag in post.tags %}
        <a class="tag is-small is-dark {{ color_main }} z-depth-1 hoverable"
           href="{{ site.baseurl }}/tags/#{{ tag | slugify }}"
          >{{ tag }}&nbsp;<span class="fa fa-folder"></span>
        </a>&nbsp;
      {% endfor %}
    </div>
  </div> 

  <div class="is-clearfix"></div>
{{< / highlight >}}

#### Render: Browser

Open in your favorite browser.

![Jekyll Bulma: Blog List][image-blog-list]

-- -- --

### 2: Colors: Tag List, Category List.

#### Partial Liquid: Terms Badge

> This is the Bulma View Part.

We can also match the layout color,
with the color in view.

* [gitlab.com/.../_includes/index/terms-badge.html][tutor-vi-terms-badge]]

{{< highlight html >}}
  {% assign color_main  = page.color_main  | default: layout.color_main %}

  <div class="field is-grouped is-grouped-multiline">
  {% for item in (0..terms.size) %}{% unless forloop.last %}
    {% assign this_word = term_array[item] | strip_newlines %}
    <div class="tags has-addons">
      {{ $posts := where $value.Pages "Type" "post" }}
      {{ $postCount := len $posts -}}
      <a href="#{{ this_word | slugify }}">
        <div class="tag is-light {{ color_main }} lighten-2 z-depth-1"
          >{{ this_word }} 
        </div><div class="tag is-dark {{ color_main }} darken-2 z-depth-1"
          >{{ terms[this_word].size }}
        </div>
      </a>
    </div>
    &nbsp;
  {% endunless %}{% endfor %}
    <div class="tags dummy"></div>
  </div>
{{< / highlight >}}

The `forloop` reference can be read here:

* [Liquid: The forloop object][forloop]

[forloop]: https://shopify.dev/docs/themes/liquid/reference/objects/for-loops

#### Partial Liquid: Terms Tree

> This is the Bulma View Part.

We require different icon for `categories` and `tags`.
So we can set it in `page.term_icon`.

* [gitlab.com/.../_includes/index/terms-tree.html][tutor-vi-terms-tree]

{{< highlight html >}}
  <section class="py-1" id="archive">
  {% for item in (0..terms.size) %}{% unless forloop.last %}
    {% assign this_word = term_array[item] | strip_newlines %}
      <div id="{{ this_word | slugify }}" class ="anchor-target">
        <span class="{{ page.term_icon }}"></span> 
        &nbsp; {{ this_word }}
      </div>

      <div class="archive-list py-1">
      {% for post in terms[this_word] %}
        {% if post.title != null %}
          {% include index/each-post.html %}
        {% endif %}
      {% endfor %}
      </div>

  {% endunless %}{% endfor %}
  </section>
{{< / highlight >}}

#### Partial Liquid: Each Post

We are going to use this partial in three places:
`terms-tree.html`, `by-year.html`, and  `by-month.html`.

* [gitlab.com/.../_includes/index/each-post.html][tutor-vi-each-post]

{{< highlight html >}}
  <div class="archive-item meta-item">
    <div class="meta_link has-text-right">
      <time class="meta_time is-pulled-right"
            datetime="{{ post.date | date_to_xmlschema }}">
        {{ post.date | date: "%b %-d, %Y" }}&nbsp;
        &nbsp;<span class="fa fa-calendar"></span></time></div>
    <div class="is-pulled-left">
    <a href="{{ post.url | prepend: site.baseurl }}">
      {{ post.title }}
    </a></div>
    <div class="is-clearfix"></div>
  </div>
{{< / highlight >}}

Now featuring icon.

#### Page Content: Categories

* [gitlab.com/.../pages/categories.html][tutor-pa-categories]

{{< highlight markdown >}}
---
# decoration
term_icon : fa fa-folder
---
{{< / highlight >}}

![Jekyll Bulma: Categories with Tree Details][image-cats-list]

#### Page Content: Tags

* [gitlab.com/.../pages/tags.html][tutor-pa-tags]

{{< highlight markdown >}}
---
# decoration
term_icon : fa fa-tag
---
{{< / highlight >}}

![Jekyll Bulma: Tags with Tree Details][image-tags-list]

#### Partial Liquid: Terms Tree

> This is the Bulma View Part.

Or even better we can put in a boxed like section.

* [gitlab.com/.../_includes/index/terms-tree.html][tutor-vi-terms-tree11]

{{< highlight html >}}
  <section class="py-1" id="archive">
  {% for item in (0..terms.size) %}{% unless forloop.last %}
    {% assign this_word = term_array[item] | strip_newlines %}

    <div class="widget white z-depth-1 hoverable m-b-10">
      <div class="widget-header {{ color_main }} lighten-4">

        <div id="{{ this_word | slugify }}" class ="anchor-target">
          <span class="{{ page.term_icon }}"></span> 
          &nbsp; {{ this_word }}
        </div>

      </div>
      <div class="widget-body">

        <div class="archive-list py-1">
        {% for post in terms[this_word] %}
          {% if post.title != null %}
            {% include index/each-post.html %}
          {% endif %}
        {% endfor %}
        </div>

      </div>
    </div>

  {% endunless %}{% endfor %}
  </section>
{{< / highlight >}}

![Jekyll Bulma: Tags with Tree Details (boxed)][image-tags-list-11]

You do not need to setup any frontmatter to access `page.term_icon`.

The color main has been set in `terms-badge`.

{{< highlight html >}}
  {% assign color_main  = page.color_main  | default: layout.color_main %}
{{< / highlight >}}

So you do not need to set the color down, all over again.

-- -- --

### 3: Colors: Archive By Year, then By Month

#### Partial Liquid: Archive By Year

> This is the Bulma View Part

The liquid logic, is remain the same.
But the view, contained in a boxed `section` element.

* [gitlab.com/.../_includes/index/by-year.html][tutor-vi-by-year]

{{< highlight html >}}
  <section class="{{ color_main }} lighten-5 z-depth-1 hoverable
                  px-1 py-1 my-1">
    <div class ="anchor-target archive-year" 
         id="{{ year.name }}">{{ year_text }}</div>

    <div class="archive-list py-1">
      {% for post in year.items %}
        {% include index/each-post.html %}
      {% endfor %}
    </div>
  </section>
{{< / highlight >}}

![Jekyll Bulma: Archive By Year][image-by-year]

#### Partial Liquid: Archive By Month

> This is the Bulma View Part

The liquid logic, is remain the same.
But the view, contained in a boxed `section` element.

* [gitlab.com/.../_includes/index/by-month.html][tutor-vi-by-month]

{{< highlight html >}}
  <section class="white z-depth-1 hoverable
                  px-1 py-1 my-1">
    <div class ="anchor-target archive-year" 
         id="{{ year.name }}">{{ year_text }}</div>

    {% assign postsByMonth = year.items
              | group_by_exp:"post", "post.date | date: '%m'"
              | sort: 'name'
              | reverse %}

    {% for month in postsByMonth %}

    <div class="py-1">

      {% for post in month.items limit:1 %}
      <div class ="archive-month" 
           id="{{ year.name }}-{{ month.name }}">
           {{ post.date | date: '%b - %Y' }}</div>
      {% endfor %}

      <div class="archive-list py-1">
        {% for post in month.items %}
          {% include index/each-post.html %}
        {% endfor %}
      </div>
    </div>

    {% endfor %}
  </section>
{{< / highlight >}}

![Jekyll Bulma: Archive By Month][image-by-month]

-- -- --

### 4: Responsive: Index

Consider fast forward to [tutor-12][tutor-html-master-12] theme
for multiline responsive content.
We need to fast forward, because we need to see the difference.

In `Bulma`, there is a `is-multiline` class,
that enable each box to flow downward if the page width is not enough.
The later image will explain better, than explaining by word.

#### General Preview

Our base layout is based on responsive design.
This below is general preview of,
what responsive content that we want to achieve.

![Jekyll: General Preview of Responsive Content Design][image-png-layout-content]

Source image is available in inkscape SVG format,
so you can modify, and make your own preview quickly.

* [Responsive Content: Image Source][image-svg-layout-content]

Just keep in mind that,
responsive content is different with responsive page.
The responsive content is placed inside a responsive page.

#### Using Multiline Class

Consider this code below:

{{< highlight html >}}
  <div class="row py-1" id="archive">
  {% for item in (0..terms.size) %}{% unless forloop.last %}
    {% assign this_word = term_array[item] | strip_newlines %}

    <div class="column is-full-mobile
                is-half-tablet is-one-third-widescreen">
      ...
    </div>

  {% endunless %}{% endfor %}
  </div>
{{< / highlight >}}

This code has:

* One Column in mobile screen: Full.

* Two Columns in tablet screen: Half.

* Three Columns in tablet screen: One Third.

You can make your own arrangement.

#### Partial: Liquid: By Year

* [gitlab.com/.../_includes/index/by-year.html][tutor-vi-by-year-12]

{{< highlight jinja >}}
{% assign color_main  = page.color_main
          | default: layout.color_main %}

{% assign postsByYear = posts |
          group_by_exp: "post", "post.date | date: '%Y'"  %}

<div class="columns is-multiline py-1" id="archive">
{% for year in postsByYear %}

  {% capture spaceless %}
    {% assign current_year = 'now' | date: '%Y' %}
    {% assign year_text = nil %}

    {% if year.name == current_year %}
      {% assign year_text = year.name
                | prepend: "This year's posts (" | append: ')' %}
    {% else %}
      {% assign year_text = year.name %}
    {% endif %}
  {% endcapture %} 

  <section class="column is-full-mobile
              is-half-tablet is-one-third-widescreen">

    <div class="widget white z-depth-1 hoverable m-b-10">
      <div class="widget-header {{ color_main }} lighten-4">

        <div class ="anchor-target archive-year" 
             id="{{ year.name }}">{{ year_text }}</div>

      </div>
      <div class="widget-body">

        <div class="archive-list py-1">
          {% for post in year.items %}
            {% include index/each-post.html %}
          {% endfor %}
        </div>

      </div>
    </div>

  </section>

{% endfor %}
</div>
{{< / highlight >}}

![Jekyll: Responsive Content: Multiline Archive by Year][image-vc-by-year]

#### Change: Liquid: By Year

I simply change the section.

{{< highlight html >}}
<div id="archive">
{% for year in postsByYear %}
  <section class="{{ color_main }} lighten-5 z-depth-1 hoverable
                  px-1 py-1 my-1">
    ...
  </section>
{% endfor %}
</div>
{{< / highlight >}}

Into this box frame layout below

{{< highlight html >}}
<div class="row" id="archive">
{% for year in postsByYear %}
  <section class="column is-full-mobile
              is-half-tablet is-one-third-widescreen">
    <div class="widget white z-depth-1 hoverable m-b-10">
      ...
    </div>
  </section>
{% endfor %}
</div>
{{< / highlight >}}

#### Partial: Liquid: By Month

* [gitlab.com/.../_includes/index/by-month.html][tutor-vi-by-month-12]

{{< highlight jinja >}}
{% assign color_main  = page.color_main
          | default: layout.color_main %}

{% assign postsByYear = posts
          | group_by_exp: "post", "post.date | date: '%Y'"  %}

<div id="archive">
{% for year in postsByYear %}

  {% capture spaceless %}
    {% assign current_year = 'now' | date: '%Y' %}
    {% assign year_text = nil %}

    {% if year.name == current_year %}
      {% assign year_text = year.name
                | prepend: "This year's posts (" | append: ')' %}
    {% else %}
      {% assign year_text = year.name %}
    {% endif %}
  {% endcapture %} 

  <div>
    <div class ="anchor-target archive-year" 
         id="{{ year.name }}">
       {{ year_text }}
    </div>

    {% assign postsByMonth = year.items
              | group_by_exp:"post", "post.date | date: '%m'"
              | sort: 'name'
              | reverse %}

    <div class="columns is-multiline py-1">
    {% for month in postsByMonth %}
    <section class="column is-full-mobile
              is-half-tablet is-one-third-widescreen">

      <div class="widget white z-depth-1 hoverable mb-2">
        <div class="widget-header {{ color_main }} lighten-4">

          {% for post in month.items limit:1 %}
          <div class ="archive-month" 
               id="{{ year.name }}-{{ month.name }}">
               {{ post.date | date: '%b - %Y' }}</div>
          {% endfor %}

        </div>
        <div class="widget-body">

          <div class="archive-list py-1">
            {% for post in month.items %}
              {% include index/each-post.html %}
            {% endfor %}
          </div>

        </div>
      </div>

    </section>
    {% endfor %}
    </div>
  </div>

{% endfor %}
</div>
{{< / highlight >}}

![Jekyll: Responsive Content: Multiline Archive by Month][image-vc-by-month]

#### Change: Liquid: By Month

This is a little bit complex.

{{< highlight html >}}
<div id="archive">
 ...

    {% for month in postsByMonth %}
    <div class="py-1">
      ....
    </div>
    {% endfor %}
  ...
</div>
{{< / highlight >}}

Into this box frame layout below:

{{< highlight html >}}
<div id="archive">
  ...
    <div class="columns is-multiline py-1">
    {% for month in postsByMonth %}
      <section class="column is-full-mobile
                      is-half-tablet is-one-third-widescreen">
        ...
      </section>
    {% endfor %}
    </div>
  ...
</div>
{{< / highlight >}}

#### Partial: Liquid: Tags Tree

* [gitlab.com/.../_includes/index/terms-tree.html][tutor-vi-terms-tree12]

{{< highlight jinja >}}
  <section class="columns is-multiline py-1" id="archive">
  {% for item in (0..terms.size) %}{% unless forloop.last %}
    {% assign this_word = term_array[item] | strip_newlines %}
  <div class="column is-full-mobile
              is-half-tablet is-one-third-widescreen">

    <div class="widget white z-depth-1 hoverable m-b-10">
      <div class="widget-header {{ color_main }} lighten-4">

        <div id="{{ this_word | slugify }}" class ="anchor-target">
          <span class="{{ page.term_icon }}"></span> 
          &nbsp; {{ this_word }}
        </div>

      </div>
      <div class="widget-body">

        <div class="archive-list py-1">
        {% for post in terms[this_word] %}
          {% if post.title != null %}
            {% include index/each-post.html %}
          {% endif %}
        {% endfor %}
        </div>

      </div>
    </div>

  </div>
  {% endunless %}{% endfor %}
  </section>
{{< / highlight >}}

#### Change: Liquid: Tags Tree

This is also need carefully carved.

{{< highlight html >}}
  <div class="py-1" id="archive">
  {% for item in (0..terms.size) %}{% unless forloop.last %}
    {% assign this_word = term_array[item] | strip_newlines %}

    <div class="widget white z-depth-1 hoverable m-b-10">
      ...
    </div>

  {% endunless %}{% endfor %}
  </div>
{{< / highlight >}}

Into this box frame layout below:

{{< highlight html >}}
  <div class="row py-1" id="archive">
  {% for item in (0..terms.size) %}{% unless forloop.last %}
    {% assign this_word = term_array[item] | strip_newlines %}

    <div class="column is-full-mobile
                is-half-tablet is-one-third-widescreen">
      <div class="widget white z-depth-1 hoverable m-b-10">
        ...
      </div>
    </div>

  {% endunless %}{% endfor %}
  </div>
{{< / highlight >}}

#### Responsive Tags

You can also examine in figure below,
how the each screen suit this multiline layout:

* Mobile (Full): One Column

![Jekyll: Multiline Tags Tree in Mobile Screen][image-vc-tags-mobile]

* Tablet (Half): Two Columns

![Jekyll: Multiline Tags Tree in Tablet Screen][image-vc-tags-tablet]

* Wide Screen (One Third): Three Columns

![Jekyll: Multiline Tags Tree in Wide Screen][image-vc-tags-wide]

We are done with both theme layout and content layout.

-- -- --

### What is Next?

Consider continue reading [ [Jekyll Bulma - Widget - Simple HTML][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/07/08/jekyll-bmd-widget-simple-html/
[tutor-html-master-10]: {{< tutor-jekyll-bmd >}}/tutor-10/
[tutor-html-master-11]: {{< tutor-jekyll-bmd >}}/tutor-11/
[tutor-html-master-12]: {{< tutor-jekyll-bmd >}}/tutor-12/

[tutor-vi-blog-meta]:   {{< tutor-jekyll-bmd >}}/tutor-10/_includes/index/blog-each-meta.html
[tutor-vi-blog-list]:   {{< tutor-jekyll-bmd >}}/tutor-10/_includes/index/blog-list.html
[tutor-vi-each-post]:   {{< tutor-jekyll-bmd >}}/tutor-10/_includes/index/each-post.html
[tutor-vi-terms-badge]: {{< tutor-jekyll-bmd >}}/tutor-10/_includes/index/terms-badge.html
[tutor-vi-terms-tree]:  {{< tutor-jekyll-bmd >}}/tutor-10/_includes/index/terms-tree.html
[tutor-vi-by-year]:     {{< tutor-jekyll-bmd >}}/tutor-10/_includes/index/by-year.html
[tutor-vi-by-month]:    {{< tutor-jekyll-bmd >}}/tutor-10/_includes/index/by-month.html

[tutor-vi-terms-tree11]:{{< tutor-jekyll-bmd >}}/tutor-11/_includes/index/terms-tree.html
[tutor-vi-terms-tree12]:{{< tutor-jekyll-bmd >}}/tutor-12/_includes/index/terms-tree.html
[tutor-vi-by-year-12]:  {{< tutor-jekyll-bmd >}}/tutor-12/_includes/index/by-year.html
[tutor-vi-by-month-12]: {{< tutor-jekyll-bmd >}}/tutor-12/_includes/index/by-month.html

[tutor-pa-tags]:        {{< tutor-jekyll-bmd >}}/tutor-10/pages/tags.html
[tutor-pa-categories]:  {{< tutor-jekyll-bmd >}}/tutor-10/pages/categories.html

[image-blog-list]:      {{< assets-ssg >}}/2020/08/bulma/10-blog-list.png
[image-tags-list]:      {{< assets-ssg >}}/2020/08/bulma/10-tags-list.png
[image-cats-list]:      {{< assets-ssg >}}/2020/08/bulma/10-cats-list.png
[image-by-year]:        {{< assets-ssg >}}/2020/08/bulma/10-by-year.png
[image-by-month]:       {{< assets-ssg >}}/2020/08/bulma/10-by-month.png
[image-tags-list-11]:   {{< assets-ssg >}}/2020/08/bulma/11-tags-list.png
[image-vc-by-year]:     {{< assets-ssg >}}/2020/08/bulma/12-multiline-archive-by-year.png
[image-vc-by-month]:    {{< assets-ssg >}}/2020/08/bulma/12-multiline-archive-by-month.png

[image-vc-tags-mobile]: {{< assets-ssg >}}/2020/08/bulma/12-multiline-tags-mobile.png
[image-vc-tags-tablet]: {{< assets-ssg >}}/2020/08/bulma/12-multiline-tags-tablet.png
[image-vc-tags-wide]:   {{< assets-ssg >}}/2020/08/bulma/12-multiline-tags-wide.png

[image-png-layout-content]: {{< baseurl >}}assets/posts/frontend/2019/12/layout-content.png
[image-svg-layout-content]: {{< baseurl >}}assets/posts/frontend/2019/12/layout-content.svg
