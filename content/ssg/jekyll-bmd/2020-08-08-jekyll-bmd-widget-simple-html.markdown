---
type   : post
title  : "Jekyll Bulma - Widget - Simple"
date   : 2020-08-08T09:17:35+07:00
slug   : jekyll-bmd-widget-simple-html
categories: [ssg]
tags      : [jekyll, bulma]
keywords  : [liquid, layout, color, widget, affiliates, responsive image]
author : epsi
opengraph:
  image: assets-ssg/2020/08/bulma/11-nerdtree-widget.png

toc    : "toc-2020-06-jekyll-step"

excerpt:
  Building Jekyll Site Step by step, using Bulma MD as stylesheet.
  Simple HTML to achieved wordpress like side panel widget.
---

### Preface

> Goal: Simple HTML to achieved wordpress like side panel widget.

#### Source Code

This article use [tutor-11][tutor-html-master-11] theme.
We will create it step by step.

-- -- --

### 1: Prepare

These widget will be shown in both `page` kind and `post` kind,
using `columns-double` layout.
We need to change the whole part of the `aside` element,
so that `aside` element can contain multiple widget.

#### Parent Layout: Liquid: Double Columns

Consider alter the `aside` element,
in parent layout for double column layout.

* [gitlab.com/.../_layouts/columns-double.html][tutor-vl-double]

{{< highlight jinja >}}
  <aside id="aside_toggler"
         class="column is-one-thirds">
    {% include {{ layout.aside_content }} %}
  </aside>
{{< / highlight >}}

#### Layout: Page and Post

Where in `_layout/page.html` you can set in frontmatter.

* [gitlab.com/.../_layouts/page.html][tutor-vl-page]

{{< highlight jinja >}}
aside_content   : page/aside-content.html
{{< / highlight >}}

And in `_layout/post.html` you can set in frontmatter.

* [gitlab.com/.../_layouts/post.html][tutor-vl-post]

{{< highlight jinja >}}
aside_content   : post/aside-content.html
{{< / highlight >}}

#### Partial: Page Aside

The `aside` element is depend on what `widget` you need to show.
My default arrangement can be shown as below:

* [gitlab.com/.../_includes/page/aside-content.html][tutor-vpa-aside-content]

{{< highlight jinja >}}
{% include widget/recent-posts.html %}
{{< / highlight >}}

#### Layout: Post

So is the `post` kind, it has `widget` on right `sidebar`,
with my default arrangement as below layout::

* [gitlab.com/.../_includes/post/aside-content.html][tutor-vpo-aside-content]

{{< highlight jinja >}}
{% include widget/archive.html %}
{% include widget/related-posts.html %}
{% include widget/tags.html %}
{{< / highlight >}}

You are freely to change what `widget` you want to show.
I utilize unused `block` to contain all widget.

{{< highlight jinja >}}
{% comment %}
  {% include widget/friends.html %}
  {% include widget/archive-gitlab.html %}
  {% include widget/archive-github.html %}

  {% include widget/categories.html %}
  {% include widget/tags.html %}

  {% include widget/recent-posts.html %}
  {% include widget/related-posts.html %}

  {% include widget/affiliates.html %}
  {% include widget/oto-spies.html %}
{% endcomment %}
{{< / highlight >}}

It is basically just making a scrathpad note for myself.
So that, I can copy paste any line from this `block`,
whenever I need to.

-- -- --

### 2: Widget Parent Class

`liquid` doesn't have block concept,
but you can have similar result using `capture`,
to prepare required variable.

#### Template Layout: Widget

This means you can refactor common component such as `widget`,
into a generic template as below:

* [gitlab.com/.../_includes/template/widget.html][tutor-vt-widget]

{{< highlight jinja >}}
<section class="aside-wrapper {{ color }}">
  <div class="widget white z-depth-3 hoverable">

    <div class="widget-header {{ color }} lighten-4">
      {{ widget_header }}
    </div>
    
    <div class="widget-body">
      {{ widget_body }}
    </div>

  </div>
</section>
{{< / highlight >}}

![Jekyll: NERDTree: Widget Layout][image-nerdtree-widget]

-- -- --

### 3: Simple Example

#### Layout: Layout: Page or Post

Consider examine only affiliate links:

{{< highlight jinja >}}
{% include widget/affiliates.html %}
{{< / highlight >}}

#### Partial Widget: HTML Affiliates

Now we can use above template,
for our simple widget example as below code:

* [gitlab.com/.../_includes/widget/affiliates.html][tutor-vw-affiliates]

{{< highlight jinja >}}
{% capture widget_header %}
  <strong>Affiliates</strong>
  <span class="fas fa-fingerprint is-pulled-right"></span>
{% endcapture %}

{% capture widget_body %}
  <ul class="widget-list">
    <li><a href="http://epsi-rns.github.io/"
          >Linux/BSD Desktop Customization</a></li>
    <li><a href="http://epsi-rns.gitlab.io/"
          >Mobile/Web Development Blog</a></li>
    <li><a href="http://oto-spies.info/"
          >Car Painting and Body Repair.</a></li>
  </ul>
{% endcapture %}

{% assign color = "green" %}
{% include template/widget.html %}
{{< / highlight >}}

This the basic of HTML class required for the rest of this article.

#### Capture

You should see how a variable captured.

{{< highlight jinja >}}
{% capture widget_header %}
  <strong>Affiliates</strong>
  <span class="fas fa-fingerprint is-pulled-right"></span>
{% endcapture %}
{{< / highlight >}}

And used later by calling `include template/widget.html`.

{{< highlight jinja >}}
<div class="widget-header {{ color }} lighten-4">
  {{ widget_header }}
</div>
{{< / highlight >}}

#### Render: Browser

You can open `page` kind, or `post` kind, to test this `widget`.

![Jekyll: Widget: Affiliates][image-wi-affiliates]

-- -- --

### 4: Responsive Image

#### Responsive Image Class

No need any additional setting,
Bulma has already use `img { max-width: 100%}` by default.
So you can have your own commercial advertisement.

#### Layout: Liquid: Page or Post

Consider examine only commercial links:

{{< highlight jinja >}}
{% include widget/oto-spies.html %}
{{< / highlight >}}

#### Partial Widget: HTML Oto Spies

* [gitlab.com/.../_includes/widget/oto-spies.html][tutor-vw-oto-spies]

{{< highlight jinja >}}
{% capture widget_header %}
  <strong>Car Painting and Body Repair</strong>
  <span class="fas fa-car-side is-pulled-right"></span>
{% endcapture %}

{% capture widget_body %}
  <a href="http://oto-spies.info/">
    <img src="{{ site.baseurl }}/assets/images/adverts/oto-spies-01.png"
         alt="Oto Spies"></a>
{% endcapture %}

{% assign color = "grey" %}
{% include template/widget.html %}
{{< / highlight >}}

Pick the right color, that match your business.

#### Render: Browser

You can open `page` kind, or `post` kind, to test this `widget`.

![Jekyll: Widget: Oto Spies][image-wi-oto-spies]

That both two example use pure HTML.
Now we need to leverage to use loop with `liquid`,
and also using static data with `liquid`.

-- -- --

### What is Next?

Consider continue reading [ [Jekyll Bulma - Widget - Liquid Loop][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/08/09/jekyll-bmd-widget-liquid-loop/
[tutor-html-master-11]: {{< tutor-jekyll-bmd >}}/tutor-11/

[tutor-sass-layout]:    {{< tutor-jekyll-bmd >}}/tutor-11/_sass/css/main/_layout-content.scss

[tutor-vl-double]:      {{< tutor-jekyll-bmd >}}/tutor-11/_layouts/columns-double.html
[tutor-vl-page]:        {{< tutor-jekyll-bmd >}}/tutor-11/_layouts/page.html
[tutor-vl-post]:        {{< tutor-jekyll-bmd >}}/tutor-11/_layouts/post.

[tutor-vpa-aside-content]:  {{< tutor-jekyll-bmd >}}/tutor-11/_includes/page/aside-content.html
[tutor-vpo-aside-content]:  {{< tutor-jekyll-bmd >}}/tutor-11/_includes/post/aside-content.html
[tutor-vt-widget]:      {{< tutor-jekyll-bmd >}}/tutor-11/_includes/template/widget.html
[tutor-vw-affiliates]:  {{< tutor-jekyll-bmd >}}/tutor-11/_includes/widget/affiliates.html
[tutor-vw-oto-spies]:   {{< tutor-jekyll-bmd >}}/tutor-11/_includes/widget/oto-spies.html

[image-wi-affiliates]:  {{< assets-ssg >}}/2020/08/bulma/11-widget-affiliates.png
[image-wi-oto-spies]:   {{< assets-ssg >}}/2020/08/bulma/11-widget-oto-spies.png
[image-nerdtree-widget]:{{< assets-ssg >}}/2020/08/bulma/11-nerdtree-widget.png
