---
type   : post
title  : "Jekyll Bulma - Widget - Loop"
date   : 2020-08-09T09:17:35+07:00
slug   : jekyll-bmd-widget-liquid-loop
categories: [ssg]
tags      : [jekyll, bulma]
keywords  : [liquid, widget, recent post, tags, archives]
author : epsi
opengraph:
  image: assets-ssg/2020/08/bulma/11-widget-archive-with-header.png

toc    : "toc-2020-06-jekyll-step"

excerpt:
  Building Jekyll Site Step by step, using Bulma MD as stylesheet.
  Miscellanous range loop to achieved wordpress like side panel widget.
---

### Preface

> Goal: Miscellanous range loop to achieved wordpress like side panel widget.

#### Source Code

This article use [tutor-11][tutor-html-master-11] theme.
We will create it step by step.

-- -- --

### 5: Recent Posts

This require `limit` in iteration,
and `shopfiy` has built in `limit` parameter.

* [Iteration][limit]

[limit]: https://shopify.github.io/liquid/tags/iteration/

#### Layout: Nunjucks Page or Post

Consider examine only recent posts:

{{< highlight jinja >}}
{% include widget/recent-posts.html %}
{{< / highlight >}}

#### Partial Widget: Liquid: Recent Post

* [gitlab.com/.../_includes/widget/recent-posts.html][tutor-vw-recent-posts]

{{< highlight jinja >}}
{% capture widget_header %}
  <strong>Recent Posts</strong>
  <span class="fa fa-newspaper is-pulled-right"></span>
{% endcapture %}

{% capture widget_body %}
  <ul class="widget-list">
    {% for post in site.posts limit:5 %}
    <li><a href="{{ site.baseurl }}{{ post.url }}"
      >{{ post.title }}</a></li>
    {% endfor %}
  </ul>
{% endcapture %}

{% assign color = "red" %}
{% include template/widget.html %}
{{< / highlight >}}

#### The Loop

`site.posts` is already sorted by date.
I limit the result for only first five result.

{{< highlight jinja >}}
    {% for post in site.posts limit:5 %}
    ...{{ post.title }}...
    {% endfor %}
{{< / highlight >}}

#### Render: Browser

You can open `page` kind, or `post` kind, to test this `widget`.

![Jekyll: Widget: Recent Posts][image-wi-recent-posts]

-- -- --

### 6: Taxonomy

Just a simple loop example.

#### Layout: Liquid: Page or Post

Consider examine only tags:

{{< highlight jinja >}}
{% include widget/tags.html %}
{{< / highlight >}}

#### Partial Widget: Liquid: Tags

* [gitlab.com/.../_includes/widget/tags.html][tutor-vw-tags]

{{< highlight jinja >}}
{% assign color = "light-blue" %}

{% capture widget_header %}
  <strong>Tags</strong>
  <span class="fa fa-tag is-pulled-right"></span>
{% endcapture %}

{% capture widget_body %}
  {% for tag in site.tags %}
    <a class="tag is-small is-light
              {{ color }} z-depth-1 hoverable pb-1"
       href="{{ site.baseurl }}/pages/tag#{{ tag[0] | slugify }}">
      <span class="fa fa-tag"></span>&nbsp;{{ tag[0] }}
    </a>&nbsp;
  {% endfor %} 
{% endcapture %}

{% include template/widget.html %}
{{< / highlight >}}

#### The Loop

This is actually, only a simple loop example,
but with complex html formatting.

{{< highlight jinja >}}
  {% for tag in site.tags %}
    ...{{ tag[0] }}...
  {% endfor %} 
{{< / highlight >}}

#### Render: Browser

You can open `page` kind, or `post` kind, to test this `widget`.

![Jekyll: Widget: Tags][image-wi-tags]

#### Layout: Liquid: Page or Post

The same applied with categories.

Consider examine only tags:

{{< highlight jinja >}}
{% include widget/categories.html %}
{{< / highlight >}}

#### Partial Widget: Liquid: Categories

* [gitlab.com/.../_includes/widget/categories.html][tutor-vw-categories]

{{< highlight jinja >}}
{% assign color = "cyan" %}

{% capture widget_header %}
  <strong>Categories</strong>
  <i data-feather="folder" class="float-right"></i>
{% endcapture %}

{% capture widget_body %}
  {% for cat in site.categories %}
    <a class="pb-5"
       href="{{ site.baseurl }}/pages/category#{{ cat[0] | slugify }}">
      <span class="badge oc-{{ color }}-3 z-depth-1 hoverable">
        {{ cat[0] }}&nbsp;
        <i data-feather="folder" class="feather-14"></i>
    </span></a>&nbsp;
  {% endfor %} 
{% endcapture %}

{% include template/widget.html %}
{{< / highlight >}}

#### The Loop

This is actually, only a simple loop example,
but with complex html formatting.

{{< highlight jinja >}}
  {% for cat in site.categories %}
    ...{{ cat[0] }}...
  {% endfor %} 
{{< / highlight >}}

#### Render: Browser

You can test this `widget` in your browser.

![Jekyll: Widget: Categories][image-wi-categories]

-- -- --

### 7: Grouped Archives

Making a grouping archive by month and year is not hard,
it just need carefully carved with patient.
Consider this step by step procedure to make this happened:

1. List without Grouping

2. Grouping by Year

3. Grouping by Year, then Month

4. Give a Nice Formatting.

#### List without Grouping

Consider examine from simple archive:

{{< highlight jinja >}}
{% include widget/archive-simple.html %}
{{< / highlight >}}

This require only one loops.

* [gitlab.com/.../_includes/widget/archive-simple.html][tutor-vw-arch-simple]

{{< highlight jinja >}}
{% capture widget_header %}
  <strong>Archives</strong>
  <span class="fa fa-archive is-pulled-right"></span>
{% endcapture %}

{% capture widget_body %}
  <ul class="widget-list">
    {% for post in site.posts %}
    <li>
      <a href="{{ site.baseurl }}{{ post.url }}"
        >{{ post.title }}</a>
    </li>
    {% endfor %}
  </ul>
{% endcapture %}

{% assign color = "green" %}
{% include template/widget.html %}
{{< / highlight >}}

With the result as below figure:

![Jekyll: Widget: Simple Archive][image-wi-archive-simple]

#### Grouping by Year

Consider examine from archive by year:

{{< highlight jinja >}}
{% include widget/archive-year.html %}
{{< / highlight >}}

This require two loops.

1. Loop of Year, using `group_by_exp` Filter

2. Loop of Post.

* [gitlab.com/.../_includes/widget/archive-year.html][tutor-vw-arch-year]

{{< highlight jinja >}}
{% capture widget_body %}
  {% assign pg_year   = page.date | date: '%Y' %}
  {% assign postsByYear = site.posts | group_by_exp: "post", "post.date | date: '%Y'"  %}

  {% for year in postsByYear %}
    {% if pg_year == year.name %}
      <ul class="widget-list">
        {% for post in year.items %}
        <li>
          <a href="{{ site.baseurl }}{{ post.url }}"
            >{{ post.title }}</a>
        </li>
        {% endfor %}
      </ul>
    {% endif %}
  {% endfor %}
{% endcapture %}
{{< / highlight >}}

With the result as below figure:

![Jekyll: Widget: Archive by Year][image-wi-archive-year]

#### Grouping by Year, then Month

Consider examine from archive by month:

{{< highlight jinja >}}
{% include widget/archive-month.html %}
{{< / highlight >}}

This require three loops.

1. Loop of Year, using `group_by_exp` Filter

2. Loop of Month in Year, also using ``group_by_exp`` Filter

3. Loop of Post.

* [gitlab.com/.../_includes/widget/archive-month.html][tutor-vw-arch-month]

{{< highlight jinja >}}
{% capture widget_body %}
  {% assign pg_year   = page.date | date: '%Y' %}
  {% assign pg_month  = page.date | date: '%m' %}
  
  {% assign postsByYear = site.posts | group_by_exp: "post", "post.date | date: '%Y'"  %}
  {% for year in postsByYear %}
    {% if pg_year == year.name %}
      {% assign postsByMonth = year.items | 
                group_by_exp: "post", "post.date | date: '%m'" %}
      {% assign postsByMonthSorted = postsByMonth | 
                sort: 'name' | reverse %}

      {% for month in postsByMonthSorted %}
        {% if pg_month == month.name %}
        <ul class="widget-list">
          {% for post in month.items %}
          <li>    
            <a href="{{ site.baseurl }}{{ post.url }}"
              >{{ post.title }}</a>
          </li>
          {% endfor %}
        </ul>
        {% endif %}
      {% endfor %}
    {% endif %}
  {% endfor %}
{% endcapture %}
{{< / highlight >}}

With the result as below figure:

![Jekyll: Widget: Archive by Month][image-wi-archive-month]

#### Give a Nice Formatting.

Consider examine our final archive partial:

{{< highlight jinja >}}
{% include widget/archive.html %}
{{< / highlight >}}

Each stuff should have their own header.

* [gitlab.com/.../_includes/widget/archive.html][tutor-vw-archive]

{{< highlight jinja >}}
{% capture widget_body %}
  {% assign pg_year   = page.date | date: '%Y' %}
  {% assign pg_month  = page.date | date: '%m' %}
  {% assign path_y = "/pages/by-year"  | prepend: site.baseurl %}
  {% assign path_m = "/pages/by-month" | prepend: site.baseurl %}
  
  {% assign postsByYear = site.posts | group_by_exp: "post", "post.date | date: '%Y'"  %}
  {% for year in postsByYear %}
      <div class ="archive-year" id="{{ year.name }}">
        <a href="{{ path_y }}#{{ year.name }}">
        {{ year.name }}</a>
      </div>

    {% if pg_year == year.name %}
    <ul class="widget-archive">
      {% assign postsByMonth = year.items | 
                group_by_exp: "post", "post.date | date: '%m'" %}
      {% assign postsByMonthSorted = postsByMonth | 
                sort: 'name' | reverse %}

      {% for month in postsByMonthSorted %}
      <li class="list-month">
        {% for post in month.items limit:1 %}
        <span class ="archive-month">
          <a href="{{ path_m }}#{{ post.date | date: '%Y-%m' }}">
            {{ post.date | date: '%B %Y' }}
          </a></span>
        {% endfor %}

        {% if pg_month == month.name %}
        <ul class="widget-list">
          {% for post in month.items %}
          <li>    
            <a href="{{ site.baseurl }}{{ post.url }}"
              >{{ post.title }}</a>
          </li>
          {% endfor %}
        </ul>
        {% endif %}
      </li>
      {% endfor %}
    </ul>
    {% endif %}
  {% endfor %}
{% endcapture %}
{{< / highlight >}}

![Jekyll: Widget: Archive with HEaderby Month][image-wi-archive-final]

This widget is specifically made for `post` kind,
because we have already set date for each `post` kind.

-- -- --

### What is Next?

Consider continue reading [ [Jekyll Bulma - Widget - Static Data][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/08/10/jekyll-bmd-widget-static-data/
[tutor-html-master-11]: {{< tutor-jekyll-bmd >}}/tutor-11/

[tutor-vw-recent-posts]:{{< tutor-jekyll-bmd >}}/tutor-11/_includes/widget/recent-posts.html
[tutor-vw-arch-simple]: {{< tutor-jekyll-bmd >}}/tutor-11/_includes/widget/archive-simple.html
[tutor-vw-arch-year]:   {{< tutor-jekyll-bmd >}}/tutor-11/_includes/widget/archive-year.html
[tutor-vw-arch-month]:  {{< tutor-jekyll-bmd >}}/tutor-11/_includes/widget/archive-month.html
[tutor-vw-archive]:     {{< tutor-jekyll-bmd >}}/tutor-11/_includes/widget/archive.html
[tutor-vw-categories]:  {{< tutor-jekyll-bmd >}}/tutor-11/_includes/widget/categories.html
[tutor-vw-tags]:        {{< tutor-jekyll-bmd >}}/tutor-11/_includes/widget/tags.html

[image-wi-recent-posts]:{{< assets-ssg >}}/2020/08/bulma/11-widget-recent-posts.png
[image-wi-tags]:        {{< assets-ssg >}}/2020/08/bulma/11-widget-tags.png
[image-wi-categories]:  {{< assets-ssg >}}/2020/08/bulma/11-widget-categories.png
[image-wi-archive-simple]:  {{< assets-ssg >}}/2020/08/bulma/11-widget-archive-simple.png
[image-wi-archive-year]:    {{< assets-ssg >}}/2020/08/bulma/11-widget-archive-year.png
[image-wi-archive-month]:   {{< assets-ssg >}}/2020/08/bulma/11-widget-archive-month.png
[image-wi-archive-final]:   {{< assets-ssg >}}/2020/08/bulma/11-widget-archive-with-header.png
