---
type   : post
title  : "Jekyll Bulma - Blog Post"
date   : 2020-08-14T09:17:35+07:00
slug   : jekyll-bmd-blog-post
categories: [ssg]
tags      : [jekyll, bulma]
keywords  : [post header, post footer, post navigation, date time, timeago, table of content, toc]
author : epsi
opengraph:
  image: assets-ssg/2020/08/bulma/11-blog-post-crop.png

toc    : "toc-2020-06-jekyll-step"

excerpt:
  Building Jekyll Site Step by step, using Bulma MD as stylesheet.
  More about blog post content, Header, Footer, and Navigation.
---

### Preface

> Goal: More about blog post content: Header, Footer, and Navigation.

#### Source Code

This article use [tutor-11][tutor-html-master-11] theme.
We will create it step by step.

-- -- --

### 1: Prepare

This preparation is required.

#### Preview: General

Consider redesign the looks of blog post.
This is what we want to achieve in this tutorial.

![Jekyll Content: Preview All Artefacts][image-blog-post-crop]

#### Layout: Liquid: Columns Double

We need to redesign `_layouts/columns-double.html` partial.
This layout is also equipped with a partial set in `blog_footer`.

* [gitlab.com/.../_layouts/columns-double.html][tutor-vl-double]

{{< highlight jinja >}}
---
layout: default

# default property
color_main  : blue
color_aside : teal

# layout chuncks
blog_header     : page/blog-header.html
aside_content   : page/aside-content.html
---

{% assign color_main  = page.color_main  | default: layout.color_main %}
{% assign color_aside = page.color_aside | default: layout.color_aside %}

  <main role="main"
        id="main_toggler"
        class="column is-two-thirds">

    <section class="main-wrapper {{ color_main }}">
      <div class="blog white z-depth-3 hoverable">

        <section class="blog-header {{ color_main }} lighten-5">
          {% include {{ layout.blog_header }} %}
        </section>

        <article class="blog-body" itemprop="articleBody">
          {{ content }}
        </article>

        {% if layout.blog_footer %}
        <section class="blog-footer {{ color_main }} lighten-5">
          {% include {{ layout.blog_footer }} %}
        </section>
        {% endif %}

      </div>
    </section>

  </main>

  <aside id="aside_toggler"
         class="column is-one-thirds">
    {% include {{ layout.aside_content }} %}
  </aside>
{{< / highlight >}}

Talking about schema, you can read this nice site:

* [schema.org/](http://schema.org/)

#### Layout: Liquid: Post

We need to manage the content in `_layouts/post.html` partial.

* [gitlab.com/.../_layouts/post.html][tutor-vl-post]

{{< highlight jinja >}}
---
layout: columns-double

# override color
color_main  : blue
color_aside : teal

# layout chuncks
blog_header     : post/blog-header.html
blog_footer     : post/blog-footer.html
aside_content   : post/aside-content.html
---

{{ content }}
{% include post/navigation.html %}
{{< / highlight >}}

![Jekyll Content: ViM Post Layout][image-vim-layout-post]

#### Partials: New Artefacts

The code above require three new partials.

Create three empty partial artefacts,

* `_includes/post/blog-header.html`,

* `_includes/post/blog-footer.html`,

* `_includes/post/navigation.html`.

And one more artefact that is required in `post/header.html`.

* `_includes/post/time-elapsed.html`

#### SASS: Main

And add relevant stylesheet

* [gitlab.com/.../_sass/css/main.scss][tutor-sass-main]

{{< highlight sass >}}
// Bulma Related
@import "bulma/initial-variables"
@import "vendors/bulma/utilities/initial-variables"
@import "vendors/bulma/utilities/functions"
@import "vendors/bulma/utilities/derived-variables"
@import "vendors/bulma/utilities/mixins"

// Materialize Related
@import "materialize/color-variables"

// Tailor Made
@import "main/layout-page"
@import "main/layout-content"
@import "main/decoration"
@import "main/pagination"
@import "main/list"

@import "post/content"
@import "post/navigation"
{{< / highlight >}}

We do not really need adjustment in Bulma.
All seems good.

-- -- --

### 2: Header

Consider begin with header.

#### Preview

![Jekyll Content: Blog Post Header][image-blog-post-header]

#### Partial: Header: Minimum

The minimum header is simply showing title.

{{< highlight jinja >}}
  <div class="main_title"> 
    <h1 class="title is-4 blog-post-title"
        itemprop="name headline">
      <a href="{{ site.url | append:site.baseurl | append:page.url }}">
        {{ page.title }}</a>
    </h1>
  </div>
{{< / highlight >}}

#### Partial: Header: Meta

Consider add **meta** data in post header.

* Author

{{< highlight jinja >}}
    {% if page.author %}
      <span class="meta_author tag is-small is-dark
                 indigo z-depth-1 hoverable">
        <span class="fa fa-user"></span>
        &nbsp;
        <span itemprop="author"
              itemscope itemtype="http://schema.org/Person">
        <span itemprop="name">{{ page.author }}</span></span>            
      </span>
    {% endif %}  
{{< / highlight >}}

* Time Elapsed (include partial)

{{< highlight jinja >}}
      <span class="meta-time tag is-small is-dark
                   green z-depth-1 hoverable">
      {% include post/time-elapsed.html %}
      </span>
{{< / highlight >}}

* Tags

{{< highlight jinja >}}
      {% for tag in page.tags %}
        <a href="{{ site.url | append:site.baseurl }}/pages/tag#{{ tag }}">
          <span class="tag is-dark is-small teal z-depth-1 hoverable">
            <span class="fa fa-tag"></span>&nbsp;{{ tag }}</span></a>
      {% endfor %}
{{< / highlight >}}

* Categories

{{< highlight jinja >}}
      {% for cat in page.categories %}
        <a href="{{ site.url | append:site.baseurl }}/pages/category#{{ cat }}">
          <span class="tag is-light is-small teal z-depth-1 hoverable">
            <span class="fa fa-folder"></span>&nbsp;{{ cat }}</span></a>
      {% endfor %}
{{< / highlight >}}

I use bulma `tag`,
with the eye candy icon from `FontAwesome`.

#### Partial: Header: Complete Code

Here below is my actual code.
I use `float-left` and `float-right` to arrange the looks.

* [gitlab.com/.../_includes/post/blog-header.html][tutor-vi-po-header]

{{< highlight html >}}

  <div class="main_title"> 
    <h1 class="title is-4 blog-post-title"
        itemprop="name headline">
      <a href="{{ site.url | append:site.baseurl | append:page.url }}">
        {{ page.title }}</a>
    </h1>
  </div>

  <div class="field p-t-5">
    <div class="is-pulled-left">
      {% if page.author %}
      <span class="meta_author tag is-small is-dark
                 indigo z-depth-1 hoverable">
        <span class="fa fa-user"></span>
        &nbsp;
        <span itemprop="author"
              itemscope itemtype="http://schema.org/Person">
        <span itemprop="name">{{ page.author }}</span></span>            
      </span>
      &nbsp;
      {% endif %}  

      <span class="meta-time tag is-small is-dark
                   green z-depth-1 hoverable">
      {% include post/time-elapsed.html %}
      </span>
      &nbsp;
    </div>

    <div class="is-pulled-right">
      {% for cat in page.categories %}
        <a href="{{ site.url | append:site.baseurl }}/pages/category#{{ cat }}">
          <span class="tag is-light is-small teal z-depth-1 hoverable">
            <span class="fa fa-folder"></span>&nbsp;{{ cat }}</span></a>
      {% endfor %}
      &nbsp;

      {% for tag in page.tags %}
        <a href="{{ site.url | append:site.baseurl }}/pages/tag#{{ tag }}">
          <span class="tag is-dark is-small teal z-depth-1 hoverable">
            <span class="fa fa-tag"></span>&nbsp;{{ tag }}</span></a>
      {% endfor %}
      &nbsp;
    </div>
  </div>

  <div class="is-clearfix p-b-5"></div>
{{< / highlight >}}

-- -- --

### 3: Elapsed Time

As it has been mentioned above, we need special partial for this.

#### Issue

As a static generator,
`jekyll` build the content whenever there are any changes.
If there are no changes, the generated time remain static.
It means we cannot tell relative time in string such as **three days ago**,
because after a week without changes, the time remain **three days ago**,
not changing into **ten days ago**.

The solution is using javascript.
You can download the script from here

* [timeago.org/](https://timeago.org/)

That way the time ago is updated dynamically as time passes,
as opposed to having to rebuild `jekyll` every day.

Do not forget to put the script in static directory.

* [gitlab.com/.../assets/js/timeago.min.js][tutor-assets-timeago]

#### Partial: Time Elapsed

* [gitlab.com/.../_includes/post/time-elapsed.html][tutor-vi-po-elapsed]

{{< highlight javascript >}}
    <time datetime="{{ page.date | date_to_xmlschema }}" 
          itemprop="datePublished">
    <span class="timeago"
          datetime="{{ page.date | date: "%Y-%m-%d %T" }}">
    </span></time>
    &nbsp; 

    <script src="{{ site.baseurl }}/assets/js/timeago.min.js"></script>
    <script type="text/javascript">
      var timeagoInstance = timeago();
      var nodes = document.querySelectorAll('.timeago');
      timeagoInstance.render(nodes, 'en_US');
      timeago.cancel();
      timeago.cancel(nodes[0]);
    </script>
{{< / highlight >}}

-- -- --

### 4: Footer

We already have `header`.
Why do not we continue with `footer`?

#### Preview

![Jekyll Content: Blog Post Footer][image-blog-post-footer]

#### Partial: Footer

* [gitlab.com/.../_includes/post/blog-footer.html][tutor-vi-po-footer]

{{< highlight html >}}
  <footer class="columns m-5">

    <div class="column is-narrow has-text-centered">
      <img src="/assets/images/license/cc-by-sa.png" 
           class="bio-photo" 
           height="31"
           width="88"
           alt="CC BY-SA 4.0"></a>
    </div>

    <div class="column has-text-left">
      This article is licensed under:&nbsp;
      <a href="https://creativecommons.org/licenses/by-sa/4.0/deed" 
         class="text-dark">
        <b>Attribution-ShareAlike</b>
        4.0 International (CC BY-SA 4.0)</a>
    </div>

  </footer>
{{< / highlight >}}

#### Assets: License

I have collect some image related with **license**,
so that you can use license easily.

* [gitlab.com/.../assets/images/license][tutor-assets-license]

-- -- --

### 5: Post Navigation

Each post also need simple navigation.

#### Preview

![Jekyll Content: Blog Post Navigation][image-blog-post-navi]

#### Partial: Navigation

By default, `jekyll` have built in function,
to handle post navigation, using:

1. `page.previous`, and

2. `page.next`.

* [gitlab.com/.../_includes/post/navigation.html][tutor-vi-po-navi]

{{< highlight jinja >}}
<nav class="pagination is-centered" 
     role="navigation" aria-label="pagination">

    <!-- Previous Page. -->
    {% if page.previous %}
      <a class="pagination-previous post-previous"
         href="{{ page.previous.url | prepend: site.baseurl }}"
         title="{{ page.previous.title }}">
        <span class="fas fa-chevron-left"></span>&nbsp;&nbsp;</a>
    {% endif %}

    {% if page.next %}
      <a class="pagination-next post-next"
         href="{{ page.next.url | prepend: site.baseurl }}" 
         title="{{ page.next.title }}">&nbsp;&nbsp;
        <span class="fas fa-chevron-right"></span></a>
    {% endif %}
</nav>
{{< / highlight >}}

#### SASS: Post Navigation

Code above require two more classes

1. `post-previous`

2. `post-next`

* [gitlab.com/.../_sass/css/post/_navigation.sass][tutor-sass-post-navi]

{{< highlight scss >}}
// -- -- -- -- --
// _post-navigation.sass

a.post-previous:after
  content: " Previous"

a.post-next:before
  content: "Next "

a.post-previous:hover,
a.post-next:hover
  background-color: map-get($yellow, 'lighten-2')
  color: #000
{{< / highlight >}}

__.__

-- -- --

### 6: Before Content: Table of Content

Sometimes we need to show recurring content, 
such as table of content in article series.
For article series, we only need one TOC.
And we do not want to repeat ourself,
writing it over and over again.

To solve this case, we need help form frontmatter.

#### Layout: Liquid: Post

We are going to insert TOC, before the content,
by including `toc` something partial, provided from frontmatter.

We need to manage the content in `_layouts/post.html` partial.

* [gitlab.com/.../_layouts/post.html][tutor-vl-post]

{{< highlight jinja >}}
---
...
---

{% if page.toc %}
  {% include {{ page.toc }} %}
{% endif %}

{{ content }}

{% include post/navigation.html %}
{{< / highlight >}}

#### Page Content: Example Frontmatter

Now here it is, the `TOC` in frontmatter,
as shown in this example below.

* [gitlab.com/.../_post/lyrics/2019-05-15-brooke-annibale-by-your-side.md][tutor-vc-po-annibale]

{{< highlight yaml >}}
---
layout      : post
title       : Brooke Annibale - By Your Side
...

related_link_ids :
  - 19052535  # Yours and Mine
---

...
{{< / highlight >}}

#### Layout: Liquid: TOC

Now you can have your TOC here.

* [gitlab.com/.../_includes/toc/2019-15-brooke-annibale.html][tutor-vi-toc-annibale]

{{< highlight html >}}

  <div class="white hoverable p-t-5 p-b-5">
    <div class="widget-header blue lighten-4">

      <strong>Table of Content</strong>
      <span class="fa fa-archive is-pulled-right"></span>

    </div>
    <div class="widget-body blue lighten-5">

      <ul class="widget-list">
        <li><a href="{{ "/lyric/2019/05/25/brooke-annibale-yours-and-mine.html"
                    | prepend: site.baseurl | prepend: site.url }}"
              >Brooke Annibale - Yours and Mine</a></li>
        <li><a href="{{ "/lyric/2019/05/15/brooke-annibale-by-your-side.html"
                    | prepend: site.baseurl | prepend: site.url }}"
              >Brooke Annibale - By Your Side</a></li>
      </ul>

    </div>
  </div>
{{< / highlight >}}

Notice the `.html` file extension. We are still using `liquid`.
This HTML document is, actually a `liquid` document.

#### Render: Browser

Now you can see the result in the browser.

![Jekyll Raw HTML: Table of Content][image-content-toc]

-- -- --

### 7: Toggler

Sometimes the audience of our blog need a wider view.
We can arrange it with javascript toggler.
You can read the detail here:

* [Bulma MD - Javascript Toggler][javascript-toggler]

#### Layout: Liquid: Post

Our complete layout would be

* [gitlab.com/.../_layouts/post.html][tutor-vl-post]

{{< highlight jinja >}}
---
layout: columns-double

# override color
color_main  : blue
color_aside : teal

# layout chuncks
blog_header     : post/blog-header.html
blog_footer     : post/blog-footer.html
aside_content   : post/aside-content.html
---

{% include post/toggler.html %}

{% if page.toc %}
  {% include {{ page.toc }} %}
{% endif %}

{{ content }}

{% include post/navigation.html %}
{{< / highlight >}}

#### Render: Browser

The toggle will be shown in desktop screen.
And hidden in mobile screen.

-- -- --

### 8: Conclusion

It is enough for now.
There are many part that can be enhanced, in about content area.

After all, it is about imagination.

-- -- --

### What is Next?

Consider continue reading [ [Jekyll Bulma - Content][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/08/15/jekyll-bmd-content/
[tutor-html-master-11]: {{< tutor-jekyll-bmd >}}/tutor-11/
[javascript-toggler]:   {{< baseurl >}}frontend/2019/06/15/bulma-md-javascript-toggler/

[tutor-sass-main]:      {{< tutor-jekyll-bmd >}}/tutor-11/_sass/css/main.scss
[tutor-sass-post-navi]: {{< tutor-jekyll-bmd >}}/tutor-11/_sass/css/post/_navigation.scss
[tutor-vl-double]:      {{< tutor-jekyll-bmd >}}/tutor-11/_layouts/columns-double.html
[tutor-vl-post]:        {{< tutor-jekyll-bmd >}}/tutor-11/_layouts/post.html
[tutor-assets-timeago]: {{< tutor-jekyll-bmd >}}/tutor-11/assets/js/timeago.min.js
[tutor-assets-license]: {{< tutor-jekyll-bmd >}}/tutor-11/assets/images/license/
[tutor-vi-po-header]:   {{< tutor-jekyll-bmd >}}/tutor-11/_includes/post/blog-header.html
[tutor-vi-po-footer]:   {{< tutor-jekyll-bmd >}}/tutor-11/_includes/post/blog-footer.html
[tutor-vi-po-navi]:     {{< tutor-jekyll-bmd >}}/tutor-11/_includes/post/navigation.html
[tutor-vi-po-elapsed]:  {{< tutor-jekyll-bmd >}}/tutor-11/_includes/post/time-elapsed.html
[tutor-vi-toc-annibale]:{{< tutor-jekyll-bmd >}}/tutor-11/_includes/toc/2019-15-brooke-annibale.html
[tutor-vc-po-annibale]: {{< tutor-jekyll-bmd >}}/tutor-11/_post/lyrics/2019-05-15-brooke-annibale-by-your-side.md

[image-blog-post-crop]:     {{< assets-ssg >}}/2020/08/bulma/11-blog-post-crop.png
[image-vim-layout-post]:    {{< assets-ssg >}}/2020/07/bootstrap/11-vim-layout-post.png
[image-blog-post-header]:   {{< assets-ssg >}}/2020/08/bulma/11-blog-post-header.png
[image-blog-post-footer]:   {{< assets-ssg >}}/2020/08/bulma/11-blog-post-footer.png
[image-blog-post-navi]:     {{< assets-ssg >}}/2020/08/bulma/11-blog-post-navigation.png
[image-content-toc]:        {{< assets-ssg >}}/2020/08/bulma/11-content-toc.png
