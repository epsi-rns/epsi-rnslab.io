---
type   : post
title  : "Jekyll Bootstrap OC - Repository"
date   : 2020-07-01T09:17:35+07:00
slug   : jekyll-bsoc-repository
categories: [ssg]
tags      : [jekyll, bootstrap]
keywords  : [static site, custom theme, summary, overview]
author : epsi
opengraph:
  image: assets-ssg/2020/07/jekyll-bootstrap-oc-preview.png

toc    : "toc-2020-06-jekyll-step"

excerpt:
  Building Jekyll Site Step by step,
  using Bootstrap OC as stylesheet.
---

### Preface

> Goal: Overview of Jekyll Repository Step By Step.

An example of pure Jekyll site with Bootstrap OC as stylesheet
for personal learning purpose.

> Build: using Bootstrap OC as stylesheet.

This repository is based on Jekyll Plain without any stylesheet.
We put on Bootstrap stylesheet as clothes using different repository,
and face some adjustment in almost all `_includes` views.

#### Jekyll Version

Since this repository is still using Jekyll 3.8, instead of Jekyll 4.0,
you might need to run `bundle install`, depend on the situation.

{{< highlight bash >}}
$ bundle install
{{< / highlight >}}

-- -- --

## Chapter Step by Step

### Tutor 08

> Add Bootstrap CSS Framework

* Add Bootstrap CSS

* Standard Header (jquery or vue or native) and Footer

* Enhance All Layouts with Bootstrap CSS

* Apply Bootstrap Two Column Responsive Layout for Most Layout

![Jekyll Bootstrap: Tutor 08][jekyll-bootstrap-preview-08]

-- -- --

### Tutor 09

> Combine with Custom SASS Material Design

* Add (a bunch of) Custom SASS (Custom Design)

* Nice Header and Footer

* Enhance All Two Column Responsive Layout with Material Design

* Nice Tag Badge in Tags Page

![Jekyll Bootstrap: Tutor 09][jekyll-bootstrap-preview-09]

-- -- --

### Tutor 10

> Loop with Liquid

* More Content: Quotes. Need this content for demo

* Simplified All Layout Using Liquid Template Inheritance

* Article Index: By Year, List Tree (By Year and Month)

![Jekyll Bootstrap: Tutor 10][jekyll-bootstrap-preview-10]

-- -- --

### Tutor 11

> Features

* Widget: Friends, Archives Tree, Categories, Tags, Recent Post, Related Post

* Refactoring Template using Capture: Widget

* Pagination (v1): Adjacent, Indicator, Responsive

* Pagination (v2): Adjacent, Indicator, Responsive

* Post: Header, Footer, Navigation

![Jekyll Bootstrap: Tutor 11][jekyll-bootstrap-preview-11]

-- -- --

### Tutor 12

> Finishing

* Layout: Service (dummy)

* Post: Markdown Content (test case)

* Post: Table of Content (dynamic include)

* Post: Responsive Images

* Post: Syntax Highlight (sass)

* Post: Shortcodes (include with parameter)

* Official Plugin: Feed

* Meta: HTML, SEO, Opengraph, Twitter

* Multi Column Responsive List: Categories, Tags, and Archives

![Jekyll Bootstrap: Tutor 12][jekyll-bootstrap-preview-12]

-- -- --

### Tutor 13

> Jekyll Plugin with Ruby

* Filter: Year Text, Term Array, Pagination Links, Link Offset Flag, Keywords

* Generator: Tag Names

![Jekyll Bootstrap: Tutor 13][jekyll-bootstrap-preview-13]

-- -- --

### Tutor 14

> Jekyll Theme with Assets and SASS

* Bundling Gem

* Using Theme

![Jekyll Bootstrap: Tutor 14][jekyll-bootstrap-preview-14]

-- -- --

### What is Next?

Consider continue reading [ [Jekyll Bootstrap - CSS Intro][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/07/02/jekyll-bsoc-css-intro/

[jekyll-bootstrap-preview]:     https://gitlab.com/epsi-rns/tutor-jekyll-bootstrap-oc/raw/master/jekyll-bootstrap-oc-preview.png
[jekyll-bootstrap-preview-08]:  https://gitlab.com/epsi-rns/tutor-jekyll-bootstrap-oc/raw/master/tutor-08/jekyll-bootstrap-oc-preview.png
[jekyll-bootstrap-preview-09]:  https://gitlab.com/epsi-rns/tutor-jekyll-bootstrap-oc/raw/master/tutor-09/jekyll-bootstrap-oc-preview.png
[jekyll-bootstrap-preview-10]:  https://gitlab.com/epsi-rns/tutor-jekyll-bootstrap-oc/raw/master/tutor-10/jekyll-bootstrap-oc-preview.png
[jekyll-bootstrap-preview-11]:  https://gitlab.com/epsi-rns/tutor-jekyll-bootstrap-oc/raw/master/tutor-11/jekyll-bootstrap-oc-preview.png
[jekyll-bootstrap-preview-12]:  https://gitlab.com/epsi-rns/tutor-jekyll-bootstrap-oc/raw/master/tutor-12/jekyll-bootstrap-oc-preview.png
[jekyll-bootstrap-preview-13]:  https://gitlab.com/epsi-rns/tutor-jekyll-bootstrap-oc/raw/master/tutor-13/jekyll-bootstrap-oc-preview.png
[jekyll-bootstrap-preview-14]:  https://gitlab.com/epsi-rns/tutor-jekyll-bootstrap-oc/raw/master/tutor-14/jekyll-bootstrap-oc-preview.png
