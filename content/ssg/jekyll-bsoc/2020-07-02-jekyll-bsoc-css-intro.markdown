---
type   : post
title  : "Jekyll Bootstrap - CSS Intro"
date   : 2020-07-02T09:17:35+07:00
slug   : jekyll-bsoc-css-intro
categories: [ssg]
tags      : [jekyll, bootstrap]
keywords  : [assets, liquid, layout, refactoring template]
author : epsi
opengraph:
  image: assets-ssg/2020/07/bootstrap/08-vim-default.png

toc    : "toc-2020-06-jekyll-step"

excerpt:
  Building Jekyll Site Step by step, using Bootstrap OC as stylesheet.
  Revamp the looks, of current Jekyll layout.
---

### Preface

> Goal: Revamp the looks,
> of current Jekyll layout, with Bootstrap Stylesheet.

#### Previous Tutorial

This article is direct chain of previous tutorial.

* [Jekyll Plain - Theme Bundling][local-whats-theme]

#### Source Code

This article use [tutor-08][tutor-html-master-08] theme.

#### Not a CSS Tutorial

This article is not a CSS tutorial,
but rather than applying simple CSS theme,
into `Jekyll` layout.
This CSS theme is a based on a SASS theme,
that we are going to discuss later.

#### Layout Preview for Tutor 08

![Liquid: Layout Preview for Tutor 08][template-inheritance-08]

[template-inheritance-08]:  {{< assets-ssg >}}/2020/06/08-template-inheritance.png

-- -- --

### 1: About Bootstrap Stylesheet

> Use Stylesheet in Jekyll

In previous articles,
we have already refactor pages into a few layouts.
All done without stylesheet,
so you have understanding of Jekyll layout, 
without the burden of learning stylesheet.

#### The Choice

Now it is a good time to give this site a good looks.
There are many options for this such as:
`Bootstrap`, `Materialize CSS`, `Semantic UI`, `Bulma`,
or even better custom tailor made without CSS Frameworks,
or `Tailwind CSS`.

#### Prerequiste

> Use Bootstrap Stylesheet in Jekyll

My choice comes to Bootstrap and Bulma,
because I have already make a few articles about Bootstrap and Bulma,
with a lot of details explanation:

* [Bootstrap - Overview][local-bootstrap]

This article rely on Bootstrap stylesheet on the article above.
You should give a fast reading of above Bootstrap article first,
before continue reading this article.

#### Responsive Design

Our base layout is based on responsive design.
This below is general preview of,
what responsive page that we want to achieve.

![Jekyll: General Preview of Responsive Page Design][image-png-layout-page]

Different website might have different preview.
Source image is available in inkscape SVG format,
so you can modify, and make your own preview quickly.

* [Responsive Page: Image Source][image-svg-layout-page]

This responsive design has already discussed in Bulma article series.

#### Related Article: CSS

I wrote about Bulma Navigation Bar that used in this article.
Step by step article, about building Navigation Bar.
You can read here:

* [Bootstrap - Navbar - Class][local-bootstrap-navbar]

* [Bootstrap - Sass - Responsive][local-bootstrap-layout]

#### Reference

* [getbootstrap.com](https://getbootstrap.com/)

-- -- --

### 2: Prepare: Assets

#### Directory Tree: Assets

There is no need to change any configuration.
Any directory without `_` prefix would be rendered.
To make your assets tidy, consider put them all in one directory,
such as `./assets`.

{{< highlight bash >}}
$ tree assets
assets
├── css
│   ├── bootstrap.css
│   └── main.css
├── images
│   ├── light-grey-terrazzo.png
│   └── logo-gear.png
└── js
    ├── bootstrap-navbar-native.js
    └── feather.min.js

3 directories, 6 files
{{< / highlight >}}

Actually, I made three different javascript choices.
`jquery`, `vue`, and `plain native vanilla`.
For simplicity reason, and also neutrality reason,
for the rest of the chapter, I use `plain` javascript.

![Jekyll: Tree: Assets][image-ss-08-tree-assets]

#### Stylesheet: Main

The `main.css` assets is the only custom stylesheet.

* [gitlab.com/.../assets/css/main.css][tutor-css-main]

{{< highlight css >}}
.layout-base {
  padding-top: 5rem;
  padding-bottom: 1rem;
}

.maxwidth {
  margin-right: 0;
  margin-left: 0;
}

@media only screen and (min-width: 1200px) {
  /* Widescreen: */
  .maxwidth {
    max-width: 1200px;
    margin-right: auto;
    margin-left: auto;
  }
}
.layout-base main {
  margin-bottom: 20px;
}

@media only screen and (min-width: 768px) {
  .layout-base main {
    margin-bottom: 0px;
  }
}
.navbar-brand img {
  width: 32px;
  height: 32px;
  margin-top: -10px;
  margin-bottom: -10px;
}

body {
  background-image: url("../images/light-grey-terrazzo.png");
}

/* Sticky footer styles */
html {
  position: relative;
  min-height: 100%;
}

body {
  margin-bottom: 60px;
  /* Margin bottom by footer height */
}

.footer {
  position: absolute;
  bottom: 0;
  width: 100%;
  height: 60px;
  /* Set the fixed height of the footer here */
  line-height: 60px;
  /* Vertically center the text there */
}

.feather {
  width: 20px;
  height: 20px;
  stroke: currentColor;
  stroke-width: 2;
  stroke-linecap: round;
  stroke-linejoin: round;
  fill: none;
  vertical-align: text-top;
}
{{< / highlight >}}

We have already discussed the stylesheet in Bootstrap article __.__

* [Bootstrap - Overview][local-bootstrap]

-- -- --

### 3: Layout: Refactoring Base

> Put on clothes on HTML body.

We should start over again from the very fundamental layout.

#### Layout: Liquid Default

* [gitlab.com/.../_layouts/default.html][tutor-vl-default]

{{< highlight jinja >}}
<!DOCTYPE html>
<html>

<head>
  {% include site/head.html %}
</head>

<body>
  <!-- header -->
  {% include site/header.html %}

  <!-- main -->
  <div class="row layout-base maxwidth">
    {{ content }}
  </div>

  <!-- footer -->
  {% include site/footer.html %}
  {% include site/scripts.html %}
</body>

</html>
{{< / highlight >}}

No we have four `include`s:

* `head`,

* `header`,

* `footer`,

* `script` (new).

For flexibility reason,
I move two `element`s to child layout:

* `main`, and

* `aside`.

![Jekyll: Base Layout][image-ss-08-vim-default]

#### Partial Liquid: Head

* [gitlab.com/.../_includes/site/head.html][tutor-vi-head]

There is a few additional changes here.
Now the header contain stylesheets, meta tag, and icons.

{{< highlight html >}}

  <meta charset="utf-8">
  <meta name="viewport"
        content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>{{ (page.title | default: site.title) | escape }}</title>

  <link rel="stylesheet" type="text/css"
        href="{{ site.baseurl }}/assets/css/bootstrap.css">
  <link rel="stylesheet" type="text/css"
        href="{{ site.baseurl }}/assets/css/main.css">

  <link rel="shortcut icon" type="image/x-icon"
        href="{{ site.baseurl }}/favicon.ico"/>

  <script src="{{ site.baseurl }}/assets/js/bootstrap-navbar-native.js"></script>
  <script src="{{ site.baseurl }}/assets/js/feather.min.js"></script>
{{< / highlight >}}

We will add meta SEO later.
Here we start from simple thing.

#### Partial Liquid: Header

It is a long header, so I crop the code.
You can get the complete code in the repository.

* [gitlab.com/.../_includes/site/header.html][tutor-vi-header]

{{< highlight html >}}
  <nav class="navbar navbar-expand-md navbar-dark maxwidth fixed-top bg-dark">

    <a class="navbar-brand"
       href="{{ site.baseurl | prepend:site.url }}">
       <img src="{{ site.baseurl }}/assets/images/logo-gear.png"
            alt="Home" />
    </a>

    <ul class="navbar-nav mr-auto">
      ...
    </ul>

    <button class="navbar-toggler" type="button" 
        ...>
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarCollapse">

      <ul class="navbar-nav mr-auto">
        ...
      </ul>

      <form class="form-inline mt-2 mt-md-0">
        ...
      </form>

    </div>
  </nav>
{{< / highlight >}}

![Jeykll: Navigation Bar: Header][image-vc-pa-header]

This is an example of header using `plain` .
You can switch to `jquery` or `vue` with example,
in `html-bootstrap` repository.

#### Partial Liquid: Scripts

Since we use feather icons, we should put this snippets at the bottom.

{{< highlight html >}}
  {% comment %}<!-- JavaScript at end of body. -->{% endcomment %}
  <script>
    feather.replace()
  </script>
{{< / highlight >}}

You can have the code here:

* [gitlab.com/.../_includes/site/scripts.html][tutor-vi-scripts]

#### Layout Liquid: Footer

* [gitlab.com/.../_includes/site/footer.html][tutor-vi-footer]

{{< highlight html >}}
  <!-- footer -->
  <footer class="footer">
    <div class="maxwidth bg-dark text-light text-center">
      &copy; {{ site.time | date: '%Y' }}.
    </div>
  </footer>
{{< / highlight >}}

-- -- --

### 4: Layout: Home

Consider use `home` layout` to begin,
and all other layout later.

#### Layout Liquid: Home

> This is a single column design.

* [gitlab.com/.../_layouts/home.html][tutor-vl-home]

`liquid`'s template inheritance in `jekyll`,
set in `frontmatter`.

{{< highlight jinja >}}
---
layout : default
---

  <main role="main" 
        class="col p-4 bg-warning">
    <header>
      <h2>{{ page.title | default: site.title }}</h2>
    </header>
    <article>
      {{ content }}
    </article>
    <div class="text-muted text-center">
      This is a home kind layout,
      to show landing page.
    </div>
  </main>
{{< / highlight >}}

#### Page Content: index

It is the same old story.

* [gitlab.com/.../index.md][tutor-vc-index]

{{< highlight html >}}
---
layout    : home
---

To have, to hold, to love,
cherish, honor, and protect?
  
To shield from terrors known and unknown?
To lie, to deceive?

To live a double life,
to fail to prevent her abduction,
erase her identity, 
force her into hiding,
take away all she has known.
{{< / highlight >}}

#### Render: Browser

Open in your favorite browser.
You should see, a simple homepage, by now.

* <http://localhost:4000/>

Well, I cut the content a little to make this screenshot below:

![Jeykll: Page Content: Home][image-vc-pa-home]

Notice that this is a single column Bootstrap page.
The other page is double columns,
and deserve a its own explanation.

-- -- --

### 5: Layout: Page, Post

Our Page Kind, and Post Kind is simply,
a double columns version of above layout.

#### Layout Liquid: Page

* [gitlab.com/.../_layouts/page.html][tutor-vl-page]

{{< highlight jinja >}}
---
layout: default
aside_message : This is a page kind layout.
---

  <main role="main" 
        class="col-md-8">
    <section class="p-4 bg-warning h-100 rounded">
      <header>
        <h2>{{ page.title | default: site.title }}</h2>
      </header>
       <article>
        {{ content }}
      </article>
    </section>
  </main>

  <aside class="col-md-4">
      <section class="p-4 bg-dark text-light h-100 rounded">
      {{ layout.aside_message }}
    </section>
  </aside>
{{< / highlight >}}

![Jekyll: Extending Default: Page][image-parent-page]

We have two block `element`s here:

* `main`,

* `aside`.

#### Page Content: pages/about

No difference with previous chapter.

* [gitlab.com/.../pages/about.md][tutor-vc-pa-about]

{{< highlight markdown >}}
---
layout    : page
title     : Rescue Mission
---

This was not a rescue mission!

Let me put to you like this.
If the secretary wanted me out of there,
then things are really bad out here
{{< / highlight >}}

We can say that this content wear `page` layout.

#### Render: Browser

Open in your favorite browser.
You should see, a black and white about page, by now.

![Jekyll: Page Content: pages/about][image-vc-about]

#### Layout Liquid: Post

* [gitlab.com/.../_layouts/post.html][tutor-vl-post]

{{< highlight jinja >}}
---
layout: default
aside_message : This is a post kind layout.
---

  <main role="main" 
        class="col-md-8">
    <section class="p-4 bg-warning h-100 rounded">
      <header>
        <h2>{{ page.title | default: site.title }}</h2>
        <p><strong>{{ page.date | date: "%B %d, %Y" }}</strong></p>
      </header>
      <article>
        {{ content }}
      </article>
    </section>
  </main>

  <aside class="col-md-4">
      <section class="p-4 bg-dark text-light h-100 rounded">
      {{ layout.aside_message }}
    </section>
  </aside>
{{< / highlight >}}

We also have two block `element`s here:

* `main`,

* `aside`.

#### Post Content: winter.md

No difference with previous chapter.

* [gitlab.com/.../_posts/2016-01-01-winter.md][tutor-vc-po-winter]

{{< highlight markdown >}}
---
layout  : post
title   : Surviving White Winter
date    : 2016-01-01 08:08:15 +0700
tags      : ['sleepy', 'husky']
---

It was a frozen winter in cold war era.
We were two lazy men, a sleepy boy, two long haired women,
a husky with attitude, and two shotguns.
After three weeks, we finally configure javascript.

But we lost our beloved husky before we finally made it.
Now, every january, we remember our husky,
that helped all of us to survive.
{{< / highlight >}}

#### Render: Browser

Open in your favorite browser.
You should see, a black and white post, by now.

![Jekyll: Post Content: _posts/winter][image-vc-winter]

-- -- --

### What is Next?

Consider continue reading [ [Jekyll Bootstrap - CSS Layout][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/07/03/jekyll-bsoc-css-layout/
[local-whats-theme]:    {{< baseurl >}}ssg/2020/06/24/jekyll-plain-theme-bundling/

[local-bootstrap]:          {{< baseurl >}}frontend/2020/03/01/bootstrap-oc-overview/
[local-bootstrap-navbar]:   {{< baseurl >}}frontend/2020/03/03/bootstrap-navbar-class/
[local-bootstrap-layout]:   {{< baseurl >}}frontend/2020/03/10/bootstrap-sass-responsive/

[tutor-html-master-08]: {{< tutor-jekyll-bsoc >}}/tutor-08/

[tutor-css-main]:       {{< tutor-jekyll-bsoc >}}/tutor-08/assets/css/main.css

[tutor-vl-default]:     {{< tutor-jekyll-bsoc >}}/tutor-08/_layouts/default.html
[tutor-vl-home]:        {{< tutor-jekyll-bsoc >}}/tutor-08/_layouts/home.html
[tutor-vl-page]:        {{< tutor-jekyll-bsoc >}}/tutor-08/_layouts/page.html
[tutor-vl-post]:        {{< tutor-jekyll-bsoc >}}/tutor-08/_layouts/post.html
[tutor-vi-head]:        {{< tutor-jekyll-bsoc >}}/tutor-08/_includes/site/head.html
[tutor-vi-header]:      {{< tutor-jekyll-bsoc >}}/tutor-08/_includes/site/header.html
[tutor-vi-footer]:      {{< tutor-jekyll-bsoc >}}/tutor-08/_includes/site/footer.html
[tutor-vi-scripts]:     {{< tutor-jekyll-bsoc >}}/tutor-08/_includes/site/scripts.html
[tutor-vc-index]:       {{< tutor-jekyll-bsoc >}}/tutor-08/index.md
[tutor-vc-pa-about]:    {{< tutor-jekyll-bsoc >}}/tutor-08/pages/about.md
[tutor-vc-po-winter]:   {{< tutor-jekyll-bsoc >}}/tutor-08/_posts/2016-01-01-winter.md

[image-png-layout-page]:    {{< baseurl >}}assets/posts/frontend/2019/12/layout-page.png
[image-svg-layout-page]:    {{< baseurl >}}assets/posts/frontend/2019/12/layout-page.svg

[image-ss-08-tree-assets]:  {{< assets-ssg >}}/2020/07/bootstrap/08-tree-assets.png
[image-ss-08-vim-default]:  {{< assets-ssg >}}/2020/07/bootstrap/08-vim-default.png
[image-vc-pa-home]:         {{< assets-ssg >}}/2020/07/bootstrap/08-pages-home.png
[image-vc-pa-header]:       {{< assets-ssg >}}/2020/07/bootstrap/08-pages-header.png
[image-parent-page]:    {{< assets-ssg >}}/2020/07/bootstrap/08-parent-page.png
[image-parent-post]:    {{< assets-ssg >}}/2020/07/bootstrap/08-parent-post.png
[image-vc-about]:       {{< assets-ssg >}}/2020/07/bootstrap/08-pages-about.png
[image-vc-winter]:      {{< assets-ssg >}}/2020/07/bootstrap/08-posts-winter.png
