---
type   : post
title  : "Jekyll Bootstrap - CSS Layout"
date   : 2020-07-03T09:17:35+07:00
slug   : jekyll-bsoc-css-layout
categories: [ssg]
tags      : [jekyll, bootstrap]
keywords  : [assets, liquid, layout, refactoring template]
author : epsi
opengraph:
  image: assets-ssg/2020/07/bootstrap/08-terms-tree.png

toc    : "toc-2020-06-jekyll-step"

excerpt:
  Building Jekyll Site Step by step, using Bootstrap OC as stylesheet.
  Using simple template inheritance with Layout in Jekyll Layout.
---

### Preface

> Goal: Using simple template inheritance with Liquid in Jekyll Layout.

This is going to be a long article,
because I pour all the code here intentionally,
so that we do not need to rewrite down,
all of the relationship all over again,
for the rest of the tutorial.

#### Source Code

This article use [tutor-08][tutor-html-master-08] theme.

#### Related Article: CSS

This article is not a CSS tutorial,
but rather than applying simple CSS theme,
into `jekyll` **layout**.

The CSS part of this article is,
already discussed in this SASS article below.
The only different is that I use plain CSS instead of SASS.

* [Bootstrap - SASS - Layout][local-bootstrap-layout]

-- -- --

### 6: Layout: Blog

#### Pattern

> Can you see the pattern?

We have very similar pages with the Bootstrap's double columns layout,
and we write down the same layout over and over again in each layout:
`post`, `page`, `archive by year`, `archive by month`,
`tags`, `tag-names`, `categories`
and who knows how this site will grow later.

Liquid is a powerful templating engine,
and capable to solve this situation.
We do not need to exhaustingly repeat ourselves.

Since `page` layout is already a double column layout,
we can utilize this layout as a parent for other layout.
This is exactly the same with previous pattern in the article.

* [Jekyll - Managing Code - Part Three][managing-code-03]

The `page` based on `layout`, and that layout call a partial `include`.
The Bootstrap part html formatting lies in partial `include`.

#### Page Content: Blog

* [gitlab.com/.../pages/index.html][tutor-pa-blog]

{{< highlight markdown >}}
---
layout    : blog
title     : Blog Posts
permalink : /pages/
---
{{< / highlight >}}

We can say that this content wear `blog` layout.

#### Layout Liquid: Blog

* [gitlab.com/.../_layouts/blog.html][tutor-vl-blog]

{{< highlight jinja >}}
---
layout: page
---

{% assign posts = site.posts %}
{% include index/blog-list.html %}
{{< / highlight >}}

This layout is a child of `page` layout.

#### Partial Liquid: Blog List

> This is the Bootstrap View Part.

We have one partial HTML view for this layout.

* [gitlab.com/.../_includes/index/blog-list.html][tutor-vi-blog-list]

{{< highlight html >}}
  <div>
  {% for post in posts %}
    <div class="archive pb-2">
    <div class="p-2">

      <div class="meta">
        <div class="float-left">
          <a class="text-dark"
             href="{{ post.url | prepend: site.baseurl }}"
            >{{ post.title }}
          </a>
        </div>
        <div class="float-right">
          {% for tag in post.tags %}
          <span class="badge badge-dark">
            <a class="text-light"
               href="{{ site.baseurl }}/tags/#{{ tag | slugify }}"
              >{{ tag }}&nbsp;<i data-feather="tag" class="feather-14"></i>
            </a></span>&nbsp;
          {% endfor %}
        </div>
      </div> 

      <div class="clearfix"></div>

    </div></div>
  {% endfor %}
  </div>
{{< / highlight >}}

![Jekyll Bootstrap: Blog List][image-blog-list]

-- -- --

### 7: Layout: Taxonomy

With the method above,
we can rewrite the all other layouts as well.

#### Partial Liquid: Terms

> This is the Bootstrap View Part.

We have two partial HTML views for this layout:
`terms-badge` for top summary, and `terms-tree` for details.

* [gitlab.com/.../_includes/index/terms-badge.html][tutor-vi-terms-badge]]

{{< highlight html >}}
  <div class="py-3">
  {% for item in (0..terms.size) %}{% unless forloop.last %}
    {% assign this_word = term_array[item] | strip_newlines %}
    <span>
      {{ $posts := where $value.Pages "Type" "post" }}
      {{ $postCount := len $posts -}}
      <a href="#{{ this_word | slugify }}">
        <span class="badge badge-dark">
          {{ this_word }} 
          <span class="badge badge-light">
          {{ terms[this_word].size }}
          </span>
        </span>
      </a>
    </span>
    &nbsp;
  {% endunless %}{% endfor %}
  </div>
{{< / highlight >}}

* [gitlab.com/.../_includes/index/terms-tree.html][tutor-vi-terms-tree]

{{< highlight html >}}
  <section class="py-1" id="archive">
  {% for item in (0..terms.size) %}{% unless forloop.last %}
    {% assign this_word = term_array[item] | strip_newlines %}
      <div id="{{ this_word | slugify }}" class ="anchor-target">
        <strong>{{ this_word }}</strong>
      </div>

      <div class="py-1">
      {% for post in terms[this_word] %}
        {% if post.title != null %}
        <div>
          <div class="has-text-right">
            <time class="float-right"
                  datetime="{{ post.date | date_to_xmlschema }}">
              {{ post.date | date: "%b %-d, %Y" }}
            </time></div>
          <div class="float-left">
          <a href="{{ post.url | prepend: site.baseurl }}">
            {{ post.title }}
          </a></div>
          <div class="clearfix"></div>
        </div>
        {% endif %}
      {% endfor %}
      </div>

  {% endunless %}{% endfor %}
  </section>
{{< / highlight >}}

![Jekyll Bootstrap: ViM Terms Tree][image-terms-tree]

As you can see there is almost nothing happened in each page content.
Most have been done within the
`_includes/index/terms-tree.html` partial,
using default Bootstrap stylesheet feature.
It means, whenever any changes happened,
you only need to rewrite this partial view.

> Template inheritance is very useful in this case.

#### Page Content: Categories

* [gitlab.com/.../pages/categories.html][tutor-pa-categories]

{{< highlight markdown >}}
---
layout    : list-category
title     : All Categories
permalink : /categories/
---
{{< / highlight >}}

#### Layout Liquid: Category List

* [gitlab.com/.../_layouts/list-category.html][tutor-vl-categories]

{{< highlight jinja >}}
---
layout: page
---

{% assign terms = site.categories %}
{% include index/terms-array.html %}
{% include index/terms-badge.html %}
{% include index/terms-tree.html %}
{{< / highlight >}}

As usual, this layout is also a child of `page` layout.

![Jekyll Bootstrap: Categories with Tree Details][image-cats-list]

#### Page Content: Tags

* [gitlab.com/.../pages/tags.html][tutor-pa-tags]

{{< highlight markdown >}}
---
layout    : blog
title     : Blog Posts
permalink : /pages/
---
{{< / highlight >}}

#### Layout Liquid: Tag List

* [gitlab.com/.../_layouts/list-tag.html][tutor-vl-tags]

{{< highlight jinja >}}
---
layout: page
---

{% assign posts = site.posts %}
{% include index/blog-list.html %}
{{< / highlight >}}

As usual, this layout is also a child of `page` layout.

![Jekyll Bootstrap: Tags with Tree Details][image-tags-list]

-- -- --

### 8: Layout: Grouped Archives

The same relationship applied to archive.
The `page` based on `layout`, and that layout call a partial `include`.

Making a grouping archive by month and year is not hard,
it just need carefully carved with patient.
We are going to build archive `by month`,
but since it has a complex logic,
we are going to start from simple using archive `by-year`.

#### Page Content: Archive By Year

* [gitlab.com/.../pages/by-year.html][tutor-pa-by-year]

{{< highlight markdown >}}
---
layout    : by-year
title     : Archive by Year
permalink : /by-year/
---
{{< / highlight >}}

#### Layout Liquid: Archive By Year

* [gitlab.com/.../_layouts/by-year.html][tutor-vl-by-year]

{{< highlight jinja >}}
---
layout: page
---

{% assign posts = site.posts %}
{% include index/by-year.html %}
{{< / highlight >}}

This layout is a child of `page` layout.

#### Partial Liquid: Archive By Year

> This is the Bootstrap View Part

As usual, we setup the liquid logic first.

* [gitlab.com/.../_includes/index/by-year.html][tutor-vi-by-year]

{{< highlight jinja >}}
{% assign postsByYear = posts
          | group_by_exp: "post", "post.date | date: '%Y'"  %}

<div id="archive">
{% for year in postsByYear %}

  {% capture spaceless %}
    {% assign current_year = 'now' | date: '%Y' %}
    {% assign year_text = nil %}

    {% if year.name == current_year %}
      {% assign year_text = year.name
                | prepend: "This year's posts (" | append: ')' %}
    {% else %}
      {% assign year_text = year.name %}
    {% endif %}
  {% endcapture %}

  <section>
    ...
  </section>

{% endfor %}
</div>
{{< / highlight >}}

And then, fill the view with `HTML` elements,
along with bootstrap class.

{{< highlight html >}}
  <section>
    <p class ="anchor-target" 
       id="{{ year.name }}"
      >{{ year_text }}</p>

    <div class="py-1">
      {% for post in year.items %}
      <div>
        <div class="float-left">
          <a href="{{ site.baseurl }}{{ post.url }}">
          {{ post.title }}
        </a></div>
        <div class="float-right text-right">
        <time>
            {{ post.date | date:"%d %b" }}&nbsp;
        </time></div>
        <div class="clearfix"></div>
      </div>
      {% endfor %}
    </div>
  </section>
{{< / highlight >}}

![Jekyll Bootstrap: Archive By Year][image-by-year]

#### Page Content: Archive By Month

* [gitlab.com/.../pages/by-month.html][tutor-pa-by-month]

{{< highlight markdown >}}
---
layout    : by-month
title     : Archive by Month
permalink : /by-month/
---
{{< / highlight >}}

#### Layout Liquid: Archive By Month

* [gitlab.com/.../_layouts/by-month.html][tutor-vl-by-month]

{{< highlight jinja >}}
---
layout: page
---

{% assign posts = site.posts %}
{% include index/by-month.html %}
{{< / highlight >}}

This layout is a child of `page` layout.

#### Partial Liquid: Archive By Month

> This is the Bootstrap View Part

As usual, we setup the liquid logic first.

* [gitlab.com/.../_includes/index/by-month.html][tutor-vi-by-month]

{{< highlight jinja >}}
{% assign postsByYear = posts
          | group_by_exp: "post", "post.date | date: '%Y'"  %}

<div id="archive">
{% for year in postsByYear %}

  {% capture spaceless %}
    {% assign current_year = 'now' | date: '%Y' %}
    {% assign year_text = nil %}

    {% if year.name == current_year %}
      {% assign year_text = year.name 
                | prepend: "This year's posts (" | append: ')' %}
    {% else %}
      {% assign year_text = year.name %}
    {% endif %}
  {% endcapture %} 

  <section>
    <p class ="anchor-target mb-0"
         id="{{ year.name }}"><b>{{ year_text }}</b></p>

    {% assign postsByMonth = year.items
              | group_by_exp:"post", "post.date | date: '%m'"
              | sort: 'name'
              | reverse %}

    {% for month in postsByMonth %}

    <div class="py-1">
      ...
    </div>

    {% endfor %}
  </section>

{% endfor %}
</div>
{{< / highlight >}}

And then, fill the view with `HTML` elements,
along with bootstrap class.

{{< highlight html >}}
    <div class="py-1">

      {% for post in month.items limit:1 %}
      <div id="{{ year.name }}-{{ month.name }}">
           {{ post.date | date: '%b - %Y' }}</div>
      {% endfor %}

      <div class="py-1">
        {% for post in month.items %}
        <div>
          <div class="float-left">
          <a class="text-dark"
             href="{{ site.baseurl }}{{ post.url }}">
            {{ post.title }}
          </a></div>
          <div class="float-right has-text-right"><time>
              {{ post.date | date:"%d %b" }}
          </time></div>
          <div class="clearfix"></div>
        </div>
        {% endfor %}
      </div>
    </div>
{{< / highlight >}}

![Jekyll Bootstrap: Archive By Month][image-by-month]

I know this is a long article,
to remind the reader about the relationship,
between `pages`, `layout` and `partial` include.
The next article should be shorter,
focusing on stylesheet stuff.

-- -- --

### What is Next?

Consider continue reading [ [Jekyll Bootstrap - SASS Intro][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/07/04/jekyll-bsoc-sass-intro
[local-bootstrap-layout]:   {{< baseurl >}}frontend/2020/03/10/bootstrap-sass-responsive/
[tutor-html-master-08]: {{< tutor-jekyll-bsoc >}}/tutor-08/

[managing-code-03]:     {{< baseurl >}}ssg/2020/06/12/jekyll-plain-managing-code-03/

[tutor-vi-blog-list]:   {{< tutor-jekyll-bsoc >}}/tutor-08/_includes/index/blog-list.html
[tutor-vi-terms-array]: {{< tutor-jekyll-bsoc >}}/tutor-08/_includes/index/terms-array.html
[tutor-vi-terms-badge]: {{< tutor-jekyll-bsoc >}}/tutor-08/_includes/index/terms-badge.html
[tutor-vi-terms-list]:  {{< tutor-jekyll-bsoc >}}/tutor-08/_includes/index/terms-list.html
[tutor-vi-terms-tree]:  {{< tutor-jekyll-bsoc >}}/tutor-08/_includes/index/terms-tree.html
[tutor-vi-by-year]:     {{< tutor-jekyll-bsoc >}}/tutor-08/_includes/index/by-year.html
[tutor-vi-by-month]:    {{< tutor-jekyll-bsoc >}}/tutor-08/_includes/index/by-month.html

[tutor-vl-blog]:        {{< tutor-jekyll-bsoc >}}/tutor-08/_layouts/blog.html
[tutor-vl-tags]:        {{< tutor-jekyll-bsoc >}}/tutor-08/_layouts/list-tag.html
[tutor-vl-categories]:  {{< tutor-jekyll-bsoc >}}/tutor-08/_layouts/list-category.html
[tutor-vl-by-year]:     {{< tutor-jekyll-bsoc >}}/tutor-08/_layouts/by-year.html
[tutor-vl-by-month]:    {{< tutor-jekyll-bsoc >}}/tutor-08/_layouts/by-month.html

[tutor-pa-blog]:        {{< tutor-jekyll-bsoc >}}/tutor-08/pages/index.html
[tutor-pa-tags]:        {{< tutor-jekyll-bsoc >}}/tutor-08/pages/tags.html
[tutor-pa-categories]:  {{< tutor-jekyll-bsoc >}}/tutor-08/pages/categories.html
[tutor-pa-by-year]:     {{< tutor-jekyll-bsoc >}}/tutor-08/pages/by-year.html
[tutor-pa-by-month]:    {{< tutor-jekyll-bsoc >}}/tutor-08/pages/by-month.html

[image-blog-list]:      {{< assets-ssg >}}/2020/07/bootstrap/08-blog-list.png
[image-tags-list]:      {{< assets-ssg >}}/2020/07/bootstrap/08-tags-list.png
[image-cats-list]:      {{< assets-ssg >}}/2020/07/bootstrap/08-cats-list.png
[image-terms-tree]:     {{< assets-ssg >}}/2020/07/bootstrap/08-terms-tree.png
[image-by-year]:        {{< assets-ssg >}}/2020/07/bootstrap/08-by-year.png
[image-by-month]:       {{< assets-ssg >}}/2020/07/bootstrap/08-by-month.png
