---
type   : post
title  : "Jekyll Bootstrap - SASS Intro"
date   : 2020-07-04T09:17:35+07:00
slug   : jekyll-bsoc-sass-intro
categories: [ssg]
tags      : [jekyll, bootstrap, sass]
keywords  : [assets, liquid, layout, columns, refactoring template]
author : epsi
opengraph:
  image: assets-ssg/2020/07/bootstrap/09-tree-sass.png

toc    : "toc-2020-06-jekyll-step"

excerpt:
  Building Jekyll Site Step by step, using Bootstrap OC as stylesheet.
  Apply SASS in Jekyll using Bulma Open Color.
---

### Preface

> Goal: Apply SASS in Jekyll using Bulma Open Color.

#### Source Code

This article use [tutor-09][tutor-html-master-09] theme.

#### Layout Preview for Tutor 09

![Liquid: Layout Preview for Tutor 09][template-inheritance-09]

[template-inheritance-09]:  {{< assets-ssg >}}/2020/06/09-template-inheritance.png

-- -- --

### 1: About Bootstrap Stylesheet

> Use SASS in Jekyll.

This is basically just changing theme,
from using simple CSS theme to customizable SASS theme.
We have to start the previous chapter all over again,
without liquid relationship, 
but with complex stylesheet.

#### The Choice

> As default, Jekyll is equipped with SASS converter

* <https://github.com/jekyll/jekyll-sass-converter>

There is always a place to enhance this site looks.

	Create necessary sass, as you need.
	However, still Bootstrap can save you,
	from writing a lot of CSS code.

SASS is not the only CSS preprocessor.
There are many options for this such as:
`Stylus`, `Less`, or even `PostCSS` via `PreCSS`.

Both `Bootstrap` and `Bulma` is using `SASS`,
so `jekyll+bootstrap` or `jekyll+bulma` or even `jekyll+custom sass`,
is the easy choice for jekyll's beginner.

There are other choice as well in the internet,
such as `jekyll+tailwind`, but I never try one.

#### Prerequiste

> Use Bootstrap MD in Jekyll.

This article rely on Bootstrap OC in article below.
You should give a fast reading of this Bootstrap OC article first,
before continue reading this article.

* [Bootstrap OC - Overview][local-bootstrap]

#### Reference

* [sass-lang.com](https://sass-lang.com/)

#### SASS in Jekyll

* Jekyll-v3 is using `ruby-sass`,
  and blamed to be the source of sluggish build.

* Jekyll-v4 is using `sassc`, and significantly improve build speed.

#### SASS Format

SASS lang has two dialect,
the one with `*.sass` extension using indentation,
and the one with `*.scss` extension using css like format.
Both format supported by Jekyll SASS.

-- -- --

### 2: Prepare: Assets

With the right configuration, `Jekyll` can manage `SASS`.
This tutorial using conventinal way, running SASS from CLI.

#### Directory Tree: CSS

First thing to do is to create `sass` directory,
and fill with required SASS.

{{< highlight bash >}}
$ tree -d _sass
_sass
├── bootstrap
│   ├── mixins
│   ├── utilities
│   └── vendor
└── css
    ├── feather
    ├── main
    ├── materialize
    ├── open-color
    └── post

10 directories
{{< / highlight >}}

[Bootstrap OC][local-bootstrap-sass] has a bunch of SASS.
If you take a look closer of the `_sass/css`,
it contains a few separated stylesheet,
prepared design to build a blog site.

{{< highlight bash >}}
$ tree _sass/css
_sass/css
├── bootstrap.scss
├── feather
│   └── _icons-feather.scss
├── helper.scss
├── main
│   ├── _decoration.scss
│   ├── _layout-content.scss
│   ├── _layout-page.scss
│   ├── _list.scss
│   ├── _logo.scss
│   └── _sticky-footer.scss
├── main.scss
├── materialize
│   └── _shadow.scss
├── open-color
│   ├── _open-color-classes.scss
│   └── _open-color-variables.scss
├── post
│   ├── _content.scss
│   └── _navigation.scss
└── _variables.scss

5 directories, 16 files
{{< / highlight >}}

For the detail of what each SASS file artefact do,
you can fast reading this article below __:__

* [Bootstrap - Sass - Introduction][local-bootstrap-sass]

So expect, to face new custom class in example ahead.

![Jekyll: Tree: SASS][image-tree-sass]

#### Calling SASS in Jekyll

With all the complexity above,
we can packed all stylesheets,
into just three CSS file artefacts.

Consider have a look at the the build destination directory in `_site`:

{{< highlight bash >}}
❯ tree _site/assets/css
_site/assets/css
├── bootstrap.css
├── helper.css
└── main.css

0 directories, 3 files
{{< / highlight >}}

And compare, with the source `SASS` in assets directory.

{{< highlight bash >}}
$ tree assets/css
assets/css
├── bootstrap.scss
├── helper.scss
└── main.scss

0 directories, 3 files
{{< / highlight >}}

For this to be happened,
we set in each `SASS` file artefact,
for example:

* [gitlab.com/.../assets/css/main.scss][tutor-sass-main]

{{< highlight sass >}}
---
# Only the main Sass file needs front matter (the dashes are enough)
---

@charset "utf-8";

// Import partials from `sass_dir` (defaults to `_sass`)
@import "css/main";
{{< / highlight >}}

And so the other two files

* [gitlab.com/.../assets/css/bootstrap.scss][tutor-sass-bootstrap]

{{< highlight sass >}}
---
---
@import "css/bootstrap";
{{< / highlight >}}

* [gitlab.com/.../assets/css/helper.scss][tutor-sass-helper]

{{< highlight sass >}}
---
---
@import "css/helper";
{{< / highlight >}}

![Jekyll: Assets Tree: SASS][image-tree-asset-sass]

This `jekyll-sass-converter` will compile them automatically.
Now the stylesheet is ready to be used.

-- -- --

### 3: Layout: Refactoring Base

> Wearable Layout

As we did with previous articles.
We should start over again from basic layout.

#### Layout: Liquid Default

* [gitlab.com/.../_layouts/default.html][tutor-vl-default]

No difference here. Just leave this `base` as it was.

#### Partial Liquid: 

* [gitlab.com/.../_includes/site/head.html][tutor-vi-head]

There is no changes here either.

#### Partial Liquid: Header

The only differences is,
I add this class: `oc-white z-depth-3 hoverable`.

It is still a long header, so again I crop the code.
You can get the complete code in the repository.

* [gitlab.com/.../_includes/site/header.html][tutor-vi-header]

{{< highlight html >}}
  {% comment %}<!-- navigation bar -->{% endcomment %}
  <nav class="navbar navbar-expand-md navbar-light fixed-top maxwidth
              oc-white z-depth-3 hoverable">
    ...
  </nav>
{{< / highlight >}}

#### Partial Liquid: Scripts

No difference. Just leave this intacts.

* [gitlab.com/.../_includes/site/scripts.html][tutor-vi-scripts]

#### Layout Liquid: Footer

Just like header,
the only differences is,
I add this class: `oc-white z-depth-3 hoverable`.

* [gitlab.com/.../_includes/site/footer.html][tutor-vi-footer]

{{< highlight html >}}
  {% comment %}<!-- footer -->{% endcomment %}
  <footer class="footer">
    <div class="maxwidth text-dark text-center
                oc-white z-depth-3 hoverable">
      &copy; {{ site.time | date: '%Y' }}.
    </div>
  </footer>
{{< / highlight >}}

-- -- --

### 4: Layout: Home

Consider use `home` layout` to begin,
and all other layout later.

#### Layout Liquid: Home

> As previous, this view is a single column design.

There are a lot of changes in here.
Now the layout is using `main-wrapper`,
and inside `main-wrapper` has `blog-header` and `blog-body`.

* [gitlab.com/.../_layouts/home.html][tutor-vl-home]

{{< highlight html >}}
---
layout : default
---
  <main role="main" class="col px-0">
    <section class="main-wrapper single oc-blue-5">
      <div class="blog oc-white z-depth-3 hoverable">
        <section class="blog-header oc-blue-1 text-center">
          <h2 class="h4 font-weight-bold" itemprop="name headline">
            {{ page.title | default: site.title }}</h2>
        </section>

        <article class="blog-body text-center" itemprop="articleBody">
          {{ content }}
        </article>
        
        <div class="text-muted text-center">
          <i data-feather="home"></i>&nbsp;
          This is a home kind layout,
          to show landing page.
        </div>
      </div>
    </section>
  </main>
{{< / highlight >}}

There will be double columns design in other layout.
Both single column and double columns design,
wear the same default parent.

#### Page Content: Index (Home)

I scrapped all the code, and place a landing page,
along with bootstrap buttons.

* [gitlab.com/.../index.html][tutor-vc-index]

{{< highlight html >}}
---
layout: home
---

  <br/>

  <p>
    <a class="btn btn-primary my-2"
       href="{{ site.baseurl }}/by-month/"
      >Articles Sorted by Month</a>
    <a class="btn btn-secondary my-2"
       href="{{ site.baseurl }}/tags/"
      >Articles Sorted by Tag</a>
  </p>

  <p>As always,
  should you be caught or killed,
  any knowledge of your actions will be disavowed.</p>

  <div class="textcenter">
    <img src="assets/images/cards/one-page.png" 
         alt="business card">
  </div>

  <p class="small lead text-muted">    
      Whitewood Street, Monday Market,
      East Jakarta, 55112, Indonesia.
  </p>
{{< / highlight >}}

#### Render: Browser

Open in your favorite browser.
You should see, a blue colored homepage, by now.

* <http://localhost:4000/>

![Jekyll: Page Content: Home][image-vc-pa-home]

Notice that this is a single column page.
All other page is double columns,
and again, deserve its own explanation.

-- -- --

### What is Next?

Consider continue reading [ [Jekyll Bootstrap - SASS Layout][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/07/05/jekyll-bsoc-sass-layout/
[local-bootstrap]:      {{< baseurl >}}frontend/2020/03/01/bootstrap-oc-overview/
[local-bootstrap-sass]: {{< baseurl >}}frontend/2020/03/07/bootstrap-sass-intro/
[tutor-html-master-09]: {{< tutor-jekyll-bsoc >}}/tutor-09/

[tutor-sass-bootstrap]: {{< tutor-jekyll-bsoc >}}/tutor-09/assets/css/bootstrap.scss
[tutor-sass-main]:      {{< tutor-jekyll-bsoc >}}/tutor-09/assets/css/main.scss
[tutor-sass-helper]:    {{< tutor-jekyll-bsoc >}}/tutor-09/assets/css/helper.scss

[tutor-vl-default]:     {{< tutor-jekyll-bsoc >}}/tutor-09/_layouts/default.html
[tutor-vl-home]:        {{< tutor-jekyll-bsoc >}}/tutor-09/_layouts/home.html

[tutor-vi-head]:        {{< tutor-jekyll-bsoc >}}/tutor-09/_includes/site/head.html
[tutor-vi-header]:      {{< tutor-jekyll-bsoc >}}/tutor-09/_includes/site/header.html
[tutor-vi-footer]:      {{< tutor-jekyll-bsoc >}}/tutor-09/_includes/site/footer.html
[tutor-vi-scripts]:     {{< tutor-jekyll-bsoc >}}/tutor-09/_includes/site/scripts.html
[tutor-vc-index]:       {{< tutor-jekyll-bsoc >}}/tutor-09/index.html

[image-tree-sass]:      {{< assets-ssg >}}/2020/07/bootstrap/09-tree-sass.png
[image-tree-asset-sass]:{{< assets-ssg >}}/2020/07/bootstrap/09-tree-assets-sass.png
[image-vc-pa-home]:     {{< assets-ssg >}}/2020/07/bootstrap/09-pages-home.png
