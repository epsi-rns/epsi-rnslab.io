---
type   : post
title  : "Jekyll Bootstrap - SASS Layout"
date   : 2020-07-05T09:17:35+07:00
slug   : jekyll-bsoc-sass-layout
categories: [ssg]
tags      : [jekyll, bootstrap, sass]
keywords  : [assets, liquid, layout, columns, refactoring template]
author : epsi
opengraph:
  image: assets-ssg/2020/07/bootstrap/09-posts-winter.png

toc    : "toc-2020-06-jekyll-step"

excerpt:
  Building Jekyll Site Step by step, using Bootstrap OC as stylesheet.
  Using custom template inheritance to manage column in layout.
---

### Preface

> Goal: Using custom template inheritance to manage column in layout.

#### Source Code

This article use [tutor-09][tutor-html-master-09] theme.

-- -- --

### 5: Layout: Page, Post

> Using the pattern to create parent layout.

Our Page Kind, and Post Kind is simply,
a double columns version of above layout.

As we did, we do it again.
Analyze pattern from one or two existing template,
write down anything similar,
and take out any differences.

#### Layout Liquid: Page

Just like previous chapter,
but very long layout, with a lot of changes.
Now the layout is using both `main-wrapper`, and `aside-wrapper`.

* [gitlab.com/.../_layouts/page.html][tutor-vl-page]

{{< highlight html >}}
---
layout: default
aside_message : This is a page kind layout.
---

  <main class="col-md-8 px-0">
    <section class="main-wrapper oc-pink-5">
      <div class="blog oc-white z-depth-3 hoverable">

        <section class="blog-header oc-pink-1">
          <h1 class="h4 font-weight-bold" itemprop="name headline">
            {{ page.title | default: site.title }}</h1>
        </section>

        <article class="blog-body" itemprop="articleBody">
          {{ content }}
        </article>

      </div>
    </section>
  </main>

  <aside class="col-md-4 px-0">   
    <section class="aside-wrapper oc-green-5">
      <div class="widget oc-white z-depth-3 hoverable">     

        <div class="widget-header oc-green-1">
          <strong>Side Menu</strong>
          <i data-feather="coffee" class="float-right"></i>
        </div>

        <div class="widget-body">
          {{ layout.aside_message }}
        </div>

      </div>
    </section>
  </aside>
{{< / highlight >}}

We have two wrapper classes here:

* `main-wrapper`, and

* `aside-wrapper`.

#### Page Content: pages/about

No difference with previous chapter.
Just leave the content that way.

* [gitlab.com/.../pages/about.md][tutor-vc-pa-about]

#### Render: Browser

Open in your favorite browser.
You should see, a colored about page, by now.

![Jekyll: Page Content: pages/about][image-vc-about]

#### Layout Liquid: Post

Just like previous chapter,
but very long layout
There is only a slight customization,
compared with current `page` layout.

* [gitlab.com/.../_layouts/post.html][tutor-vl-post]

{{< highlight html >}}
---
layout: default
aside_message : This is a post kind layout.
---

  <main class="col-md-8 px-0">
    <section class="main-wrapper oc-blue-5">
      <div class="blog oc-white z-depth-3 hoverable">

        <section class="blog-header oc-blue-1">
          <h1 class="h4 font-weight-bold" itemprop="name headline">
            {{ page.title | default: site.title }}</h1>
          <p><strong>{{ page.date | date: "%B %d, %Y" }}</strong></p>
        </section>

        <article class="blog-body" itemprop="articleBody">
          {{ content }}
        </article>

      </div>
    </section>
  </main>

  <aside class="col-md-4 px-0">   
    <section class="aside-wrapper oc-teal-5">
      <div class="widget oc-white z-depth-3 hoverable">     

        <div class="widget-header oc-teal-1">
          <strong>Side Menu</strong>          
          <i data-feather="activity" class="float-right"></i>
        </div>

        <div class="widget-body">
          {{ layout.aside_message }}
        </div>

      </div>
    </section>
  </aside>
{{< / highlight >}}

#### Post Content: winter.md

No difference with previous chapter.
Just leave the content that way.

* [gitlab.com/.../_posts/2016-01-01-winter.md][tutor-vc-po-winter]

#### Render: Browser

Open in your favorite browser.
You should see, a colored post, by now.

![Jekyll: Post Content: _posts/winter][image-vc-winter]

-- -- --

### 6: Layout: Blog

Page content and layout is remain the same.
Consider go straight to the view.

#### Partial Liquid: Blog List

> This is the Bootstrap View Part.

We have one partial HTML view for this layout.

* [gitlab.com/.../_includes/index/blog-list.html][tutor-vi-blog-list]

{{< highlight html >}}
  <div class="post-list">
  {% for post in posts %}
    <div class="archive pb-2">
    <div class="meta-item p-2 hoverable">

      <div class="meta">
        <div class="float-left">
          <a class="meta_link oc-pink-9-text" 
             href="{{ post.url | prepend: site.baseurl }}"
            >{{ post.title }}
          </a>
        </div>
        <div class="meta_tags float-right">
          {% for tag in post.tags %}
          <span class="badge oc-pink-9 z-depth-1 hoverable m-2">
            <a class="oc-white-text"
               href="{{ site.baseurl }}/tags/#{{ tag | slugify }}"
              >{{ tag }}&nbsp;<i data-feather="tag" class="feather-14"></i>
            </a></span>&nbsp;
          {% endfor %}
        </div>
      </div> 

      <div class="clearfix"></div>

    </div></div>
  {% endfor %}
  </div>
{{< / highlight >}}

Actually, there are only very few differences with our last partial.
I just add a few class with the very same structure.

![Jekyll Bootstrap: Blog List][image-blog-list]

The most thing that make the looks so different is,
the `page` layout: header, footer, and the frame.

-- -- --

### 7: Layout: Tag List, Category List.

We can rewrite related views as well.

#### Partial Liquid: Terms

> This is the Bootstrap View Part.

We have two partial HTML views for this layout:
`terms-badge` for top summary, and `terms-tree` for details.

Just like `blog-list`,
I just add a few class without any change in structure.

* [gitlab.com/.../_includes/index/terms-badge.html][tutor-vi-terms-badge]]

{{< highlight html >}}
  <div class="py-3">
  {% for item in (0..terms.size) %}{% unless forloop.last %}
    {% assign this_word = term_array[item] | strip_newlines %}
    <span>
      {{ $posts := where $value.Pages "Type" "post" }}
      {{ $postCount := len $posts -}}
      <a href="#{{ this_word | slugify }}">
        <span class="badge oc-pink-3 oc-black-text
                     z-depth-1 hoverable">
          {{ this_word }} 
          <span class="badge oc-pink-7 oc-white-text">
          {{ terms[this_word].size }}
          </span>
        </span>
      </a>
    </span>
    &nbsp;
  {% endunless %}{% endfor %}
  </div>
{{< / highlight >}}

But this one, I refactor the inside loop to separate partial,
so I can reuse the code in archive.

* [gitlab.com/.../_includes/index/terms-tree.html][tutor-vi-terms-tree]

{{< highlight html >}}
  <section class="py-1" id="archive">
  {% for item in (0..terms.size) %}{% unless forloop.last %}
    {% assign this_word = term_array[item] | strip_newlines %}
      <div id="{{ this_word | slugify }}" class ="anchor-target">
        <strong>{{ this_word }}</strong>
      </div>

      <div class="archive-list py-1">
      {% for post in terms[this_word] %}
        {% if post.title != null %}
          {% include index/each-post.html %}
        {% endif %}
      {% endfor %}
      </div>

  {% endunless %}{% endfor %}
  </section>
{{< / highlight >}}

#### Partial Liquid: Each Post

We are going to use this partial in three places:
`terms-tree.html`, `by-year.html`, and  `by-month.html`.

* [gitlab.com/.../_includes/index/each-post.html][tutor-vi-each-post]

{{< highlight html >}}
  <div class="archive-item meta-item">
    <div class="meta_link text-right">
      <time class="meta_time float-right"
            datetime="{{ post.date | date_to_xmlschema }}">
        {{ post.date | date: "%b %-d, %Y" }}&nbsp;
        &nbsp;<i data-feather="calendar"></i></time></div>
    <div class="float-left">
    <a href="{{ post.url | prepend: site.baseurl }}">
      {{ post.title }}
    </a></div>
    <div class="clearfix"></div>
  </div>
{{< / highlight >}}

#### Categories

Page content and layout is remain the same.

![Jekyll Bootstrap: Categories with Tree Details][image-cats-list]

#### Tags

Page content and layout is remain the same.

![Jekyll Bootstrap: Tags with Tree Details][image-tags-list]

-- -- --

### 8: Layout: Archive By Year, then By Month

Page content and layout is remain the same.
Consider go straight to the view.

#### Partial Liquid: Archive By Year

> This is the Bootstrap View Part

The liquid logic, still remain the same.

* [gitlab.com/.../_includes/index/by-year.html][tutor-vi-by-year]

And the view with `HTML` elements, is also shorter now.

{{< highlight html >}}
  <section>
    <p class ="anchor-target" 
       id="{{ year.name }}"
      >{{ year_text }}</p>

    <div class="py-1">
    {% for post in year.items %}
      {% include index/each-post.html %}
    {% endfor %}
    </div>
  </section>
{{< / highlight >}}

![Jekyll Bootstrap: Archive By Year][image-by-year]

#### Partial Liquid: Archive By Month

> This is the Bootstrap View Part

The liquid logic, is remain the same.

* [gitlab.com/.../_includes/index/by-month.html][tutor-vi-by-month]

And the view with `HTML` elements, is also shorter now.

{{< highlight html >}}
  <section>
    <p class ="anchor-target" 
         id="{{ year.name }}"><b>{{ year_text }}</b></p>

    {% assign postsByMonth = year.items
              | group_by_exp:"post", "post.date | date: '%m'"
              | sort: 'name'
              | reverse %}

    {% for month in postsByMonth %}

    <div class="py-1">

      {% for post in month.items limit:1 %}
      <div id="{{ year.name }}-{{ month.name }}">
           {{ post.date | date: '%b - %Y' }}</div>
      {% endfor %}

      <div class="py-1">
      {% for post in month.items %}
        {% include index/each-post.html %}
      {% endfor %}
      </div>
    </div>

    {% endfor %}
  </section>
{{< / highlight >}}

![Jekyll Bootstrap: Archive By Month][image-by-month]

-- -- --

#### Conclusion

Small thing, might have great impact.

> Start from general design, then go further into details.

It would be nice if you can port templates from pure html to jekyll.

-- -- --

### What is Next?

Consider continue reading [ [Jekyll Bootstrap - Layout - Variables][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/07/06/jekyll-bsoc-layout-variables/
[tutor-html-master-09]: {{< tutor-jekyll-bsoc >}}/tutor-09/

[tutor-vl-page]:        {{< tutor-jekyll-bsoc >}}/tutor-09/_layouts/page.html
[tutor-vl-post]:        {{< tutor-jekyll-bsoc >}}/tutor-09/_layouts/post.html

[tutor-vi-blog-list]:   {{< tutor-jekyll-bsoc >}}/tutor-09/_includes/index/blog-list.html
[tutor-vi-each-post]:   {{< tutor-jekyll-bsoc >}}/tutor-09/_includes/index/each-post.html
[tutor-vi-terms-badge]: {{< tutor-jekyll-bsoc >}}/tutor-09/_includes/index/terms-badge.html
[tutor-vi-terms-tree]:  {{< tutor-jekyll-bsoc >}}/tutor-09/_includes/index/terms-tree.html
[tutor-vi-by-year]:     {{< tutor-jekyll-bsoc >}}/tutor-09/_includes/index/by-year.html
[tutor-vi-by-month]:    {{< tutor-jekyll-bsoc >}}/tutor-09/_includes/index/by-month.html

[tutor-vc-index]:       {{< tutor-jekyll-bsoc >}}/tutor-09/index.html
[tutor-vc-pa-about]:    {{< tutor-jekyll-bsoc >}}/tutor-09/pages/about.md
[tutor-vc-po-winter]:   {{< tutor-jekyll-bsoc >}}/tutor-09/_posts/2016-01-01-winter.md

[image-vc-about]:       {{< assets-ssg >}}/2020/07/bootstrap/09-pages-about.png
[image-vc-winter]:      {{< assets-ssg >}}/2020/07/bootstrap/09-posts-winter.png
[image-blog-list]:      {{< assets-ssg >}}/2020/07/bootstrap/09-blog-list.png
[image-tags-list]:      {{< assets-ssg >}}/2020/07/bootstrap/09-tags-list.png
[image-cats-list]:      {{< assets-ssg >}}/2020/07/bootstrap/09-cats-list.png
[image-terms-tree]:     {{< assets-ssg >}}/2020/07/bootstrap/09-terms-tree.png
[image-by-year]:        {{< assets-ssg >}}/2020/07/bootstrap/09-by-year.png
[image-by-month]:       {{< assets-ssg >}}/2020/07/bootstrap/09-by-month.png
