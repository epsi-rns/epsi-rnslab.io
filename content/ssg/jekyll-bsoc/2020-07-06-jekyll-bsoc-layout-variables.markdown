---
type   : post
title  : "Jekyll Bootstrap - Layout - Variables"
date   : 2020-07-06T09:17:35+07:00
slug   : jekyll-bsoc-layout-variables
categories: [ssg]
tags      : [jekyll, bootstrap]
keywords  : [liquid, layout, columns, color, open colors]
author : epsi
opengraph:
  image: assets-ssg/2020/07/bootstrap/10-color-inheritance.png

toc    : "toc-2020-06-jekyll-step"

excerpt:
  Building Jekyll Site Step by step, using Bootstrap OC as stylesheet.
  Using custom template inheritance to manage color variables.
---

### Preface

> Goal: Using custom template inheritance to manage color variables.

#### Source Code

This article use [tutor-10][tutor-html-master-10] theme.


#### Layout Preview for Tutor 10

![Liquid: Layout Preview for Tutor 10][template-inheritance-10]

[template-inheritance-10]:  {{< assets-ssg >}}/2020/06/10-template-inheritance.png

-- -- --

### 1: Parent: Single Column

We meet again with layout, for the third time.
We require both column kind:

* Single Column: child of Base Layout.

* Double Columns: child of Base Layout.

We are going to get rid unused sidebar.
So that all pages will be a `column-single` layout,
except `page` layout and `post` layout.

The directory tree can be shown as below figure:

![Jekyll: NERDTree: A Few Layouts][image-nerdtree-layout]

#### Parent Liquid Layout: Single Column

Consider create a parent layout for single column layout.
We have two parts here:

1. The frontmatter and variables setting.
2. The `main` html view, where we use the variables.

This layout is equipped with a partial set in `blog_header`.

* [gitlab.com/.../_layouts/column-single.html][tutor-vl-single]

The Frontmatter:

{{< highlight yaml >}}
---
layout: default

# default property
color_main  : blue
blog_class  : ""

# layout chuncks
blog_header : page/blog-header.html
---

{% assign color_main  = page.color_main | default: layout.color_main %}
{% assign blog_class  = page.blog_class | default: layout.blog_class %}
{{< / highlight >}}

The `main` Content:

{{< highlight HTML >}}
  <main role="main" class="col px-0">
    <section class="main-wrapper single oc-{{ color_main }}-5">
      <div class="blog oc-white z-depth-3 hoverable">

        <section class="blog-header oc-{{ color_main }}-0 {{ blog_class }}">
          {% include {{ layout.blog_header }} %}
        </section>

        <article class="blog-body {{ blog_class }}" itemprop="articleBody">
          {{ content }}
        </article>
        
      </div>
    </section>
  </main>
{{< / highlight >}}

#### Partial Liquid: Page/ Blog Header

We can have default partial chunks in parent layout,
and override the partial chunks in child layout.

{{< highlight jinja >}}
---
# layout chuncks
blog_header : page/blog-header.html
---
{{< / highlight >}}

The `blog_header` partial is:

* [gitlab.com/.../_includes/page/blog-header.html][tutor-vpa-blog-header]

{{< highlight jinja >}}
  <div class="main_title"> 
    <h1 class="h4 font-weight-bold" itemprop="name headline">
      {{ page.title | default: site.title }}</h1>
  </div>
{{< / highlight >}}

#### Layout: Default Color

The default color for `main` element is set as below code:

{{< highlight jinja >}}
---
# default property
color_main  : blue
---

{% assign color_main  = page.color_main | default: layout.color_main %}
{{< / highlight >}}

#### Rule: Color Setting

Keep in mind this rule:

1. The frontmatter `color_main` in layout: set `layout.color_main`.
2. The frontmatter `color_main` can be overriden in child layout.
3. The frontmatter `color_main` in page: set `page.color_main`.
4. The final `color_main` for the content: set by `assign` in liquid.
5. Color name must match the previously prepared,
   custom `open-color` stylesheet.

The frontmatter set `layout.color_main` to `blue` as default.
With assigment below frontmatter, we can override default `blue` color,
by set any color in `page.color_main`.

-- -- --

### 2: Parent: Double Columns

Most of the layout remain the same,
except the `blog_header` part using external liquid partial.

#### Parent Layout: Liquid: Double Columns

Consider modify the parent layout for double column layout.
We have two parts here:

1. The frontmatter and variables setting.
2. The `main` html view, where we use the variables.
3. The `aside` html view, where we also use the variables.

This layout is also equipped with a partial set in `blog_header`.

* [gitlab.com/.../_layouts/columns-double.html][tutor-vl-double]

The Frontmatter:

{{< highlight yaml >}}
---
layout : default

# default color
color_main  : blue
color_aside : teal

# layout chuncks
blog_header     : page/blog-header.html
aside_message   : This is a double columns kind layout.
---

{% assign color_main  = page.color_main  | default: layout.color_main %}
{% assign color_aside = page.color_aside | default: layout.color_aside %}
{{< / highlight >}}

The `main` Content:

{{< highlight HTML >}}
  <main class="col-md-8 px-0">
    <section class="main-wrapper oc-{{ color_main }}-5">
      <div class="blog oc-white z-depth-3 hoverable">

        <section class="blog-header oc-{{ color_main }}-0">
          {% include {{ layout.blog_header }} %}
        </section>

        <article class="blog-body" itemprop="articleBody">
          {{ content }}
        </article>

      </div>
    </section>
  </main>
{{< / highlight >}}

The `aside` Content:

{{< highlight HTML >}}
  <aside class="col-md-4 px-0">
    <section class="aside-wrapper oc-{{ color_aside }}-5">
      <div class="widget oc-white z-depth-3 hoverable">     

        <div class="widget-header oc-{{ color_aside }}-1">
          <strong>Side Menu</strong>
          <i data-feather="coffee" class="float-right"></i>
        </div>

        <div class="widget-body">
          {{ layout.aside_message }}
        </div>

      </div>
    </section>
  </aside>
{{< / highlight >}}

#### Partial Liquid: Page/ Blog Header

The partial chunk is the same with what we have in `single-column`.
Notice: the partial chunk is still `page/blog-header`.
Do not confused with `post/blog-header`.

#### Default Color

The default color for `main` element is set as below code:

{{< highlight jinja >}}
{% assign color_main  = page.color_main  | default: layout.color_main %}
{{< / highlight >}}

While the default color for `aside` element is set as below code:

{{< highlight jinja >}}
{% assign color_aside = page.color_aside | default: layout.color_aside %}
{{< / highlight >}}

The rule is still the same as what we have in `single-column`.

-- -- --

### 3: Child: Page and Post

Consider have applying the template for `page`  kind and `post` kind.

The example of color setting for each pages,
can be shown as below figure.
Most default color can be overriden in frontmatter.

![Jekyll: NERDTree: A Few Pages][image-nerdtree-pages]

#### Layout Liquid: Page

Now the layout is short,
bu equipped with extra variable override in frontmatter.

* [gitlab.com/.../_layouts/page.html][tutor-vl-page]

{{< highlight jinja >}}
---
layout: columns-double

# override color
color_main  : pink
color_aside : green

# layout chuncks
blog_header     : page/blog-header.html
aside_message   : This is a page kind layout.
---

{{ content }}
{{< / highlight >}}

#### Layout Liquid: Post

Now the layout is short,
bu equipped with extra variable override in frontmatter.

* [gitlab.com/.../_layouts/post.html][tutor-vl-post]

{{< highlight jinja >}}
---
layout: columns-double

# override color
color_main  : blue
color_aside : teal

# layout chuncks
blog_header     : post/blog-header.html
aside_message   : This is a post kind layout.
---

{{ content }}
{{< / highlight >}}

The `blog_header` part has different partial chunck.

#### Partial Liquid: Post/ Blog Header

Notice this in frontmatter:

{{< highlight jinja >}}
---
# layout chuncks
blog_header     : post/blog-header.html
---
{{< / highlight >}}

The `blog_header` partial is:

* [gitlab.com/.../_includes/post/blog-header.html][tutor-vpo-blog-header]

{{< highlight jinja >}}
  <div class="main_title"> 
    <h1 class="h4 font-weight-bold" itemprop="name headline">
      {{ page.title | default: site.title }}</h1>
    <p><strong>{{ page.date | date: "%B %d, %Y" }}</strong></p>
  </div>
{{< / highlight >}}

This is the short version of blog post header.
There will be several changes in later chapter.

#### Page Content

Since we are not going to use,
default color for pages and post,
just leave the the content intact.
All pages will have `pink` color,
and all post will have `blue` color.
However if you wish for an example,
you can examine this content below:

* [gitlab.com/.../_post/quotes/2015-01-01-jerry-maguire.md][tutor-vc-po-maguire]

{{< highlight markdown >}}
---
layout      : post
title       : Jerry Maguire
date        : 2015-01-01 17:35:15 +0700
categories  : movie
tags        : [subtitle, story]
color_main  : red
---

You had me at Hello.
{{< / highlight >}}

![Jekyll: Post Content: quotes/jerry-maguire][image-vc-po-maguire]

As a summary, the color will inherit,
from `jerry-maguire.md` frontmatter,
to `post` layout, then to `double-columns` layout.

![Jekyll: Color Property Propagation][image-color-inherit]

-- -- --

### 4: Child: Home

Instead of access `default` layout directly,
you can utilize `column-single`.

#### Layout Liquid: Home

> Siginficantly short.

* [gitlab.com/.../_layouts/home.html][tutor-vl-home]

{{< highlight jinja >}}
---
layout: columns-single

# override color
color_main  : indigo
blog_class  : text-center
---

{{ content }}
{{< / highlight >}}

Notice that we use `blog-class` to center all element in `home` layout.

#### Page Content: Landing Page

The content is remain untouched.

* [gitlab.com/.../index.html][tutor-vc-index]

{{< highlight html>}}
---
layout: home
---

  <br/>
...
{{< / highlight >}}

-- -- --

### 5: Child: All Index List Kind (Archive)

With the same logic, we can apply colors to all other pages as well.
For all layout, I change parent from `page` to `columns-single`.
And override all color from pages.

#### Layout Liquid: Blog

* [gitlab.com/.../_layouts/blog.html][tutor-vl-blog]

{{< highlight jinja >}}
---
layout: columns-single
---

{% assign posts = site.posts %}
{% include index/blog-list.html %} %}
{{< / highlight >}}

#### Page Content: Blog

* [gitlab.com/.../pages/index.html][tutor-pa-blog]

{{< highlight markdown >}}
---
layout      : blog
title       : Blog Posts
permalink   : /pages/

# override color
color_main  : teal
# color_aside : cyan
---
{{< / highlight >}}

I also add `color_aside` in frontmatter,
just in case you want to change the layout to `columns-double`.

#### Layout Liquid: Category List

The same logic also applied with `categories` and `tags`
This layout, is now featuring `page.icon`.
The detail will be shown in the next article.

* [gitlab.com/.../_layouts/list-category.html][tutor-vl-categories]

{{< highlight jinja >}}
---
layout: columns-single
---

{% assign terms = site.categories %}
{% include index/terms-array.html %}
{% include index/terms-badge.html %}
{% include index/terms-tree.html %}
{{< / highlight >}}

#### Page Content: Categories

* [gitlab.com/.../pages/categories.html][tutor-pa-categories]

{{< highlight markdown >}}
---
layout      : list-category
title       : All Categories
permalink   : /categories/

# override color
color_main  : indigo
# color_aside : orange

# decoration
term_icon : folder
---
{{< / highlight >}}

#### Layout Liquid: Tag List

* [gitlab.com/.../_layouts/list-tag.html][tutor-vl-tags]

{{< highlight jinja >}}
---
layout: columns-single
---

{% assign terms = site.tags %}
{% include index/terms-array.html %}
{% include index/terms-badge.html %}
{% include index/terms-tree.html %}
{{< / highlight >}}

#### Page Content: Tags

* [gitlab.com/.../pages/tags.html][tutor-pa-tags]

{{< highlight markdown >}}
---
layout      : list-tag
title       : All Tags
permalink   : /tags/

# override color
color_main  : teal
# color_aside : lime

# decoration
term_icon : tag
---
{{< / highlight >}}

#### Layout Liquid: Archive By Year

* [gitlab.com/.../_layouts/by-year.html][tutor-vl-by-year]

{{< highlight jinja >}}
---
layout: columns-single
---

{% assign posts = site.posts %}
{% include index/by-year.html %}
{{< / highlight >}}

#### Page Content: Archive By Year

* [gitlab.com/.../pages/by-year.html][tutor-pa-by-year]

{{< highlight markdown >}}
---
layout      : by-year
title       : Archive by Year
permalink   : /by-year/

# override color
color_main  : cyan
# color_aside : pink
---
{{< / highlight >}}

#### Layout Liquid: Archive By Month

* [gitlab.com/.../_layouts/by-month.html][tutor-vl-by-month]

{{< highlight jinja >}}
---
layout: columns-single
---

{% assign posts = site.posts %}
{% include index/by-month.html %}
{{< / highlight >}}

#### Page Content: Archive By Month

* [gitlab.com/.../pages/by-month.html][tutor-pa-by-month]

{{< highlight markdown >}}
---
layout      : by-month
title       : Archive by Month
permalink   : /by-month/

# override color
color_main  : cyan
# color_aside : grape
---
{{< / highlight >}}

-- -- --

### 6: Summary

As a summary, here is columns and colors.

#### Template Inheritance

Parent-child relation:

1. Single Column: extend Default

2. Double Columns: extend Default

* Page: extend Double Columns

* Post: extend Double Columns

* Home: extend Single Column

* Blog: extend Single Column

* Tags, Categories: extend Single Column

* By Year, By Month: extend Single Column

#### Default Color Variable in Layout

The default colors in layout is set as below:

1. Single Column: extend Default

{{< highlight jinja >}}
---
# default property
color_main  : blue
blog_class  : ""
---

{% assign color_main  = page.color_main | default: layout.color_main %}
{% assign blog_class  = page.blog_class | default: layout.blog_class %}
{{< / highlight >}}

2. Double Columns: extend Default

{{< highlight jinja >}}
---
# default color
color_main  : blue
color_aside : teal
---

{% assign color_main  = page.color_main  | default: layout.color_main %}
{% assign color_aside = page.color_aside | default: layout.color_aside %}
{{< / highlight >}}

* Page: extend Double Columns

{{< highlight jinja >}}
---
# override color
color_main  : pink
color_aside : green
---
{{< / highlight >}}

* Post: extend Double Columns

{{< highlight jinja >}}
---
# override color
color_main  : blue
color_aside : teal
---
{{< / highlight >}}

* Home: extend Single Column

{{< highlight jinja >}}
---
# override color
color_main  : indigo
blog_class  : text-center
---
{{< / highlight >}}

* Blog: extend Single Column

{{< highlight jinja >}}
---
# empty, no property override
---
{{< / highlight >}}

* Tags, Categories: extend Single Column

{{< highlight jinja >}}
---
# empty, no property override
---
{{< / highlight >}}

* By Year, By Month: extend Single Column

{{< highlight jinja >}}
---
# empty, no property override
---
{{< / highlight >}}

#### Overriding Color

For both content and layout,
just set in frontmatter as below example:

{{< highlight jinja >}}
---
...

# override color
color_main  : teal
color_aside : cyan
---
{{< / highlight >}}

-- -- --

### What is Next?

Consider continue reading [ [Jekyll Bootstrap - Layout - Index List][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/07/07/jekyll-bsoc-layout-index-list/
[tutor-html-master-10]: {{< tutor-jekyll-bsoc >}}/tutor-10/

[tutor-vl-default]:     {{< tutor-jekyll-bsoc >}}/tutor-10/_layouts/default.html
[tutor-vl-page]:        {{< tutor-jekyll-bsoc >}}/tutor-10/_layouts/page.html
[tutor-vl-post]:        {{< tutor-jekyll-bsoc >}}/tutor-10/_layouts/post.html
[tutor-vl-double]:      {{< tutor-jekyll-bsoc >}}/tutor-10/_layouts/columns-double.html
[tutor-vl-single]:      {{< tutor-jekyll-bsoc >}}/tutor-10/_layouts/column-single.html
[tutor-vl-home]:        {{< tutor-jekyll-bsoc >}}/tutor-10/_layouts/home.html

[tutor-vl-blog]:        {{< tutor-jekyll-bsoc >}}/tutor-10/_layouts/blog.html
[tutor-vl-tags]:        {{< tutor-jekyll-bsoc >}}/tutor-10/_layouts/list-tag.html
[tutor-vl-categories]:  {{< tutor-jekyll-bsoc >}}/tutor-10/_layouts/list-category.html
[tutor-vl-by-year]:     {{< tutor-jekyll-bsoc >}}/tutor-10/_layouts/by-year.html
[tutor-vl-by-month]:    {{< tutor-jekyll-bsoc >}}/tutor-10/_layouts/by-month.html

[tutor-vpa-blog-header]:{{< tutor-jekyll-bsoc >}}/tutor-10/_includes/page/blog-header.html
[tutor-vpo-blog-header]:{{< tutor-jekyll-bsoc >}}/tutor-10/_includes/post/blog-header.html

[tutor-vc-index]:       {{< tutor-jekyll-bsoc >}}/tutor-10/index.html
[tutor-pa-blog]:        {{< tutor-jekyll-bsoc >}}/tutor-10/pages/index.html
[tutor-pa-tags]:        {{< tutor-jekyll-bsoc >}}/tutor-10/pages/tags.html
[tutor-pa-categories]:  {{< tutor-jekyll-bsoc >}}/tutor-10/pages/categories.html
[tutor-pa-by-year]:     {{< tutor-jekyll-bsoc >}}/tutor-10/pages/by-year.html
[tutor-pa-by-month]:    {{< tutor-jekyll-bsoc >}}/tutor-10/pages/by-month.html
[tutor-vc-po-maguire]:  {{< tutor-jekyll-bsoc >}}/tutor-10/_post/quotes/2015-01-01-jerry-maguire.md

[image-nerdtree-layout]:{{< assets-ssg >}}/2020/07/bootstrap/10-nerdtree-layouts.png
[image-nerdtree-pages]: {{< assets-ssg >}}/2020/07/bootstrap/10-nerdtree-pages.png
[image-vc-po-maguire]:  {{< assets-ssg >}}/2020/07/bootstrap/10-post-maguire.png
[image-color-inherit]:  {{< assets-ssg >}}/2020/07/bootstrap/10-color-inheritance.png
