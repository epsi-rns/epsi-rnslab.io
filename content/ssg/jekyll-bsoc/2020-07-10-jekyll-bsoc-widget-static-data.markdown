---
type   : post
title  : "Jekyll Bootstrap - Widget - Data"
date   : 2020-07-10T09:17:35+07:00
slug   : jekyll-bsoc-widget-static-data
categories: [ssg]
tags      : [jekyll, bootstrap]
keywords  : [liquid, widget, related posts, friends, backlink]
author : epsi
opengraph:
  image: assets-ssg/2020/07/bootstrap/11-widget-friends.png

toc    : "toc-2020-06-jekyll-step"

excerpt:
  Building Jekyll Site Step by step, using Bootstrap OC as stylesheet.
  Using static data feature.
---

### Preface

> Goal: Using static data feature.

#### Source Code

This article use [tutor-11][tutor-html-master-11] theme.
We will create it step by step.

-- -- --

### 8: Friends

As every SSG does, `jekyll` also has feature to access static data.

#### Data: YAML: Friends

I always like to give a link to my fellow blogger folks.
So I made a special data for these great people.

* [gitlab.com/.../_data/friends.yml][tutor-data-friends]

{{< highlight yaml >}}
# Helper for friends widget

- title: "BanditHijo (R)-Chive"
  url: https://bandithijo.github.io/

- title: "Elianiva"
  url: https://elianiva.github.io/

- title: "Azzam Syawqi Aziz"
  url: https://azzamsa.com/
  
- title: "Muhammad Fahri"
  url: https://muhammadfahri.now.sh/

- title: "Sira Argia (Aflasio)"
  url: https://aflasio.netlify.app/

...
{{< / highlight >}}

#### Liquid: Shuffle

I need to show five bloggers randomly for each pages.
We already have `limit` filter in previous article.
Now we require to add a new `shuffle` filter in main configuration.
Since `liquid` does not have a built in function for`shuffle`.
We need to have additional fake randomize:

After duckduckwent fo a while,
I got this shuffle code from stackoverflow:

* [Generate random numbers using liquid (shopify)][randomly-shuffle]

{{< highlight jinja >}}
  {% assign length = 5 %}
  {% assign size_adjusted = site.data.friends | size | minus: length %}
  {% assign start = page.date | date: "%d" | modulo: size_adjusted
{{< / highlight >}}

Note that, you can also use plugin instead.

#### Partial Widget: Liquid: Friends

* [gitlab.com/.../_includes/widget/friends.html][tutor-vw-friends]

{{< highlight jinja >}}
{% comment %}
fake randomize by using date day for array under 30 data
{% endcomment %}

{% capture widget_header %}
  <strong>Isle of Dotsites</strong>
  <i data-feather="users" class="float-right"></i>
{% endcapture %}

{% capture widget_body %}
  {% assign length = 5 %}
  {% assign size_adjusted = site.data.friends | size | minus: length %}
  {% assign start = page.date | date: "%d" | modulo: size_adjusted %}

  <ul class="widget-list">
    {% assign selection = site.data.friends | slice: start, length %}
    {% for friend in selection %}
    <li><a href="{{ friend.url }}">{{ friend.title }}</a></li>
    {% endfor %}
  </ul>
{% endcapture %}

{% assign color = "pink" %}
{% include template/widget.html %}
{{< / highlight >}}

#### Data Loop

Using data is straightforward

{{< highlight jinja >}}
    {% assign selection = site.data.friends %}
    {% for friend in selection %}
    <li><a href="{{ friend.url }}">{{ friend.title }}</a></li>
    {% endfor %}
{{< / highlight >}}

#### Filter: Shuffle

If you can afford plugin in your CI/CD,
you can utilize ruby `shuffle` instead in a filter.

* [How do I shuffle the order of an array in Jekyll?][randomly-shuffle]

{{< highlight ruby >}}
module Jekyll
  module ShuffleFilter
    def shuffle(array)
      array.shuffle
    end
  end
end

Liquid::Template.register_filter(Jekyll::ShuffleFilter)
{{< / highlight >}}

Now the loop become short and simple.

{{< highlight jinja >}}
{% capture widget_body %}
  <ul class="widget-list">
    {% assign selection = site.data.friends | shuffle %}
    {% for friend in selection limit:5 %}
    <li><a href="{{ friend.url }}">{{ friend.title }}</a></li>
    {% endfor %}
  </ul>
{% endcapture %}
{{< / highlight >}}

#### Render: Browser

You can open `page` kind, or `post` kind, to test this `widget`.

![Jekyll: Widget: Friends][image-wi-friends]

-- -- --

### 9: Backlink: External Archive

I have four blogs, each already have generated archive data.
So it makes sense that I put backlinks from my own static blog,
in each post.

#### Data: YAML: Github Archive

My generated data from my Jekyll site looks like below:

* [gitlab.com/.../_data/archive-github.yml][tutor-data-ar-github]

{{< highlight yaml >}}
# Helper for related links

- id: 20020225
  title: "Presentation - Learning Linux Diversity"
  url: /system/2020/02/02/presentation-linux-diversity.html

- id: 20012025
  title: "Presentation - Desktop Customization"
  url: /desktop/2020/01/20/presentation-desktop-customization.html

- id: 19120325
  title: "Awesome WM - Presentation - Statusbar"
  url: /desktop/2019/12/03/awesome-presentation-statusbar.html

...
{{< / highlight >}}

I put this in my jekyll static data directory manually.

#### Data: YAML: Gitlab Archive

And, my generated data from my Hugo site looks like below:

* [gitlab.com/.../_data/archive-github.yml][tutor-data-ar-github]

{{< highlight yaml >}}
# Helper for related links
# Archives by Date

- id: 20051517
  title: "Template - Koa"
  url: /frontend/2020/05/15/template-koa/
  
- id: 20051317
  title: "Template - Express - Handlebars"
  url: /frontend/2020/05/13/template-express-handlebars/
  
- id: 20051217
  title: "Template - Express - EJS"
  url: /frontend/2020/05/12/template-express-ejs/
{{< / highlight >}}

From my Hugo site:

* <http://localhost:1313/pages/archives/index.txt>

I also put this in my `jekyll` static data directory manually.

#### Partial Widget: Liquid: Github Archive

I also use fake randomize.

* [gitlab.com/.../_includes/widget/archive-github.html][tutor-data-ar-github]

{{< highlight jinja >}}
{% comment %}
fake randomize by using year day for array under 365 data
{% endcomment %}

{% capture widget_header %}
  <strong>Web/Mobile Development</strong>
  <i data-feather="external-link" class="float-right"></i>
{% endcapture %}

{% capture widget_body %}
  {% assign length = 5 %}
  {% assign archives = site.data.archives_gitlab %}
  {% assign size_adjusted = archives | size | minus: length %}
  {% assign start = page.date | date: "%j" | modulo: size_adjusted %}

  <ul class="widget-list">
    {% assign selection = archives | slice: start, length %}
    {% for archive in selection %}
    <li><a href="https://epsi-rns.gitlab.io{{ archive.url }}"
      >{{ archive.title }}</a></li>
    {% endfor %}
  </ul>
{% endcapture %}

{% assign color = "lime" %}
{% include template/widget.html %}
{{< / highlight >}}

#### Filter: Shuffle

If you can afford plugin in your CI/CD,
you can utilize ruby `shuffle` instead in a filter,
so the loop become short and simple.

{{< highlight jinja >}}
{% capture widget_body %}
  <ul class="widget-list">
    {% assign selection = site.data.archives_github | shuffle %}
    {% for archive in selection limit:5 %}
    <li><a href="https://epsi-rns.github.io{{ archive.url }}"
      >{{ archive.title }}</a></li>
    {% endfor %}
  </ul>
{% endcapture %}
{{< / highlight >}}

#### Render: Browser

You can open `page` kind, or `post` kind, to test this `widget`.

![Jekyll: Widget: Backlinks from Github][image-wi-github]

#### Partial Widget: Liquid: Gitlab Archive

Again, I use fake randomize.

* [gitlab.com/.../_includes/widget/archive-gitlab.html][tutor-data-ar-gitlab]

{{< highlight jinja >}}
{% comment %}
fake randomize by using year day for array under 365 data
{% endcomment %}

{% capture widget_header %}
  <strong>Web/Mobile Development</strong>
  <i data-feather="external-link" class="float-right"></i>
{% endcapture %}

{% capture widget_body %}
  {% assign length = 5 %}
  {% assign archives = site.data.archives_gitlab %}
  {% assign size_adjusted = archives | size | minus: length %}
  {% assign start = page.date | date: "%j" | modulo: size_adjusted %}

  <ul class="widget-list">
    {% assign selection = archives | slice: start, length %}
    {% for archive in selection %}
    <li><a href="https://epsi-rns.gitlab.io{{ archive.url }}"
      >{{ archive.title }}</a></li>
    {% endfor %}
  </ul>
{% endcapture %}

{% assign color = "lime" %}
{% include template/widget.html %}
{{< / highlight >}}

#### Filter: Shuffle

The same method applied here.

#### Render: Browser

You can open `page` kind, or `post` kind, to test this `widget`.

![Jekyll: Widget: Backlinks from Gitlab][image-wi-gitlab]

-- -- --

### 10: Related Posts

#### Custom Output

We have already seen in previous article,
that we can generate our own custom data:

* [Jekyll Plain - Custom Output][local-custom-output]

The generated data from my Jekyll site can be accessed from:

* <http://localhost:4000/pages/archives.yml>

Using previously saved file `_data/archives.yaml`.
Note that this archive saved manually from generated content.
You can just copy the result manually,
from `_site/pages/archives.yml`
to folder `_data/archives.yml`.

#### Data: YAML: Archives

The data looks like below:

* [gitlab.com/.../_data/archives.yml][tutor-archives]

{{< highlight yaml >}}
# Helper for related links

- id: 20031535
  title: "Company of Thieves - Oscar Wilde"
  url: /lyric/2020/03/15/company-of-thieves-oscar-wilde.html

- id: 19071535
  title: "Mothers -  No Crying in Baseball"
  url: /lyric/2019/07/15/mothers-no-crying-in-baseball.html

- id: 19052535
  title: "Brooke Annibale - Yours and Mine"
  url: /lyric/2019/05/25/brooke-annibale-yours-and-mine.html
{{< / highlight >}}

The ID, comes from dates and minutes.
automatic generated by script from previous article.

#### Page Content: Frontmatter

Now we can add variable in page content.
Consider name the variable `related_link_ids`.
This should contain array of `id` in yaml.

* [gitlab.com/.../_posts/lyrics/2019-05-15-brooke-annibale-by-your-side.md][tutor-po-yours-and-mine]

{{< highlight yaml >}}
---
layout      : post
title       : Brooke Annibale - Yours and Mine
date        : 2019-05-25 07:35:05 +0700
categories  : lyric
tags        : [pop, 2010s]
keywords    : [OST, "One Tree Hill"]
author      : Brooke Annibale

related_link_ids :
  - 19051535  # By Your Side
---
...
{{< / highlight >}}

* [gitlab.com/.../_posts/lyrics/2019-05-25-brooke-annibale-yours-and-mine.md][tutor-po-by-your-side]

{{< highlight yaml >}}
---
layout      : post
title       : Brooke Annibale - By Your Side
date        : 2019-05-15 07:35:05 +0700
categories  : lyric
tags        : [pop, 2010s]
keywords    : [OST, "Grey's Anatomy"]
author      : Brooke Annibale

related_link_ids :
  - 19052535  # Yours and Mine
---
...
{{< / highlight >}}

![Jekyll: Widget: Related Links ID][image-related-link-id]

#### Partial Widget: Liquid: Related Posts

Here is how its works.
The widget accessed `page.related_link_ids`.

* [gitlab.com/.../_includes/widget/related-posts.html][tutor-vw-related-posts]

{{< highlight jinja >}}
{% unless page.related_link_ids == nil 
   or page.related_link_ids == empty %}

  {% capture widget_header %}
  <strong>Related Posts</strong>
  <i data-feather="link-2" class="float-right"></i>
  {% endcapture %}

  {% capture widget_body %}
  <ul class="widget-list">
    {% for link_id in page.related_link_ids %}
    <li>
      {% for dpost in site.data.archives %}
      {% if dpost.id == link_id %}
        <a href="{{ site.baseurl }}{{ dpost.url }}"
          >{{ dpost.title }}</a>
      {% endif %}
      {% endfor %}
    </li>
    {% endfor %}
  </ul>
  {% endcapture %}

  {% assign color = "lime" %}
  {% include template/widget.html %}
{% endunless %}
{{< / highlight >}}

I know it is a little bit complex.
And lately, I rarely use this `related_link_ids` feature myself.
But I still keep this feature for a few of my posts.

#### Render: Browser

You can open `post` kind, to test this `widget`.

![Jekyll: Widget: Related Posts][image-wi-related-post]

You can have as many related posts as you need.

-- -- --

### 11: Summary

As a summary here is the `aside` partial for `post` layout.

#### Partial: Post Aside

* [gitlab.com/.../_includes/post/aside-content.html][tutor-vpo-aside-content]

{{< highlight jinja >}}
{% include widget/archive.html %}
{% include widget/related-posts.html %}
{% include widget/tags.html %}

{% comment %}
  {% include widget/friends.html %}
  {% include widget/archives-gitlab.html %}
  {% include widget/archives-github.html %}

  {% include widget/categories.html %}
  {% include widget/tags.html %}

  {% include widget/recent-posts.html %}
  {% include widget/related-posts.html %}

  {% include widget/affiliates.html %}
  {% include widget/oto-spies.html %}
{% endcomment %}
{{< / highlight >}}

You can add or remove widget, as you want them to be.

#### Render: Browser

The looks in desktop screen is shown as below figure:

![Jekyll: Widgets in Desktop][image-widgets-desktop]

-- -- --

### What is Next?

Consider continue reading [ [Jekyll Bootstrap - Pagination - Stylesheet][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/07/12/jekyll-bsoc-pagination-stylesheet/
[local-custom-output]:  {{< baseurl >}}ssg/2020/06/07/jekyll-plain-custom-output/
[randomly-shuffle]:     https://www.131-studio.com/blogs/shopify-conversion/generate-random-numbers-using-liquid-shopify
[filter-shuffle]:       https://stackoverflow.com/questions/27179385/how-do-i-shuffle-the-order-of-an-array-in-jekyll
[tutor-html-master-11]: {{< tutor-jekyll-bsoc >}}/tutor-11/

[tutor-data-friends]:   {{< tutor-jekyll-bsoc >}}/tutor-11/_data/friends.yml
[tutor-archives]:       {{< tutor-jekyll-bsoc >}}/tutor-11/_data/archives.yml
[tutor-data-ar-github]: {{< tutor-jekyll-bsoc >}}/tutor-11/_data/archive-github.yml
[tutor-data-ar-gitlab]: {{< tutor-jekyll-bsoc >}}/tutor-11/_data/archive-gitlab.yml

[tutor-vpa-aside-content]:  {{< tutor-jekyll-bsoc >}}/tutor-11/_includes/page/aside-content.html
[tutor-vpo-aside-content]:  {{< tutor-jekyll-bsoc >}}/tutor-11/_includes/post/aside-content.html
[tutor-vw-arch-month]:      {{< tutor-jekyll-bsoc >}}/tutor-11/_includes/widget/archive-month.html
[tutor-vw-friends]:         {{< tutor-jekyll-bsoc >}}/tutor-11/_includes/widget/friends.html
[tutor-vw-related-posts]:   {{< tutor-jekyll-bsoc >}}/tutor-11/_includes/widget/related-posts.html
[tutor-po-by-your-side]:    {{< tutor-jekyll-bsoc >}}/tutor-11/_posts/lyrics/2019-05-15-brooke-annibale-by-your-side.md
[tutor-po-yours-and-mine]:  {{< tutor-jekyll-bsoc >}}/tutor-11/_posts/lyrics/2019-05-25-brooke-annibale-yours-and-mine.md

[image-wi-friends]:     {{< assets-ssg >}}/2020/07/bootstrap/11-widget-friends.png
[image-wi-github]:      {{< assets-ssg >}}/2020/07/bootstrap/11-widget-archive-github.png
[image-wi-gitlab]:      {{< assets-ssg >}}/2020/07/bootstrap/11-widget-archive-gitlab.png
[image-wi-related-post]:{{< assets-ssg >}}/2020/07/bootstrap/11-widget-related-posts.png
[image-widgets-desktop]:{{< assets-ssg >}}/2020/07/bootstrap/11-widgets-in-desktop.png
[image-related-link-id]:{{< assets-ssg >}}/2020/07/bootstrap/11-related-links-id.png
