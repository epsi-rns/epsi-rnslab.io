---
type   : post
title  : "Jekyll Bootstrap - Content"
date   : 2020-07-15T09:17:35+07:00
slug   : jekyll-bsoc-content
categories: [ssg]
tags      : [jekyll, bootstrap]
keywords  : [markdown, shortcode, syntax highlighting, mathjax]
author : epsi
opengraph:
  image: assets-ssg/2020/07/bootstrap/11-blog-post-crop.png

toc    : "toc-2020-06-jekyll-step"

excerpt:
  Building Jekyll Site Step by step, using Bootstrap OC as stylesheet.
  More about blog post content, Header, Footer, and Navigation.
---

### Preface

> Goal: More about blog post content, Header, Footer, and Navigation.

#### Source Code

This article use [tutor-11][tutor-html-master-11] theme.
We will create it step by step.

-- -- --

### 1: Markdown

There is not much to say about markdown.
Most topic have already been discussed in previous article:

* [Jekyll Plain - Markdown][jekyll-plain-markdown]

We just need a few stylesheet adjustment.

#### SASS

* [gitlab.com/.../_sass/css/post/_content.scss][tutor-sass-content]

My complete `sass` is as below:

{{< highlight scss >}}
h1, h2, h3, h4, h5, h6 {
  font-family: "Playfair Display", Georgia, "Times New Roman", serif;
}

.blog-body {
  margin-bottom: 1rem;
}
.blog-header {
  h1, h2, h3, h4, h5, h6 {
    margin-bottom: .25rem;
    a {
      color: map-get($oc-gray-list, "9");
    }
    a:hover {
      color: map-get($oc-gray-list, "6") !important;
    }
  }
}
.blog-header .meta {
  margin-bottom: 2.0rem;
}
.blog-body p {
  margin-top: 0.5rem;
  margin-bottom: 0.5rem;
}
{{< / highlight >}}

Space between paragraph, should be distinct, with space between lines.
Without stylesheet above, you should still be fine.

![Jekyll: Markdown: SASS Space][image-sass-space]

#### Shortcodes

Already discussed in previous artcle.
Consider see how it looks with frame.

![Jekyll: Shortcodes: Commercial][image-shortcodes]

-- -- --

### 2: Syntax Highlighting

There are at least two way to highlight in Jekyll.
Both should show similar result.

#### Triple Backtick

{{< highlight haskell >}}
```haskell
-- Layout Hook
commonLayout = renamed [Replace "common"]
    $ avoidStruts 
    $ gaps [(U,5), (D,5)] 
    $ spacing 10
    $ Tall 1 (5/100) (1/3)
```
{{< / highlight >}}

#### Highlight Tag Directive

{{< highlight haskell >}}
{% highlight haskell %}
-- Layout Hook
commonLayout = renamed [Replace "common"]
    $ avoidStruts 
    $ gaps [(U,5), (D,5)] 
    $ spacing 10
    $ Tall 1 (5/100) (1/3)
{% endhighlight %}
{{< / highlight >}}

#### Configuration

To enable this you should configure `.eleventy.js` first.

* [gitlab.com/.../_config.yaml][tutor-configuration]

{{< highlight yaml >}}
# Build settings
markdown: kramdown
highlighter: rouge
{{< / highlight >}}

You can also use `pygments` instead.

#### Official Documentation

* [rouge-ruby.github.io/docs](https://rouge-ruby.github.io/docs/)

#### SASS

I provide two SASS

* [Bright Highlighter][tutor-sass-bright]

* [Dark Highlighter][tutor-sass-dark]

I do not own the source code.
I grab them from somewhere.

#### Render: Browser

Now you can see have your expected result in the browser.

![Jekyll Syntax Highlighting: Formatting Fixed][image-highlighter]

-- -- --

### 3: MathJax

MathJax is a huge topic that deserve their own article series.
But using MathJax is simple and straight away.

This Mathjax can be in both `markdown` and `html`.
So it is not specifically designed for markdown only content.

I have plan to use LaTeX and MathJax extensively.
so I put this MathJax setting in this chapter.
Just in case I forget,
I can find my own example easily.

#### Partial: Liquid: Scripts

MathJax can be use directly by using setting up this partial below:

{{< highlight jinja >}}
  {% if page.mathjax %}
    {% include site/mathjax.html %}
  {% endif %}
{{< / highlight >}}

Or in a complete anner, as below code:

* [gitlab.com/.../_includes/scripts.html][tutor-vi-scripts]

{{< highlight jinja >}}
  {% comment %}
  JavaScript at end of body for optimized loading
  {% endcomment %}

  {% if page.mathjax %}
    {% include site/mathjax.html %}
  {% endif %}

  {% if page.customjs %}
    {% for js in page.customjs %}
      <script src="{{ site.baseurl }}{{ js }}"></script>
    {% endfor %}
  {% endif %}

  <script>
    feather.replace()
  </script>
{{< / highlight >}}

This means you have to set the frontmatter for using MathJax.

{{< highlight yaml >}}
mathjax     : true
{{< / highlight >}}

#### Page Content: Post

Again, content for example purpose:

* [gitlab.com/.../_post/lyrics/2017-03-25-nicole-atkins-a-night-of-serious-drinking.md][tutor-vc-po-atkins]

{{< highlight latex >}}
---
layout      : post
title       : Nicole Atkins - A Night of Serious Drinking
...
mathjax     : true
---

On a jet plane sailing through the night  
I find I’m thinking of you  
9 years ago was just like yesterday

$$ \lim\limits_{x \to \infty} \exp(-x) = 0 $$

On a jet plane sailing through the night  
I guess I’m wasting precious time  
Seven years ago you told me you had plans to go

$$ \int_0^\infty \mathrm{e}^{-x}\,\mathrm{d}x $$

This jet plane lands silently still  
Can’t believe I’ll be there soon  
Three years ago felt like a lifetime
{{< / highlight >}}

![Jekyll Markdown: MathJax][image-mathjax]

-- -- --

### What is Next?

Consider continue reading [ [Jekyll - Summary][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/08/20/jekyll-summary/
[tutor-html-master-12]: {{< tutor-jekyll-bsoc >}}/tutor-12/

[javascript-toggler]:   {{< baseurl >}}frontend/2020/03/16/bootstrap-oc-javascript-toggler/
[jekyll-plain-markdown]:{{< baseurl >}}ssg/2020/06/08/jekyll-plain-markdown/

[tutor-configuration]:  {{< tutor-jekyll-bsoc >}}/tutor-11/_config.yml
[tutor-sass-content]:   {{< tutor-jekyll-bsoc >}}/tutor-11/_sass/css/post/_content.scss
[tutor-sass-bright]:    {{< tutor-jekyll-bsoc >}}/tutor-11/_sass/css/post/_syntax-highlighting-bright.scss
[tutor-sass-dark]:      {{< tutor-jekyll-bsoc >}}/tutor-11/_sass/css/post/_syntax-highlighting-dark.scss
[tutor-vi-scripts]:     {{< tutor-jekyll-bsoc >}}/tutor-11/_includes/site/scripts.html
[tutor-vc-po-atkins]:   {{< tutor-jekyll-bsoc >}}/tutor-11/_post/lyrics/2017-03-25-nicole-atkins-a-night-of-serious-drinking.md

[image-sass-space]:     {{< assets-ssg >}}/2020/07/bootstrap/12-sass-space.png
[image-shortcodes]:     {{< assets-ssg >}}/2020/07/bootstrap/12-commercial.png
[image-highlighter]:    {{< assets-ssg >}}/2020/07/bootstrap/12-highlighter.png
[image-mathjax]:        {{< assets-ssg >}}/2020/07/bootstrap/12-mathjax.png
