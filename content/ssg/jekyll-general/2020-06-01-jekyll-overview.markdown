---
type   : post
title  : "Jekyll - Overview"
date   : 2020-06-01T09:17:35+07:00
slug   : jekyll-overview
categories: [ssg]
tags      : [jekyll]
keywords  : [static site, custom theme, summary, overview]
author : epsi
opengraph:
  image: assets-ssg/2020/06/jekyll-bulma-md-preview.png

toc    : "toc-2020-06-jekyll-step"

excerpt:
  Building Jekyll Site Step by step,
  from plain without stylesheet,
  followed by Bulma and Bootstrap showcase.

---

### Preface

> Goal: Explanation of Jekyll Repository Step By Step

This tutorial consist of two stage, but in three parts.
The second stage is a choice between Bulma and Bootstrap.

### Jekyll Step By Step

This Tutorial is made in two stages.

1. Jekyll in plain HTML:
   covering liquid, avoiding burden of any stylesheet

2. Jekyll with CSS Frameworks:
   with two offered choices, either Bulma or Bootstrap.

#### Stage 1: Plain

* [Jekyll Step by Step Repository][tutorial-jekyll-plain]

Contain Tutor-01 until Tutor-05.
With additional Tutor-06 to create plugins,
and Tutor-07 to create basic theme in Gem.

#### Stage 2: Bootstrap OC

> This repository:

* [Jekyll (Bootstrap OC)][tutorial-jekyll-boc]

Each contain Tutor-08 until Tutor-12.
With additional Tutor-13
as a complete Bootstrap site with stylesheet and plugins,
and Tutor-14 as complete theme in Gem.

#### Stage 2: Bulma MD

Alternative implementation choice.

* [Jekyll (Bulma MD)][tutorial-jekyll-bmd]

Each contain Tutor-08 until Tutor-12.
With additional Tutor-12 and Tutor-13.

[tutorial-jekyll-bmd]:  https://gitlab.com/epsi-rns/tutor-jekyll-bulma-md/
[tutorial-jekyll-boc]:  https://gitlab.com/epsi-rns/tutor-jekyll-bootstrap-oc/
[tutorial-jekyll-plain]:https://gitlab.com/epsi-rns/tutor-jekyll-plain/

-- -- --

### A Journey with SSG

> The need to move on

On december 2019 I finally decided to setup my fourth blog.
Now I have four blog, each with different static site generator.

1. on github: still with Jekyll

2. on gitlab: Hugo

3. on netlify using github: Hexo

4. on bitbucket: Pelican

I have already made two article series:
Hugo, then Hugo again, then Hexo, and finally 35 articles of Eleventy.
Bu I never have a chance to write a complete Jeykll article series.

I eager to learn something new such as:
CSS without framework, Tailwind CSS, Gatsby, and also RedwoodJS.
Before I move on to this new knowledge frontier,
I need to share my old knowledge to article series.

![What is SSG Good for?][what-ssg-good-for]

#### A Journey with Jekyll

> My first SSG, and I'm still using Jekyll

Jekyll is slow, but it is very suitable for beginner.
Especially for github user.
SSG user have to deal with repository,
and github is usually their very first touch.
So yeah, Jeykll and Github, is the right step for beginner.

However, here a I am, for you folks,
I can't wait to write down,
my eleventy experience,
as an article series.

-- -- --

### About Jekyll

#### Official Site.

* [jekyllrb.com](https://jekyllrb.com/)

#### Why Static Site Generator?

No need to go deep learning backend.
So you can mock-up your theme quickly.

#### Why Markdown?

Most SSG is using markdown, instead of HTML.
Markdown reduce the complexity of formatting.
Reduce time wasted on formatting.
Hence concentrate more on content.

#### Why Jekyll?

> I do not know.

Jekyll is my first SSG, and I am cool with that.

#### Why Liquid?

> Liquid is the only templating engine in Jekyll.

If you feel that Liquid is not enough,
you can write Jekyll Plugin in Ruby.

#### The Downside of Jekyll

> Jekyll 3.x is slow.

Jekyll is not that sluggish, if you use `incremental` build.
It is slow at start, but faster while editing markdown.
This `incremental` feature, is considered experimental.
This won't update any theme changes.

If you have a hundreds pages and desire speed,
you better use `incremental` build.
For small website, Jekyll is still very suitable.

And don't worry, Jekyll 4.x is faster.

> Github Support?

By the time this article is written,
Jekyll 4.x has no been supported as standard github build.
You have to use either Travis, or Circle CI.
Or even use Gitlab instead of Github.
Or you may consider using netlify or vercel.

#### Demo Jekyll

Just visit my site:

* [epsi-rns.github.io](https://epsi-rns.github.io/)

-- -- --

### Preview

For your appetite, I present you preview of each stages:

#### Stage 1: Plain

![Jekyll Plain Preview][jekyll-plain-preview]

#### Stage 2: Bootstrap OC

![Jekyll Bootstrap Preview][jekyll-bootstrap-preview]

#### Stage 2: Bulma MD

![Jekyll Bulma Preview][jekyll-bulma-preview]

[jekyll-plain-preview]:     {{< assets-ssg >}}/2020/06/jekyll-plain-preview.png
[jekyll-bulma-preview]:     {{< assets-ssg >}}/2020/06/jekyll-bulma-md-preview.png
[jekyll-bootstrap-preview]: {{< assets-ssg >}}/2020/06/jekyll-bootstrap-oc-preview.png

-- -- --

### The Article Series

#### Making a Theme

Supposed you want to make your own theme.
What would you do first?

* Step One: **Start From the Layout**:
  Although you can create the layout with pure HTML.
  Static site generator can make this job easier.

* Step Two: **Custom Pages**:
  You need to populate with content example.
  From post and page, to archive and taxonomy.
  This require a lot of loop, that we can achieve using Liquid.
  Also adding feature, such as meta SEO, or creating plugin.

* Step Three: **Adding Stylesheet**:
  General design require Sass, for nice looks.
  And carefully crafted responsive layout.
  And Sass for finishing touch.
  Each chapter will modify **layout**.

* Step Four: **Additional Feature**
  Such as static data, widget, or making pagination.

* Step Five: **Page Content**:
  Adding layout such as page header, footer, and navigation.
  Also adding feature, such as commenting system.
  Then a few Sass adjustment to handle rendered markdown content in post.
  For coder, you need to test syntax highlighting.
  As a bonus tips, math lover should embrace MathJax.

* Additionaly: **Deploy**:
  This is a huge topic that deserve their own article series.
  Deploy is discussed in separated article along with other SSG.
  But I also give an introduction articles.

These article series cover most of those five main topics above.
The stylesheet design covered in different article series.
And additional stuff as beyond making site,
such as deployment covered in different article series.

#### Expectation

> No. Not at all.

* **Case can be applied somewhere else**.
  I do not expect most coder to use Jekyll.
  In fact, I love diversity, and static site world support diversities.
  I provide this eleventy case, just for an example.
  After this you might want to apply methods covered here,
  somewhere else with different static site generator.

* **Beginner have an example case**.
  With this case, at least beginner can make their own blog easier.
  For either portfolio, or maybe personal blog,
  or even making company profile for family business.

* **Starting point for a serious writer**.
  I also do not want to go political,
  that every coder should share their knowledge,
  and pour code into blog.
  I just feel that maybe there is someone out there,
  need to learn to share something,
  and need to start with blogging or youtubing.
  Static site generator is,
  a starting point for serious writer.
  After all, making a blog site is easy and fun.
  
#### Theming Skill

> Should I make custom theme?

Even when you are using a ready to use theme,
you might still need customization to suit your needs.
This adjustment is a reccuring issue that arise regularly.

I do not ask for every reader to be capable of making custom theme,
nor encourage people to avoid third party theme.
Let the design part be somebody else's job.

This article series give you understanding,
on how to modify existing theme,
to alter a bit to match your requirement.

#### What is Not?

This is yet, just another static site tutorial, for beginner.
Nothing super duper here. No database, no login, no API, no animation.

After writing four hundreds of basic articles for,
now I feels like __mediocre specialist__.
I have to move on someday soon.

#### Disclaimer

This is would not be the best blog template that you ever have.
Because I only put most common stuff,
to keep the tutorial simple.

After you learn this guidance,
you understand the fundamental skill.
Thus, a base for you, to make your own blog site.
With your imagination, you may continue,
to build your own super duper `Jekyll` site.
Far better than, what I have achieved.

#### Content

A site need content.
I do not want __lorem ipsum__.

To make this step by step tutorial alive,
I choose most common topic, quote from movies.

I put some lyrics from great beautiful songs.
I'm not sure exactly what these songs means.
I just find it easier to use this kind of content.

Without the content, you can still make your site anyway.
You do not really need to understand the story behind the content.

#### Table of Content

The table content is available as header on each tutorial.
There, I present a Hexo Tutorial, step by step, for beginners.

-- -- --

### Additional Links

#### HTML Step By Step

Additional guidance:

* [Bulma Material Design Step by Step Repository][tutorial-bulma-md] (frankenbulma)

* [Bulma Step by Step Repository][tutorial-bulma]

* [Materialize Step by Step Repository][tutorial-materialize]

#### Comparison

Comparation with other static site generator

* [Eleventy (Materialize) Step by Step Repository][tutorial-11ty-m]

* [Eleventy (Bulma MD) Step by Step Repository][tutorial-11ty-b]

* [Hugo Step by Step Repository][tutorial-hugo]

* [Hexo Step by Step Repository][tutorial-hexo]

* [Pelican Step by Step Repository][tutorial-pelican]

#### Presentation

* [Concept SSG - Presentation Slide][ssg-presentation]

* [Concept CSS - Presentation Slide][css-presentation]

[tutorial-pelican]:     https://gitlab.com/epsi-rns/tutor-pelican-bulma-md/
[tutorial-hugo]:        https://gitlab.com/epsi-rns/tutor-hugo-bulma-md/
[tutorial-11ty-m]:      https://gitlab.com/epsi-rns/tutor-11ty-materialize/
[tutorial-11ty-b]:      https://gitlab.com/epsi-rns/tutor-11ty-bulma-md/
[tutorial-hexo]:        https://gitlab.com/epsi-rns/tutor-hexo-bulma/

[tutorial-bulma-md]:    https://gitlab.com/epsi-rns/tutor-html-bulma-md/
[tutorial-bulma]:       https://gitlab.com/epsi-rns/tutor-html-bulma/
[tutorial-materialize]: https://gitlab.com/epsi-rns/tutor-html-materialize/

[ssg-presentation]:     https://epsi-rns.gitlab.io/ssg/2019/02/17/concept-ssg/
[css-presentation]:     https://epsi-rns.gitlab.io/frontend/2019/02/15/concept-css/

-- -- --

### Begin The Guidance

Let's get the tutorial started.
Long and short of it, this is the tale.

Consider continue reading [ [Jekyll - Install using RVM][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/06/02/jekyll-install-rvm/

[what-ssg-good-for]:{{< assets-ssg >}}/2020/what-ssg-good-for.png
