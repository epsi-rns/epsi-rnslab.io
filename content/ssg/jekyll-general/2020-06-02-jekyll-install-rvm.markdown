---
type   : post
title  : "Jekyll - Install using RVM"
date   : 2020-06-02T09:17:35+07:00
slug   : jekyll-install-rvm
categories: [ssg]
tags      : [jekyll]
keywords  : [Gemfile, Bundle, RVM, Ruby Version Manager]
author : epsi
opengraph:
  image: assets-ssg/2020/06/00-rvm-gpg-keys.png

toc    : "toc-2020-06-jekyll-step"

excerpt:
  Building Jekyll Site Step by step,
  Install Jekyll on localhost,
  using Ruby Version Manager (RVM).

---

### Preface

> Goal: Time to embrace the CLI webtools

Installing Jekyll should be easy and straight away,
until have to deal with different version of Jekyll,
so you have to use `bundle`.
Or worst, you might need diferrent ruby at once.
This can be managed using RVM

-- -- --

### Jekyll

I won't go much further with typical installment,
as it is already covered well in official documentation.

#### Why Another Tutorial?

> You can skip this article.

I suggest you have a fast reading for this RVM article,
before you dive in to this long `jekyll` article series.
Using RVM is more like a choice,
and this article below is more like an overview,
so you can use different Jekyll at once in your system.

I utilize screenshot so I can help people.
For each step, `bundle` or `RVM`, I can give screenshot.
So the people get the idea, of what to expect,
when issuing command on terminal shell.

If you think, that you do not currently require RVM,
you can skip this article, and go straight to Jekyll configuration,
in the next article.

#### Official Documentation

The complete guidance in official site is in this link below:
Beginner can use this method, and just run Jekyll.

* [jekyllrb.com/docs/installation](https://jekyllrb.com/docs/installation/)

#### Windows Users

Windows folks can also install Jekyll.

* [jekyllrb.com/docs/installation/windows](https://jekyllrb.com/docs/installation/windows/)

It is basically a guidance to use `rubyinstaller`.

* [rubyinstaller.org](https://rubyinstaller.org/)

#### Checking version

You can check by running this command.

{{< highlight ruby >}}
$ jekyll -v
jekyll 4.1.1

$ gem -v
2.7.10

$ ruby -v
ruby 2.4.4p296 (2018-03-28 revision 63013) [x86_64-linux]

$ rvm -v
rvm 1.29.4 (latest) by Michal Papis, Piotr Kuczynski, Wayne E. Seguin [https://rvm.io]
{{< / highlight >}}

![Jekyll Install: Checking Version][image-version]

#### Issue

You can use any jekyll version that you want.
Now here is the issue, github only use jekyll 3.8.7 version.

* [pages.github.com/versions](https://pages.github.com/versions/)

The Jekyll version is locked. So you must adapt your localhost.

-- -- --

### Bundle

> Jekyll, Gemfile, Bundle, RVM, Ruby?

Jekyll dependencies managed in Gemfile by bundle.
This way, you can choose which version of Jekyll that you need.
This bundle lock the jekyll version, 
along with all of its dependencies.

* [bundler.io](https://bundler.io/)

#### Bundle vs RVM

> Each project has different package dependency.

With bundle you can choose between package, for example

1. Jekyll 3.8.7, or

2. Jekyll 4.1.1

While with RVM you can choose between ruby, for example

1. Ruby 2.4.9, or

2. Ruby 2.6.5

#### Gem Install Bundler

You need to install bundler first.

{{< highlight bash >}}
$ gem install bundler
Fetching: bundler-2.1.4.gem (100%)
Successfully installed bundler-2.1.4
Parsing documentation for bundler-2.1.4
Installing ri documentation for bundler-2.1.4
Done installing documentation for bundler after 10 seconds
1 gem installed
{{< / highlight >}}

![Jekyll Install: gem install bundler][image-gem-bundler]

#### Gemfile

> Gemfile is not mandatory in github pages

A typical Jekyll `Gemfile`would looks like this.

{{< highlight ruby >}}
source "https://rubygems.org"
gem "jekyll", "~> 4.1.1"

gem "minima", "~> 2.0"

group :jekyll_plugins do
  gem "jekyll-paginate"
end
{{< / highlight >}}

![Jekyll Install: Bundle: Gemfile][image-bundle-gemfile]

You can run Jekyll without `Gemfile` by manually install dependency.

#### Bundle Install

Before running Jekyll,
You should run `bundle install`.
This will process the `Gemfile` above.

{{< highlight bash >}}
$ bundle install
Using public_suffix 4.0.5
Using addressable 2.7.0
Using bundler 2.0.1
Using colorator 1.1.0
Using concurrent-ruby 1.1.6
Using eventmachine 1.2.7
Using http_parser.rb 0.6.0
Using em-websocket 0.5.1
Using ffi 1.13.1
Using forwardable-extended 2.6.0
Using i18n 1.8.3
Using sassc 2.4.0
Using jekyll-sass-converter 2.1.0
Using rb-fsevent 0.10.4
Using rb-inotify 0.10.1
Using listen 3.2.1
Using jekyll-watch 2.2.1
Using rexml 3.2.4
Using kramdown 2.3.0
Using kramdown-parser-gfm 1.1.0
Using liquid 4.0.3
Using mercenary 0.4.0
Using pathutil 0.16.2
Using rouge 3.20.0
Using safe_yaml 1.0.5
Using unicode-display_width 1.7.0
Using terminal-table 1.8.0
Using jekyll 4.1.1
Using jekyll-feed 0.15.0
Using jekyll-paginate 1.1.0
Using jekyll-seo-tag 2.6.1
Using minima 2.5.1
Bundle complete! 3 Gemfile dependencies, 32 gems now installed.
Use `bundle info [gemname]` to see where a bundled gem is installed.
{{< / highlight >}}

![Jekyll Install: bundle install][image-bundle-install]

#### Gemfile Lock

That bundle install above will create a `Gemfile.lock`

{{< highlight bash >}}
$ cat Gemfile.lock
GEM
  remote: https://rubygems.org/
  specs:
    addressable (2.7.0)
      public_suffix (>= 2.0.2, < 5.0)
    colorator (1.1.0)
    concurrent-ruby (1.1.6)
    em-websocket (0.5.1)
      eventmachine (>= 0.12.9)
      http_parser.rb (~> 0.6.0)
    eventmachine (1.2.7)
    ffi (1.13.1)
    forwardable-extended (2.6.0)
    http_parser.rb (0.6.0)
    i18n (1.8.3)
      concurrent-ruby (~> 1.0)
    jekyll (4.1.1)
      addressable (~> 2.4)
      colorator (~> 1.0)
      em-websocket (~> 0.5)
      i18n (~> 1.0)
      jekyll-sass-converter (~> 2.0)
      jekyll-watch (~> 2.0)
      kramdown (~> 2.1)
      kramdown-parser-gfm (~> 1.0)
      liquid (~> 4.0)
      mercenary (~> 0.4.0)
      pathutil (~> 0.9)
      rouge (~> 3.0)
      safe_yaml (~> 1.0)
      terminal-table (~> 1.8)
    jekyll-feed (0.15.0)
      jekyll (>= 3.7, < 5.0)
    jekyll-paginate (1.1.0)
    jekyll-sass-converter (2.1.0)
      sassc (> 2.0.1, < 3.0)
    jekyll-seo-tag (2.6.1)
      jekyll (>= 3.3, < 5.0)
    jekyll-watch (2.2.1)
      listen (~> 3.0)
    kramdown (2.3.0)
      rexml
    kramdown-parser-gfm (1.1.0)
      kramdown (~> 2.0)
    liquid (4.0.3)
    listen (3.2.1)
      rb-fsevent (~> 0.10, >= 0.10.3)
      rb-inotify (~> 0.9, >= 0.9.10)
    mercenary (0.4.0)
    minima (2.5.1)
      jekyll (>= 3.5, < 5.0)
      jekyll-feed (~> 0.9)
      jekyll-seo-tag (~> 2.1)
    pathutil (0.16.2)
      forwardable-extended (~> 2.6)
    public_suffix (4.0.5)
    rb-fsevent (0.10.4)
    rb-inotify (0.10.1)
      ffi (~> 1.0)
    rexml (3.2.4)
    rouge (3.20.0)
    safe_yaml (1.0.5)
    sassc (2.4.0)
      ffi (~> 1.9)
    terminal-table (1.8.0)
      unicode-display_width (~> 1.1, >= 1.1.1)
    unicode-display_width (1.7.0)

PLATFORMS
  ruby

DEPENDENCIES
  jekyll (~> 4.1.1)
  jekyll-paginate
  minima (~> 2.0)

BUNDLED WITH
   2.0.1
{{< / highlight >}}

![Jekyll Install: Bundle: Gemfile.lock][image-bundle-lock]

#### Bundle Exec

Run the Jekyll, this will read the <code>_config.yml</code>.

Instead of 

{{< highlight bash >}}
$ jekyll serve
{{< / highlight >}}

Run the Jekyll inside bundle.

{{< highlight bash >}}
$ bundle exec jekyll serve
{{< / highlight >}}

![Jekyll Install: bundle exec jekyll serve][image-bundle-exec]

{{< highlight bash >}}
$ bundle exec jekyll serve
Configuration file: /home/epsi/tutor-01/_config.yml
            Source: /home/epsi/tutor-01
       Destination: /home/epsi/tutor-01/_site
 Incremental build: disabled. Enable with --incremental
      Generating... 
                    done in 0.035 seconds.
 Auto-regeneration: enabled for '/home/epsi/tutor-01'
    Server address: http://127.0.0.1:4000/
  Server running... press ctrl-c to stop.
{{< / highlight >}}

#### Open in Your Browser

Now you can open Jekyll in your favorite browser.

* <localhost:4000>

-- -- --

### RVM

> RVM is not mandatory.

Non beginner might require different ruby.
Actually, it is safer than rely on version in distribution,
that could change from time to time.

I use Ruby Verion Manager (RVM) for this guidance,
although you can use any other method to install Jekyll.

#### Official Documentation

* [rvm.io](https://rvm.io)

The step in this guidance is
almost the same with the officials site below.

* [rvm.io/rvm/install](https://rvm.io/rvm/install)

#### Issue

> sitewide or userwide ?

Here, we install RVM using userwide.

**Do not** install sitewide.
Running <code>apt install rvm</code> would create issue later on.

#### Step

> Using RVM require more steps, and patient

* Install Ruby with RVM

* Install Jekyll with Bundle

* Run the site on localhost

We have already cover the last two steps.

#### Install Ruby with RVM

These are the steps summary:

{{< highlight bash >}}
$ gpg --keyserver hkp://keys.gnupg.net --recv-keys \
  409B6B1796C275462A1703113804BB82D39DC0E3 \
  7D2BAF1CF37B13E2069D6956105BD0E739499BDB

$ \curl -sSL https://get.rvm.io | bash -s stable

$ source ~/.rvm/scripts/rvm

$ rvm list known

$ rvm install 2.6

$ rvm —default use 2.6.4

$ ruby -v

$ which ruby
{{< / highlight >}}

#### GPG

{{< highlight bash >}}
$ gpg --keyserver hkp://keys.gnupg.net --recv-keys \
  409B6B1796C275462A1703113804BB82D39DC0E3 \
  7D2BAF1CF37B13E2069D6956105BD0E739499BDB
gpg: key 105BD0E739499BDB: "Piotr Kuczynski <piotr.kuczynski@gmail.com>" not changed
gpg: key 3804BB82D39DC0E3: "Michal Papis (RVM signing) <mpapis@gmail.com>" 1 new signature
gpg: Total number processed: 2
gpg:              unchanged: 1
gpg:         new signatures: 1
{{< / highlight >}}

You may consider, clicking the image for wider figure.

![RVM: GPG Keys][image-rvm-gpg-keys]

#### Get RVM

You need a cript that you can get directly from <https://get.rvm.io>.

{{< highlight bash >}}
$ \curl -sSL https://get.rvm.io | bash -s stable
{{< / highlight >}}

![RVM: Get RVM][image-rvm-get-rvm]

Use backslash to avoid any `curl` alias defined in `.bashrc`.
This will run `curl` executable without alias.

#### List Known Ruby

{{< highlight bash >}}
$ source ~/.rvm/scripts/rvm

$ rvm list known
{{< / highlight >}}

![RVM: List Known][image-rvm-list-known]

We can see some Ruby version, such as `2.6`.

#### Install Ruby

Consider install Ruby `2.6`.

{{< highlight bash >}}
$ rvm install 2.6
{{< / highlight >}}

This figure require for bigger resolution.

![RVM: Install Ruby][image-rvm-install-26]

#### Set Environment

Set yor path environment
such as in `.bashrc` or `.zshrc`.
You may use any text editor, e.g. `gedit`, `geany`, `nano`, or `ViM`.
Add these two lines:

{{< highlight bash >}}
export PATH=${PATH}:~/.rvm/gems/ruby-2.6.5/bin/
source ~/.rvm/scripts/rvm
{{< / highlight >}}

![RVM: bashrc zshrc][image-rvm-zsh-path]

#### Set Default Ruby

{{< highlight bash >}}
$ source ~/.rvm/scripts/rvm

$ rvm --default use 2.6.5
Using /home/epsi/.rvm/gems/ruby-2.6.5

$ ruby -v
ruby 2.6.5p114 (2019-10-01 revision 67812) [x86_64-linux]

$ which ruby
/home/epsi/.rvm/rubies/ruby-2.6.5/bin/ruby
{{< / highlight >}}

![RVM: Default Ruby][image-rvm-def-ruby]

#### Install Jekyll with Bundle

The next step is of course install Jekyll with bundle within this RVM.

{{< highlight bash >}}
$ gem install bundler

$ cd tutor-01

$ bundle install

$ bundle exec jekyll serve
{{< / highlight >}}

Now your jekyll is ready to use.

#### Alternative Setup

You may use docker.
But I never try this method.

-- -- --

### What's Next?

Consider continue reading [ [Jekyll - Plain - Repository][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/06/03/jekyll-plain-repository/

[image-version]:        {{< assets-ssg >}}/2020/06/intro/00-version.png
[image-gem-bundler]:    {{< assets-ssg >}}/2020/06/intro/00-gem-install-bundler.png
[image-bundle-gemfile]: {{< assets-ssg >}}/2020/06/intro/00-bundle-gemfile.png
[image-bundle-install]: {{< assets-ssg >}}/2020/06/intro/00-bundle-install.png
[image-bundle-lock]:    {{< assets-ssg >}}/2020/06/intro/00-bundle-gemfile-lock.png
[image-bundle-exec]:    {{< assets-ssg >}}/2020/06/intro/00-bundle-exec.png

[image-rvm-gpg-keys]:   {{< assets-ssg >}}/2020/06/intro/00-rvm-gpg-keys.png
[image-rvm-get-rvm]:    {{< assets-ssg >}}/2020/06/intro/00-rvm-get-rvm.png
[image-rvm-list-known]: {{< assets-ssg >}}/2020/06/intro/00-rvm-list-known.png
[image-rvm-install-26]: {{< assets-ssg >}}/2020/06/intro/00-rvm-install-ruby.png
[image-rvm-zsh-path]:   {{< assets-ssg >}}/2020/06/intro/00-rvm-zsh-path.png
[image-rvm-def-ruby]:   {{< assets-ssg >}}/2020/06/intro/00-rvm-default-ruby.png
