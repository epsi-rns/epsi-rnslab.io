---
type   : post
title  : "Jekyll - Deploy - Simple"
date   : 2020-06-25T09:17:35+07:00
slug   : jekyll-deploy-simple
categories: [ssg]
tags      : [jekyll]
keywords  : [deploy, repository, github]
author : epsi
opengraph:
  image:  assets/posts/ssg/2020/06/intro/10-github-create-repository.png

toc    : "toc-2020-06-jekyll-step"

excerpt:
  Building Jekyll Site Step by step,
  Overview of deploying Jekyll in repository,
  without the burden of CI/CD.

---

### Preface

> Goal: Overview of deploying Jekyll in repository.

-- -- --

### Introduction

Now that you have created your Jekyll site on your local notebook,
it is a good time to make your site live,
so your friend can visit, and admire your work.

-- -- --

### 1: Git Repository

#### Using Git in Terminal

Using `git` is fundamental skill for web development nowadays
`git` command is a part of `terminal` skill.

In the old days, making website is depend on this stack:

* Skill: `HTML`, `CSS`, and `JS`.

Now the skill become:

* Skill: `HTML`, `CSS`, `JS`, and `terminal`.

The easiest `git` command to for beginner is `git clone`

{{< highlight bash >}}
$ git clone https://gitlab.com/epsi-rns/tutor-jekyll-plain/
{{< / highlight >}}

#### Git in Operating System

1. `git` is available in most GNU Linux distribution.

2. `git` is also available to setup in MacOS.

3. You can also use `termux` to access git in android smartphone.

4. Windows 7 user, can install `chocolatey`,
   and install `git` using `choco`.
   
   * [Windows: Setting up CLI Environment][github-windows-cli]

5. I do not know about Windows 10. I never use one.
   Chance is, you have to ask your friend, or IT support.

#### Create Repository

> Make your own repository

First thing to do is to crate your repository account.
You can choose either github or gitlab or bitbucket.

After your account ready,
you should make your own a repository.

![Github: create Repository][image-create-repo]

It is easier `SSH` to access personal repository.
So do not forget to setup SSH account,
on your github or gitlab or bitbucket.
With SSH, you clone command use different protocol.

{{< highlight bash >}}
$ git clone git@github.com:epsi-rns/jekyll4-test.git
{{< / highlight >}}

#### Working with Repository

Your daily command is just these commands.

{{< highlight bash >}}
$ git status
$ git add --all
$ git status
{{< / highlight >}}

And then

{{< highlight bash >}}
$ git commit -m "Something"
$ git push -u origin master
{{< / highlight >}}

For more detail you can read this article:

* [Using Git for Working Between Computer][local-using-git]

More recent github using `main` instead of `master`.

{{< highlight bash >}}
$ git push -u origin main
{{< / highlight >}}

#### Reading

* <https://git-scm.com/book/en/>

-- -- --

### 2: Github

As default, github pages build from jekyll source.
This means, you can just upload your Jekyll site,
to master branch on github,
and github will automatically built for you.

![Jekyll: Github Pages Served][image-pages-served]

If you want other third party to manage Jekyll site,
just put `.nojekyll` in your directory.

#### Plugin Caveat

Be aware that for security reason
github won't allow custom plugin, to be executed.
You custom plugin won't work.
And there is only a few default core plugin that allowed in github.

For this to work, we need third party CI/CD,
discussed later in the next article.

-- -- --

### 3: Development Configuration

Your configuration, between live site and localhost,
might have differences.
For this to works you need to create to two configurations.

#### Live Example

{{< highlight yaml >}}
# Site settings
title: Yet Another Open Source Blog
email: epsi.nurwijayadi@gmail.com

description: > # this means to ignore newlines until "baseurl:"
  Learn and Discover Open Source with Daily Genuine Experience.
  From Coding, Design, Package Management Tweak to Desktop Customization.

# the base hostname & protocol for your site
url: "http://epsi-rns.github.io" 

# the subpath of your site, e.g. /blog
baseurl: "" 

# Missing timezone in metadata date can lead to wrong date in url
timezone: Asia/Jakarta

# External Services, leave blank to ignore or put true to use
service:
  google_analytics_key: "UA-78098670-1"
  disqus_key: epsirnsgithubio
   
comments: true
{{< / highlight >}}

#### Development Example

You do not need to write down all configuration over again.
You just need to write, setting that need to be overriden.

{{< highlight yaml >}}
# override your production parameters
# $ jekyll serve --config _config.yml,_config_dev.yml 

url: "http://localhost:4000"
baseurl: "" 

# External Services, leave blank to ignore or put true to use
service:
  google_analytics_key: false
  disqus_key: false  
  
comments: false
{{< / highlight >}}

#### Configuration Commands

Two manage this two configurations at once,
we can utilize  `--config` parameter argument.

{{< highlight bash >}}
$ jekyll serve --config _config.yml,_config_dev.yml
{{< / highlight >}}

This is a long command, so we must find away to type this easily.

#### Alias

In my PC and notebook I setup an alias in my `zshrc`.

{{< highlight bash >}}
alias jekyll-blog='jekyll serve --config _config.yml,_config_dev.yml --incremental'
{{< / highlight >}}

Using jekyll command has never been that easy.

#### A Few Tricks.

I'm using `FZF` in `ZSH`,
so I do not have any difficulties to find the command again.

I also put the command above in configuration in comments,
so I do not need to be worry to lose the command,
while working somewhere else.

-- -- --

### Celebrate

After a long hours of study, and preparing website,
Your site is finally live.

It is time to celebrate,
by taking a nap.

### What is Next?

Consider continue reading [ [Jekyll Plain - Deploy - CI/CD][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/06/26/jekyll-deploy-ci-cd/

[local-using-git]:      https://epsi-rns.github.io/opensource/2018/01/15/using-git.html
[github-windows-cli]:   https://epsi-rns.github.io/system/2019/01/21/windows-cli-environment.html
[image-create-repo]:    {{< assets-ssg >}}/2020/06/intro/10-github-create-repository.png
[image-pages-served]:   {{< assets-ssg >}}/2020/06/intro/10-github-pages-served.png
