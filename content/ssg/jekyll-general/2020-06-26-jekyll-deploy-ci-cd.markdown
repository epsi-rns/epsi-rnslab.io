---
type   : post
title  : "Jekyll - Deploy - CI/CD"
date   : 2020-06-26T09:17:35+07:00
slug   : jekyll-deploy-ci-cd
categories: [ssg]
tags      : [jekyll]
keywords  : [deploy, CI/CD, DevOps, repository]
author : epsi
opengraph:
  image: assets/posts/devops/2020/02/refs/02-manual-13-site-branch-gh-pages.png

toc    : "toc-2020-06-jekyll-step"

excerpt:
  Building Jekyll Site Step by step,
  Overview of CI/CD for Jekyll, for intermediate user.

---

### Preface

> Goal: Overview of CI/CD for Jekyll, for intermediate user.

-- -- --

### Introduction

> This require CI/CD knowledge.

There are many ways to deploy Jekyll,
rather than just using github pages.

#### Choice: Deploying Method

How many ways to deploy your static site?
I found at least ten ways to deploy SSG site.

![Illustration: Deploy Options][illustration-deploy]

#### Target Reader

This article is not meant to be read,
for beginner who does not know,
how to build a simple jekyll site.
If you do not know, what Jekyll is all about,
please go back to repository overview.

* [Jekyll Plain - Repository][local-repository]

#### What to Do on Each CI/CD

> One step closer to be a YAML engineer.

Just writing configuration, mostly in YAML.
Create trigger if necessary.
Then upload to repository.

The CI/CD would do the jobs for you.

#### No Github Pages

If you use github repository,
add `.nojekyll` to disable autmatic github pages generation.

-- -- --

### 4: Github Actions

Instead of github pages, we can utilize github actions

#### Article

I get this configuration originally from `mas Didik`.
The article is in local language.

* [GitHub Actions untuk publikasi situs web statis dari Jekyll][didik]

He did it really great. And I rewrite on my own article.

* [CI/CD - Github Workflows][local-github-workflows]

#### Configuration Artefact

* `.github/workflows/deploy.yml`.

You can have any name, workflow directory.

#### YAML Configuration: Official Guidance

Jekyll has official documentation for Github Actions

* [Jekyll GitHub Actions][jekyll-github-actions]

{{< highlight bash >}}
name: Build and deploy Jekyll site to GitHub Pages

on:
  push:
    branches:
      - master

jobs:
  github-pages:
    runs-on: ubuntu-16.04
    steps:
      - uses: actions/checkout@v2
      - uses: helaili/jekyll-action@2.0.1
        env:
          JEKYLL_PAT: ${{ secrets.JEKYLL_PAT }}
{{< / highlight >}}

#### YAML Configuration: Alternate Guidance

{{< highlight yaml >}}
name: Jekyll Deploy

on:
  push:
    branches:
      - master

jobs:
  deploy:
    runs-on: ubuntu-latest
    steps:
    - name: Checkout to master
      uses: actions/checkout@master
    - name: Set up Ruby 2.6
      uses: actions/setup-ruby@v1
      with:
        ruby-version: 2.6.x
    - name: Install Gems and Configure Dependencies
      run: |
        gem install bundler
        bundle install --jobs 4 --retry 3
    - name: Deploy Static Files to gh-pages Branch
      env:
        GITHUB_TOKEN: ${{ secrets.GITHUB_TOKEN }}
      run: |
        bundle exec rake site:publish
{{< / highlight >}}


![Github Workflows: Jekyll Run Checkout][image-ss-workflows-05]

#### Rakefile

What is it this `rake` is all about?
Ruby use a file named `Rakefile`.
I copy-paste Didik's `Rakefile`, and adjust a little.
You can get Didik's `Rakefile` from:

* <https://github.com/id-ruby/id-ruby/blob/master/Rakefile>

And I trace the source come from

* <https://github.com/kippt/jekyll-incorporated/blob/master/Rakefile>

After a few adjustment, my `Rakefile` looks like below code:

{{< highlight ruby >}}
require "rubygems"
require "tmpdir"

require "bundler/setup"
require "jekyll"

# Change your GitHub reponame 
# eg. "kippt/jekyll-incorporated"
# or "id-ruby/id-ruby"
GITHUB_REPONAME = "epsi-rns/workflows-jekyll"
GITHUB_TOKEN    = ENV["GITHUB_TOKEN"]


namespace :site do
  desc "Generate blog files"
  task :generate do
    Jekyll::Site.new(Jekyll.configuration({
      "source"      => ".",
      "destination" => "_site"
    })).process
  end


  desc "Generate and publish blog to gh-pages"
  task :publish => [:generate] do
    current_directory = Dir.pwd

    Dir.mktmpdir do |tmp|
      cp_r "_site/.", tmp
      Dir.chdir tmp
      system "git init"
      system "git config user.name someone"
      system "git config user.email someone@somewhere"
      system "git add ."
      message = "Site updated at #{Time.now.utc}"
      system "git commit -m #{message.inspect}"
      system "git remote add origin #{git_origin}"
      system "git push origin master:refs/heads/gh-pages --force"
      Dir.chdir current_directory
    end
  end

  def git_origin
    if GITHUB_TOKEN
      "https://x-access-token:#{GITHUB_TOKEN}@github.com/#{GITHUB_REPONAME}.git"
    else
      "git@github.com:#{GITHUB_REPONAME}.git"
    end
  end
end
{{< / highlight >}}

Wow... I have so much to learn from this `Rakefile`.
You have done great job, mr. `Didik`!.

-- -- --

### 5: Gitlab CI

#### Configuration Artefact

* `.gitlab-ci.yml`.

#### YAML Configuration: Official Guidance

* [Jekyll Gitlab Pages](https://gitlab.com/pages/jekyll)

{{< highlight yaml >}}
image: ruby:latest

variables:
  JEKYLL_ENV: production
  LC_ALL: C.UTF-8

before_script:
  - gem install bundler
  - bundle install

test:
  stage: test
  script:
  - bundle exec jekyll build -d test
  artifacts:
    paths:
    - test
  except:
  - master

pages:
  stage: deploy
  script:
  - bundle exec jekyll build -d public
  artifacts:
    paths:
    - public
  only:
  - master
{{< / highlight >}}

I run Gitlab CI for my Jekyll Bootstrap Examples.

![Gitlab CI: Jekyll Bootstrap Examples][image-gitlab-ci-example]

-- -- --

### 6: Netlify

#### Article

I never try Jekyll on Netlify APP.
I use netlify for my Hexo site.
But I think Jekyll should also works.

* [Host Hexo on Netlify, Step By Step][local-deploy-netlify]

#### Configuration Artefact

Not mandatory, but you can use one.

* `netlify.toml`.

#### Official Guidance

* [Common Configurations][common-configurations]

[common-configurations]: https://docs.netlify.com/configure-builds/common-configurations/#jekyll

-- -- --

### 7: Vercel (Zeit Now)

You must link your Vercel account with,
either github, gitlab or bitbucket.

#### Article

I also write an article about Vercel App.

* [CI/CD - Zeit Now][local-zeit-now]

#### Configuration Artefact

Not required.

#### Vercel Configuration

![Vercel App: Jekyll on Bitbucket][image-vercel-example]

#### Live Site

My Jekyll on bitbucket repository run well with Vercel.

* [zeit-jekyll.now.sh](https://zeit-jekyll.now.sh/)

-- -- --

### 8: Travis

Travis do the jobs for you.
Without the need to type git commands.

#### Article

* [CI/CD - Travis][local-travis]

#### Configuration Artefact

* `.travis.yml`.

#### YAML Configuration: 

{{< highlight yaml >}}
language: ruby
rvm:
  - 2.6.3

before_script: bundle install
script: bundle exec jekyll build

deploy:
  local-dir: _site
  provider: pages
  skip-cleanup: true

  # Set in travis-ci.org dashboard, marked secure
  github-token: $GITHUB_TOKEN
  keep-history: true
  on:
    branch: master
{{< / highlight >}}

![Travis: Jekyll on Github][image-travis-example]

-- -- --

### 9: CircleCI

#### Articles

* [CI/CD - CircleCI - Github][local-circleci-github]

#### Configuration Artefact

* `.circleci/config.yml`.

#### YAML Configuration

I use a low level approach.
Accessing git directly.

{{< highlight yaml >}}
version: 2

# https://jtway.co/deploying-jekyll-to-github-pages-with-circleci-2-0-3eb69324bc6e
# https://willschenk.com/articles/2018/automating_hugo_with_circleci/

workflows:
  version: 2
  build:
    jobs:
      - buildsite:
         filters:
            branches:
              only: master

jobs:
  buildsite:
    docker:
      # Choose Image that Support Git Worktree Command
      - image: circleci/ruby:2.4
    working_directory: ~/source
    environment:
      BUILD_DIR: ~/public
    steps:
      - checkout
      - add_ssh_keys:
          fingerprints:
            - "dc:18:73:7a:1a:a7:5f:92:31:67:cb:20:eb:0f:77:31"
      - run:
          name: Prepare Git Initialization
          command: |
            git config user.email "someone@somewhere"
            git config user.name "someone"
            git branch -a -l | cat
            if [ $(git branch -a -l | grep gh-pages | wc -l) -eq "0" ]; then
              echo "[Create gh-pages for the first time]"
              git checkout -b gh-pages
              git commit --allow-empty -m "Create gh-pages for the first time"
              git push --set-upstream origin gh-pages
              git checkout master
            fi
      - run: bundle install
      - run: bundle exec jekyll build
      - deploy:
          name: Precheck Output 
          command: |
            git worktree add -B gh-pages $BUILD_DIR origin/gh-pages
            git worktree list
            cd $BUILD_DIR
            ls -lah
            find . -maxdepth 1 ! -name '.git' -exec rm -rf {} \;
            mv ~/source/_site/* .
            touch .nojekyll
            ls -lah
      - deploy:
          name: Deploy Release to GitHub
          command: |
            cd $BUILD_DIR
            git add --all
            git status
            git commit --allow-empty -m "[skip ci] $(git log master -1 --pretty=%B)"
            git push --set-upstream origin gh-pages
            echo "[Deployed Successfully]"
{{< / highlight >}}

![CircleCI: Jekyll Steps Skeleton][image-ss-circleci-22]

-- -- --

### 10: Manual

This is a long journey, that deserve its own articles.

#### Articles

You can read it here.

* [CI/CD - Git Refs][local-refs]

#### Worktree

I utilize `git worktree` command.

This below is an example for Pelican,
but this should also works with Jekyll.

![git worktree: Two Panes tmux, prepare HEAD for each branches][image-ss-worktree-05]

-- -- --

### Conclusion

There are many ways to deploy Jekyll.
Once it build, you can put a CNAME,
so that the result displayed in your very own domain.

> Confused?

Although this CI/CD article is not for beginner.
You should not get intimidated by the YAML configuration.
This article meant to be an CI/CD overview for Jekyll,
You can learn easily, by following the tutorial step by step,
because there is guidance in detail over the CI/CD article series.
You can thoroughly read, start from this article.

* [CI/CD - Deploy Overview][local-ci-cd-overview]

-- -- --

### What's Next?

We are done with `jekyll plain`, and also deployment.
We still have `jekyll` with `stylesheet` to cover.
Put on clothes to our plain jekyll, 
and make the blog looks fashionable.

Consider continue reading:
to [ [Jekyll - Bootstrap OC - Repository][local-whats-bsoc] ],
or [ [Jekyll - Bulma MD - Repository][local-whats-bmd] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[illustration-deploy]:  {{< assets-ssg >}}/2020/deploy-options.png

[local-whats-bsoc]:     {{< baseurl >}}ssg/2020/07/01/jekyll-bsoc-repository/
[local-whats-bmd]:      {{< baseurl >}}ssg/2020/08/01/jekyll-bmd-repository/
[local-repository]:     {{< baseurl >}}ssg/2020/06/03/jekyll-plain-repository/

[local-ci-cd-overview]:     {{< baseurl >}}devops/2020/02/10/ci-cd-overview/
[local-deploy-netlify]:     {{< baseurl >}}ssg/2019/05/29/deploy-netlify/
[local-whats-next]:         {{< baseurl >}}devops/2020/02/12/ci-cd-git-refs/
[local-refs]:               {{< baseurl >}}devops/2020/02/12/ci-cd-git-refs/
[local-zeit-now]:           {{< baseurl >}}devops/2020/02/13/ci-cd-zeit-now/
[local-worktree-01]:        {{< baseurl >}}devops/2020/02/14/ci-cd-git-worktree-01/
[local-travis]:             {{< baseurl >}}devops/2020/02/15/ci-cd-travis-01/
[local-circleci-github]:    {{< baseurl >}}devops/2020/02/16/ci-cd-circleci-02/
[local-github-workflows]:   {{< baseurl >}}devops/2020/02/18/ci-cd-github-workflows/
[local-git-hooks]:          {{< baseurl >}}devops/2020/02/19/ci-cd-git-hooks

[jekyll-github-actions]:    https://jekyllrb.com/docs/continuous-integration/github-actions/
[didik]:                    https://didik.id/github-actions-untuk-mempublikasikan-situs-web-statis-dari-jekyll

[image-ss-workflows-05]:    {{< baseurl >}}assets/posts/devops/2020/02/workflows/08-workflows-05-jekyll-job-deploy.png
[image-gitlab-ci-example]:  {{< assets-ssg >}}/2020/06/intro/10-gitlab-ci-example.png
[image-vercel-example]:     {{< assets-ssg >}}/2020/06/intro/10-vercel-example.png
[image-travis-example]:     {{< assets-ssg >}}/2020/06/intro/10-travis-example.png
[image-ss-circleci-22]:     {{< baseurl >}}assets/posts/devops/2020/02/circleci/06-circleci-22-jekyll-worktree.png
[image-ss-worktree-05]:     {{< baseurl >}}assets/posts/devops/2020/02/worktree/04-worktree-05-compare-head.png
