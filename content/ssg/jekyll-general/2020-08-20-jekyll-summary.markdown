---
type   : post
title  : "Jekyll - Summary"
date   : 2020-08-20T09:17:35+07:00
slug   : jekyll-summary
categories: [ssg]
tags      : [jekyll, bulma]
keywords  : [summary]
author : epsi
opengraph:
  image: assets-ssg/2020/08/toc-jekyll-all.png

toc    : "toc-2020-06-jekyll-step"

excerpt:
  Building Jekyll Site Step by step.
  Summary of article series.
---

### Preface

> Goal: What Jekyll can do in this article series

#### Repositories

This tutorial consist of two stage, but in three parts.
The second stage is a choice between Bulma and Bootstrap.

* Stage 1: **Jekyll Plain**  
  * Layout, Content, Managing Code, Feature: Pagination, Plugin, Theme.

* Stage 2: **Jekyll + Bootstrap**  
  * CSS, SASS, Layout, Widget, Pagination, Blog Post, Content.

* Stage 2: **Jekyll + Bulma**  
  * CSS, SASS, Layout, Widget, Pagination, Blog Post, Content.

#### Table of Content

* Preface: Table of Content

* 1: Jekyll in General

* Jekyll Plain

  * 2: Jekyll Layout

  * 3: Jekyll Content

  * 4: Managing Code

  * 5: Feature: Pagination

  * 6: Ruby Plugins

  * 7: Theme Bundling

* Jekyll with Bootstrap or Bulma

  * 8: Apply CSS

  * 9: Apply SASS

  * 10: Layout

  * 11: Widget

  * 12: Pagination

  * 13: Blog Post

  * 14: Content

* 15: Deploy

* Conclusion

-- -- --

### 1: Jekyll in General

#### Topics Covered

* Foreword

* Install

* Deploy

#### Why Jekyll?

> But why?

  * This is just an example project.
    Most you can do with one SSG can be done with other SSG as well.
  * You are free to use your SSG choice:
    Jekyl, Hugo, Eleventy, 11ty, Pelican, Gatsby.
  * Here is the [Concept SSG Presentation][concept-ssg].

#### Any Expectation?

  * Case can be applied somewhere else.
    Most stuff applied in other SSG. Just don't be a fanatic.
  * An example case for beginner.
    Just another humble example.
  * Starting point for a serious writer.
    After all, making a blog site is easy and fun.

#### Theme Making Cycle

Making theme consist of these steps:

  * Start From the Layout  
    * Preparing layout with templating engine.

  * Custom Pages:  
    * Liquid loop for custom pages and layout.

  * Adding Stylesheet:  
    * Give wearable clothes to above loop, for nice looks.

  * Additional Feature:  
    * Such as static data, widget and pagination.

  * Page Content:  
    * Meta SEO, TOC, markdown, shortcode,
      syntax highlighting, stylesheet adjustment, MathJax.

#### Theming Skill

> Adjustment is a reccuring issue that arise regularly.

Even when you are using a ready to use theme,
you might still need customization to suit your needs.

#### Install Jekyll

  * Default Package from distro:
    * Easy for beginner.
  
  * Bundle:
    * Bundle can handle multiple Jekyll installment.
  
  * RVM:
    * RVM can handle multiple Ruby installment.

![Jekyll Install: Bundle: Gemfile][image-bundle-gemfile]

> Article: [Jekyll - Install using RVM][jekyll-overview]

#### Simple Deployment

  * Git skill is a must.
  
  * Multiple configuration:
    * Such as using `_config_dev.yml`.

![Jekyll Deploy: Github Pages Served][image-pages-served]

> Article: [Jekyll - Deploy - Simple][jekyll-deploy-simple]

#### CI/CD Deployment

Flexible for intermediate user:

  * Github Actions
  
  * Gitlab CI
  
  * Netlify
  
  * Vercel (Zeit Now)
  
  * Travis
  
  * CircleCI
  
  * Manual

![Jekyll Deploy: CircleCI Steps Skeleton][image-ss-circleci-22]

> Article: [Jekyll - Deploy - CI/CD][jekyll-ci-cd]

[image-bundle-gemfile]: {{< assets-ssg >}}/2020/06/intro/00-bundle-gemfile.png
[image-pages-served]:   {{< assets-ssg >}}/2020/06/intro/10-github-pages-served.png
[image-ss-circleci-22]: {{< baseurl >}}assets/posts/devops/2020/02/circleci/06-circleci-22-jekyll-worktree.png

-- -- --

### 2: Jekyll Layout

#### Topics Covered

* [Plain Tutor 01][plain-01]:
  Minimal Layout

* [Plain Tutor 02][plain-02]:
  Partial Layout

* [Plain Tutor 03][plain-03]:
  Custom Pages

* [Plain Tutor 03][plain-03]:
  Custom Output

![Jekyll: Layout changes for each steps][template-inheritance]

[template-inheritance]: {{< assets-ssg >}}/2020/08/template-inheritance-animate.gif

#### Minimal Layout

The easiest thing to do this is to make a minimal layout,
and modified later.

![Jekyll Layout: Minimal][image-01-nerdtree]

> Article: [Jekyll Plain - Minimal][jekyll-plain-minimal]

#### Partial Layout

Just a bit of refactoring.

![Jekyll Layout: Partial][image-02-nerdtree]

With a very humble result as figure below

![Jekyll Layout: Home in Browser][image-vc-home]

> Article: [Jekyll Plain - Layout][jekyll-plain-layout]

#### Custom Pages

With Jekyll layout, you can add custom pages, such as:
archives, tags, categories, blog list.

![Jekyll Layout: Custom Pages][image-03-nerdtree]

> Article: [Jekyll Plain - Custom Pages][jekyll-plain-custom-pages]

#### Custom Output

Jekyll can generate custom output,
without adding any plugin.

![Jekyll: Custom Output: JSON][image-03-browser-json]

> Article: [Jekyll Plain - Custom Output][jekyll-plain-custom-output]

[image-01-nerdtree]:    {{< assets-ssg >}}/2020/06/plain/01-jekyll-nerdtree.png
[image-02-nerdtree]:    {{< assets-ssg >}}/2020/06/plain/02-jekyll-nerdtree.png
[image-03-nerdtree]:    {{< assets-ssg >}}/2020/06/plain/03-jekyll-nerdtree.png
[image-vc-home]:        {{< assets-ssg >}}/2020/06/plain/02-jekyll-home.png
[image-03-browser-json]:{{< assets-ssg >}}/2020/06/plain/03-browser-json.png

-- -- --

### 3: Jekyll Content

#### Topics Covered

* [Plain Tutor 04][plain-04]:
  Markdown

* [Plain Tutor 04][plain-04]:
  Shortcode

* [Plain Tutor 05][plain-05]:
  Custom Code: Meta SEO

#### Markdown

Markdown is simply, just a text format.

![Jekyll Content: Markdown in ViM][image-vim-plain]

Which will be processed to markup.

![Jekyll Content: Processed Markdown in Browser][image-browser-newline]

> Article: [Jekyll Plain - Markdown][jekyll-plain-markdown]

#### Shortcode

We can utilize shortcodes like code in Jekyll.
To provide link, or to provide image.

![Jekyll Content: Shortcode in ViM"][image-vim-shortcode]

Which will also be processed to HTML.

![Jekyll Content: Processed Shortcode in Browser][image-browser-advert]

This shortcode is just an include statement.

#### Custom Code: Meta SEO

Opengraph is a must nowadays.

![Jekyll Meta: Telegram Desktop Opengraph][image-meta-seo]

> Article: [Jekyll Plain - Meta SEO][jekyll-plain-meta-seo]

[image-vim-plain]:      {{< assets-ssg >}}/2020/06/plain/04-vim-plain.png
[image-browser-newline]:{{< assets-ssg >}}/2020/06/plain/04-browser-newline-small.png
[image-vim-shortcode]:  {{< assets-ssg >}}/2020/06/plain/04-vim-shortcode.png
[image-browser-advert]: {{< assets-ssg >}}/2020/06/plain/04-browser-shortcode-small.png
[image-meta-seo]:       {{< baseurl >}}assets/posts/ssg/2019/05/59-tele-desktop-opengraph-s.png

-- -- --

### 4: Managing Code

#### Topics Covered

* [Plain Tutor 04][plain-04]:
  Part One
  
  * Liquid Code Placement, Layout Enhancement.

* [Plain Tutor 04][plain-04]:
  Part Two
  
  * Category and Tag, Archive by Year, then by Month.

* [Plain Tutor 05][plain-05]:
  Part Three
  
  * Clean up pages from code, prepare theme for use in Gemfile.

#### Liquid Code Placement

So far we have three places to put liquid code:

1. `pages` as content, or
  
2. `_layouts`, or
  
3. `_includes`.

Guidance:

* Avoid liquid codes in `pages`
  except for content such as shortcodes.
* Logic and reusable should be moved to `_includes`.
* HTML layout view to parent template in `_layouts`.
* Child template in `_layouts` contain only frontmatter and include calls.

> Article: [Jekyll Plain - Managing Code - Part One][jekyll-plain-managing-code-01]

#### Layout Enhancement

More Layout based on Default

![Jekyll: Managing Code: More Layout][image-04-nerdtree]

#### Template Inheritance: Page

First, you need to understand template inheritance.

![Jekyll: Managing Code: Template Inheritance: Layouts][image-04-layouts]

With the result as below figure

![Jekyll: Managing Code: Template Inheritance: Page Example][image-04-about]

#### Template Inheritance: Blog List

And apply this to other layout kind.

![Jekyll: Managing Code: Template Inheritance: Blog List: Flows][image-05-flow-blog]

With the result as below figure

![Jekyll: Managing Code: Template Inheritance: Blog List: Page Example][image-04-blog-list]

#### Template Inheritance: Tags Tree

Go further

![Jekyll: Managing Code: Template Inheritance: Tags Tree: Flows][image-05-flow-tags]

With the result as below figure

![Jekyll: Managing Code: Template Inheritance: Tags Tree: Page Example][image-04-tags-tree]

> Article: [Jekyll Plain - Managing Code - Part Two][jekyll-plain-managing-code-02]

#### Template Inheritance: Archive By Month

And go further again:

![Jekyll: Managing Code: Template Inheritance: Archive By Month: Flows][image-05-flow-month]

With the result as below figure

![Jekyll: Managing Code: Template Inheritance: Archive By Month: Page Example][image-04-by-month]

> Article: [Jekyll Plain - Managing Code - Part Three][jekyll-plain-managing-code-03]

[image-04-about]:       {{< assets-ssg >}}/2020/06/plain/04-jekyll-about-small.png
[image-04-nerdtree]:    {{< assets-ssg >}}/2020/06/plain/04-jekyll-nerdtree.png
[image-04-layouts]:     {{< assets-ssg >}}/2020/06/plain/04-jekyll-layouts.png
[image-05-flow-blog]:   {{< assets-ssg >}}/2020/06/plain/05-flow-blog-list.png
[image-04-blog-list]:   {{< assets-ssg >}}/2020/06/plain/04-jekyll-blog-list-small.png
[image-05-flow-tags]:   {{< assets-ssg >}}/2020/06/plain/05-flow-tags.png
[image-04-tags-tree]:   {{< assets-ssg >}}/2020/06/plain/04-jekyll-tags-tree-small.png
[image-05-flow-month]:  {{< assets-ssg >}}/2020/06/plain/05-flow-by-month.png
[image-04-by-month]:    {{< assets-ssg >}}/2020/06/plain/04-jekyll-by-month-small.png

-- -- --

### 5: Feature: Pagination

#### Topics Covered

* [Plain Tutor 05][plain-05]:
  Intro

* [Plain Tutor 05][plain-05]:
  Simple

* [Plain Tutor 05][plain-05]:
  Number

* [Plain Tutor 05][plain-05]:
  Adjacent

* [Plain Tutor 05][plain-05]:
  Indicator

#### Intro

Example Pagination URL

  * Page 1: http://localhost:4000/pages/
  
  * Page 2: http://localhost:4000/pages/blog-2/
  
  * Page 3: http://localhost:4000/pages/blog-3/
  
  * And so on.

> Article: [Jekyll Plain - Pagination - Intro][jekyll-pagination-intro]

#### Simple

![Jekyll Pagination: Simple Pagination with Navigation][image-05-pg-simple]

> Article: [Jekyll Plain - Pagination - Simple][jekyll-pagination-simple]

#### Number

![Jekyll Pagination: Number Pagination without Navigation][image-05-pg-number]

> Article: [Jekyll Plain - Pagination - Number][jekyll-pagination-number]

#### Adjacent

Prepare pagination with indicator,
by only showing what necessary, and hide the rest.

![Jekyll Pagination: Basic Adjacent Pagination: Animation][image-adjacent-gif]

> Article: [Jekyll Plain - Pagination - Adjacent][jekyll-pagination-adjacent]

Offset calculation require logic, and math.

> Article: [Jekyll Plain - Pagination - Logic][jekyll-pagination-logic]

#### Indicator

And the result is as figure below:

![Jekyll Pagination: Adjacent Pagination with Indicator: Animation][image-indicator-gif]

> Article: [Jekyll Plain - Pagination - Indicator][jekyll-pagination-indicator]

[image-05-pg-simple]:   {{< assets-ssg >}}/2020/06/plain/05-pagination-01-simple.png
[image-05-pg-number]:   {{< assets-ssg >}}/2020/06/plain/05-pagination-02-number.png
[image-adjacent-gif]:   {{< assets-ssg >}}/2020/06/plain/jekyll-plain-adjacent-animate.gif
[image-indicator-gif]:  {{< assets-ssg >}}/2020/06/plain/jekyll-plain-indicator-animate.gif

-- -- --

### 6: Ruby Plugins

* [Plain Tutor 06][plain-06]:
  Simple

* [Plain Tutor 06][plain-06]:
  Pagination

* [Plain Tutor 06][plain-06]:
  Tag Names

#### Simple

Liquid limitation make code complex.

![Jekyll Plugin: Long Code with Liquid][image-vim-keywords-liquid]

This can be simplified by plugin.

![Jekyll Plugin: Simple Code with Ruby Filter][image-vim-keywords]

> Article: [Jekyll Plain - Plugin - Simple][jekyll-plugin-simple]

#### Pagination

Simplified Adjacent Calculation with Ruby Plugin.

![Jekyll Plugin: Adjacent Calculation in Ruby Filter][image-vim-liquid]

Dramatically reduce liquid code.

![Jekyll Plugin: Pagination Using Ruby Filter][image-vim-filter]

And also avoid buggy code.

> Article: [Jekyll Plain - Plugin - Pagination][jekyll-plugin-pagination]

#### Tag Names

Extending official examples,
now each tag name URLs can show list of post.

![Jekyll Plugin: Tag Names Showing List of Post][image-06-tags-list]

> Article: [Jekyll Plain - Plugin - Tag Names][jekyll-plugin-tag-names]

[image-vim-keywords-liquid]:{{< assets-ssg >}}/2020/06/plain/06-terms-keywords-liquid.png
[image-vim-keywords]:   {{< assets-ssg >}}/2020/06/plain/06-vim-keywords.png
[image-vim-liquid]:     {{< assets-ssg >}}/2020/06/plain/06-plugin-is-show-adjacent.png
[image-vim-filter]:     {{< assets-ssg >}}/2020/06/plain/06-using-is-show-adjacent.png
[image-06-tags-list]:   {{< assets-ssg >}}/2020/06/plain/06-tags-blog-list.png

-- -- --

### 7: Theme Bundling

* [Plain Tutor 07][plain-07]:
  Build Gemfile

* [Plain Tutor 07][plain-07]:
  Using Gemfile

#### Build Gemfile

Bundling theme with Gemspec,
for use with other people.

![Jekyll Plugin: Example of Gemspec][image-07-gemspec]

> Article: [Jekyll Plain - Theme Bundling][jekyll-plain-theme-bundling]

#### Using Theme in Gemfile

![Jekyll Plugin: Using Theme in Gemfile][image-07-src-gemfile]

[image-07-gemspec]:     {{< assets-ssg >}}/2020/06/plain/07-oriclone-plain-gemspec.png
[image-07-src-gemfile]: {{< assets-ssg >}}/2020/06/plain/07-src-gemfile.png

-- -- --

### 8: Apply CSS

* Tutor 08: [Bootstrap][bsoc-08]/[Bulma][bmd-08]:
  Mobile First Page Design

* Tutor 08: [Bootstrap][bsoc-08]/[Bulma][bmd-08]:
  Adding Assets

* Tutor 08: [Bootstrap][bsoc-08]/[Bulma][bmd-08]:
  Using CSS Frameworks

* Tutor 08: [Bootstrap][bsoc-08]/[Bulma][bmd-08]:
  Parent and Child Layouts

* Tutor 08: [Bootstrap][bsoc-08]/[Bulma][bmd-08]:
  All Layouts

#### Mobile First Page Design

![Jekyll: Responsive Page Design][image-png-layout-page]

#### Adding Assets

You can add any necessary assets such as:
images, css frameworks, and javascript frameworks.

![Jekyll Bootstrap: Assets Tree][image-ss-08-tree-assets]

#### Using CSS Frameworks

Using CSS framework such as Bulma or Bootstrap or Custom,
can make your life easier.

![Jekyll Bootstrap: Home Page Content][image-vc-pa-home]

> Article: [Jekyll - Bootstrap - CSS - Part One][jekyll-bsoc-css-intro]

> Article: [Jekyll - Bulma - CSS - Part One][jekyll-bmd-css-intro]

#### Parent and Child Layouts

Both are using Bootstrap now.

![Jekyll Bootstrap: Parent Layout][image-parent-page]

#### All Layouts

![Jekyll Bootstrap: Tags Layout][image-pages-tags]

> Article: [Jekyll - Bootstrap - CSS - Part Two][jekyll-bsoc-css-layout]

> Article: [Jekyll - Bulma - CSS - Part Two][jekyll-bmd-css-layout]

[image-png-layout-page]:    {{< baseurl >}}assets/posts/frontend/2019/12/layout-page.png
[image-ss-08-tree-assets]:  {{< assets-ssg >}}/2020/07/bootstrap/08-tree-assets.png
[image-vc-pa-home]:         {{< assets-ssg >}}/2020/07/bootstrap/08-pages-home-alternate.png
[image-parent-page]:    {{< assets-ssg >}}/2020/07/bootstrap/08-parent-page.png
[image-pages-tags]:     {{< assets-ssg >}}/2020/07/bootstrap/08-pages-tags-alternate.png

-- -- --

### 9: Apply SASS

* Tutor 09: [Bootstrap][bsoc-09]/[Bulma][bmd-09]:
  Using Custom SASS

* Tutor 09: [Bootstrap][bsoc-09]/[Bulma][bmd-09]:
  Adding SASS Assets

* Tutor 09: [Bootstrap][bsoc-09]/[Bulma][bmd-09]:
  Apply to All Layouts

#### Using Custom SASS

  * You desire for a better design

    * Further, you can utilize stylesheet
    to control your responsive space,
    or custom component

  * Long code tends to get messy.

    * You need to get organized.

Enhance theme looks: __Small thing, might have great impact.__

![Jekyll Bootstrap: Home Page Content][image-vc-pa-home-09]

> Article: [Jekyll - Bootstrap - SASS - Part One][jekyll-bsoc-sass-intro]

> Article: [Jekyll - Bulma - SASS - Part One][jekyll-bmd-sass-intro]

#### Adding SASS Assets

Jekyll utilize SASS/SCSS as default CSS preprocessor.

![Jekyll Bootstrap: SASS Assets Tree][image-tree-sass]

Import SASS from any directory.

![Jekyll Bootstrap: SASS Import][image-tree-asset-sass]

#### Apply to All Layouts

Again, we can apply to other layouts as well.

![Jekyll Bootstrap: Pages Layout: About][image-vc-about-09]

> Article: [Jekyll - Bootstrap - SASS - Part Two][jekyll-bsoc-sass-layout]

[image-vc-pa-home-09]:  {{< assets-ssg >}}/2020/07/bootstrap/09-pages-home-alternate.png
[image-tree-sass]:      {{< assets-ssg >}}/2020/07/bootstrap/09-tree-sass.png
[image-tree-asset-sass]:{{< assets-ssg >}}/2020/07/bootstrap/09-tree-assets-sass.png
[image-vc-about-09]:    {{< assets-ssg >}}/2020/07/bootstrap/09-pages-about.png

-- -- --

### 10: Layout

* Tutor 10: [Bootstrap][bsoc-10]/[Bulma][bmd-10]:
  Variables

* Tutor 10: [Bootstrap][bsoc-10]/[Bulma][bmd-10]:
  Responsive Content

#### Variables

Managing Default Color Between Layout.

![Jekyll Bootstrap: Managing Default Color Between Layout][image-vc-po-maguire]

Color property propagate,
from pages frontmatter to child layout to parent layout.

![Jekyll Bootstrap: Color Property Propagation][image-color-inherit]

> Article: [Jekyll - Bootstrap - Layout - Variables][jekyll-bsoc-layout-variables]

> Article: [Jekyll - Bulma - Layout - Variables][jekyll-bmd-layout-variables]

#### Responsive Content

Mobile First Content Design

![Jekyll Bootstrap: Responsive Content Design][image-layout-content]

Mobile First Index Columns

![Jekyll Bootstrap: Responsive Index Columns][image-vc-by-year]

> Article: [Jekyll - Bootstrap - Layout - Index List][jekyll-bsoc-layout-index-list]

> Article: [Jekyll - Bulma - Layout - Index List][jekyll-bmd-layout-index-list]

[image-nerdtree-layout]:{{< assets-ssg >}}/2020/07/bootstrap/10-nerdtree-layouts.png
[image-nerdtree-pages]: {{< assets-ssg >}}/2020/07/bootstrap/10-nerdtree-pages.png
[image-vc-po-maguire]:  {{< assets-ssg >}}/2020/07/bootstrap/10-post-maguire.png
[image-color-inherit]:  {{< assets-ssg >}}/2020/07/bootstrap/10-color-inheritance.png
[image-layout-content]: {{< baseurl >}}assets/posts/frontend/2019/12/layout-content-trans.png
[image-vc-by-year]:     {{< assets-ssg >}}/2020/07/bootstrap/12-multiline-archive-by-year.png

-- -- --

### 11: Widget

* Tutor 11: [Bootstrap][bsoc-11]/[Bulma][bmd-11]:
  Template

* Tutor 11: [Bootstrap][bsoc-11]/[Bulma][bmd-11]:
  Simple

* Tutor 11: [Bootstrap][bsoc-11]/[Bulma][bmd-11]:
  Liquid Loop

* Tutor 11: [Bootstrap][bsoc-11]/[Bulma][bmd-11]:
  Static Data

#### Template

Prepare the default looks.

![Jekyll Bootstrap: Default Widget Template][image-nerdtree-widget]

#### Simple

Start from simple, such as plain HTML list.

![Jekyll Bootstrap: Simple Widget Example][image-wi-affiliates]

> Article: [Jekyll - Bootstrap - Widget - Simple HTML][jekyll-bsoc-widget-simple-html]

> Article: [Jekyll - Bulma - Widget - Simple HTML][jekyll-bmd-widget-simple-html]

#### Liquid Loop

You can also put anything in your widget as creative as you are.

![Jekyll Bootstrap: Custom Widget with Loop in Liquid][image-wi-archive-with]

> Article: [Jekyll - Bootstrap - Widget - Liquid Loop][jekyll-bsoc-widget-liquid-loop]

> Article: [Jekyll - Bulma - Widget - Liquid Loop][jekyll-bmd-widget-liquid-loop]

#### Static Data

To generate content, such as backlink, internal link, or other stuff.

![Jekyll Bootstrap: Backlink form Github Blog][image-wi-github]

![Jekyll Bootstrap: Backlink form Gitlab Blog][image-wi-gitlab]

Now consider link the data, from this content below.

![Jekyll Bootstrap: Frontmatter: Related Post ID][image-related-link-id]

With previous frontmatter, we can see one or more links in a widget.

![Jekyll Bootstrap: Widget: Related Posts][image-wi-related-post]

> Article: [Jekyll - Bootstrap - Widget - Static Data][jekyll-bsoc-widget-static-data]

> Article: [Jekyll - Bulma - Widget - Static Data][jekyll-bmd-widget-static-data]

[image-nerdtree-widget]:{{< assets-ssg >}}/2020/07/bootstrap/11-nerdtree-widget.png
[image-wi-affiliates]:  {{< assets-ssg >}}/2020/07/bootstrap/11-widget-affiliates.png
[image-wi-archive-with]:{{< assets-ssg >}}/2020/07/bootstrap/11-widget-archive-with-header.png
[image-wi-github]:      {{< assets-ssg >}}/2020/07/bootstrap/11-widget-archive-github.png
[image-wi-gitlab]:      {{< assets-ssg >}}/2020/07/bootstrap/11-widget-archive-gitlab.png
[image-wi-related-post]:{{< assets-ssg >}}/2020/07/bootstrap/11-widget-related-posts.png
[image-related-link-id]:{{< assets-ssg >}}/2020/07/bootstrap/11-related-links-id.png

-- -- --

### 12: Pagination

* Tutor 12: [Bootstrap][bsoc-12]/[Bulma][bmd-12]:
  Bootstrap Stylesheet

* Tutor 12: [Bootstrap][bsoc-12]/[Bulma][bmd-12]:
  Bootstrap Responsive

* Tutor 12: [Bootstrap][bsoc-12]/[Bulma][bmd-12]:
  Bulma Stylesheet

* Tutor 12: [Bootstrap][bsoc-12]/[Bulma][bmd-12]:
  Bulma Responsive

#### Bootstrap Stylesheet

Using Liquid and Bootstrap stylesheet,
a common pagination could be achieved.

![Jekyll Bootstrap Pagination: Combined Animation][image-bsoc-indicator-gif]

> Article: [Jekyll - Bootstrap - Pagination - Stylesheet][jekyll-bsoc-pg-stylesheet]

#### Bootstrap Responsive

Using Liquid and custom stylesheet,
a responsive pagination also could be achieved.

![Jekyll Bootstrap Pagination: Responsive Animation][image-bsoc-responsive-gif]

Or alternatively

![Jekyll Bootstrap Pagination: Alternate Responsive Animation][image-bsoc-alternate-gif]

> Article: [Jekyll - Bootstrap - Pagination - Responsive][jekyll-bsoc-pg-responsive]

#### Bulma Stylesheet

Using Liquid and Bulma stylesheet,
a common pagination could be achieved.

![Jekyll Bulma Pagination: Combined Animation][image-bmd-indicator-gif]

Or alternatively

![Jekyll Bulma Pagination: Alternate Combined Animation][image-bmd-alternate-gif]

> Article: [Jekyll - Bulma - Pagination - Stylesheet][jekyll-bmd-pg-stylesheet]

#### Bulma Responsive

Again, using Liquid and custom stylesheet,
a responsive pagination also could be achieved.

![Jekyll Bulma Pagination: Responsive Animation][image-bmd-responsive-gif]

> Article: [Jekyll - Bulma - Pagination - Responsive][jekyll-bmd-pg-responsive]

[image-bsoc-indicator-gif]: {{< assets-ssg >}}/2020/07/bootstrap/jekyll-pg-bsoc-04-indicator.gif
[image-bsoc-responsive-gif]:{{< assets-ssg >}}/2020/07/bootstrap/jekyll-pg-bsoc-05-responsive.gif
[image-bsoc-alternate-gif]: {{< assets-ssg >}}/2020/07/bootstrap/jekyll-pg-bsoc-05-alternate.gif
[image-bmd-indicator-gif]:  {{< assets-ssg >}}/2020/08/bulma/jekyll-pg-bmd-04-indicator.gif
[image-bmd-alternate-gif]:  {{< assets-ssg >}}/2020/08/bulma/jekyll-pg-bmd-04-alternate.gif
[image-bmd-responsive-gif]: {{< assets-ssg >}}/2020/08/bulma/jekyll-pg-bmd-05-responsive.gif

-- -- --

### 13: Blog Post

* Tutor 11: [Bootstrap][bsoc-11]/[Bulma][bmd-11]:
  Parent Layout

* Tutor 11: [Bootstrap][bsoc-11]/[Bulma][bmd-11]:
  Child Layout

* Tutor 11: [Bootstrap][bsoc-11]/[Bulma][bmd-11]:
  Preview

* Tutor 11: [Bootstrap][bsoc-11]/[Bulma][bmd-11]:
  Additional

#### Parent Layout

Your generic `columns-double.html`
should contain header, and optionally footer.

![Jekyll Bootstrap Content: Blog Post Parent Layout][image-vim-layout-parent]

#### Child Layout

May contain additional stuff such as:
table of content, navigation, and maybe license.

![Jekyll Bootstrap Content: Blog Post Child Layout][image-vim-layout-post]

#### Preview

A typical post can be seen in preview below.

![Jekyll Bootstrap: Post with Header, Footer, and Navigation][image-blog-post-crop]

> Article: [Jekyll - Bootstrap - Content - Blog Post][jekyll-bsoc-blog-post]

> Article: [Jekyll - Bulma - Content - Blog Post][jekyll-bmd-blog-post]

#### Additional

You can add any stuff such as Table of Content.

![Jekyll Bootstrap: Post with Table of Content][image-content-toc]


[image-vim-layout-parent]:  {{< assets-ssg >}}/2020/07/bootstrap/11-vim-layout-columns-double.png
[image-vim-layout-post]:    {{< assets-ssg >}}/2020/07/bootstrap/11-vim-layout-post.png
[image-blog-post-crop]:     {{< assets-ssg >}}/2020/07/bootstrap/11-blog-post-crop.png
[image-content-toc]:        {{< assets-ssg >}}/2020/07/bootstrap/11-content-toc.png

-- -- --

### 14: Content

* Tutor 12: [Bootstrap][bsoc-12]/[Bulma][bmd-12]:
  Markdown

* Tutor 12: [Bootstrap][bsoc-12]/[Bulma][bmd-12]:
  Shortcode

* Tutor 12: [Bootstrap][bsoc-12]/[Bulma][bmd-12]:
  Syntax Highlighting

* Tutor 12: [Bootstrap][bsoc-12]/[Bulma][bmd-12]:
  Miscellanous: MathJax

#### Markdown

The same as previous `plain` tutorial.

![Jekyll Bootstrap: Markdown][image-sass-space]

#### Shortcode

The same as previous `plain` tutorial.

![Jekyll Bootstrap: Shortcodes][image-shortcodes]

#### Syntax Highlighting

This require additional stylesheets.

![Jekyll Bootstrap: Syntax Highlighter][image-highlighter]

> Article: [Jekyll - Bootstrap - Content][jekyll-bsoc-content]

> Article: [Jekyll - Bulma - Content][jekyll-bmd-content]


#### Miscellanous: MathJax

![Jekyll Bootstrap: MathJax][image-mathjax]

[image-sass-space]:     {{< assets-ssg >}}/2020/07/bootstrap/12-sass-space.png
[image-shortcodes]:     {{< assets-ssg >}}/2020/07/bootstrap/12-commercial.png
[image-highlighter]:    {{< assets-ssg >}}/2020/07/bootstrap/12-highlighter.png
[image-mathjax]:        {{< assets-ssg >}}/2020/07/bootstrap/12-mathjax.png

-- -- --

### 15: Deploy!

To Github or Gitlab or Bitbucket or Netlify or else.

![CI/CD: Github Workflows: Eleventy Runner][image-deploy-overview]

> Article: [CI/CD - Deploy Overview][deploy-overview]

[image-deploy-overview]:{{< baseurl >}}assets/posts/devops/2020/02/workflows/08-workflows-05-jekyll-job-deploy.png

-- -- --

### Conclusion

There in nothing more after summary,
except this presentation slide.

Consider continue watching [ [Eleventy - Presentation Slide][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/02/07/11ty-presentation/

[plain-01]: {{< tutor-jekyll-plain >}}/tutor-01/
[plain-02]: {{< tutor-jekyll-plain >}}/tutor-02/
[plain-03]: {{< tutor-jekyll-plain >}}/tutor-03/
[plain-04]: {{< tutor-jekyll-plain >}}/tutor-04/
[plain-05]: {{< tutor-jekyll-plain >}}/tutor-05/
[plain-06]: {{< tutor-jekyll-plain >}}/tutor-06/
[plain-07]: {{< tutor-jekyll-plain >}}/tutor-07/

[bsoc-08]:  {{< tutor-jekyll-bsoc >}}/tutor-08/
[bsoc-09]:  {{< tutor-jekyll-bsoc >}}/tutor-09/
[bsoc-10]:  {{< tutor-jekyll-bsoc >}}/tutor-10/
[bsoc-11]:  {{< tutor-jekyll-bsoc >}}/tutor-11/
[bsoc-12]:  {{< tutor-jekyll-bsoc >}}/tutor-12/
[bsoc-13]:  {{< tutor-jekyll-bsoc >}}/tutor-13/
[bsoc-14]:  {{< tutor-jekyll-bsoc >}}/tutor-14/

[bmd-08]:  {{< tutor-jekyll-bmd >}}/tutor-08/
[bmd-09]:  {{< tutor-jekyll-bmd >}}/tutor-09/
[bmd-10]:  {{< tutor-jekyll-bmd >}}/tutor-10/
[bmd-11]:  {{< tutor-jekyll-bmd >}}/tutor-11/
[bmd-12]:  {{< tutor-jekyll-bmd >}}/tutor-12/
[bmd-13]:  {{< tutor-jekyll-bmd >}}/tutor-13/
[bmd-14]:  {{< tutor-jekyll-bmd >}}/tutor-14/

[concept-ssg]:  {{< baseurl >}}/ssg/2019/02/17/concept-ssg/

[//]: <> ( -- -- -- links below -- -- -- )

[jekyll-overview]:          {{< baseurl >}}/ssg/2020/06/01/jekyll-overview/
[jekyll-install-rvm]:       {{< baseurl >}}/ssg/2020/06/02/jekyll-install-rvm/
[jekyll-deploy-simple]:     {{< baseurl >}}/ssg/2020/06/25/jekyll-deploy-simple/
[jekyll-ci-cd]:             {{< baseurl >}}/ssg/2020/06/26/jekyll-ci-cd/

[//]: <> ( -- -- -- plain -- -- -- )

[jekyll-plain-repository]:  {{< baseurl >}}/ssg/2020/06/03/jekyll-plain-repository/
[jekyll-plain-minimal]:     {{< baseurl >}}/ssg/2020/06/04/jekyll-plain-minimal/
[jekyll-plain-layout]:      {{< baseurl >}}/ssg/2020/05/05/jekyll-plain-layout/
[jekyll-plain-custom-pages]:    {{< baseurl >}}/ssg/2020/06/06/plain-custom-pages/
[jekyll-plain-custom-output]:   {{< baseurl >}}/ssg/2020/06/07/jekyll-plain-custom-output/
[jekyll-plain-markdown]:        {{< baseurl >}}/ssg/2020/06/08/jekyll-plain-markdown/

[jekyll-plain-managing-code-01]:{{< baseurl >}}/ssg/2020/06/10/plain-managing-code-01/
[jekyll-plain-managing-code-02]:{{< baseurl >}}/ssg/2020/06/11/plain-managing-code-02/
[jekyll-plain-managing-code-03]:{{< baseurl >}}/ssg/2020/06/12/plain-managing-code-03/
[jekyll-plain-meta-seo]:        {{< baseurl >}}/ssg/2020/06/13/jekyll-plain-meta-seo/

[jekyll-pagination-intro]:      {{< baseurl >}}/ssg/2020/06/14/jekyll-plain-pagination-intro/
[jekyll-pagination-simple]:     {{< baseurl >}}/ssg/2020/06/15/jekyll-plain-pagination-simple/
[jekyll-pagination-number]:     {{< baseurl >}}/ssg/2020/06/16/jekyll-plain-pagination-number/
[jekyll-pagination-adjacent]:   {{< baseurl >}}/ssg/2020/06/17/jekyll-plain-pagination-adjacent/
[jekyll-pagination-logic]:      {{< baseurl >}}/ssg/2020/06/18/jekyll-plain-pagination-logic/
[jekyll-pagination-indicator]:  {{< baseurl >}}/ssg/2020/06/19/jekyll-plain-pagination-indicator/

[jekyll-plugin-simple]:         {{< baseurl >}}/ssg/2020/06/21/jekyll-plain-plugin-simple/
[jekyll-plugin-pagination]:     {{< baseurl >}}/ssg/2020/06/22/jekyll-plain-plugin-pagination/
[jekyll-plugin-tag-names]:      {{< baseurl >}}/ssg/2020/06/23/jekyll-plain-plugin-tag-names/

[jekyll-plain-theme-bundling]:  {{< baseurl >}}/ssg/2020/06/24/jekyll-plain-theme-bundling/

[//]: <> ( -- -- -- bootstrap -- -- -- )

[jekyll-bsoc-repository]:      {{< baseurl >}}/ssg/2020/07/01/jekyll-bsoc-repository/
[jekyll-bsoc-css-intro]:       {{< baseurl >}}/ssg/2020/07/02/jekyll-bsoc-css-intro/
[jekyll-bsoc-css-layout]:      {{< baseurl >}}/ssg/2020/07/03/jekyll-bsoc-css-layout/
[jekyll-bsoc-sass-intro]:      {{< baseurl >}}/ssg/2020/07/04/jekyll-bsoc-sass-intro/
[jekyll-bsoc-sass-layout]:     {{< baseurl >}}/ssg/2020/07/05/jekyll-bsoc-sass-layout/
[jekyll-bsoc-layout-variables]:    {{< baseurl >}}/ssg/2020/07/06/jekyll-bsoc-layout-variables/
[jekyll-bsoc-layout-index-list]:   {{< baseurl >}}/ssg/2020/07/07/jekyll-bsoc-layout-index-list/
[jekyll-bsoc-widget-simple-html]:  {{< baseurl >}}/ssg/2020/07/08/jekyll-bsoc-widget-simple-html/
[jekyll-bsoc-widget-liquid-loop]:  {{< baseurl >}}/ssg/2020/07/09/jekyll-bsoc-widget-liquid-loop/
[jekyll-bsoc-widget-static-data]:  {{< baseurl >}}/ssg/2020/07/10/jekyll-bsoc-widget-static-data/
[jekyll-bsoc-pg-stylesheet]:   {{< baseurl >}}/ssg/2020/07/12/jekyll-bsoc-pagination-stylesheet/
[jekyll-bsoc-pg-responsive]:   {{< baseurl >}}/ssg/2020/07/13/jekyll-bsoc-pagination-responsive/
[jekyll-bsoc-blog-post]:       {{< baseurl >}}/ssg/2020/07/14/jekyll-bsoc-blog-post/
[jekyll-bsoc-content]:         {{< baseurl >}}/ssg/2020/07/15/jekyll-bsoc-content/

[//]: <> ( -- -- -- bulma -- -- -- )

[jekyll-bmd-repository]:       {{< baseurl >}}/ssg/2020/08/01/jekyll-bmd-repository/
[jekyll-bmd-css-intro]:        {{< baseurl >}}/ssg/2020/08/02/jekyll-bmd-css-intro/
[jekyll-bmd-css-layout]:       {{< baseurl >}}/ssg/2020/08/03/jekyll-bmd-css-layout/
[jekyll-bmd-sass-intro]:       {{< baseurl >}}/ssg/2020/08/04/jekyll-bmd-sass-intro/
[jekyll-bmd-sass-layout]:      {{< baseurl >}}/ssg/2020/08/05/jekyll-bmd-sass-layout/
[jekyll-bmd-layout-variables]:     {{< baseurl >}}/ssg/2020/08/06/jekyll-bmd-layout-variables/
[jekyll-bmd-layout-index-list]:    {{< baseurl >}}/ssg/2020/08/07/jekyll-bmd-layout-index-list/
[jekyll-bmd-widget-simple-html]:   {{< baseurl >}}/ssg/2020/08/08/jekyll-bmd-widget-simple-html/
[jekyll-bmd-widget-liquid-loop]:   {{< baseurl >}}/ssg/2020/08/09/jekyll-bmd-widget-liquid-loop/
[jekyll-bmd-widget-static-data]:   {{< baseurl >}}/ssg/2020/08/10/jekyll-bmd-widget-static-data/
[jekyll-bmd-pg-stylesheet]:    {{< baseurl >}}/ssg/2020/08/12/jekyll-bmd-pagination-stylesheet/
[jekyll-bmd-pg-responsive]:    {{< baseurl >}}/ssg/2020/08/13/jekyll-bmd-pagination-responsive/
[jekyll-bmd-blog-post]:        {{< baseurl >}}/ssg/2020/08/14/jekyll-bmd-blog-post/
[jekyll-bmd-content]:          {{< baseurl >}}/ssg/2020/08/15/jekyll-bmd-content/

[//]: <> ( -- -- -- extra -- -- -- )

[deploy-overview]:          {{< baseurl >}}devops/2020/02/10/ci-cd-overview/

