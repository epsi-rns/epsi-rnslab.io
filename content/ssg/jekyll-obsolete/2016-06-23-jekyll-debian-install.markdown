---
type   : post
title  : "Jekyll Installation on Debian"
date   : 2016-06-23T04:29:15+07:00
slug   : jekyll-debian-install
categories: [ssg]
tags      : [install, jekyll]
keywords  : [static site, debian]
author : epsi
opengraph:
  image: assets/site/images/topics/jekyll-markdown.png

excerpt:
  Since Jekyll is available in Official Debian repository,
  Jekyll Installation on Debian is Easy.
  Just issue this command to install system wide Jekyll gem,
  and Debian will take care of Jekyll's dependencies.

related_link_ids: 
  - 16081315  # Side Menu Without Javascript
  - 16081215  # Side Menu Simple Tree
  - 16081115  # Responsive Side Menu
  - 16062202  # Jekyll Install Arch/Manjaro
# - 16062127  # Install Yaourt
  - 16061409  # Webcoder Begin
  - 16060951  # Jekyll Archives
  - 16061051  # Inkscape Stripes
  - 16052948  # Jekyll Related Posts

---

Since Jekyll is available in Official Debian repository,
Jekyll Installation on Debian is Easy.
Just issue this command to install system wide Jekyll gem,
and Debian will take care of Jekyll's dependencies.

{{< highlight bash >}}
$ apt-get install jekyll
{{< / highlight >}}

-- -- --

### Install Ruby

Let's not make this guidance too short,
and play for a while.

To check whether Ruby installed or not,
you can issue dpkg command in your shell..

{{< highlight bash >}}
$ dpkg -l 'ruby*'
{{< / highlight >}}

If you are using <code>ruby*</code> instead of just <code>ruby</code> word,
there will be bunch of ruby packages coming in the list.

[![Using dpkg to check Ruby Installation][image-ss-debian-dpkg-ruby]][photo-ss-debian-dpkg-ruby]

-- -- --

### Install Jekyll

Installing Jekyll in Debian means installing Jekyll Official Plugin.
Although you might not need them, it has its advantage.
You don't need to worry about missing plugin while cloning a blog from github.

{{< highlight bash >}}
$ apt install jekyll
{{< / highlight >}}

[![Using apt to install Jekyll][image-ss-debian-apt-jekyll]][photo-ss-debian-apt-jekyll]

-- -- --

### Running Jekyll

Let's see if Jekyll works

{{< highlight bash >}}
$ jekyll -v
$ jekyll --help
{{< / highlight >}}

Prepare your directory.
And run Jekyll in your directory.

{{< highlight bash >}}
$ cd /media/Works/Development/
$ mkdir test-jekyll
$ cd test-jekyll
{{< / highlight >}}

{{< highlight bash >}}
$ jekyll new .
$ ls -l
{{< / highlight >}}

You will see, some new directory and files
required to run a simple Jekyll Blog.

{{< highlight bash >}}
$ jekyll build
$ jekyll serve
{{< / highlight >}}


[![Running Jekyll Server][image-ss-debian-jekyll-nbs]][photo-ss-debian-jekyll-nbs]

You should see the site in your favorite browser running on port 4000.

* <http://localhost:4000/>

-- -- --

Thank you for reading.



[//]: <> ( -- -- -- links below -- -- -- )

[image-ss-debian-dpkg-ruby]: {{< assets-ssg >}}/2016/06//debian-dpkg-ruby-only.png
[photo-ss-debian-dpkg-ruby]: https://photos.google.com/album/AF1QipOI-OvBHZtRX5saQhwM3h7JWm32xboQ5aCs5fLr/photo/AF1QipM-aciyOG7tHD6xa61lHOQAbYcmmd1flM_gUyOl

[image-ss-debian-apt-jekyll]: {{< assets-ssg >}}/2016/06//debian-apt-jekyll-l.png
[photo-ss-debian-apt-jekyll]: https://photos.google.com/album/AF1QipOI-OvBHZtRX5saQhwM3h7JWm32xboQ5aCs5fLr/photo/AF1QipO3ca1rL59iNmRwzCbzA9HMU5zBXAfgspLqDaB4

[image-ss-debian-jekyll-nbs]: {{< assets-ssg >}}/2016/06//debian-jekyll-serve.png
[photo-ss-debian-jekyll-nbs]: https://photos.google.com/album/AF1QipOI-OvBHZtRX5saQhwM3h7JWm32xboQ5aCs5fLr/photo/AF1QipOmeULt0yP7ANRqhq2YOXHTgYJioBflIoh_1-VJ

