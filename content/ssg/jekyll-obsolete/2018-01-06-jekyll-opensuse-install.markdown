---
type   : post
title  : "Jekyll Installation on openSUSE"
date   : 2018-01-06T16:02:15+07:00
slug   : ekyll-opensuse-install
categories: [ssg]
tags      : [install, jekyll]
keywords  : [static site, opensuse, ruby gem, system wide, local rvm]
author : epsi
opengraph:
  image: assets/site/images/topics/jekyll-markdown.png

excerpt:
  Installing Jekyll on openSUSE is easy and straightforward
  This article also cover installing RVM on openSUSE.

related_link_ids: 
  - 18022117  # Jekyll Step by Step
  - 16062329  # Jekyll Install Debian
  - 16062202  # Jekyll Install Arch/Manjaro
  - 16052948  # Jekyll Related Posts

---

### Preface

In 2016 I wrote two articles:
[installing Jekyll in Debian][local-jekyll-debian], and
[installing Jekyll in Arch/Manjaro][local-jekyll-arch]. 
Now that I use openSUSE, I also write the third article.

There are two approaches for openSUSE,

* System Wide: Install gem as root with Ruby provided by openSUSE

* Local RVM: Install gem in user space with RVM (Ruby Version Manager) 

You can click images for complete screenshot.

-- -- --

### System Wide

This article is short, compared with Debian or Arch.
It is incredibly easy to run Jekyll on openSUSE.

#### Install Required Ruby Package

{{< highlight bash >}}
% sudo zypper in ruby-devel
{{< / highlight >}}

#### Install Jekyll via Gem

Do not forget to use <code>sudo</code>, and also install optional package.

{{< highlight bash >}}
% sudo gem install jekyll
% gem list
% sudo gem install jekyll-paginate
{{< / highlight >}}

[![Tumbleweed: sudo gem install jekyll][image-ss-gem-jekyll]][photo-ss-gem-jekyll]

#### Run jekyll

Since I already has my own blog, I can change directory and just run this jekyll blog server.

{{< highlight bash >}}
% cd ~/epsi-rns.github.io
% /usr/lib64/ruby/gems/2.4.0/gems/jekyll-3.7.0/exe/jekyll -h 
{{< / highlight >}}

[![Tumbleweed: Ruby Long Path][image-ss-ruby-long-path]][photo-ss-ruby-long-path]

#### Set Path

For convenience, In <code>.bashrc</code> or <code>.zshrc</code>, so we do not
need to remember jekyll path.

{{< highlight bash >}}
export PATH=${PATH}:/usr/lib64/ruby/gems/2.4.0/gems/jekyll-3.7.0/exe
{{< / highlight >}}

[![Tumbleweed: Setting Path in .bashrc or .zshrc][image-ss-set-path]][photo-ss-set-path]

For convenience, we can also make an alias.

{{< highlight bash >}}
alias jekyll-blog='jekyll serve --config _config.yml,_config_dev.yml'
{{< / highlight >}}

And run with very simple command.

{{< highlight bash >}}
% jekyll-blog
{{< / highlight >}}

-- -- --

### Local RVM

For some reasons, it is good to run Ruby outside root.
Don't ask me why, I'm a n00b in Ruby.

#### Reading

This is a must read link before you start RVM.

* [rvm.io](https://rvm.io/)

#### Prerequisite

Development Tools

{{< highlight bash >}}
% sudo zypper in ruby-devel
{{< / highlight >}}

And

{{< highlight bash >}}
% sudo zypper in patch automake bison libtool m4 patch \
  gdbm-devel libffi-devel libopenssl-devel readline-devel \
  sqlite3-devel libyaml-devel
{{< / highlight >}}

And this rpm downloaded

{{< highlight bash >}}
% sudo zypper in ~/Downloads/libdb-4_5-4.5.20-134.14.x86_64.rpm
{{< / highlight >}}

[![RVM: System Dependencies][image-ss-rvm-deps]][photo-ss-rvm-deps]

#### Install RVM

{{< highlight bash >}}
% \curl -sSL https://get.rvm.io | bash -s stable --ruby 
{{< / highlight >}}

[![RVM: Install: Start ][image-ss-rvm-install-1]][photo-ss-rvm-install-1]

[![RVM: Install: Finish][image-ss-rvm-install-3]][photo-ss-rvm-install-3]

#### Setup RVM

{{< highlight bash >}}
% source ~/.rvm/scripts/rvm

% rvm list known

% rvm install 2.4

% rvm --default use 2.4.1

% ruby -v
ruby 2.4.1p111 (2017-03-22 revision 58053) [x86_64-linux]

% which ruby
/home/epsi/.rvm/rubies/ruby-2.4.1/bin/ruby
{{< / highlight >}}

#### Install Gem

{{< highlight bash >}}
% source ~/.rvm/scripts/rvm
% gem install jekyll
{{< / highlight >}}

[![RVM: gem install jekyll ][image-ss-rvm-gem]][photo-ss-rvm-gem]

Do not forget any optional dependencies.

{{< highlight bash >}}
% gem install jekyll-paginate
{{< / highlight >}}

Now you have these path for Jekyll.

* ~/.rvm/gems/ruby-2.4.1/gems/jekyll-3.7.0/exe/jekyll

* ~/.rvm/gems/ruby-2.4.1/bin/jekyll 

#### Running Jekyll

For convenience, add these lines to <code>.bashrc</code> or <code>.zshrc</code>.

{{< highlight bash >}}
export PATH=${PATH}:~/.rvm/gems/ruby-2.4.1/bin/
source ~/.rvm/scripts/rvm
alias jekyll-blog='jekyll serve --config _config.yml,_config_dev.yml'
{{< / highlight >}}

[![RVM: .bashrc or .zshrc][image-ss-rvm-zshrc]][photo-ss-rvm-zshrc]

Now we can run jekyll with either of these three

{{< highlight bash >}}
% ~/.rvm/gems/ruby-2.4.1/gems/jekyll-3.7.0/exe/jekyll serve --config _config.yml,_config_dev.yml

% ~/.rvm/gems/ruby-2.4.1/bin/jekyll serve --config _config.yml,_config_dev.yml

% jekyll-blog
{{< / highlight >}}

[![RVM: Running Jekyll ][image-ss-rvm-jekyll]][photo-ss-rvm-jekyll]

-- -- --

If you desire some details, you may refer to my old post about 

*	[installing Jekyll in Debian][local-jekyll-debian], or 

*	[installing Jekyll in Arch/Manjaro][local-jekyll-arch].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-jekyll-debian]: {{< baseurl >}}webdev/2016/06/23/jekyll-debian-install.html
[local-jekyll-arch]:   {{< baseurl >}}webdev/2016/06/22/jekyll-arch-install.html

[image-ss-gem-jekyll]:      {{< assets-ssg >}}/2018/01/opensuse-gem-install-jekyll.png
[photo-ss-gem-jekyll]:      https://photos.google.com/album/AF1QipONAnoC9vhwhNUX26skiUTYcT1xwz71c10Aqi3M/photo/AF1QipMuNhbTvbu0fJzMNnTw-gUGXinAvHBhuOYDpVWx

[image-ss-ruby-long-path]:  {{< assets-ssg >}}/2018/01//opensuse-jekyll-long-path.png
[photo-ss-ruby-long-path]:  https://photos.google.com/album/AF1QipONAnoC9vhwhNUX26skiUTYcT1xwz71c10Aqi3M/photo/AF1QipP4PtBdXBHluuaDB5erlSuGccoSdIAW_twFwOzQ

[image-ss-set-path]:        {{< assets-ssg >}}/2018/01//opensuse-jekyll-set-path.png
[photo-ss-set-path]:        https://photos.google.com/album/AF1QipONAnoC9vhwhNUX26skiUTYcT1xwz71c10Aqi3M/photo/AF1QipN2mRN53kgUPkgQklLeV5FRBkNi43ZGQk5MVfW3

[image-ss-rvm-deps]:        {{< assets-ssg >}}/2018/01//opensuse-rvm-dependencies.png
[photo-ss-rvm-deps]:        https://photos.google.com/album/AF1QipONAnoC9vhwhNUX26skiUTYcT1xwz71c10Aqi3M/photo/AF1QipMiH_IZ7pF4tOnGiCo3gyIkGJjrvTihnMwzQE4s

[image-ss-rvm-install-1]:   {{< assets-ssg >}}/2018/01//opensuse-rvm-install-1.png
[photo-ss-rvm-install-1]:   https://photos.google.com/album/AF1QipONAnoC9vhwhNUX26skiUTYcT1xwz71c10Aqi3M/photo/AF1QipNB4EGcI81z6rsTeLCt_qzOnkpRFjedkDimTMM4

[image-ss-rvm-install-3]:   {{< assets-ssg >}}/2018/01//opensuse-rvm-install-3.png
[photo-ss-rvm-install-3]:   https://photos.google.com/album/AF1QipONAnoC9vhwhNUX26skiUTYcT1xwz71c10Aqi3M/photo/AF1QipMfVposE81bjLrO_ShXsRrVyBefZ_BXBg-ASUm3

[image-ss-rvm-gem]:         {{< assets-ssg >}}/2018/01//opensuse-rvm-gem-install-jekyll.png
[photo-ss-rvm-gem]:         https://photos.google.com/album/AF1QipONAnoC9vhwhNUX26skiUTYcT1xwz71c10Aqi3M/photo/AF1QipO5vkmrfdInRSfXlX1dGhD_iy4K8Nhog5s96GE0

[image-ss-rvm-zshrc]:       {{< assets-ssg >}}/2018/01//opensuse-rvm-jekyll-zshrc.png
[photo-ss-rvm-zshrc]:       https://photos.google.com/album/AF1QipONAnoC9vhwhNUX26skiUTYcT1xwz71c10Aqi3M/photo/AF1QipNOf9VGgLX8H_1nxyYBIwFhP6yj0N2qJoNPx9CD

[image-ss-rvm-jekyll]:      {{< assets-ssg >}}/2018/01//opensuse-rvm-jekyll-blog.png
[photo-ss-rvm-jekyll]:      https://photos.google.com/album/AF1QipONAnoC9vhwhNUX26skiUTYcT1xwz71c10Aqi3M/photo/AF1QipOo3P08pW_MVz8-IlF8XVft-Bkf9nXdT7mZkrO2
