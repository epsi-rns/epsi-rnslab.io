---
type   : post
title  : "Jekyll Pagination - Summary"
date   : 2019-01-01T09:17:35+07:00
slug   : jekyll-pagination-summary
categories: [ssg]
tags      : [jekyll]
keywords  : [static site, custom theme, configuration, pagination, summary]
author : epsi
opengraph:
  image: assets/site/images/topics/jekyll.png

toc    : "toc-2019-01-jekyll-pagination"

excerpt:
  Building Jekyll Pagination Step by Step, using Bootstrap.
  This is the summary page.

---

### Preface

This article series is intended for beginner,
who already has knowledge about Jekyll.

> Goal: Make Custom Jekyll Pagination Step By Step

I'm using <code>Bootstrap</code> as an example in this guidance.
Of course you can use any CSS framework that you like,
such as <code>Materialize</code> or <code>Bulma</code>.

#### Reading

You should read this, for preparation, before you begin.

* [jekyllrb.com pagination/](https://jekyllrb.com/docs/pagination/)

#### About Jekyll

Jekyll is the default SSG (Static Site Generator),
that is supported by **github**.
Jekyll is not the only **SSG** (Static Site Generator).

#### Disclaimer

This is would not be the best custom pagination that you ever have.

After you learn this guidance,
you understand the fundamental skill.
Thus, a base for you, to make your custom pagination.

#### Source

I respect copyright.
The code below ported from Glenn McComb.
The original code is using Hugo Chroma,
I have made code overhaul while porting,
this Hugo Chroma to Jekyll Liquid.

* <https://glennmccomb.com/articles/how-to-build-custom-hugo-pagination/>

But of course, the logic behind, is remain the same.

-- -- --

### Table of Content

The table content is available as header on each tutorial.
There, I present Jekyll Pagination tutorial, step by step, for beginners.

* 1: Simple Pagination

![Jekyll Pagination: Simple Positioning Indicator][image-ss-02-simple-indicator]

* 2: Number Pagination

![Jekyll Pagination: Stylesheet Before After][image-ss-02-scss-before-after]

* 3: Middle Pagination Only, with complex logic

![Jekyll Pagination: Adjacent Animation][image-ss-02-adjacent-animation]

* 4: Combined (Indicator and Middle Pagination)

![Hugo Pagination: Combined Animation][image-ss-02-combined-animation]

* 5: Responsive, using SASS

![Hugo Pagination: Responsive Animation][image-ss-02-responsive-animation]

* 6: Screenreader

No Preview.

This is basically just where to put <code>sr-only</code> class.

-- -- --

### Running Demo

#### Source Code

Source code used in this tutorial, is available at this repository:

*	[github.com/epsi-rns/demo-jekyll][demo-jekyll-master]

#### Development Config

You should have set your development config as below code:

* <code>_config_dev.yml</code>

{{< highlight yaml >}}
url: "http://localhost:4000"
baseurl: "/demo-jekyll"
{{< / highlight >}}

Then you can run jekyll on terminal.

{{< highlight bash >}}
% jekyll serve --config _config.yml,_config_dev.yml 
{{< / highlight >}}

Then you can preview localhost on your browser.

* http://localhost:4000/demo-jekyll/

Notice the subdirectory <code>/demo-jekyll</code>.

-- -- --

### Prepare Artefact

#### Config

* <code>_config.yaml</code>
  : [gitlab.com/.../_config.yaml][demo-jekyll-config]

{{< highlight yaml >}}
paginate: 7
paginate_path: "pages/blog-:num"
{{< / highlight >}}

#### Using Pagination: Blog List Page

I have already made, a ready to use blog page.
You may switch from one kind of pagination to another.
Each kind of pagination, will be discussed one by one.
One article each.

{{< highlight twig >}}
---
layout: page
paginate_root: /pages
---
...

{% comment %}
  {% include pagination/01-simple.html %}
  {% include pagination/02-number.html %}
  {% include pagination/03-adjacent.html %}
  {% include pagination/04-indicator.html %}
  {% include pagination/05-responsive.html %}
  {% include pagination/06-screenreader.html %}
{% endcomment %}

  {% include pagination/06-screenreader.html %}

  <!-- This loops through the paginated posts -->
<section id="archive">
...
</section>
{{< / highlight >}}

Complete code is here below:

* <code>/pages/index.html</code>
 : [github.com/.../pages/index.html][demo-blog-list-page].
 
And the blog list page will show you as below figure:

![Jekyll Demo: Blog List Page][image-ss-00-blog-list-page]

-- -- --

#### Begin The Guidance

Let's get the tutorial started.

Consider continue reading [ [Jekyll Pagination - Simple][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2019/01/03/jekyll-pagination-simple/

[demo-jekyll-master]:   {{< demo-jekyll >}}/

[image-ss-02-simple-indicator]:     {{< assets-ssg >}}/2018/11/52-simple-indicator-number.png
[image-ss-02-scss-before-after]:    {{< assets-ssg >}}/2018/11/52-number-before-after.png
[image-ss-02-combined-animation]:   {{< assets-ssg >}}/2018/11/52-pagination-combined.gif
[image-ss-02-adjacent-animation]:   {{< assets-ssg >}}/2018/11/52-pagination-adjacent.gif
[image-ss-02-responsive-animation]: {{< assets-ssg >}}/2018/11/52-pagination-responsive.gif
[image-ss-00-blog-list-page]:       {{< assets-ssg >}}/2019/01/00-pagination-blog-list-page.png


[demo-jekyll-config]:               {{< demo-jekyll >}}/config.yaml
[demo-blog-list-page]:              {{< demo-jekyll >}}/pages/index.html
