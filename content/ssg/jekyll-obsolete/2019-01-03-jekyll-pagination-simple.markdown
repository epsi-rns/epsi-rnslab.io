---
type   : post
title  : "Jekyll Pagination - Simple"
date   : 2019-01-03T09:17:35+07:00
slug   : jekyll-pagination-simple
categories: [ssg]
tags      : [jekyll]
keywords  : [static site, custom theme, liquid, pagination, simple]
author : epsi
opengraph:
  image: assets/site/images/topics/jekyll.png

toc    : "toc-2019-01-jekyll-pagination"

excerpt:
  Building Jekyll Pagination Step by Step, using Bootstrap.
  A simple introduction.

---

### Preface

> Goal: A Simple Pagination.

#### Preview

This is what we want to achieve in this tutorial.

![Jekyll Pagination: Simple Positioning Indicator][image-ss-02-simple-indicator]

#### More Recent Code

I have made a better code with Bulma
utilizing `site.pagination.permalink`
which you can examine at:

* [gitlab.com/.../_includes/pagination/01-simple.html][jbmd-pagination-simple]

-- -- --

### 1: Prepare

Artefact that we need.

#### Includes: Pagination: Simple

Create an empty artefact.
Source code used in this tutorial, is available at this repository:

* <code>/_includes/pagination/01-simple.html</code>

#### Pages: Blog List

Set this blog list page:

* <code>/pages/index.html</code>
 : [github.com/.../pages/index.html][demo-blog-list-page].

{{< highlight twig >}}
  {% include pagination/01-simple.html %}
{{< / highlight >}}

#### HTML Preview

The HTML that we want to achieve in this article, is similar as below.

{{< highlight html >}}
<nav aria-label="Page navigation">
  <ul class="pagination justify-content-center">

    <!-- First Page. -->
    <li class="page-item">
      <a class="page-link" href="/demo-jekyll/pages" rel="first">First</a>
    </li>

    <!-- Previous Page. -->
    <li class="page-item">
      <a class="page-link" href="/demo-jekyll/pages/blog-4" rel="prev">Previous</a>
    </li>

    <!-- Indicator Number. -->
    <li class="page-item disabled">
      <span class="page-link">
        Page: 5 of 10</span>
    </li>

    <!-- Next Page. -->
    <li class="page-item">
      <a class="page-link" href="/demo-jekyll/pages/blog-6" rel="next">Next</a>
    </li>

    <!-- Last Page. -->
    <li class="page-item">
      <a class="page-link" href="/demo-jekyll/pages/blog-10" rel="last">Last</a>
    </li>
    
  </ul>
</nav>
{{< / highlight >}}

We will achieve this with Jekyll code.

-- -- --

### 2: Skeleton

#### The Structure

Our simple pagination contain five parts:

* First Page.

* Previous Page.

* Indicator Number.

* Next Page.

* Last Page.

#### Includes: Pagination: Simple

Create an empty artefact.
Source code used in this tutorial, is available at this repository:

* <code>/_includes/pagination/01-simple.html</code>
  : [gitlab.com/.../_includes/pagination/01-simple.html][repo-pagination-simple]

Now you need only the minimum skeleton:

{{< highlight twig >}}
{% capture spaceless %}
{% endcapture %}

<nav aria-label="Page navigation">

  {% if total_pages > 1 %}
  <ul class="pagination justify-content-center">
    <!-- First Page. -->

    <!-- Previous Page. -->

    <!-- Indicator Number. -->

    <!-- Next Page. -->

    <!-- Last Page. -->

  </ul>
  {% endif %}

</nav>
{{< / highlight >}}

Of course, there is no need to show any pagination navigation,
if we what we have is only one page.
This is why we put this <code>if total_pages > 1</code>.

#### Separate Logic

> Avoid useless space.

You might wonder what this <code>capture spaceless</code> is all about.
Well.... You know.. This pagination will require complex logic later.
I need to separate th logic and the view.

In order to avoid useless space,
I put all logic inside this <code>capture spaceless</code>.
You can use any variable name instead of <code>spaceless</code>.

Consider make a variable, for he sake of examples.
Just a short version of <code>paginator.total_pages</code>.
Because we are going to use it few times.

{{< highlight twig >}}
{% capture spaceless %}
  {% assign total_pages = paginator.total_pages %}
{% endcapture %}
{{< / highlight >}}

-- -- --

### 3: Navigation: Previous and Next

Our minimal pagination logic would contain,
only <code>Previous Page</code> and <code>Next Page</code>.

* Previous Page

{{< highlight twig >}}
    <!-- Previous Page. -->
    {% if paginator.previous_page %}
      <li class="page-item">
        <a class="page-link" 
           href="{{ site.baseurl }}{{ paginator.previous_page_path }}" 
           rel="prev">Previous</a>
      </li>
    {% else %}
      <li class="page-item disabled">
        <span class="page-link">Previous</span>
      </li>
    {% endif %}
{{< / highlight >}}

* Next Page

{{< highlight twig >}}
    <!-- Next Page. -->
    {% if paginator.next_page %}
      <li class="page-item">
        <a class="page-link" 
           href="{{ site.baseurl }}{{ paginator.next_page_path }}" 
           rel="next">Next</a>
      </li>
    {% else %}
      <li class="page-item disabled">
        <span class="page-link">Next</span>
      </li>
    {% endif %}
{{< / highlight >}}

Of course this is not enough.
Our minimal pagination should show something.

Notice that the tags:
<code>rel="prev"</code> and <code>rel="next"</code>,
are <code>html4</code> tags.
Both are considered obsolote in <code>html5</code>.
We are going to remove it the next article.

#### Browser: Pagination Preview

![Jekyll Pagination: Simple Previous Next][image-ss-02-simple-prev-next]

#### How does it works ?

This code above rely on 

* Previous Page: <code>if paginator.previous_page</code>, and 

* Next Page: <code>if paginator.next_page</code>.

This should be self explanatory.

#### Stylesheet: Bootstrap Class

Bootstrap specific class are these below

* ul: <code>pagination</code>

* li: <code>page-item</code>

* a, span: <code>page-link</code>

* li: <code>disabled</code>

-- -- --

### 4: Navigation: First and Last

Consider also add first page and last page.

* First Page

{{< highlight twig >}}
    <!-- First Page. -->
    {% unless paginator.page == 1 %}
      <li class="page-item">
        <a class="page-link"
           href="{{ site.baseurl }}{{ page.paginate_root }}" 
           rel="first">First</a>
      </li>
    {% else %}
      <li class="page-item disabled">
        <span class="page-link">First</span>
      </li>
    {% endunless %}
{{< / highlight >}}

* Last Page

{{< highlight twig >}}
    <!-- Last Page. -->
    {% unless paginator.page == total_pages %}
      <li class="page-item">
        <a class="page-link"
           href="{{ site.paginate_path | relative_url | replace: ':num', total_pages }}"
           rel="last">Last</a>
      </li>
    {% else %}
      <li class="page-item disabled">
        <span class="page-link">Last</span>
      </li>
    {% endunless %}
{{< / highlight >}}

#### Browser: Pagination Preview

![Jekyll Pagination: Simple First Last][image-ss-02-simple-first-last]

#### How does it works ?

Since there is no equivalent
for <code>paginator.previous_page</code>
and <code>paginator.next_page</code>
to show first page and last page,
we utilize:

* First Page: <code>paginator.page == 1</code>.

* Last Page: <code>paginator.page == total_pages</code>.

#### Paginate Path

The <code>paginate_path</code> taken from official jekyll pages,
does not require <code>site.baseurl</code>.

Notice this:

{{< highlight twig >}}
href="{{ site.paginate_path | relative_url | replace: ':num', total_pages }}"
{{< / highlight >}}

And not this code below:

{{< highlight twig >}}
href="{{ site.baseurl }}{{ site.paginate_path | relative_url | replace: ':num', total_pages }}"
{{< / highlight >}}

-- -- --

### 5: Active Page Indicator

And also the pagination positioning in between,
in the middle of those.

{{< highlight twig >}}
    <!-- Indicator Number. -->
    <li class="page-item disabled">
      <span class="page-link">
        Page: {{ paginator.page }} of {{ total_pages }}</span>
    </li>
{{< / highlight >}}

#### Browser: Pagination Preview

![Hugo Pagination: Simple Positioning Indicator][image-ss-02-simple-indicator]

#### How does it works ?

This just show:

{{< highlight twig >}}
Page: {{ paginator.page }} of {{ total_pages }}
{{< / highlight >}}

-- -- --

### 6: Summary

Complete code is here below:

* <code>/_includes/pagination/01-simple.html</code>
  : [gitlab.com/.../_includes/pagination/01-simple.html][repo-pagination-simple]

{{< highlight html >}}
{% capture spaceless %}
  {% assign total_pages = paginator.total_pages %}
{% endcapture %}

<nav aria-label="Page navigation">

  {% if total_pages > 1 %}
  <ul class="pagination justify-content-center">
    <!-- First Page. -->
    {% unless paginator.page == 1 %}
      <li class="page-item">
        <a class="page-link"
           href="{{ site.baseurl }}{{ page.paginate_root }}" 
           rel="first">First</a>
      </li>
    {% else %}
      <li class="page-item disabled">
        <span class="page-link">First</span>
      </li>
    {% endunless %}

    <!-- Previous Page. -->
    {% if paginator.previous_page %}
      <li class="page-item">
        <a class="page-link" 
           href="{{ site.baseurl }}{{ paginator.previous_page_path }}" 
           rel="prev">Previous</a>
      </li>
    {% else %}
      <li class="page-item disabled">
        <span class="page-link">Previous</span>
      </li>
    {% endif %}

    <!-- Indicator Number. -->
    <li class="page-item disabled">
      <span class="page-link">
        Page: {{ paginator.page }} of {{ total_pages }}</span>
    </li>

    <!-- Next Page. -->
    {% if paginator.next_page %}
      <li class="page-item">
        <a class="page-link" 
           href="{{ site.baseurl }}{{ paginator.next_page_path }}" 
           rel="next">Next</a>
      </li>
    {% else %}
      <li class="page-item disabled">
        <span class="page-link">Next</span>
      </li>
    {% endif %}

    <!-- Last Page. -->
    {% unless paginator.page == total_pages %}
      <li class="page-item">
        <a class="page-link"
           href="{{ site.paginate_path | relative_url | replace: ':num', total_pages }}"
           rel="last">Last</a>
      </li>
    {% else %}
      <li class="page-item disabled">
        <span class="page-link">Last</span>
      </li>
    {% endunless %}
  </ul>
  {% endif %}

</nav>
{{< / highlight >}}

-- -- --

#### What is Next ?

Consider continue reading [ [Jekyll Pagination - Number][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2019/01/04/jekyll-pagination-number/

[repo-pagination-simple]:   {{< demo-jekyll >}}/_includes/pagination/01-simple.html
[jbmd-pagination-simple]:   {{< jekyll-bulma-md >}}/tutor-06/_includes/pagination-v2/01-simple.html
[demo-blog-list-page]:      {{< demo-jekyll >}}/pages/index.html

[image-ss-07-demo-hugo]:    {{< assets-ssg >}}/2018/12/67-demo-hugo.png

[image-ss-02-simple-indicator]:     {{< assets-ssg >}}/2018/11/52-simple-indicator-number.png

[image-ss-02-simple-prev-next]:     {{< assets-ssg >}}/2018/11/52-simple-prev-next.png
[image-ss-02-simple-first-last]:    {{< assets-ssg >}}/2018/11/52-simple-first-last.png
