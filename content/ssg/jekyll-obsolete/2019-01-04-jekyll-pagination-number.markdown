---
type   : post
title  : "Jekyll Pagination - Number"
date   : 2019-01-04T09:17:35+07:00
slug   : jekyll-pagination-number
categories: [ssg]
tags      : [jekyll]
keywords  : [static site, custom theme, liquid, pagination, numbering]
author : epsi
opengraph:
  image: assets/site/images/topics/jekyll.png

toc    : "toc-2019-01-jekyll-pagination"

excerpt:
  Building Jekyll Pagination Step by Step, using Bootstrap.
  Pagination for dummies, list all of page.

---

### Preface

> Goal: Pagination using Number.

#### Preview

This is what we want to achieve in this tutorial.

![Jekyll Pagination: Stylesheet Before After][image-ss-02-scss-before-after]

#### More Recent Code

I have made a better code with Bulma
utilizing `site.pagination.permalink`
which you can examine at:

* [gitlab.com/.../_includes/pagination/02-number.html][jbmd-pagination-number]

-- -- --

### 1: Prepare

Artefact that we need.

#### Includes: Pagination: Number

Create an empty artefact.
Source code used in this tutorial, is available at this repository:

* <code>/_includes/pagination/02-number.html</code>

#### Pages: Blog List

Set this blog list page:

* <code>/pages/index.html</code>
 : [github.com/.../pages/index.html][demo-blog-list-page].

{{< highlight twig >}}
  {% include pagination/02-number.html %}
{{< / highlight >}}

-- -- --

### 2: Skeleton

#### The Structure

Our number pagination contain three parts:

* Previous Page.

* Indicator Number.

* Next Page.

We have already had,
this <code>First</code>, and <code>Last</code>, in number.
So we do not require it.

#### HTML Preview

The HTML that we want to achieve in this article, is similar as below.

{{< highlight html >}}
<nav aria-label="Page navigation">
  <ul class="pagination justify-content-center">

    <!-- Previous Page. -->
    <li class="page-item blog_previous">
      <a class="page-link" href="/demo-jekyll/pages/blog-4" rel="prev">«</a>
    </li>

    <!-- Page numbers. -->
    <li class="page-item ">
        <a class="page-link" href="/demo-jekyll/pages">1</a>
    </li>
    <li class="page-item ">
        <a class="page-link" href="/demo-jekyll/pages/blog-2">2</a>
    </li>    
    <li class="page-item ">
        <a class="page-link" href="/demo-jekyll/pages/blog-3">3</a>
    </li>    
    <li class="page-item ">
        <a class="page-link" href="/demo-jekyll/pages/blog-4">4</a>
    </li>
    
    <li class="page-item  active">
        <span class="page-link">5</span>
    </li>
    
    <li class="page-item ">
        <a class="page-link" href="/demo-jekyll/pages/blog-6">6</a>
    </li>    
    <li class="page-item ">
        <a class="page-link" href="/demo-jekyll/pages/blog-7">7</a>
    </li>    
    <li class="page-item ">
        <a class="page-link" href="/demo-jekyll/pages/blog-8">8</a>
    </li>
    <li class="page-item ">
        <a class="page-link" href="/demo-jekyll/pages/blog-9">9</a>
    </li>
    
    <li class="page-item ">
        <a class="page-link" href="/demo-jekyll/pages/blog-10">10</a>
    </li>

    <!-- Next Page. -->
    <li class="page-item blog_next">
      <a class="page-link" href="/demo-jekyll/pages/blog-6" rel="next">»</a>
    </li>
  </ul>

</nav>
{{< / highlight >}}

We will achieve this with Jekyll code.

-- -- --

### 3: Navigation: Number

Pagination by number is also simple.

#### Partial: Pagination Number

{{< highlight twig >}}
{% capture spaceless %}
  {% if page.paginate_root == nil %}
    {% assign paginate_root = "/" %}
  {% else %}    
    {% assign paginate_root = page.paginate_root %}
  {% endif %}

  {% assign total_pages = paginator.total_pages %}
  {% assign page_number = paginator.page %}
{% endcapture %}

<nav aria-label="Page navigation">
  {% if total_pages > 1 %}
  <ul class="pagination justify-content-center">

    <!-- Page numbers. -->
    {% for page in (1..total_pages) %}
    <li class="page-item {% if page == page_number %} active{% endif %}">
      {% if page == page_number %} 
        <span class="page-link">
          {{ page }}
        </span>
      {% elsif page == 1 %}
        <a class="page-link"
           href="{{ site.baseurl }}{{ paginate_root }}"
        >1</a>
      {% else %}
        <a class="page-link"
           href="{{ site.paginate_path | relative_url | replace: ':num', page }}"
        >{{ page }}
        </a>
      {% endif %}
    </li>
    {% endfor %}
</nav>
{{< / highlight >}}

#### Case of Page in Loop

There are three cases for this pagination

* **Active Page**: No hyperlink.
  With additional<code>active</code> class.

{{< highlight twig >}}
  <span class="page-link">
    {{ page }}
  </span>
{{< / highlight >}}

* **First Page**: Because Jekyll use base path
  that different with <code>paginate_path</code>.

{{< highlight twig >}}
  <a class="page-link"
     href="{{ site.baseurl }}{{ paginate_root }}"
  >1</a>
{{< / highlight >}}

* **The Rest**: using <code>paginate_path</code>.

{{< highlight twig >}}
  <a class="page-link"
     href="{{ site.paginate_path | relative_url | replace: ':num', page }}"
  >{{ page }}
{{< / highlight >}}

#### Separate Logic

> paginate_root lost in loop

There is a little issue, that the value of <code>paginate_root</code>,
become <code>nil</code> inside the loop. So we have to define it,
in a variable, in the first place.

{{< highlight twig >}}
{% capture spaceless %}
  {% if page.paginate_root == nil %}
    {% assign paginate_root = "/" %}
  {% else %}    
    {% assign paginate_root = page.paginate_root %}
  {% endif %}

  {% assign total_pages = paginator.total_pages %}
  {% assign page_number = paginator.page %}
{% endcapture %}
{{< / highlight >}}

Now we can have the correct value of <code>paginate_root</code>.

#### Browser: Pagination Preview

![Hugo Pagination: Number List][image-ss-02-number-list]

#### How does it works ?

Just a simple loop.

{{< highlight twig >}}
    {% for page in (1..total_pages) %}
    <li class="page-item {% if page == page_number %} active{% endif %}">
      ...
    </li>
    {% endfor %}
{{< / highlight >}}

-- -- --

### 4: Navigation: Previous and Next

Consider give it a <code>prev</code> and <code>next</code> button.
The code is very similar with our previous article,
with a few differences.

* Previous Page

{{< highlight twig >}}
    <!-- Previous Page. -->
    {% if paginator.previous_page %}
      <li class="page-item blog_previous">
        <a class="page-link" 
           href="{{ site.baseurl }}{{ paginator.previous_page_path }}"
           rel="prev">&laquo;</a>
      </li>
    {% else %}
      <li class="page-item blog_previous disabled">
        <span class="page-link">&laquo;</span>
      </li>
    {% endif %}
{{< / highlight >}}

* Next Page

{{< highlight twig >}}
    <!-- Next Page. -->
    {% if paginator.next_page %}
      <li class="page-item blog_next">
        <a class="page-link" 
           href="{{ site.baseurl }}{{ paginator.next_page_path }}" 
           rel="next">&raquo;</a>
      </li>
    {% else %}
      <li class="page-item blog_next disabled">
        <span class="page-link">&raquo;</span>
      </li>
    {% endif %}
{{< / highlight >}}

#### Browser: Pagination Preview

![Hugo Pagination: Symbol Previous Next][image-ss-02-number-laquo-raquo]

#### Changes

That code above is using:

* symbol <code>&laquo;</code> instead of <code>previous</code> word.

* symbol <code>&raquo;</code> instead of <code>next</code> word.

Notice that, we also have additional stylesheet named 

* blog_previous, and 

* blog_next

#### Complete Source

Complete source code for this artefact available at:

* <code>/_includes/pagination/02-number.html</code>
  : [gitlab.com/.../_includes/pagination/02-number.html][repo-pagination-number]

-- -- --

### 5: Stylesheet: Before and After

Consider define these pagination stylesheet:

* <code>_sass/themes/oriclone/_pagination.scss</code>
  : [gitlab.com/.../_pagination.scss][tutor-jekyll-sass-pagination].

{{< highlight scss >}}
.blog_previous {
  span:after,
  a:after {
    content: " previous"
  }
}

.blog_next {
  span:before,
  a:before {
    content: "next "
  }
}
{{< / highlight >}}

And add the new pagination scss in main file.

* <code>_sass/themes/oriclone/main.scss</code>
  : [gitlab.com/.../main.scss][tutor-jekyll-sass-main].

{{< highlight scss >}}
@import
  ...

  // custom: general
    "layout",
    "decoration",
    "stripes",
    "list",
    "pagination",

  ...
;
{{< / highlight >}}

#### Browser: Pagination Preview

![Hugo Pagination: Stylesheet Before After][image-ss-02-scss-before-after]

-- -- --

### 6: Stylesheet: Responsive

#### Pure CSS

You can make this responsive by using standard css

{{< highlight scss >}}
@media only screen and (min-width: 768px) {
  ...
}
{{< / highlight >}}

Or as complete scss would be:

{{< highlight scss >}}
@media only screen and (min-width: 768px) {

  .blog_previous {
    span:after,
    a:after {
      content: " previous"
    }
  }

  .blog_next {
    span:before,
    a:before {
      content: "next "
    }
  }

}
{{< / highlight >}}

#### Bootstrap Mixins

By the help of <code>mixins/breakpoints</code>,
we can rewrite it in bootstrap style.

{{< highlight scss >}}
@include media-breakpoint-up(md) {
  ...
}
{{< / highlight >}}

Or as complete <code>_pagination.scss</code> would be as below:

* <code>_sass/themes/oriclone/_pagination.scss</code>
  : [gitlab.com/.../_pagination.scss][demo-jekyll-sass-pagination].

{{< highlight scss >}}
@include media-breakpoint-up(md) {

  .blog_previous {
    span:after,
    a:after {
      content: " previous"
    }
  }

  .blog_next {
    span:before,
    a:before {
      content: "next "
    }
  }

}
{{< / highlight >}}

Do not forget to add mixins/breakpoints.

{{< highlight scss >}}
@import "vendors/bootstrap/mixins/breakpoints";
{{< / highlight >}}

So we have the complete <code>main.scss</code> as below.

* <code>_sass/themes/oriclone/main.scss</code>
  : [gitlab.com/.../main.scss][demo-jekyll-sass-main].

{{< highlight scss >}}
@import
  // variables
    "vendors/bootstrap/functions",
    "variables",
    "vendors/bootstrap/variables",
    "vendors/bootstrap/mixins/breakpoints",
    "vendors/bootstrap/mixins/image",
    "vendors/bootstrap/mixins/gradients",
    "vendors/bootstrap/mixins/transition",
    "vendors/bootstrap/mixins/box-shadow",

  // taken from bootstrap
    "sticky-footer-navbar",
    "bootstrap-custom",

  // custom: general
    "layout",
    "decoration",
    "stripes",
    "list",
    "pagination",

  // custom: post
    "post-navigation",
    "post-calendar",
    "post-header",
    "post-content",
    "post-code",
    "post-highlight-jekyll"
;
{{< / highlight >}}

-- -- --

### 7: Math: Algebra

The next article require complex logic.
thus, we need to approach mathematic right away.

#### Assumption

Consider an example, a blog post contain **ten** posts.

In Jekyll Liquid we have:

{{< highlight conf >}}
  {% assign total_pages = paginator.total_pages %}
{{< / highlight >}}

For this math example, we can write the variable as below:

{{< highlight conf >}}
# CONST

$totalPost   = 10
{{< / highlight >}}

#### Table

We can also achieve <code>$totalPages</code> by **ceiling division**.
And the result is on this table below.

{{< highlight conf >}}
# ALGEBRA

+--------------+-------+-------+-------+-------+-------+
| $pagination  |   1   |   2   |   5   |   7   |  10   |
+--------------+-------+-------+-------+-------+-------+
| VARIABLE                                             |
| $division    | 10.0  |  5.0  |  2.0  |  1.4  |   1   |
| $totalPages  |  10   |   5   |   2   |   2   |  N/A  |
+--------------+-------+-------+-------+-------+-------+
{{< / highlight >}}

Of course, we do not need to show any pagination,
if there is only one page for all result.
That is why we can optionally,
convert <code>1</code> into <code>N/A</code>.

-- -- --

#### What is Next ?

Consider continue reading [ [Jekyll Pagination - Adjacent][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2019/01/05/jekyll-pagination-adjacent/

[repo-pagination-number]:   {{< demo-jekyll >}}/_includes/pagination/02-number.html
[jbmd-pagination-number]:   {{< jekyll-bulma-md >}}/tutor-06/_includes/pagination-v2/02-number.html
[demo-blog-list-page]:      {{< demo-jekyll >}}/pages/index.html

[image-ss-07-demo-hugo]:    {{< assets-ssg >}}/2018/12/67-demo-hugo.png

[image-ss-02-scss-before-after]:    {{< assets-ssg >}}/2018/11/52-number-before-after.png
[image-ss-02-number-laquo-raquo]:   {{< assets-ssg >}}/2018/11/52-number-laquo-raquo.png
[image-ss-02-number-list]:          {{< assets-ssg >}}/2018/11/52-number-list.png

[demo-jekyll-sass-main]:            {{< demo-jekyll >}}/_sass/themes/oriclone/main.scss
[demo-jekyll-sass-pagination]:      {{< demo-jekyll >}}/_sass/themes/oriclone/_pagination.scss

