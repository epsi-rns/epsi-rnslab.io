---
type   : post
title  : "Jekyll Pagination - Indicator"
date   : 2019-01-06T09:17:35+07:00
slug   : jekyll-pagination-indicator
categories: [ssg]
tags      : [jekyll]
keywords  : [static site, custom theme, liquid, middle pagination, indicator]
author : epsi
opengraph:
  image: assets/site/images/topics/jekyll.png

toc    : "toc-2019-01-jekyll-pagination"

excerpt:
  Building Jekyll Pagination Step by Step, using Bootstrap.
  Middle pagination. Not so for dummies.
  Combined with Indicator.

---

### Preface

> Goal: Add indicator, and putting all pagination part together.

#### Preview

This is what we want to achieve in this tutorial.

![Hugo Pagination: Combined Animation][image-ss-02-combined-animation]

#### More Recent Code

I have made a better code with Bulma
utilizing `site.pagination.permalink`
which you can examine at:

* [gitlab.com/.../_includes/pagination/04-indicator.html][jbmd-pagination-indicator]

-- -- --

### 1: Prepare

Artefact that we need.

#### Source

I respect copyright.
Most of the code below, especially middle pagination,
ported from Hugo Chroma to Jekyll Liquid:

* <https://glennmccomb.com/articles/how-to-build-custom-hugo-pagination/>

The rest is my custom modification.

#### Includes: Pagination: Combined

Create an empty artefact, to combine adjacent and indicator.
Source code used in this tutorial, is available at this repository:

* <code>/_includes/pagination/04-indicator.html</code>
 : [gitlab.com/.../partials/pagination-indicator.html][repo-pagination-indicator].

You should have this minimal code, before you begin.

{{< highlight twig >}}
{% capture spaceless %}
  ...
{% endcapture %}

<nav aria-label="Page navigation">

  {% if total_pages > 1 %}
  <ul class="pagination justify-content-center">

  ...

  </ul>
  {% endif %}

</nav>
{{< / highlight >}}

#### Pages: Blog List

Set this blog list page:

* <code>/pages/index.html</code>
 : [github.com/.../pages/index.html][demo-blog-list-page].

{{< highlight twig >}}
  {% include pagination/04-indicator.html %}
{{< / highlight >}}

We will achieve this with Jekyll code.

-- -- --

### 2: Preview: General

#### Structure

This consist of at least seven parts:

* Previous Page: **&laquo;**

* First Page: always 1

* Left Indicator

* Middle Pagination: **Glenn McComb**

* Right Indicator

* Last Page: always the same number

* Next Page: **&raquo;**

We will not discuss about **Middle Pagination**,
as it has already been discussed in previous article.

#### HTML Preview

The HTML that we want to achieve is similar as below.

{{< highlight html >}}
  <ul class="pagination justify-content-center">
      <li class="page-item blog_previous">...</li>
      <li class="page-item first">...</li>
      <li class="pages-indicator first disabled">...</li>

      <li class="page-item">...</li>
      <li class="page-item">...</li>
      <li class="page-item active ">...</li>
      <li class="page-item">...</li>
      <li class="page-item">...</li>

      <li class="pages-indicator last disabled">...</li>
      <li class="page-item last">...</li>
      <li class="page-item blog_next">...</li>
  </ul>
{{< / highlight >}}

#### Small Preview

This is the complete version.

![Hugo Pagination: Combined Animation][image-ss-02-combined-animation]

#### Wide Preview

Wide version, in responsive context, is slightly different.

![Hugo Pagination: Preview Wide][image-ss-02-preview-wide]

We will use responsive CSS later to achieve this effect.

#### Partial: Pagination Code Skeleton

As usual, the skeleton, to show the complexity.

* <code>/_includes/pagination/04-indicator.html</code>
 : [gitlab.com/.../partials/pagination/04-indicator.html][repo-pagination-indicator].

{{< highlight twig >}}
<nav aria-label="Page navigation">
{% capture spaceless %}
  <!-- Variable Initialization. -->
{% endcapture %}

  {% if total_pages > 1 %}
  <ul class="pagination justify-content-center">

    <!-- Previous Page. -->
    <!-- First Page. -->
    <!-- Early (More Pages) Indicator. -->

    {% for page in (1..total_pages) %}
      <!-- Flag Calculation -->
      {% assign page_current_flag = false %}

      {% if total_pages > link_max %}
        <!-- Complex page numbers. -->
      
          <!-- Lower limit pages. -->
          <!-- Upper limit pages. -->
          <!-- Middle pages. -->

      {% else %}
        <!-- Simple page numbers. -->
      {% endif %}

      {% if page_current_flag == true %}
        <!-- Show Pager. -->
      {% endif %}
    {% endfor %}

    <!-- Late (More Pages) Indicator. -->
    <!-- Last Page. -->
    <!-- Next Page. -->

  </ul>
  {% endif %}

</nav>
{{< / highlight >}}

#### Each Pagination

Consider again, have a look at the animation above, frame by frame.

We have from first page (1), to last page (10).

![Hugo Pagination: Adjacent Page 1][image-ss-02-combined-01]

![Hugo Pagination: Adjacent Page 2][image-ss-02-combined-02]

![Hugo Pagination: Adjacent Page 3][image-ss-02-combined-03]

![Hugo Pagination: Adjacent Page 4][image-ss-02-combined-04]

![Hugo Pagination: Adjacent Page 5][image-ss-02-combined-05]

![Hugo Pagination: Adjacent Page 6][image-ss-02-combined-06]

![Hugo Pagination: Adjacent Page 7][image-ss-02-combined-07]

![Hugo Pagination: Adjacent Page 8][image-ss-02-combined-08]

![Hugo Pagination: Adjacent Page 9][image-ss-02-combined-09]

![Hugo Pagination: Adjacent Page 10][image-ss-02-combined-10]

-- -- --

### 3: Variables: Initialization

Consider refresh our memory, about our variables initialization.

{{< highlight twig >}}
{% capture spaceless %}

  {% comment %}
  Pagination links 
  * https://glennmccomb.com/articles/how-to-build-custom-hugo-pagination/
  {% endcomment %}

  {% if page.paginate_root == nil %}
    {% assign paginate_root = "/" %}
  {% else %}    
    {% assign paginate_root = page.paginate_root %}
  {% endif %}

  {% assign total_pages   = paginator.total_pages %}
  {% assign page_current  = paginator.page %}

  {% assign link_offset   = 2 %}  
  {% assign link_max      = link_offset | times: 2 | plus: 1 %}
  
  {% assign limit_lower   = link_offset | plus: 1 %}
  {% assign limit_upper   = total_pages  | minus: link_offset %}
  
  {% assign min_lower     = link_max %}  
  {% assign max_upper     = total_pages | minus: link_max %}
    
  {% assign lower_offset  = page_current | minus: link_offset %}
  {% assign upper_offset  = page_current | plus: link_offset %}

  {% assign lower_indicator = 2 %}
  {% assign upper_indicator = total_pages | minus: 1 %}

{% endcapture %}
{{< / highlight >}}

-- -- --

### 4: Navigation: Previous and Next

It is similar to our simple pagination.
Except that, now we use dictionary.

#### Navigation: Previous

{{< highlight twig >}}
    <!-- Previous Page. -->
    {% if paginator.previous_page %}
      <li class="page-item blog_previous">
        <a class="page-link" 
           href="{{ site.baseurl }}{{ paginator.previous_page_path }}"
           rel="prev">&laquo;</a>
      </li>
    {% else %}
      <li class="page-item blog_previous disabled">
        <span class="page-link">&laquo;</span>
      </li>
    {% endif %}
{{< / highlight >}}

#### Navigation: Next

{{< highlight twig >}}
    <!-- Next Page. -->
    {% if paginator.next_page %}
      <li class="page-item blog_next">
        <a class="page-link" 
           href="{{ site.baseurl }}{{ paginator.next_page_path }}" 
           rel="next">&raquo;</a>
      </li>
    {% else %}
      <li class="page-item blog_next disabled">
        <span class="page-link">&raquo;</span>
      </li>
    {% endif %}
{{< / highlight >}}

-- -- --

### 5: Navigation: First and Last

It is different to our simple pagination.
Although it is based on the same logic.

#### Navigation: First

This will not be shown, if it is already be shown middle pagination.

{{< highlight twig >}}
    {% if total_pages > link_max %}
      <!-- First Page. -->
      {% if lower_offset > 1 %}
        <li class="page-item first">
          <a class="page-link"
             href="{{ site.baseurl }}{{ page.paginate_root }}">1</a>
        </li>
      {% endif %}
    {% endif %}
{{< / highlight >}}

#### Navigation: Last

This will not be shown, if it is already be shown middle pagination.

{{< highlight twig >}}
    {% if total_pages > link_max %}
      <!-- Last Page. -->
      {% if upper_offset < total_pages %}
        <li class="page-item last">
          <a class="page-link" 
             href="{{ site.paginate_path | relative_url | replace: ':num', total_pages }}"
          >{{ total_pages }}</a>
        </li>
      {% endif %}
    {% endif %}
{{< / highlight >}}

-- -- --

### 6: Indicator: Left and Right

This will only be shown, if necessary.

#### Indicator: Left

{{< highlight twig >}}
    {% if total_pages > link_max %}
      <!-- Early (More Pages) Indicator. -->
      {% if lower_offset > lower_indicator %}
        <li class="pages-indicator first disabled">
          <span class="page-link">...</span>
        </li>
      {% endif %}
    {% endif %}
{{< / highlight >}}

#### Indicator: Right

{{< highlight twig >}}
    {% if total_pages > link_max %}
      <!-- Late (More Pages) Indicator. -->
      {% if upper_offset < upper_indicator %}
        <li class="pages-indicator last disabled">
          <span class="page-link">...</span>
        </li>
      {% endif %}
    {% endif %}
{{< / highlight >}}

-- -- --

### 8: Combined Code

It is about the right time to put all the code together.

![Hugo Pagination: Combined Animation][image-ss-02-combined-animation]

* <code>/_includes/pagination/04-indicator.html</code>
 : [gitlab.com/.../partials/pagination/04-indicator.html][repo-pagination-indicator].

{{< highlight twig >}}
{% capture spaceless %}

  {% comment %}
  Pagination links 
  * https://glennmccomb.com/articles/how-to-build-custom-hugo-pagination/
  {% endcomment %}

  {% if page.paginate_root == nil %}
    {% assign paginate_root = "/" %}
  {% else %}    
    {% assign paginate_root = page.paginate_root %}
  {% endif %}

  {% assign total_pages   = paginator.total_pages %}
  {% assign page_current  = paginator.page %}

  {% assign link_offset   = 2 %}  
  {% assign link_max      = link_offset | times: 2 | plus: 1 %}
  
  {% assign limit_lower   = link_offset | plus: 1 %}
  {% assign limit_upper   = total_pages  | minus: link_offset %}
  
  {% assign min_lower     = link_max %}  
  {% assign max_upper     = total_pages | minus: link_max %}
    
  {% assign lower_offset  = page_current | minus: link_offset %}
  {% assign upper_offset  = page_current | plus: link_offset %}

  {% assign lower_indicator = 2 %}
  {% assign upper_indicator = total_pages | minus: 1 %}

{% endcapture %}

<nav aria-label="Page navigation">

  {% if total_pages > 1 %}
  <ul class="pagination justify-content-center">
    <!-- Previous Page. -->
    {% if paginator.previous_page %}
      <li class="page-item blog_previous">
        <a class="page-link" 
           href="{{ site.baseurl }}{{ paginator.previous_page_path }}"
           rel="prev">&laquo;</a>
      </li>
    {% else %}
      <li class="page-item blog_previous disabled">
        <span class="page-link">&laquo;</span>
      </li>
    {% endif %}

    {% if total_pages > link_max %}
      <!-- First Page. -->
      {% if lower_offset > 1 %}
        <li class="page-item first">
          <a class="page-link"
             href="{{ site.baseurl }}{{ page.paginate_root }}">1</a>
        </li>
      {% endif %}

      <!-- Early (More Pages) Indicator. -->
      {% if lower_offset > lower_indicator %}
        <li class="pages-indicator first disabled">
          <span class="page-link">...</span>
        </li>
      {% endif %}
    {% endif %}

    <!-- Page numbers. -->
    {% for page in (1..total_pages) %}
    
    {% capture spaceless %}
      {% assign page_current_flag = false %}

      {% if total_pages > link_max %}

        {% if page_current <= limit_lower %}
          {% if page <= min_lower %}
            {% assign page_current_flag = true %}
          {% endif %}

        {% elsif page_current >= limit_upper %}
          {% if page > max_upper %}
            {% assign page_current_flag = true %}
          {% endif %}

        {% else %}
          
          {% if (page >= lower_offset) and (page <= upper_offset) %}
            {% assign page_current_flag = true %}
          {% endif %}

        {% endif %}

      {% else %}
      
        {% assign page_current_flag = true %}
      {% endif %}
    {% endcapture %}
    
      <!-- Show Pager. -->
      {% if page_current_flag == true %}
      <li class="page-item {% if page == page_current %} active{% endif %}">
        {% if page == page_current %} 
          <span class="page-link">
            {{ page }}
          </span>
        {% elsif page == 1 %}
          <a class="page-link"
             href="{{ site.baseurl }}{{ paginate_root }}"
           >1</a>
        {% else %}
          <a class="page-link"
             href="{{ site.paginate_path | relative_url | replace: ':num', page }}"
           >{{ page }}
          </a>
        {% endif %}
      </li>
      {% endif %}
    {% endfor %}

    {% if total_pages > link_max %}
      <!-- Late (More Pages) Indicator. -->
      {% if upper_offset < upper_indicator %}
        <li class="pages-indicator last disabled">
          <span class="page-link">...</span>
        </li>
      {% endif %}

      <!-- Last Page. -->
      {% if upper_offset < total_pages %}
        <li class="page-item last">
          <a class="page-link" 
             href="{{ site.paginate_path | relative_url | replace: ':num', total_pages }}"
          >{{ total_pages }}</a>
        </li>
      {% endif %}
    {% endif %}

    <!-- Next Page. -->
    {% if paginator.next_page %}
      <li class="page-item blog_next">
        <a class="page-link" 
           href="{{ site.baseurl }}{{ paginator.next_page_path }}" 
           rel="next">&raquo;</a>
      </li>
    {% else %}
      <li class="page-item blog_next disabled">
        <span class="page-link">&raquo;</span>
      </li>
    {% endif %}
  </ul>
  {% endif %}

</nav>
{{< / highlight >}}

> Pretty long code, sure.

-- -- --

### 9: Responsive: Simple

Before we continue to the next pagination article about responsive,
consider having this very simple example using bootstrap.

![Hugo Pagination: Preview Wide][image-ss-02-preview-wide]

This will show the word **previous** and **next** for medium page.

#### SASS: Custom Pagination

I'm using Bootstrap 4 grid breakpoints.

* <code>_sass/themes/oriclone/_pagination.scss</code>
  : [gitlab.com/.../_pagination.scss][demo-jekyll-sass-pagination].

{{< highlight css >}}
@include media-breakpoint-up(md) {

  .blog_previous {
    span.page-link:after,
    a.page-link:after {
      content: " previous"
    }
  }

  .blog_next {
    span.page-link:before,
    a.page-link:before {
      content: "next "
    }
  }

}
{{< / highlight >}}

#### SASS: Main

* <code>_sass/themes/oriclone/main.scss</code>
  : [gitlab.com/.../main.scss][demo-jekyll-sass-main].

{{< highlight scss >}}
@import
  // variables
    "vendors/bootstrap/functions",
    "variables",
    "vendors/bootstrap/variables",
    "vendors/bootstrap/mixins/breakpoints",
    "vendors/bootstrap/mixins/image",
    "vendors/bootstrap/mixins/gradients",
    "vendors/bootstrap/mixins/transition",
    "vendors/bootstrap/mixins/box-shadow",

  // taken from bootstrap
    "sticky-footer-navbar",
    "bootstrap-custom",

  // custom: general
    "layout",
    "decoration",
    "stripes",
    "list",
    "pagination",

  // custom: post
    "post-navigation",
    "post-calendar",
    "post-header",
    "post-content",
    "post-code",
    "post-highlight-jekyll"
;

{{< / highlight >}}

#### SASS: Bootstrap Grid Breakpoint Variables.

Bootstrap 4 grid breakpoints are defined as below.

* <code>_sass/vendors/bootstrap/_variables.scss</code>

{{< highlight scss >}}
// Grid breakpoints
//
// Define the minimum dimensions at which your layout will change,
// adapting to different screen sizes, for use in media queries.

$grid-breakpoints: (
  xs: 0,
  sm: 576px,
  md: 768px,
  lg: 992px,
  xl: 1200px
) !default;
{{< / highlight >}}

-- -- --

#### What is Next ?

Consider continue reading [ [Jekyll Pagination - Responsive][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2019/01/07/jekyll-pagination-responsive/

[repo-pagination-indicator]:    {{< demo-jekyll >}}/_includes/pagination/04-indicator.html
[jbmd-pagination-indicator]:    {{< jekyll-bulma-md >}}/tutor-06/_includes/pagination-v2/04-indicator.html
[demo-blog-list-page]:      {{< demo-jekyll >}}/pages/index.html

[image-ss-07-demo-hugo]:    {{< assets-ssg >}}/2018/12/67-demo-hugo.png

[image-ss-02-combined-animation]:   {{< assets-ssg >}}/2018/11/52-pagination-combined.gif

[image-ss-02-combined-animation]: {{< assets-ssg >}}/2018/11/52-pagination-combined.gif
[image-ss-02-preview-wide]:       {{< assets-ssg >}}/2018/11/52-indicator-wide-blue.png

[image-ss-02-combined-01]:        {{< assets-ssg >}}/2018/11/52-adjacent-combined-01.png
[image-ss-02-combined-02]:        {{< assets-ssg >}}/2018/11/52-adjacent-combined-02.png
[image-ss-02-combined-03]:        {{< assets-ssg >}}/2018/11/52-adjacent-combined-03.png
[image-ss-02-combined-04]:        {{< assets-ssg >}}/2018/11/52-adjacent-combined-04.png
[image-ss-02-combined-05]:        {{< assets-ssg >}}/2018/11/52-adjacent-combined-05.png
[image-ss-02-combined-06]:        {{< assets-ssg >}}/2018/11/52-adjacent-combined-06.png
[image-ss-02-combined-07]:        {{< assets-ssg >}}/2018/11/52-adjacent-combined-07.png
[image-ss-02-combined-08]:        {{< assets-ssg >}}/2018/11/52-adjacent-combined-08.png
[image-ss-02-combined-09]:        {{< assets-ssg >}}/2018/11/52-adjacent-combined-09.png
[image-ss-02-combined-10]:        {{< assets-ssg >}}/2018/11/52-adjacent-combined-10.png

[demo-jekyll-sass-main]:          {{< demo-jekyll >}}/_sass/themes/oriclone/main.scss
[demo-jekyll-sass-pagination]:    {{< demo-jekyll >}}/_sass/themes/oriclone/_pagination.scss
