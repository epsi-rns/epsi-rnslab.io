---
type   : post
title  : "Jekyll Pagination - Responsive"
date   : 2019-01-07T09:17:35+07:00
slug   : jekyll-pagination-responsive
categories: [ssg]
tags      : [jekyll]
keywords  : [static site, custom theme, liquid, pagination, responsive breakpoints]
author : epsi
opengraph:
  image: assets/site/images/topics/jekyll.png

toc    : "toc-2019-01-jekyll-pagination"

excerpt:
  Building Jekyll Pagination Step by Step, using Bootstrap.
  Responsive pagination using Bootstrap. Not so for dummies.

---

### Preface

> Goal: Bringing responsive pagination, using mobile first.

#### Preview

This is what we want to achieve in this tutorial.

![Hugo Pagination: Responsive Animation][image-ss-02-responsive-animation]

#### More Recent Code

I have made a better code with Bulma
utilizing `site.pagination.permalink`
which you can examine at:

* [gitlab.com/.../_includes/pagination/05-responsive.html][jbmd-pagination-responsive]

-- -- --

### 1: Prepare

Artefact that we need.
We should switch our focus,
to stylesheet using SASS instead of Jekyll Liquid.

#### Source

I respect copyright.
The SASS code below inspired by:

* https://www.timble.net/blog/2015/05/better-pagination-for-jekyll/

* https://github.com/timble/jekyll-pagination

I made a slight modification.
But of course the logic remain the same.


#### Includes: Pagination: Responsive

Create an empty artefact.
Source code used in this tutorial, is available at this repository:

* <code>/_includes/pagination/05-responsive.html</code>

You should have this minimal code, before you begin.

{{< highlight twig >}}
{% capture spaceless %}
  ...
{% endcapture %}

<nav aria-label="Page navigation">

  {% if total_pages > 1 %}
  <ul class="pagination justify-content-center">

  ...

  </ul>
  {% endif %}

</nav>
{{< / highlight >}}

#### Pages: Blog List

Set this blog list page:

* <code>/pages/index.html</code>
 : [github.com/.../pages/index.html][demo-blog-list-page].

{{< highlight twig >}}
  {% include pagination/05-responsive.html %}
{{< / highlight >}}

#### SASS: Main

* <code>_sass/themes/oriclone/main.scss</code>
  : [gitlab.com/.../main.scss][demo-jekyll-sass-main].

{{< highlight scss >}}
@import
  // variables
    "bootstrap/functions",
    "variables",
    "bootstrap/variables",
    "bootstrap/mixins/breakpoints",

  // taken from bootstrap
  // ...

  // custom: general
  // ...
    "pagination"
;
{{< / highlight >}}

#### SASS: Custom Pagination

I'm using Bootstrap 4 grid breakpoints.

* <code>_sass/themes/oriclone/_pagination.scss</code>
  : [gitlab.com/.../_pagination.scss][demo-jekyll-sass-pagination].

{{< highlight css >}}
@include media-breakpoint-up(md) {
  ...
}
{{< / highlight >}}

-- -- --

### 2: Navigation: HTML Class

#### The Final Result.

Consider have a look at the image below.

![Hugo Pagination: Responsive 1][image-ss-02-responsive-01]

The HTML that we want to achieve is similar as below.

{{< highlight html >}}
  <ul class="pagination justify-content-center">
      <li class="page-item blog_previous">...</li>
      <li class="page-item first">...</li>
      <li class="pages-indicator first disabled">...</li>
      <li class="page-item pagination--offset-2">...</li>
      <li class="page-item pagination--offset-1">...</li>
      <li class="page-item pagination--offset-0 active ">...</li>
      <li class="page-item pagination--offset-1">...</li>
      <li class="page-item pagination--offset-2">...</li>
      <li class="pages-indicator last disabled">...</li>
      <li class="page-item last">...</li>
      <li class="page-item blog_next">...</li>
  </ul>
{{< / highlight >}}

#### Middle Pagination

All you need to care is, only these lines.

{{< highlight html >}}
      <li class="page-item pagination--offset-2">...</li>
      <li class="page-item pagination--offset-1">...</li>
      <li class="page-item pagination--offset-0 active ">...</li>
      <li class="page-item pagination--offset-1">...</li>
      <li class="page-item pagination--offset-2">...</li>
{{< / highlight >}}

Our short term goal is,
to put the <code>pagination--offset</code> class.

#### Partial: Pagination Code Skeleton

As usual, the skeleton, to show the complexity.

* <code>/_includes/pagination/05-responsive.html</code>
 : [gitlab.com/.../partials/pagination/05-responsive.html][repo-pagination-responsive].

{{< highlight twig >}}
<nav aria-label="Page navigation">
{% capture spaceless %}
  <!-- Variable Initialization. -->
{% endcapture %}

  {% if total_pages > 1 %}
  <ul class="pagination justify-content-center">

    <!-- Previous Page. -->
    <!-- First Page. -->
    <!-- Early (More Pages) Indicator. -->

    {% for page in (1..total_pages) %}
      <!-- Flag Calculation -->
      {% assign page_current_flag = false %}

      {% if total_pages > link_max %}
        <!-- Complex page numbers. -->
      
          <!-- Lower limit pages. -->
          <!-- Upper limit pages. -->
          <!-- Middle pages. -->

      {% else %}
        <!-- Simple page numbers. -->
      {% endif %}

      {% if page_current_flag == true %}
      <!-- Calculate Offset Class. -->
      {% endif %}

      {% if page_current_flag == true %}
        <!-- Show Pager. -->
      {% endif %}
    {% endfor %}

    <!-- Late (More Pages) Indicator. -->
    <!-- Last Page. -->
    <!-- Next Page. -->

  </ul>
  {% endif %}

</nav>
{{< / highlight >}}

Notice this new section

* <code>Calculate Offset Class</code>.

#### Partial: Pagination Code: Long Version

Before you begin, consider copy-and-paste from our last tutorial.

* <code>/_includes/pagination/04-indicator.html</code>
 : [gitlab.com/.../partials/pagination/04-indicator.html][repo-pagination-indicator].

#### Calculate Offset Value

It is just a matter of difference, between current page and pager.

{{< highlight twig >}}
      {% if page_current_flag == true %}
      <!-- Calculate Offset Class. -->
        {% assign diff_offset = page | minus: page_current | abs %}
      {% endif %}
{{< / highlight >}}

We are lucky that, Jekyll Liquid have this <code>abs</code>.

#### Using Offset Class

All we need is just adding the offset class.

{{< highlight twig >}}
      <li class="page-item pagination--offset-{{ diff_offset }}">
        ...
      </li>
{{< / highlight >}}

#### Combined Code

The real code, is also not very simple.

{{< highlight twig >}}
    {% capture spaceless %}
      {% if page_current_flag == true %}
      <!-- Calculate Offset Class. -->
        {% assign diff_offset = page | minus: page_current | abs %}
      {% endif %}
    {% endcapture %}

      <!-- Show Pager. -->
      {% if page_current_flag == true %}
      <li class="page-item {% if page == page_current %} active{% endif %} pagination--offset-{{ diff_offset }}">
        {% if page == page_current %} 
          <span class="page-link">
            {{ page }}
          </span>
        {% elsif page == 1 %}
          <a class="page-link"
             href="{{ site.baseurl }}{{ paginate_root }}"
           >1</a>
        {% else %}
          <a class="page-link"
             href="{{ site.paginate_path | relative_url | replace: ':num', page }}"
           >{{ page }}
          </a>
        {% endif %}
      </li>
      {% endif %}
{{< / highlight >}}

That is all.
Now that the HTML part is ready, we should go on,
by setting up the responsive breakpoints using SCSS.

-- -- --

### 4: Responsive: Breakpoints

Responsive is easy if you understand the logic.

> It is all about breakpoints.

#### Preview: Each Breakpoint

Consider again, have a look at the animation above, frame by frame.
We have at least five breakpoint as six figures below:

![Hugo Pagination: Responsive 1][image-ss-02-responsive-01]

![Hugo Pagination: Responsive 2][image-ss-02-responsive-02]

![Hugo Pagination: Responsive 3][image-ss-02-responsive-03]

![Hugo Pagination: Responsive 4][image-ss-02-responsive-04]

![Hugo Pagination: Responsive 5][image-ss-02-responsive-05]

![Hugo Pagination: Responsive 6][image-ss-02-responsive-06]

#### SASS: Bootstrap Grid Breakpoint Variables.

Bootstrap 4 grid breakpoints are defined as below.

* <code>_sass/vendors/bootstrap/_variables.scss</code>

{{< highlight scss >}}
// Grid breakpoints
//
// Define the minimum dimensions at which your layout will change,
// adapting to different screen sizes, for use in media queries.

$grid-breakpoints: (
  xs: 0,
  sm: 576px,
  md: 768px,
  lg: 992px,
  xl: 1200px
) !default;
{{< / highlight >}}

#### SASS: Bootstrap Breakpoint Skeleton

With breakpoint above, we can setup css skeleton, with empty css rules.

{{< highlight scss >}}
ul.pagination {
  @include media-breakpoint-up(xs) {}  
  @include media-breakpoint-up(sm) {}
  @include media-breakpoint-up(md) {}
  @include media-breakpoint-up(lg) {}
  @include media-breakpoint-up(xl) {}
}
{{< / highlight >}}

#### SASS: Using Bootstrap Breakpoint: Simple

We can fill any rules, as below:

{{< highlight scss >}}
@include media-breakpoint-up(md) {

  .blog_previous {
    span.page-link:after,
    a.page-link:after {
      content: " previous"
    }
  }

  .blog_next {
    span.page-link:before,
    a.page-link:before {
      content: "next "
    }
  }

}
{{< / highlight >}}

#### SASS: Using Bootstrap Breakpoint: Pagination Offset

We can fill any rules, inside <code>pagination</code> class as below:

{{< highlight scss >}}
ul.pagination {

  li.pagination--offset-1,
  li.pagination--offset-2,
  li.pagination--offset-3,
  li.pagination--offset-4,
  li.pagination--offset-5,
  li.pagination--offset-6,
  li.pagination--offset-7 {
    display: none;
  }

  @include media-breakpoint-up(xs) {
  }
  
  @include media-breakpoint-up(sm) {
    li.pagination--offset-1,
    li.pagination--offset-2 {
      display: inline-block;
    }
  }

  @include media-breakpoint-up(md) {
    li.pagination--offset-3,
    li.pagination--offset-4 {
      display: inline-block;
    }
  }

  @include media-breakpoint-up(lg) {
    li.pagination--offset-5,
    li.pagination--offset-6,
    li.pagination--offset-7 {
      display: inline-block;
    }
  }

  @include media-breakpoint-up(xl) {
  }

}
{{< / highlight >}}

#### SASS: Responsive Indicator

You can also add CSS rules for indicator.

{{< highlight scss >}}
ul.pagination {

  li.first,
  li.last,
  li.pages-indicator {
    display: none;
  }

  @include media-breakpoint-up(xs) {
  }
  
  @include media-breakpoint-up(sm) {
    li.pages-indicator {
      display: inline-block;
    }
  }

  @include media-breakpoint-up(md) {
    li.first,
    li.last {
      display: inline-block;
    }
  }

  @include media-breakpoint-up(lg) {
  }

  @include media-breakpoint-up(xl) {
  }

}
{{< / highlight >}}

#### SASS: Enhanced Bootstrap Breakpoint

If you desire smoother transition effect,
you can create your own breakpoint, beyond bootstrap.

{{< highlight scss >}}
$grid-breakpoints-custom: (
  xs:  0,
  xs2: 320px,
  xs3: 400px,
  xs4: 480px,
  sm:  576px,
  sm2: 600px,
  md:  768px,
  lg:  992px,
  xl:  1200px
) !default;
{{< / highlight >}}

And later, add css rule as code below:

{{< highlight scss >}}
ul.pagination {
  @include media-breakpoint-up(xs2, $grid-breakpoints-custom) {
    li.pages-indicator {
      display: inline-block;
    }
  }
}
{{< / highlight >}}

#### SASS: Complete Code

Now you can have the complete code as below:

* <code>_sass/themes/oriclone/_pagination.scss</code>
  : [gitlab.com/.../_pagination.scss][demo-jekyll-sass-pagination].

{{< highlight scss >}}
@include media-breakpoint-up(md) {

  .blog_previous {
    span.page-link:after,
    a.page-link:after {
      content: " previous"
    }
  }

  .blog_next {
    span.page-link:before,
    a.page-link:before {
      content: "next "
    }
  }

}

$grid-breakpoints-custom: (
  xs:  0,
  xs2: 320px,
  xs3: 400px,
  xs4: 480px,
  sm:  576px,
  sm2: 600px,
  md:  768px,
  lg:  992px,
  xl:  1200px
) !default;

ul.pagination {

  li.first,
  li.last,
  li.pages-indicator {
    display: none;
  }

  li.pagination--offset-1,
  li.pagination--offset-2,
  li.pagination--offset-3,
  li.pagination--offset-4,
  li.pagination--offset-5,
  li.pagination--offset-6,
  li.pagination--offset-7 {
    display: none;
  }

  @include media-breakpoint-up(xs, $grid-breakpoints-custom) {
  }
  
  @include media-breakpoint-up(xs2, $grid-breakpoints-custom) {
    li.pages-indicator {
      display: inline-block;
    }
  }

  @include media-breakpoint-up(xs3, $grid-breakpoints-custom) {
    li.pagination--offset-1 {
      display: inline-block;
    }
  }
  
  @include media-breakpoint-up(xs4, $grid-breakpoints-custom) {
    li.pagination--offset-2 {
      display: inline-block;
    }
  }

  @include media-breakpoint-up(sm, $grid-breakpoints-custom) {
    li.first,
    li.last,
    li.pagination--offset-3 {
      display: inline-block;
    }
  }
  
  @include media-breakpoint-up(sm2, $grid-breakpoints-custom) {
    li.pagination--offset-4 {
      display: inline-block;
    }
  }

  @include media-breakpoint-up(md, $grid-breakpoints-custom) {
    li.pagination--offset-5,
    li.pagination--offset-6 {
      display: inline-block;
    }
  }

  @include media-breakpoint-up(lg, $grid-breakpoints-custom) {
    li.pagination--offset-7 {
      display: inline-block;
    }
  }

  @include media-breakpoint-up(xl, $grid-breakpoints-custom) {
  }

}
{{< / highlight >}}

-- -- --

### 5: Summary

You can have a look at our complete code here:

* <code>/_includes/pagination/05-responsive.html</code>
 : [gitlab.com/.../partials/pagination/05-responsive.html][repo-pagination-responsive].

We will have to complete the code later, in the next article.
This has been a long article.
We still need a few changes, for screen reader, and clean-up unneeded comments.

-- -- --

#### What is Next ?

Consider continue reading [ [Jekyll Pagination - Screenreader][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2019/01/08/jekyll-pagination-screenreader/

[repo-pagination-indicator]:    {{< demo-jekyll >}}/_includes/pagination/04-indicator.html
[repo-pagination-responsive]:   {{< demo-jekyll >}}/_includes/pagination/05-responsive.html
[jbmd-pagination-responsive]:   {{< jekyll-bulma-md >}}/tutor-06/_includes/pagination-v2/05-responsive.html
[demo-blog-list-page]:      {{< demo-jekyll >}}/pages/index.html

[image-ss-07-demo-hugo]:    {{< assets-ssg >}}/2018/12/67-demo-hugo.png

[image-ss-02-simple-indicator]:     {{< assets-ssg >}}/2018/11/52-simple-indicator-number.png

[image-ss-02-responsive-animation]: {{< assets-ssg >}}/2018/11/52-pagination-responsive.gif


[image-ss-02-responsive-01]:        {{< assets-ssg >}}/2018/11/52-responsive-01.png
[image-ss-02-responsive-02]:        {{< assets-ssg >}}/2018/11/52-responsive-02.png
[image-ss-02-responsive-03]:        {{< assets-ssg >}}/2018/11/52-responsive-03.png
[image-ss-02-responsive-04]:        {{< assets-ssg >}}/2018/11/52-responsive-04.png
[image-ss-02-responsive-05]:        {{< assets-ssg >}}/2018/11/52-responsive-05.png
[image-ss-02-responsive-06]:        {{< assets-ssg >}}/2018/11/52-responsive-06.png

[demo-jekyll-sass-main]:            {{< demo-jekyll >}}/_sass/themes/oriclone/main.scss
[demo-jekyll-sass-pagination]:      {{< demo-jekyll >}}/_sass/themes/oriclone/_pagination.scss
