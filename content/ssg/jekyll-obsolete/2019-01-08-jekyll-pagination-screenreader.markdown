---
type   : post
title  : "Jekyll Pagination - Screenreader"
date   : 2019-01-08T09:17:35+07:00
slug   : jekyll-pagination-screenreader
categories: [ssg]
tags      : [jekyll]
keywords  : [static site, custom theme, liquid, pagination, screen reader]
author : epsi
opengraph:
  image: assets/site/images/topics/jekyll.png

toc    : "toc-2019-01-jekyll-pagination"

excerpt:
  Building Jekyll Pagination Step by Step, using Bootstrap.
  Accessability for pagination using Bootstrap.
  Complete the custom pagination.
  This the summary code for our pagination.

---

### Preface

> Goal: Bringing screen reader accessability in pagination, and clean up.

#### Preview

No Preview. You can use any screenreader to test with your browser.

This is basically just where to put <code>sr-only</code> class.

#### More Recent Code

I have made a better code with Bulma
utilizing `site.pagination.permalink`
which you can examine at:

* [gitlab.com/.../_includes/pagination/06-screenreader.html][jbmd-pagination-screenreader]

-- -- --

### 1: Prepare

Artefact that we need.

#### Source

I just follow bootstrap guidance:

* https://getbootstrap.com/docs/4.0/components/pagination/

To hidden content visually,
you simply need to add <code>sr-only</code>.

{{< highlight twig >}}
<span class="sr-only">Hidden Content</span>
{{< / highlight >}}

This content can be read by screenreader.

#### Includes: Pagination: Accesability

Create an empty artefact.
Source code used in this tutorial, is available at this repository:

* <code>/_includes/pagination/06-screenreader.html</code>

#### Pages: Blog List

Set this blog list page:

* <code>/pages/index.html</code>
 : [github.com/.../pages/index.html][demo-blog-list-page].

{{< highlight twig >}}
  {% include pagination/06-screenreader.html %}
{{< / highlight >}}

-- -- --

### 2: Navigation: Previous and Next

Just a slight modification.

#### Navigation: Previous

{{< highlight html >}}
    {% if paginator.previous_page %}
      <li class="page-item blog_previous">
        <a class="page-link" 
           href="{{ site.baseurl }}{{ paginator.previous_page_path }}"
           rel="prev">
          <span aria-hidden="true">&laquo;</span>
          <span class="sr-only">Previous</span>
        </a>
      </li>
    {% else %}
      <li class="page-item blog_previous disabled">
        <span class="page-link">&laquo;</span>
      </li>
    {% endif %}
{{< / highlight >}}

#### Navigation: Next

{{< highlight html >}}
    {% if paginator.next_page %}
      <li class="page-item blog_next">
        <a class="page-link" 
           href="{{ site.baseurl }}{{ paginator.next_page_path }}" 
           rel="next">
          <span aria-hidden="true">&raquo;</span>
          <span class="sr-only">Next</span>
        </a>
      </li>
    {% else %}
      <li class="page-item blog_next disabled">
        <span class="page-link">&raquo;</span>
      </li>
    {% endif %}
{{< / highlight >}}

-- -- --

### 3: Navigation: First and Last

Just a slight modification.

#### Navigation: First

{{< highlight html >}}
    {% if total_pages > link_max %}
      <!-- First Page. -->
      {% if lower_offset > 1 %}
        <li class="page-item first">
          <a class="page-link"
             href="{{ site.baseurl }}{{ page.paginate_root }}">
            <span class="sr-only">Page </span>1</a>
        </li>
      {% endif %}
    {% endif %}
{{< / highlight >}}

#### Navigation: Last

{{< highlight html >}}
    {% if total_pages > link_max %}
      <!-- Last Page. -->
      {% if upper_offset < total_pages %}
        <li class="page-item last">
          <a class="page-link" 
             href="{{ site.paginate_path | relative_url | replace: ':num', total_pages }}"
          ><span class="sr-only">Page </span>{{ total_pages }}</a>
        </li>
      {% endif %}
    {% endif %}
{{< / highlight >}}

-- -- --

### 4: Middle Navigation: Number

{{< highlight html >}}
      {% if page_current_flag == true %}
      <li class="page-item {% if page == page_current %} active{% endif %} pagination--offset-{{ diff_offset }}">
        {% if page == page_current %} 
          <span class="page-link">
            <span class="sr-only">Page </span>{{ page }}
          </span>
        {% elsif page == 1 %}
          <a class="page-link"
             href="{{ site.baseurl }}{{ paginate_root }}"
           ><span class="sr-only">Page </span>1</a>
        {% else %}
          <a class="page-link"
             href="{{ site.paginate_path | relative_url | replace: ':num', page }}"
           ><span class="sr-only">Page </span>{{ page }}
          </a>
        {% endif %}
      </li>
      {% endif %}
{{< / highlight >}}

-- -- --

### 5: Conclusion

#### Complete Code

As a summary of this pagination tutorial,
the complete code is here below:

* <code>themes/tutor-05/layouts/partials/pagination/06-screenreader.html</code>
 : [gitlab.com/.../partials/pagination/06-screenreader.html][repo-pagination-screenreader].
 
#### Clean-up Code

As I promise, get rid of unneeded comments.

* <code>themes/tutor-05/layouts/partials/pagination/07-cleanup.html</code>
 : [gitlab.com/.../partials/pagination/07-cleanup.html][repo-pagination-cleanup].

{{< highlight twig >}}
{% capture spaceless %}

  {% comment %}
  Pagination links 
  * https://glennmccomb.com/articles/how-to-build-custom-hugo-pagination/
  {% endcomment %}

  {% if page.paginate_root == nil %}
    {% assign paginate_root = "/" %}
  {% else %}    
    {% assign paginate_root = page.paginate_root %}
  {% endif %}

  {% assign total_pages   = paginator.total_pages %}
  {% assign page_current  = paginator.page %}

  {% assign link_offset   = 2 %}  
  {% assign link_max      = link_offset | times: 2 | plus: 1 %}
  
  {% assign limit_lower   = link_offset | plus: 1 %}
  {% assign limit_upper   = total_pages  | minus: link_offset %}
  
  {% assign min_lower     = link_max %}  
  {% assign max_upper     = total_pages | minus: link_max %}
    
  {% assign lower_offset  = page_current | minus: link_offset %}
  {% assign upper_offset  = page_current | plus: link_offset %}

  {% assign lower_indicator = 2 %}
  {% assign upper_indicator = total_pages | minus: 1 %}

{% endcapture %}

<nav aria-label="Page navigation">

  {% if total_pages > 1 %}
  <ul class="pagination justify-content-center">
    <!-- Previous Page. -->
    {% if paginator.previous_page %}
      <li class="page-item blog_previous">
        <a class="page-link" 
           href="{{ site.baseurl }}{{ paginator.previous_page_path }}"
           rel="prev">
          <span aria-hidden="true">&laquo;</span>
          <span class="sr-only">Previous</span>
        </a>
      </li>
    {% else %}
      <li class="page-item blog_previous disabled">
        <span class="page-link">&laquo;</span>
      </li>
    {% endif %}

    {% if total_pages > link_max %}
      <!-- First Page. -->
      {% if lower_offset > 1 %}
        <li class="page-item first">
          <a class="page-link"
             href="{{ site.baseurl }}{{ page.paginate_root }}">
            <span class="sr-only">Page </span>1</a>
        </li>
      {% endif %}

      <!-- Early (More Pages) Indicator. -->
      {% if lower_offset > lower_indicator %}
        <li class="pages-indicator first disabled">
          <span class="page-link">...</span>
        </li>
      {% endif %}
    {% endif %}

    <!-- Page numbers. -->
    {% for page in (1..total_pages) %}
    
    {% capture spaceless %}
      {% assign page_current_flag = false %}

      {% if total_pages > link_max %}

        {% if page_current <= limit_lower %}
          {% if page <= min_lower %}
            {% assign page_current_flag = true %}
          {% endif %}

        {% elsif page_current >= limit_upper %}
          {% if page > max_upper %}
            {% assign page_current_flag = true %}
          {% endif %}

        {% else %}
          
          {% if (page >= lower_offset) and (page <= upper_offset) %}
            {% assign page_current_flag = true %}
          {% endif %}

        {% endif %}
      {% else %}

        {% assign page_current_flag = true %}
      {% endif %}

      {% if page_current_flag == true %}
        {% assign diff_offset = page | minus: page_current | abs %}
      {% endif %}
    {% endcapture %}

      {% if page_current_flag == true %}
      <li class="page-item {% if page == page_current %} active{% endif %} pagination--offset-{{ diff_offset }}">
        {% if page == page_current %} 
          <span class="page-link">
            <span class="sr-only">Page </span>{{ page }}
          </span>
        {% elsif page == 1 %}
          <a class="page-link"
             href="{{ site.baseurl }}{{ paginate_root }}"
           ><span class="sr-only">Page </span>1</a>
        {% else %}
          <a class="page-link"
             href="{{ site.paginate_path | relative_url | replace: ':num', page }}"
           ><span class="sr-only">Page </span>{{ page }}
          </a>
        {% endif %}
      </li>
      {% endif %}
    {% endfor %}

    {% if total_pages > link_max %}
      <!-- Late (More Pages) Indicator. -->
      {% if upper_offset < upper_indicator %}
        <li class="pages-indicator last disabled">
          <span class="page-link">...</span>
        </li>
      {% endif %}

      <!-- Last Page. -->
      {% if upper_offset < total_pages %}
        <li class="page-item last">
          <a class="page-link" 
             href="{{ site.paginate_path | relative_url | replace: ':num', total_pages }}"
          ><span class="sr-only">Page </span>{{ total_pages }}</a>
        </li>
      {% endif %}
    {% endif %}

    <!-- Next Page. -->
    {% if paginator.next_page %}
      <li class="page-item blog_next">
        <a class="page-link" 
           href="{{ site.baseurl }}{{ paginator.next_page_path }}" 
           rel="next">
          <span aria-hidden="true">&raquo;</span>
          <span class="sr-only">Next</span>
        </a>
      </li>
    {% else %}
      <li class="page-item blog_next disabled">
        <span class="page-link">&raquo;</span>
      </li>
    {% endif %}
  </ul>
  {% endif %}

</nav>
{{< / highlight >}}

That code above, should be our final code, in our tutorial.

-- -- --

#### Summary

Thank you for reading.
I think that's all.
Consider go back reading [ [Jekyll Pagination - Summary][local-whats-next] ].

Farewell.
We shall meet again.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2019/01/01/jekyll-pagination-summary/

[repo-pagination-screenreader]: {{< demo-jekyll >}}/_includes/pagination/06-screenreader.html
[jbmd-pagination-screenreader]: {{< jekyll-bulma-md >}}/tutor-06/_includes/pagination-v2/06-screenreader.html
[repo-pagination-cleanup]:  {{< demo-jekyll >}}/_includes/pagination/07-cleanup.html
[demo-blog-list-page]:      {{< demo-jekyll >}}/pages/index.html

[image-ss-07-demo-hugo]:    {{< assets-ssg >}}/2018/12/67-demo-hugo.png

[image-ss-02-simple-indicator]:     {{< assets-ssg >}}/2018/11/52-simple-indicator-number.png
