---
type   : post
title  : "Jekyll Plain - Repository"
date   : 2020-06-03T09:17:35+07:00
slug   : jekyll-plain-repository
categories: [ssg]
tags      : [jekyll, plain]
keywords  : [static site, custom theme, summary, overview]
author : epsi
opengraph:
  image: assets-ssg/2020/06/jekyll-plain-preview.png

toc    : "toc-2020-06-jekyll-step"

excerpt:
  Building Jekyll Site Step by step,
  in plain HTML without stylesheet burden.
---

### Preface

> Goal: Overview of Jekyll Repository Step By Step

An example of pure Jekyll site without burden of any stylesheet
for personal learning purpose.

> Build: in plain HTML without stylesheet burden.

This repository is intentionally made without any stylesheet.
We will use CSS frameworks later using different repository.

#### Jekyll Version

Since this repository is still using Jekyll 3.8, instead of Jekyll 4.0,
you might need to run `bundle install`, depend on the situation.

{{< highlight bash >}}
$ bundle install
{{< / highlight >}}

-- -- --

## Chapter Step by Step

### Tutor 01

> Generate Only Pure HTML

* Configuration: Minimal

* General Layout: Default

![Jekyll Plain: Tutor 01][jekyll-plain-preview-01]

-- -- --

### Tutor 02

> Include Liquid Partials

* Setup Directory for Minimal Jekyll

* New Layout: [Page, Post, Home] (extends Default)

* Partials: HTML Head, Header, Footer

* Basic Content

![Jekyll Plain: Tutor 02][jekyll-plain-preview-02]

-- -- --

### Tutor 03

> Loop with Liquid

* More Content: Lyrics. This is required for loop demo

* Index Content: Index, Category, Tag, Archive By Year/Month

![Jekyll Plain: Tutor 03][jekyll-plain-preview-03]

-- -- --

### Tutor 04

> Template Inheritance in Liquid

* Custom Output: YAML, JSON

* List Tree: Archives: By Year, By Year and Month

* List Tree: Tags and Categories

* Partials: Blog List, Terms, Archive

* New Layout: Index (extends Page)

* Markdown

* Shortcode

![Jekyll Plain: Tutor 04][jekyll-plain-preview-04]

-- -- --

### Tutor 05

> Pagination with Liquid

* Pagination (v1): Simple, Number, Adjacent, Indicator

* Pagination (v2): Simple, Number, Adjacent, Indicator

* Meta HTML

* Post Navigation

![Jekyll Plain: Tutor 05][jekyll-plain-preview-05]

-- -- --

### Tutor 06

> Jekyll Plugin with Ruby

* Filter: Year Text

* Filter: Term Array

* Filter: Pagination Links, Link Offset Flag

* Filter: Keywords

* Generator: Tag Names

![Jekyll Plain: Tutor 06][jekyll-plain-preview-06]

-- -- --

### Tutor 07

> Jekyll Theme

* Bundling Gem

* Using Theme

![Jekyll Plain: Tutor 07][jekyll-plain-preview-07]

-- -- --

### What's Next?

Consider continue reading [ [Jekyll - Plain - Minimal][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/06/04/jekyll-plain-minimal/

[jekyll-plain-preview]:     https://gitlab.com/epsi-rns/tutor-jekyll-plain/raw/master/jekyll-plain-preview.png
[jekyll-plain-preview-01]:  https://gitlab.com/epsi-rns/tutor-jekyll-plain/raw/master/tutor-01/jekyll-plain-preview.png
[jekyll-plain-preview-02]:  https://gitlab.com/epsi-rns/tutor-jekyll-plain/raw/master/tutor-02/jekyll-plain-preview.png
[jekyll-plain-preview-03]:  https://gitlab.com/epsi-rns/tutor-jekyll-plain/raw/master/tutor-03/jekyll-plain-preview.png
[jekyll-plain-preview-04]:  https://gitlab.com/epsi-rns/tutor-jekyll-plain/raw/master/tutor-04/jekyll-plain-preview.png
[jekyll-plain-preview-05]:  https://gitlab.com/epsi-rns/tutor-jekyll-plain/raw/master/tutor-05/jekyll-plain-preview.png
[jekyll-plain-preview-06]:  https://gitlab.com/epsi-rns/tutor-jekyll-plain/raw/master/tutor-06/jekyll-plain-preview.png
[jekyll-plain-preview-07]:  https://gitlab.com/epsi-rns/tutor-jekyll-plain/raw/master/tutor-07/jekyll-plain-preview.png

