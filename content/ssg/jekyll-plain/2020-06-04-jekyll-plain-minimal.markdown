---
type   : post
title  : "Jekyll Plain - Minimal"
date   : 2020-06-04T09:17:35+07:00
slug   : jekyll-plain-minimal
categories: [ssg]
tags      : [jekyll, plain]
keywords  : [jekyll new, jekyll serve, Gemfile, minimal, port]
author : epsi
opengraph:
  image: assets-ssg/2020/06/plain/01-jekyll-nerdtree.png

toc    : "toc-2020-06-jekyll-step"

excerpt:
  Jekyll in plain HTML without stylesheet burden.
  Show Minimal Homepage in Jekyll.
---

### Preface

> Goal: Show Minimal Homepage in Jekyll.

#### Source Code

This article use [tutor-01][tutor-html-master-01] theme.
We will create it step by step.

#### Layout Preview for Tutor 01

![Liquid: Layout Preview for Tutor 01][template-inheritance-01]

[template-inheritance-01]:  {{< assets-ssg >}}/2020/06/01-template-inheritance.png

-- -- --

### 1: Create A New Site

To create a new site, you can simply issue `jekyll new` command.

{{< highlight bash >}}
$ mkdir tutor-01

$ cd tutor-01

$ jekyll new .
Running bundle install in /home/epsi/Documents/tutor-01... 
  Bundler: Fetching gem metadata from https://rubygems.org/..........
  Bundler: Fetching gem metadata from https://rubygems.org/.
  Bundler: Resolving dependencies...
  Bundler: Using public_suffix 4.0.5
  Bundler: Using addressable 2.7.0
  Bundler: Using bundler 2.1.4
  Bundler: Using colorator 1.1.0
  Bundler: Using concurrent-ruby 1.1.6
  Bundler: Using eventmachine 1.2.7
  Bundler: Using http_parser.rb 0.6.0
  Bundler: Using em-websocket 0.5.1
  Bundler: Using ffi 1.13.1
  Bundler: Using forwardable-extended 2.6.0
  Bundler: Using i18n 0.9.5
  Bundler: Using rb-fsevent 0.10.4
  Bundler: Using rb-inotify 0.10.1
  Bundler: Using sass-listen 4.0.0
  Bundler: Using sass 3.7.4
  Bundler: Using jekyll-sass-converter 1.5.2
  Bundler: Using listen 3.2.1
  Bundler: Using jekyll-watch 2.2.1
  Bundler: Using kramdown 1.17.0
  Bundler: Using liquid 4.0.3
  Bundler: Using mercenary 0.3.6
  Bundler: Using pathutil 0.16.2
  Bundler: Using rouge 3.21.0
  Bundler: Using safe_yaml 1.0.5
  Bundler: Using jekyll 3.8.7
  Bundler: Using jekyll-feed 0.15.0
  Bundler: Using jekyll-seo-tag 2.6.1
  Bundler: Using minima 2.5.1
  Bundler: Bundle complete! 6 Gemfile dependencies, 28 gems now installed.
  Bundler: Use `bundle info [gemname]` to see where a bundled gem is installed.
New jekyll site installed in /home/epsi/Documents/tutor-01. 
{{< / highlight >}}

![CLI: jekyll new .][image-01-jekyll-new]

Al we nee3d to know is,
beside Jekyll there is other default Gem.

{{< highlight bash >}}
  Bundler: Using jekyll 3.8.7
  Bundler: Using jekyll-feed 0.15.0
  Bundler: Using jekyll-seo-tag 2.6.1
  Bundler: Using minima 2.5.1
{{< / highlight >}}

* Theme: Minima

* Plugin: Jekyll Feed

* Plugin: Jekyll SEO Tag

-- -- --

### 2: Running for the First Time

#### Bundle Exec

You can run directly from the command line.

{{< highlight bash >}}
$ bundle exec jekyll serve
Configuration file: /home/epsi/Documents/tutor-01/_config.yml
            Source: /home/epsi/Documents/tutor-01
       Destination: /home/epsi/Documents/tutor-01/_site
 Incremental build: disabled. Enable with --incremental
      Generating... 
       Jekyll Feed: Generating feed for posts
                    done in 0.674 seconds.
 Auto-regeneration: enabled for '/home/epsi/Documents/tutor-01'
    Server address: http://127.0.0.1:4000/
  Server running... press ctrl-c to stop.
{{< / highlight >}}

![CLI: jekyll new .][image-01-jekyll-serve]

Now you can see the result in Browser.

![Jekyll: Minima in Browser][image-01-jekyll-minima]

This default template is `minima`.
We wil not use `minima` for this tutorial.

#### Port

It is common to work with multiple Jekyll at one time,
for one reason or another.
You can achieve it by passing `port` parameter argument.

{{< highlight bash >}}
$ jekyll serve --port=4001
...
    Server address: http://127.0.0.1:4001/
{{< / highlight >}}

#### Incremental

Working with hundred articles make your notoriously Jekyll slugish.
This is not Jekyll's fault. In fact Jekyll can be speed-up by
using `incremental` parameter argument.
It is just some jekyll blogger forget that this options exist.

{{< highlight bash >}}
$ jekyll serve --incremental
{{< / highlight >}}

-- -- --

### 3: Gemfile

The `jekyll serve` above generated a `Gemfile` with a long documentation.

{{< highlight ruby >}}
source "https://rubygems.org"

# Hello! This is where you manage which Jekyll version is used to run.
# When you want to use a different version, change it below, save the
# file and run `bundle install`. Run Jekyll with `bundle exec`, like so:
#
#     bundle exec jekyll serve
#
# This will help ensure the proper Jekyll version is running.
# Happy Jekylling!
gem "jekyll", "~> 3.8.6"

# This is the default theme for new Jekyll sites. You may change this to anything you like.
gem "minima", "~> 2.0"

# If you want to use GitHub Pages, remove the "gem "jekyll"" above and
# uncomment the line below. To upgrade, run `bundle update github-pages`.
# gem "github-pages", group: :jekyll_plugins

# If you have any plugins, put them here!
group :jekyll_plugins do
# gem "jekyll-feed", "~> 0.6"
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
# and associated library.
install_if -> { RUBY_PLATFORM =~ %r!mingw|mswin|java! } do
  gem "tzinfo", "~> 1.2"
  gem "tzinfo-data"
end

# Performance-booster for watching directories on Windows
gem "wdm", "~> 0.1.0", :install_if => Gem.win_platform?
{{< / highlight >}}

* [gitlab.com/.../Gemfile][tutor-gemfile]

In short, is just this three lines.

{{< highlight ruby >}}
source "https://rubygems.org"

gem "jekyll", "~> 3.8.6"
gem "minima", "~> 2.0"
{{< / highlight >}}

This is where the default template `minima` is set.

-- -- --

### 3: YAML Configuration

Jekyll use a configuration file named `_config.yml`.
Of course in YAML format.

#### Original Configuration

* [gitlab.com/.../_config.yaml][tutor-configuration]

{{< highlight yaml >}}
title: Your awesome title
email: your-email@example.com
description: >- # this means to ignore newlines until "baseurl:"
  Write an awesome description for your new site here. You can edit this
  line in _config.yml. It will appear in your document head meta (for
  Google search results) and in your feed.xml site description.
baseurl: "" # the subpath of your site, e.g. /blog
url: "" # the base hostname & protocol for your site, e.g. http://example.com
twitter_username: jekyllrb
github_username:  jekyll

# Build settings
markdown: kramdown
theme: minima
plugins:
  - jekyll-feed
{{< / highlight >}}

In Jekyll, all started with `_` is considered, not a content.
We will have `_` in many places such as:

* file: `_config.yml`.

* directory: `_site`, `_layouts`, `_includes`, `_data`.

#### Simple Configuration

We will not use above configuration,
instead we are going to make original theme from scratch.

{{< highlight yaml >}}
# Site settings
title: Your mission. Good Luck!

# the subpath of your site, e.g. /blog
baseurl: "" 

# the base hostname & protocol for your site, e.g. http://example.com
url: "http://localhost:4000"
{{< / highlight >}}

#### Exclude

I use `README.md` and other files in each step.
You can omit regeneration of this addtional files by using `exclude`.

{{< highlight yaml >}}
exclude:
  - Gemfile
  - Gemfile.lock
  - README.md
  - jekyll-plain-preview.png
{{< / highlight >}}

-- -- --

### 4: Minimal Views

#### Layout

Our minimal theme required `default layout`,
with any name you want, in the `_layouts` directory.

* [gitlab.com/.../_site/index.html][tutor-layout-default]

#### Content: Markdown Index

To make our theme useful, we are going to need a content.

* [gitlab.com/.../index.md][tutor-content-index]

{{< highlight markdown >}}
---
layout : default
---

To have, to hold, to love,
cherish, honor, and protect?
  
To shield from terrors known and unknown?
To lie, to deceive?

To live a double life,
to fail to prevent her abduction,
erase her identity, 
force her into hiding,
take away all she has known.
{{< / highlight >}}

You can use either `markdown` format, `html` format.

#### Directory Tree

Consider have a look at this directory tree.
So far we have six file artefacts.

{{< highlight bash >}}
❯ tree
.
├── _config.yml
├── Gemfile
├── Gemfile.lock
├── index.md
├── _layouts
│   └── default.html
└── _site
    └── index.html

2 directories, 6 files
{{< / highlight >}}

That `_site` is generated each time,
a `jekyll build` or `jekyll serve` command issued. __.__

![Jekyll: Tutor-01: NERDTree][image-01-jekyll-nerdtree]

The `index.html` comes from `index.md`.
Converted from `markdown` format to `html` format.

-- -- --

### Liquid Layout

#### Content: Index

The `index.md`, is a markdown file using `default.html` as layout.

{{< highlight yaml >}}
---
layout : default
---

To have, ...
{{< / highlight >}}

#### Layout: Liquid Default

So how is the layout looks like?
It is just an HTML page with a few `liquid`.

You are free to name it to `default.html` or `base.html`, or else.

* [gitlab.com/.../_site/index.html][tutor-layout-default]

{{< highlight html >}}
<!DOCTYPE html>
<html>

<head>
  <title>{{ page.title | default: site.title }}</title>
</head>

<body>
  <!-- header -->
  <p><i>Your mission, should you decide to accept it.</i></p>
  <hr/>

  <!-- main -->
  <main role="main">
    <h2>{{ page.title | default: site.title }}</h2>

    {{ content }}

    <blockquote>Any question? <b>Good Luck</b>.</blockquote>
  </main>

  <!-- footer -->
  <hr/>
  <p><i>As always, should you be caught or killed,
  any knowledge of your actions will be disavowed.</i></p>
</body>

</html>
{{< / highlight >}}

This layout could be simpler,
we will refactor it later.

![Jekyll: Default Layout][image-01-jekyll-default]

The only `liquid` tag here these two below as below:

{{< highlight jinja >}}
{{ page.title | default: site.title }}
{{< / highlight >}}

and most importantly:

{{< highlight jinja >}}
{{ content }}
{{< / highlight >}}

#### Render

We can see the combination of content in layout,
in rendered file in `_site` directory.

* `_site/index.html`

{{< highlight bash >}}
$ cat _site/index.html
{{< / highlight >}}

Or this would be more fancy with curl,
then pipe from stdin to ViM in read only mode. __.__

{{< highlight bash >}}
$ curl -s localhost:4000 | vim -R -
{{< / highlight >}}

Now you can see

![Jekyll: Rendered Source][image-01-jekyll-curl-vim]

In your favorite browser, this would looks as simple as this.

![Jekyll: Minimal Theme in Browser][image-01-jekyll-home]

This is all for minimal  original theme from scratch. 

-- -- --

### What's Next?

Consider continue reading [ [Jekyll - Plain - Layout][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/06/05/jekyll-plain-layout/
[tutor-html-master-01]: {{< tutor-jekyll-plain >}}/tutor-01/

[tutor-gemfile]:        {{< tutor-jekyll-plain >}}/tutor-01/Gemfile
[tutor-configuration]:  {{< tutor-jekyll-plain >}}/tutor-01/_config.yml

[tutor-content-index]:  {{< tutor-jekyll-plain >}}/tutor-01/index.md
[tutor-layout-default]: {{< tutor-jekyll-plain >}}/tutor-01/_layouts/default.html

[image-01-jekyll-new]:      {{< assets-ssg >}}/2020/06/plain/01-jekyll-new.png
[image-01-jekyll-serve]:    {{< assets-ssg >}}/2020/06/plain/01-jekyll-serve.png
[image-01-jekyll-minima]:   {{< assets-ssg >}}/2020/06/plain/01-jekyll-minima.png
[image-01-jekyll-nerdtree]: {{< assets-ssg >}}/2020/06/plain/01-jekyll-nerdtree.png
[image-01-jekyll-home]:     {{< assets-ssg >}}/2020/06/plain/01-jekyll-home.png
[image-01-jekyll-default]:  {{< assets-ssg >}}/2020/06/plain/01-jekyll-layout-default.png
[image-01-jekyll-curl-vim]: {{< assets-ssg >}}/2020/06/plain/01-jekyll-curl-vim.png
