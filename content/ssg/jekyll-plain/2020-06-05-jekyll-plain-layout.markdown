---
type   : post
title  : "Jekyll Plain - Layout"
date   : 2020-06-05T09:17:35+07:00
slug   : jekyll-plain-layout
categories: [ssg]
tags      : [jekyll, plain]
keywords  : [template engine, liquid, layout, refactoring template]
author : epsi
opengraph:
  image: assets-ssg/2020/06/plain/02-jekyll-nerdtree.png

toc    : "toc-2020-06-jekyll-step"

excerpt:
  Jekyll in plain HTML without stylesheet burden.
  Getting started with layout using Liquid templating engine.
---

### Preface

> Goal: Getting started with layout using Liquid templating engine.

Layout enable you to create many kind of page template,
such as page kind, post kind, landing page (home kind), or archive page.

#### Source Code

This article use [tutor-02][tutor-html-master-02] theme.
We will create it step by step.

#### Official Site.

* [shopify.github.io/liquid](https://shopify.github.io/liquid/)

#### Layout Preview for Tutor 02

![Liquid: Layout Preview for Tutor 02][template-inheritance-02]

[template-inheritance-02]:  {{< assets-ssg >}}/2020/06/02-template-inheritance.png

-- -- --

### 0: Theme Making

Consider to have a look at the big picture below:

![SSG Illustration: Theme Making][theme-making]

-- -- --

### 1: Refactor Includes

#### Directory Tree: Includes

Consider create each file artefacts manually.

{{< highlight bash >}}
$ tree _layouts _includes
_layouts
├── default.html
├── home.html
├── page.html
└── post.html
_includes
├── footer.html
├── header.html
└── head.html

0 directories, 7 files
{{< / highlight >}}

We are going to focus on these file artefacts above __.__

![Jekyll: Tutor-02: NERDTree][image-02-nerdtree]

#### Layout: Liquid Default

Instead of put everything in one `html` file,
we can separate each `html` part in different `liquid` file,
and call them using `include`.

* [gitlab.com/.../_layouts/default.html][tutor-vl-default]

{{< highlight jinja >}}
<!DOCTYPE html>
<html>

<head>
  {% include head.html %}
</head>

<body>
  {% include header.html %}

  <main role="main">
    {{ content }}
  </main>

  {% include footer.html %}
</body>

</html>
{{< / highlight >}}

And three `include`s:

* `head`,

* `header`,

* `footer`.

![Jekyll: Refactoring Base Default to Head, Header and Footer][image-02-includes]

#### Partial: Liquid Head

* [gitlab.com/.../_includes/head.html][tutor-vi-head]

{{< highlight jinja >}}
  <title>{{ (page.title | default: site.title) | escape }}</title>
{{< / highlight >}}

The default `site.title` is set in `_config.yml` as

{{< highlight yaml >}}
# Site settings
title: Your mission. Good Luck!
{{< / highlight >}}

We can use default title from `site.title`,
if there is no `page.title` available in frontmatter.

#### Partial: Liquid Header

* [gitlab.com/.../_includes/header.html][tutor-vi-header]

{{< highlight html >}}
  <p><i>Your mission, should you decide to accept it.</i></p>
  <hr/>
{{< / highlight >}}

#### Partial: Liquid Footer

* [gitlab.com/.../_includes/footer.html][tutor-vi-footer]

{{< highlight html >}}
  <hr/>
  <p><i>As always, should you be caught or killed,
  any knowledge of your actions will be disavowed.</i></p>
{{< / highlight >}}

#### Finally

We are done with simple refactoring.
These simple snippets, show how `liquid` works.

-- -- --

### 2: Layout: Page and Post

We are freely to make any kind of pages.
Here we make commons blog page kind:

* Page Kind

* Post Kind

* Home Kind, for landing page

Remember, that in configuration, 
every `html` file is actually using `liquid` template engine.

#### Layout: Liquid Page

* [gitlab.com/.../_layouts/page.html][tutor-vl-page]

Template inheritance in `liquid`,
set in frontmatter.

{{< highlight jinja >}}
---
layout: default
---

  <h2>{{ page.title | default: site.title }}</h2>
  <strong>This is a page kind layout.</strong>
  <br/>

  {{ content }}

  <blockquote>Any question? <b>Good Luck</b>.</blockquote>
{{< / highlight >}}

![Jekyll: Template Inheritance: Parent and Child][image-02-layouts]

You should see how `{{ content }}` propagate,
from one child layout to parent layout.

#### Layout: Liquid Post

* [gitlab.com/.../_layouts/post.html][tutor-vl-post]

Template inheritance in `liquid`,
set in frontmatter.

{{< highlight jinja >}}
---
layout: default
---

  <h2>{{ page.title | default: site.title }}</h2>
  <pre>{{ page.date | date: "%B %d, %Y" }}</pre>
  <br/>

  <strong>This is a post kind layout.</strong>
  <br/>

  {{ content }}

{{< / highlight >}}

I add `{{ page.date }}` to differ `post` kind and `page` kind.

#### Layout: Liquid Home

* [gitlab.com/.../_layouts/home.html][tutor-vl-home]

Template inheritance in `liquid`,
set in frontmatter.

{{< highlight jinja >}}
---
layout : default
---

  <h2>{{ page.title | default: site.title }}</h2>
  <strong>This is a home kind layout, to show landing page.</strong>
  <br/>

  {{ content }}
{{< / highlight >}}

This one is pretty similar with the `page` layout.

#### Finally

We are done with simple template inheritance.
These simple snippets, also show how `liquid` works.

-- -- --

### 3: Content: Pages and Posts

After `_layouts` and `_includes` ready, we can serve our content.

#### Directory Tree: Includes

Consider create each artefacts manually.

{{< highlight bash >}}
$ tree _posts pages
_posts
└── 2015-10-03-every-day.md
pages
├── about.md
└── index.html

0 directories, 3 files
{{< / highlight >}}

#### Page Content: index

It is actually very similar with previous `index.html`.
With additional menu, in markdown format.
And now using `home` layout.

* [gitlab.com/.../index.md][tutor-vc-index]

{{< highlight html >}}
---
layout    : home
---

Menu:
[[Blog](/pages/)]
[[About](/pages/about)]

To have, ...
{{< / highlight >}}

![Jekyll: Home in Browser][image-vc-home]

#### Page Content: pages/about

This is a markdown file artefact, with frontmatter.

* [gitlab.com/.../pages/about.md][tutor-vc-pa-about]

{{< highlight markdown >}}
---
layout    : page
title     : Rescue Mission
---

This was not a rescue mission!

Let me put to you like this.
If the secretary wanted me out of there,
then things are really bad out here
{{< / highlight >}}

![Jekyll: About Page in Browser][image-vc-about]

Notice the `page` layout, in frontmatter.

#### Page Content: pages/index

It is clear that this loop is using `liquid` template engine.

* [gitlab.com/.../pages/index.html][tutor-vc-pa-archive]

{{< highlight jinja >}}
---
layout    : page
title     : Blog Posts
---

  {% assign posts = site.posts %}

  <ul>
  {% for post in posts %}
    <li>
      <a href="{{ site.baseurl }}{{ post.url }}">
        {{ post.title }}
      </a>
    </li>
  {% endfor %}
  </ul>
{{< / highlight >}}

Pay attention to the loop.

{{< highlight jinja >}}
  {% assign posts = site.posts %}
  {% for post in posts %}
    ...
  {% endfor %}
{{< / highlight >}}

![Jekyll: Archive Page in Browser][image-vc-archive]

#### Post Content: posts/every-day

And this one has some more information in frontmatter.

* [gitlab.com/.../_posts/2015-10-03-every-day.md][tutor-vc-po-everyday]

{{< highlight markdown >}}
---
layout    : post
title     : Every Day
date      : 2015-10-03 08:08:15 +0700
categories: movie
---

I've been thinking about this
over and over and over.

I mean, really, truly,
imagine it.

...
{{< / highlight >}}

![Jekyll: Everyday Post in Browser][image-vc-everyday]

#### Local Render

You might want to see the render result,
by looking at the `_site` directory.

{{< highlight bash >}}
$ tree _site
_site
├── index.html
├── movie
│   └── 2015
│       └── 10
│           └── 03
│               └── every-day.html
└── pages
    ├── about.html
    └── index.html

5 directories, 4 files
{{< / highlight >}}

![Jekyll: Tree: _site][image-02-tree-site]

What's in your browser is what rendered in `_site`.

-- -- --

### What's Next?

Consider continue reading [ [Jekyll - Plain - Custom Pages][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/06/06/jekyll-plain-custom-pages/
[tutor-html-master-02]: {{< tutor-jekyll-plain >}}/tutor-02/

[theme-making]:         {{< assets-ssg >}}/2020/theme-making.png

[tutor-vl-default]:     {{< tutor-jekyll-plain >}}/tutor-02/_layouts/default.html
[tutor-vi-head]:        {{< tutor-jekyll-plain >}}/tutor-02/_includes/head.html
[tutor-vi-header]:      {{< tutor-jekyll-plain >}}/tutor-02/_includes/header.html
[tutor-vi-footer]:      {{< tutor-jekyll-plain >}}/tutor-02/_includes/footer.html
[tutor-vl-page]:        {{< tutor-jekyll-plain >}}/tutor-02/_layouts/page.html
[tutor-vl-post]:        {{< tutor-jekyll-plain >}}/tutor-02/_layouts/post.html
[tutor-vl-home]:        {{< tutor-jekyll-plain >}}/tutor-02/_layouts/home.html
[tutor-vc-index]:       {{< tutor-jekyll-plain >}}/tutor-02/index.md
[tutor-vc-pa-about]:    {{< tutor-jekyll-plain >}}/tutor-02/pages/about.md
[tutor-vc-pa-archive]:  {{< tutor-jekyll-plain >}}/tutor-02/pages/index.html
[tutor-vc-po-everyday]: {{< tutor-jekyll-plain >}}/tutor-02/_posts/2015-10-03-every-day.md

[image-02-tree]:        {{< assets-ssg >}}/2020/06/plain/02-jekyll-tree.png
[image-02-tree-site]:   {{< assets-ssg >}}/2020/06/plain/02-jekyll-tree-site.png
[image-02-nerdtree]:    {{< assets-ssg >}}/2020/06/plain/02-jekyll-nerdtree.png
[image-02-includes]:    {{< assets-ssg >}}/2020/06/plain/02-jekyll-includes.png
[image-02-layouts]:     {{< assets-ssg >}}/2020/06/plain/02-jekyll-layouts.png

[image-vc-home]:        {{< assets-ssg >}}/2020/06/plain/02-jekyll-home.png
[image-vc-about]:       {{< assets-ssg >}}/2020/06/plain/02-jekyll-about.png
[image-vc-archive]:     {{< assets-ssg >}}/2020/06/plain/02-jekyll-archive.png
[image-vc-everyday]:    {{< assets-ssg >}}/2020/06/plain/02-jekyll-everyday.png
