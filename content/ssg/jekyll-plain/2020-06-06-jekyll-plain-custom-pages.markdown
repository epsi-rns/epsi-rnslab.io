---
type   : post
title  : "Jekyll Plain - Custom Pages"
date   : 2020-06-06T09:17:35+07:00
slug   : jekyll-plain-custom-pages
categories: [ssg]
tags      : [jekyll, plain]
keywords  : [populate content, loop, liquid, archives, tags, categories]
author : epsi
opengraph:
  image: assets-ssg/2020/06/plain/03-jekyll-nerdtree.png

toc    : "toc-2020-06-jekyll-step"

excerpt:
  Jekyll in plain HTML without stylesheet burden.
  Apply loop with Liquid to make custom archive pages.
---

### Preface

> Goal: Apply loop with Liquid to make custom archive pages.

#### Source Code

This article use [tutor-03][tutor-html-master-03] theme.
We will create it step by step.

#### Layout Preview for Tutor 03

![Liquid: Layout Preview for Tutor 03][template-inheritance-03]

[template-inheritance-03]:  {{< assets-ssg >}}/2020/06/03-template-inheritance.png

-- -- --

### 1: Populate the Content

This is required for loop demo.

> Subgoal: Populate content for use with special custom index pages

As always, populate content with good reading is,
as hard as giving meaningful variable.
Luckily I have done it for you.

#### Custom Index Page

We are going to explore custom index page such as:

1. Archive

   * <http://localhost:4000/by-year/>
   
   * <http://localhost:4000/by-month/>

2. Tags and Categories

   * <http://localhost:4000/tags/>
   
   * <http://localhost:4000/categories/>

3. Blog (Article List)

   * <http://localhost:4000/pages/>

![Illustration: Custom Layouts][illust-custom-layout]

For this to work, we need a proper blog content,
with such diversity such as posting year, and tags for each post,
so we can represent the list nicely,
with sorting, and also grouping whenever necessary.

My content choice comes to `song lyrics`.
We can tag `song lyrics` with its genre,
and naturally `song lyrics` also has date released.

I the also add unique post,
to examine what does it looks,
to have entirely different folder category.

[//]: <> ( -- -- -- 
I the also add `movie quotes` in later chapter,
to examine what does it looks,
to have entirely different folder category.
-- -- -- )

#### Configuration

I also add `permalink` configuration.

* [gitlab.com/.../_config.yaml][tutor-configuration]

{{< highlight yaml >}}
# Produces a cleaner folder structure when using categories
# permalink: /:year/:month/:title.html
permalink: /:categories/:year/:month/:day/:title:output_ext
{{< / highlight >}}

#### Pattern

Our typical content is usually short quote from a lyric,
with four frontmatter items as below:

* [gitlab.com/.../_posts/lyrics/2018-09-13-julien-baker-sprained-ankle.md][tutor-po-sprained-ankle]

{{< highlight yaml >}}
---
layout      : post
title       : Julien Baker - Sprained Ankle
date        : 2018-09-13 07:35:05 +0700
categories  : lyric
tags        : [rock, 2010s]
author      : Julien Baker

excerpt: Wish I could write songs about anything other than death
---

A sprinter learning to wait  
A marathon runner, my ankles are sprained  
A marathon runner, my ankles are sprained  
{{< / highlight >}}

All example content should follow the pattern above.
All these content wear `post` layout.

#### Directory Tree: Posts

A few content is enough.
I add ten lyrics to make sure the content have enough diversity.

{{< highlight bash >}}
$ tree _posts/
_posts/
├── 2016-01-01-winter.md
└── lyrics
    ├── 2017-03-15-nicole-atkins-a-litle-crazy.md
    ├── 2017-03-25-nicole-atkins-a-night-of-serious-drinking.md
    ├── 2018-01-15-emily-king-distance.md
    ├── 2018-02-15-emma-ruth-rundle-shadows-of-my-name.md
    ├── 2018-09-07-julien-baker-something.md
    ├── 2018-09-13-julien-baker-sprained-ankle.md
    ├── 2019-03-15-hemming-vitamins.md
    ├── 2019-05-15-brooke-annibale-by-your-side.md
    ├── 2019-05-25-brooke-annibale-yours-and-mine.md
    ├── 2019-07-15-mothers-no-crying-in-baseball.md
    └── 2020-03-15-company-of-thieves-oscar-wilde.md

1 directory, 12 files
{{< / highlight >}}

![Jekyll: All Content Sources][image-03-tree-posts]

Also edit the frontmatter as necessary,
for the rest of the content.

#### Rendered Posts

How does it looks in your file systems?

{{< highlight bash >}}
$ tree _site/lyric
_site/lyric
├── 2017
│   └── 03
│       ├── 15
│       │   └── nicole-atkins-a-litle-crazy.html
│       └── 25
│           └── nicole-atkins-a-night-of-serious-drinking.html
├── 2018
│   ├── 01
│   │   └── 15
│   │       └── emily-king-distance.html
│   ├── 02
│   │   └── 15
│   │       └── emma-ruth-rundle-shadows-of-my-name.html
│   └── 09
│       ├── 07
│       │   └── julien-baker-something.html
│       └── 13
│           └── julien-baker-sprained-ankle.html
├── 2019
│   ├── 03
│   │   └── 15
│   │       └── hemming-vitamins.html
│   ├── 05
│   │   ├── 15
│   │   │   └── brooke-annibale-by-your-side.html
│   │   └── 25
│   │       └── brooke-annibale-yours-and-mine.html
│   └── 07
│       └── 15
│           └── mothers-no-crying-in-baseball.html
└── 2020
    └── 03
        └── 15
            └── company-of-thieves-oscar-wilde.html

23 directories, 11 files
{{< / highlight >}}

![Jekyll: All Rendered Lyrics][image-03-tree-lyrics]

-- -- --

### 2: Includes: Refactoring

#### Layout: Header

We need to modify the header a little,
so we have simple navigation in plain HTML.

* [gitlab.com/.../_includes/site/header.html][tutor-vi-header]

{{< highlight html >}}
  <p>
    [ <a href="/">Home</a> ]
    [ <a href="/pages/">Blog</a> ]
    [ <a href="/pages/about">About</a> ]
    [ <a href="/tags/">Tags</a> ]
    [ <a href="/categories/">Categories</a> ]
    [ <a href="/by-year/">By Year</a> ]
  </p>

  <hr/>
{{< / highlight >}}

![Jekyll: Home Navigation][image-03-navigation]

#### Includes: Site Directory

Also prepare for later refactoring,
we need to put those three files in their own folder.

{{< highlight bash >}}
$ tree _includes
_includes
└── site
    ├── footer.html
    ├── header.html
    └── head.html

1 directory, 3 files
{{< / highlight >}}

![Jekyll: Directory NERDTree with Navigation in Header][image-03-nerdtree]

#### Layout: Liquid Default

Repecstively, we need to modify `includes` toi reflect the right path.

* [gitlab.com/.../_layouts/default.html][tutor-vl-default]

{{< highlight jinja >}}
<!DOCTYPE html>
<html>

<head>
  {% include site/head.html %}
</head>

<body>
  {% include site/header.html %}

  <main role="main">
    {{ content }}
  </main>

  {% include site/footer.html %}
</body>

</html>
{{< / highlight >}}

-- -- --

### 3: Content: Index

There is no significance changes for `index.html`,
except you can change the name to `blog.html`,
and add the right permalink.

* [gitlab.com/.../pages/index.html][tutor-pa-blog]

{{< highlight jinja >}}
---
layout    : page
title     : Blog Posts
permalink : /pages/
---

 {% assign posts = site.posts %}

  <ul>
  {% for post in posts %}
    <li>
      <a href="{{ site.baseurl }}{{ post.url }}">
        {{ post.title }}
      </a>
    </li>
  {% endfor %}
  </ul>
{{< / highlight >}}

Now see how it looks like this page with new content.

![Jekyll: Cutom Pages: Blog][image-03-blog]

> Filename Caveat

There is caveat of using `blog.html`.
Jekyll `pagination-v1` would only work with `index.html`.
This issue has been fixed with `pagination-v2`.
So for safety issue, we use `index.html` instead,
but leave the `/pages/` permalink,
just instead we decide to use `pagination-v2`,
and rename the file to `blog.html` in later chapter.

-- -- --

### 4: Content: Category and Tag

On most SSG, dealing with tags and categories,
need understanding of its data structures.

#### Content: Frontmatter

Example of Tags and categories in content is as below:

{{< highlight yaml >}}
---
layout      : post
title       : Julien Baker - Sprained Ankle
date        : 2018-09-13 07:35:05 +0700
categories  : lyric
tags        : [rock, 2010s]
---
{{< / highlight >}}

#### Data Structure

Both `category` and `tag` have very similar structure.
While tag use `site.tags`, category use `site.categories`.
The issue is extracting the tags collection.

In Jekyll, `site.tags` is actually a hash (key+value),
that contains array of posts related to that tag.

You can print with related with `soul` tag

{{< highlight jinja >}}
{{ site.tags['soul'] }}
{{< / highlight >}}

And check how many post related with that `soul` tag

{{< highlight jinja >}}
{{ site.tags['soul'] | size }}
{{< / highlight >}}

In liquid `site.tags` can be presented as an array of two:

1. `tag[0]` contain tag name.
2. `tag[1]` contain array of posts.

You can check yourself with this snippet.

{{< highlight jinja >}}
  {% assign tag = site.tags | first %}
  {{ tag[0] }}
  {{ tag[1] }}
{{< / highlight >}}

#### Building Array of Terms

Now all we have to do is to create an array contain all unique posts.
Since `liquid` has no built in way to create array,
we have to use split:

{{< highlight jinja >}}
{% assign term_array = "" | split: "|" %}
{{< / highlight >}}

To avoid useless whitespace,
we put the code between `capture` tag.

{{< highlight jinja >}}
{% capture spaceless %}
  ...
{% endcapture %}
{{< / highlight >}}

And finally we have the code

{{< highlight jinja >}}
{% capture spaceless %}
  {% assign term_array = "" | split: "|" %}
  {% for tag in terms %}
    {% assign term_first = tag | first %}
    {% assign term_array = term_array | push: term_first %}
  {% endfor %}
  
  {% assign term_array = term_array | sort %}
{% endcapture %}
{{< / highlight >}}

I know this code above looks odd.
With ruby filter for liquid we can make it shorter as below:

{{< highlight ruby >}}
    def term_array(terms)
      terms.keys
    end
{{< / highlight >}}

But we won't be using any filter in this chapter.

#### Tags

Now we can apply above snippets to custom tags pages.

* [gitlab.com/.../pages/tags.html][tutor-pa-tags]

{{< highlight jinja >}}
---
layout    : page
title     : All Tags
permalink : /tags/
---

{% assign terms = site.tags %}

{% capture spaceless %}
  {% assign term_array = "" | split: "|" %}
  {% for tag in terms %}
    {% assign term_first = tag | first %}
    {% assign term_array = term_array | push: term_first %}
  {% endfor %}
  
  {% assign term_array = term_array | sort %}
{% endcapture %}

  <p>Tag List:
  <ul>
  {% for item in (0..terms.size) %}{% unless forloop.last %}
    {% assign this_word = term_array[item] | strip_newlines %}
    <li id="{{ this_word | slugify }}" class ="anchor-target">
      {{ this_word }}
    </li>
  {% endunless %}{% endfor %}
  </ul>
  </p>
{{< / highlight >}}

![Jekyll: Cutom Pages: Tags][image-03-tags]

The `forloop` reference can be read here:

* [Liquid: The forloop object][forloop]

[forloop]: https://shopify.dev/docs/themes/liquid/reference/objects/for-loops

#### Categories

* [gitlab.com/.../pages/categories.html][tutor-pa-categories]

{{< highlight jinja >}}
---
layout    : page
title     : All Categories
permalink : /categories/
---

{% assign terms = site.categories %}

{% capture spaceless %}
  {% assign term_array = "" | split: "|" %}
  {% for tag in terms %}
    {% assign term_first = tag | first %}
    {% assign term_array = term_array | push: term_first %}
  {% endfor %}
  
  {% assign term_array = term_array | sort %}
{% endcapture %}

  <p>Tag List:
  <ul>
  {% for item in (0..terms.size) %}{% unless forloop.last %}
    {% assign this_word = term_array[item] | strip_newlines %}
    <li id="{{ this_word | slugify }}" class ="anchor-target">
      {{ this_word }}
    </li>
  {% endunless %}{% endfor %}
  </ul>
  </p>
{{< / highlight >}}

![Jekyll: Cutom Pages: Tags][image-03-categories]

-- -- --

### 5: Content: Archive by Year

#### Grouping

Instead of showing list of pages in plain fashioned,
we can show list of pages, grouped by `year`, or even by `year+month`.
To do this we utilize `group_by_exp` filter.

{{< highlight jinja >}}
{% assign postsByYear = site.posts
          | group_by_exp: "post", "post.date | date: '%Y'"  %}
{{< / highlight >}}

With code above we can loop to show each group,
and then having inner loop contain each posts

{{< highlight jinja >}}
{% assign postsByYear = site.posts
          | group_by_exp: "post", "post.date | date: '%Y'"  %}

<div id="archive">
{% for year in postsByYear %}

  <section>
    <p class ="anchor-target" 
       id="{{ year.name }}"
      >{{ year.name }}</p>

    <ul>
      {% for post in year.items %}
      <li><a href="{{ site.baseurl }}{{ post.url }}">
          {{ post.title }}
      </a></li>
      {% endfor %}
    </ul>
  </section>

{% endfor %}
</div>
{{< / highlight >}}

#### Year Text

We can creatively change the text to show nicer name such as
__This year's posts (2020)__ instead just __2020__.

{{< highlight jinja >}}
  {% capture spaceless %}
    {% assign current_year = 'now' | date: '%Y' %}
    {% assign year_text = nil %}

    {% if year.name == current_year %}
      {% assign year_text = year.name
                | prepend: "This year's posts (" | append: ')' %}
    {% else %}
      {% assign year_text = year.name %}
    {% endif %}
  {% endcapture %}
{{< / highlight >}}

With plugin, this could be as short as:

{{< highlight ruby >}}
    def text_year(post_year)
      (post_year == Time.now.strftime("%Y")) ?
        "This year's posts (#{post_year})" : post_year
    end
{{< / highlight >}}

But we won't be using plugin in this chapter.

#### Finally

The complete code is as below:

* [gitlab.com/.../pages/by-year.html][tutor-pa-by-year]

{{< highlight jinja >}}
---
layout    : page
title     : Archive by Year
permalink : /by-year/
---

{% assign postsByYear = site.posts
          | group_by_exp: "post", "post.date | date: '%Y'"  %}

<div id="archive">
{% for year in postsByYear %}

  {% capture spaceless %}
    {% assign current_year = 'now' | date: '%Y' %}
    {% assign year_text = nil %}

    {% if year.name == current_year %}
      {% assign year_text = year.name
                | prepend: "This year's posts (" | append: ')' %}
    {% else %}
      {% assign year_text = year.name %}
    {% endif %}
  {% endcapture %}

  <section>
    <p class ="anchor-target" 
       id="{{ year.name }}"
      >{{ year_text }}</p>

    <ul>
      {% for post in year.items %}
      <li><a href="{{ site.baseurl }}{{ post.url }}">
          {{ post.title }}
      </a></li>
      {% endfor %}
    </ul>
  </section>

{% endfor %}
</div>
{{< / highlight >}}

![Jekyll: Cutom Pages: Archives][image-03-archives]

You can compare with lyrics directory tree above.

-- -- --

### What's Next?

Consider continue reading [ [Jekyll - Plain - Custom Output][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/06/07/jekyll-plain-custom-output/
[tutor-html-master-03]: {{< tutor-jekyll-plain >}}/tutor-03/

[tutor-configuration]:  {{< tutor-jekyll-plain >}}/tutor-03/_config.yml
[tutor-vl-default]:     {{< tutor-jekyll-plain >}}/tutor-03/_layouts/default.html
[tutor-vi-header]:      {{< tutor-jekyll-plain >}}/tutor-03/_includes/site/header.html
[tutor-pa-blog]:        {{< tutor-jekyll-plain >}}/tutor-03/pages/index.html
[tutor-pa-tags]:        {{< tutor-jekyll-plain >}}/tutor-03/pages/tags.html
[tutor-pa-categories]:  {{< tutor-jekyll-plain >}}/tutor-03/pages/categories.html
[tutor-pa-by-year]:     {{< tutor-jekyll-plain >}}/tutor-03/pages/by-year.html
[tutor-po-sprained-ankle]:  {{< tutor-jekyll-plain >}}/tutor-03/_posts/lyrics/2018-09-13-julien-baker-sprained-ankle.md

[image-03-blog]:        {{< assets-ssg >}}/2020/06/plain/03-jekyll-blog.png
[image-03-tags]:        {{< assets-ssg >}}/2020/06/plain/03-jekyll-tags.png
[image-03-categories]:  {{< assets-ssg >}}/2020/06/plain/03-jekyll-categories.png
[image-03-archives]:    {{< assets-ssg >}}/2020/06/plain/03-jekyll-archives.png
[image-03-tree-posts]:  {{< assets-ssg >}}/2020/06/plain/03-jekyll-tree-posts.png
[image-03-tree-lyrics]: {{< assets-ssg >}}/2020/06/plain/03-jekyll-tree-lyrics.png
[image-03-nerdtree]:    {{< assets-ssg >}}/2020/06/plain/03-jekyll-nerdtree.png
[image-03-navigation]:  {{< assets-ssg >}}/2020/06/plain/03-jekyll-home-navigation.png

[illust-custom-layout]: {{< assets-ssg >}}/2020/06/custom-layout.png
