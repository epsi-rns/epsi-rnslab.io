---
type   : post
title  : "Jekyll Plain - Custom Output"
date   : 2020-06-07T09:17:35+07:00
slug   : jekyll-plain-custom-output
categories: [ssg]
tags      : [jekyll, plain]
keywords  : [liquid, json, yaml, custom output]
author : epsi
opengraph:
  image: assets-ssg/2020/06/plain/03-browser-json.png

toc    : "toc-2020-06-jekyll-step"

excerpt:
  Jekyll in plain HTML without stylesheet burden.
  More about blog archive example, from just html, 
  to custom content output such as json, text and yaml.
---

### Preface

> Goal: Custom content output such as JSON.

#### Source Code

This article use [tutor-03][tutor-html-master-03] theme.
We will create it step by step.

-- -- --

### 1: Archive Data: JSON

Jekyll can read data, and can also produce data.

#### Frontmatter: Layout

{{< highlight yaml >}}
---
layout: null
---
{{< / highlight >}}

Or if you wish to exclude this from sitemap

{{< highlight yaml >}}
---
layout: null
sitemap:
  exclude: 'yes'
---
{{< / highlight >}}

#### Page Content: JSON Example

The complete `JSON` example is below:

* [gitlab.com/.../pages/archive.json][tutor-pa-json]

{{< highlight jinja >}}
---
layout: null
---
{
  {% for post in site.posts %}

    "{{ post.date | date: "%y%m%d%M" }}": {
      "title": "{{ post.title | json}}",
      "url": "{{ post.url | xml_escape }}"
    }{% unless forloop.last %},{% endunless %}
  {% endfor %}
}
{{< / highlight >}}

I give an identitiy number for each post using dates and minutes.
It is rarely that a person blog in the same exact dates and minutes.
So I guess, giving an ID like this is valid enough.

You can have any `JSON` that you like such as this one below:

{{< highlight jinja >}}
---
layout: null
---
{
  {% for post in site.posts %}

    "{{ post.url | slugify }}": {
      "title": "{{ post.title | json}}",
      "url": "{{ post.url | xml_escape }}"
    }
    {% unless forloop.last %},{% endunless %}
  {% endfor %}
}
{{< / highlight >}}

#### Render: Browser

The blog list, now looks like this below:

* <http://localhost:4000/pages/archive.json>

![Jekyll: Custom Output: Archive in JSON][image-03-browser-json]

#### Data: archives.json

The raw source of the `JSON` output should looks like this:

{{< highlight json >}}
{
    "20031535": {
      "title": "Company of Thieves - Oscar Wilde",
      "url": "/lyric/2020/03/15/company-of-thieves-oscar-wilde.html"
    },
    "19071535": {
      "title": "Mothers -  No Crying in Baseball",
      "url": "/lyric/2019/07/15/mothers-no-crying-in-baseball.html"
    },
    "19052535": {
      "title": "Brooke Annibale - Yours and Mine",
      "url": "/lyric/2019/05/25/brooke-annibale-yours-and-mine.html"
    },
    "19051535": {
      "title": "Brooke Annibale - By Your Side",
      "url": "/lyric/2019/05/15/brooke-annibale-by-your-side.html"
    },
    ...
}
{{< / highlight >}}

You can examine using curl:

{{< highlight bash >}}
$ curl http://localhost:4000/pages/archive.json | vim -R -
{{< / highlight >}}

![Jekyll: Data archive.json][image-03-vim-json]

We are going to use this data to be a feed for other SSG blog.

-- -- --

### 2: Archive Data: YAML

Just like `json`, Jekyll can produce any text output such as `yaml`.

#### Page Content: YAML Example

The `YAML` example is below:

* [gitlab.com/.../pages/archive.yml][tutor-pa-yaml]

{{< highlight jinja >}}
---
layout: null
sitemap:
  exclude: 'yes'
---
# Helper for related links

{% for post in site.posts %}{% unless post.published == false %}{% if post.date %}
- id: {{ post.date | date: "%y%m%d%M" }}
  title: "{{ post.title }}"
  url: {{ post.url }}
{% endif %}{% endunless %}{% endfor %}
{{< / highlight >}}

#### Render: Browser

The blog list, can be accessed from thislink below:

* <http://localhost:4000/pages/archive.yml>

#### Data: archives.yml

The raw source of the `YAML` output should looks like this:

{{< highlight yaml >}}
# Helper for related links

- id: 20031535
  title: "Company of Thieves - Oscar Wilde"
  url: /lyric/2020/03/15/company-of-thieves-oscar-wilde.html

- id: 19071535
  title: "Mothers -  No Crying in Baseball"
  url: /lyric/2019/07/15/mothers-no-crying-in-baseball.html

- id: 19052535
  title: "Brooke Annibale - Yours and Mine"
  url: /lyric/2019/05/25/brooke-annibale-yours-and-mine.html

- id: 19051535
  title: "Brooke Annibale - By Your Side"
  url: /lyric/2019/05/15/brooke-annibale-by-your-side.html

...
{{< / highlight >}}

You can examine using curl:

{{< highlight bash >}}
$ curl http://localhost:4000/pages/archive.yml | vim -R -
{{< / highlight >}}

![Jekyll: Data archive.yaml][image-03-vim-yaml]

#### Using Data

Just copy the result manually,
from `_site/pages/archives.yml`
to folder `_data/archives.yml`.

Or automated with script, but execute it manually.

{{< highlight bash >}}
#!/usr/bin/env bash

cp ../_site/pages/archive.yml ../_data/archive.yml
{{< / highlight >}}

We are going to use this data in later chapter.

-- -- --

### What's Next?

Consider continue reading [ [Jekyll - Plain - Markdown][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/06/08/jekyll-plain-markdown/
[tutor-html-master-03]: {{< tutor-jekyll-plain >}}/tutor-03/

[tutor-pa-blog]:        {{< tutor-jekyll-plain >}}/tutor-03/pages/index.html
[tutor-pa-json]:        {{< tutor-jekyll-plain >}}/tutor-03/pages/archive.json
[tutor-pa-yaml]:        {{< tutor-jekyll-plain >}}/tutor-03/pages/archive.yml

[image-03-browser-json]:{{< assets-ssg >}}/2020/06/plain/03-browser-json.png
[image-03-vim-json]:    {{< assets-ssg >}}/2020/06/plain/03-vim-json.png
[image-03-vim-yaml]:    {{< assets-ssg >}}/2020/06/plain/03-vim-yaml.png
