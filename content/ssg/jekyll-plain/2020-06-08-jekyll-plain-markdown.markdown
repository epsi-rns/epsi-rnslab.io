---
type   : post
title  : "Jekyll Plain - Markdown"
date   : 2020-06-08T09:17:35+07:00
slug   : jekyll-plain-markdown
categories: [ssg]
tags      : [jekyll, plain]
keywords  : [markdown, line break, title, link, image, shortcode]
author : epsi
opengraph:
  image: assets-ssg/2020/06/plain/04-browser-newline.png

toc    : "toc-2020-06-jekyll-step"

excerpt:
  Jekyll in plain HTML without stylesheet burden.
  Common use of markdown in Jekyll content.
---

### Preface

> Goal: Common use of markdown in Jekyll content.

#### Source Code

This article use [tutor-04][tutor-html-master-04] theme.
We will create it step by step.

#### Layout Preview for Tutor 04

![Liquid: Layout Preview for Tutor 04][template-inheritance-04]

[template-inheritance-04]:  {{< assets-ssg >}}/2020/06/04-template-inheritance.png

-- -- --

### 0: Markdown Content

> Focus on document structure

![Illustration: Markdown Content][illustration-markdown]

-- -- --

### 1: Separate of Concern

There is not much to say about markdown.
Markdown is easy, however there might be a tricky issue.

#### Why Markdown?

> More on Writing Content

While markup focus on document formatting,
markdown focus on document structure.

#### Reading

* [daringfireball.net markdown syntax][link-daringfireball]

#### Common Structure

The common structure for content in Jekyll are:

1. Frontmatter: YAML only.

2. Content: Markdown or HTML

#### Real Life Structure

I think that, it is easier just to live with this structure,
split the content, and the resource (link and image).

1. Frontmatter: YAML

2. Content: Markdown

3. Resource: Markdown

{{< highlight markdown >}}
---
layout : page
---

# Header 1

My content

[My Link][my-image]

-- -- --

## Header 1.1

My sub content

![My Image][mylink]

-- -- --

## Header 1.2

My other sub content

[//]: <> ( -- -- -- links below -- -- -- )

[my-image]:    http://some.site/an-image.jpg
[my-link]:     http://some.site/an-article.html
{{< / highlight >}}

#### Split Resource

Notice, that the markdown divided into two part by using.

{{< highlight markdown >}}
[//]: <> ()
{{< / highlight >}}

It is a good practice to split resource and content.
But do not take that to far.
Simple things do not need to be separated.

-- -- --

### 2: Line Break

`Jekyll` markdown require double space to make a new line break.
So you do not need to worry about different markdown standard.

#### Page Content: Post

As usual, we require example content.

* [gitlab.com/.../lyrics/2019-03-15-hemming-vitamins.md][tutor-po-vitamins]

{{< highlight markdown >}}
# Heading 1

You swallow me whole without even thinking now  
Your hands are as cold as whatever you're drinking down

## Heading 2

> Been trying to fill all the holes you've been digging for yourself  
  But I can't replace everything that's gone missing from your shell

### Example Content

* Do you think I'll make you feel better?  
* Do you think I'll make you feel better?
{{< / highlight >}}

Markdown is simply text format.

![Jekyll Markdown: Simple Text in ViM][image-vim-plain]

Consider have a look at the whitespace in Vim.

![Jekyll Markdown: Newline in ViM][image-vim-newline]

And notice the double space at the end of the sentence.

#### Render: With Issue

This will result as:

![Jekyll Markdown: Newline in Browser][image-browser-newline]

-- -- --

### 3: Resources: Link and Image

We can utilize pure markdown to provide link,
or to provide image in `jekyll`.

#### Page Content: Resources in Markdown

Again, content for example purpose:

* [gitlab.com/.../lyrics/2019-07-15-mothers-no-crying-in-baseball.md][tutor-po-mothers]

{{< highlight markdown >}}
[Terminal Dofiles][one-link].

There's no crying in baseball.  
Try to understand.

![Business Card][one-image]

-- -- --

### Example Dotfiles

ViM RC: [bandithijo][link-dotfiles]

-- -- --

### What's next ?

Our next song would be [Company of Thieves - Oscar Wilde][local-whats-next]
{{< / highlight >}}

And the resource defined below, the above code, in the same document.

{{< highlight markdown >}}
[//]: <> ( -- -- -- links below -- -- -- )

{% assign asset_path = '/assets/images' %}
{% assign dotfiles = 'https://gitlab.com/epsi-rns/dotfiles/tree/master' %}

[local-whats-next]: /lyric/2020/03/15/company-of-thieves-oscar-wilde.html
[link-dotfiles]:    {{ dotfiles }}/terminal/vimrc/vimrc.bandithijo

[one-image]:    {{ asset_path }}/logo-gear.png
[one-link]:     {{ dotfiles }}/terminal/
{{< / highlight >}}

![Jekyll Markdown: Image and Link Resources in Markdown][image-browser-mothers]

#### Prepare Image Directory

It is, a good practice to have a good structured directory.
Just put all your resources in `assets` directory.
And how you structure you image, javascript, vector and such,
is actually up to you.

{{< highlight bash >}}
$ tree assets/images/
assets/images/
├── adverts
│   └── oto-spies-01.png
└── logo-gear.png

1 directory, 2 files
{{< / highlight >}}

-- -- --

### 4: Shortcode

> Just include statement.

We can also utilize shortcode liek code in Jekyll,
that can be equipped with parameter argument,
to provide `raw HTML` in the middle of content.

#### Shortcode

We need to add a shortcode in `_layout`:

* [gitlab.com/.../_includes/shortcodes/advert.html][shortcode-advert]

{{< highlight jinja >}}
<img alt="advertisement" 
     src="{{ "/assets/images/adverts/"
             | append: include.img
             | prepend: site.baseurl }}">
{{< / highlight >}}

We can use this `advert` shortcode later,
in markdown as `liquid` code as shown below:

{{< highlight jinja >}}
{% include shortcodes/advert.html img="oto-spies-01.png" %}
{{< / highlight >}}

This shortcode is just an `include` statement.

#### Page Content: Markdown with Shortcut

Again, content for example purpose

* [gitlab.com/.../lyrics/2018-02-15-emma-ruth-rundle-shadows-of-my-name.md][tutor-po-emma-ruth]

{{< highlight jinja >}}
---
layout      : post
title       : Emma Ruth Rundle - Shadows of My Name
date        : 2018-02-15 07:35:05 +0700
---

I lay back in salt  
Please forgive my name  

{% include shortcodes/advert.html img="oto-spies-01.png" %}

I won’t speak at all  
Just to sing again  
{{< / highlight >}}

![Jekyll Content: Shortcode in ViM][image-vim-shortcode]

With result as below:

![Jekyll Content: Advertisement in Markdown using Shortcode][image-browser-advert]

You can use markdown directly to show images,
or in special case you might consider shortcode.
Now it is your choice, which one is easier for you.

-- -- --

### What's Next?

Consider continue reading [ [Jekyll - Plain - Managing Code - Part One][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/06/09/jekyll-plain-managing-code-01/
[link-daringfireball]:  https://daringfireball.net/projects/markdown/syntax
[tutor-html-master-04]: {{< tutor-jekyll-plain >}}/tutor-04/
[illustration-markdown]:{{< assets-ssg >}}/2020/markdown-focus.png

[shortcode-advert]:     {{< tutor-jekyll-plain >}}/tutor-04/_includes/shortcodes/advert.html
[tutor-po-vitamins]:    {{< tutor-jekyll-plain >}}/tutor-04/_posts/lyrics/2019-03-15-hemming-vitamins.md
[tutor-po-mothers]:     {{< tutor-jekyll-plain >}}/tutor-04/_posts/lyrics/2019-07-15-mothers-no-crying-in-baseball.md
[tutor-po-emma-ruth]:   {{< tutor-jekyll-plain >}}/tutor-04/_posts/lyrics/2018-02-15-emma-ruth-rundle-shadows-of-my-name.md

[image-03-browser-json]:{{< assets-ssg >}}/2020/06/plain/03-browser-json.png
[image-03-vim-json]:    {{< assets-ssg >}}/2020/06/plain/03-vim-json.png
[image-03-vim-yaml]:    {{< assets-ssg >}}/2020/06/plain/03-vim-yaml.png
[image-vim-newline]:    {{< assets-ssg >}}/2020/06/plain/04-vim-newline.png
[image-vim-plain]:      {{< assets-ssg >}}/2020/06/plain/04-vim-plain.png
[image-vim-shortcode]:  {{< assets-ssg >}}/2020/06/plain/04-vim-shortcode.png
[image-browser-newline]:{{< assets-ssg >}}/2020/06/plain/04-browser-newline.png
[image-browser-mothers]:{{< assets-ssg >}}/2020/06/plain/04-browser-resources.png
[image-browser-advert]: {{< assets-ssg >}}/2020/06/plain/04-browser-shortcode.png
