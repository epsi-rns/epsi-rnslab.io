---
type   : post
title  : "Jekyll - Managing Code - Part One"
date   : 2020-06-10T09:17:35+07:00
slug   : jekyll-plain-managing-code-01
categories: [ssg]
tags      : [jekyll, plain]
keywords  : [liquid, loop, managing code, include, layout, pages]
author : epsi
opengraph:
  image: assets-ssg/2020/06/plain/04-jekyll-layouts.png

toc    : "toc-2020-06-jekyll-step"

excerpt:
  Jekyll in plain HTML without stylesheet burden.
  Get organized with your theme.
---

### Preface

> Goal: Get organized with your theme.

#### Source Code

This article use [tutor-04][tutor-html-master-04] theme.
We will create it step by step.

-- -- --

### 1: Liquid Code Placement

> The Issue: Where to Put Liquid Code?

So far we have three places to put liquid code:

* `pages` as content, or

* `_layouts`, or

* `_includes`.

#### Layout, Include?

> Be flexible!

Should I place code in `_layouts` or `_includes`?
The rule is simple, if the code is reusable,
the code is better placed in `_includes`.

You can use your own rule to manage your coding placement.
Personally I'm using `_layouts`,
to manage content placement using CSS layouts,
such as `main` and `aside`, but there is no rule for this.
You can put your code, depend on your situation.

#### What Code?

We are going to deal with our last custom pages.

1. Archive

   * <http://localhost:4000/by-year/>
   
   * <http://localhost:4000/by-month/>

2. Tags and Categories

   * <http://localhost:4000/tags/>
   
   * <http://localhost:4000/categories/>

3. Blog (Article List)

   * <http://localhost:4000/pages/>

#### Layout: Header

Since our site become more complex,
and it need to be fitted in small screen.
we need to prepare with a better navigation.

* [gitlab.com/.../_includes/site/header.html][tutor-vi-header]

{{< highlight html >}}
  <p>
    Menu:
    <ul>
      <li>
      [ <a href="/">Home</a> ]
      [ <a href="/pages/">Blog</a> ]
      [ <a href="/pages/about">About</a> ]
      </li>
      <li>
      [ <a href="/tags/">Archive by Tags</a> ]
      [ <a href="/categories/">Archive by Categories</a> ]
      </li>
      <li>
      [ <a href="/by-month/">Archive by Month</a> ]
      [ <a href="/by-year/">Archive by Year</a> ]
      </li>
    </ul>
  </p>

  <hr/>
{{< / highlight >}}

![Jekyll: Home Navigation][image-04-navigation]

#### Rewrite Overview

We are going to use these file artefacts.

1. Blog:
   * Content:  `pages/index.html`
   * Layout:   `_layouts/index.html`
   * Includes: `_includes/index/blog-list.html`

2. Tags and Categories:
   * Content:  `pages/tags.html`, `pages/categories.html`
   * Layout:   `_layouts/index.html`
   * Includes: `_includes/index/terms*.html`

3. Archive (by-year, by-month):
   * Content:  `pages/by-year.html`, `pages/by-month.html`
   * Layout:   `_layouts/index.html`
   * Includes: `_includes/index/by-year.html`, `_includes/index/by-month.html`

Notice, that all custom pages use the same `index` layout.

-- -- --

#### 2: Layout Enhancement

#### Directory Tree: Includes

The `_layouts` directory contain files as below:

{{< highlight bash >}}
$ tree _layouts
_layouts
├── default.html
├── home.html
├── index.html
├── page.html
└── post.html

0 directories, 5 files
{{< / highlight >}}

We are going to focus on these file artefacts above.

#### Layout: Liquid Default

Consider experiment with `html5` tag `main` and `aside`.

* [gitlab.com/.../_layouts/default.html][tutor-vl-default]

{{< highlight jinja >}}
<!DOCTYPE html>
<html>

<head>
  {% include site/head.html %}
</head>

<body>
  {% include site/header.html %}

  <main role="main">
    {{ content }}
  </main>

  <aside>
    <strong>{{ layout.aside_message }}</strong>
  </aside>

  {% include site/footer.html %}
</body>

</html>
{{< / highlight >}}

![Jekyll: Tutor-04: NERDTree][image-04-nerdtree]

Notice that we now have `layout.aside_message` variable.
This should be set in any child layout, in frontmatter.

#### Frontmatter: aside_message

> How does it looks ?

Layout: Liquid **Page**

* [gitlab.com/.../_layouts/page.html][tutor-vl-page]

{{< highlight jinja >}}
---
layout: default
aside_message : This is a page kind layout.
---

  <h2>{{ page.title | default: site.title }}</h2>

  {{ content }}

  <blockquote>Any question? <b>Good Luck</b>.</blockquote>
{{< / highlight >}}

Layout: Liquid **Post**

* [gitlab.com/.../_layouts/post.html][tutor-vl-post]

{{< highlight jinja >}}
---
layout: default
aside_message : This is a post kind layout.
---

  <h2>{{ page.title | default: site.title }}</h2>
  <pre>{{ page.date | date: "%B %d, %Y" }}</pre>

  {{ content }}
{{< / highlight >}}

Layout: Liquid **Home**

* [gitlab.com/.../_layouts/home.html][tutor-vl-home]

{{< highlight jinja >}}
---
layout : default
aside_message : This is a home kind layout, to show landing page.
---

  <h2>{{ page.title | default: site.title }}</h2>

  {{ content }}
{{< / highlight >}}

#### Layout: Liquid Index

Add finally `index` layout, as a child of `page`.

* [gitlab.com/.../_layouts/index.html][tutor-vl-index]

{{< highlight jinja >}}
---
layout: page
aside_message : This is an index kind layout, to show list of archive.
---

{{ content }}
{{< / highlight >}}

You can use `page` layout directly, I'm using special `index` page,
just to differ ordinary page and custom page that contain liquid loop.

![Jekyll: Template Inheritance: Parent and Child][image-04-layouts]

-- -- --

### 3: Custom Pages: Index

Consider move liquid code,
from content in `pages/` folder to partial in `_includes/index/`.

#### Partial Liquid: Blog List

* [gitlab.com/.../_includes/index/blog-list.html][tutor-vi-blog-list]

{{< highlight jinja >}}
  <ul>
  {% for post in posts %}
    <li>
      <a href="{{ site.baseurl }}{{ post.url }}">
        {{ post.title }}
      </a><br/>
      Tag: 
      {% for tag in post.tags %}
        [ <a href="{{ site.baseurl }}/tags/#{{ tag | slugify }}"
            >{{ tag }}</a> ]
      {% endfor %}
    </li>
  {% endfor %}
  </ul>
{{< / highlight >}}

#### Separate Variable Initialization

> But why would I want that?

Notice that we haven't assign anything to `posts` variables.
This variables should be set in `pages` or `_layouts`.

This means we can use the same view output,
but using different variable initialization,
such three choices below:

{{< highlight jinja >}}
{% assign posts = site.posts %}
{% assign posts = paginator.posts %}
{% assign posts = page.posts %}
{{< / highlight >}}

Or one variable initialization,
to adapt for different view scenario,
such three choices below:

{{< highlight jinja >}}
{% assign posts = page.posts %}
{% include index/blog-list.html %}

{% comment %}
  Alternative view
  {% include index/by-month.html %}
  {% include index/by-year.html %}
  {% include index/blog-list.html %}
{% endcomment %}
{{< / highlight >}}

#### Content: Blog

* [gitlab.com/.../pages/index.html][tutor-pa-blog]

{{< highlight jinja >}}
---
layout    : index
title     : Blog Posts
permalink : /pages/
---

{% assign posts = site.posts %}
{% include index/blog-list.html %}
{{< / highlight >}}

Now see how this custom page looks like.

![Jekyll: Custom Pages: Blog][image-04-blog]

-- -- --

### What's Next?

Consider continue reading [ [Jekyll - Plain - Managing Code - Part Two][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/06/10/jekyll-plain-managing-code-02/
[tutor-html-master-04]: {{< tutor-jekyll-plain >}}/tutor-04/

[tutor-vl-default]:     {{< tutor-jekyll-plain >}}/tutor-04/_layouts/default.html
[tutor-vl-page]:        {{< tutor-jekyll-plain >}}/tutor-04/_layouts/page.html
[tutor-vl-post]:        {{< tutor-jekyll-plain >}}/tutor-04/_layouts/post.html
[tutor-vl-home]:        {{< tutor-jekyll-plain >}}/tutor-04/_layouts/home.html
[tutor-vl-index]:       {{< tutor-jekyll-plain >}}/tutor-04/_layouts/index.html

[tutor-vi-header]:      {{< tutor-jekyll-plain >}}/tutor-04/_includes/site/header.html
[tutor-vi-blog-list]:   {{< tutor-jekyll-plain >}}/tutor-04/_includes/index/blog-list.html

[tutor-pa-blog]:        {{< tutor-jekyll-plain >}}/tutor-04/pages/index.html

[image-04-navigation]:  {{< assets-ssg >}}/2020/06/plain/04-jekyll-home-navigation.png
[image-04-nerdtree]:    {{< assets-ssg >}}/2020/06/plain/04-jekyll-nerdtree.png
[image-04-layouts]:     {{< assets-ssg >}}/2020/06/plain/04-jekyll-layouts.png
[image-04-blog]:        {{< assets-ssg >}}/2020/06/plain/04-jekyll-blog.png
