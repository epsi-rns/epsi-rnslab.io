---
type   : post
title  : "Jekyll - Managing Code - Part Three"
date   : 2020-06-12T09:17:35+07:00
slug   : jekyll-plain-managing-code-03
categories: [ssg]
tags      : [jekyll, plain]
keywords  : [liquid, loop, managing code, include, layout, pages]
author : epsi
opengraph:
  image: assets-ssg/2020/06/plain/05-flow-blog-list.png

toc    : "toc-2020-06-jekyll-step"

excerpt:
  Jekyll in plain HTML without stylesheet burden.
  Clean up pages from code, prepare theme for use in Gemfile.
---

### Preface

> Goal: Clean up pages from code, prepare theme for use in Gemfile.

#### Source Code

This article use [tutor-05][tutor-html-master-05] theme.
We will create it step by step.

#### Layout Preview for Tutor 05

![Liquid: Layout Preview for Tutor 05][template-inheritance-05]

[template-inheritance-05]:  {{< assets-ssg >}}/2020/06/05-template-inheritance.png

-- -- --

### 1: Liquid Code Placement

#### The Issue

> Where to Put Liquid Code?

So far we have these places to put liquid code:

* `pages` as content, or

* `_layouts`, or `_includes`.

The issue will rise,
when you bundle your theme as Gem for use with other folks.
From now on, consider make `content` free from any liquid code.

#### Rewrite Overview

We are going to use these file artefacts.

1. Blog:
   * Content:  `pages/index.html`
   * Layout:   `_layouts/blog.html`
   * Includes: `_includes/index/blog-list.html`

2. Tags:
   * Content:  `pages/tags.html`
   * Layout:   `_layouts/list-tag.html`
   * Includes: `_includes/index/terms*.html`

2. Categories:
   * Content:  `pages/categories.html`
   * Layout:   `_layouts/list-category.html`
   * Includes: `_includes/index/terms*.html`

4. Archive by Year:
   * Content:  `pages/by-year.html`
   * Layout:   `_layouts/by-year.html`
   * Includes: `_includes/index/by-year.html`

5. Archive by Month:
   * Content:  `pages/by-month.html`
   * Layout:   `_layouts/by-month.html`
   * Includes: `_includes/index/by-month.html`

Notice, that all custom pages use different layouts.

-- -- --

### 2: Layout Enhancement

#### Directory Tree: Includes

The `_layouts` directory contain files as below:

{{< highlight bash >}}
_layouts
├── blog.html
├── by-month.html
├── by-year.html
├── default.html
├── home.html
├── list-category.html
├── list-tag.html
├── page.html
└── post.html

0 directories, 9 files
{{< / highlight >}}

We are going to focus on these file artefacts above __.__

Notice that we have no-more `index.html`.
This `index.html` is by five new layout.

-- -- --

### 3: Custom Pages: Blog

#### Layout: Blog

Again, move code: from `pages/index.html` to `_layouts/blog.html`.

* [gitlab.com/.../_layouts/blog.html][tutor-vl-blog]

{{< highlight jinja >}}
---
layout: page
---

{% assign posts = site.posts %}
{% include index/blog-list.html %}
{{< / highlight >}}

#### Content: Blog

We do not need any content, after frontmatter.

* [gitlab.com/.../pages/index.html][tutor-pa-blog]

{{< highlight jinja >}}
---
layout    : blog
title     : Blog Posts
---
{{< / highlight >}}

![Jekyll: Code Flow: Archive in Blog List][image-05-flow-blog]

With the result in browser,
exactly the same with previous theme.

-- -- --

### 4: Custom Pages: Category and Tag

#### Layout: Tag List

Move code: from `pages/tags.html` to `_layouts/list-tag.html`.

* [gitlab.com/.../_layouts/list-tag.html][tutor-vl-tags]

{{< highlight jinja >}}
---
layout: page
---

{% assign terms = site.tags %}
{% include index/terms-array.html %}
{% include index/terms-badge.html %}
{% include index/terms-tree.html %}
{{< / highlight >}}

#### Content: Tags

We do not need any content, after frontmatter.

* [gitlab.com/.../pages/tags.html][tutor-pa-tags]

{{< highlight jinja >}}
---
layout    : list-tag
title     : All Tags
permalink : /tags/
---
{{< / highlight >}}

![Jekyll: Code Flow: Archive in Tag List][image-05-flow-tags]

With the result in browser,
exactly the same with previous theme.

#### Layout: Category List

Move code: from `pages/categories.html` to `_layouts/list-category.html`.

* [gitlab.com/.../_layouts/list-category.html][tutor-vl-categories]

{{< highlight jinja >}}
---
layout: page
---

{% assign terms = site.categories %}
{% include index/terms-array.html %}
{% include index/terms-badge.html %}
{% include index/terms-tree.html %}
{{< / highlight >}}

#### Content: Categories

We do not need any content, after frontmatter.

* [gitlab.com/.../pages/categories.html][tutor-pa-categories]

{{< highlight jinja >}}
---
layout    : list-category
title     : All Categories
permalink : /categories/
---
{{< / highlight >}}

![Jekyll: Code Flow: Archive in Category List][image-05-flow-cats]

With the result in browser,
exactly the same with previous theme.

-- -- --

### 5: Custom Pages: Archive by Year, then by Month

#### Layout: Archive by Year

Move code: from `pages/by-year.html` to `_layouts/by-year.html`.

* [gitlab.com/.../_layouts/by-year.html][tutor-vl-by-year]

{{< highlight jinja >}}
---
layout: page
---

{% assign posts = site.posts %}
{% include index/by-year.html %}
{{< / highlight >}}

#### Content: Archive by Year

We do not need any content, after frontmatter.

* [gitlab.com/.../pages/by-year.html][tutor-pa-by-year]

{{< highlight jinja >}}
---
layout    : by-year
title     : Archive by Year
permalink : /by-year/
---
{{< / highlight >}}

![Jekyll: Code Flow: Archive by Year][image-05-flow-year]

With the result in browser,
exactly the same with previous theme.

#### Layout: Archive by Month

Move code: from `pages/by-month.html` to `_layouts/by-month.html`.

* [gitlab.com/.../_layouts/by-month.html][tutor-vl-by-month]

{{< highlight jinja >}}
---
layout: page
---

{% assign posts = site.posts %}
{% include index/by-month.html %}
{{< / highlight >}}

#### Content: Archive by Month

We do not need any content, after frontmatter.

* [gitlab.com/.../pages/by-month.html][tutor-pa-by-month]

{{< highlight jinja >}}
---
layout    : by-month
title     : Archive by Month
permalink : /by-month/
---
{{< / highlight >}}

![Jekyll: Code Flow: Archive by Month][image-05-flow-month]

With the result in browser,
exactly the same with previous theme.

-- -- --

### What's Next?

Consider continue reading [ [Jekyll Plain - Meta SEO][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/06/13/jekyll-plain-meta-seo/
[tutor-html-master-05]: {{< tutor-jekyll-plain >}}/tutor-05/

[tutor-vl-blog]:        {{< tutor-jekyll-plain >}}/tutor-05/_layouts/blog.html
[tutor-vl-tags]:        {{< tutor-jekyll-plain >}}/tutor-05/_layouts/list-tag.html
[tutor-vl-categories]:  {{< tutor-jekyll-plain >}}/tutor-05/_layouts/list-category.html
[tutor-vl-by-year]:     {{< tutor-jekyll-plain >}}/tutor-05/_layouts/by-year.html
[tutor-vl-by-month]:    {{< tutor-jekyll-plain >}}/tutor-05/_layouts/by-month.html

[tutor-pa-blog]:        {{< tutor-jekyll-plain >}}/tutor-05/pages/index.html
[tutor-pa-tags]:        {{< tutor-jekyll-plain >}}/tutor-05/pages/tags.html
[tutor-pa-categories]:  {{< tutor-jekyll-plain >}}/tutor-05/pages/categories.html
[tutor-pa-by-year]:     {{< tutor-jekyll-plain >}}/tutor-05/pages/by-year.html
[tutor-pa-by-month]:    {{< tutor-jekyll-plain >}}/tutor-05/pages/by-month.html

[image-05-flow-blog]:   {{< assets-ssg >}}/2020/06/plain/05-flow-blog-list.png
[image-05-flow-month]:  {{< assets-ssg >}}/2020/06/plain/05-flow-by-month.png
[image-05-flow-year]:   {{< assets-ssg >}}/2020/06/plain/05-flow-by-year.png
[image-05-flow-cats]:   {{< assets-ssg >}}/2020/06/plain/05-flow-categories.png
[image-05-flow-tags]:   {{< assets-ssg >}}/2020/06/plain/05-flow-tags.png
