---
type   : post
title  : "Jekyll Plain - Pagination - Intro"
date   : 2020-06-14T09:17:35+07:00
slug   : jekyll-plain-pagination-intro
categories: [ssg]
tags      : [jekyll, plain]
keywords  : [liquid, pagination, intro]
author : epsi
opengraph:
  image: assets-ssg/2020/06/plain/05-pagination-tree.png

toc    : "toc-2020-06-jekyll-step"

excerpt:
  Jekyll in plain HTML without stylesheet burden.
  An introduction to pagination for dummies.
---

### Preface

> Goal: An introduction to pagination.

Pagination is a whole big topic, that takes a few articles on its own.
It is rare that common front end web based component require algorithm.
This pagination takes thinking time, instead of just rely on design skill.

However, do not ever let this complex code intimidate you.

#### Source Code

This article use [tutor-05][tutor-html-master-05] theme.
We will create it step by step.

-- -- --

### 1: Article Provided

For each step, I have made different partial,
and explain the detail in separate articles.
This article series start from simple pagination layout.

#### Step By Step

We are going to have four steps to achieve this pagination.

* Layout 01: Simple

* Layout 02: Number

* Layout 03: Adjacent

* Layout 04: Indicator

This is the spoiler, of what we are going to do in this article series.

#### Additional

I also provide other additional article as well:

* Introduction

* Navigation Style (using layout 02 as example)

-- -- --

### 2: Prepare: Plugin

This preparation is required.

#### Issue with Jekyll

Jekyll has been around for a long time.
Jekyll on github has official plugin called `pagination-v1`.
This `pagination-v1` still works well.

There is also a better pagination called `pagination-v2`.
The only problem is, this `pagination-v2` is not supported by github.

We will cover these two, `pagination-v1` and `pagination-v2`.
`pagination-v1` and `pagination-v2`, require different configuration.
And also require different setting on frontmatter.

#### Build Directory

Depend on your settings, 
with either jekyll pagination,
you can achieve these urls,
as below example:

* Page 1: http://localhost:4000/pages/

And consecutively

* Page 2: http://localhost:4000/pages/page-2/

* Page 3: http://localhost:4000/pages/page-3/

* Page 4: http://localhost:4000/pages/page-4/

![Jekyll: Tree Build URL][image-05-pg-tree]

#### Pagination-v1: Configuration

* [gitlab.com/.../_config.yaml][tutor-configuration]

{{< highlight yaml >}}
# all plugins
plugins:
  - jekyll-paginate

# jekyll-paginate-v1
paginate: 2
paginate_path: "pages/blog-:num"
{{< / highlight >}}

#### Pagination-v2: Configuration

* [gitlab.com/.../_config.yaml][tutor-configuration]

{{< highlight yaml >}}
# all plugins
plugins:
  - jekyll-paginate-v2

# jekyll-paginate-v2
# Pagination Settings
pagination:
  enabled      : true
  per_page     : 2
  permalink    : '/blog-:num/'
  title        : ':title | :num of :max pages'
  limit        : 0
  sort_field   : 'date'
  sort_reverse : true
{{< / highlight >}}

#### Pagination-v1: Content

There is no additional setting for `pagination-v1` in frontmatter,
but there is caveat.
`pagination-v1` require the content name to be exactly `index.html`.

#### Pagination-v2: Content

There is additional setting for `pagination-v2` in frontmatter.

{{< highlight yaml >}}
pagination: 
  enabled: true
{{< / highlight >}}

#### Custom Property: Content

I made an additional custom setting in frontmatter.
Applied for both `pagination-v1`, and `pagination-v2`.

{{< highlight yaml >}}
paginate_root   : /pages
{{< / highlight >}}

#### How Many Pagination

Since we have very few article, we set the pagination to `2`.
For real life blog, you can use `7` or `10` or else.
It is all up to you.

#### Content: Example Article

Just make sure you have enough article,
to test all pagination case.

{{< highlight bash >}}
❯ tree _posts
_posts
├── 2016-01-01-winter.md
├── lyrics
│   ├── 2017-03-15-nicole-atkins-a-litle-crazy.md
│   ├── 2017-03-25-nicole-atkins-a-night-of-serious-drinking.md
│   ├── 2018-01-15-emily-king-distance.md
│   ├── 2018-02-15-emma-ruth-rundle-shadows-of-my-name.md
│   ├── 2018-09-07-julien-baker-something.md
│   ├── 2018-09-13-julien-baker-sprained-ankle.md
│   ├── 2019-03-15-hemming-vitamins.md
│   ├── 2019-05-15-brooke-annibale-by-your-side.md
│   ├── 2019-05-25-brooke-annibale-yours-and-mine.md
│   ├── 2019-07-15-mothers-no-crying-in-baseball.md
│   └── 2020-03-15-company-of-thieves-oscar-wilde.md
└── quotes
    ├── 2015-01-01-dead-poets-society.md
    ├── 2015-01-01-fight-club.md
    ├── 2015-01-01-jerry-maguire.md
    ├── 2015-01-01-scott-pilgrim.md
    └── 2015-10-03-every-day.md

2 directories, 17 files
{{< / highlight >}}

Here we have 17 articles.

![Jekyll: Tree Posts Content][image-05-pg-content]

-- -- --

### 3: Layout

Consider make a slight change in `layout/blog.html` partial.

#### Partial: Pagination 

Since pagination code can be complex, it is better to put,
the pagination as partial, somewhere else.

The complete code is as simply as below layout.

* [gitlab.com/.../_layouts/blog.html][tutor-vl-blog]

{{< highlight jinja >}}
---
layout: page
---

{% include pagination-v1/04-indicator.html %}

{% assign posts = paginator.posts %}
{% include index/blog-list.html %}
{{< / highlight >}}

Notice the `paginator` object.

#### All Pagination Options

Since we are going to have four steps,
we may switch the layout often,
so we need a way to __copy-and-paste__ easily.
Here below a commented code that contain each pagination step.
You should only select one of them.

{{< highlight jinja >}}
{% comment %}

v1: not developed anymore for years
  {% include pagination-v1/01-simple.html %}
  {% include pagination-v1/02-number.html %}
  {% include pagination-v1/03-adjacent.html %}
  {% include pagination-v1/04-indicator.html %}

v2: not supported by github pages (Without travis)
  {% include pagination-v2/01-simple.html %}
  {% include pagination-v2/02-number.html %}
  {% include pagination-v2/03-adjacent.html %}
  {% include pagination-v2/04-indicator.html %}

The choice is yours.

{% endcomment %}
{{< / highlight >}}

From now on we will use `pagination` extensively.

-- -- --

### 4: Skeleton

This part is the most basic part,
and would be used in each layout for each step.

#### Layout: Layout Pagination Code

The skeleton is similar as below:

{{< highlight html >}}
<!-- helper function -->
<!-- variable initialization -->
<nav role="navigation">

    <!-- left  navigation code -->
    <!-- middle numbering code -->
    <!-- right navigation code -->

</nav>
{{< / highlight >}}

Navigation code contain buttons such as:

* Left Pagination: First

* Left Pagination: Previous

* Right Pagination: Next

* Right Pagination: Last

While numbering code is in the middle

* Middle Pagination: Numbering

#### Minimal Logic

The pagination would only be shown,
if there is more than one pagination page.
No need to show any pagination navigation,
if what we have is only one page.
Hence our minimal pagination logic would be:

{{< highlight jinja >}}
{% assign total_pages = paginator.total_pages %}

{% if total_pages > 1 %}
<nav ...>
    ...
    <!-- pagination code -->

</nav>
{% endif %}
{{< / highlight >}}

Of course this is not enough.
Our minimal pagination should show something.

#### Loop

The loop for each pagination, is simply as below:

{{< highlight jinja >}}
    {% for page_cursor in (1..total_pages) %}
      ...
    {% endfor %}
{{< / highlight >}}

I'm using the term `page_cursor` for loop,
and `page_current` for paginator.

-- -- --

### 5: Math: Algebra

#### Assumption

Consider an example, this blog post that contain **seventeen** posts.

{{< highlight conf >}}
# CONST

Total Post   = 17
Per Page     = 2

# COMPUTED

total_pages   = 9
{{< / highlight >}}

#### Equation

And change the number of page for each pagination,
by number for example: `2`, `3`, `4`, `5`, `10`, `20`.

* [gitlab.com/.../_config.yaml][tutor-configuration]

{{< highlight yaml >}}
# jekyll-paginate-v1
paginate: 2
paginate_path: "pages/blog-:num"
{{< / highlight >}}

#### Table

Do not worry!
`total_pages` do the math internally in `jekyll`.

{{< highlight jinja >}}
{% assign total_pages = paginator.total_pages %}
{{< / highlight >}}

We can also achieve `total_pages` by **ceiling division**.
And the result is on this table below.

{{< highlight conf >}}
# ALGEBRA

+-------------+-------+-------+-------+-------+-------+
| pagination  |   2   |   3   |   5   |  10   |  20   |
+-------------+-------+-------+-------+-------+-------+
| VARIABLE                                            |
| division    |  8.5  |  5.7  |  3.4  |  1.7  |  0.85 |
| total_pages |   9   |   6   |   4   |   2   |  N/A  |
+-------------+-------+-------+-------+-------+-------+
{{< / highlight >}}

Of course, we do not need to show any pagination,
if there is only one page for all result.
That is why we can optionally,
convert `1` into `N/A`.

-- -- --

### What is Next ?

Now we are ready to begin this pagination journey.

Consider continue reading [ [Jekyll Plain - Pagination - Simple][local-whats-next] ].
We should start from the simple one.

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/06/15/jekyll-plain-pagination-simple/
[tutor-html-master-05]: {{< tutor-jekyll-plain >}}/tutor-05/

[tutor-vl-blog]:        {{< tutor-jekyll-plain >}}/tutor-05/_layouts/blog.html

[tutor-configuration]:  {{< tutor-jekyll-plain >}}/tutor-05/_config.yml
[image-05-pg-tree]:     {{< assets-ssg >}}/2020/06/plain/05-pagination-tree.png
[image-05-pg-content]:  {{< assets-ssg >}}/2020/06/plain/05-pagination-content.png
