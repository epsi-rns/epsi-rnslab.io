---
type   : post
title  : "Jekyll Plain - Pagination - Simple"
date   : 2020-06-15T09:17:35+07:00
slug   : jekyll-plain-pagination-simple
categories: [ssg]
tags      : [jekyll, plain]
keywords  : [liquid, pagination, simple, first, last, previous, next]
author : epsi
opengraph:
  image: assets-ssg/2020/06/plain/05-pagination-01-simple.png

toc    : "toc-2020-06-jekyll-step"

excerpt:
  Jekyll in plain HTML without stylesheet burden.
  Pagination for dummies, a simple start for beginner.
---

### Preface

> Goal: A Simple Pagination.

Every journey start with a single step.

#### Source Code

This article use [tutor-05][tutor-html-master-05] theme.
We will create it step by step.

-- -- --

### 1: Preview: General

This is what we want to achieve in this tutorial.

![Jekyll Pagination: Simple Pagination with Navigation][image-05-pg-simple]

#### Layout Liquid: Blog

Consider use `pagination-v1/01-simple.html` partial,
in `_layouts/blog.html`.

* [gitlab.com/.../_layouts/blog.html][tutor-vl-blog]
  
{{< highlight jinja >}}
{% include pagination-v1/01-simple.html %}
{{< / highlight >}}

#### HTML Preview

Consider fast forward to the last page,
so we can have both enabled button and disabled button.
We need those two kinds of button as an ideal example

![Jekyll Pagination: Enable and Disable Button][image-05-pg-simple-last]

The HTML that we want to achieve in this article, is similar as below.

{{< highlight html >}}
<nav role="navigation">
    <!-- First Page. -->
      [ <a href="/pages">First</a> ]

    <!-- Previous Page. -->
      [ <a href="/pages/blog-8">Previous</a> ]

    <!-- Indicator Number. -->
      [ Page: 9 of 9 ]

    <!-- Next Page. -->
      [ Next ]

    <!-- Last Page. -->
      [ Last ]
</nav>
{{< / highlight >}}

We will achieve this with `jekyll` code.

-- -- --

### 2: Navigation: Previous and Next

{{< highlight jinja >}}
{% capture spaceless %}
  {% assign total_pages = paginator.total_pages %}

  <!-- Get paginate_root from page in frontmatter -->
  {% assign paginate_root = page.paginate_root %}
{% endcapture %}

<nav role="navigation">
  {% if total_pages > 1 %}

    <!-- Previous Page. -->
    {% if paginator.previous_page %}
      {% assign p_prev = paginator.previous_page_path
                       | prepend: site.baseurl %}
      [ <a href="{{ p_prev }}">Previous</a> ]
    {% else %}
      [ Previous ]
    {% endif %}

    <!-- Next Page. -->
    {% if paginator.next_page %}
      {% assign p_next = paginator.next_page_path
                       | prepend: site.baseurl %}
      [ <a href="{{ p_next }}">Next</a> ]
    {% else %}
      [ Next ]
    {% endif %}

  {% endif %}
</nav>
{% endif %}

{{< / highlight >}}

#### Browser: Pagination Preview

Examine any page in blog section.

* <http://localhost:4000/pages/page-9/>

#### How does it works ?

This code above rely on 

* Previous Page: `paginator.previous_page`, and 

* Next Page: `paginator.next_page`.

This should be self explanatory.

-- -- --

### 3: Navigation: First and Last

Consider also add first page and last page.

{{< highlight jinja >}}
{% capture spaceless %}
  {% assign total_pages = paginator.total_pages %}

  <!-- Get paginate_root from page in frontmatter -->
  {% assign paginate_root = page.paginate_root %}
{% endcapture %}

<nav role="navigation">
  {% if total_pages > 1 %}

    <!-- First Page. -->
    {% unless paginator.page == 1 %}
      {% assign p_first = paginate_root 
                        | prepend: site.baseurl %}
      [ <a href="{{ p_first }}">First</a> ]
    {% else %}
      [ First ]
    {% endunless %}

    <!-- Last Page. -->
    {% unless paginator.page == total_pages %}
      {% assign p_last = site.paginate_path
                       | relative_url
                       | replace: ':num', total_pages %}
      [ <a href="{{ p_last }}">Last</a> ]
    {% else %}
      [ Last ]
    {% endunless %}

  {% endif %}
</nav>
{{< / highlight >}}

#### Browser: Pagination Preview

Examine any page in blog section.

* <http://localhost:4000/blog/page-9/>

#### How does it works ?

`jekyll` utilize `paginator.page`,
to show first page and last page.

{{< highlight javascript >}}
{% assign total_pages = paginator.total_pages %} %}
{{< / highlight >}}

* First Page: `{% unless paginator.page == 1 %}`.

* Last Page: `{% unless paginator.page == total_pages %}`.

This code above rely on the calculation of:

* First Page: `p_first`, and 

* Last Page: `p_last`.

-- -- --

### 4: Active Page Indicator

And also the pagination positioning in between,
in the middle of those.

{{< highlight jinja >}}
{% capture spaceless %}
  {% assign total_pages = paginator.total_pages %}

  <!-- Get paginate_root from page in frontmatter -->
  {% assign paginate_root = page.paginate_root %}
{% endcapture %}

{% if totalPages > 1 %}
<nav class="pagination is-small is-centered" 
     role="navigation" aria-label="pagination">

    <!-- Indicator Number. -->
      [ Page: {{ paginator.page }} of {{ total_pages }} ]

</nav>
{% endif %}
{{< / highlight >}}

#### Browser: Pagination Preview

Examine any page in blog section.

* <http://localhost:4000/blog/page-9/>

#### How does it works ?

This just show:

{{< highlight jinja >}}
[ Page: {{ paginator.page }} of {{ total_pages }} ]
{{< / highlight >}}

-- -- --

### 5: Summary: Pagination-v1

Complete code is here below:

* [gitlab.com/.../_includes/pagination-v1/01-simple.html][tutor-pg-v1-simple]

{{< highlight jinja >}}
{% capture spaceless %}
  {% assign total_pages = paginator.total_pages %}

  <!-- Get paginate_root from page in frontmatter -->
  {% assign paginate_root = page.paginate_root %}
{% endcapture %}

<nav role="navigation">
  {% if total_pages > 1 %}

    <!-- First Page. -->
    {% unless paginator.page == 1 %}
      {% assign p_first = paginate_root 
                        | prepend: site.baseurl %}
      [ <a href="{{ p_first }}">First</a> ]
    {% else %}
      [ First ]
    {% endunless %}

    <!-- Previous Page. -->
    {% if paginator.previous_page %}
      {% assign p_prev = paginator.previous_page_path
                       | prepend: site.baseurl %}
      [ <a href="{{ p_prev }}">Previous</a> ]
    {% else %}
      [ Previous ]
    {% endif %}

    <!-- Indicator Number. -->
      [ Page: {{ paginator.page }} of {{ total_pages }} ]

    <!-- Next Page. -->
    {% if paginator.next_page %}
      {% assign p_next = paginator.next_page_path
                       | prepend: site.baseurl %}
      [ <a href="{{ p_next }}">Next</a> ]
    {% else %}
      [ Next ]
    {% endif %}

    <!-- Last Page. -->
    {% unless paginator.page == total_pages %}
      {% assign p_last = site.paginate_path
                       | relative_url
                       | replace: ':num', total_pages %}
      [ <a href="{{ p_last }}">Last</a> ]
    {% else %}
      [ Last ]
    {% endunless %}

  {% endif %}
</nav>
{{< / highlight >}}

#### Browser: Pagination Preview

Examine any page in blog section.

* <http://localhost:4000/blog/page-5>

![Jekyll Pagination: Simple Pagination with Navigation][image-05-pg-simple]

-- -- --

### 6: Summary: Pagination-v2

#### Difference

`pagination-v2` code is slightly different.

While in `pagination-v1`, the last URL computed by this:

{{< highlight jinja >}}
      {% assign p_last = site.paginate_path
                       | relative_url
                       | replace: ':num', total_pages %}
{{< / highlight >}}

In `pagination-v2`, the last URL computed by this:

{{< highlight jinja >}}
      {% assign p_last = site.pagination.permalink
                       | prepend: paginate_root
                       | relative_url
                       | replace: ':num', total_pages
       %}
{{< / highlight >}}

#### Complete Code

Complete code is here below:

* [gitlab.com/.../_includes/pagination-v2/01-simple.html][tutor-pg-v2-simple]

{{< highlight jinja >}}
{% capture spaceless %}
  {% assign total_pages = paginator.total_pages %}

  <!-- Get paginate_root from page in frontmatter -->
  {% assign paginate_root = page.paginate_root %}
{% endcapture %}

<nav role="navigation">
  {% if total_pages > 1 %}

    <!-- First Page. -->
    {% unless paginator.page == 1 %}
      {% assign p_first = paginate_root
                        | prepend: site.baseurl %}
       [ <a href="{{ p_first }}">First</a> ]
    {% else %}
      [ First ]
    {% endunless %}

    <!-- Previous Page. -->
    {% if paginator.previous_page %}
      {% assign p_prev = paginator.previous_page_path
                       | prepend: site.baseurl %}
      [ <a href="{{ p_prev }}">Previous</a> ]
    {% else %}
      [ Previous ]
    {% endif %}

    <!-- Indicator Number. -->
      [ Page: {{ paginator.page }} of {{ total_pages }} ]

    <!-- Next Page. -->
    {% if paginator.next_page %}
      {% assign p_next = paginator.next_page_path
                       | prepend: site.baseurl %}
      [ <a href="{{ p_next }}">Next</a> ]
    {% else %}
      [ Next ]
    {% endif %}

    <!-- Last Page. -->
    {% unless paginator.page == total_pages %}
      {% assign p_last = site.pagination.permalink
                       | prepend: paginate_root
                       | relative_url
                       | replace: ':num', total_pages
       %}
      [ <a href="{{ p_last }}">Last</a> ]
    {% else %}
      [ Last ]
    {% endunless %}

  {% endif %}
</nav>
{{< / highlight >}}

-- -- --

### What is Next ?

Consider continue reading [ [Jekyll Plain - Pagination - Number][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/06/16/jekyll-plain-pagination-number/
[tutor-html-master-05]: {{< tutor-jekyll-plain >}}/tutor-05/

[tutor-configuration]:  {{< tutor-jekyll-plain >}}/tutor-05/_config.yml
[tutor-vl-blog]:        {{< tutor-jekyll-plain >}}/tutor-05/_layouts/blog.html
[tutor-pg-v1-simple]:   {{< tutor-jekyll-plain >}}/tutor-05/_includes/pagination-v1/01-simple.html
[tutor-pg-v2-simple]:   {{< tutor-jekyll-plain >}}/tutor-05/_includes/pagination-v2/01-simple.html

[image-05-pg-tree]:     {{< assets-ssg >}}/2020/06/plain/05-pagination-tree.png
[image-05-pg-content]:  {{< assets-ssg >}}/2020/06/plain/05-pagination-content.png

[image-05-pg-simple]:       {{< assets-ssg >}}/2020/06/plain/05-pagination-01-simple.png
[image-05-pg-simple-last]:  {{< assets-ssg >}}/2020/06/plain/05-pagination-01-simple-last.png

