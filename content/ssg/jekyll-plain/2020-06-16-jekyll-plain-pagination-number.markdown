---
type   : post
title  : "Jekyll Plain - Pagination - Number"
date   : 2020-06-16T09:17:35+07:00
slug   : jekyll-plain-pagination-number
categories: [ssg]
tags      : [jekyll, plain]
keywords  : [liquid, pagination, number]
author : epsi
opengraph:
  image: assets-ssg/2020/06/plain/05-pagination-02-number.png

toc    : "toc-2020-06-jekyll-step"

excerpt:
  Jekyll in plain HTML without stylesheet burden.
  Pagination for dummies, list all of page.
---

### Preface

> Goal: Pagination using Number.

#### Source Code

This article use [tutor-05][tutor-html-master-05] theme.
We will create it step by step.

-- -- --

### 1: Prepare

#### Layout Liquid: Blog

Consider use `pagination-v1/02-number.html` partial,
in `_layouts/blog.html`.

* [gitlab.com/.../_layouts/blog.html][tutor-vl-blog]
  
{{< highlight jinja >}}
{% include pagination-v1/02-number.html %}
{{< / highlight >}}

-- -- --

### 2: Preview: General

This is what we want to achieve in this tutorial.

![Jekyll Pagination: Number Pagination without Navigation][image-05-pg-number]

#### HTML Preview

The HTML that we want to achieve in this article, is similar as below.

{{< highlight html >}}
<nav role="navigation">
  <!-- Page numbers. -->
    [ <a href="/pages">1</a> ]
    [ <a href="/pages/blog-2">2</a> ]
    [ <a href="/pages/blog-3">3</a> ]
    [ <a href="/pages/blog-4">4</a> ]
    [ 5 ]
    [ <a href="/pages/blog-6">6</a> ]
    [ <a href="/pages/blog-7">7</a> ]
    [ <a href="/pages/blog-8">8</a> ]
    [ <a href="/pages/blog-9">9</a> ]
</nav>
{{< / highlight >}}

We will achieve this with `jekyll` code.

-- -- --

### 3: Navigation: Number

Pagination by number is also simple.

#### Partial: Pagination-v1 Number

* [gitlab.com/.../_includes/pagination-v1/02-number.html][tutor-pg-v1-number]

We can make the code safer, but this is not necessary.

{{< highlight jinja >}}
{% capture spaceless %}
  {% if page.paginate_root == nil %}
    {% assign paginate_root = "/" %}
  {% else %}    
    {% assign paginate_root = page.paginate_root %}
  {% endif %}

  {% assign total_pages = paginator.total_pages %}
{% endcapture %}
{{< / highlight >}}

This below should be sufficient.

{{< highlight jinja >}}
{% assign paginate_root = page.paginate_root %}
{% assign total_pages = paginator.total_pages %}

<nav role="navigation">
  {% if total_pages > 1 %}

    {% capture spaceless %}
      {% assign p_first = paginate_root
                        | prepend: site.baseurl %}
      {% assign page_current = paginator.page %}
    {% endcapture %}

    <!-- Page numbers. -->
    {% for page_cursor in (1..total_pages) %}
      {% if page_cursor == page_current %}
        [ {{ page_cursor }} ]
      {% else %}

        {% if page_cursor == 1 %}
          {% assign p_link = p_first %}
        {% else %}
          {% assign p_link = site.paginate_path
                           | relative_url
                           | replace: ':num', page_cursor %}
        {% endif %}

        [ <a href="{{ p_link }}">{{ page_cursor }}</a> ]
       {% endif %}
    {% endfor %}

  {% endif %}
</nav>
{{< / highlight >}}

#### Partial: Pagination-v2 Number

As usual, there is a slight difference,
between `pagination-v1` and `pagination-v2`.

* [gitlab.com/.../_includes/pagination-v2/02-number.html][tutor-pg-v2-number]

{{< highlight jinja >}}
  {% assign p_link = site.pagination.permalink
                   | prepend: paginate_root
                   | relative_url
                   | replace: ':num', page_cursor
{{< / highlight >}}

#### How does it works ?

Just a simple loop:

{{< highlight jinja >}}
    {% for page_cursor in (1..total_pages) %}
      ...
    {% endfor %}
{{< / highlight >}}

And simple conditional:

{{< highlight jinja >}}
{% assign page_current = paginator.page %} %}
...

      {% if page_cursor == page_current %}
        [ {{ page_cursor }} ]
      {% else %}

        {% if page_cursor == 1 %}
          {% assign p_link = p_first %}
        {% else %}
          {% assign p_link = site.paginate_path
                           | relative_url
                           | replace: ':num', page_cursor %}
        {% endif %}

        [ <a href="{{ p_link }}">{{ page_cursor }}</a> ]
       {% endif %}
{{< / highlight >}}

There is, no `href` link for current page.

-- -- --

### 4: Navigation: Button

Sure you can combine with navigation button

* previous, and next;

* first, and last.

{{< highlight jinja >}}
<nav role="navigation">
  {% if total_pages > 1 %}

    {% capture spaceless %}
      {% assign p_first = paginate_root
                        | prepend: site.baseurl %}
      {% assign page_current = paginator.page %}
    {% endcapture %}

    <!-- First Page. -->
    {% unless paginator.page == 1 %}
      [ <a href="{{ p_first }}">First</a> ]
    {% else %}
      [ First ]
    {% endunless %}

    <!-- Previous Page. -->
    {% if paginator.previous_page %}
      {% assign p_prev = paginator.previous_page_path
                       | prepend: site.baseurl %}
      [ <a href="{{ p_prev }}">Previous</a> ]
    {% else %}
      [ Previous ]
    {% endif %}

    <!-- Page numbers. -->
    ...

    <!-- Next Page. -->
    {% if paginator.next_page %}
      {% assign p_next = paginator.next_page_path
                       | prepend: site.baseurl %}
      [ <a href="{{ p_next }}">Next</a> ]
    {% else %}
      [ Next ]
    {% endif %}

    <!-- Last Page. -->
    {% unless paginator.page == total_pages %}
      {% assign p_last = site.paginate_path
                       | relative_url
                       | replace: ':num', total_pages
       %}
      [ <a href="{{ p_last }}">Last</a> ]
    {% else %}
      [ Last ]
    {% endunless %}

  {% endif %}
</nav>
{{< / highlight >}}

We are going to make this code simpler,
by using filter plugin in later chapter.

-- -- --

### What is Next ?

Consider continue reading [ [Jekyll Plain - Pagination - Adjacent][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/06/17/jekyll-plain-pagination-adjacent/
[tutor-html-master-05]: {{< tutor-jekyll-plain >}}/tutor-05/

[tutor-configuration]:  {{< tutor-jekyll-plain >}}/tutor-05/_config.yml
[tutor-vl-blog]:        {{< tutor-jekyll-plain >}}/tutor-05/_layouts/blog.html
[tutor-pg-v1-number]:   {{< tutor-jekyll-plain >}}/tutor-05/_includes/pagination-v1/02-number.html
[tutor-pg-v2-number]:   {{< tutor-jekyll-plain >}}/tutor-05/_includes/pagination-v2/02-number.html

[image-05-pg-number]:   {{< assets-ssg >}}/2020/06/plain/05-pagination-02-number.png

