---
type   : post
title  : "Jekyll Plain - Pagination - Logic"
date   : 2020-06-18T09:17:35+07:00
slug   : jekyll-plain-pagination-logic
categories: [ssg]
tags      : [jekyll, plain]
keywords  : [liquid, pagination, adjacent, ruby]
author : epsi
opengraph:
  image: assets-ssg/2020/06/plain/05-jekyll-liquid-is-show-adjacent.png

toc    : "toc-2020-06-jekyll-step"

excerpt:
  Jekyll in plain HTML without stylesheet burden.
  Reverse engineering of Glenn McComb Pagination using Math and Table.
---

### Preface

> Goal: Explaining Glenn McComb Pagination using Math and Table

#### Source Code

This article use [tutor-05][tutor-html-master-05] theme.
We will create it step by step.

-- -- --

### 1: Preview: Detail

Consider, have a look at the pagination below in a stripped down model.

#### Layout Liquid: Blog

Consider use `pagination-v1/03-adjacent.html` partial,
in `_layouts/blog.html`.

* [gitlab.com/.../_layouts/blog.html][tutor-vl-blog]
  
{{< highlight jinja >}}
{% include pagination-v1/03-adjacent.html %}
{{< / highlight >}}

This link offset in `adjacent` partial rely on,
complex `page_current_flag` calculation.

#### Skeleton

Which the calulation itself has this skeleton below:

{{< highlight jinja >}}
      {% capture spaceless %}
        <!-- Flag Calculation -->
        {% assign page_current_flag = false %}

        {% if total_pages > link_max %}
        <!-- Complex page numbers. -->

          <!-- Lower limit pages. -->
          {% if page_current <= limit_lower %}
            ...

          <!-- Upper limit pages. -->
          {% elsif page_current >= limit_upper %}
            ...

          <!-- Middle pages. -->
          {% else %}
            ...

          {% endif %}

        {% else %}
        <!-- Simple page numbers. -->

          {% assign page_current_flag = true %}
        {% endif %}
      {% endcapture %}
{{< / highlight >}}

![Jekyll Pagination: Liquid: page_current_flag][image-vim-liquid]]

#### Structure

This will only show one part:

* Middle Pagination: **Glenn McComb**

#### Each Pagination

Consider, have a look at the animation above, frame by frame.
I'm going to do some reverse engineering,
to accomplish better understanding on how this pagination works.

We have from first page (1), to last page (9).

![Jekyll Pagination: Adjacent Page 1][image-pg-stripped-01]

![Jekyll Pagination: Adjacent Page 2][image-pg-stripped-02]

![Jekyll Pagination: Adjacent Page 3][image-pg-stripped-03]

![Jekyll Pagination: Adjacent Page 4][image-pg-stripped-04]

![Jekyll Pagination: Adjacent Page 5][image-pg-stripped-05]

![Jekyll Pagination: Adjacent Page 6][image-pg-stripped-06]

![Jekyll Pagination: Adjacent Page 7][image-pg-stripped-07]

![Jekyll Pagination: Adjacent Page 8][image-pg-stripped-08]

![Jekyll Pagination: Adjacent Page 9][image-pg-stripped-09]

#### Table

We can rewrite the table with additional rows as below.

{{< highlight conf >}}
+-------------+-------+-------+-------+-------+-------+
| pagination  |   2   |   3   |   5   |  10   |  20   |
+-------------+-------+-------+-------+-------+-------+
| VARIABLE                                            |
| total_pages |   9   |   6   |   4   |   2   |  N/A  |
| max_links   |   5   |   5   |   5   |   5   |  N/A  |
| lower_limit |   3   |   3   |   3   |   3   |  N/A  |
| upper_limit |   7   |   4   |   2   |   0   |  N/A  |
+-------------+-------+-------+-------+-------+-------+
| MIDDLE PAGINATION                                   |
| page  = 1   | 1..5  | 1..5  | 1..4  | 1..2  |-------+
| page  = 2   | 1..5  | 1..5  | 1..4  | 1..2  |       |
| page  = 3   | 1..5  | 1..5  | 1..4  |-------+       |
| page  = 4   | 2..6  | 4..6  | 1..4  |               |
| page  = 5   | 3..7  | 4..6  |-------+               |
| page  = 6   | 4..8  | 4..6  |                       |
| page  = 7   | 5..9  |-------+                       |
| page  = 8   | 5..9  |                               |
| page  = 9   | 5..9  |                               |
+-------------+-------+-------------------------------+
{{< / highlight >}}

-- -- --

### 2: Math: Conditional

#### Part: Middle Pages

This is already discussed in, so I won't explain it nomore.

* <https://glennmccomb.com/articles/how-to-build-custom-hexo-pagination/>

{{< highlight ruby >}}
  <!-- Lower limit pages. -->
  {% if page_current <= limit_lower %}
    ...

  <!-- Upper limit pages. -->
  {% elsif page_current >= limit_upper %}
    ...

  <!-- Middle pages. -->
  {% else %}

    {% if (page_cursor >= lower_offset) and (page_cursor <= upper_offset) %}
      {% assign page_current_flag = true %}
    {% endif %}

  {% endif %}
{{< / highlight >}}

What you need to know is the conditional result in table:

{{< highlight conf >}}
+-------------+-------+
| pagination  |   2   |
| adjacent    |   2   |
| total post  |  17   |
+-------------+-------+
| VARIABLE            |
| total_pages |   9   |
| max_links   |   5   |
| lower_limit |   3   |
| upper_limit |   7   |
+-------------+-------+--+
| selected    | adjacent |
+-------------+----------+
| page  =  1  |   1..3   |
| page  =  2  |   1..4   |
| page  =  3  |   1..5   |
| page  =  4  |   2..6   |
| page  =  5  |   3..7   |
| page  =  6  |   4..8   |
| page  =  7  |   5..9   |
| page  =  8  |   5..9   |
| page  =  9  |   5..9   |
+-------------+----------+
{{< / highlight >}}

#### Part: Lower Limit Pages

Consider stripped more for each part.

{{< highlight ruby >}}
  <!-- Lower limit pages. -->
  {% if page_current <= limit_lower %}

    <!-- If the current loop page is less than max_links. -->
    {% if page_cursor <= min_lower %}
      {% assign page_current_flag = true %}
    {% endif %}

  <!-- Upper limit pages. -->
  {% elsif page_current >= limit_upper %}
    ...

  <!-- Middle pages. -->
  {% else %}
    ...

  {% endif %}
{{< / highlight >}}

Notice that there is two part of conditional.

* Outer conditional:
  result true for the first three row,
  as defined by `lower_limit`.

* Inner conditional: always result `1..5`.

Thus, the conditional result in table:

{{< highlight conf >}}
+------------+-------+-------+--------+
| selected   | lower | l max | result |
+------------+-------+-------+--------+
| page  =  1 |   T   | 1..5  |  1..5  |
| page  =  2 |   T   | 1..5  |  1..5  |
| page  =  3 |   T   | 1..5  |  1..5  |
| page  =  4 |       | 1..5  |        |
| page  =  5 |       | 1..5  |        |
| page  =  6 |       | 1..5  |        |
| page  =  7 |       | 1..5  |        |
| page  =  8 |       | 1..5  |        |
| page  =  9 |       | 1..5  |        |
+------------+-------+-------+--------+
{{< / highlight >}}

#### Part: Upper Limit Pages

Now the `upper limit` condtitional.

{{< highlight ruby >}}
  <!-- Lower limit pages. -->
  {% if page_current <= limit_lower %}
    ...

  <!-- Upper limit pages. -->
  {% elsif page_current >= limit_upper %}

    <!-- If the current loop page is greater than total pages minus $max_links -->
    {% if page_cursor > max_upper %}
      {% assign page_current_flag = true %}
    {% endif %}

  <!-- Middle pages. -->
  {% else %}
    ...

  {% endif %}
{{< / highlight >}}

#### Combined: All Conditional

Now we have all the logic combined at once.

{{< highlight conf >}}
+-------------+-------+
| pagination  |   1   |
| adjacent    |   2   |
| total_post  |  10   |
+-------------+-------+
| VARIABLE            |
| total_pages |  10   |
| max_links   |   5   |
| lower_limit |   3   |
| upper_limit |   7   |
+-------------+-------+-+-------+-------+-------+-------+
| selected    | adjacent| lower | l max | upper | u max |
+-------------+---------+-------+-------+-------+-------+
| cursor = 1  |  1..3   |   T   | 1..5  |       | 5..9  |
| cursor = 2  |  1..4   |   T   | 1..5  |       | 5..9  |
| cursor = 3  |  1..5   |   T   | 1..5  |       | 5..9  |
| cursor = 4  |  2..6   |       | 1..5  |       | 5..9  |
| cursor = 5  |  3..7   |       | 1..5  |       | 5..9  |
| cursor = 6  |  4..8   |       | 1..5  |       | 5..9  |
| cursor = 7  |  5..9   |       | 1..5  |   T   | 5..9  |
| cursor = 8  |  5..9   |       | 1..5  |   T   | 5..9  |
| cursor = 9  |  5..9   |       | 1..5  |   T   | 5..9  |
+-------------+---------+-------+-------+-------+-------+
{{< / highlight >}}

#### Final Result

As a conclusion table.

{{< highlight conf >}}
+-------------+-------+
| VARIABLE            |
| total_pages |  10   |
| max_links   |   5   |
| lower_limit |   3   |
| upper_limit |   8   |
+-------------+-------+-------+---------+
| selected    | lower | upper | adjacent|
+-------------+-------+-------+---------+
| cursor = 1  | 1..5  |       |         |
| cursor = 2  | 1..5  |       |         |
| cursor = 3  | 1..5  |       |         |
| cursor = 4  |       |       |  2..6   |
| cursor = 5  |       |       |  3..7   |
| cursor = 6  |       |       |  4..8   |
| cursor = 7  |       | 5..9  |         |
| cursor = 8  |       | 5..9  |         |
| cursor = 9  |       | 5..9  |         |
+-------------+-------+-------+---------+
| selected    | if elsif else | result  |
+-------------+---------------+---------+
| cursor = 1  |               |  1..5   |
| cursor = 2  |               |  1..5   |
| cursor = 3  |               |  1..5   |
| cursor = 4  |               |  2..6   |
| cursor = 5  |               |  3..7   |
| cursor = 6  |               |  4..8   |
| cursor = 7  |               |  5..9   |
| cursor = 8  |               |  5..9   |
| cursor = 9  |               |  5..9   |
+-------------+---------------+---------+
{{< / highlight >}}

-- -- --

### What is Next ?

Consider continue reading [ [Jekyll Plain - Pagination - Indicator][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/06/19/jekyll-plain-pagination-indicator/
[tutor-html-master-05]: {{< tutor-jekyll-plain >}}/tutor-05/
[glennmccomb]:          https://glennmccomb.com/articles/how-to-build-custom-hugo-pagination/

[tutor-vl-blog]:        {{< tutor-jekyll-plain >}}/tutor-05/_layouts/blog.html
[tutor-pg-v1-adjacent]: {{< tutor-jekyll-plain >}}/tutor-05/_includes/pagination-v1/03-adjacent.html
[tutor-pg-v2-adjacent]: {{< tutor-jekyll-plain >}}/tutor-05/_includes/pagination-v2/03-adjacent.html

[image-pg-stripped-01]: {{< assets-ssg >}}/2020/06/plain/05-pg-03-adjacent-page-01.png
[image-pg-stripped-02]: {{< assets-ssg >}}/2020/06/plain/05-pg-03-adjacent-page-02.png
[image-pg-stripped-03]: {{< assets-ssg >}}/2020/06/plain/05-pg-03-adjacent-page-03.png
[image-pg-stripped-04]: {{< assets-ssg >}}/2020/06/plain/05-pg-03-adjacent-page-04.png
[image-pg-stripped-05]: {{< assets-ssg >}}/2020/06/plain/05-pg-03-adjacent-page-05.png
[image-pg-stripped-06]: {{< assets-ssg >}}/2020/06/plain/05-pg-03-adjacent-page-06.png
[image-pg-stripped-07]: {{< assets-ssg >}}/2020/06/plain/05-pg-03-adjacent-page-07.png
[image-pg-stripped-08]: {{< assets-ssg >}}/2020/06/plain/05-pg-03-adjacent-page-08.png
[image-pg-stripped-09]: {{< assets-ssg >}}/2020/06/plain/05-pg-03-adjacent-page-09.png

[image-vim-liquid]: {{< assets-ssg >}}/2020/06/plain/05-jekyll-liquid-is-show-adjacent.png
[image-vim-filter]: {{< assets-ssg >}}/2020/06/plain/05-jekyll-liquid-plugin-is-show-adjacent.png
[image-vim-plugin]: {{< assets-ssg >}}/2020/06/plain/05-jekyll-plugin-is-show-adjacent.png
