---
type   : post
title  : "Jekyll Plain - Plugin - Tag Names"
date   : 2020-06-23T09:17:35+07:00
slug   : jekyll-plain-plugin-tag-names
categories: [ssg]
tags      : [jekyll, plain]
keywords  : [liquid, ruby, plugin, tag names]
author : epsi
opengraph:
  image: assets-ssg/2020/06/plain/06-vim-tag-names.png

toc    : "toc-2020-06-jekyll-step"

excerpt:
  Jekyll in plain HTML without stylesheet burden.  
  Using generator to manage pages for each tag name.
---

### Preface

> Goal: Using generator to manage pages for each tag name.

#### Source Code

This article use [tutor-06][tutor-html-master-06] theme.
We will create it step by step.

-- -- --

### Introduction

#### Official Documentation

The code in this page is almost a verbatim copy from:

* [jekyllrb.com/docs/plugins/generators][jekyll-generators]

Applying this short snippets in real life blog,
turned out a wide topic, that deserve an article of its own.

#### Tag Names

> What is this rubbish?

This means we can make pages based on tags such as:

* http://localhost:4000/tags/soul/

* http://localhost:4000/tags/jazz/

Jekyll cannot generate this extended URL by default,
but Jekyll allow you to create this using plugin.
Or to be exact, generator plugin,
with the generated files result as below:

{{< highlight bash >}}
$ tree _site/tags
_site/tags
├── 2010s
│   └── index.html
├── husky
│   └── index.html
├── index.html
├── indie
│   └── index.html
├── jazz
│   └── index.html
├── pop
│   └── index.html
├── rock
│   └── index.html
├── sleepy
│   └── index.html
├── soul
│   └── index.html
├── story
│   └── index.html
└── subtitle
    └── index.html

10 directories, 11 files
{{< / highlight >}}

![Jekyll: Tags NERDTree][image-06-tags-tree]

#### Caveat

> Not all CI/CD support plugin.

If your CI/CD do not support jekyll plugin,
then you cannot use any plugin,
including this custom generator plugin

#### Preparation: Using Generator

All you need to care is to make a layout name, such as `tag-names.html`.
You can choose any name, as long as it match the code in your plugin.

* [gitlab.com/.../_layouts/tag-names.html][tutor-vl-tag-names]

{{< highlight bash >}}
---
layout: page
---
{{< / highlight >}}

-- -- --

### 1: Generator: Tag Names

#### The Original Code

> Hmmm....

I do not really understand ruby,
but the example code in official site contain this two classes.

{{< highlight ruby >}}
module Jekyll
  class TagPageNameGenerator < Generator
    ...
  end

  # A Page subclass used in the `TagPageGenerator`
  class TagPageName < Page
    ...
  end
end
{{< / highlight >}}

The `TagPageNameGenerator`.

{{< highlight ruby >}}
  class TagPageNameGenerator < Generator
    safe true

    def generate(site)
      if site.layouts.key? 'tags'
        dir = site.config['tag_dir'] || 'tags'
        site.tags.each_key do |tag|
          site.pages << TagPageName.new(
            site, site.source, File.join(dir, tag), tag)
        end
      end
    end
  end
{{< / highlight >}}

And `TagPageName`.

{{< highlight ruby >}}
  # A Page subclass used in the `TagPageGenerator`
  class TagPageName < Page
    def initialize(site, base, dir, tag)
      @site = site
      @base = base
      @dir  = dir
      @name = 'index.html'

      self.process(@name)
      self.read_yaml(File.join(base, '_layouts'), 'tags.html')
      self.data['tag'] = tag

      tag_title_prefix = site.config['tag_title_prefix'] || 'Tag: '
      self.data['title'] = "#{tag_title_prefix}#{tag}"
    end
  end
{{< / highlight >}}

This should be working with your `tags.html` layout.

#### What does it offer?

> Requirement?

How about an adjustment, to suit my needs?

1. Source Layout Name: `tag-names.html`

2. Destination files: `tags/*/index.html`

3. Content: `tag` name and `post`.

#### Changing source name:

The first one is easy, just change the name

The `TagPageNameGenerator`.

{{< highlight ruby >}}
    def generate(site)
      if site.layouts.key? 'tag-names'
        dir = site.config['tag_dir'] || 'tags'
        ...
      end
    end
{{< / highlight >}}

And `TagPageName`.

{{< highlight ruby >}}
    def initialize(site, base, dir, tag)
      ...

      self.process(@name)
      self.read_yaml(File.join(base, '_layouts'), 'tag-names.html')
      self.data['tag'] = tag

      ...
    end
{{< / highlight >}}

#### The Need of Content

We do not want empty pages right?

As a preparation, our page can simply contain badge of tags as usual.

{{< highlight bash >}}
---
layout: page
aside_message : This line will be ignored
---

{% assign terms = site.tags %}
{% assign term_array = terms | term_array | sort %}
{% include index/terms-badge.html %}
{{< / highlight >}}

![Jekyll: Tags: Badges Only][image-06-tags-badges]

Of course this is note enough,
we need something more in each tag page,
such as showing all post related to that tag.

* [gitlab.com/.../_layouts/tag-names.html][tutor-vl-tag-names]

{{< highlight bash >}}
---
layout: page
aside_message : This line will be ignored
---

{% assign terms = site.tags %}
{% assign term_array = terms | term_array | sort %}
{% include index/terms-badge.html %}

{% assign posts = page.posts %}
{% include index/blog-list.html %}
{{< / highlight >}}

![Jekyll: Tags: Blog List][image-06-tags-list]

#### The Riddle?

> How do I get this `page.posts`?

Is is more like a question of where, rather than how to,
because al you need to do is just adding this one line.

{{< highlight ruby >}}
    def initialize(site, base, dir, tag)
      ...

      self.process(@name)
      self.read_yaml(File.join(base, '_layouts'), 'tag-names.html')
      self.data['tag'] = tag
      self.data['posts'] = site.tags[tag]

      ...
    end
{{< / highlight >}}

Notice this bad boy: `self.data['posts'] = site.tags[tag]`.

#### Add Decoration

You can also add a slight change to make the tag name capitalized.

{{< highlight ruby >}}
    def initialize(site, base, dir, tag)
      ...

      tag_title_prefix = site.config['tag_title_prefix'] || 'Tag: '
      self.data['title'] = "#{tag_title_prefix}#{tag.capitalize}"
    end
{{< / highlight >}}

#### Complete Plugin Code

Now we can summarized the ruby generator plugin code.

* [gitlab.com/.../_plugins/tag-names.rb][tutor-pl-tag-names]

{{< highlight ruby >}}
module Jekyll
  class TagPageNameGenerator < Generator
    safe true

    def generate(site)
      if site.layouts.key? 'tag-names'
        dir = site.config['tag_dir'] || 'tags'
        site.tags.each_key do |tag|
          site.pages << TagPageName.new(
            site, site.source, File.join(dir, tag), tag)
        end
      end
    end
  end

  # A Page subclass used in the `TagPageGenerator`
  class TagPageName < Page
    def initialize(site, base, dir, tag)
      @site = site
      @base = base
      @dir  = dir
      @name = 'index.html'

      self.process(@name)
      self.read_yaml(File.join(base, '_layouts'), 'tag-names.html')
      self.data['tag'] = tag
      self.data['posts'] = site.tags[tag]

      tag_title_prefix = site.config['tag_title_prefix'] || 'Tag: '
      self.data['title'] = "#{tag_title_prefix}#{tag.capitalize}"
    end
  end
end
{{< / highlight >}}

![Jekyll: Generator Plugin: Tag Names][image-06-tag-names]

We are finished with the generator plugin.
But we are not done yet with this cool topic.

-- -- --

### 2: Layout: Changing Include View

#### Alternate View

If you looks closely in my repository,
there is also a few lines in commented code:

* [gitlab.com/.../_layouts/tag-names.html][tutor-vl-tag-names]

{{< highlight jinja >}}
---
layout: page
aside_message : This line will be ignored
---

{% assign terms = site.tags %}
{% assign term_array = terms | term_array | sort %}
{% include index/terms-badge.html %}

{% assign posts = page.posts %}
{% include index/blog-list.html %}

{% comment %}
  Alternative view
  {% include index/by-month.html %}
  {% include index/by-year.html %}
  {% include index/blog-list.html %}
{% endcomment %}
{{< / highlight >}}

![Jekyll: Tag Names: as Blog List][image-06-tags-list]

#### Tags: by Year

Consider see how that tag name pages looks,
in archive `by-year` list fashioned.

{{< highlight jinja >}}
{% assign posts = page.posts %}
{% include index/by-year.html %}
{{< / highlight >}}

![Jekyll: Tag Names: as Archive by Year][image-06-tags-by-year]

#### Tags: by Month

And how that tag name pages looks,
in archive `by-month` list fashioned.

{{< highlight jinja >}}
{% assign posts = page.posts %}
{% include index/by-month.html %}
{{< / highlight >}}

![Jekyll: Tag Names: as Archive by Month][image-06-tags-by-month]

I hope that you got the point,
why I separate the reusable code in `_includes`,
instead of just put them in `layouts`.

-- -- --

### 3: Adjustment: URL Changes

We are still, not done yet.
Changes might impact URLs.
So we need adjustment in a few places.
In this simple blog site, only two places.

#### Partial Liquid: Terms Badge

Adjust the `href` to change URL from

{{< highlight jinja >}}
  <a href="#{{ this_word | slugify }}">
{{< / highlight >}}

To

{{< highlight jinja >}}
  <a href="{{ site.baseurl }}/tags/{{ this_word | slugify }}/">
{{< / highlight >}}

* [gitlab.com/.../_includes/index/terms-badge.html][tutor-vi-terms-badge]

{{< highlight jinja >}}
  <p>Tag Badges:
  {% for item in (0..terms.size) %}{% unless forloop.last %}
    {% assign this_word = term_array[item] | strip_newlines %}
    <span style="white-space: nowrap;">[
      <a href="{{ site.baseurl }}/tags/{{ this_word | slugify }}/">
        {{ this_word }}
      </a> 
      <small>({{ terms[this_word].size }})</small>
    ]</span>
    &nbsp;
  {% endunless %}{% endfor %}
  </p>
{{< / highlight >}}

#### Partial Liquid: Blog List

Adjust the `href` to change URL from

{{< highlight jinja >}}
  [ <a href="{{ site.baseurl }}/tags/#{{ tag | slugify }}"
      >{{ tag }}</a> ]
{{< / highlight >}}

To

{{< highlight jinja >}}
  [ <a href="{{ site.baseurl }}/tags/{{ tag | slugify }}"
      >{{ tag }}</a> ]
{{< / highlight >}}

* [gitlab.com/.../_includes/index/blog-list.html][tutor-vi-blog-list]

{{< highlight jinja >}}
  <ul>
  {% for post in posts %}
    <li>
      <a href="{{ site.baseurl }}{{ post.url }}">
        {{ post.title }}
      </a><br/>
      Tag: 
      {% for tag in post.tags %}
        [ <a href="{{ site.baseurl }}/tags/{{ tag | slugify }}"
            >{{ tag }}</a> ]
      {% endfor %}
    </li>
  {% endfor %}
  </ul>
{{< / highlight >}}

> And that is all.

In the next chapter with CSS frameworks with a lot of content,
we are going to need more adjustment.
Just be aware of URL changes,
when you need apply something new.

-- -- --

### What is Next ?

Consider continue reading [ [Jekyll Plain - Theme - Bundling][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/06/24/jekyll-plain-theme-bundling/
[jekyll-generators]:    https://jekyllrb.com/docs/plugins/generators/
[tutor-html-master-06]: {{< tutor-jekyll-plain >}}/tutor-06/

[tutor-vl-tag-names]:   {{< tutor-jekyll-plain >}}/tutor-06/_layouts/tag-names.html
[tutor-pl-tag-names]:   {{< tutor-jekyll-plain >}}/tutor-06/_plugins/tag-names.rb
[tutor-vi-terms-badge]: {{< tutor-jekyll-plain >}}/tutor-06/_includes/index/terms-badge.html
[tutor-vi-blog-list]:   {{< tutor-jekyll-plain >}}/tutor-06/_includes/index/blog-list.html

[image-06-tags-tree]:   {{< assets-ssg >}}/2020/06/plain/06-tags-nerdtree.png
[image-06-tags-badges]: {{< assets-ssg >}}/2020/06/plain/06-tags-badges-only.png
[image-06-tags-list]:   {{< assets-ssg >}}/2020/06/plain/06-tags-blog-list.png
[image-06-tags-by-year]:    {{< assets-ssg >}}/2020/06/plain/06-tags-by-year.png
[image-06-tags-by-month]:   {{< assets-ssg >}}/2020/06/plain/06-tags-by-month.png
[image-06-tag-names]:   {{< assets-ssg >}}/2020/06/plain/06-vim-tag-names.png

