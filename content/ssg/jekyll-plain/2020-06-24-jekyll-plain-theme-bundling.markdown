---
type   : post
title  : "Jekyll Plain - Theme Bundling"
date   : 2020-06-24T09:17:35+07:00
slug   : jekyll-plain-theme-bundling
categories: [ssg]
tags      : [jekyll, plain]
keywords  : [liquid, ruby, gem, bundling theme]
author : epsi
opengraph:
  image: assets-ssg/2020/06/plain/06-vim-tag-names.png

toc    : "toc-2020-06-jekyll-step"

excerpt:
  Jekyll in plain HTML without stylesheet burden.  
  Bundling theme in Gem, for use with other people.
---

### Preface

> Goal: Bundling theme in Gem, for use with other people.

#### Source Code

This article use [tutor-07][tutor-html-master-07] theme.
We will create it step by step.

-- -- --

### Introduction

This chapter is a little bit different.
We are not talking about just Jekyll anymore,
but more about Jekyll in Gemfile, and how to use the Gem properly.

What can be bundled?

1. `_includes`, and

2. `_layouts`,

And additionaly

3. `assets`, and

4. `_sass`.

#### Directory Structure

Now consider make two separate directories:

1. `gem`, for theme

2. `src`, for Jekyll source

Then move both `_includes`, and `_layouts` to `gem` directory.

{{< highlight bash >}}
$ tree -d -L 2
.
├── gem
│   ├── _includes
│   └── _layouts
└── src
    ├── pages
    ├── _posts
    └── _site

7 directories
{{< / highlight >}}

It is easier to have directory side by side as above __.__
So you can move from one directory to another,
or put something back, whenever there is trouble.

![Jekyll: Manage Directories Between Gem and Jekyll Source][image-07-tutor-tree]

I added MIT `LICENSE` file, just for example sake.

-- -- --

### 1: Build Gemfile

We need to prepare `gemspec`, the Ruby Gem specification.

#### Official Documentation

It is easy to google, this one:

* [guides.rubygems.org/make-your-own-gem][make-your-own-gem]

* [guides.rubygems.org/specification-reference][specification-reference]

#### Ruby Gem Specification

* [gitlab.com/.../gem/oriclone-plain.gemspec][gem-specification]

{{< highlight ruby >}}
Gem::Specification.new do |spec|
  spec.name        = 'oriclone-plain'
  spec.version     = '0.1.0'
  spec.date        = '2020-07-06'
  spec.authors     = ["E.R. Nurwijayadi"]
  spec.email       = 'epsi.nurwijayadi@gmail.com'

  spec.summary     = "Oriclone Plain - Jekyll Theme"
  spec.description = "An example of Jekyll Theme without Stylesheet Burden"
  spec.homepage    = "https://gitlab.com/epsi-rns/tutor-jekyll-plain"
  spec.license     = "MIT"

  require 'rake'
  spec.files       = Dir.glob("{_layouts,_includes}/**/**/*") +
                     ['LICENSE']
end
{{< / highlight >}}

![Jekyll: oriclone-plain.gemspec][image-07-gemspec]

The early lines in specification above is just string.
The issue comes with files, especially for ruby beginner like me.

#### Files Specification

How about multiple files in multiple directories?

In a few of article I read in the internet suggest to use `git`.
The issue rise, for my tutorial, because I'm not always put files,
in git folder.

{{< highlight ruby >}}
  spec.files         = git ls-files -z.split("\x0")... }
{{< / highlight >}}

But I finally find a way to `glob`,
even though I never see any gem using this method.

{{< highlight ruby >}}
  require 'rake'
  spec.files       = Dir.glob("{_layouts,_includes}/**/**/*") +
                     ['LICENSE']
{{< / highlight >}}

> Sorry for my Ruby skill.

#### Bundle Theme in Gem

Just as the official document said:

{{< highlight bash >}}
$ cd gem
$ gem build oriclone-plain.gemspec
  Successfully built RubyGem
  Name: oriclone-plain
  Version: 0.1.0
  File: oriclone-plain-0.1.0.gem
{{< / highlight >}}

![Jekyll: gem build oriclone-plain.gemspec][image-07-gem-build]

You should see a file named `oriclone-plain-0.1.0.gem` pop up,
in directory.

#### Install Bundled Theme

Now install, the theme we just bundled.

{{< highlight bash >}}
$ gem install ./oriclone-plain-0.1.0.gem
Successfully installed oriclone-plain-0.1.0
Parsing documentation for oriclone-plain-0.1.0
Installing ri documentation for oriclone-plain-0.1.0
Done installing documentation for oriclone-plain after 1 seconds
1 gem installed
{{< / highlight >}}

![Jekyll: gem install ./oriclone-plain-0.1.0.gem][image-07-gem-install]

#### Check The Installation.

Now install, the theme we just bundled.

{{< highlight bash >}}
$ ls ~/.rvm/gems/ruby-2.6.5/gems | grep oriclone
oriclone-plain-0.1.0

$ tree -d -L 1 ~/.rvm/gems/ruby-2.6.5/gems/oriclone-plain-0.1.0
/home/epsi/.rvm/gems/ruby-2.6.5/gems/oriclone-plain-0.1.0
├── _includes
└── _layouts

2 directories
{{< / highlight >}}

Just make sure the theme is there __.__

![Jekyll: Gems in RVM][image-07-rvm-gems]

I believe we are ready to use the gem theme.

-- -- --

### 2: Using Gemfile

This require changing in both `Gemfile` and `_config.yml`.

#### Gemfile

Now we can get rid of `minima`, and use our very own theme.

{{< highlight bash >}}
source "https://rubygems.org"
gem "jekyll", "~> 4.1.1"

# gem "minima", "~> 2.0"
gem "oriclone-plain", "~> 0.1.0"

group :jekyll_plugins do
  gem "jekyll-paginate"
# gem "jekyll-paginate-v2"
end
{{< / highlight >}}

![Jekyll: Gemfile in Jekyll Source Directory][image-07-src-gemfile]

{{< highlight bash >}}
$ bundle install
...
Using jekyll 4.1.1
Using jekyll-paginate 1.1.0
Using oriclone-plain 0.1.0
Bundle complete! 3 Gemfile dependencies, 30 gems now installed.
Use `bundle info [gemname]` to see where a bundled gem is installed.
{{< / highlight >}}

![Jekyll: Source Directory: bundle install][image-07-src-bundle-i]

#### Configuration

* [gitlab.com/.../_config.yaml][tutor-configuration]

{{< highlight yaml >}}
# Custom Gem Theme
theme: oriclone-plain
{{< / highlight >}}

Now run `jekyll serve`. Your site should working properly.

![Jekyll: Qutebrowser: Home][image-07-src-home]

Just be aware that, not every `_includes` or even `_layouts`,
should be moved to Gem theme.
For flexibility reason, some shortcodes, or other `_includes`,
that tightly closed to content, or assets, or change regularly,
should be better to be placed outside bundle.

-- -- --

### Conclusion

We are done with `jekyll plain`,
but we still have `jekyll` with `stylesheet` to cover.
Let's put on clothes to our plain jekyll,
and make the blog looks fashionable.

The direct chain of this theme bundling article is:
either [ [Jekyll Bootstrap - CSS Intro][local-whats-bsoc] ],
or [ [Jekyll Bulma MD - CSS Intro][local-whats-bmd] ].

-- -- --

### What is Next?

For now, consider make your site live, and continue later with stylesheet.
Consider continue reading [ [Jekyll Plain - Deploy - Simple][local-whats-next] ].

Thank you for reading.

[//]: <> ( -- -- -- links below -- -- -- )

[local-whats-next]:     {{< baseurl >}}ssg/2020/06/25/jekyll-deploy-simple/
[local-whats-bsoc]:     {{< baseurl >}}ssg/2020/07/02/jekyll-bsoc-css-intro/
[local-whats-bmd]:      {{< baseurl >}}ssg/2020/08/02/jekyll-bmd-css-intro/
[tutor-html-master-07]: {{< tutor-jekyll-plain >}}/tutor-07/
[specification-reference]:  https://guides.rubygems.org/specification-reference/
[make-your-own-gem]:        https://guides.rubygems.org/make-your-own-gem/

[tutor-configuration]:  {{< tutor-jekyll-plain >}}/tutor-07/src/_config.yml
[gem-specification]:    {{< tutor-jekyll-plain >}}/tutor-07/gem/oriclone-plain.gemspec

[image-07-tutor-tree]:  {{< assets-ssg >}}/2020/06/plain/07-tutor-nerdtree.png
[image-07-gemspec]:     {{< assets-ssg >}}/2020/06/plain/07-oriclone-plain-gemspec.png
[image-07-gem-build]:   {{< assets-ssg >}}/2020/06/plain/07-gem-build.png
[image-07-gem-install]: {{< assets-ssg >}}/2020/06/plain/07-gem-install.png
[image-07-rvm-gems]:    {{< assets-ssg >}}/2020/06/plain/07-rvm-gems.png
[image-07-src-gemfile]: {{< assets-ssg >}}/2020/06/plain/07-src-gemfile.png
[image-07-src-bundle-i]:{{< assets-ssg >}}/2020/06/plain/07-src-bundle-install.png
[image-07-src-home]:    {{< assets-ssg >}}/2020/06/plain/07-src-home.png

