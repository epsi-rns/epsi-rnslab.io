function createPaths(svg_root, range) {
  const xmlns = 'http://www.w3.org/2000/svg';
  const blueScale = [     '#E3F2FD', 
    '#BBDEFB', '#90CAF9', '#64B5F6',
    '#42A5F5', '#2196F3', '#1E88E5',
    '#1976D2', '#1565C0', '#0D47A1'];

  range.forEach((_, index) => {
    area = document.createElementNS(xmlns,"path");
    area.setAttribute("class", "area");
    area.setAttribute("id", `svg_area_${index}`);
    area.setAttribute("fill", blueScale[index]);
    svg_root.appendChild(area);
  });
}

function getBezierParams(index) {
  const i = index + 1;

  // Initialize with destructuring style
  let [mx, my] = [0, 190];
  let [c1x, c2x, c3x] = [-10, 70, 120];
  let c1y, c2y, c3y;
  let [s1x, s1y, s2x, s2y] = [170, 230, 200, 220];

  my  -= 20*i;
  c1x += 10*i;
  [c1y, c2y, c3y] = [my-10, my-10, my+10]
  s1y -= 20*i;
  s2y -= 20*i;

  return {
    mx: mx,  my: my,
    c1x: c1x, c1y: c1y, c2x: c2x, c2y: c2y,
    c3x: c3x, c3y: c3y,
    s1x: s1x, s1y: s1y, s2x: s2x, s2y: s2y
  };
}

function modifyPaths(svg_root, range) {
  range.forEach((_, index) => {
    const {
      mx, my,
      c1x, c1y, c2x, c2y, c3x, c3y,
      s1x, s1y, s2x, s2y
    } = getBezierParams(index);

    const area  = document
      .getElementById(`svg_area_${index}`);

    area.setAttribute("d",
      `M ${mx}  ${my}
       C ${c1x} ${c1y}, ${c2x} ${c2y}, ${c3x} ${c3y}
       S ${s1x} ${s1y}, ${s2x} ${s2y}
       L   200,     0  L    0,     0
       Z ${mx}  ${my}`);
  });
}

document.addEventListener(
  "DOMContentLoaded", function(event) {
    const svg_root  = document
      .getElementById("svg_root");
    const range = [...Array(10)];

    createPaths(svg_root, range);
    modifyPaths(svg_root, range);
});


